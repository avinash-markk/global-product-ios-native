//
//  NotificationService.swift
//  NotificationServiceExt
//
//  Created by Pushkar Deshmukh on 27/08/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import UserNotifications
import FirebaseRemoteConfig

class NotificationService: UNNotificationServiceExtension {

    var contentHandler: ((UNNotificationContent) -> Void)?
    var bestAttemptContent: UNMutableNotificationContent?

    override func didReceive(_ request: UNNotificationRequest, withContentHandler contentHandler: @escaping (UNNotificationContent) -> Void) {
        self.contentHandler = contentHandler
        bestAttemptContent = (request.content.mutableCopy() as? UNMutableNotificationContent)
        
        setupRemoteConfig()
        
        if let bestAttemptContent = bestAttemptContent, let image = bestAttemptContent.userInfo["image"] as? String, let mediaType = bestAttemptContent.userInfo["mediaType"] as? String  {
            // Modify the notification content here...
            
            guard let url = getImageURL(image: image, mediaType: mediaType) else {
                contentHandler(bestAttemptContent)
                return
            }
            
            DownloadService.shared.getImage(with: url.absoluteString) { (url) in
                do {
                    let attachment = try UNNotificationAttachment(identifier: "image", url: url, options: nil)
                    bestAttemptContent.attachments = [attachment]
                    
                    contentHandler(bestAttemptContent)
                } catch {
                    print(error)
                    contentHandler(bestAttemptContent)
                }
                
            }
        }
    }

    
    override func serviceExtensionTimeWillExpire() {
        // Called just before the extension will be terminated by the system.
        // Use this as an opportunity to deliver your "best attempt" at modified content, otherwise the original push payload will be used.
        if let contentHandler = contentHandler, let bestAttemptContent =  bestAttemptContent {
            contentHandler(bestAttemptContent)
        }
    }

    fileprivate func setupRemoteConfig() {
        RemoteConfig.remoteConfig().setDefaults(ApiConfig.remoteconfigDefaults)
        
        let fetchDuration: TimeInterval = 0
        RemoteConfig.remoteConfig().fetch(withExpirationDuration: fetchDuration) {
            [weak self] (status, error) in
            // Error
            guard error == nil else {
                print ("Uh-oh. Got an error fetching remote values \(String(describing: error))")
                return
            }
            // Success
            RemoteConfig.remoteConfig().activateFetched()
        }
    }
    
    fileprivate func getImageURL(image: String, mediaType: String) -> URL? {
        let tempUrl: URL?
        if isValidURL(urlString: image) {
            tempUrl = URL(string: image)
        } else {
            tempUrl = CloudinaryService.shared.getCloudinaryURL(mediaType: mediaType, mediaName: image)
        }
        
        return tempUrl
    }
    
    fileprivate func isValidURL(urlString: String) -> Bool {
        let detector = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
        if let match = detector.firstMatch(in: urlString, options: [], range: NSRange(location: 0, length: urlString.utf16.count)) {
            // it is a link, if the match covers the whole string
            return match.range.length == urlString.utf16.count
        } else {
            return false
        }
    }
    
}
