//
//  CameraColorPickerView.swift
//  MarkkCamera
//
//  Created by Sebastian Sztemberg on 13/02/2019.
//

import UIKit

protocol CameraColorPickerViewDelegate: AnyObject {
    func cameraColorPickerDidSelectColor(_ color: UIColor)
}

class CameraColorPickerView: UIView {
    public weak var delegate: CameraColorPickerViewDelegate?

    fileprivate let colors = ["#FFFFFF", "#000000", "#3896F0", "#70BF50", "#FCCA5B", "#FC8C32", "#EC4855", "#D10569", "#A307B9", "#EC0013", "#ED858E", "#FFD1D3", "#FFDAB4", "#FFC282", "#D18E46", "#986339", "#432224", "#1C4929", "#252626", "#353636", "#555455", "#727273", "#989898", "#B1B2B1", "#C6C6C7", "#DADADA", "#EEEEEE"]
    fileprivate lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 6
        layout.scrollDirection = .horizontal

        let collectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        collectionView.backgroundColor = UIColor.clear
        collectionView.isPagingEnabled = true
        collectionView.delegate = self
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.dataSource = self
        collectionView.register(CameraColorPickerCollectionViewCell.self, forCellWithReuseIdentifier: "ColorCell")
        return collectionView
    }()

    fileprivate lazy var pageControl: UIPageControl = {
        let pageControl = UIPageControl(frame: CGRect.zero)
        pageControl.numberOfPages = 3
        pageControl.pageIndicatorTintColor = UIColor.lightGray
        pageControl.currentPageIndicatorTintColor = UIColor.white
        pageControl.backgroundColor = UIColor.clear
        return pageControl
    }()

    override init(frame: CGRect) {
        super.init(frame: CGRect.zero)

        initUI()
    }

    func initUI() {
        addSubview(collectionView)
        collectionView.snp.makeConstraints { make in
            make.top.equalTo(self)
            make.left.right.equalTo(self)
            make.height.equalTo(45)
        }
        addSubview(pageControl)
        pageControl.snp.makeConstraints { make in
            make.top.equalTo(collectionView.snp.bottom)
            make.bottom.equalTo(self)
            make.centerX.equalTo(self)
        }
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}

extension CameraColorPickerView: UICollectionViewDelegateFlowLayout, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = (collectionView.frame.width - 6) / 9 - 6
        return CGSize(width: size, height: size)
    }
    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
//        return
//    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 6, bottom: 0, right: 6)
    }
}
extension CameraColorPickerView: UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 3
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return colors.count / 3
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ColorCell", for: indexPath) as! CameraColorPickerCollectionViewCell
        cell.colorView.backgroundColor = UIColor(hexString: colors[indexPath.section * colors.count / 3 + indexPath.row])
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        delegate?.cameraColorPickerDidSelectColor(UIColor(hexString: colors[indexPath.section * colors.count / 3 + indexPath.row]))
    }

    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        pageControl.currentPage = Int(ceil(scrollView.contentOffset.x / scrollView.frame.width))
    }
}
