//
//  TooltipView.swift
//  MarkkCamera
//
//  Created by Sebastian Sztemberg on 28/02/2019.
//

import UIKit

class TooltipView: UIView {
    public var text: String? {
        didSet {
            label.text = text
        }
    }

    fileprivate lazy var innerView: UIView = {
        let innerView = UIView()
        innerView.backgroundColor = UIColor.white
        innerView.layer.cornerRadius = 6
        innerView.layer.masksToBounds = true
        return innerView
    }()

    fileprivate lazy var triangleView: TriangleView = {
        let view = TriangleView(color: UIColor.white)
        return view
    }()

    fileprivate lazy var label: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.black
        label.font = UIFont.boldSystemFont(ofSize: 12)
        label.textAlignment = .center
        label.adjustsFontSizeToFitWidth = true
        return label
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)

        initUI()
    }

    fileprivate func initUI() {
        backgroundColor = UIColor.clear
        addSubview(innerView)
        innerView.snp.makeConstraints { make in
            make.bottom.left.right.equalTo(self)
        }

        addSubview(triangleView)
        triangleView.snp.makeConstraints { make in
            make.height.equalTo(10)
            make.width.equalTo(12)
            make.centerX.equalTo(self)
            make.top.equalTo(self)
            make.bottom.equalTo(innerView.snp.top)
        }

        innerView.addSubview(label)
        label.snp.makeConstraints { make in
            make.top.left.right.bottom.equalTo(innerView).inset(6)
        }
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
