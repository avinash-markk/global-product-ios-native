//
//  TriangleView.swift
//  MarkkCamera
//
//  Created by Sebastian Sztemberg on 06/02/2019.
//

import UIKit

class TriangleView: UIView {
    fileprivate var triangleColor = UIColor.white

    init(color: UIColor) {
        super.init(frame: CGRect.zero)
        triangleColor = color
        backgroundColor = UIColor.clear
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override func draw(_ rect: CGRect) {
        guard let context = UIGraphicsGetCurrentContext() else { return }

        context.beginPath()
        context.move(to: CGPoint(x: rect.minX, y: rect.maxY))
        context.addLine(to: CGPoint(x: rect.maxX, y: rect.maxY))
        context.addLine(to: CGPoint(x: (rect.maxX / 2.0), y: rect.minY))
        context.closePath()

        context.setFillColor(triangleColor.cgColor)
        context.fillPath()
    }
}
