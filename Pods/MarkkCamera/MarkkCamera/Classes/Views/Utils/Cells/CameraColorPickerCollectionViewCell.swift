//
//  CameraColorPickerCollectionViewCell.swift
//  MarkkCamera
//
//  Created by Sebastian Sztemberg on 13/02/2019.
//

import UIKit

class CameraColorPickerCollectionViewCell: UICollectionViewCell {
    public lazy var colorView: UIView = {
        let view = UIView(frame: CGRect.zero)
        view.layer.borderWidth = 2
        view.layer.borderColor = UIColor.white.cgColor
        return view
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)

        initUI()
    }

    func initUI() {
        addSubview(colorView)
        colorView.snp.makeConstraints { make in
            make.edges.equalTo(self)
        }
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        colorView.layer.cornerRadius = colorView.frame.width/2
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}
