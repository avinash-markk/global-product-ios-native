//
//  CameraAuthorizationView.swift
//  Sminq
//
//  Created by Sebastian Sztemberg on 30/01/2019.
//  Copyright © 2019 Suraj Sagare. All rights reserved.
//

import UIKit
import SnapKit

protocol CameraAuthorizationViewDelegate: AnyObject {
    func cameraAuthorizationClose()
    func authorizationPermissionGiven()
    func cameraAuthorizationGiven(permission: String)
    func microphoneAuthorizationGiven(permission: String)
}

class CameraAuthorizationView: UIView {
    public weak var delegate: CameraAuthorizationViewDelegate?

    fileprivate lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.text = "permission_title".localized
        label.font = UIFont(name: "Inter-Regular", size: 20.0)
        label.textColor = UIColor.white
        label.textAlignment = .center
        label.numberOfLines = 0
        return label
    }()

    fileprivate lazy var closeButton: UIButton = {
        let button = UIButton(type: .custom)
        button.showsTouchWhenHighlighted = true
        button.setImage(UIImage.bundledImage(named: "iconClose24WhitePureShadow"), for: .normal)
        button.addTarget(self, action: #selector(handleClose), for: .touchUpInside)
        return button
    }()

    fileprivate lazy var microphoneButton: CameraPermissionButton = {
        let button = CameraPermissionButton(type: .custom)
        button.inactiveText = "microphone_access".localized
        button.activeText = "microphone_access_given".localized
        button.addTarget(self, action: #selector(handleMicrophone), for: .touchUpInside)
        return button
    }()

    fileprivate lazy var cameraButton: CameraPermissionButton = {
        let button = CameraPermissionButton(type: .custom)
        button.inactiveText = "camera_access".localized
        button.activeText = "camera_access_given".localized
        button.addTarget(self, action: #selector(handleCamera), for: .touchUpInside)
        return button
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)

        initUI()
        checkPermissions()
    }

    fileprivate func initUI() {
        self.backgroundColor = CameraUtils.CameraColors.gray200
        addSubview(closeButton)
        closeButton.snp.makeConstraints { make in
            make.size.equalTo(30)
            make.top.equalTo(safeArea.top).offset(20)
            make.right.equalTo(self).offset(-20)
        }

        addSubview(titleLabel)
        titleLabel.snp.makeConstraints { make in
            make.top.equalTo(safeArea.top).offset(144)
            make.left.equalTo(self).offset(46)
            make.right.equalTo(self).offset(-46)
        }

        addSubview(microphoneButton)
        microphoneButton.snp.makeConstraints { make in
            make.left.equalTo(self).offset(20)
            make.right.equalTo(self).offset(-20)
            make.height.equalTo(48)
            make.bottom.equalTo(safeArea.bottom).offset(-20)
        }

        addSubview(cameraButton)
        cameraButton.snp.makeConstraints { make in
            make.left.equalTo(self).offset(20)
            make.right.equalTo(self).offset(-20)
            make.height.equalTo(48)
            make.bottom.equalTo(microphoneButton.snp.top).offset(-16)
        }
    }

    func checkPermissions() {
        microphoneButton.active = CameraPermissionManager.checkPermission(type: .microphone)
        cameraButton.active = CameraPermissionManager.checkPermission(type: .camera)

        if microphoneButton.active && cameraButton.active {
            delegate?.authorizationPermissionGiven()
        }
    }

    @objc fileprivate func handleClose(sender: UIButton) {
        delegate?.cameraAuthorizationClose()
    }

    @objc fileprivate func handleMicrophone(sender: UIButton) {
        CameraPermissionManager.requestPermission(type: .microphone) { granted in
            self.delegate?.microphoneAuthorizationGiven(permission: "\(granted ? "granted" : "denied")")
            if granted {
                self.checkPermissions()
            }
        }
    }

    @objc fileprivate func handleCamera(sender: UIButton) {
        CameraPermissionManager.requestPermission(type: .camera) { granted in
            self.delegate?.cameraAuthorizationGiven(permission: "\(granted ? "granted" : "denied")")
            if granted {
                self.checkPermissions()
            }
        }
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
