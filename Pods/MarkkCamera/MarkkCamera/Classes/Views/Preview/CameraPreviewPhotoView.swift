//
//  CameraPreviewPhotoView.swift
//  MarkkCamera
//
//  Created by Sebastian Sztemberg on 01/02/2019.
//

import UIKit

class CameraPreviewPhotoView: UIView {
    public var photoImageView = UIImageView()

    init(image: UIImage) {
        super.init(frame: CGRect.zero)
        photoImageView.image = image

        initUI()
    }

    func initUI() {
        photoImageView.contentMode = .scaleAspectFill
        photoImageView.layer.masksToBounds = true
        addSubview(photoImageView)
        photoImageView.snp.makeConstraints { make in
            make.edges.equalTo(self)
        }
    }

    public func updatePhoto(_ photo: UIImage) {
        photoImageView.image = photo
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}
