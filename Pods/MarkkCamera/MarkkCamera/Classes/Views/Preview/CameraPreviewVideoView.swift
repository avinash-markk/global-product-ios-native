//
//  CameraPreviewVideoView.swift
//  MarkkCamera
//
//  Created by Sebastian Sztemberg on 31/01/2019.
//

import UIKit
import AVFoundation

class CameraPreviewVideoView: UIView {
    fileprivate var videoURL: URL?
    fileprivate var queuePlayer = AVQueuePlayer()
    fileprivate var playerItem: AVPlayerItem?
    fileprivate var playerLooper: AVPlayerLooper?

    fileprivate var videoPlayer: AVPlayer?
    fileprivate var playerLayer: AVPlayerLayer?

    init(videoUrl: URL) {
        super.init(frame: CGRect.zero)
        videoURL = videoUrl
        initUI()
    }

    fileprivate func initUI() {
        backgroundColor = UIColor.black
        setupVideo()
    }

    fileprivate func setupVideo() {
        guard let videoUrl = videoURL else { return }

//        videoPlayer = AVPlayer(url: videoUrl)
        playerLayer = AVPlayerLayer(player: queuePlayer)
        playerItem = AVPlayerItem(url: videoUrl)
        playerLooper = AVPlayerLooper(player: queuePlayer, templateItem: playerItem!)
        playerLayer?.frame = bounds
        playerLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
        queuePlayer.play()
        layer.addSublayer(playerLayer!)
    }

    public func setStartTime(_ time: Double) {
        queuePlayer.seek(to: CMTime(seconds: time, preferredTimescale: 1000))
    }

    public func setEndTime(_ time: Double) {
        queuePlayer.pause()
        playerItem?.forwardPlaybackEndTime = CMTime(seconds: time, preferredTimescale: 1000)
        queuePlayer.play()
    }
    
    public func setMute(_ muted: Bool) {
        queuePlayer.isMuted = muted
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        playerLayer?.frame = bounds
    }
}
