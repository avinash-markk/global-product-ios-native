//
//  CameraPreviewOverlayView.swift
//  MarkkCamera
//
//  Created by Sebastian Sztemberg on 01/02/2019.
//

import UIKit

protocol CameraPreviewOverlayViewDelegate: AnyObject {
    func cameraPreviewOverlayDidSelectDownload()
    func cameraPreviewOverlayDidSelectClose()
    func cameraPreviewOverlayDidSelectLocation()
    func cameraPreviewOverlayDidSelectStickers()
    func cameraPreviewOverlayDidSelectText()
    func cameraPreviewOverlayDidSelectCrop()
    func cameraPreviewOverlayDidSelectDraw()
    func cameraPreviewOverlayDidSelectAccept()
    func cameraPreviewOverlayDidSelectMute(muted: Bool)
}

class CameraPreviewOverlayView: UIView, UIGestureRecognizerDelegate {
    public weak var delegate: CameraPreviewOverlayViewDelegate?
    public var cameraManager: CameraManager?
    
    public var showCrop: Bool = false {
        didSet {
            cropButton.isHidden = !showCrop
        }
    }
    
    public var showMute: Bool = false {
        didSet {
            muteButton.isHidden = !showMute
        }
    }
    
    fileprivate var muted = false
    
    fileprivate lazy var closeButton: UIButton = {
        let button = UIButton(type: .custom)
        button.showsTouchWhenHighlighted = true
        button.setImage(UIImage.bundledImage(named: "iconClose24WhitePureShadow"), for: .normal)
        button.addTarget(self, action: #selector(handleClosePress), for: .touchUpInside)
        return button
    }()

    fileprivate lazy var downloadButton: UIButton = {
        let button = UIButton(type: .custom)
        button.showsTouchWhenHighlighted = true
        button.setImage(UIImage.bundledImage(named: "iconDownload"), for: .normal)
        button.addTarget(self, action: #selector(handleDownloadPress), for: .touchUpInside)
        return button
    }()

    fileprivate lazy var postButton: UIButton = {
        let button = UIButton(type: .custom)
        button.showsTouchWhenHighlighted = true
        button.setTitle("rate".localized, for: .normal)
        button.setImage(UIImage.bundledImage(named: "iconChevronRight24BlackDark50"), for: .normal)
        button.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
        button.titleLabel?.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
        button.imageView?.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
        button.addTarget(self, action: #selector(handleAcceptPress), for: .touchUpInside)
        button.setTitleColor(CameraUtils.CameraColors.blackDark100.withAlphaComponent(0.5), for: .normal)
        button.titleLabel?.font = UIFont(name: "Inter-Bold", size: 14.0)
        button.backgroundColor = UIColor.white
        button.layer.cornerRadius = 16
        return button
    }()
    
    fileprivate lazy var postButtonSubview: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handlePostButtonTap))
        tapGesture.delegate = self
        view.addGestureRecognizer(tapGesture)
        return view
    }()

    fileprivate lazy var locationButton: UIButton = {
        let button = UIButton(type: .custom)
        button.showsTouchWhenHighlighted = true
        button.setImage(UIImage.bundledImage(named: "iconMarker"), for: .normal)
        button.addTarget(self, action: #selector(handleLocationPress), for: .touchUpInside)
        return button
    }()

    fileprivate lazy var stickerButton: UIButton = {
        let button = UIButton(type: .custom)
        button.showsTouchWhenHighlighted = true
        button.setImage(UIImage.bundledImage(named: "iconSticker24WhitePureShadow"), for: .normal)
        button.addTarget(self, action: #selector(handleStickerPress), for: .touchUpInside)
        return button
    }()

    fileprivate lazy var textButton: UIButton = {
        let button = UIButton(type: .custom)
        button.showsTouchWhenHighlighted = true
        button.setImage(UIImage.bundledImage(named: "iconText"), for: .normal)
        button.addTarget(self, action: #selector(handleTextPress), for: .touchUpInside)
        return button
    }()

    fileprivate lazy var drawButton: UIButton = {
        let button = UIButton(type: .custom)
        button.showsTouchWhenHighlighted = true
        button.setImage(UIImage.bundledImage(named: "iconPencil"), for: .normal)
        button.addTarget(self, action: #selector(handleDrawPress), for: .touchUpInside)
        return button
    }()
    
    fileprivate lazy var muteButton: UIButton = {
        let button = UIButton(type: .custom)
        button.showsTouchWhenHighlighted = true
        button.setImage(UIImage.bundledImage(named: "iconVolumeEnabled"), for: .normal)
        button.addTarget(self, action: #selector(handleMute), for: .touchUpInside)
        return button
    }()

    fileprivate lazy var cropButton: UIButton = {
        let button = UIButton(type: .custom)
        button.showsTouchWhenHighlighted = true
        button.setImage(UIImage.bundledImage(named: "iconCrop"), for: .normal)
        button.addTarget(self, action: #selector(handleCropPress), for: .touchUpInside)
        return button
    }()
    
    init(cameraManager: CameraManager?, frame: CGRect) {
        super.init(frame: frame)
        self.cameraManager = cameraManager

        initUI()
    }

    func initUI() {
        addSubview(closeButton)
        closeButton.snp.makeConstraints { make in
            make.size.equalTo(30)
            make.top.equalTo(self).offset(20)
            make.right.equalTo(self).offset(-20)
        }

        addSubview(locationButton)
        locationButton.snp.makeConstraints { make in
            make.size.equalTo(30)
            make.top.equalTo(self).offset(20)
            make.left.equalTo(self).offset(20)
        }

        addSubview(stickerButton)
        stickerButton.snp.makeConstraints { make in
            make.size.equalTo(30)
            make.centerY.equalTo(locationButton)
            make.left.equalTo(locationButton.snp.right).offset(10)
        }

        addSubview(textButton)
        textButton.snp.makeConstraints { make in
            make.size.equalTo(30)
            make.centerY.equalTo(locationButton)
            make.left.equalTo(stickerButton.snp.right).offset(10)
        }

        addSubview(drawButton)
        drawButton.snp.makeConstraints { make in
            make.size.equalTo(30)
            make.centerY.equalTo(locationButton)
            make.left.equalTo(textButton.snp.right).offset(10)
        }
        
        addSubview(muteButton)
        muteButton.snp.makeConstraints { make in
            make.size.equalTo(30)
            make.centerY.equalTo(locationButton)
            make.left.equalTo(drawButton.snp.right).offset(10)
        }

        addSubview(downloadButton)
        downloadButton.snp.makeConstraints { make in
            make.size.equalTo(30)
            make.bottom.equalTo(safeArea.bottom).offset(-20)
            make.left.equalTo(self).offset(20)
        }

        addSubview(cropButton)
        cropButton.isHidden = true
        cropButton.snp.makeConstraints { make in
            make.size.equalTo(25)
            make.centerY.equalTo(downloadButton)
            make.left.equalTo(downloadButton.snp.right).offset(20)
        }

        addSubview(postButtonSubview)
        postButtonSubview.snp.makeConstraints { make in
            make.width.equalTo(89)
            make.centerY.equalTo(downloadButton)
            make.height.equalTo(44)
            make.right.equalTo(self).offset(-32)
        }
        postButtonSubview.addSubview(postButton)
        postButton.snp.makeConstraints { make in
            make.width.equalTo(89)
            make.centerY.equalTo(postButtonSubview)
            make.height.equalTo(32)
            make.right.equalTo(postButtonSubview)
        }
    
    }

    // MARK: - Handle buttons
    
    @objc fileprivate func handleClosePress(sender: UIButton) {
        delegate?.cameraPreviewOverlayDidSelectClose()
    }

    @objc fileprivate func handleDownloadPress(sender: UIButton) {
        delegate?.cameraPreviewOverlayDidSelectDownload()
    }

    @objc fileprivate func handleLocationPress(sender: UIButton) {
        delegate?.cameraPreviewOverlayDidSelectLocation()
    }

    @objc fileprivate func handleStickerPress(sender: UIButton) {
        delegate?.cameraPreviewOverlayDidSelectStickers()
    }

    @objc fileprivate func handleTextPress(sender: UIButton) {
        delegate?.cameraPreviewOverlayDidSelectText()
    }

    @objc fileprivate func handleDrawPress(sender: UIButton) {
        delegate?.cameraPreviewOverlayDidSelectDraw()
    }

    @objc fileprivate func handleCropPress(sender: UIButton) {
        delegate?.cameraPreviewOverlayDidSelectCrop()
    }

    @objc fileprivate func handleAcceptPress(sender: UIButton) {
        delegate?.cameraPreviewOverlayDidSelectAccept()
    }
    
    @objc fileprivate func handleMute(sender: UIButton) {
        muted = !muted
        muteButton.setImage(UIImage.bundledImage(named: muted ? "iconVolumeDisabled" : "iconVolumeEnabled"), for: .normal)
        delegate?.cameraPreviewOverlayDidSelectMute(muted: muted)
    }

    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        let result = super.hitTest(point, with: event)
        if result == self { return nil }
        return result
    }
    
    @objc fileprivate func handlePostButtonTap(_ sender: UITapGestureRecognizer) {
        delegate?.cameraPreviewOverlayDidSelectAccept()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}
