//
//  CameraPreviewView.swift
//  MarkkCamera
//
//  Created by Sebastian Sztemberg on 31/01/2019.
//

import UIKit
import Photos
import KRProgressHUD

public protocol CameraPreviewViewDelegate: AnyObject {
    func cameraPreviewDidClose()
    func cameraPreviewDidDownload()
    func cameraPreviewReceivedError(_ error: String)
    func cameraPreviewDidSelectLocation()
    func cameraPreviewDidSelectMood()
    func cameraPreviewDidAccept(_ asset: CameraAsset, addons: [UIView])
    func cameraPreviewDidReceiveStickerSearch(text: String)
    func cameraPreviewDidSelectDraw()
    func cameraPreviewDidSelectSticker(sticker: Sticker)
    func cameraDidAddTextSticker(text: String)
}

public class CameraPreviewView: UIView {
    public weak var delegate: CameraPreviewViewDelegate?

    public var cameraAsset: CameraAsset?

    public var previewView: UIView?
    public var cameraManager: CameraManager?

    fileprivate var stickersContainerView = CameraStickersContainerView(frame: CGRect.zero)
    fileprivate var drawableContainerView = CameraDrawableContainerView(frame: CGRect.zero)

    fileprivate var cameraPreviewOverlayView: CameraPreviewOverlayView!
    fileprivate var cameraStickerPickerView: CameraStickerPickerView!
    
    fileprivate lazy var safeAreaTitleOverlayView: UIView = {
        let view = UIView()
        view.backgroundColor = CameraUtils.CameraColors.blackDark100.withAlphaComponent(0.75)
        return view
    }()
    
    fileprivate lazy var safeAreaLabel: UIView = {
        let label = UILabel()
        label.text = "TEXT & STICKERS PLACED IN THIS AREA MIGHT GET CROPPED OR HIDDEN"
        label.font = UIFont(name: "Inter-Bold", size: 12.0)
        label.textColor = UIColor.init(red: 242/255, green: 242/255, blue: 242/255, alpha: 1.0)
        label.textAlignment = .center
        label.numberOfLines = 0
        return label
    }()
    
    fileprivate lazy var safeAreaOverlayView: UIView = {
        let view = UIView(frame: CGRect(x: 0, y: 56, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height))
        return view
    }()
    
    fileprivate lazy var safeAreaBorderView: UIView = {
        let view = UIView(frame: CGRect(x: 16, y: 56, width: UIScreen.main.bounds.width - 32, height: UIScreen.main.bounds.height - 112))
        return view
    }()
    
    fileprivate lazy var safeAreaOverlayLayer: CAShapeLayer = {
        let layer = CAShapeLayer()
        return layer
    }()
    
    fileprivate lazy var safeAreaBorderLayer: CAShapeLayer = {
        let layer = CAShapeLayer()
        return layer
    }()
    
    fileprivate var cameraPhotoCroppingView: CameraPhotoCroppingView?
    
    init(asset: CameraAsset, cameraManager: CameraManager?) {
        super.init(frame: CGRect.zero)
        self.cameraManager = cameraManager
        cameraPreviewOverlayView = CameraPreviewOverlayView(cameraManager: cameraManager, frame: CGRect.zero)
        cameraStickerPickerView = CameraStickerPickerView(cameraManager: cameraManager, frame: CGRect.zero)
        cameraAsset = asset

        backgroundColor = UIColor.black
        layer.masksToBounds = true

        if asset.type == .video, let url = asset.url {
            previewView = CameraPreviewVideoView(videoUrl: url)
        } else if asset.type == .photo, let image = asset.image {
            previewView = CameraPreviewPhotoView(image: image)
        }
        guard let previewView = previewView else { return }

        addSubview(previewView)
        previewView.snp.makeConstraints { make in
            make.center.equalTo(self)
            make.width.equalTo(self)
            make.height.equalTo(snp.width).multipliedBy(CameraUtils.cameraRatio)
        }

        initPreview()
        initSafeAreaPreviewComponents()
        self.hideSafeAreaPreview(value: true)
    }

    func initPreview() {
        guard let asset = cameraAsset, let previewView = previewView  else { return }

        addSubview(drawableContainerView)
        drawableContainerView.delegate = self
        drawableContainerView.isEnabled = false
        drawableContainerView.snp.makeConstraints { make in
            make.edges.equalTo(previewView)
        }

        addSubview(stickersContainerView)
        stickersContainerView.delegate = self
        stickersContainerView.snp.makeConstraints { make in
            make.edges.equalTo(previewView)
        }
        cameraPreviewOverlayView.delegate = self
        cameraPreviewOverlayView.isHidden = true
        cameraPreviewOverlayView.showCrop = asset.type == .photo && asset.fromGallery
        cameraPreviewOverlayView.showMute = asset.type == .video
        addSubview(cameraPreviewOverlayView)
        cameraPreviewOverlayView.snp.makeConstraints { make in
            make.edges.equalTo(previewView)
        }

        addSubview(cameraStickerPickerView)
        cameraStickerPickerView.isHidden = true
        cameraStickerPickerView.delegate = self
        cameraStickerPickerView.snp.makeConstraints { make in
            make.height.equalTo(self)
            make.bottom.equalTo(self)
            make.left.right.equalTo(self)
        }
    }
    
    func initSafeAreaPreviewComponents() {
        
        addSubview(safeAreaTitleOverlayView)
        safeAreaTitleOverlayView.snp.makeConstraints { make in
            make.top.equalTo(safeArea.top).offset(0)
            make.left.right.equalTo(self)
            make.width.equalTo(self)
            make.height.equalTo(56)
            make.width.equalTo(self)
        }
        
        safeAreaTitleOverlayView.addSubview(safeAreaLabel)
        safeAreaLabel.snp.makeConstraints { make in
            make.top.equalTo(8)
            make.left.equalTo(self).offset(40)
            make.right.equalTo(self).offset(-40)
            make.height.equalTo(40)
        }
        
        self.addOverlayLayer()
        self.addBorderLayer()
    }
    
    func addOverlayLayer() {
        let pathBigRect = UIBezierPath(rect: safeAreaOverlayView.frame)
        let pathSmallRect = UIBezierPath(rect: safeAreaBorderView.frame)
        
        pathBigRect.append(pathSmallRect)
        pathBigRect.usesEvenOddFillRule = false

        safeAreaOverlayLayer.path = pathBigRect.cgPath
        safeAreaOverlayLayer.fillRule = CAShapeLayerFillRule.evenOdd
        safeAreaOverlayLayer.fillColor = CameraUtils.CameraColors.blackDark100.withAlphaComponent(0.75).cgColor
        self.layer.addSublayer(safeAreaOverlayLayer)
    }
    
    func addBorderLayer() {
        safeAreaBorderLayer.frame = safeAreaBorderView.bounds
        let shapeRect = CGRect(x: safeAreaBorderView.frame.origin.x, y: safeAreaBorderView.frame.origin.y, width: safeAreaBorderView.frame.size.width, height: safeAreaBorderView.frame.size.height)
    
        safeAreaBorderLayer.fillColor = UIColor.clear.cgColor
        safeAreaBorderLayer.strokeColor = UIColor.white.cgColor
        safeAreaBorderLayer.lineWidth = 1
        safeAreaBorderLayer.lineJoin = CAShapeLayerLineJoin.round
        safeAreaBorderLayer.lineDashPattern = [6,3]
        safeAreaBorderLayer.path = UIBezierPath(roundedRect: shapeRect, cornerRadius: 5).cgPath
        self.layer.addSublayer(safeAreaBorderLayer)
    }
    
    func hideSafeAreaPreview(value: Bool) {
            safeAreaTitleOverlayView.isHidden = value
            safeAreaLabel.isHidden = value
            safeAreaOverlayLayer.isHidden = value
            safeAreaBorderLayer.isHidden = value
    }

    public func loadStickers() {
        if let stickers = cameraAsset?.selectedPlace?.stickers {
            if let selectedMood = cameraAsset?.selectedMood, let moodIntValue = Int(selectedMood) {
                switch moodIntValue {
                case 1:
                    cameraStickerPickerView.stickers = stickers.sorted(by: { $0.weight < $1.weight })
                case 2:
                    let stickersWithWeight31 = stickers.filter({ $0.weight == 3 || $0.weight == 1 })
                   cameraStickerPickerView.stickers = stickers.filter({ $0.weight == 2 }) + stickersWithWeight31.sorted(by: { $0.weight > $1.weight })
                    
                case 3:
                     cameraStickerPickerView.stickers = stickers.sorted(by: { $0.weight > $1.weight })
                    
                default:
                    cameraStickerPickerView.stickers = stickers.sorted(by: { $0.weight < $1.weight })
                }
            } else {
                cameraStickerPickerView.stickers = stickers
            }
        }
    }

    public func showStickersPicker() {
        cameraStickerPickerView.show(true, mood: cameraAsset?.selectedMood)
        cameraPreviewOverlayView.fadeOut()
        loadStickers()
    }
    
    public func showPreviewOverlay() {
        cameraPreviewOverlayView.fadeIn()
    }

    public func hidePreviewOverlay() {
        cameraPreviewOverlayView.fadeOut()
    }
    
    public func updateKeyboardFrame(keyboardHeight: CGFloat) {
        let offset = keyboardHeight - (previewView?.frame.origin.y ?? 0)
        stickersContainerView.updateKeyboardOffset(offset: offset)
        cameraStickerPickerView.updateKeyboardFrame(keyboardHeight: offset)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}

extension CameraPreviewView: CameraStickersContainerViewDelegate {
    
    func cameraTextStickerAdded(text: String) {
        self.delegate?.cameraDidAddTextSticker(text: text)
    }
    
    
    func fadeOutCameraPreviewView() {
        cameraPreviewOverlayView.fadeOut()
    }
    
    func cameraStickersContainerDidBeginInteraction() {
        cameraPreviewOverlayView.fadeOut()
        self.hideSafeAreaPreview(value: false)
    }

    func cameraStickersContainerDidEndInteraction() {
        cameraPreviewOverlayView.fadeIn()
        self.hideSafeAreaPreview(value: true)
    }
    
    
}

extension CameraPreviewView: CameraPreviewOverlayViewDelegate {
    func cameraPreviewOverlayDidSelectMute(muted: Bool) {
        if let videoPreviewView = previewView as? CameraPreviewVideoView {
            videoPreviewView.setMute(muted)
            cameraAsset?.muted = muted
        }
    }
    
    func cameraPreviewOverlayDidSelectAccept() {
        guard var cameraAsset = cameraAsset, let previewView = previewView else { return }

        if let photoPreviewView = previewView as? CameraPreviewPhotoView {
            cameraAsset.image = photoPreviewView.photoImageView.image
        }

        let imageStickers: [Sticker] = stickersContainerView.stickers.compactMap { sticker in
            if let stickerView = sticker as? CameraStickerView {
                return stickerView.sticker
            }
            return nil
        }

        let textStickers: [TextSticker] = stickersContainerView.stickers.compactMap { sticker in
            if let textView = sticker as? CameraTextView {
                let radians = atan2(textView.transform.b, textView.transform.a)
                let degrees = radians * 180 / .pi // Get rotation

                var textPosition = TextStickerPosition()
                textPosition.height = textView.frame.height
                textPosition.width = textView.frame.width
                textPosition.rotation = degrees
                textPosition.topLeft = [textView.frame.minX, textView.frame.minY]

                var textSticker = TextSticker()
                if let pointSize = textView.textView.font?.pointSize {
                    textSticker.fontSize = Int(pointSize)
                }
                textSticker.text = textView.textView.text
                textSticker.position = textPosition
                textSticker.uniqueId = textView.tag

                return textSticker
            }
            return nil
        }
        cameraAsset.imageStickers = imageStickers
        cameraAsset.textStickers = textStickers

        delegate?.cameraPreviewDidAccept(cameraAsset, addons: [stickersContainerView, drawableContainerView])
    }

    func cameraPreviewOverlayDidSelectCrop() {
        if let cameraAsset = cameraAsset, cameraAsset.type == .photo, let image = cameraAsset.image, let previewView = previewView {
            cameraPhotoCroppingView = CameraPhotoCroppingView(photo: image)
            if let cameraPhotoCroppingView = cameraPhotoCroppingView {
                cameraPhotoCroppingView.delegate = self
                addSubview(cameraPhotoCroppingView)
                cameraPhotoCroppingView.snp.makeConstraints { make in
                    make.edges.equalTo(previewView)
                }
            }
        }
    }

    func cameraPreviewOverlayDidSelectText() {
        stickersContainerView.addText()
    }

    func cameraPreviewOverlayDidSelectDraw() {
        cameraPreviewOverlayView.fadeOut()
        stickersContainerView.isUserInteractionEnabled = false
        drawableContainerView.isEnabled = true
        delegate?.cameraPreviewDidSelectDraw()
    }

    func cameraPreviewOverlayDidSelectClose() {
        delegate?.cameraPreviewDidClose()
    }

    func cameraPreviewOverlayDidSelectLocation() {
        delegate?.cameraPreviewDidSelectLocation()
    }

    func cameraPreviewOverlayDidSelectStickers() {
        cameraStickerPickerView.show(true)
        cameraPreviewOverlayView.fadeOut()
    }
    
    func downloadAsset() {
        guard var cameraAsset = cameraAsset, let previewView = previewView else { return }
        
        if let photoPreviewView = previewView as? CameraPreviewPhotoView {
            cameraAsset.image = photoPreviewView.photoImageView.image
        }
        
        cameraManager?.generateFinalAsset(previewView: self, asset: cameraAsset, addons: [stickersContainerView, drawableContainerView]) { asset in
            KRProgressHUD.show(withMessage: "loading".localized)
            switch asset.type {
            case CameraAssetType.photo:
                PHPhotoLibrary.shared().performChanges({
                    PHAssetChangeRequest.creationRequestForAsset(from: asset.image!)
                }, completionHandler: { (success, _) in
                    if success {
                        DispatchQueue.main.async {
                            self.delegate?.cameraPreviewDidDownload()
                        }
                    }
                    KRProgressHUD.dismiss()
                })
            case CameraAssetType.video:
                PHPhotoLibrary.shared().performChanges({
                    PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: asset.url!)
                }, completionHandler: { (success, _) in
                    if success {
                        DispatchQueue.main.async {
                            self.delegate?.cameraPreviewDidDownload()
                        }
                    }
                    KRProgressHUD.dismiss()
                })
            }
        }
    }
    func cameraPreviewOverlayDidSelectDownload() {
        CameraPermissionManager.requestPermission(type: .gallery) { granted in
            if granted {
                self.downloadAsset()
            } else {
                self.delegate?.cameraPreviewReceivedError("gallery_access_required".localized)
            }
        }
    }
}

extension CameraPreviewView: CameraStickerPickerViewDelegate {
    
    func cameraStickerDidSearch(text: String) {
        self.delegate?.cameraPreviewDidReceiveStickerSearch(text: text)
    }
    
    func cameraStickerPickerDidSelectSticker(sticker: Sticker, image: UIImage?) {
        stickersContainerView.addSticker(sticker: sticker, image: image)
        self.delegate?.cameraPreviewDidSelectSticker(sticker: sticker)
    }

    func cameraStickerPickerDidClose() {
        cameraPreviewOverlayView.fadeIn()
    }
    
    func cameraStickerPickerDidSelectMood() {
        delegate?.cameraPreviewDidSelectMood()
    }
}

extension CameraPreviewView: CameraDrawableContainerViewDelegate {
    func cameraDrawableContainerDidClose() {
        cameraPreviewOverlayView.fadeIn()
        stickersContainerView.isUserInteractionEnabled = true
        drawableContainerView.isEnabled = false
    }
}

extension CameraPreviewView: CameraPhotoCroppingViewDelegate {
    func cameraPhotoCroppingDidCropPhoto(_ photo: UIImage?) {
        if let photo = photo, let previewView = previewView as? CameraPreviewPhotoView, let cameraPhotoCroppingView = cameraPhotoCroppingView {
            previewView.updatePhoto(photo)
            cameraPhotoCroppingView.removeFromSuperview()
        }
    }
}
