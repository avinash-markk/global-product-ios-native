//
//  CameraCaptureOverlayView.swift
//  MarkkCamera
//
//  Created by Sebastian Sztemberg on 07/02/2019.
//

import UIKit

protocol CameraCaptureOverlayViewDelegate: AnyObject {
    func cameraCaptureOverlayDidClose()
    func cameraCaptureOverlayDidSelectGallery()
}

class CameraCaptureOverlayView: UIView {
    public weak var delegate: CameraCaptureOverlayViewDelegate?
    public var cameraManager: CameraManager?
    public var cameraRotateMode: CameraRotateMode = .back
    public lazy var cameraButton = CameraRecordButton()

    fileprivate lazy var flashButton: CameraFlashButton = {
        let button = CameraFlashButton(type: .custom)
        button.cameraManager = cameraManager
        button.loadType()
        button.showsTouchWhenHighlighted = true
        button.addTarget(self, action: #selector(handleFlash), for: .touchUpInside)
        return button
    }()

    fileprivate lazy var galleryButton: UIButton = {
        let button = UIButton(type: .custom)
        button.showsTouchWhenHighlighted = true
        button.setImage(UIImage.bundledImage(named: "iconGallery24WhitePureShadow"), for: .normal)
        button.addTarget(self, action: #selector(handleGallery), for: .touchUpInside)
        button.layer.borderWidth = 2.0
        button.layer.borderColor = UIColor.white.cgColor
        button.layer.cornerRadius = 4.0
        button.clipsToBounds = true
        return button
    }()

    fileprivate lazy var closeButton: UIButton = {
        let button = UIButton(type: .custom)
        button.showsTouchWhenHighlighted = true
        button.setImage(UIImage.bundledImage(named: "iconClose24WhitePureShadow"), for: .normal)
        button.addTarget(self, action: #selector(handleClose), for: .touchUpInside)
        return button
    }()
    
    fileprivate lazy var cameraRotateButton: CameraRotateButton = {
        let button = CameraRotateButton(type: .custom)
        button.showsTouchWhenHighlighted = true
        button.setImage(UIImage.bundledImage(named: "iconCameraReverse32WhitePureShadow"), for: .normal)
        button.addTarget(self, action: #selector(handleCameraRotateAction), for: .touchUpInside)
        return button
    }()
    
    init(cameraManager: CameraManager?, frame: CGRect) {
        super.init(frame: frame)
        self.cameraManager = cameraManager
        
        initUI()
    }

    func initUI() {
        addSubview(cameraButton)
        cameraButton.snp.makeConstraints { make in
            make.size.equalTo(80)
            make.centerX.equalTo(self)
            make.centerY.equalTo(safeArea.bottom).offset(-50)
        }

        addSubview(closeButton)
        closeButton.snp.makeConstraints { make in
            make.size.equalTo(30)
            make.top.equalTo(self).offset(20)
            make.right.equalTo(self).offset(-20)
        }
        addSubview(flashButton)
        flashButton.snp.makeConstraints { make in
            make.size.equalTo(24)
            make.top.equalTo(self).offset(25)
            make.left.equalTo(self).offset(20)
        }
        addSubview(galleryButton)
        galleryButton.snp.makeConstraints { make in
            make.size.equalTo(30)
            make.centerY.equalTo(cameraButton)
            make.left.equalTo(self).offset(30)
        }
        
        addSubview(cameraRotateButton)
        cameraRotateButton.snp.makeConstraints { make in
            make.size.equalTo(32)
            make.centerY.equalTo(cameraButton)
            make.right.equalTo(self).offset(-25)
        }
        
    }

    public func showCloseAndFlashButton(_ show: Bool) {
        if show {
            closeButton.fadeIn()
            flashButton.fadeIn()
        } else {
            closeButton.fadeOut()
            flashButton.fadeOut()
        }
    }

    // MARK: - Handle buttons

    @objc fileprivate func handleFlash(sender: UIButton) {
        flashButton.type.next()
    }

    @objc fileprivate func handleGallery(sender: UIButton) {
        delegate?.cameraCaptureOverlayDidSelectGallery()
    }

    @objc fileprivate func handleClose(sender: UIButton) {
        delegate?.cameraCaptureOverlayDidClose()
    }
    
    @objc fileprivate func handleCameraRotateAction(sender: UIButton) {
        cameraRotateMode.next()
       // cameraRotateButton.updateCamera(rotateMode: cameraRotateMode)
        cameraManager?.rotateCamera(mode: cameraRotateMode)
    }

    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        let result = super.hitTest(point, with: event)
        if result == self { return nil }
        return result
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
