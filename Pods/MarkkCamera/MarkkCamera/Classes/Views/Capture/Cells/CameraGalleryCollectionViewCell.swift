//
//  CameraGalleryCollectionViewCell.swift
//  Sminq
//
//  Created by Sebastian Sztemberg on 30/01/2019.
//  Copyright © 2019 Suraj Sagare. All rights reserved.
//

import UIKit
import Photos

class CameraGalleryCollectionViewCell: UICollectionViewCell {
    override var isSelected: Bool {
        didSet {
            layer.borderWidth = isSelected ? 3 : 0
        }
    }

    fileprivate lazy var thumbImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.layer.masksToBounds = true
        return imageView
    }()

    fileprivate lazy var durationView: UIView = {
        let view = UIView()
        view.layer.cornerRadius = 8
        view.backgroundColor = UIColor.white
        return view
    }()
    fileprivate lazy var durationLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "Inter-Bold", size: 11.0)
        label.textColor = UIColor.black
        return label
    }()

    public var asset: PHAsset? {
        didSet {
            if let asset = asset {
                PHCachingImageManager.default().requestImage(for: asset, targetSize: self.frame.size, contentMode: PHImageContentMode.aspectFill, options: nil) { (image, _) in
                    self.thumbImageView.image = image
                }

                durationView.isHidden = asset.duration == 0 || asset.mediaType != .video
                durationLabel.text = String(duration: asset.duration)
            }
        }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        layer.cornerRadius = 8
        layer.masksToBounds = true
        layer.borderColor = UIColor(hexString: "FECB03").cgColor
        addSubview(thumbImageView)
        thumbImageView.snp.makeConstraints { make in
            make.edges.equalTo(self)
        }

        addSubview(durationView)
        durationView.snp.makeConstraints { make in
            make.top.right.equalTo(self).inset(10)
        }

        durationView.addSubview(durationLabel)
        durationLabel.snp.makeConstraints { make in
            make.edges.equalTo(durationView).inset(6)
        }
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
