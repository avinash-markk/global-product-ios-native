//
//  CameraCaptureView.swift
//  Sminq
//
//  Created by Sebastian Sztemberg on 23/01/2019.
//  Copyright © 2019 Suraj Sagare. All rights reserved.
//

import UIKit
import AVFoundation
import Photos

protocol CameraCaptureViewDelegate: AnyObject {
    func cameraCaptureDidClose()
    func cameraCaptureGalleryAssetSelected(videoCount: Int?, imageCount: Int?, totalCount: Int?)
    
}

class CameraCaptureView: UIView {
    public weak var delegate: CameraCaptureViewDelegate?
    public var cameraManager: CameraManager?
    
    fileprivate var galleryView: CameraGalleryView!

    fileprivate var captureOverlayView: CameraCaptureOverlayView!
    fileprivate lazy var captureProgressView = CameraCaptureProgressView(frame: CGRect.zero)
    
    fileprivate lazy var blurredView: UIVisualEffectView = {
        let blurredView = UIVisualEffectView(effect: UIBlurEffect(style: .light))
        blurredView.isHidden = true
        let gesture: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleBlur))
        blurredView.addGestureRecognizer(gesture)
        return blurredView
    }()

    fileprivate lazy var pinchGesture: UIPinchGestureRecognizer = {
        let gesture = UIPinchGestureRecognizer(target: self, action: #selector(handlePinch))
        gesture.delegate = self
        return gesture
    }()

    fileprivate lazy var swipeGestureUp: UISwipeGestureRecognizer = {
        let gesture = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipe))
        gesture.direction = .up
        gesture.delegate = self
        return gesture
    }()

    fileprivate lazy var swipeGestureDown: UISwipeGestureRecognizer = {
        let gesture = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipe))
        gesture.direction = .down
        gesture.delegate = self
        return gesture
    }()
    
    init(cameraManager: CameraManager?, frame: CGRect) {
        super.init(frame: frame)
        self.cameraManager = cameraManager
        
        captureOverlayView = CameraCaptureOverlayView(cameraManager: cameraManager, frame: CGRect.zero)
        
        galleryView = CameraGalleryView(cameraManager: cameraManager, frame: CGRect.zero)
        
        initSession()
        initUI()
    }

    // MARK: - Init

    fileprivate func initUI() {
        addGestureRecognizer(pinchGesture)
        addGestureRecognizer(swipeGestureUp)
        addGestureRecognizer(swipeGestureDown)

        captureOverlayView.delegate = self
        captureOverlayView.cameraButton.delegate = self
        addSubview(captureOverlayView)
        captureOverlayView.snp.makeConstraints { make in
            make.center.equalTo(self)
            make.width.equalTo(self)
            make.height.equalTo(snp.width).multipliedBy(CameraUtils.cameraRatio)
        }

        addSubview(captureProgressView)
        captureProgressView.fadeOut()
        captureProgressView.snp.makeConstraints { make in
            make.top.equalTo(captureOverlayView.snp.top).offset(6)
            make.left.right.equalTo(self).inset(10)
        }

        addSubview(blurredView)
        blurredView.snp.makeConstraints { make in
            make.edges.equalTo(self)
        }

        addSubview(galleryView)
        galleryView.delegate = self
        galleryView.snp.makeConstraints { make in
            make.height.equalTo(240)
            make.left.right.equalTo(self)
            make.bottom.equalTo(240)
        }
    }
    
    fileprivate func initSession() {
        if let cameraManager = cameraManager {
            cameraManager.initSession()
            layer.addSublayer(cameraManager.videoCaptureLayer!)
            cameraManager.startSession()
        }
    }

    public func showRecordingTimer() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            if let cameraManager = self.cameraManager, cameraManager.isRecording() {
                self.captureProgressView.fadeIn()
            }
        }
        captureProgressView.startTimer()
        captureOverlayView.showCloseAndFlashButton(false)
    }

    public func hideRecordingTimer() {
        captureProgressView.fadeOut()
        captureProgressView.stopTimer()
        captureOverlayView.showCloseAndFlashButton(true)
    }

    fileprivate func blurredViewHidden(_ hidden: Bool) {
        UIView.transition(with: self, duration: 0.5, options: .transitionCrossDissolve, animations: {
            self.blurredView.isHidden = hidden
        })
    }

    fileprivate func galleryViewHidden(_ hidden: Bool) {
        UIView.transition(with: self, duration: 0.5, options: .curveEaseInOut, animations: {
            self.galleryView.snp.updateConstraints({ make in
                make.bottom.equalTo(hidden ? 240 : 0)
            })
            self.layoutIfNeeded()
        })
    }

    @objc fileprivate func handlePinch(_ sender: UIPinchGestureRecognizer) {
        let velocity = sender.velocity
        cameraManager?.updateCameraZoom(distance: velocity)
    }

    @objc fileprivate func handleSwipe(_ sender: UISwipeGestureRecognizer) {
        if let cameraManager = cameraManager, cameraManager.recordingStarted { return }
        if sender.direction == .up {
            galleryViewHidden(false)
            blurredViewHidden(false)
        } else if sender.direction == .down {
            galleryViewHidden(true)
            blurredViewHidden(true)
        }
    }

    // MARK: - Handle buttons

    @objc fileprivate func handleBlur() {
        galleryViewHidden(true)
        blurredViewHidden(true)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        if let cameraManager = cameraManager {
            cameraManager.videoCaptureLayer?.frame = bounds
        }
    }
}

extension CameraCaptureView: UIGestureRecognizerDelegate {
    override func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}

extension CameraCaptureView: CameraRecordButtonDelegate {
    func cameraButtonTouchStarted() {
        cameraManager?.startRecording()
    }

    func cameraButtonTouchEnded() {
        cameraManager?.stopRecording()
    }

    func cameraButtonHandleDrag(offset: CGFloat) {
        cameraManager?.updateVideoZoom(distance: offset)
    }
}

extension CameraCaptureView: CameraCaptureOverlayViewDelegate {
    func cameraCaptureOverlayDidClose() {
        delegate?.cameraCaptureDidClose()
    }

    func cameraCaptureOverlayDidSelectGallery() {
        galleryViewHidden(false)
        blurredViewHidden(false)
        galleryView.loadPhotos()
    }
}

extension CameraCaptureView: CameraGalleryViewDelegate {
    func cameraGalleryDidSelectAssets(_ assets: [PHAsset]) {
        self.getGalleryAssetDetailsForAnalyticsEvents(assets)
        cameraManager?.selectGalleryAssets(assets)
        galleryViewHidden(true)
        blurredViewHidden(true)
    }
    
    func getGalleryAssetDetailsForAnalyticsEvents(_ assets: [PHAsset]) {
        var imageCount = 0
        var videoCount = 0
        for asset in assets {
            if asset.mediaType == PHAssetMediaType.video {
                videoCount = videoCount + 1
            } else if asset.mediaType == PHAssetMediaType.image {
                imageCount = imageCount + 1
            }
        }
        self.delegate?.cameraCaptureGalleryAssetSelected(videoCount: videoCount, imageCount: imageCount, totalCount: assets.count)
    }
    
}
