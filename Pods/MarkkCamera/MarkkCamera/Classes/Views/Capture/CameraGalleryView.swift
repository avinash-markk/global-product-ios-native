//
//  CameraGalleryView.swift
//  Sminq
//
//  Created by Sebastian Sztemberg on 29/01/2019.
//  Copyright © 2019 Suraj Sagare. All rights reserved.
//

import UIKit
import Photos

protocol CameraGalleryViewDelegate: AnyObject {
    func cameraGalleryDidSelectAssets(_ assets: [PHAsset])
}

class CameraGalleryView: UIView {
    public weak var delegate: CameraGalleryViewDelegate?
    fileprivate var assets = [PHAsset]()
    fileprivate var selectedAssets = [PHAsset]()
    
    public var cameraManager: CameraManager?

    fileprivate lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 8
        layout.scrollDirection = .horizontal

        let collectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        collectionView.backgroundColor = UIColor.clear
        collectionView.delegate = self
        collectionView.allowsMultipleSelection = true
        collectionView.contentInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        collectionView.dataSource = self
        collectionView.register(CameraGalleryCollectionViewCell.self, forCellWithReuseIdentifier: "GalleryCell")
        return collectionView
    }()

    fileprivate lazy var headerLabel: UILabel = {
        let galleryInterval = cameraManager?.galleryInterval ?? 0
        let label = UILabel()
        label.text = "LAST \(galleryInterval) HOURS"
        label.font = UIFont(name: "Inter-Regular", size: 12.0)
        label.textColor = UIColor(white: 1, alpha: 0.8)
        return label
    }()

    fileprivate lazy var selectButton: UIButton = {
        let button = UIButton(type: .custom)
        button.setTitle("select".localized, for: .normal)
        button.addTarget(self, action: #selector(handleSelectPress), for: .touchUpInside)
        button.setTitleColor(UIColor.white, for: .normal)
        button.titleLabel?.font = UIFont(name: "Inter-Bold", size: 14.0)
        return button
    }()

    fileprivate lazy var separatorView: UIView = {
        let view = UILabel()
        view.backgroundColor = UIColor(white: 1, alpha: 0.4)
        return view
    }()

    fileprivate lazy var infoLabel: UILabel = {
        let label = UILabel()
        label.text = ""
        label.numberOfLines = 0
        label.font = UIFont(name: "Inter-Regular", size: 16.0)
        label.textAlignment = .center
        label.textColor = UIColor(white: 1, alpha: 0.8)
        label.isHidden = true
        return label
    }()

    fileprivate lazy var permissionButton: UIButton = {
        let button = UIButton(type: .custom)
        button.contentEdgeInsets = UIEdgeInsets(top: 4, left: 4, bottom: 4, right: 4)

        button.setTitle("give_permission".localized, for: .normal)
        button.setTitleColor(UIColor(white: 1, alpha: 0.8), for: .normal)
        button.titleLabel?.font = UIFont(name: "Inter-Regular", size: 14.0)
        button.addTarget(self, action: #selector(handlePermission), for: .touchUpInside)
        button.isHidden = true
        button.layer.borderColor = UIColor(white: 1, alpha: 0.8).cgColor
        button.layer.borderWidth = 1
        button.layer.cornerRadius = 4
        return button
    }()

    init(cameraManager: CameraManager?, frame: CGRect) {
        super.init(frame: frame)
        self.cameraManager = cameraManager
        
        initUI()
    }

    fileprivate func initUI() {
        backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.8)

        addSubview(headerLabel)
        headerLabel.snp.makeConstraints { make in
            make.top.left.equalTo(self).offset(10)
        }

        addSubview(selectButton)
        selectButton.isHidden = true
        selectButton.snp.makeConstraints { make in
            make.centerY.equalTo(headerLabel)
            make.right.equalTo(self).inset(10)
        }

        addSubview(separatorView)
        separatorView.snp.makeConstraints { make in
            make.left.right.equalTo(self)
            make.top.equalTo(headerLabel.snp.bottom).offset(10)
            make.height.equalTo(1)
        }

        addSubview(collectionView)
        collectionView.snp.makeConstraints { make in
            make.left.right.equalTo(self)
            make.top.equalTo(separatorView.snp.bottom).offset(10)
            make.bottom.equalTo(safeArea.bottom)
        }

        addSubview(infoLabel)
        infoLabel.snp.makeConstraints { make in
            make.centerX.equalTo(self)
            make.centerY.equalTo(self).offset(-20)
            make.left.right.equalTo(self).inset(10)
        }

        addSubview(permissionButton)
        permissionButton.snp.makeConstraints { make in
            make.top.equalTo(infoLabel.snp.bottom).offset(8)
            make.centerX.equalTo(infoLabel)
        }

        loadPhotos()
    }

    public func loadPhotos() {
        if !CameraPermissionManager.checkPermission(type: .gallery) {
            infoLabel.isHidden = false
            infoLabel.text = "gallery_access".localized
            permissionButton.isHidden = false
            return
        }
        infoLabel.isHidden = true
        let hourValue = cameraManager?.galleryInterval ?? 0
        let yesterday = Calendar.current.date(byAdding: .hour, value: -hourValue, to: Date())
        let options = PHFetchOptions()
        options.sortDescriptors = [NSSortDescriptor.init(key: "creationDate", ascending: false)]
        if let yesterday = yesterday {
            options.predicate = NSPredicate(format: "creationDate > %@ AND (duration >= %f OR mediaType = %d)", yesterday as NSDate, CameraConfig.minVideoDuration, PHAssetMediaType.image.rawValue)
        }
        self.assets = []
        let fetchedAssets = PHAsset.fetchAssets(with: options)
        fetchedAssets.enumerateObjects({ (object, _, _) in
            self.assets.append(object)
        })
        if assets.count == 0 {
            infoLabel.isHidden = false
            infoLabel.text = "You haven't taken any photos or videos in the last \(hourValue) hours."
        }
        selectedAssets = []
        collectionView.reloadData()
    }

    // MARK: - Handle buttons
    
    @objc fileprivate func handlePermission(sender: UIButton) {
        CameraPermissionManager.requestPermission(type: .gallery) { granted in
            if granted {
                self.infoLabel.isHidden = true
                self.permissionButton.isHidden = true
                self.loadPhotos()
            }
        }
    }
    
    @objc fileprivate func handleSelectPress(sender: UIButton) {
        delegate?.cameraGalleryDidSelectAssets(selectedAssets)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension CameraGalleryView: UICollectionViewDelegateFlowLayout, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let height = collectionView.frame.size.height - 20

        return CGSize(width: height * 0.6, height: height)
    }
}
extension CameraGalleryView: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return assets.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GalleryCell", for: indexPath) as! CameraGalleryCollectionViewCell
        cell.asset = assets[indexPath.row]
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectButton.isHidden = false
        selectedAssets.append(assets[indexPath.row])
    }

    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        selectButton.isHidden = collectionView.indexPathsForSelectedItems?.count == 0
        selectedAssets.removeAll{$0 == assets[indexPath.row]}
    }
}
