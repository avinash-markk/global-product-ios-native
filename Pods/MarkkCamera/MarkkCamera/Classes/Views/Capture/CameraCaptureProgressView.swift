//
//  CameraCaptureProgressView.swift
//  MarkkCamera
//
//  Created by Sebastian Sztemberg on 27/02/2019.
//

import UIKit

class CameraCaptureProgressView: UIView {
    fileprivate var timer: Timer?
    fileprivate var animator: UIViewPropertyAnimator?

    fileprivate lazy var progressView: UIProgressView = {
        let progressView = UIProgressView()
        progressView.alpha = 0.6
        return progressView
    }()

    fileprivate lazy var limitView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(hexString: "D8403D")
        return view
    }()

    fileprivate var tooltipView = TooltipView()

    override init(frame: CGRect) {
        super.init(frame: frame)

        initUI()
    }

    fileprivate func initUI() {
        addSubview(progressView)
        progressView.snp.makeConstraints { make in
            make.top.equalTo(self).offset(8)
            make.left.right.equalTo(self)
            make.height.equalTo(2)
        }

        let dummyView = UIView()
        addSubview(dummyView)
        dummyView.snp.makeConstraints { make in
            make.left.equalTo(self)
            make.width.equalTo(progressView).multipliedBy(0.33)
        }

        addSubview(limitView)
        limitView.snp.makeConstraints { make in
            make.centerY.equalTo(progressView)
            make.height.equalTo(6)
            make.width.equalTo(2)
            make.left.equalTo(dummyView.snp.right)
        }

        addSubview(tooltipView)
        tooltipView.text = "MIN. 5 SECONDS"
        tooltipView.snp.makeConstraints { make in
            make.top.equalTo(progressView.snp.bottom).offset(10)
            make.left.equalTo(progressView)
            make.width.equalTo(progressView).multipliedBy(CameraConfig.minVideoDuration / CameraConfig.maxVideoDuration)
            make.bottom.equalTo(self)
        }
    }

    public func startTimer() {
        timer?.invalidate()
        progressView.progressTintColor = UIColor(hexString: "D8403D")
        progressView.progress = 0
        progressView.layoutIfNeeded()
        tooltipView.fadeIn()
        limitView.fadeIn()
        print("Started2")
        animator = UIViewPropertyAnimator(duration: CameraConfig.maxVideoDuration, curve: .linear) {
            self.progressView.setProgress(1.0, animated: true)
        }
        animator?.startAnimation()

        timer = Timer.scheduledTimer(withTimeInterval: CameraConfig.minVideoDuration, repeats: false) { _ in
            self.tooltipView.fadeOut()
            self.limitView.fadeOut()
            self.progressView.progressTintColor = UIColor(hexString: "3DC881")
        }
    }

    public func stopTimer() {
        animator?.stopAnimation(true)
        timer?.invalidate()
        timer = nil
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
