//
//  CameraDrawableContainerView.swift
//  MarkkCamera
//
//  Created by Sebastian Sztemberg on 13/02/2019.
//

import UIKit
import SwiftyDraw

protocol CameraDrawableContainerViewDelegate: AnyObject {
    func cameraDrawableContainerDidClose()
}

class CameraDrawableContainerView: UIView {
    public weak var delegate: CameraDrawableContainerViewDelegate?

    public var isEnabled: Bool = false {
        didSet {
            isUserInteractionEnabled = isEnabled
            drawableView.isEnabled = isEnabled
            if isEnabled {
                drawableOverlay.fadeIn()
            } else {
                drawableOverlay.fadeOut()
            }
        }
    }

    fileprivate var drawableView = SwiftyDrawView(frame: CGRect.zero)
    fileprivate var drawableOverlay = CameraDrawableOverlayView(frame: CGRect.zero)

    override init(frame: CGRect) {
        super.init(frame: frame)

        initUI()
    }

    fileprivate func initUI() {
        addSubview(drawableView)
        drawableView.brush.width = 8
        drawableView.isEnabled = false
        drawableView.snp.makeConstraints { make in
            make.edges.equalTo(self)
        }

        addSubview(drawableOverlay)
        drawableOverlay.isHidden = true
        drawableOverlay.delegate = self
        drawableOverlay.snp.makeConstraints { make in
            make.edges.equalTo(self)
        }
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}

extension CameraDrawableContainerView: CameraDrawableOverlayViewDelegate {
    func cameraDrawableOverlayDidSelectBrushSize(_ size: Int) {
        drawableView.brush.width = CGFloat(size)
    }

    func cameraDrawableOverlayDidSelectColor(_ color: UIColor) {
        drawableView.brush.color = color
    }

    func cameraDrawableOverlayDidSelectUndo() {
        drawableView.undo()
    }

    func cameraDrawableOverlayDidClose() {
        delegate?.cameraDrawableContainerDidClose()
    }
}
