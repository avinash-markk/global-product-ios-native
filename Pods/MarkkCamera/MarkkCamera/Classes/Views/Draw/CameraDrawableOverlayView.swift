//
//  CameraDrawableOverlayView.swift
//  MarkkCamera
//
//  Created by Sebastian Sztemberg on 14/02/2019.
//

import UIKit
protocol CameraDrawableOverlayViewDelegate: AnyObject {
    func cameraDrawableOverlayDidSelectBrushSize(_ size: Int)
    func cameraDrawableOverlayDidSelectColor(_ color: UIColor)
    func cameraDrawableOverlayDidSelectUndo()
    func cameraDrawableOverlayDidClose()
}

class CameraDrawableOverlayView: UIView {
    public weak var delegate: CameraDrawableOverlayViewDelegate?

    fileprivate let brushSizes: [Int] = [4, 8, 12, 16]

    fileprivate let colorPickerView = CameraColorPickerView()

    fileprivate lazy var doneButton: UIButton = {
        let button = UIButton(type: .custom)
        button.showsTouchWhenHighlighted = true
        button.setTitle("done".localized, for: .normal)
        button.addTarget(self, action: #selector(handleClosePress), for: .touchUpInside)
        button.setTitleColor(UIColor.white, for: .normal)
        button.titleLabel?.font = UIFont(name: "Inter-Bold", size: 14.0)
        return button
    }()

    fileprivate lazy var undoButton: UIButton = {
        let button = UIButton(type: .custom)
        button.showsTouchWhenHighlighted = true
        button.setImage(UIImage.bundledImage(named: "iconUndo"), for: .normal)
        button.addTarget(self, action: #selector(handleUndoPress), for: .touchUpInside)
        return button
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)

        initUI()
    }

    fileprivate func initUI() {
        var lastBrush: UIButton?

        addSubview(doneButton)
        addSubview(undoButton)

        doneButton.snp.makeConstraints { make in
            make.top.right.equalTo(self).inset(20)
        }
        undoButton.snp.makeConstraints { make in
            make.top.left.equalTo(self).inset(20)
        }

        for (index, _) in brushSizes.enumerated() {
            let brushButton = UIButton(type: .custom)
            brushButton.tag = index
            brushButton.showsTouchWhenHighlighted = true
            brushButton.setImage(UIImage.bundledImage(named: "iconPencil"), for: .normal)
            brushButton.addTarget(self, action: #selector(handleBrushPress), for: .touchUpInside)
            let inset = CGFloat(2 * (brushSizes.count - (index + 1)))
            brushButton.contentEdgeInsets = UIEdgeInsets(top: inset, left: inset, bottom: inset, right: inset)
            addSubview(brushButton)
            brushButton.snp.makeConstraints({ make in
                make.size.equalTo(25)
                make.left.equalTo(lastBrush != nil ? lastBrush!.snp.right : undoButton.snp.right).offset(10)
                make.centerY.equalTo(undoButton)
            })
            lastBrush = brushButton
        }

        colorPickerView.delegate = self
        addSubview(colorPickerView)
        colorPickerView.snp.makeConstraints { make in
            make.left.right.equalTo(self)
            make.bottom.equalTo(self)
        }
    }

    @objc fileprivate func handleClosePress(sender: UIButton) {
        delegate?.cameraDrawableOverlayDidClose()
    }

    @objc fileprivate func handleUndoPress(sender: UIButton) {
        delegate?.cameraDrawableOverlayDidSelectUndo()
    }

    @objc fileprivate func handleBrushPress(sender: UIButton) {
        delegate?.cameraDrawableOverlayDidSelectBrushSize(brushSizes[sender.tag])
    }

    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        let result = super.hitTest(point, with: event)
        if result == self { return nil }
        return result
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}

extension CameraDrawableOverlayView: CameraColorPickerViewDelegate {
    func cameraColorPickerDidSelectColor(_ color: UIColor) {
        delegate?.cameraDrawableOverlayDidSelectColor(color)
    }
}
