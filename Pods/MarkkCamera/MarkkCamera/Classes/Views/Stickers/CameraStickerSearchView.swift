//
//  CameraStickerSearchView
//  MarkkCamera
//
//  Created by Sebastian Sztemberg on 05/04/2019.
//

import UIKit

class CameraStickerSearchView: UIView {
    
    public let searchTextField = DesignableUITextField()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = UIColor.white
        searchTextField.backgroundColor = CameraUtils.CameraColors.blackDark100.withAlphaComponent(0.05)
        searchTextField.clearButtonMode = .whileEditing
        searchTextField.font = UIFont(name: "Inter-Regular", size: 16.0)
        searchTextField.leftImage  = UIImage.bundledImage(named: "iconSearch24BlackDark50")
        searchTextField.leftPadding = 8
        searchTextField.textColor = CameraUtils.CameraColors.blackDark100.withAlphaComponent(0.75)
        searchTextField.placeholder = "Search for a sticker..."
        
        addSubview(searchTextField)
        
        let bottomLineView = UIView()
        bottomLineView.backgroundColor = UIColor(red: 144.0/255.0, green: 126.0/255.0, blue: 127.0/255.0, alpha: 0.25)
        addSubview(bottomLineView)
        
        searchTextField.snp.makeConstraints { make in
            make.top.equalTo(self)
            make.left.right.equalTo(self)
            make.height.equalTo(40)
        }
        bottomLineView.snp.makeConstraints { make in
            make.left.right.bottom.equalTo(self)
            make.height.equalTo(0.5)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
