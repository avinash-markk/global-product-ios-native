//
//  CameraStickerCollectionViewCell.swift
//  MarkkCamera
//
//  Created by Sebastian Sztemberg on 06/02/2019.
//

import UIKit

class CameraStickerCollectionViewCell: UICollectionViewCell {
    public lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)

        addSubview(imageView)

        imageView.snp.makeConstraints { make in
            make.edges.equalTo(self)
        }
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}
