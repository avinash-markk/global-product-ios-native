//
//  CameraStickerHeaderCollectionReusableView.swift
//  MarkkCamera
//
//  Created by Sebastian Sztemberg on 06/05/2019.
//

import UIKit

class CameraStickerHeaderCollectionReusableView: UICollectionReusableView {
    public lazy var label: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "Inter-Bold", size: 12.0)
        label.numberOfLines = 1
        label.textColor = CameraUtils.CameraColors.blackDark100.withAlphaComponent(0.5)
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(label)
        
        label.snp.makeConstraints { make in
            make.edges.equalTo(self)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}
