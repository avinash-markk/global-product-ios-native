//
//  CameraTextOverlayView.swift
//  MarkkCamera
//
//  Created by Sebastian Sztemberg on 11/02/2019.
//

import UIKit

protocol CameraTextOverlayViewDelegate: AnyObject {
    func cameraTextOverlayDidSelectAlignMode(_ alignMode: CameraTextAlignMode)
    func cameraTextOverlayDidSelectBackgroundColor(_ enabled: Bool)
    func cameraTextOverlayDidSelectColor(_ color: UIColor)
    func cameraTextOverlayDidClose()
}

class CameraTextOverlayView: UIView {
    public weak var delegate: CameraTextOverlayViewDelegate?

    fileprivate var currentAlignMode: CameraTextAlignMode = .left
    
    fileprivate let colorPickerView = CameraColorPickerView()

    fileprivate lazy var alignButton: CameraTextAlignButton = {
        let button = CameraTextAlignButton(type: .custom)
        button.showsTouchWhenHighlighted = true
        button.updateTextAlign(alignMode: currentAlignMode)
        button.addTarget(self, action: #selector(handleAlignPress), for: .touchUpInside)
        return button
    }()

    fileprivate lazy var backgroundButton: UIButton = {
        let button = UIButton(type: .custom)
        button.showsTouchWhenHighlighted = true
        button.setImage(UIImage.bundledImage(named: "iconText"), for: .normal)
        button.addTarget(self, action: #selector(handleTextBackgroundPress), for: .touchUpInside)
        return button
    }()

    fileprivate lazy var doneButton: UIButton = {
        let button = UIButton(type: .custom)
        button.setTitle("done".localized, for: .normal)
        button.addTarget(self, action: #selector(handleClosePress), for: .touchUpInside)
        button.setTitleColor(UIColor.white, for: .normal)
        button.titleLabel?.font = UIFont(name: "Inter-Bold", size: 14.0)
        return button
    }()

    fileprivate lazy var backgroundView: UIView = {
        let view = UIView(frame: CGRect.zero)
        view.backgroundColor = CameraUtils.CameraColors.blackDark100.withAlphaComponent(0.75)
        let gesture: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleClosePress))
        view.addGestureRecognizer(gesture)
        return view
    }()
    
    override init(frame: CGRect) {
        super.init(frame: CGRect.zero)
        initUI()
    }

    fileprivate func initUI() {
        addSubview(backgroundView)
        addSubview(doneButton)

        backgroundView.snp.makeConstraints { make in
            make.edges.equalTo(self)
        }
        doneButton.snp.makeConstraints { make in
            make.top.right.equalTo(self).inset(20)
        }

        addSubview(alignButton)
        alignButton.snp.makeConstraints { make in
            make.size.equalTo(30)
            make.top.left.equalTo(self).inset(20)
        }
        
        addSubview(backgroundButton)
        backgroundButton.snp.makeConstraints { make in
            make.size.equalTo(30)
            make.centerY.equalTo(alignButton)
            make.left.equalTo(alignButton.snp.right).offset(10)
        }

        colorPickerView.delegate = self
        addSubview(colorPickerView)
        colorPickerView.snp.makeConstraints { make in
            make.left.right.equalTo(self)
            make.bottom.equalTo(self)
        }
    }

    public func updateAlign(_ align: CameraTextAlignMode) {
        currentAlignMode = align
        alignButton.updateTextAlign(alignMode: align)
    }

    public func updateKeyboardOffset(_ offset: CGFloat) {
        UIView.animate(withDuration: 0.2) {
            self.colorPickerView.snp.updateConstraints { make in
                make.bottom.equalTo(self).offset(-offset)
            }
            self.layoutIfNeeded()
        }
    }

    public func updateTextBackground(enabled: Bool) {
        backgroundButton.isSelected = enabled
        backgroundButton.setImage(UIImage.bundledImage(named: enabled ? "iconTextWithBackground" : "iconText"), for: .normal)
    }

    // MARK: - Handle buttons

    @objc fileprivate func handleAlignPress(sender: UIButton) {
        currentAlignMode.next()
        alignButton.updateTextAlign(alignMode: currentAlignMode)
        delegate?.cameraTextOverlayDidSelectAlignMode(currentAlignMode)
    }

    @objc fileprivate func handleTextBackgroundPress(sender: UIButton) {
        delegate?.cameraTextOverlayDidSelectBackgroundColor(!backgroundButton.isSelected)
        updateTextBackground(enabled: !backgroundButton.isSelected)
    }

    @objc fileprivate func handleClosePress(sender: UIButton) {
       delegate?.cameraTextOverlayDidClose()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}

extension CameraTextOverlayView: CameraColorPickerViewDelegate {
    func cameraColorPickerDidSelectColor(_ color: UIColor) {
        delegate?.cameraTextOverlayDidSelectColor(color)
    }
}
