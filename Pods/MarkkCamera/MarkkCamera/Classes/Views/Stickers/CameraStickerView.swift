//
//  CameraStickerView.swift
//  MarkkCamera
//
//  Created by Sebastian Sztemberg on 04/02/2019.
//

import UIKit

class CameraStickerView: CameraDraggableView {
    fileprivate lazy var imageView: UIImageView = {
        let imageView = UIImageView(frame: CGRect.zero)
        return imageView
    }()

    public var sticker: Sticker?

    init(image: UIImage?, sticker: Sticker) {
        super.init(frame: CGRect.zero)
        self.sticker = sticker
        imageView.image = image
    }

    override func initUI() {
        super.initUI()
        addSubview(imageView)
        imageView.snp.makeConstraints { make in
            make.edges.equalTo(self)
        }
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}
