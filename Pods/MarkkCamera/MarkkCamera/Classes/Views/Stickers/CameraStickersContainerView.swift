//
//  CameraStickersContainerView.swift
//  MarkkCamera
//
//  Created by Sebastian Sztemberg on 04/02/2019.
//

import UIKit

protocol CameraStickersContainerViewDelegate: AnyObject {
    func cameraStickersContainerDidBeginInteraction()
    func cameraStickersContainerDidEndInteraction()
    func fadeOutCameraPreviewView()
    func cameraTextStickerAdded(text: String)
}

extension CameraStickersContainerViewDelegate {
    
    func fadeOutCameraPreviewView() {
    }
    
}

class CameraStickersContainerView: UIView {
    public weak var delegate: CameraStickersContainerViewDelegate?

    fileprivate var lastScale: CGFloat = 1
    fileprivate var nearestStickerView: CameraDraggableView?

    public var stickers = [CameraDraggableView]()
    fileprivate var activeSticker: CameraDraggableView?

    fileprivate var panActive = false
    fileprivate var pinchActive = false
    fileprivate var rotationActive = false

    fileprivate var cameraTextOverlay = CameraTextOverlayView(frame: CGRect.zero)

    fileprivate lazy var deleteImage: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage.bundledImage(named: "iconDelete")
        return imageView
    }()

    fileprivate lazy var tapGesture: UITapGestureRecognizer = {
        let gesture = UITapGestureRecognizer(target: self, action: #selector(handleTap))
        gesture.delegate = self
        return gesture
    }()

    fileprivate lazy var pinchGesture: UIPinchGestureRecognizer = {
        let gesture = UIPinchGestureRecognizer(target: self, action: #selector(handlePinch))
        gesture.delegate = self
        return gesture
    }()

    fileprivate lazy var rotationGesture: UIRotationGestureRecognizer = {
        let gesture = UIRotationGestureRecognizer(target: self, action: #selector(handleRotation))
        gesture.delegate = self
        return gesture
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)

        initUI()
    }

    fileprivate func initUI() {
        addGestureRecognizer(tapGesture)
        addGestureRecognizer(pinchGesture)
        addGestureRecognizer(rotationGesture)

        isUserInteractionEnabled = true
        layer.masksToBounds = true
        addSubview(deleteImage)
        deleteImage.isHidden = true
        deleteImage.snp.makeConstraints { make in
            make.size.equalTo(24)
            make.centerX.equalTo(self)
            make.bottom.equalTo(safeArea.bottom).inset(20)
        }
        cameraTextOverlay.delegate = self
        addSubview(cameraTextOverlay)
        cameraTextOverlay.snp.makeConstraints { make in
            make.top.left.right.bottom.equalTo(self)
        }
        cameraTextOverlay.alpha = 0
        cameraTextOverlay.isHidden = true
    }

    public func addSticker(sticker: Sticker, image: UIImage?) {
        let sticker = CameraStickerView(image: image, sticker: sticker)
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(handlePan))
        sticker.addGestureRecognizer(panGesture)
        sticker.delegate = self
        sticker.contentMode = .scaleAspectFit
        sticker.frame = CGRect(x: 0, y: 0, width: 150, height: 150)
        sticker.center = center
        addSubview(sticker)
        bringSubviewToFront(deleteImage)
        stickers.append(sticker)
    }

    public func addText() {
        let textView = CameraTextView()
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(handlePan))
        textView.addGestureRecognizer(panGesture)
        textView.delegate = self
        textView.contentMode = .scaleAspectFit
        textView.frame = CGRect(x: 0, y: 0, width: frame.width, height: 100)
        textView.center = center
        addSubview(textView)
        bringSubviewToFront(deleteImage)
        stickers.append(textView)
        textView.focus()
    }

    fileprivate func showDelete(_ show: Bool) {
        if show {
            deleteImage.fadeIn()
        } else {
            deleteImage.fadeOut()
        }
    }

    fileprivate func expandDelete(_ expand: Bool) {
        let generator = UIImpactFeedbackGenerator(style: .heavy)
        generator.impactOccurred()

        UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseInOut, animations: {
            self.deleteImage.snp.updateConstraints({ make in
                make.size.equalTo(expand ? 60 : 40)
                self.layoutIfNeeded()
            })
        }, completion: nil)
    }

    fileprivate func checkDeleteCanvas(sticker: CameraDraggableView, point: CGPoint) {
        let deleteArea = deleteImage.frame.insetBy(dx: -50, dy: -50)
        if deleteArea.contains(point) && activeSticker == nil {
            activeSticker = sticker
            expandDelete(true)
        } else if !deleteArea.contains(point) && activeSticker != nil {
            activeSticker = nil
            expandDelete(false)
        }
    }

    public func updateKeyboardOffset(offset: CGFloat) {
        cameraTextOverlay.updateKeyboardOffset(offset)

        if let cameraTextView = activeSticker as? CameraTextView {
            cameraTextView.keyboardOffset = offset
        }
    }

    fileprivate func nearestSticker(_ point: CGPoint) -> CameraDraggableView? {
        let nearestStickers = stickers.sorted {
            return point.distance(from: $0.frame) < point.distance(from: $1.frame)
        }

        if let lastSticker = nearestStickers.first, point.distance(from: lastSticker.frame) < 100 {
            return lastSticker
        }
        return nil
    }

    // Gestures

    @objc fileprivate func handleTap(_ sender: UITapGestureRecognizer) {
        let location = sender.location(in: self)

        if activeSticker == nil && nearestSticker(location) == nil {
            addText()
        }
    }

    @objc fileprivate func handlePan(_ sender: UIPanGestureRecognizer) {
        guard let view = sender.view as? CameraDraggableView, !view.isEditing, nearestStickerView == nil || nearestStickerView == view else { return }

        let translation = sender.translation(in: self)
        let centerLocation = sender.location(in: self)
        let p = CGPoint(x: view.center.x + translation.x, y: view.center.y + translation.y)
        view.center = p
        checkDeleteCanvas(sticker: view, point: centerLocation)
        sender.setTranslation(CGPoint.zero, in: self)

        if !panActive && !rotationActive && !pinchActive {
            panActive = true
            showDelete(true)
            delegate?.cameraStickersContainerDidBeginInteraction()
        }

        if sender.state == .began && nearestStickerView == nil {
            nearestStickerView = view
        } else if sender.state == .cancelled || sender.state == .ended || sender.state == .failed {
            panActive = false
            if let removeSticker = activeSticker {
                removeSticker.removeFromSuperview()
                if let index = stickers.index(of: removeSticker) {
                    stickers.remove(at: index)
                    expandDelete(false)
                }
            }
            activeSticker = nil
            nearestStickerView = nil
            showDelete(false)
            delegate?.cameraStickersContainerDidEndInteraction()
        }
    }

    @objc fileprivate func handlePinch(_ sender: UIPinchGestureRecognizer) {
        if let cameraTextView = activeSticker as? CameraTextView, cameraTextView.isEditing {
            cameraTextView.handlePinch(velocity: sender.velocity)
            return
        }

        let scale = sender.scale

        if sender.state == .began && nearestStickerView == nil {
            pinchActive = true
            let location = sender.location(in: self)
            if let sticker = nearestSticker(location) {
                nearestStickerView = sticker
            }
            delegate?.cameraStickersContainerDidBeginInteraction()
        } else if sender.state == .cancelled || sender.state == .ended || sender.state == .failed {
            pinchActive = false
            nearestStickerView = nil
            lastScale = 1
            delegate?.cameraStickersContainerDidEndInteraction()
        }

        guard let nearestStickerView = nearestStickerView else { return }

        let currentScale = nearestStickerView.layer.value(forKeyPath: "transform.scale")! as! CGFloat
        let kMinScale: CGFloat = 0.3
        var newScale = 1 - (lastScale - scale)
        newScale = max(newScale, kMinScale / currentScale)
        let transforms = nearestStickerView.transform.scaledBy(x: newScale, y: newScale)
        nearestStickerView.transform = transforms
        lastScale = scale
    }

    @objc fileprivate func handleRotation(_ sender: UIRotationGestureRecognizer) {
        if let cameraTextView = activeSticker as? CameraTextView, cameraTextView.isEditing {
            return
        }

        if sender.state == .began && nearestStickerView == nil {
            rotationActive = true
            let location = sender.location(in: self)
            if let sticker = nearestSticker(location) {
                nearestStickerView = sticker
            }
            delegate?.cameraStickersContainerDidBeginInteraction()
        } else if sender.state == .cancelled || sender.state == .ended || sender.state == .failed {
            rotationActive = false
            nearestStickerView = nil
            delegate?.cameraStickersContainerDidEndInteraction()
        }

        guard let nearestStickerView = nearestStickerView else { return }

        nearestStickerView.transform = nearestStickerView.transform.rotated(by: sender.rotation)
        sender.rotation = 0
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}

extension CameraStickersContainerView: CameraDraggableViewDelegate {
    func cameraDraggableViewDidBeginEditing(_ cameraTextView: CameraTextView) {
        activeSticker = cameraTextView
        cameraTextOverlay.fadeIn()
        cameraTextOverlay.updateAlign(cameraTextView.currentAlignMode)
        bringSubviewToFront(cameraTextOverlay)
        bringSubviewToFront(cameraTextView)
        delegate?.fadeOutCameraPreviewView()
    }

    func cameraDraggableViewDidEndEditing(_ cameraTextView: CameraTextView) {
        activeSticker = nil
        cameraTextOverlay.fadeOut()
        delegate?.cameraStickersContainerDidEndInteraction()
    }
}

extension CameraStickersContainerView: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }

    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if let activeSticker = activeSticker, activeSticker.isEditing, gestureRecognizer == tapGesture {
            return false
        }
        return true
    }
}

extension CameraStickersContainerView: CameraTextOverlayViewDelegate {
    func cameraTextOverlayDidSelectBackgroundColor(_ enabled: Bool) {
        guard let cameraTextView = activeSticker as? CameraTextView else { return }
        cameraTextView.backgroundEnabled = enabled
    }

    func cameraTextOverlayDidSelectColor(_ color: UIColor) {
        guard let cameraTextView = activeSticker as? CameraTextView else { return }
        cameraTextView.color = color
    }

    func cameraTextOverlayDidSelectAlignMode(_ alignMode: CameraTextAlignMode) {
        guard let cameraTextView = activeSticker as? CameraTextView else { return }
        cameraTextView.currentAlignMode = alignMode
    }

    func cameraTextOverlayDidClose() {
        guard let cameraTextView = activeSticker as? CameraTextView else {return}
        self.delegate?.cameraTextStickerAdded(text: cameraTextView.textView.text!)
        cameraTextView.close()
    }
    
}
