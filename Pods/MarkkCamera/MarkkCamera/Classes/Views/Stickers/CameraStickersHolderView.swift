//
//  CameraStickersHolderView.swift
//  MarkkCamera
//
//  Created by Sebastian Sztemberg on 14/02/2019.
//

import UIKit

class CameraStickersHolderView: UIView {
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        let result = super.hitTest(point, with: event)
        if result == self { return nil }
        return result
    }
}
