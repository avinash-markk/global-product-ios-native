//
//  CameraDraggableView.swift
//  MarkkCamera
//
//  Created by Sebastian Sztemberg on 11/02/2019.
//

import UIKit

protocol CameraDraggableViewDelegate: AnyObject {
    func cameraDraggableViewDidBeginEditing(_ cameraTextView: CameraTextView)
    func cameraDraggableViewDidEndEditing(_ cameraTextView: CameraTextView)
}

class CameraDraggableView: UIView {
    public weak var delegate: CameraDraggableViewDelegate?

    public var isEditing: Bool = false

    override init(frame: CGRect) {
        super.init(frame: frame)
        initUI()
    }

    public func initUI() {
        isUserInteractionEnabled = true
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}
