//
//  CameraStickerPickerView.swift
//  MarkkCamera
//
//  Created by Sebastian Sztemberg on 06/02/2019.
//

import UIKit
import Kingfisher

protocol CameraStickerPickerViewDelegate: AnyObject {
    func cameraStickerPickerDidSelectSticker(sticker: Sticker, image: UIImage?)
    func cameraStickerPickerDidClose()
    func cameraStickerPickerDidSelectMood()
    func cameraStickerDidSearch(text: String)
}

class CameraStickerPickerView: UIView {
    public weak var delegate: CameraStickerPickerViewDelegate?
    var searchText: String = "" {
        didSet {
            collectionView.reloadData()
        }
    }
    public var cameraManager: CameraManager?
    
    public var stickers = [Sticker]() {
        didSet {
            collectionView.reloadData()
        }
    }
    
    public var searchableStickers: [Sticker] {
        if searchText.count > 2 {
            return stickers.filter { searchText.count == 0 || $0.searchTags.lowercased().contains(searchText.lowercased()) }
        } else {
            return stickers
        }
    }
    
    fileprivate lazy var backgroundView: UIView = {
        let view = UIView()
        view.backgroundColor = CameraUtils.CameraColors.blackDark100.withAlphaComponent(0.75)
        let gesture: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleClose))
        view.addGestureRecognizer(gesture)
        return view
    }()

    fileprivate lazy var triangleView: TriangleView = {
        let view = TriangleView(color: UIColor.white)
        return view
    }()

    fileprivate lazy var doneButton: UIButton = {
        let button = UIButton(type: .custom)
        button.alpha = 0
        button.setTitle("done".localized, for: .normal)
        button.addTarget(self, action: #selector(handleClose), for: .touchUpInside)
        button.setTitleColor(UIColor.white, for: .normal)
        button.titleLabel?.font = UIFont(name: "Inter-Bold", size: 14.0)
        return button
    }()
    
    fileprivate lazy var moodButton: UIButton = {
        let button = UIButton(type: .custom)
        button.imageView?.contentMode = .scaleAspectFit
        button.addTarget(self, action: #selector(handleMood), for: .touchUpInside)
        button.layer.borderColor = UIColor.white.cgColor
        button.layer.borderWidth = 2.0
        button.clipsToBounds = true
        button.layer.cornerRadius = 30.0
        return button
    }()

    fileprivate lazy var pickerView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        return view
    }()
    
    fileprivate lazy var searchView: CameraStickerSearchView = {
        let view = CameraStickerSearchView(frame: CGRect.zero)
        view.searchTextField.delegate = self
        view.searchTextField.addTarget(self, action: #selector(handleSearch), for: .editingChanged)
        return view
    }()

    fileprivate lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 8

        let collectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        collectionView.backgroundColor = UIColor.clear
        collectionView.delegate = self
        collectionView.contentInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        collectionView.dataSource = self
        collectionView.register(CameraStickerCollectionViewCell.self, forCellWithReuseIdentifier: "StickerCell")
        collectionView.register(CameraStickerHeaderCollectionReusableView.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "StickerHeader")
        return collectionView
    }()
    
    fileprivate lazy var overlayView: UIView = {
        let view = UIView.init(frame: self.frame)
        view.backgroundColor = CameraUtils.CameraColors.blackDark100.withAlphaComponent(0.75)
        return view
    }()

    init(cameraManager: CameraManager?, frame: CGRect) {
        super.init(frame: frame)
        self.cameraManager = cameraManager

        initUI()
    }

    fileprivate func initUI() {
        addSubview(backgroundView)
        addSubview(triangleView)
        addSubview(pickerView)
        pickerView.addSubview(searchView)
        pickerView.addSubview(collectionView)
        addSubview(doneButton)
        addSubview(moodButton)
        

        backgroundView.snp.makeConstraints { make in
            make.edges.equalTo(self)
        }

        doneButton.snp.makeConstraints { make in
            make.top.equalTo(safeArea.top).offset(CameraUtils.hasSafeArea ? 48 : 20)
            make.right.equalTo(self).inset(20)
        }

        triangleView.snp.makeConstraints { make in
            make.height.equalTo(6)
            make.width.equalTo(12)
            make.centerX.equalTo(self)
            make.bottom.equalTo(pickerView.snp.top)
        }
        pickerView.snp.makeConstraints { make in
            make.left.right.equalTo(self)
            make.height.equalTo(self.snp.height).dividedBy(2)
            make.bottom.equalTo(self).multipliedBy(2)
        }
        searchView.snp.makeConstraints { make in
            make.top.left.right.equalTo(pickerView).inset(16)
            make.bottom.equalTo(collectionView.snp.top)
            make.height.equalTo(62)
        }
        collectionView.snp.makeConstraints { make in
            make.left.bottom.right.equalTo(pickerView).inset(16)
        }
        moodButton.snp.makeConstraints { make in
            make.height.width.equalTo(60)
            make.bottom.equalTo(triangleView.snp.top).offset(-2)
            make.centerX.equalTo(triangleView)
        }
        
    }
    
    public func updateKeyboardFrame(keyboardHeight: CGFloat) {
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseInOut, animations: {
            self.pickerView.snp.remakeConstraints { make in
                if keyboardHeight > 0 {
                    make.left.right.equalTo(self)
                    make.bottom.equalTo(keyboardHeight)
                    make.top.equalTo(self.safeArea.top).offset(CameraUtils.hasSafeArea ? 80 : 70)
                } else {
                    make.left.right.equalTo(self)
                    make.height.equalTo(self.snp.height).dividedBy(2)
                    make.bottom.equalTo(self)
                }
            }
            self.layoutIfNeeded()
        })
    }
    
    public func show(_ show: Bool, mood: String?) {
        if let mood = mood {
            let moodInt = Int(mood)!
            moodButton.setImage(UIImage.bundledImage(named: moodInt.getRatingsImageMeta().image64), for: .normal)
        }
        
        if show {
            isHidden = false
        } else {
            searchView.searchTextField.endEditing(true)
            delegate?.cameraStickerPickerDidClose()
        }
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseInOut, animations: {
            
            self.pickerView.snp.remakeConstraints { make in
                make.left.right.equalTo(self)
                make.height.equalTo(self.snp.height).dividedBy(2)
                if show {
                    make.bottom.equalTo(self.snp.bottom).multipliedBy(1)
                } else {
                    make.bottom.equalTo(self.snp.bottom).multipliedBy(2)
                }
            }
            
            self.doneButton.alpha = show ? 1 : 0
            self.layoutIfNeeded()
        }, completion: { (_) in
            self.isHidden = !show
        })
    }
    
    public func show(_ show: Bool) {
        self.show(show, mood: nil)
    }
    
    @objc fileprivate func handleClose(sender: UIButton) {
        if searchView.searchTextField.isEditing {
            searchView.searchTextField.endEditing(true)
        } else {
            show(false)
        }
    }
    
    @objc fileprivate func handleSearch(sender: UITextField) {
        guard let text = sender.text else {
            return
        }
        if text.count > 2 {
            self.delegate?.cameraStickerDidSearch(text: text)
        }
        searchText = text
    }
    
    @objc fileprivate func handleMood(sender: UIButton) {
        show(false)
        delegate?.cameraStickerPickerDidSelectMood()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        searchView.searchTextField.roundCorners(corners: [.topLeft, .topRight, .bottomRight, .bottomLeft], radius: 8)
        pickerView.roundCorners(corners: [.topLeft, .topRight], radius: 10)
    }
}

extension CameraStickerPickerView: UICollectionViewDelegateFlowLayout, UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: searchableStickers.count == 0 && searchText.count > 0 ? 30 : 0 )
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = (collectionView.frame.width - 60) / 3
        
        return CGSize(width: size, height: size)
    }
}

extension CameraStickerPickerView: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return searchableStickers.count
    }
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let reusableView = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "StickerHeader", for: indexPath) as! CameraStickerHeaderCollectionReusableView
        
        reusableView.label.text = String(format: "no_matching_results".localized, searchText.uppercased())
        return reusableView
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "StickerCell", for: indexPath) as! CameraStickerCollectionViewCell
        cell.imageView.kf.indicatorType = .activity
        cell.imageView.kf.setImage(with: searchableStickers[indexPath.row].image)
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        show(false)
        KingfisherManager.shared.retrieveImage(with: searchableStickers[indexPath.row].image) { result in
            switch result {
            case .success(let value):
                self.delegate?.cameraStickerPickerDidSelectSticker(sticker: self.searchableStickers[indexPath.row], image: value.image)
            case .failure:
                break
            }
        }
    }
}

extension CameraStickerPickerView: UITextFieldDelegate {
    // MARK: Text field delegate
    
    public func textFieldDidBeginEditing(_ textField: UITextField) {
        
    }
    
    public func textFieldShouldClear(_ textField: UITextField) -> Bool {
        searchText = ""
        return true
    }
    
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
