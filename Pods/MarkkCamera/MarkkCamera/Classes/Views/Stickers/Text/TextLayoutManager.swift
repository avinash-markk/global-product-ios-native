//
//  TextLayoutManager.swift
//  MarkkCamera
//
//  Created by Sebastian Sztemberg on 11/02/2019.
//

import UIKit

class TextLayoutManager: NSLayoutManager {
    fileprivate var lastDrawPoint = CGPoint(x: 0, y: 0)
    fileprivate var textAlignment: NSTextAlignment = .left

    override func drawBackground(forGlyphRange glyphsToShow: NSRange, at origin: CGPoint) {
        lastDrawPoint = origin
        super.drawBackground(forGlyphRange: glyphsToShow, at: origin)
        lastDrawPoint = CGPoint.zero
    }
    // swiftlint:disable:next function_body_length
    override func fillBackgroundRectArray(_ rectArray: UnsafePointer<CGRect>, count rectCount: Int, forCharacterRange charRange: NSRange, color: UIColor) {
        let cornerRadius: CGFloat = 10

        guard let ctx = UIGraphicsGetCurrentContext() else {
            super.fillBackgroundRectArray(rectArray, count: rectCount, forCharacterRange: charRange, color: color)
            return
        }

        ctx.saveGState()

        ctx.clear(UIScreen.main.bounds)
        ctx.setAllowsAntialiasing(true)
        ctx.setShouldAntialias(true)

        let gRange: NSRange = glyphRange(forCharacterRange: charRange, actualCharacterRange: nil)
        let textOffset: CGPoint = lastDrawPoint

        var lineRange = NSRange(location: gRange.location, length: 1)

        while NSMaxRange(lineRange) <= NSMaxRange(gRange) {
            var lineBounds: CGRect = lineFragmentUsedRect(forGlyphAt: lineRange.location, effectiveRange: &lineRange)
            lineBounds.origin.x += textOffset.x
            lineBounds.origin.y += textOffset.y

            var glyphRangeInLine: NSRange = NSIntersectionRange(gRange, lineRange)
            let truncatedGRange: NSRange = truncatedGlyphRange(inLineFragmentForGlyphAt: glyphRangeInLine.location)
            if truncatedGRange.location != NSNotFound {
                let sameRange: NSRange = NSIntersectionRange(glyphRangeInLine, truncatedGRange)
                if sameRange.length > 0 && NSMaxRange(sameRange) == NSMaxRange(glyphRangeInLine) {
                    glyphRangeInLine = NSRange(location: glyphRangeInLine.location, length: sameRange.location - glyphRangeInLine.location)
                }
            }

            if glyphRangeInLine.length > 0 {
                var startDrawY: CGFloat = CGFloat.greatestFiniteMagnitude
                var maxLineHeight: CGFloat = 0.0
                for glyphIndex in glyphRangeInLine.location..<NSMaxRange(glyphRangeInLine) {
                    let charIndex: Int = characterIndexForGlyph(at: glyphIndex)
                    let font = textStorage?.attribute(.font, at: charIndex, effectiveRange: nil) as? UIFont

                    let gLocation: CGPoint = location(forGlyphAt: glyphIndex)
                    startDrawY = CGFloat(fmin(Float(startDrawY), Float(lineBounds.origin.y + gLocation.y - (font?.ascender ?? 0.0))))
                    maxLineHeight = CGFloat(fmax(Float(maxLineHeight), Float(font?.lineHeight ?? 0.0)))
                }

                var size: CGSize = lineBounds.size
                var orgin: CGPoint = lineBounds.origin
                orgin.y = startDrawY
                size.height = maxLineHeight

                lineBounds.size = size
                lineBounds.origin = orgin
            }

            let path = CGMutablePath()

            var validRect: CGRect = lineBounds
            validRect.origin.y -= 10
            validRect.origin.x -= 10
            validRect.size.height += 20
            validRect.size.width += 20

            path.move(to: CGPoint(x: validRect.minX + cornerRadius * 2, y: validRect.minY), transform: .identity)
            path.addArc(tangent1End: CGPoint(x: validRect.maxX, y: validRect.minY), tangent2End: CGPoint(x: validRect.maxX, y: validRect.minY + cornerRadius * 2), radius: cornerRadius, transform: .identity)
            path.addArc(tangent1End: CGPoint(x: validRect.maxX, y: validRect.maxY), tangent2End: CGPoint(x: validRect.maxX - cornerRadius * 2, y: validRect.maxY), radius: cornerRadius, transform: .identity)
            path.addArc(tangent1End: CGPoint(x: validRect.minX, y: validRect.maxY), tangent2End: CGPoint(x: validRect.minX, y: validRect.maxY - cornerRadius * 2), radius: cornerRadius, transform: .identity)
            path.addArc(tangent1End: CGPoint(x: validRect.minX, y: validRect.minY), tangent2End: CGPoint(x: validRect.minX + cornerRadius * 2, y: validRect.minY), radius: cornerRadius, transform: .identity)
            ctx.addPath(path)
            ctx.setLineCap(CGLineCap(rawValue: 1)!)
            color.setFill()
            ctx.setLineWidth(0)
            ctx.drawPath(using: .fill)

            lineRange = NSRange(location: NSMaxRange(lineRange), length: 1)
        }
        ctx.restoreGState()
    }
}
