//
//  CameraTextView.swift
//  MarkkCamera
//
//  Created by Sebastian Sztemberg on 08/02/2019.
//

import UIKit

class CameraTextView: CameraDraggableView {
    fileprivate var lastTransform: CGAffineTransform?
    fileprivate var lastCenter: CGPoint?

    fileprivate var fontSize: CGFloat = 28.0

    public var keyboardOffset: CGFloat = 300 {
        didSet {
            adjustBounds()
        }
    }

    public var color: UIColor = .white {
        didSet {
            updateTextAttributes()
        }
    }

    public var currentAlignMode: CameraTextAlignMode = .left {
        didSet {
            updateTextAttributes()
        }
    }

    public var backgroundEnabled: Bool = false {
        didSet {
            updateTextAttributes()
        }
    }

    public lazy var textView: UITextView = {
        let textLayout = TextLayoutManager()
        let textStorage = NSTextStorage()
        textStorage.addLayoutManager(textLayout)

        let textContainer = NSTextContainer(size: CGSize.zero)
        textLayout.addTextContainer(textContainer)

        let longPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress))
        let textView = UITextView(frame: CGRect.zero, textContainer: textContainer)
        textView.backgroundColor = UIColor.clear
        textView.bounces = false
        textView.textContainerInset = UIEdgeInsets(top: 10, left: 10, bottom: 0, right: 10)
        textView.isScrollEnabled = false
        textView.delegate = self
        textView.addGestureRecognizer(longPressGesture)
        return textView
    }()

    override func initUI() {
        super.initUI()

        tag = Int(NSDate().timeIntervalSince1970)

        addSubview(textView)
        textView.snp.makeConstraints { make in
            make.edges.equalTo(self)
        }
        updateTextAttributes()
    }

    fileprivate func updateTextAttributes() {
        var textAlign: NSTextAlignment
        switch currentAlignMode {
        case .left:
            textAlign = NSTextAlignment.left
        case .center:
            textAlign = NSTextAlignment.center
        case .right:
            textAlign = NSTextAlignment.right
        }

        var textColor = color

        if backgroundEnabled {
            textColor = color.isDarkColor ? UIColor.white : UIColor.black
        }

        let titleParagraphStyle = NSMutableParagraphStyle()
        titleParagraphStyle.alignment = textAlign
        textView.textStorage.setAttributes([NSAttributedString.Key.foregroundColor: textColor, NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: fontSize), NSAttributedString.Key.paragraphStyle: titleParagraphStyle, NSAttributedString.Key.backgroundColor: backgroundEnabled ? color : UIColor.clear ], range: NSRange(location: 0, length: textView.text.count))
    }

    fileprivate func adjustBounds() {
        let maxHeight = superview!.frame.height - keyboardOffset
        let size = textView.sizeThatFits(CGSize.init(width: frame.width, height: CGFloat(MAXFLOAT)))
        bounds = CGRect.init(x: 0, y: 0, width: frame.width, height: size.height > maxHeight ? maxHeight : size.height + 20)
        textView.scrollRangeToVisible(NSRange.init(location: textView.text.count, length: 1))
    }

    fileprivate func fixAttribues() {
        center = CGPoint.init(x: superview!.frame.width / 2, y: (superview!.frame.height - keyboardOffset) / 2)
        adjustBounds()
        updateTextAttributes()
    }

    public func focus() {
        textView.becomeFirstResponder()
    }

    public func close() {
        textView.resignFirstResponder()
    }

    public func handlePinch(velocity: CGFloat) {
        let pinchFactor: CGFloat = 1.0

        let maxFontSize: CGFloat = 60.0
        let desiredZoomFactor = fontSize + atan2(velocity, pinchFactor)
        fontSize = max(5.0, min(desiredZoomFactor, maxFontSize))
        fixAttribues()
    }

    @objc fileprivate func handleLongPress(sender: UILongPressGestureRecognizer) {
        let point = sender.location(in: textView)
        if let position = textView.closestPosition(to: point) {
            textView.selectedTextRange = textView.textRange(from: position, to: position)
        }
    }
}

extension CameraTextView: UITextViewDelegate {
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        delegate?.cameraDraggableViewDidBeginEditing(self)

        lastTransform = transform
        lastCenter = center
        isEditing = true

        transform = CGAffineTransform.identity

        textView.sizeToFit()
        frame.size.width = superview!.frame.width
        center = CGPoint.init(x: superview!.frame.width / 2, y: (superview!.frame.height - keyboardOffset) / 2)
        return true
    }

    func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
        delegate?.cameraDraggableViewDidEndEditing(self)
        isEditing = false

        textView.sizeToFit()
        resizeToFitSubviews()
        frame.size = CGSize(width: frame.width + 30, height: frame.height + 20)
        textView.frame.size = CGSize(width: textView.frame.width + 30, height: textView.frame.height + 20)
        UIView.animate(withDuration: 0.2) {
            if let lastTransform = self.lastTransform, let lastCenter = self.lastCenter {
                self.transform = lastTransform
                self.center = lastCenter
            }
        }
        return true
    }

//    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
//        if ((textView.bounds.origin.y + textView.bounds.size.height) > superview!.frame.height - 300) {
//            textView.resignFirstResponder()
//            return false
//        }
//        return true
//    }
    func textViewDidChange(_ textView: UITextView) {
        fixAttribues()
    }
}
