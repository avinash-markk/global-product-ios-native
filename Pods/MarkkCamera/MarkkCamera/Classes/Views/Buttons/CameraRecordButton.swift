//
//  CameraRecordButton.swift
//  Sminq
//
//  Created by Sebastian Sztemberg on 28/01/2019.
//  Copyright © 2019 Suraj Sagare. All rights reserved.
//

import UIKit

protocol CameraRecordButtonDelegate: AnyObject {
    func cameraButtonTouchStarted()
    func cameraButtonTouchEnded()
    func cameraButtonHandleDrag(offset: CGFloat)
}

class CameraRecordButton: UIControl {
    public weak var delegate: CameraRecordButtonDelegate?

    fileprivate var centerPoint: CGPoint {
        return CGPoint(x: frame.width / 2.0, y: frame.width / 2.0)
    }

    fileprivate var firstTouchOffset: CGFloat?

    fileprivate lazy var blurredView: UIVisualEffectView = {
        let blurredView = UIVisualEffectView(effect: UIBlurEffect(style: .light))
        blurredView.center = centerPoint
        blurredView.layer.cornerRadius = 35
        blurredView.clipsToBounds = true
        blurredView.isUserInteractionEnabled = false
        return blurredView
    }()

    fileprivate lazy var insideView: UIView = {
        let insideView = UIView()
        insideView.center = centerPoint
        insideView.layer.cornerRadius = 25
        insideView.clipsToBounds = true
        insideView.backgroundColor = UIColor.white
        insideView.isUserInteractionEnabled = false
        return insideView
    }()

    // MARK: - Init

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.clear

        addSubview(blurredView)
        blurredView.snp.makeConstraints { make in
            make.size.equalTo(70)
            make.center.equalTo(self)
        }
        addSubview(insideView)
        insideView.snp.makeConstraints { make in
            make.size.equalTo(50)
            make.center.equalTo(self)
        }

        addTarget(self, action: #selector(handleTouchDown), for: .touchDown)
        addTarget(self, action: #selector(handleTouchUp), for: [.touchUpInside, .touchUpOutside, .touchCancel])
        self.addTarget(self, action: #selector(handleDrag), for: .touchDragInside)
        self.addTarget(self, action: #selector(handleDrag), for: .touchDragOutside)
    }

    fileprivate func startAnimating() {
        UIView.animate(withDuration: 0.3) {
            self.blurredView.layer.cornerRadius = 40
            self.blurredView.snp.updateConstraints { make in
                make.size.equalTo(80)
            }

            self.insideView.layer.cornerRadius = 20
            self.insideView.snp.updateConstraints { make in
                make.size.equalTo(40)
            }
            self.layoutIfNeeded()
        }
    }

    fileprivate func stopAnimating() {
        UIView.animate(withDuration: 0.3) {
            self.blurredView.layer.cornerRadius = 35
            self.blurredView.snp.updateConstraints { make in
                make.size.equalTo(70)
            }

            self.insideView.layer.cornerRadius = 25
            self.insideView.snp.updateConstraints { make in
                make.size.equalTo(50)
            }
            self.layoutIfNeeded()
        }
    }

    @objc fileprivate func handleDrag(sender: UIControl, event: UIEvent) {
        if let touch = event.allTouches?.first {
            let pointLocation = touch.location(in: self)
            if firstTouchOffset == nil {
                firstTouchOffset = pointLocation.y
            }
            delegate?.cameraButtonHandleDrag(offset: max(firstTouchOffset! - pointLocation.y, 0))
        }
    }

    @objc fileprivate func handleTouchDown(sender: UIControl) {
        firstTouchOffset = nil
        startAnimating()
        delegate?.cameraButtonTouchStarted()
    }

    @objc fileprivate func handleTouchUp(sender: UIControl) {
        stopAnimating()
        delegate?.cameraButtonTouchEnded()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
