//
//  CameraTextAlignButton.swift
//  MarkkCamera
//
//  Created by Sebastian Sztemberg on 11/02/2019.
//

import UIKit

enum CameraTextAlignMode: Int {
    case left, center, right

    mutating func next() {
        self = CameraTextAlignMode(rawValue: rawValue + 1) ?? .left
    }
}

class CameraTextAlignButton: UIButton {
    public func updateTextAlign(alignMode: CameraTextAlignMode) {
        var image: String
        switch alignMode {
        case .left:
            image = "iconAlignLeft"
        case .center:
            image = "iconAlignCenter"
        case .right:
            image = "iconAlignRight"
        }
        setImage(UIImage.bundledImage(named: image), for: .normal)
    }
}
