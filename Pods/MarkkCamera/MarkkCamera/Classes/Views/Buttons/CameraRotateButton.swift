//
//  CameraRotateButton.swift
//  MarkkCamera
//
//  Created by Avinash Thakur on 17/04/19.
//

import UIKit

public enum CameraRotateMode: Int {
    case back, front
    
    mutating func next() {
        self = CameraRotateMode(rawValue: rawValue + 1) ?? .back
    }
}

class CameraRotateButton: UIButton {
    
//    public func updateCamera(rotateMode: CameraRotateMode) {
//        var image: String
//        switch rotateMode {
//        case .front:
//            image = "iconAlignLeft"
//        case .back:
//            image = "iconAlignCenter"
//        }
//        setImage(UIImage.bundledImage(named: image), for: .normal)
//    }
    
}
