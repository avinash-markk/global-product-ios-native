//
//  CameraPermissionButton.swift
//  Sminq
//
//  Created by Sebastian Sztemberg on 30/01/2019.
//  Copyright © 2019 Suraj Sagare. All rights reserved.
//

import UIKit

class CameraPermissionButton: UIButton {
    public var activeText: String?
    public var inactiveText: String?
    
    public lazy var iconImageView: UIImageView = {
        let iconImageView = UIImageView.init(frame: CGRect(x: 14, y: 14, width: 20, height: 20))
        return iconImageView
    }()
    
    public var active: Bool = true {
        didSet {
            setTitle(active ? activeText : inactiveText, for: .normal)
            setTitleColor(active ? CameraUtils.CameraColors.green100 : UIColor(white: 1, alpha: 1.0), for: .normal)
            layer.borderColor = active ? CameraUtils.CameraColors.green100.cgColor : UIColor(white: 1, alpha: 0.6).cgColor
            iconImageView.image = active ? UIImage.bundledImage(named: "iconCheckCircleFilled24Green100")! : UIImage.bundledImage(named: "iconCheckCircleOutline24WhitePure")!
        }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        layer.borderWidth = 1
        titleLabel?.font = UIFont(name: "Inter-Bold", size: 12.0)
        layer.cornerRadius = 24
        self.addSubview(iconImageView)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
