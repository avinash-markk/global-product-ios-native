//
//  CameraFlashButton.swift
//  Sminq
//
//  Created by Sebastian Sztemberg on 28/01/2019.
//  Copyright © 2019 Suraj Sagare. All rights reserved.
//

import UIKit

class CameraFlashButton: UIButton {
    public var cameraManager: CameraManager?
    
    public var type: CameraFlashMode = .off {
        didSet {
            UserDefaults.standard.set(type.rawValue, forKey: "flashType")
            cameraManager?.currentFlashMode = type
            var image: String
            switch type {
            case .auto:
                image = "iconFlashAuto24WhitePureShadow"
            case .on:
                image = "iconFlashOn24WhitePureShadow"
            case .off:
                image = "iconFlashOff24WhitePureShadow"
            }
            setImage(UIImage.bundledImage(named: image), for: .normal)
        }
    }
    public func loadType() {
        type = CameraFlashMode(rawValue: UserDefaults.standard.integer(forKey: "flashType")) ?? .off
    }
}
