//
//  CroppingTimelineView.swift
//  MarkkCamera
//
//  Created by Sebastian Sztemberg on 20/02/2019.
//

import UIKit
import AVFoundation

protocol CroppingTimelineViewDelegate: AnyObject {
    func croppingTimeLineDidUpdateTime(startSeconds: Double, endSeconds: Double)
}

class CroppingTimelineView: UIView {
    public weak var delegate: CroppingTimelineViewDelegate?

    fileprivate var videoAsset: AVAsset?

    fileprivate var timelineView = UIScrollView()
    fileprivate var croppingAreaView = VideoCroppingAreaView()

    init(asset: AVAsset) {
        super.init(frame: CGRect.zero)
        videoAsset = asset

        initUI()
    }

    fileprivate func initUI() {
        backgroundColor = UIColor.black
        addSubview(timelineView)
        timelineView.snp.makeConstraints { make in
            make.top.left.equalTo(self)
            make.right.equalTo(self)
            make.height.equalTo(50)
        }

        addSubview(croppingAreaView)
        croppingAreaView.isHidden = true
        croppingAreaView.touchableAreaSize = 40
        croppingAreaView.delegate = self
        croppingAreaView.snp.makeConstraints { make in
            make.top.left.equalTo(self)
            make.right.equalTo(self)
            make.height.equalTo(50)
        }

        generateThumbnails()
    }

    fileprivate func updateVideoRange() {
        guard let asset = videoAsset else { return}

        let width = timelineView.contentSize.width
        let durationSeconds = CMTimeGetSeconds(asset.duration)
        croppingAreaView.durationLabel.text = String(duration: CameraConfig.maxVideoDuration)

        croppingAreaView.minSize = CGSize(width: width * CGFloat(CameraConfig.minVideoDuration / durationSeconds), height: 50)
        croppingAreaView.maxSize = CGSize(width: width * CGFloat(CameraConfig.maxVideoDuration / durationSeconds), height: 50)
        croppingAreaView.fadeIn()
    }

    fileprivate func generateThumbnails() {
        guard let asset = videoAsset else { return}

        let durationSeconds = CMTimeGetSeconds(asset.duration)
        let generator = AVAssetImageGenerator(asset: asset)

        generator.appliesPreferredTrackTransform = true

        let array = Array (0...Int(durationSeconds/3))

        let times = array.map { number -> NSValue in
            let time = CMTimeMakeWithSeconds(Double(number) * 3, preferredTimescale: 1000)
            return NSValue(time: time)
        }

        var lastThumb: UIImageView?

        var generatedImages = 0

        generator.generateCGImagesAsynchronously(forTimes: times) { (_, thumbnail, _, _, _) in
            if let thumbnail = thumbnail {
                DispatchQueue.main.sync {
                    generatedImages += 1
                    let thumbImageView = UIImageView(image: UIImage(cgImage: thumbnail))
                    thumbImageView.contentMode = .scaleAspectFill
                    thumbImageView.layer.masksToBounds = true
                    self.timelineView.addSubview(thumbImageView)
                    thumbImageView.snp.makeConstraints({ make in
                        make.top.equalTo(self.timelineView).offset(3)
                        make.bottom.equalTo(self.timelineView).offset(3)
                        make.width.equalTo(self.timelineView).dividedBy(7)
                        make.height.equalTo(44)
                        make.left.equalTo(lastThumb != nil ? lastThumb!.snp.right : self.timelineView).offset(lastThumb != nil ? 0 : 10)
                    })
                    lastThumb = thumbImageView

                    if generatedImages >= times.count {
                        thumbImageView.snp.makeConstraints { make in
                            make.right.equalTo(self.timelineView).offset(-10)
                        }
                        print("Done")
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                            self.updateVideoRange()
                        }
                    }
                }
            }
        }
    }

    override func layoutSubviews() {
        super.layoutSubviews()

//        if frame.width > 0 && croppingAreaView.maxSize == nil {
//            updateVideoRange()
//        }
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension CroppingTimelineView: VideoCroppingAreaViewDelegate {
    func croppingAreaViewDidChangeFrame(cropFrame: CGRect) {
        guard let asset = videoAsset else { return}

        let durationSeconds = CMTimeGetSeconds(asset.duration)

        let startSeconds = Double(cropFrame.minX / timelineView.contentSize.width) * durationSeconds
        let endSeconds = Double(cropFrame.maxX / timelineView.contentSize.width) * durationSeconds

        croppingAreaView.durationLabel.text = String(duration: Double(round(endSeconds - startSeconds)))
    }

    func croppingAreaViewDidEndChangingFrame(cropFrame: CGRect) {
        guard let asset = videoAsset else { return}

        let durationSeconds = CMTimeGetSeconds(asset.duration)

        let startSeconds = Double(timelineView.convert(CGPoint(x: cropFrame.minX, y: 0), from: croppingAreaView.superview).x / timelineView.contentSize.width) * durationSeconds
        let endSeconds = Double(timelineView.convert(CGPoint(x: cropFrame.maxX, y: 0), from: croppingAreaView.superview).x / timelineView.contentSize.width) * durationSeconds

        delegate?.croppingTimeLineDidUpdateTime(startSeconds: startSeconds, endSeconds: endSeconds)
    }

    func croppingAreaViewDidReachEdge(edge: VideoCroppingEdge) {
        var offset: CGPoint?
        if edge == .left {
            if timelineView.contentOffset.x - 10 > 0 {
                offset = CGPoint(x: timelineView.contentOffset.x - 10, y: 0)
            } else {
                offset = CGPoint(x: 0, y: 0)
            }
        } else {
            if timelineView.contentOffset.x + 10 < frame.maxX {
                offset = CGPoint(x: timelineView.contentOffset.x + 10, y: 0)
            } else {
                offset = CGPoint(x: frame.maxX, y: 0)
            }
        }

        if let offset = offset {
            timelineView.setContentOffset(offset, animated: false)
        }
    }
}
