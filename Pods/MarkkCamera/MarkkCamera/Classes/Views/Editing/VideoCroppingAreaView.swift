//
//  VideoCroppingAreaView.swift
//  MarkkCamera
//
//  Created by Sebastian Sztemberg on 25/03/2019.
//

import UIKit

enum VideoCroppingEdge {
    case left, right
}

enum VideoCroppingSnapPoint {
    case left, right, inside
}

protocol VideoCroppingAreaViewDelegate: AnyObject {
    func croppingAreaViewDidChangeFrame(cropFrame: CGRect)
    func croppingAreaViewDidEndChangingFrame(cropFrame: CGRect)
    func croppingAreaViewDidReachEdge(edge: VideoCroppingEdge)
}

class VideoCroppingAreaView: UIView {
    public weak var delegate: VideoCroppingAreaViewDelegate?

    public var touchableGap: CGFloat = 50
    public var touchableAreaSize: CGFloat = 80

    public var croppingView = UIView()

    fileprivate var selectedSnapPoint: VideoCroppingSnapPoint?
    fileprivate var lastPoint: CGPoint?

    fileprivate var timer: Timer?
    fileprivate var currentEdge: VideoCroppingEdge?

    public var minSize: CGSize = CGSize(width: 0, height: 0)
    public var maxSize: CGSize? {
        didSet {
            if let size = maxSize, let superview = superview {
                croppingView.snp.updateConstraints { make in
                    make.right.equalTo(self).offset(-(superview.frame.width - size.width))
                }
            }
        }
    }

    public lazy var durationLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "Inter-Bold", size: 10.0)
        label.textColor = UIColor.white
        label.textAlignment = .center
        return label
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)

        isUserInteractionEnabled = true

        initUI()
    }

    fileprivate func initUI() {
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(handlePan))
        addGestureRecognizer(panGesture)

        let lineColor = UIColor(hexString: "FECB03")

        let topLine = UIView()
        topLine.backgroundColor = lineColor
        let leftLine = UIView()
        leftLine.backgroundColor = lineColor
        let rightLine = UIView()
        rightLine.backgroundColor = lineColor
        let bottomLine = UIView()
        bottomLine.backgroundColor = lineColor

        let leftSnap = UIView()
        leftSnap.backgroundColor = lineColor
        let rightSnap = UIView()
        rightSnap.backgroundColor = lineColor

        let gridLineY1 = UIView()
        gridLineY1.backgroundColor = lineColor
        let gridLineY2 = UIView()
        gridLineY2.backgroundColor = lineColor

        let gridLineX1 = UIView()
        gridLineX1.backgroundColor = lineColor
        let gridLineX2 = UIView()
        gridLineX2.backgroundColor = lineColor

        croppingView.layer.cornerRadius = 8
        croppingView.layer.masksToBounds = true
        addSubview(croppingView)

        croppingView.addSubview(topLine)
        croppingView.addSubview(leftLine)
        croppingView.addSubview(rightLine)
        croppingView.addSubview(bottomLine)

        croppingView.addSubview(leftSnap)
        croppingView.addSubview(rightSnap)

        croppingView.addSubview(durationLabel)

        croppingView.snp.makeConstraints { make in
            make.top.left.right.bottom.equalTo(self)
        }

        leftSnap.snp.makeConstraints { make in
            make.top.left.bottom.equalTo(croppingView)
            make.width.equalTo(20)
        }

        rightSnap.snp.makeConstraints { make in
            make.top.right.bottom.equalTo(croppingView)
            make.width.equalTo(20)
        }

        topLine.snp.makeConstraints { make in
            make.top.left.right.equalTo(croppingView)
            make.height.equalTo(2)
        }

        leftLine.snp.makeConstraints { make in
            make.top.left.bottom.equalTo(croppingView)
            make.width.equalTo(2)
        }

        rightLine.snp.makeConstraints { make in
            make.top.right.bottom.equalTo(croppingView)
            make.width.equalTo(2)
        }

        bottomLine.snp.makeConstraints { make in
            make.left.right.bottom.equalTo(croppingView)
            make.height.equalTo(2)
        }

        durationLabel.snp.makeConstraints { make in
            make.center.equalTo(croppingView)
        }
    }

    public func reset() {
        croppingView.snp.updateConstraints { make in
            make.top.left.right.bottom.equalTo(self)
        }
    }

    fileprivate func setEdgeTimer() {
        if timer != nil { return }
        timer = Timer.scheduledTimer(withTimeInterval: 0.05, repeats: true, block: { _ in
            if let currentEdge = self.currentEdge {
                self.delegate?.croppingAreaViewDidReachEdge(edge: currentEdge)
            }
        })
    }

    fileprivate func stopEdgeTimer() {
        if timer != nil {
            timer?.invalidate()
            timer = nil
            currentEdge = nil
        }
    }

    @objc public func handlePan(_ sender: UIPanGestureRecognizer) {
        let fixedTouchableArea = min(touchableAreaSize, croppingView.frame.width / 4)
        let leftArea = CGRect(x: croppingView.frame.minX - touchableGap, y: 0, width: fixedTouchableArea + touchableGap, height: 50)
        let rightArea = CGRect(x: croppingView.frame.maxX - fixedTouchableArea, y: 0, width: fixedTouchableArea + touchableGap, height: 50)

        let leftEdge = CGRect(x: frame.minX, y: 0, width: 100, height: 50)
        let rightEdge = CGRect(x: frame.maxX - 100, y: 0, width: 100, height: 50)

        let touchPoint = sender.location(in: self)
        let translation = sender.translation(in: self)

        if !self.bounds.contains(touchPoint) { return }

        switch sender.state {
        case .began:
            lastPoint = nil
            if leftArea.contains(touchPoint) {
                selectedSnapPoint = .left
            } else if rightArea.contains(touchPoint) {
                selectedSnapPoint = .right
            } else if frame.contains(touchPoint) {
                selectedSnapPoint = .inside
            }
        case .changed:
            guard let selectedSnapPoint = selectedSnapPoint else { return }

            switch selectedSnapPoint {
            case .left:
                croppingView.snp.updateConstraints { make in
                    if (croppingView.frame.width > minSize.width || touchPoint.x < croppingView.frame.minX) && (croppingView.frame.width < maxSize?.width ?? frame.width || touchPoint.x > croppingView.frame.minX) {
                        make.left.equalTo(self).offset(touchPoint.x)
                    }
                }
            case .right:
                croppingView.snp.updateConstraints { make in
                    if (croppingView.frame.width > minSize.width || touchPoint.x > croppingView.frame.maxX) && (croppingView.frame.width < maxSize?.width ?? frame.width || touchPoint.x < croppingView.frame.maxX) {
                        make.right.equalTo(self).offset(-(bounds.maxX - touchPoint.x))
                    }
                }
            case .inside:
                if let lastTranslation = lastPoint {
                    var left: CGFloat = 0.0
                    var right: CGFloat = 0.0

                    left = croppingView.frame.minX + (translation.x - lastTranslation.x)
                    right = bounds.maxX - croppingView.frame.maxX - (translation.x - lastTranslation.x)
                    lastPoint = translation

                    if left < 0 || right < 0 { return }

                    croppingView.snp.updateConstraints { make in
                        make.left.equalTo(self).inset(left)
                        make.right.equalTo(self).inset(right)
                    }

                    if leftEdge.contains(touchPoint) {
                        currentEdge = .left
                        setEdgeTimer()
                    } else if rightEdge.contains(touchPoint) {
                        currentEdge = .right
                        setEdgeTimer()
                    } else {
                        stopEdgeTimer()
                    }
                } else {
                    lastPoint = translation
                }
            }

            delegate?.croppingAreaViewDidChangeFrame(cropFrame: croppingView.frame)

        case .ended, .cancelled, .failed:
            if selectedSnapPoint != nil {
                delegate?.croppingAreaViewDidEndChangingFrame(cropFrame: croppingView.frame)
            }
            stopEdgeTimer()
            selectedSnapPoint = nil
        default:
            break
        }
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
