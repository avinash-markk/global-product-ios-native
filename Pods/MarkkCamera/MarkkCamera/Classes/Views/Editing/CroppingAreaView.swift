//
//  CroppingAreaView.swift
//  MarkkCamera
//
//  Created by Sebastian Sztemberg on 18/02/2019.
//

import UIKit

enum CroppingSnapPoint {
    case topLeft, topRight, bottomLeft, bottomRight, inside
}

public enum CroppingMode {
    case photo, video
}

protocol CroppingAreaViewDelegate: AnyObject {
    func croppingAreaViewDidChangeFrame(cropFrame: CGRect)
    func croppingAreaViewDidEndChangingFrame(cropFrame: CGRect)
}

class CroppingAreaView: UIView {
    public weak var delegate: CroppingAreaViewDelegate?

    public var touchableGap: CGFloat = 50
    public var touchableAreaSize: CGFloat = 80

    public var croppingView = UIView()

    fileprivate var selectedSnapPoint: CroppingSnapPoint?
    fileprivate var lastPoint: CGPoint?

    public var croppingMode: CroppingMode = .photo {
        didSet {
            durationLabel.isHidden = croppingMode != .video
        }
    }

    public var minSize: CGSize = CGSize(width: 0, height: 0)
    public var maxSize: CGSize? {
        didSet {
            if let size = maxSize, let superview = superview {
                croppingView.snp.updateConstraints { make in
                    make.right.equalTo(self).offset(-(superview.frame.width - size.width))
                }
            }
        }
    }

    public lazy var durationLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "Inter-Bold", size: 10.0)
        label.textColor = UIColor.white
        label.textAlignment = .center
        return label
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)

        isUserInteractionEnabled = true

        initUI()
    }

    fileprivate func initUI() {
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(handlePan))
        addGestureRecognizer(panGesture)

        let lineColor = UIColor(hexString: "FECB03")

        let topLine = UIView()
        topLine.backgroundColor = lineColor
        let leftLine = UIView()
        leftLine.backgroundColor = lineColor
        let rightLine = UIView()
        rightLine.backgroundColor = lineColor
        let bottomLine = UIView()
        bottomLine.backgroundColor = lineColor

        let topLeftCornerSideLine = UIView()
        topLeftCornerSideLine.backgroundColor = lineColor
        let topLeftCornerUpperLine = UIView()
        topLeftCornerUpperLine.backgroundColor = lineColor

        let topRightCornerSideLine = UIView()
        topRightCornerSideLine.backgroundColor = lineColor
        let topRightCornerUpperLine = UIView()
        topRightCornerUpperLine.backgroundColor = lineColor

        let bottomLeftCornerSideLine = UIView()
        bottomLeftCornerSideLine.backgroundColor = lineColor
        let bottomLeftCornerUpperLine = UIView()
        bottomLeftCornerUpperLine.backgroundColor = lineColor

        let bottomRightCornerSideLine = UIView()
        bottomRightCornerSideLine.backgroundColor = lineColor
        let bottomRightCornerUpperLine = UIView()
        bottomRightCornerUpperLine.backgroundColor = lineColor

        let gridLineY1 = UIView()
        gridLineY1.backgroundColor = lineColor
        let gridLineY2 = UIView()
        gridLineY2.backgroundColor = lineColor

        let gridLineX1 = UIView()
        gridLineX1.backgroundColor = lineColor
        let gridLineX2 = UIView()
        gridLineX2.backgroundColor = lineColor

        addSubview(croppingView)

        croppingView.addSubview(topLine)
        croppingView.addSubview(leftLine)
        croppingView.addSubview(rightLine)
        croppingView.addSubview(bottomLine)

        croppingView.addSubview(topLeftCornerSideLine)
        croppingView.addSubview(topLeftCornerUpperLine)
        croppingView.addSubview(topRightCornerSideLine)
        croppingView.addSubview(topRightCornerUpperLine)
        croppingView.addSubview(bottomLeftCornerSideLine)
        croppingView.addSubview(bottomLeftCornerUpperLine)
        croppingView.addSubview(bottomRightCornerSideLine)
        croppingView.addSubview(bottomRightCornerUpperLine)

        croppingView.addSubview(durationLabel)

        croppingView.snp.makeConstraints { make in
            make.top.left.right.bottom.equalTo(self)
        }

        topLeftCornerSideLine.snp.makeConstraints { make in
            make.top.left.equalTo(croppingView)
            make.height.equalTo(16)
            make.width.equalTo(3)
        }
        topLeftCornerUpperLine.snp.makeConstraints { make in
            make.top.left.equalTo(croppingView)
            make.height.equalTo(3)
            make.width.equalTo(16)
        }

        topRightCornerSideLine.snp.makeConstraints { make in
            make.top.right.equalTo(croppingView)
            make.height.equalTo(16)
            make.width.equalTo(3)
        }
        topRightCornerUpperLine.snp.makeConstraints { make in
            make.top.right.equalTo(croppingView)
            make.height.equalTo(3)
            make.width.equalTo(16)
        }

        bottomLeftCornerSideLine.snp.makeConstraints { make in
            make.bottom.left.equalTo(croppingView)
            make.height.equalTo(16)
            make.width.equalTo(3)
        }
        bottomLeftCornerUpperLine.snp.makeConstraints { make in
            make.bottom.left.equalTo(croppingView)
            make.height.equalTo(3)
            make.width.equalTo(16)
        }

        bottomRightCornerSideLine.snp.makeConstraints { make in
            make.bottom.right.equalTo(croppingView)
            make.height.equalTo(16)
            make.width.equalTo(3)
        }
        bottomRightCornerUpperLine.snp.makeConstraints { make in
            make.bottom.right.equalTo(croppingView)
            make.height.equalTo(3)
            make.width.equalTo(16)
        }

        topLine.snp.makeConstraints { make in
            make.top.left.right.equalTo(croppingView)
            make.height.equalTo(2)
        }

        leftLine.snp.makeConstraints { make in
            make.top.left.bottom.equalTo(croppingView)
            make.width.equalTo(2)
        }

        rightLine.snp.makeConstraints { make in
            make.top.right.bottom.equalTo(croppingView)
            make.width.equalTo(2)
        }

        bottomLine.snp.makeConstraints { make in
            make.left.right.bottom.equalTo(croppingView)
            make.height.equalTo(2)
        }

        durationLabel.isHidden = croppingMode != .video
        durationLabel.snp.makeConstraints { make in
            make.center.equalTo(croppingView)
        }
    }

    public func reset() {
        croppingView.snp.updateConstraints { make in
            make.top.left.right.bottom.equalTo(self)
        }
    }

    @objc public func handlePan(_ sender: UIPanGestureRecognizer) {
        let fixedTouchableArea = min(touchableAreaSize, croppingView.frame.width / 4)
        let topLeftArea = CGRect(x: croppingView.frame.minX - touchableGap, y: croppingView.frame.minY - touchableGap, width: fixedTouchableArea + touchableGap, height: fixedTouchableArea + touchableGap)
        let topRightArea = CGRect(x: croppingView.frame.maxX - fixedTouchableArea, y: croppingView.frame.minY - touchableGap, width: fixedTouchableArea + touchableGap, height: fixedTouchableArea + touchableGap)
        let bottomLeftArea = CGRect(x: croppingView.frame.minX - touchableGap, y: croppingView.frame.maxY - fixedTouchableArea, width: fixedTouchableArea + touchableGap, height: fixedTouchableArea + touchableGap)
        let bottomRightArea = CGRect(x: croppingView.frame.maxX - fixedTouchableArea, y: croppingView.frame.maxY - fixedTouchableArea, width: fixedTouchableArea + touchableGap, height: fixedTouchableArea + touchableGap)

        let touchPoint = sender.location(in: self)
        let translation = sender.translation(in: self)

        print(touchPoint)
        if !self.bounds.contains(touchPoint) { return }

        switch sender.state {
        case .began:
            lastPoint = nil
            if topLeftArea.contains(touchPoint) {
                selectedSnapPoint = .topLeft
            } else if topRightArea.contains(touchPoint) {
                selectedSnapPoint = .topRight
            } else if bottomLeftArea.contains(touchPoint) {
                selectedSnapPoint = .bottomLeft
            } else if bottomRightArea.contains(touchPoint) {
                selectedSnapPoint = .bottomRight
            } else if frame.contains(touchPoint) {
                selectedSnapPoint = .inside
            }
        case .changed:
            guard let selectedSnapPoint = selectedSnapPoint else { return }

            switch selectedSnapPoint {
            case .topLeft:
                croppingView.snp.updateConstraints { make in
                    if croppingView.frame.height > minSize.height || touchPoint.y < croppingView.frame.minY && croppingMode != .video {
                        make.top.equalTo(self).offset(touchPoint.y)
                        make.left.equalTo(self).offset(touchPoint.y * frame.size.width / frame.size.height)
                    }
                }
            case .topRight:
               croppingView.snp.updateConstraints { make in
                if croppingView.frame.height > minSize.height || touchPoint.y < croppingView.frame.minY && croppingMode != .video {
                    make.top.equalTo(self).offset(touchPoint.y)
                    make.right.equalTo(self).offset(-((touchPoint.y) * frame.size.width / frame.size.height))
                }
                }
            case .bottomLeft:
                croppingView.snp.updateConstraints { make in
                    if croppingView.frame.height > minSize.height || touchPoint.y > croppingView.frame.maxY && croppingMode != .video {
                        make.bottom.equalTo(self).offset(-(bounds.maxY - touchPoint.y))
                        make.left.equalTo(self).offset(((bounds.maxY - touchPoint.y) * frame.size.width / frame.size.height))
                    }
                }
            case .bottomRight:
                croppingView.snp.updateConstraints { make in
                    if croppingView.frame.height > minSize.height || touchPoint.y > croppingView.frame.maxY && croppingMode != .video {
                        make.bottom.equalTo(self).offset(-(bounds.maxY - touchPoint.y))
                        make.right.equalTo(self).offset(-((bounds.maxY - touchPoint.y) * frame.size.width / frame.size.height))
                    }
                }
            case .inside:
                if let lastTranslation = lastPoint, croppingMode == .video {
                    var left: CGFloat = 0.0
                    var right: CGFloat = 0.0

                    left = croppingView.frame.minX + (translation.x - lastTranslation.x)
                    right = bounds.maxX - croppingView.frame.maxX - (translation.x - lastTranslation.x)
                    lastPoint = translation

                    if left < 0 || right < 0 { return }

                    croppingView.snp.updateConstraints { make in
                        make.left.equalTo(self).offset(left)
                        make.right.equalTo(self).offset(-right)
                    }
                } else {
                    lastPoint = translation
                }
            }

            delegate?.croppingAreaViewDidChangeFrame(cropFrame: croppingView.frame)

        case .ended, .cancelled, .failed:
            if selectedSnapPoint != nil {
                delegate?.croppingAreaViewDidEndChangingFrame(cropFrame: croppingView.frame)
            }
            selectedSnapPoint = nil
        default:
            break
        }
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
