//
//  CameraPhotoCroppingView.swift
//  MarkkCamera
//
//  Created by Sebastian Sztemberg on 18/02/2019.
//

import UIKit

protocol CameraPhotoCroppingViewDelegate: AnyObject {
    func cameraPhotoCroppingDidCropPhoto(_ photo: UIImage?)
}
class CameraPhotoCroppingView: UIView {
    public weak var delegate: CameraPhotoCroppingViewDelegate?

    fileprivate lazy var photoImageView: UIImageView = {
        let imageView = UIImageView()
        return imageView
    }()

    fileprivate lazy var resetButton: UIButton = {
        let button = UIButton(type: .custom)
        button.setTitle("reset".localized, for: .normal)
        button.addTarget(self, action: #selector(handleResetPress), for: .touchUpInside)
        button.setTitleColor(UIColor.white, for: .normal)
        button.titleLabel?.font = UIFont(name: "Inter-Bold", size: 14.0)
        return button
    }()

    fileprivate lazy var doneButton: UIButton = {
        let button = UIButton(type: .custom)
        button.setTitle("done".localized, for: .normal)
        button.addTarget(self, action: #selector(handleDonePress), for: .touchUpInside)
        button.setTitleColor(UIColor.white, for: .normal)
        button.titleLabel?.font = UIFont(name: "Inter-Bold", size: 14.0)
        return button
    }()

    fileprivate var cropContainerView = UIView()
    fileprivate var croppingAreaView = CroppingAreaView()

    convenience init(photo: UIImage) {
        self.init(frame: CGRect.zero)

        photoImageView.image = photo
    }

    override init(frame: CGRect) {
        super.init(frame: frame)

        initUI()
    }

    fileprivate func initUI() {
        backgroundColor = UIColor.black
        //
        //        let scrollView = UIScrollView()
        //
        //        addSubview(scrollView)
//        scrollView.snp.makeConstraints { make in
//            make.edges.equalTo(self)
//        }
//

//        scrollView.delegate = self
//        scrollView.minimumZoomScale = 1
//        scrollView.maximumZoomScale = 4

        addSubview(cropContainerView)
        cropContainerView.snp.makeConstraints { make in
            make.width.height.equalTo(self).multipliedBy(0.85)
            make.center.equalTo(self)
        }

        cropContainerView.addSubview(photoImageView)
        photoImageView.snp.makeConstraints { make in
            make.edges.equalTo(cropContainerView)
        }

        cropContainerView.addSubview(croppingAreaView)
        croppingAreaView.minSize = CGSize(width: 200, height: 200)
        croppingAreaView.snp.makeConstraints { make in
            make.top.left.right.bottom.equalTo(cropContainerView)
        }

        addSubview(resetButton)
        resetButton.snp.makeConstraints { make in
            make.top.left.equalTo(self).inset(20)
        }
        addSubview(doneButton)
        doneButton.snp.makeConstraints { make in
            make.top.right.equalTo(self).inset(20)
        }
    }

    @objc fileprivate func handleDonePress(sender: UIButton) {
        guard let inputImage = photoImageView.image else { return }

        let imageViewScale = max(inputImage.size.width / photoImageView.frame.width,
                                 inputImage.size.height / photoImageView.frame.height)

        let cropZone = CGRect(x: croppingAreaView.croppingView.frame.origin.x * imageViewScale,
                              y: croppingAreaView.croppingView.frame.origin.y * imageViewScale,
                              width: croppingAreaView.croppingView.frame.width * imageViewScale,
                              height: croppingAreaView.croppingView.frame.height * imageViewScale)

        guard let cutImageRef: CGImage = inputImage.cgImage?.cropping(to: cropZone)
            else { return }

        let croppedImage: UIImage = UIImage(cgImage: cutImageRef)

        delegate?.cameraPhotoCroppingDidCropPhoto(croppedImage)
    }

    @objc fileprivate func handleResetPress(sender: UIButton) {
        croppingAreaView.reset()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

//extension CameraPhotoCroppingView: UIScrollViewDelegate {
//    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
//        return photoImageView
//    }
//}
