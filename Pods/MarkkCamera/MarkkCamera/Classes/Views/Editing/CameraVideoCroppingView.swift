//
//  CameraVideoCroppingView.swift
//  MarkkCamera
//
//  Created by Sebastian Sztemberg on 20/02/2019.
//

import UIKit
import AVFoundation
import KRProgressHUD

protocol CameraVideoCroppingViewDelegate: AnyObject {
    func cameraVideoCroppingDidClose()
    func cameraVideoCroppingDidCreateAsset(_ asset: CameraAsset)
}
class CameraVideoCroppingView: UIView {
    public weak var delegate: CameraVideoCroppingViewDelegate?

    fileprivate var asset: AVAsset?
    fileprivate var videoUrl: URL?

    fileprivate var startTime: Double = 0
    fileprivate var endTime: Double = CameraConfig.maxVideoDuration

    fileprivate var previewView: CameraPreviewVideoView?
    fileprivate var croppingTimelineView: CroppingTimelineView?
    
    public var cameraManager: CameraManager?

    fileprivate lazy var cancelButton: UIButton = {
        let button = UIButton(type: .custom)
        button.setTitle("cancel".localized, for: .normal)
        button.addTarget(self, action: #selector(handleCancelPress), for: .touchUpInside)
        button.setTitleColor(UIColor.white, for: .normal)
        button.titleLabel?.font = UIFont(name: "Inter-Bold", size: 14.0)
        return button
    }()

    fileprivate lazy var saveButton: UIButton = {
        let button = UIButton(type: .custom)
        button.setTitle("save".localized, for: .normal)
        button.addTarget(self, action: #selector(handleSavePress), for: .touchUpInside)
        button.setTitleColor(UIColor.white, for: .normal)
        button.titleLabel?.font = UIFont(name: "Inter-Bold", size: 14.0)
        return button
    }()

    init(url: URL, cameraManager: CameraManager?) {
        super.init(frame: CGRect.zero)
        self.cameraManager = cameraManager
        videoUrl = url
        asset = AVAsset(url: url)

        initUI()
    }

    fileprivate func initUI() {
        guard let videoUrl = videoUrl, let asset = asset else { return }

        previewView = CameraPreviewVideoView(videoUrl: videoUrl)
        croppingTimelineView = CroppingTimelineView(asset: asset)

        guard let previewView = previewView, let croppingTimelineView = croppingTimelineView else { return }

        addSubview(previewView)
        previewView.snp.makeConstraints { make in
            make.center.equalTo(self)
            make.width.equalTo(self)
            make.height.equalTo(snp.width).multipliedBy(CameraUtils.cameraRatio)
        }

        addSubview(croppingTimelineView)
        croppingTimelineView.delegate = self
        croppingTimelineView.snp.makeConstraints { make in
            make.left.right.bottom.equalTo(self)
            make.height.equalTo(90)
        }

        previewView.addSubview(cancelButton)
        cancelButton.snp.makeConstraints { make in
            make.top.left.equalTo(previewView).inset(20)
        }

        previewView.addSubview(saveButton)
        saveButton.snp.makeConstraints { make in
            make.top.right.equalTo(previewView).inset(20)
        }
    }

    fileprivate func cropVideo(startTime: Double, endTime: Double, completion: ((_ outputUrl: URL) -> Void)? = nil) {
        guard let videoAsset = asset, let outputURL = CameraFileManager.filePath(type: .video), let exportSession = AVAssetExportSession(asset: videoAsset, presetName: AVAssetExportPresetHighestQuality) else { return }
        exportSession.outputURL = outputURL
        exportSession.outputFileType = .mp4

        let timeRange = CMTimeRange(start: CMTime(seconds: startTime, preferredTimescale: 1000),
                                    end: CMTime(seconds: endTime, preferredTimescale: 1000))

        exportSession.timeRange = timeRange
        exportSession.exportAsynchronously {
            switch exportSession.status {
            case .completed:
                print("exported at \(outputURL)")
                completion?(outputURL)
            case .failed:
                print("failed \(exportSession.error.debugDescription)")
            case .cancelled:
                print("cancelled \(exportSession.error.debugDescription)")
            default: break
            }
        }
    }

    @objc fileprivate func handleCancelPress(sender: UIButton) {
        delegate?.cameraVideoCroppingDidClose()
    }

    @objc fileprivate func handleSavePress(sender: UIButton) {
        KRProgressHUD.show(withMessage: "loading_processing_video".localized)
        cropVideo(startTime: startTime, endTime: endTime) { croppedUrl in
            KRProgressHUD.dismiss()
            DispatchQueue.main.async {
                self.delegate?.cameraVideoCroppingDidCreateAsset(CameraAsset(type: .video, url: croppedUrl, image: nil, fromGallery: true, location: nil, createdTime: nil))
            }
        }
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension CameraVideoCroppingView: CroppingTimelineViewDelegate {
    func croppingTimeLineDidUpdateTime(startSeconds: Double, endSeconds: Double) {
        startTime = startSeconds
        endTime = endSeconds

        previewView?.setStartTime(startSeconds)
        previewView?.setEndTime(endSeconds)
    }
}
