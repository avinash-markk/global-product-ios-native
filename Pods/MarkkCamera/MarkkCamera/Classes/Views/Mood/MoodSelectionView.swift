//
//  MoodSelectionView.swift
//  Sminq
//
//  Created by Bjorn Mascarenhas on 28/09/18.
//  Copyright © 2018 Suraj Sagare. All rights reserved.
//

import UIKit

protocol MoodSelectionViewDelegate: NSObjectProtocol {
    func moodSelectionOverlayClose()
    func moodSelectLocation()
    func moodSelected(moodStr: String, asset: CameraAsset?)
}

class MoodSelectionView: UIView {
    public var multiplier: CGFloat = 0.0
    public weak var delegate: MoodSelectionViewDelegate?
    var isFirstRating: Bool = false
    var ratingImageName: String = "moodPlaceHolder"
    var selectedCameraAsset: CameraAsset?
    public var cameraManager: CameraManager?
    
    fileprivate var moodDrawerView = UIView()
    fileprivate var moodButtonNope = UIButton()
    fileprivate var moodButtonOkay = UIButton()
    fileprivate var moodButtonDope = UIButton()
    
    fileprivate lazy var closeBtn: UIButton = {
        let btn = UIButton.init(type: .custom)
        btn.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btn.showsTouchWhenHighlighted = true
        btn.setImage(UIImage.bundledImage(named: "iconClose24WhitePureShadow"), for: .normal)
        btn.addTarget(self, action: #selector(closeAction), for: .touchUpInside)
        return btn
    }()
    
    fileprivate lazy var locationBtn: UIButton = {
        let btn = UIButton.init(type: .custom)
        btn.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btn.showsTouchWhenHighlighted = true
        btn.setImage(UIImage.bundledImage(named: "iconMarker"), for: .normal)
        btn.addTarget(self, action: #selector(selectLocationAction), for: .touchUpInside)
        return btn
    }()
    
    fileprivate lazy var locationLabel: UILabel = {
        let locationLabel = UILabel.init()
        locationLabel.text = "my place"
        locationLabel.font = UIFont(name: "Inter-Bold", size: 20.0)
        locationLabel.textColor = UIColor.white
        return locationLabel
    }()
    
    fileprivate lazy var moodLabel: UILabel = {
        let moodLabel = UILabel.init()
        moodLabel.text = "Select a Live Rating"
        moodLabel.numberOfLines = 2
        moodLabel.font = UIFont(name: "Inter-Bold", size: 20.0)
        moodLabel.textAlignment = NSTextAlignment.center
        moodLabel.textColor = UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1.0)
        return moodLabel
    }()
    
    fileprivate lazy var selectedMoodButton: UIButton = {
        let btn = UIButton.init(type: .custom)
        btn.showsTouchWhenHighlighted = true
        return btn
    }()
    
    fileprivate lazy var overlayView: UIView = {
        let view = UIView.init(frame: self.frame)
        view.backgroundColor = CameraUtils.CameraColors.blackDark100.withAlphaComponent(0.75)
        return view
    }()
    
    init(cameraManager: CameraManager?, frame: CGRect) {
        super.init(frame: frame)
        self.cameraManager = cameraManager
        multiplier = frame.width / 375  // Note: The with of reference view (zeplin design) had width 375
        self.initializeUI()
        self.initializeMoodLabel()
        self.ratingImageName = "moodIconWithText"
        self.initializeMoodSubview()
    }
    
    public func initializeUI() {
        self.addSubview(overlayView)
        
        locationBtn.center = CGPoint.init(x: 20 + locationBtn.frame.width / 2, y: locationBtn.frame.height / 2 + 20)
        self.overlayView.addSubview(locationBtn)
        
        closeBtn.center = CGPoint.init(x: self.frame.width - closeBtn.frame.width / 2 - 20, y: closeBtn.frame.height / 2 + 20)
        self.overlayView.addSubview(closeBtn)
        
        locationLabel.bounds = CGRect.init(x: 0, y: 0, width: 225, height: 24)
        locationLabel.frame.origin = CGPoint.init(x: 55, y: 22)
        self.overlayView.addSubview(locationLabel)
    }
    
    public func initializeMoodLabel() {
        moodLabel.bounds = CGRect.init(x: 0, y: 0, width: 140, height: 50)
        if UIScreen.main.nativeBounds.height <= 568 {
            moodLabel.frame.origin = CGPoint.init(x: (self.frame.width / 2) - (moodLabel.frame.width / 2), y: self.frame.height - 245)
        } else {
            moodLabel.frame.origin = CGPoint.init(x: (self.frame.width / 2) - (moodLabel.frame.width / 2), y: self.frame.height - 265)
        }
        self.overlayView.addSubview(moodLabel)
        moodLabel.alpha = 0
    }
    
    public func initializeMoodSubview() {
        moodDrawerView.frame = CGRect(x: 0, y: self.frame.height - 188, width: self.frame.width, height: 160)
        selectedMoodButton.frame = CGRect(x: (moodDrawerView.frame.width/2 - 32), y: moodDrawerView.frame.height - 64, width: 64, height: 64)
        selectedMoodButton.layer.cornerRadius = selectedMoodButton.frame.height / 2
        selectedMoodButton.layer.borderWidth = 2
        selectedMoodButton.layer.borderColor = UIColor.white.cgColor
        selectedMoodButton.clipsToBounds = true
        moodDrawerView.addSubview(selectedMoodButton)
        
        self.initializeMoodButton(button: moodButtonNope, tag: 1)
        self.initializeMoodButton(button: moodButtonOkay, tag: 2)
        self.initializeMoodButton(button: moodButtonDope, tag: 3)
        
        self.overlayView.addSubview(moodDrawerView)
    }
    
    public func initializeMoodButton(button: UIButton, tag: Int) {
        button.frame = CGRect(x: (moodDrawerView.frame.width/2 - 48), y: moodDrawerView.frame.height - 96, width: 96, height: 96)
        button.showsTouchWhenHighlighted = true
        button.setImage(UIImage.bundledImage(named: tag.getRatingsImageMeta().image96), for: .normal)
        button.tag = tag
        button.addTarget(self, action: #selector(moodSelectedAction), for: .touchUpInside)
        moodDrawerView.addSubview(button)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc fileprivate func closeAction() {
        self.delegate?.moodSelectionOverlayClose()
    }
    
    @objc fileprivate func selectLocationAction() {
        self.dismiss()
        self.delegate?.moodSelectLocation()
    }
    
    @objc fileprivate func moodSelectedAction(sender: UIButton) {
        UIView.animate(withDuration: 0.3, animations: {
            sender.transform = CGAffineTransform(rotationAngle: CGFloat(self.deg2rad(0)))
            sender.frame = self.selectedMoodButton.frame
        }, completion: { (_: Bool) in
            self.selectedMoodButton.setImage(UIImage.bundledImage(named: sender.tag.getRatingsImageMeta().image64), for: .normal)
            self.resetMoodDrawerAnimation()
            self.delegate?.moodSelected(moodStr: "\(sender.tag)", asset: self.selectedCameraAsset)
        })
    }
    
    public func dismiss() {
        UIView.animate(withDuration: 0.3, animations: {
            self.resetMoodDrawerAnimation()
            self.alpha = 0
        }, completion: { (x) in
            if x {
                self.isHidden = true
            }
        })
    }
    
    public func animateMoodDrawerForFirstRating() {
        self.disableMoodButtonInterations()
        UIView.animate(withDuration: 0.5, animations: {
            self.animateButtonWithNewFrame(button: self.moodButtonNope, newFrame: CGRect(x: (self.moodDrawerView.frame.width/4) - 48 + 15, y: 24, width: 96, height: 96), transform: nil)
            
            self.animateButtonWithNewFrame(button: self.moodButtonOkay, newFrame: CGRect(x: self.moodDrawerView.frame.width/2 - 48, y: 0, width: 96, height: 96), transform: nil)
            
            self.animateButtonWithNewFrame(button: self.moodButtonDope, newFrame: CGRect(x: (self.moodDrawerView.frame.width * (3/4)) - 48 - 15, y: 24, width: 96, height: 96), transform: nil)
        }, completion: { _ in
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                self.animateMoodIconsWithText()
            })
        })
    }
    
    public func animateMoodDrawer() {
        UIView.animate(withDuration: 0.5, animations: {
            self.animateButtonWithNewFrame(button: self.moodButtonNope, newFrame: CGRect(x: (self.moodDrawerView.frame.width/4) - 48 + 15, y: 24, width: 96, height: 96), transform: CGAffineTransform(rotationAngle: CGFloat(self.deg2rad(-35))))
            
            self.animateButtonWithNewFrame(button: self.moodButtonOkay, newFrame: CGRect(x: self.moodDrawerView.frame.width/2 - 48, y: 0, width: 96, height: 96), transform: nil)
            
            self.animateButtonWithNewFrame(button: self.moodButtonDope, newFrame: CGRect(x: (self.moodDrawerView.frame.width * (3/4)) - 48 - 15, y: 24, width: 96, height: 96), transform: CGAffineTransform(rotationAngle: CGFloat(self.deg2rad(35))))
        }, completion: { _ in
            self.moodLabel.alpha = 1
        })
    }
    
    func animateButtonWithNewFrame(button: UIButton, newFrame: CGRect, transform: CGAffineTransform?) {
        button.frame = newFrame
        button.alpha = 1
        if let transformAnimation = transform {
            button.transform = transformAnimation
        }
    }
    
    func deg2rad(_ number: Double) -> Double {
        return number * .pi / 180
    }
    
    public func animateMoodIconsWithText() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.10, execute: {
            UIView.animate(withDuration: 0.10, animations: {
                self.moodButtonNope.setImage(UIImage.bundledImage(named: self.moodButtonNope.tag.getRatingsImageMeta().image96), for: .normal)
                self.moodButtonNope.transform = CGAffineTransform(rotationAngle: CGFloat(self.deg2rad(-35)))
            }, completion: { _ in
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.10, execute: {
                    UIView.animate(withDuration: 0.10, animations: {
                        self.moodButtonOkay.setImage(UIImage.bundledImage(named: self.moodButtonOkay.tag.getRatingsImageMeta().image96), for: .normal)
                    }, completion: { _ in
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.10, execute: {
                            UIView.animate(withDuration: 0.10, animations: {
                                self.moodButtonDope.setImage(UIImage.bundledImage(named: self.moodButtonDope.tag.getRatingsImageMeta().image96), for: .normal)
                                self.moodButtonDope.transform = CGAffineTransform(rotationAngle: CGFloat(self.deg2rad(35)))
                            }, completion: { _ in
                                DispatchQueue.main.asyncAfter(deadline: .now() + 0.10, execute: {
                                    self.animateMoodLabel()
                                })
                            })
                        })
                    })
                })
            })
        })
    }
    
    public func animateMoodLabel() {
        UIView.animate(withDuration: 0.05, delay: 0.0, options: UIView.AnimationOptions.showHideTransitionViews, animations: {
            self.moodLabel.alpha = 1
        }, completion: { _ in
            self.enableMoodButtonInterations()
        })
    }
    
    public func enableMoodButtonInterations() {
        moodButtonNope.isUserInteractionEnabled = true
        moodButtonOkay.isUserInteractionEnabled = true
        moodButtonDope.isUserInteractionEnabled = true
        locationBtn.isUserInteractionEnabled = true
        closeBtn.isUserInteractionEnabled = true
    }
    
    public func disableMoodButtonInterations() {
        moodButtonNope.isUserInteractionEnabled = false
        moodButtonOkay.isUserInteractionEnabled = false
        moodButtonDope.isUserInteractionEnabled = false
        locationBtn.isUserInteractionEnabled = false
        closeBtn.isUserInteractionEnabled = false
    }
    
    public func resetMoodDrawerAnimation() {
        UIView.animate(withDuration: 0.1, animations: {
            self.resetMoodButtons(button: self.moodButtonNope)
            self.resetMoodButtons(button: self.moodButtonOkay)
            self.resetMoodButtons(button: self.moodButtonDope)
        }, completion: { _ in
            self.moodLabel.alpha = 0
        })
    }
    
    public func resetMoodButtons(button: UIButton) {
        button.frame = CGRect(x: self.moodDrawerView.frame.width/2 - 36, y: self.moodDrawerView.frame.height - 72, width: 72, height: 72)
        button.setImage(UIImage.bundledImage(named: button.tag.getRatingsImageMeta().image96), for: .normal)
        button.transform = CGAffineTransform(rotationAngle: CGFloat(self.deg2rad(0)))
        button.alpha = 0
    }
    
    public func display(placeName: String?, cameraAsset: CameraAsset?) {
        selectedCameraAsset = cameraAsset
        UIView.animate(withDuration: 0.1, animations: {
            self.isHidden = false
            self.selectedMoodButton.setImage(nil, for: .normal)
            if let placeText: String = placeName {
                self.locationLabel.text = placeText
            }
        }, completion: { (_: Bool) in
            DispatchQueue.main.async {
                self.alpha = 1
                self.animateRatings()
            }
        })
    }
    
    public func animateRatings() {
        self.moodButtonNope.alpha = 0
        self.moodButtonOkay.alpha = 0
        self.moodButtonDope.alpha = 0
        if self.isFirstRating {
            self.animateMoodDrawerForFirstRating()
        } else {
            self.animateMoodDrawer()
        }
    }
    
}

extension Int {
    /**
     Returns meta associated for ratings which contains image name
     
     - Returns: Image meta data for mood
     */
    
    func getRatingsImageMeta() -> (image64: String, image96: String) {
        switch self {
        case 1:
            return (image64: "ratingsNope64", image96: "ratingsLabelNope96")
        case 2:
            return (image64: "ratingsOkay64", image96: "ratingsLabelOkay96")
        case 3:
            return (image64: "ratingsDope64", image96: "ratingsLabelDope96")
        default:
            return (image64: "ratingsOkay64", image96: "ratingsLabelOkay96")
        }
    }
    
}
