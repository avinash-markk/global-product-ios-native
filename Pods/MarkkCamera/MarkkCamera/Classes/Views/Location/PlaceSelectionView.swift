//
//  PlaceSelectionView.swift
//  Markk
//
//  Created by Bjorn Mascarenhas on 06/08/18.
//  Copyright © 2018 com.garry. All rights reserved.
//
import UIKit
import Foundation
import KRProgressHUD

public struct CameraPlace {
    public var id: String
    public var name: String?
    public var address: String?
    public var distance: CGFloat?
    public var distanceString: String?
    public var stickers: [Sticker]?
    
    public init(id: String, name: String, address: String) {
        self.id = id
        self.name = name
        self.address = address
    }
}

public protocol PlaceSelectionViewDelegate: NSObjectProtocol {
    func placeSelectedWith(placeDetail: CameraPlace)
    func placeSelectionOverlayClosed()
    func showErrorToast(message: String)
}

//TODO: Use SnapKit

public class PlaceSelectionView: UIView {
    public weak var delegate: PlaceSelectionViewDelegate?
    
    var searchedPlaces: [CameraPlace] = []
    var storedSearchPlaces: [CameraPlace] = []
    let searchTextField = DesignableUITextField()
    var searchText: String = ""
    public let placeSubview = UIView()
    let roundedSubview = UIView()
    let closeSubButton = UIButton()
    
    public var cameraManager: CameraManager?
    public var cameraSource: CameraDataSource?
    public var placesTableView: UITableView!
    
    var nearbyPlacesMode: Bool = true
    
    fileprivate lazy var locationLabel: UILabel = {
        let locationLabel = UILabel.init()
        locationLabel.text = "Tag a place"
        locationLabel.font = UIFont(name: "Inter-Bold", size: 20.0)
        locationLabel.textColor = UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1.0)
        return locationLabel
    }()
    
    fileprivate lazy var closeBtn: UIButton = {
        let btn = UIButton.init(type: .custom)
        btn.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btn.showsTouchWhenHighlighted = true
        btn.setImage(UIImage.bundledImage(named: "iconClose24WhitePureShadow"), for: .normal)
        btn.addTarget(self, action: #selector(closeAction), for: .touchUpInside)
        return btn
    }()
    
    fileprivate lazy var overlayView: UIView = {
        let view = UIView.init(frame: self.frame)
        view.backgroundColor = CameraUtils.CameraColors.blackDark100.withAlphaComponent(0.75)
        return view
    }()

    init(cameraManager: CameraManager?, frame: CGRect) {
        super.init(frame: frame)
        self.cameraManager = cameraManager
        addSubview(overlayView)
        
        closeBtn.center = CGPoint.init(x: self.frame.width - closeBtn.frame.width / 2 - 20, y: closeBtn.frame.height / 2 + 20)
        overlayView.addSubview(closeBtn)
        
        let displayWidth: CGFloat = self.frame.width
        let displayHeight: CGFloat = self.frame.height
        
        locationLabel.frame = CGRect(x: 0, y: 0, width: displayWidth, height: 24)
        locationLabel.textAlignment = NSTextAlignment.center
        placeSubview.addSubview(locationLabel)
        
        closeSubButton.showsTouchWhenHighlighted = true
        closeSubButton.setImage(UIImage.bundledImage(named: "iconClose24WhitePureShadow"), for: .normal)
        closeSubButton.addTarget(self, action: #selector(closeAction), for: .touchUpInside)
        closeSubButton.frame = CGRect(x: self.frame.width - 60, y: 0, width: 32, height: 32)
        placeSubview.addSubview(closeSubButton)
        
        let triangle = TriangleView.init(color: UIColor.white)
        triangle.frame = CGRect(x: 0, y: 0, width: 12, height: 6)
        triangle.center = CGPoint(x: self.frame.width/2, y: 47)
        placeSubview.addSubview(triangle)
        
        roundedSubview.frame = CGRect(x: 0, y: 50, width: displayWidth, height: 10)
        roundedSubview.roundCorners(corners: [.topLeft, .topRight], radius: 8.0)
        roundedSubview.backgroundColor = UIColor.white
        placeSubview.addSubview(roundedSubview)
        placeSubview.backgroundColor = UIColor.clear
        
        placeSubview.frame = CGRect(x: 0, y: displayHeight/2 - 60, width: displayWidth, height: 60)
        
        overlayView.addSubview(placeSubview)
        
        placesTableView = UITableView(frame: CGRect(x: 0, y: displayHeight/2, width: displayWidth, height: displayHeight/2))
        
        placesTableView.register(PlaceSearchTableViewCell.self, forCellReuseIdentifier: "PlaceSearchTableViewCell")
        
        placesTableView.rowHeight = UITableView.automaticDimension
        placesTableView.estimatedRowHeight = 140
        
        placesTableView.isUserInteractionEnabled = true
        placesTableView.isScrollEnabled = true
        placesTableView.dataSource = self as UITableViewDataSource
        placesTableView.delegate = self as UITableViewDelegate
        placesTableView.separatorColor = UIColor(red: 144.0/255.0, green: 126.0/255.0, blue: 127.0/255.0, alpha: 0.25)
        overlayView.addSubview(placesTableView)
        
        searchTextField.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        
    }
    
    
    @objc fileprivate func textFieldDidChange() {
        guard let text = searchTextField.text else { return }
        
        if text.count == 0 {
            searchedPlaces.removeAll()
            populateNearByPlaces()
        }
    }
    
    public func retrySearch() {
        if !nearbyPlacesMode {
            searchPlaces(searchedString: searchText)
        } else {
            populateNearByPlaces()
        }
    }
    
    public func updateKeyboardFrame(keyboardHeight: CGFloat) {
        self.placesTableView.frame = CGRect(x: self.placesTableView.frame.origin.x, y: self.placesTableView.frame.origin.y, width: self.placesTableView.frame.width, height: self.frame.height - 85 - keyboardHeight + self.frame.origin.y)
        
        self.placeSubview.frame = CGRect(x: self.placeSubview.frame.origin.x, y: self.placeSubview.frame.origin.y, width: self.placeSubview.frame.width, height: self.frame.height - 28 - keyboardHeight + self.frame.origin.y)
    }
    
    @objc fileprivate func closeAction() {
        self.endEditing(true)
        self.delegate?.placeSelectionOverlayClosed()
    }
    
    public func dismiss() {
        self.endEditing(true)
        self.searchedPlaces = []
        self.searchedPlaces = self.storedSearchPlaces
        self.placesTableView.reloadData()
        UIView.animate(withDuration: 0.3, animations: {
            self.placeSubview.frame =  CGRect(x: 0, y: self.frame.height, width: self.frame.width, height: 0)
            self.placesTableView.frame = CGRect(x: 0, y: self.frame.height, width: self.frame.width, height: 0)
            self.closeSubButton.isHidden = true
            self.alpha = 0
        }, completion: { (x) in
            if x {
                self.isHidden = true
            }
        })
    }
    
    public func display() {
        searchTextField.text = ""
        self.layer.removeAllAnimations()
        self.isHidden = false
        
        UIView.animate(withDuration: 0.3) {
            self.placeSubview.frame =  CGRect(x: 0, y: self.frame.height/2 - 60, width: self.frame.width, height: self.frame.height/2 + 60)
            self.closeSubButton.isHidden = true
            self.closeBtn.isHidden = false
            self.placesTableView.frame = CGRect(x: 0, y: self.frame.height/2, width: self.frame.width, height: self.frame.height/2)
            self.alpha = 1
        }
        populateNearByPlaces()
    }
    
    public func clearPlaceList() {
        self.searchedPlaces = []
        self.storedSearchPlaces = []
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension PlaceSelectionView: UITableViewDelegate, UITableViewDataSource {
    public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1.0)
        searchTextField.frame = CGRect(x: 16, y: 6, width: self.frame.size.width - 32, height: 40)
        searchTextField.backgroundColor = CameraUtils.CameraColors.blackDark100.withAlphaComponent(0.05)
        searchTextField.delegate = self
        searchTextField.clearButtonMode = .whileEditing
        searchTextField.font = UIFont(name: "Inter-Regular", size: 16.0)
        searchTextField.leftImage  = UIImage.bundledImage(named: "iconSearch24BlackDark50")
        searchTextField.leftPadding = 8
        searchTextField.textColor = CameraUtils.CameraColors.blackDark100.withAlphaComponent(0.5)
        searchTextField.placeholder = "Search for a place..."
        searchTextField.roundCorners(corners: [.topLeft, .topRight, .bottomRight, .bottomLeft], radius: 8)
        
        headerView.addSubview(searchTextField)
        
        let bottomLineView = UIView()
        bottomLineView.frame = CGRect(x: 0, y: 61, width: self.frame.size.width, height: 0.5)
        bottomLineView.backgroundColor = UIColor(red: 144.0/255.0, green: 126.0/255.0, blue: 127.0/255.0, alpha: 0.25)
        headerView.addSubview(bottomLineView)
        
        return headerView
    }
    
    public func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 62
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.endEditing(true)
        if searchedPlaces.count > 0 {
            if let locationSelectCallback = cameraSource?.locationSelectCallback {
                KRProgressHUD.show()
                locationSelectCallback(self.searchedPlaces[indexPath.row]) { filledLocation, error in
                    DispatchQueue.main.async {
                        KRProgressHUD.dismiss()
                        if let filledLocation = filledLocation {
                            self.delegate?.placeSelectedWith(placeDetail: filledLocation)
                        }
                    }
                }
            }
        }
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchedPlaces.count > 0 {
            return searchedPlaces.count
        } else {
            return Int(0)
        }
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "PlaceSearchTableViewCell", for: indexPath) as? PlaceSearchTableViewCell {
            if searchedPlaces.count > 0 {
                cell.placeNameLabel.text = searchedPlaces[indexPath.row].name
                
                if let distance = searchedPlaces[indexPath.row].distance, let distanceString = searchedPlaces[indexPath.row].distanceString {
                    if let address = searchedPlaces[indexPath.row].address {
                        if distance > 0.0 {
                            cell.distanceLabel.text = "\(distanceString) • \(address)"
                        } else {
                            cell.distanceLabel.text = "\(address)"
                        }
                    }
                } else {
                    if let address = searchedPlaces[indexPath.row].address {
                        cell.distanceLabel.text = String("\(address)")
                    }
                }
            }
            
            cell.preservesSuperviewLayoutMargins = false
            cell.separatorInset = UIEdgeInsets.zero
            cell.layoutMargins = UIEdgeInsets.zero
            return cell
        } else {
            return UITableViewCell()
        }
    }
}

extension PlaceSelectionView: UITextFieldDelegate {
    // MARK: Text field delegate
    
    public func textFieldDidBeginEditing(_ textField: UITextField) {
        placeSubview.frame = CGRect(x: 0, y: 28, width: self.frame.width, height: self.frame.height - 28)
        self.placesTableView.frame = CGRect(x: 0, y: 85, width: self.frame.width, height: self.frame.height - 85)
        closeSubButton.isHidden = false
        self.closeBtn.isHidden = true
    }
    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (textField.text?.count)! > 1 {
            self.searchedPlaces.removeAll()
            searchText = textField.text!
            searchPlaces(searchedString: textField.text!)
        }
        return true
    }
    
    public func textFieldShouldClear(_ textField: UITextField) -> Bool {
        self.searchedPlaces.removeAll()
        return true
    }
    
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func reloadTableView() {
        self.placesTableView.reloadData()
    }
    
    func populateNearByPlaces() {
        nearbyPlacesMode = true
        placesTableView.isHidden = false
        
        if let nearbyCallback = cameraSource?.nearbyLocationsCallback {
            KRProgressHUD.show()
            nearbyCallback { places, error in
                DispatchQueue.main.async {
                    KRProgressHUD.dismiss()
                    if let places = places {
                        self.searchedPlaces = places
                        self.placesTableView.reloadData()
                    }
                }
            }
        }
    }
    
    func searchPlaces(searchedString: String) {
        nearbyPlacesMode = false
        
        if let locationCallback = cameraSource?.locationsCallback {
            locationCallback(searchedString) { places, error in
                DispatchQueue.main.async {
                    KRProgressHUD.dismiss()
                    if let places = places {
                        self.searchedPlaces = places
                        self.placesTableView.reloadData()
                    }
                }
            }
        }
    }
}
