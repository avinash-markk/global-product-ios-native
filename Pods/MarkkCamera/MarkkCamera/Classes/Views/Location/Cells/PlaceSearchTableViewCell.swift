//
//  PlaceSearchTableViewCell.swift
//  Sminq
//
//  Created by Bjorn Mascarenhas on 02/08/18.
//  Copyright © 2018 Suraj Sagare. All rights reserved.
//
import UIKit

class PlaceSearchTableViewCell: UITableViewCell {
    fileprivate lazy var markerImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.image = UIImage.bundledImage(named: "iconMarker24BlackDark50")
        imageView.layer.masksToBounds = true
        return imageView
    }()

    public lazy var placeNameLabel: UILabel = {
        let label = UILabel()
        label.textColor = CameraUtils.CameraColors.blackDark100.withAlphaComponent(0.75)
        label.font = UIFont(name: "Inter-Regular", size: 16.0)
        return label
    }()

    public lazy var distanceLabel: UILabel = {
        let label = UILabel()
        label.textColor = CameraUtils.CameraColors.blackDark100.withAlphaComponent(0.5)
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = UIFont(name: "Inter-Regular", size: 12.0)
        return label
    }()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        addSubview(markerImageView)
        addSubview(placeNameLabel)
        addSubview(distanceLabel)

        markerImageView.snp.makeConstraints { make in
            make.width.height.equalTo(24)
            make.left.equalTo(self).offset(16)
            make.centerY.equalTo(self)
        }

        placeNameLabel.snp.makeConstraints { make in
            make.left.equalTo(markerImageView.snp.right).offset(8)
            make.right.equalTo(self).offset(-16)
            make.top.equalTo(self).offset(10)
            make.height.equalTo(20)
        }

        distanceLabel.snp.makeConstraints { make in
            make.left.right.equalTo(placeNameLabel)
            make.top.equalTo(placeNameLabel.snp.bottom).offset(5)
            make.bottom.equalTo(self).offset(-10)
        }
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
