//
//  CameraViewController.swift
//  Sminq
//
//  Created by Sebastian Sztemberg on 22/01/2019.
//  Copyright © 2019 Suraj Sagare. All rights reserved.
//

import UIKit
import AVFoundation
import KRProgressHUD

public protocol CameraViewControllerDelegate: AnyObject {
    func cameraDidAcceptAssets(_ assets: [CameraAsset])
    func cameraDidDismiss()
    func cameraViewDidReceiveStickerSearch(text: String)
    func cameraDidSelectRating(rating: String, asset: CameraAsset?)
    func permissionsGrantedFor(type: String, permission: String)
    func cameraDidTapAction(name: String)
    func cameraDiscardRating(multiple: Bool)
    func cameraDidSelectSticker(sticker: Sticker)
    func cameraDidAddTextSticker(stickerText: String)
    func cameraMultipleAssetsFromGallerySelected(videoCount: Int?, imageCount: Int?, totalCount: Int?)
}

public struct CameraError : LocalizedError {
    public var errorDescription: String?
    
    public init(errorDescription: String) {
        self.errorDescription = errorDescription
    }
}

open class CameraViewController: UIViewController {
    public weak var delegate: CameraViewControllerDelegate?
   
    public var cameraSource: CameraDataSource?
    public var initialPlace: CameraPlace?
    public var initialMood: String?
    public var galleryInterval: Int!
    
    fileprivate var cameraCaptureView: CameraCaptureView?
    fileprivate var cameraAuthorizationView: CameraAuthorizationView?
    fileprivate var cameraPreviewView: CameraPreviewView?
    fileprivate var cameraVideoCroppingView: CameraVideoCroppingView?

    open var cameraLocationView: PlaceSelectionView?
    fileprivate var cameraMoodView: MoodSelectionView?

    fileprivate var previewActive = false
    
    fileprivate var initialTouchPoint: CGPoint = CGPoint(x: 0, y: 0)

    fileprivate let cameraManager = CameraManager()
    
    override open func viewDidLoad() {
        super.viewDidLoad()
        CameraUtils.registerFonts()
        cameraManager.capturingDelegate = self
        cameraManager.galleryInterval = self.galleryInterval ?? 24
        initUI()

        NotificationCenter.default.addObserver(self, selector: #selector(willResignActive), name: UIApplication.willResignActiveNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(willEnterForeground), name: UIApplication.didBecomeActiveNotification, object: nil)
    }
    
    override open func viewWillAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        cameraManager.capturingDelegate = self
        cameraManager.resumeSession()
        checkPermissions()
    }

    override open func viewWillDisappear(_ animated: Bool) {
        cameraManager.pauseSession()
    }

    fileprivate func initUI() {
        view.backgroundColor = UIColor.black

        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(handlePan))
        panGesture.delegate = self
        view.addGestureRecognizer(panGesture)

        cameraLocationView = PlaceSelectionView(cameraManager: cameraManager, frame: CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.width * CameraUtils.cameraRatio))
        cameraLocationView?.cameraSource = cameraSource
        cameraMoodView = MoodSelectionView(cameraManager: cameraManager, frame: CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.width * CameraUtils.cameraRatio))

        guard let cameraLocationView = cameraLocationView, let cameraMoodView = cameraMoodView else { return }

        cameraLocationView.center = CGPoint(x: view.frame.width / 2, y: view.frame.height / 2)
        cameraLocationView.delegate = self
        cameraLocationView.isHidden = true
        view.addSubview(cameraLocationView)

        cameraMoodView.center = CGPoint(x: view.frame.width / 2, y: view.frame.height / 2)
        cameraMoodView.isHidden = true
        cameraMoodView.delegate = self
        view.addSubview(cameraMoodView)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }

    fileprivate func checkPermissions() {
        if !CameraPermissionManager.checkPermission(type: .camera) || !CameraPermissionManager.checkPermission(type: .microphone) {
            cameraAuthorizationView = CameraAuthorizationView(frame: CGRect.zero)
            cameraAuthorizationView?.delegate = self
            view.addSubview(cameraAuthorizationView!)
            cameraAuthorizationView?.snp.makeConstraints({ make in
                make.edges.equalTo(view)
            })
        } else {
            cameraCaptureView = CameraCaptureView(cameraManager: cameraManager, frame: CGRect.zero)
            cameraCaptureView?.delegate = self
            view.addSubview(cameraCaptureView!)
            cameraCaptureView?.snp.makeConstraints({ make in
                make.edges.equalTo(view)
            })
        }
    }

    fileprivate func closePreview() {
        cameraManager.resumeSession()
        closeVideoCroppingView()
        previewActive = false
        cameraPreviewView?.removeFromSuperview()
        cameraPreviewView = nil
        cameraMoodView?.dismiss()
        cameraLocationView?.dismiss()
    }

    fileprivate func showPreview(asset: CameraAsset) {
        cameraPreviewView?.removeFromSuperview()
        cameraPreviewView = nil
        cameraPreviewView = CameraPreviewView(asset: asset, cameraManager: cameraManager)
        guard let cameraPreviewView = cameraPreviewView else { return }
        cameraPreviewView.delegate = self
        cameraManager.pauseSession()
        previewActive = true
        view.addSubview(cameraPreviewView)
        cameraPreviewView.snp.makeConstraints { make in
            make.edges.equalTo(view)
        }
        cameraManager.pauseSession()
        
        guard let cameraLocationView = cameraLocationView, let cameraMoodView = cameraMoodView else { return }
        
        view.bringSubviewToFront(cameraLocationView)
        view.bringSubviewToFront(cameraMoodView)
        
        if let initialPlace = initialPlace {
            if let initialMood = initialMood {
                cameraPreviewView.cameraAsset?.selectedPlace = initialPlace
                cameraPreviewView.cameraAsset?.selectedMood = initialMood
                DispatchQueue.main.async {
                    KRProgressHUD.show()
                }
                if let locationSelectCallback = cameraSource?.locationSelectCallback {
                    locationSelectCallback(initialPlace) { filledLocation, error in
                        DispatchQueue.main.async {
                            KRProgressHUD.dismiss()
                        }
                        if error == nil {
                            cameraPreviewView.cameraAsset?.selectedPlace = filledLocation
                            cameraPreviewView.showPreviewOverlay()
                            cameraPreviewView.showStickersPicker()
                        }
                    }
                }
            } else {
                DispatchQueue.main.async {
                    KRProgressHUD.show()
                }
                if let locationSelectCallback = cameraSource?.locationSelectCallback {
                    locationSelectCallback(initialPlace) { filledLocation, error in
                        if error == nil {
                            DispatchQueue.main.async {
                                KRProgressHUD.dismiss()
                                cameraPreviewView.cameraAsset?.selectedPlace = filledLocation
                                cameraMoodView.display(placeName: filledLocation?.name ?? "", cameraAsset: cameraPreviewView.cameraAsset)
                            }
                        }
                    }
                }
            }
        } else if let previousAsset = cameraManager.finishedAssets.first {
            cameraPreviewView.cameraAsset?.selectedPlace = previousAsset.selectedPlace
            cameraPreviewView.cameraAsset?.selectedMood = previousAsset.selectedMood
            cameraPreviewView.showPreviewOverlay()
            cameraPreviewView.showStickersPicker()
        } else {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                cameraLocationView.display()
            }
        }
    }

    fileprivate func discardPost() {
        let alertController = UIAlertController(title: "discard_story_title".localized, message: "discard_story_message".localized, preferredStyle: .alert)

        let keepAction = UIAlertAction(title: "discard_story_keep".localized, style: .cancel )

        let discardStoryAction = UIAlertAction(title: "discard_story_multiple".localized, style: .destructive) { _ in
            self.closePreview()
            self.cameraManager.restartCamera()
            self.cameraManager.resumeSession()
            self.delegate?.cameraDiscardRating(multiple: true)
        }
        let discardPostActiom = UIAlertAction(title: "discard_story_single".localized, style: .destructive) { _ in
            KRProgressHUD.show()
            self.delegate?.cameraDiscardRating(multiple: false)
            self.cameraManager.pickAssetFromQueue { success in
                DispatchQueue.main.async {
                    if !success {
                        KRProgressHUD.dismiss()
                        self.closePreview()
                        if self.cameraManager.finishedAssets.count > 0 {
                            self.delegate?.cameraDidAcceptAssets(self.cameraManager.finishedAssets)
                            self.cameraManager.restartCamera()
                            self.dismiss(animated: true, completion: {
                                self.delegate?.cameraDidDismiss()
                            })
                        } else {
                            self.cameraManager.resumeSession()
                        }
                    }
                }
            }
        }
            
            
        alertController.addAction(keepAction)
        if cameraManager.assetsQueue.count > 0 || cameraManager.finishedAssets.count > 0 {
            alertController.addAction(discardStoryAction)
        }
        
        alertController.addAction(discardPostActiom)

        present(alertController, animated: true, completion: nil)
    }

    fileprivate func showVideoCroppingView(asset: CameraAsset) {
        showAlert(message: String(format: "video_too_long".localized, Int(CameraConfig.maxVideoDuration)))
        cameraVideoCroppingView = CameraVideoCroppingView(url: asset.url!, cameraManager: cameraManager)
        cameraVideoCroppingView?.delegate = self
        if let cameraVideoCroppingView = cameraVideoCroppingView {
            view.addSubview(cameraVideoCroppingView)
            cameraVideoCroppingView.snp.makeConstraints({ make in
                make.edges.equalTo(view)
            })
        }
    }

    fileprivate func closeVideoCroppingView() {
        cameraVideoCroppingView?.removeFromSuperview()
        cameraVideoCroppingView = nil
    }

    @objc fileprivate func handlePan(_ sender: UIPanGestureRecognizer) {
        let touchPoint = sender.location(in: view?.window)

        if sender.state == .began {
            initialTouchPoint = touchPoint
        } else if sender.state == .changed {
            if touchPoint.y - initialTouchPoint.y > 0 {
                view.frame = CGRect(x: 0, y: touchPoint.y - initialTouchPoint.y, width: view.frame.width, height: view.frame.size.height)
            }
        } else if sender.state == .ended || sender.state == .cancelled {
            if touchPoint.y - initialTouchPoint.y > view.frame.height / 3 {
                dismiss(animated: true, completion: nil)
            } else {
                UIView.animate(withDuration: 0.3, animations: {
                    self.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
                })
            }
        }
    }

    override open var prefersStatusBarHidden: Bool {
        if CameraUtils.hasSafeArea {
            return false
        } else {
            return true
        }
    }

    override open var preferredStatusBarStyle: UIStatusBarStyle {
        if CameraUtils.hasSafeArea {
            return .lightContent
        } else {
            return .default
        }
    }
    
    fileprivate func updateKeyboardFrameHeight(keyboardHeight: CGFloat) {
        cameraPreviewView?.updateKeyboardFrame(keyboardHeight: keyboardHeight)
        cameraLocationView?.updateKeyboardFrame(keyboardHeight: keyboardHeight)
    }
    
    fileprivate func updateKeyboardFrame(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            let keyboardFrame: CGRect = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
            let keyboardHeight = keyboardFrame.size.height
            updateKeyboardFrameHeight(keyboardHeight: keyboardHeight)
        }
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        updateKeyboardFrame(notification: notification)
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        updateKeyboardFrameHeight(keyboardHeight: 0)
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}

extension CameraViewController: UIGestureRecognizerDelegate {
   public func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        let touchPoint = gestureRecognizer.location(in: view?.window)
        return !previewActive && (touchPoint.y < 200)
    }
}

extension CameraViewController {
    @objc fileprivate func willResignActive(_ notification: Notification) {
        cameraManager.pauseSession()
    }

    @objc fileprivate func willEnterForeground(_ notification: Notification) {
        cameraManager.resumeSession()
    }
}

extension CameraViewController: CameraCaptureViewDelegate {
    
    func cameraCaptureGalleryAssetSelected(videoCount: Int?, imageCount: Int?, totalCount: Int?) {
        self.delegate?.cameraMultipleAssetsFromGallerySelected(videoCount: videoCount, imageCount: imageCount, totalCount: totalCount)
    }
    
    func cameraCaptureDidClose() {
        cameraManager.restartCamera()
        dismiss(animated: true, completion: nil)
    }
}

extension CameraViewController: CameraPreviewViewDelegate {
    
    public func cameraDidAddTextSticker(text: String) {
        self.delegate?.cameraDidAddTextSticker(stickerText: text)
    }
    
    
    public func cameraPreviewDidSelectSticker(sticker: Sticker) {
        self.delegate?.cameraDidSelectSticker(sticker: sticker)
    }
    
    
    public func cameraPreviewDidSelectDraw() {
        self.delegate?.cameraDidTapAction(name: "EditDraw")
    }
    
    
    public func cameraPreviewDidReceiveStickerSearch(text: String) {
        self.delegate?.cameraViewDidReceiveStickerSearch(text: text)
    }
    
    public func cameraPreviewReceivedError(_ error: String) {
        showAlert(message: error)
    }
    
    public func cameraPreviewDidClose() {
        discardPost()
    }
    
    public func cameraPreviewDidSelectLocation() {
        self.delegate?.cameraDidTapAction(name: "EditLocation")
        cameraPreviewView?.hidePreviewOverlay()
        cameraLocationView?.display()
    }
    
    public func cameraPreviewDidSelectMood() {
        self.delegate?.cameraDidTapAction(name: "EditRating")
        if let cameraAsset = cameraPreviewView?.cameraAsset {
            cameraPreviewView?.hidePreviewOverlay()
            cameraMoodView?.display(placeName: cameraAsset.selectedPlace?.name, cameraAsset: cameraAsset)
        }
    }
    
    public func cameraPreviewDidDownload() {
        showAlert(message: "saved_to_roll".localized)
    }
    
    public func cameraPreviewDidAccept(_ asset: CameraAsset, addons: [UIView]) {
        if let previewView = cameraPreviewView {
            cameraManager.acceptView(view: previewView, asset: asset, addons: addons) { _ in
                KRProgressHUD.show()
                self.cameraManager.pickAssetFromQueue { success in
                    KRProgressHUD.dismiss()

                    self.closePreview()
                    if !success {
                        self.delegate?.cameraDidAcceptAssets(self.cameraManager.finishedAssets)
                        self.cameraManager.restartCamera()
                        self.dismiss(animated: true, completion: {
                            self.delegate?.cameraDidDismiss()
                        })
                    }
                }
                
            }
        }
    }
}

extension CameraViewController: CameraAuthorizationViewDelegate {
    
    func authorizationPermissionGiven() {
        cameraAuthorizationView?.removeFromSuperview()
        checkPermissions()
    }
    
    func cameraAuthorizationGiven(permission: String) {
        self.delegate?.permissionsGrantedFor(type: "Camera", permission: permission)
    }
    
    func microphoneAuthorizationGiven(permission: String) {
        self.delegate?.permissionsGrantedFor(type: "Microphone", permission: permission)
    }
    
    func cameraAuthorizationClose() {
        dismiss(animated: true, completion: nil)
    }

}

extension CameraViewController: CameraManagerCapturingDelegate {
    public func cameraManagerCaptureDidStart() {
        guard let cameraCaptureView = cameraCaptureView else { return }

        cameraCaptureView.showRecordingTimer()
    }

    public func cameraManagerCaptureVideoTooShort() {
        showAlert(message: String(format: "video_too_short".localized, Int(CameraConfig.minVideoDuration)))
        guard let cameraCaptureView = cameraCaptureView else { return }

        cameraCaptureView.hideRecordingTimer()
    }

    public func cameraManagerCaptureDidCreateAsset(_ asset: CameraAsset) {
        if let mediaSelectedCallback = cameraSource?.mediaSelectedCallback {
            mediaSelectedCallback(asset)
        }
        if asset.exceededLength {
            showVideoCroppingView(asset: asset)
            return
        }
        showPreview(asset: asset)
        
        guard let cameraCaptureView = cameraCaptureView else { return }
        
        cameraCaptureView.hideRecordingTimer()
    }
    
}

extension CameraViewController: CameraVideoCroppingViewDelegate {
    func cameraVideoCroppingDidClose() {
        cameraPreviewDidClose()
    }

    func cameraVideoCroppingDidCreateAsset(_ asset: CameraAsset) {
        closeVideoCroppingView()
        showPreview(asset: asset)
    }
}

extension CameraViewController: PlaceSelectionViewDelegate {
    
    public func placeSelectedWith(placeDetail: CameraPlace) {
        cameraPreviewView?.cameraAsset?.selectedPlace = placeDetail
        cameraLocationView?.dismiss()
        cameraPreviewView?.loadStickers()
        if cameraPreviewView?.cameraAsset?.selectedMood != nil {
            cameraPreviewView?.showPreviewOverlay()
        } else {
            cameraMoodView?.display(placeName: cameraPreviewView?.cameraAsset?.selectedPlace?.name ?? "Unknown", cameraAsset: cameraPreviewView?.cameraAsset)
        }
    }

    public func placeSelectionOverlayClosed() {
        if cameraPreviewView?.cameraAsset?.selectedPlace != nil {
            cameraPreviewView?.showPreviewOverlay()
            cameraLocationView?.dismiss()
        } else {
            discardPost()
        }
    }

    public func showErrorToast(message: String) {
    }
}

extension CameraViewController: MoodSelectionViewDelegate {
    
    func moodSelectionOverlayClose() {
        if cameraPreviewView?.cameraAsset?.selectedMood != nil {
            cameraPreviewView?.showPreviewOverlay()
            cameraMoodView?.dismiss()
        } else {
            discardPost()
        }
    }
    
    func moodSelectLocation() {
        self.delegate?.cameraDidTapAction(name: "EditLocation")
        cameraMoodView?.dismiss()
        cameraLocationView?.display()
    }

    func moodSelected(moodStr: String, asset: CameraAsset?) {
        self.delegate?.cameraDidSelectRating(rating: moodStr, asset: asset)
        cameraPreviewView?.cameraAsset?.selectedMood = moodStr
        UserDefaults.standard.set("\(moodStr)", forKey: "moodSelected")
        cameraPreviewView?.showPreviewOverlay()
        cameraPreviewView?.showStickersPicker()
        cameraMoodView?.dismiss()
    }
}
