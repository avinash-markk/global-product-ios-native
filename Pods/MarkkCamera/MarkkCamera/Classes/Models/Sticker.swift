//
//  Sticker.swift
//  MarkkCamera
//
//  Created by Sebastian Sztemberg on 18/02/2019.
//

import UIKit

public struct TextStickerPosition {
    public var topLeft: [CGFloat]?
    public var height: CGFloat?
    public var width: CGFloat?
    public var rotation: CGFloat?
}

public struct TextSticker {
    public var text: String?
    public var fontSize: Int?
    public var position: TextStickerPosition?
    public var uniqueId: Int?
}

public struct Sticker {
    public var id: String
    public var name: String
    public var image: URL
    public var weight: Int
    public var searchTags: String
    public var weightGroup: [Int]?

    public init(id: String, name: String, image: URL, weight: Int, searchTags: String, weightGroup: [Int]?) {
        self.id = id
        self.name = name
        self.image = image
        self.weight = weight
        self.searchTags = searchTags
        self.weightGroup = weightGroup
    }
}
