//
//  CameraManager.swift
//  MarkkCamera
//
//  Created by Sebastian Sztemberg on 05/02/2019.
//

import UIKit
import AVFoundation
import Photos
import KRProgressHUD

public enum CameraAssetType {
    case video, photo
}

public struct CameraAsset {
    public var id: String
    public var type: CameraAssetType
    public var url: URL?
    public var image: UIImage?
    public var fromGallery: Bool = false
    public var exceededLength: Bool = false
    public var selectedMood: String?
    public var selectedPlace: CameraPlace?
    public var imageStickers: [Sticker]?
    public var textStickers: [TextSticker]?
    public var muted: Bool = false
    public var location: [Double]?
    public var createdTime: String?

    public init(type: CameraAssetType, url: URL?, image: UIImage?) {
        self.init(type: type, url: url, image: image, fromGallery: false, exceededLength: false, location: [], createdTime: nil)
    }

    public init(type: CameraAssetType, url: URL?, image: UIImage?, fromGallery: Bool, location: [Double]?, createdTime: String?) {
        self.init(type: type, url: url, image: image, fromGallery: fromGallery, exceededLength: false, location: location, createdTime: createdTime)
    }

    public init(type: CameraAssetType, url: URL?, image: UIImage?, fromGallery: Bool, exceededLength: Bool, location: [Double]?, createdTime: String?) {
        self.id = CameraUtils.getCurrentTimeStamp()
        self.type = type
        self.url = url
        self.image = image
        self.fromGallery = fromGallery
        self.exceededLength = exceededLength
        self.location = location
        self.createdTime = createdTime
    }
}

public enum CameraFlashMode: Int {
    case off, on, auto

    mutating func next() {
        self = CameraFlashMode(rawValue: rawValue + 1) ?? .off
    }
}

public typealias AssetCallback = (CameraAsset) -> Void

public protocol CameraManagerCapturingDelegate: AnyObject {
    func cameraManagerCaptureDidStart()
    func cameraManagerCaptureVideoTooShort()
    func cameraManagerCaptureDidCreateAsset(_ asset: CameraAsset)
}

public class CameraManager: NSObject {
    public weak var capturingDelegate: CameraManagerCapturingDelegate?

    public var videoCaptureLayer: AVCaptureVideoPreviewLayer?
    public var currentFlashMode: CameraFlashMode = .off
    public var assetsQueue = [PHAsset]()
    public var finishedAssets = [CameraAsset]()
    public var galleryInterval: Int = 24
    
    public var recordingStarted: Bool = false

    fileprivate var cameraAsset: CameraAsset?
    fileprivate var currentZoom: CGFloat = 0

    fileprivate var session: AVCaptureSession = AVCaptureSession()
    fileprivate var stillImageOutput: AVCapturePhotoOutput = AVCapturePhotoOutput()
    fileprivate var movieFileOutput: AVCaptureMovieFileOutput?
    fileprivate var captureDeviceInput: AVCaptureDeviceInput?

    public func initSession() {
        session.sessionPreset = CameraConfig.videoSessionPreset
        guard let backCamera =  AVCaptureDevice.default(for: .video) else {
            return
        }
        self.updateCameraSessionWith(cameraDevice: backCamera)
    }
    
    func updateCameraSessionWith(cameraDevice: AVCaptureDevice) {
        var input: AVCaptureDeviceInput!
        var error: NSError!
        for input in session.inputs {
            session.removeInput(input as AVCaptureInput)
        }
        
        do {
            input = try AVCaptureDeviceInput(device: cameraDevice)
        } catch let error1 as NSError {
            error = error1
            input = nil
            print(error!.localizedDescription)
        }
        
        if error == nil && session.canAddInput(input) {
            session.addInput(input)
            captureDeviceInput = input
            if session.canAddOutput(stillImageOutput) {
                session.addOutput(stillImageOutput)
                videoCaptureLayer = AVCaptureVideoPreviewLayer(session: session)
                videoCaptureLayer!.videoGravity = .resizeAspect
                videoCaptureLayer!.connection?.videoOrientation = .portrait
            }
        } else {
            session.addInput(input)
            captureDeviceInput = input
        }
        
        let audioDevice = AVCaptureDevice.default(for: AVMediaType.audio)
        if let audioDevice = audioDevice {
            do {
                let audioInput = try AVCaptureDeviceInput(device: audioDevice)
                if session.canAddInput(audioInput) {
                    session.addInput(audioInput)
                }
            } catch {
                print("Unable to add audio device to the recording.")
            }
        }
        
    }

    // MARK: - Setup capturing

    public func startSession() {
        session.startRunning()
    }

    public func pauseSession() {
        videoCaptureLayer?.connection?.isEnabled = false
        videoCaptureLayer?.isHidden = true
        session.stopRunning()
    }

    public func resumeSession() {
        videoCaptureLayer?.connection?.isEnabled = true
        videoCaptureLayer?.isHidden = false
        session.startRunning()
    }

    public func resetSession() {
        currentZoom = 0
        updateVideoZoom(distance: 1)
    }
    
    public func restartCamera() {
        resetSession()
        assetsQueue = []
        finishedAssets = []
    }

    public func updateCameraZoom(distance: CGFloat) {
        guard let device = captureDeviceInput?.device, (try? device.lockForConfiguration()) != nil else { return }

        let pinchFactor: CGFloat = 20.0

        let maxZoomFactor = min(device.activeFormat.videoMaxZoomFactor, CameraConfig.maxCameraZoom)
        let desiredZoomFactor = device.videoZoomFactor + atan2(distance, pinchFactor)
        let zoom = max(1.0, min(desiredZoomFactor, maxZoomFactor))
        currentZoom = zoom - 1

        device.videoZoomFactor = zoom

        device.unlockForConfiguration()
    }

    public func updateVideoZoom(distance: CGFloat) {
        let gap: CGFloat = 20
        let maxDistance: CGFloat = 300
        updateZoom(distance: distance, gap: gap, maxDistance: maxDistance)
    }

    fileprivate func updateZoom(distance: CGFloat, gap: CGFloat, maxDistance: CGFloat) {
        guard let device = captureDeviceInput?.device, (try? device.lockForConfiguration()) != nil else { return }

        let fixedDistance = distance < gap ? 0 : min(distance - gap, maxDistance)
        let maxZoomFactor = min(device.activeFormat.videoMaxZoomFactor, CameraConfig.maxCameraZoom)
        let percent = fixedDistance / maxDistance
        let zoom = percent * maxZoomFactor
        device.videoZoomFactor = min(currentZoom + zoom + 1.0, maxZoomFactor)

        device.unlockForConfiguration()
    }

    fileprivate func setupFlash(flashMode: CameraFlashMode) {
        guard let device = captureDeviceInput?.device, (try? device.lockForConfiguration()) != nil else { return }

        let torchMode = AVCaptureDevice.TorchMode(rawValue: flashMode.rawValue)!
        if device.isTorchModeSupported(torchMode) {
            device.torchMode = torchMode
        }
        device.unlockForConfiguration()
    }
    
    func rotateCamera(mode: CameraRotateMode) {
        switch mode {
        case .back:
            guard let backCameraDevice = AVCaptureDevice.default(.builtInWideAngleCamera, for: .video, position: .back) else {
                return
            }
            self.updateCameraSessionWith(cameraDevice: backCameraDevice)
            
        case .front:
            guard let frontCameraDevice = AVCaptureDevice.default(.builtInWideAngleCamera, for: .video, position: .front) else {
                return
            }
            self.updateCameraSessionWith(cameraDevice: frontCameraDevice)
        }
    }

    // MARK: - Capturing

    public func startRecording() {
        recordingStarted = true
        if movieFileOutput == nil {
            initMovieFileCapture()
        }

        guard let movieFileOutput = movieFileOutput, let fileOutput = CameraFileManager.filePath(type: .video), !movieFileOutput.isRecording else {
            return
        }

        movieFileOutput.maxRecordedDuration = CMTimeMakeWithSeconds(CameraConfig.maxVideoDuration, preferredTimescale: 1000)

        let connection = movieFileOutput.connection(with: .video)
        connection?.videoOrientation = .portrait

        setupFlash(flashMode: currentFlashMode)
        movieFileOutput.startRecording(to: fileOutput, recordingDelegate: self)
    }

    public func stopRecording() {
        recordingStarted = false
        guard let movieFileOutput = movieFileOutput else {
            return
        }
        movieFileOutput.stopRecording()
        setupFlash(flashMode: .off)
    }
    
    public func isRecording() -> Bool {
        return movieFileOutput?.isRecording ?? false
    }
    
    public func takePhoto() {
        let settings = AVCapturePhotoSettings()
        settings.flashMode = AVCaptureDevice.FlashMode(rawValue: currentFlashMode.rawValue)!
        
        let previewPixelType = settings.availablePreviewPhotoPixelFormatTypes.first!
        let previewFormat = [
            kCVPixelBufferPixelFormatTypeKey as String: previewPixelType,
            kCVPixelBufferWidthKey as String: 160,
            kCVPixelBufferHeightKey as String: 160
        ]
        settings.previewPhotoFormat = previewFormat
        
        stillImageOutput.capturePhoto(with: settings, delegate: self)
    }
    
    // MARK: - AVCaptureMovieFileOutput
    
    fileprivate func initMovieFileCapture() {
        let fileOutput = AVCaptureMovieFileOutput()
        if session.canAddOutput(fileOutput) {
            session.addOutput(fileOutput)
            movieFileOutput = fileOutput
        }
    }
    
    // MARK: - Gallery
    
    public func selectGalleryAssets(_ assets: [PHAsset]) {
        assetsQueue = assets
        pickAssetFromQueue { success in
            
        }
    }
    
    public func pickAssetFromQueue(callback: @escaping SuccessCallback) {
        if let asset = assetsQueue.first {
            self.selectGalleryAsset(asset) { success in
                self.assetsQueue.remove(at: 0)
                callback(true)
            }
        } else {
            callback(false)
        }
    }

    
    func getLocationFrom(asset: PHAsset) -> [Double]? {
        if let location = asset.location {
            let latitude = location.coordinate.latitude
            let longitude = location.coordinate.longitude
            return [latitude, longitude]
        }
        return nil
    }
    
    func getCreatedTimeFrom(asset: PHAsset) -> String? {
        if let date = asset.creationDate {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = "dd-MM-yyyy hh:mm:ss a"
        let timeStampString = dateFormatter.string(from: date)
        return timeStampString
        }
        return nil
    }
    
    public func selectGalleryAsset(_ asset: PHAsset, callback: @escaping SuccessCallback) {
        KRProgressHUD.show(withMessage: "loading".localized)

        if asset.mediaType == .video {
            let options = PHVideoRequestOptions()
            options.deliveryMode = PHVideoRequestOptionsDeliveryMode.highQualityFormat
            options.version = PHVideoRequestOptionsVersion.current

            PHImageManager.default().requestAVAsset(forVideo: asset, options: options) { (videoAsset, _, _) in
                guard let videoAsset = videoAsset else {
                    KRProgressHUD.dismiss()
                    return
                }
                // Support slow-mo
                if videoAsset.isKind(of: AVComposition.self) {

                    guard let avCompositionAsset = videoAsset as? AVComposition else { return }
                    if avCompositionAsset.tracks.count > 1 {
                        let exporter = AVAssetExportSession(asset: avCompositionAsset, presetName: AVAssetExportPresetPassthrough)
                        exporter?.outputURL = CameraFileManager.filePath(type: .video)
                        exporter?.outputFileType = .mp4
                        exporter?.shouldOptimizeForNetworkUse = false
                        exporter?.exportAsynchronously {
                            DispatchQueue.main.async {
                                KRProgressHUD.dismiss()
                                callback(true)
                                let url = exporter!.outputURL
                                self.capturingDelegate?.cameraManagerCaptureDidCreateAsset(CameraAsset(type: .video, url: url, image: nil, fromGallery: true, exceededLength: videoAsset.duration.seconds > CameraConfig.maxVideoDuration, location: self.getLocationFrom(asset: asset), createdTime: self.getCreatedTimeFrom(asset: asset)))
                            }
                        }
                    } else {
                        callback(false)
                    }
                } else {
                    guard let url = (videoAsset as? AVURLAsset)?.url else {
                        KRProgressHUD.dismiss()
                        callback(false)
                        return
                    }
                    DispatchQueue.main.async {
                        callback(true)
                        KRProgressHUD.dismiss()
                        self.capturingDelegate?.cameraManagerCaptureDidCreateAsset(CameraAsset(type: .video, url: url, image: nil, fromGallery: true, exceededLength:
                            videoAsset.duration.seconds > CameraConfig.maxVideoDuration, location: self.getLocationFrom(asset: asset), createdTime: self.getCreatedTimeFrom(asset: asset)))
                    }
                }
            }
        } else if asset.mediaType == .image {
            let options = PHImageRequestOptions()
            options.isSynchronous = true
            options.resizeMode = .exact
            options.version = PHImageRequestOptionsVersion.current
            options.deliveryMode = PHImageRequestOptionsDeliveryMode.highQualityFormat

            PHImageManager.default().requestImage(for: asset, targetSize: CGSize(width: 720, height: 1280), contentMode: .aspectFill, options: options) { (image, _) in
                DispatchQueue.main.async {
                    callback(true)
                    KRProgressHUD.dismiss()
                    self.capturingDelegate?.cameraManagerCaptureDidCreateAsset(CameraAsset(type: .photo, url: nil, image: image, fromGallery: true, location: self.getLocationFrom(asset: asset), createdTime: self.getCreatedTimeFrom(asset: asset)))
                }
            }
        }
    }

    // MARK: - Post

    private func orientationFromTransform(transform: CGAffineTransform) -> (orientation: UIImage.Orientation, isPortrait: Bool) {
        var assetOrientation = UIImage.Orientation.up
        var isPortrait = false
        if transform.a == 0 && transform.b == 1.0 && transform.c == -1.0 && transform.d == 0 {
            assetOrientation = .right
            isPortrait = true
        } else if transform.a == 0 && transform.b == -1.0 && transform.c == 1.0 && transform.d == 0 {
            assetOrientation = .left
            isPortrait = true
        } else if transform.a == 1.0 && transform.b == 0 && transform.c == 0 && transform.d == 1.0 {
            assetOrientation = .up
        } else if transform.a == -1.0 && transform.b == 0 && transform.c == 0 && transform.d == -1.0 {
            assetOrientation = .down
        }
        return (assetOrientation, isPortrait)
    }

    private func videoCompositionInstructionForTrack(track: AVCompositionTrack, asset: AVAsset) -> AVMutableVideoCompositionLayerInstruction {
        let instruction = AVMutableVideoCompositionLayerInstruction(assetTrack: track)
        let assetTrack = asset.tracks(withMediaType: AVMediaType.video)[0]

        let transform = assetTrack.preferredTransform
        let assetInfo = orientationFromTransform(transform: transform)

        var scaleToFitRatio = UIScreen.main.bounds.width / assetTrack.naturalSize.width
        if assetInfo.isPortrait {
            scaleToFitRatio = UIScreen.main.bounds.width / assetTrack.naturalSize.height
            let scaleFactor = CGAffineTransform(scaleX: scaleToFitRatio, y: scaleToFitRatio)
            instruction.setTransform(assetTrack.preferredTransform.concatenating(scaleFactor),
                                     at: CMTime.zero)
        } else {
            let scaleFactor = CGAffineTransform(scaleX: scaleToFitRatio, y: scaleToFitRatio)
            var concat = assetTrack.preferredTransform.concatenating(scaleFactor).concatenating(CGAffineTransform(translationX: 0, y: UIScreen.main.bounds.width / 2))
            if assetInfo.orientation == .down {
                let fixUpsideDown = CGAffineTransform(rotationAngle: CGFloat(Double.pi))
                let windowBounds = UIScreen.main.bounds
                let yFix = assetTrack.naturalSize.height + windowBounds.height
                let centerFix = CGAffineTransform(translationX: assetTrack.naturalSize.width, y: yFix)
                concat = fixUpsideDown.concatenating(centerFix).concatenating(scaleFactor)
            }
            instruction.setTransform(concat, at: CMTime.zero)
        }

        return instruction
    }

    public func generateFinalAsset(previewView: CameraPreviewView, asset: CameraAsset, addons: [UIView], callback: @escaping AssetCallback) {
        KRProgressHUD.show(withMessage: "loading".localized)
        if let preview = previewView.previewView, asset.type == .photo {
            UIGraphicsBeginImageContextWithOptions(preview.bounds.size, true, 0)
            preview.drawHierarchy(in: preview.bounds, afterScreenUpdates: true)
            addons.forEach { $0.drawHierarchy(in: preview.bounds, afterScreenUpdates: true) }
            let image = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            var newAsset = asset
            newAsset.image = image
            DispatchQueue.main.async {
                KRProgressHUD.dismiss()
                callback(newAsset)
            }
        } else if asset.type == .video, let videoUrl = asset.url {
            let videoAsset = AVAsset(url: videoUrl)

            let mixComposition = AVMutableComposition()
            var compositionAudioTrack: AVMutableCompositionTrack?
            var clipAudioTrack: AVAssetTrack?

            let compositionVideoTrack = mixComposition.addMutableTrack(withMediaType: AVMediaType.video, preferredTrackID: Int32(kCMPersistentTrackID_Invalid))

            if videoAsset.tracks(withMediaType: AVMediaType.audio).count > 0 && !asset.muted {
                compositionAudioTrack = mixComposition.addMutableTrack(withMediaType: AVMediaType.audio, preferredTrackID: Int32(kCMPersistentTrackID_Invalid))
                clipAudioTrack = videoAsset.tracks(withMediaType: AVMediaType.audio)[0]
            }
            let clipVideoTrack = videoAsset.tracks(withMediaType: AVMediaType.video)[0]
            do {
                try compositionVideoTrack?.insertTimeRange(CMTimeRangeMake(start: CMTime.zero, duration: videoAsset.duration), of: clipVideoTrack, at: CMTime.zero)
                if let compositionAudioTrack = compositionAudioTrack, let clipAudioTrack = clipAudioTrack {
                    try compositionAudioTrack.insertTimeRange(CMTimeRangeMake(start: CMTime.zero, duration: videoAsset.duration), of: clipAudioTrack, at: CMTime.zero)
                }
            } catch {
                print(error.localizedDescription)
                KRProgressHUD.dismiss()
            }

            let videoSize = CGSize(width: min(clipVideoTrack.naturalSize.height, clipVideoTrack.naturalSize.width), height: max(clipVideoTrack.naturalSize.height, clipVideoTrack.naturalSize.width))

            let parentLayer = CALayer()
            let videoLayer = CALayer()
            parentLayer.frame = CGRect(x: 0, y: 0, width: videoSize.width, height: videoSize.height)
            videoLayer.frame = CGRect(x: 0, y: 0, width: videoSize.width, height: videoSize.height)
            parentLayer.addSublayer(videoLayer)

            addons.forEach { view in
                UIGraphicsBeginImageContextWithOptions(view.frame.size, false, UIScreen.main.scale)
                if let context: CGContext = UIGraphicsGetCurrentContext() {
                    view.layer.render(in: context)
                    let screenshot = UIGraphicsGetImageFromCurrentImageContext()
                    UIGraphicsEndImageContext()
                    
                    let layer = CALayer()
                    layer.contents = screenshot?.cgImage
                    layer.frame = view.frame
                    layer.transform = CATransform3DMakeScale(videoSize.width/view.frame.width, videoSize.height/view.frame.height, 1)
                    layer.frame = CGRect(x: 0, y: 0, width: videoSize.width, height: videoSize.height)
                    parentLayer.addSublayer(layer)
                }
            }

            let videoComp = AVMutableVideoComposition()
            videoComp.renderSize = videoSize
            videoComp.frameDuration = CMTimeMake(value: 1, timescale: 30)
            videoComp.animationTool = AVVideoCompositionCoreAnimationTool(postProcessingAsVideoLayer: videoLayer, in: parentLayer)

            let instruction = AVMutableVideoCompositionInstruction()
            instruction.timeRange = CMTimeRangeMake(start: CMTime.zero, duration: mixComposition.duration)
            _ = mixComposition.tracks(withMediaType: AVMediaType.video)[0] as AVAssetTrack

            let layerInstruction = AVMutableVideoCompositionLayerInstruction(assetTrack: compositionVideoTrack!)
            layerInstruction.setTransform(clipVideoTrack.preferredTransform, at: CMTime.zero)

            instruction.layerInstructions = [layerInstruction]
            videoComp.instructions = [instruction]

            let documentDirectory = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
            let url = URL(fileURLWithPath: documentDirectory).appendingPathComponent("output-\(CameraUtils.getCurrentTimeStamp()).mov")

            let exporter = AVAssetExportSession(asset: mixComposition, presetName: AVAssetExportPreset1280x720)
            exporter?.outputURL = url
            exporter?.outputFileType = AVFileType.mov
            exporter?.shouldOptimizeForNetworkUse = true
            exporter?.videoComposition = videoComp

            exporter?.exportAsynchronously {
                DispatchQueue.main.async {
                    KRProgressHUD.dismiss()
                    if exporter?.status == .completed {
                        var newAsset = asset
                        newAsset.url = exporter?.outputURL
                        callback(newAsset)
                    }
                }
            }
        } else {
            KRProgressHUD.dismiss()
        }
    }

    public func acceptView(view: CameraPreviewView, asset: CameraAsset, addons: [UIView], callback: @escaping SuccessCallback) {
        generateFinalAsset(previewView: view, asset: asset, addons: addons) { asset in
            self.finishedAssets.append(asset)
            callback(true)
        }
    }
}

extension CameraManager: AVCaptureFileOutputRecordingDelegate {
    public func fileOutput(_ output: AVCaptureFileOutput, didStartRecordingTo fileURL: URL, from connections: [AVCaptureConnection]) {
        DispatchQueue.main.async {
        self.capturingDelegate?.cameraManagerCaptureDidStart()
        }
    }

    public func fileOutput(_ output: AVCaptureFileOutput, didFinishRecordingTo outputFileURL: URL, from connections: [AVCaptureConnection], error: Error?) {
        let maximumDurationReached: Bool = (error as? AVError)?.code == AVError.maximumDurationReached

        // Take photo if video shorter than 1 second
        if output.recordedDuration.seconds < 1 && !maximumDurationReached {
            takePhoto()
        } else if output.recordedDuration.seconds < CameraConfig.minVideoDuration {
            capturingDelegate?.cameraManagerCaptureVideoTooShort()
            resetSession()
        } else if error != nil && !maximumDurationReached {
            // Handle error
            return
        } else {
            capturingDelegate?.cameraManagerCaptureDidCreateAsset(CameraAsset(type: .video, url: outputFileURL, image: nil))
            resetSession()
        }
    }
}

extension CameraManager: AVCapturePhotoCaptureDelegate {
    public func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photoSampleBuffer: CMSampleBuffer?, previewPhoto previewPhotoSampleBuffer: CMSampleBuffer?, resolvedSettings: AVCaptureResolvedPhotoSettings, bracketSettings: AVCaptureBracketedStillImageSettings?, error: Error?) {
        if let error = error {
            print(error.localizedDescription)
        }

        if let sampleBuffer = photoSampleBuffer, let previewBuffer = previewPhotoSampleBuffer, let dataImage = AVCapturePhotoOutput.jpegPhotoDataRepresentation(forJPEGSampleBuffer: sampleBuffer, previewPhotoSampleBuffer: previewBuffer) {
            if let image = UIImage(data: dataImage) {
                capturingDelegate?.cameraManagerCaptureDidCreateAsset(CameraAsset(type: .photo, url: nil, image: image))
                resetSession()
            }
        }
    }
}
