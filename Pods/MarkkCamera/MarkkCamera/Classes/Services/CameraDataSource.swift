//
//  CameraDataSource.swift
//  MarkkCamera
//
//  Created by Sebastian Sztemberg on 18/02/2019.
//

import UIKit

public typealias SuccessCallback = (Bool) -> Void
public typealias LocationsSourceCallback = ([CameraPlace]?, CameraError?) -> Void
public typealias LocationSourceCallback = (CameraPlace?, CameraError?) -> Void


public class CameraDataSource: NSObject {
    public var locationsCallback: ((_ searchQuery: String, @escaping LocationsSourceCallback) -> Void)?
    public var nearbyLocationsCallback: ((@escaping LocationsSourceCallback) -> Void)?
    public var locationSelectCallback: ((_ place: CameraPlace, @escaping LocationSourceCallback) -> Void)?
    public var mediaSelectedCallback: ((_ asset: CameraAsset) -> Void)?
}
