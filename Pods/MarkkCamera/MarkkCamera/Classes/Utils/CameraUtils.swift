//
//  CameraUtils.swift
//  MarkkCamera
//
//  Created by Sebastian Sztemberg on 02/02/2019.
//

import UIKit

class CameraUtils {
    public static let cameraRatio: CGFloat = 16.0 / 9.0

    public static func getCurrentTimeStamp() -> String {
        let timestamp = NSDate().timeIntervalSince1970
        let date = Date(timeIntervalSince1970: timestamp)
        let dateFormatter = DateFormatter()
        dateFormatter.locale = NSLocale.current

        dateFormatter.dateFormat = "dd-MM-yyyy hh:mm:ss a"
        let timeStampString = dateFormatter.string(from: date)
        return timeStampString
    }

    public static var hasSafeArea: Bool {
        guard #available(iOS 11.0, *), let topPadding = UIApplication.shared.keyWindow?.safeAreaInsets.top, topPadding > 24 else {
            return false
        }
        return true
    }
    
    public static func registerFonts() {
        let appBundle = Bundle(for: CameraViewController.self)
        guard let url = appBundle.url(forResource: "MarkkCameraBundle", withExtension: "bundle"), let bundle = Bundle(url: url) else { return }
        
        UIFont.registerFont(bundle: bundle, fontName: "Inter-Regular", fontExtension: "ttf")
        UIFont.registerFont(bundle: bundle, fontName: "Inter-Bold", fontExtension: "ttf")
        UIFont.registerFont(bundle: bundle, fontName: "Montserrat-Black", fontExtension: "ttf")
    }
    
    struct CameraColors {
        static let blackDark100 = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 1.0)
        static let gray200 = UIColor(red: 31 / 255, green: 31 / 255, blue: 31 / 255, alpha: 1.0)
        static let green100 = UIColor(red: 90 / 255, green: 194 / 255, blue: 154 / 255, alpha: 1.0)
    }
    
}
