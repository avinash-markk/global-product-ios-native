//
//  CameraPermissionManager.swift
//  Sminq
//
//  Created by Sebastian Sztemberg on 30/01/2019.
//  Copyright © 2019 Suraj Sagare. All rights reserved.
//

import UIKit
import Photos

enum CameraPermissionsType {
    case camera, microphone, gallery
}

typealias PermissionCallback = (Bool) -> Void

class CameraPermissionManager: NSObject {
    public static func checkPermission(type: CameraPermissionsType) -> Bool {
        switch type {
        case .gallery:
            return PHPhotoLibrary.authorizationStatus() == .authorized
        case .camera:
            return AVCaptureDevice.authorizationStatus(for: AVMediaType.video) == .authorized
        case .microphone:
            return AVAudioSession.sharedInstance().recordPermission == .granted
        }
    }
    public static func openPermissionSettings() {
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(URL(string: UIApplication.openSettingsURLString)!)
        }
    }
    public static func requestPermission(type: CameraPermissionsType, callback: @escaping PermissionCallback) {
        switch type {
        case .gallery:
            requestGalleryPermission(callback: callback)
        case .camera:
            requestCameraPermission(callback: callback)
        case .microphone:
            requestMicrophonePermission(callback: callback)
        }
    }
    fileprivate static func requestGalleryPermission(callback: @escaping PermissionCallback) {
        switch PHPhotoLibrary.authorizationStatus() {
        case .authorized:
            callback(true)
        case .notDetermined:
            PHPhotoLibrary.requestAuthorization({ (granted) in
                DispatchQueue.main.async {
                    callback(granted == .authorized)
                }
            })
        case .denied, .restricted:
            callback(false)
            openPermissionSettings()
        }
    }
    fileprivate static func requestCameraPermission(callback: @escaping PermissionCallback) {
        switch AVCaptureDevice.authorizationStatus(for: AVMediaType.video) {
        case .authorized:
            callback(true)
        case .notDetermined:
            AVCaptureDevice.requestAccess(for: AVMediaType.video, completionHandler: { granted in
                DispatchQueue.main.async {
                    callback(granted)
                }
            })
        case .denied, .restricted:
            callback(false)
            openPermissionSettings()
        }
    }

    fileprivate static func requestMicrophonePermission(callback: @escaping PermissionCallback) {
        switch AVAudioSession.sharedInstance().recordPermission {
        case .granted:
            callback(true)
        case .undetermined:
            AVAudioSession.sharedInstance().requestRecordPermission({ granted in
                DispatchQueue.main.async {
                    callback(granted)
                }
            })
        case .denied:
            callback(false)
            openPermissionSettings()
        }
    }
}
