//
//  CameraFileManager.swift
//  Sminq
//
//  Created by Sebastian Sztemberg on 25/01/2019.
//  Copyright © 2019 Suraj Sagare. All rights reserved.
//

import UIKit

public enum CameraOutputType {
    case video
    case photo
}

class CameraFileManager: NSObject {
    public static func filePath(type: CameraOutputType) -> URL? {
        let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        let dataPath = documentsDirectory.appendingPathComponent(CameraConfig.fileOutputDir)

        do {
            try FileManager.default.createDirectory(atPath: dataPath.relativePath, withIntermediateDirectories: true, attributes: nil)

            let fileName = type == .video ? "mov" : "png"
            let outputUrl = URL(fileURLWithPath: "\(dataPath.relativePath)/output.\(fileName)")
            try? FileManager.default.removeItem(at: outputUrl)

            return outputUrl
        } catch let error as NSError {
            return nil
        }
    }
}
