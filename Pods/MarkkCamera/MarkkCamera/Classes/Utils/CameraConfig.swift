//
//  CameraConfig.swift
//  Sminq
//
//  Created by Sebastian Sztemberg on 24/01/2019.
//  Copyright © 2019 Suraj Sagare. All rights reserved.
//

import UIKit
import AVFoundation

class CameraConfig: NSObject {
    public static let minVideoDuration: Double = 5
    public static let maxVideoDuration: Double = 15
    public static let maxCameraZoom: CGFloat = 10
    public static let videoSessionPreset = AVCaptureSession.Preset.hd1280x720

    public static let fileOutputDir = "Camera"
}
