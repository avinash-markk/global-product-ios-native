//
//  CGPoint+Camera.swift
//  MarkkCamera
//
//  Created by Sebastian Sztemberg on 27/02/2019.
//

import UIKit

extension CGPoint {
    func distance(from rect: CGRect) -> CGFloat {
        let dx = max(rect.minX - x, x - rect.maxX, 0)
        let dy = max(rect.minY - y, y - rect.maxY, 0)
        return dx * dy == 0 ? max(dx, dy) : hypot(dx, dy)
    }
}
