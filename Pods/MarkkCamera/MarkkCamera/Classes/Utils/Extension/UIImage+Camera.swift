//
//  UIImage+Camera.swift
//  MarkkCamera
//
//  Created by Sebastian Sztemberg on 31/01/2019.
//

import UIKit

public extension UIImage {
    public static func bundledImage(named: String) -> UIImage? {
       let bundle = Bundle(for: CameraViewController.self)
        if let url = bundle.url(forResource: "MarkkCameraBundle", withExtension: "bundle"), let image = UIImage(named: named, in: Bundle(url: url), compatibleWith: nil) {
            return image
        }
        return nil
    }
}
