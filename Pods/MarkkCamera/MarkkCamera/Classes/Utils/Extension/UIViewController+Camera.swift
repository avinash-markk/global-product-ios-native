//
//  UIViewController+Camera.swift
//  MarkkCamera
//
//  Created by Sebastian Sztemberg on 02/02/2019.
//

import UIKit

public extension UIViewController {
    func showAlert(message: String) {
        DispatchQueue.main.async {
            let alertView = UIAlertController(title: "Alert", message: message, preferredStyle: .alert)
            alertView.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alertView, animated: true, completion: nil)
        }
    }
}
