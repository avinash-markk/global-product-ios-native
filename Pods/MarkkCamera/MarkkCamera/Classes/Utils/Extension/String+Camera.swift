//
//  String+Camera.swift
//  MarkkCamera
//
//  Created by Sebastian Sztemberg on 25/02/2019.
//

import UIKit

extension String {
    var localized: String {
        let bundle = Bundle(for: CameraViewController.self)
        if let url = bundle.url(forResource: "MarkkCameraLocalizedBundle", withExtension: "bundle"), let localizedBundle = Bundle(url: url) {
            return NSLocalizedString(self, tableName: "Localizable", bundle: localizedBundle, value: "**\(self)**", comment: "")
        }
        return NSLocalizedString(self, comment: "")
    }
    init(duration: Double) {
        let min = Int(duration / 60)
        let sec = Int(duration.truncatingRemainder(dividingBy: 60))
        self = String(format: "%2d:%02d", min, sec)
    }
}
