# MarkkCamera

[![CI Status](https://img.shields.io/travis/Sebastian Sztemberg/MarkkCamera.svg?style=flat)](https://travis-ci.org/Sebastian Sztemberg/MarkkCamera)
[![Version](https://img.shields.io/cocoapods/v/MarkkCamera.svg?style=flat)](https://cocoapods.org/pods/MarkkCamera)
[![License](https://img.shields.io/cocoapods/l/MarkkCamera.svg?style=flat)](https://cocoapods.org/pods/MarkkCamera)
[![Platform](https://img.shields.io/cocoapods/p/MarkkCamera.svg?style=flat)](https://cocoapods.org/pods/MarkkCamera)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

MarkkCamera is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'MarkkCamera'
```

## Author

Sebastian Sztemberg, sztembus@gmail.com

## License

MarkkCamera is available under the MIT license. See the LICENSE file for more info.
