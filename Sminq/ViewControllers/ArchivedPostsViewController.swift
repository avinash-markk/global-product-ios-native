//
//  ArchivedPostsViewController.swift
//  Sminq
//
//  Created by Pushkar Deshmukh on 05/10/18.
//  Copyright © 2018 Suraj Sagare. All rights reserved.
//

import UIKit
import Gloss
import SVProgressHUD
import Firebase

class ArchivedPostsViewController: UIViewController {
    
    @IBOutlet weak var postsCollectionView: UICollectionView!
    @IBOutlet weak var postsCollectionViewFlowLayout: UICollectionViewFlowLayout!
    let errorView = ErrorView()
    
    var emptyStateUIView = EmptyState()
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        
        let attributes: [NSAttributedStringKey: Any] = [
            NSAttributedStringKey.foregroundColor: AppConstants.SminqColors.grey,
            NSAttributedStringKey.font: UIFont(name: AppConstants.Font.primaryRegular, size: 12.0) as Any
        ]
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh", attributes: attributes)
        refreshControl.addTarget(self, action:
            #selector(handleRefresh),
                                 for: UIControlEvents.valueChanged)
        refreshControl.tintColor = AppConstants.SminqColors.grey
        return refreshControl
    }()
    
    var testArray: [String] = ["test1", "test2", "test3", "test4", "test5"]
    
    var placesStories = [PlacesStoriesData]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        self.initUI()
        
        self.setupNavigationBar()
        self.getArchievedPosts()
        self.errorViewSetup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
        setNeedsStatusBarAppearanceUpdate()
        showTabBar()
    }
    
    override func viewDidDisappear(_ animated: Bool){
        super.viewDidDisappear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.isHidden = false
    }
    
    override public var prefersStatusBarHidden: Bool {
        return false
    }
    
    override public var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: Private functions
    
    func errorViewSetup() {
        // TODO: Instead of using '56' calculate two label's height dynamically
        errorView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 8 + 56)
        errorView.setStackViewTopConstraint(top: 8)
        self.view.addSubview(errorView)
        errorView.isHidden = true
        errorView.delegate = self
    }
    
    func initUI() {
        self.view.backgroundColor = AppConstants.SminqColors.blackDark100
        
        self.postsCollectionView.backgroundColor = AppConstants.SminqColors.blackDark100
        self.postsCollectionView.delegate = self
        self.postsCollectionView.dataSource = self
        
        self.postsCollectionView.register(UINib(nibName: "ArchivedPostCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ArchivedPostCollectionViewCell")
        self.postsCollectionView.addSubview(self.refreshControl)
        
        self.postsCollectionViewFlowLayout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        self.postsCollectionViewFlowLayout.minimumLineSpacing = 0
        self.postsCollectionViewFlowLayout.minimumInteritemSpacing = 0
        
        self.postsCollectionView.backgroundView = UIView()
        
        self.emptyStateViewSetup()
    }
    
    func emptyStateViewSetup() {
        self.emptyStateUIView.isHidden = true
        self.emptyStateUIView.frame = self.view.bounds
        self.emptyStateUIView.autoresizingMask = [.flexibleWidth, .flexibleHeight] // So that subview matches parent's height width
        self.emptyStateUIView.headingLabel.text = "Get started!"
        self.emptyStateUIView.headingLabel.textColor = AppConstants.SminqColors.white100
        self.emptyStateUIView.headingLabel.font = UIFont(name: AppConstants.Font.primaryRegular, size: 24)
        self.emptyStateUIView.subHeadingLabel.text = "Once you start adding live ratings, \n you'll be able to view them here."
        self.emptyStateUIView.subHeadingLabel.textColor = AppConstants.SminqColors.white100
        self.emptyStateUIView.subHeadingLabel.font = UIFont(name: AppConstants.Font.primaryRegular, size: 16)
        self.emptyStateUIView.setContentViewBackgroundColor(color: AppConstants.SminqColors.blackDark100)
        
        self.view.insertSubview(self.emptyStateUIView, belowSubview: self.postsCollectionView)
    }
    
    func setupNavigationBar() {
        // Back Button
        let backButton = UIButton(frame: CGRect(x: 0, y: 0, width: 35, height: 35))
        let upArrow = UIImage(named: "iconBack24White100")?.withRenderingMode(.alwaysTemplate)
        backButton.setImage(upArrow, for: UIControlState())
        backButton.tintColor = UIColor.white
        backButton.imageEdgeInsets = UIEdgeInsetsMake(0, -25, 0, 2)
        backButton.addTarget(self, action: #selector(tappedBackButton), for: .touchUpInside)
        navigationItem.leftBarButtonItems = [.init(customView: backButton)]
        
        // Navbar title label
        let navTitleView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width - 105, height: 44))
        
        let commentLabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.view.frame.width - 105, height: 44))
        commentLabel.font = UIFont(name: AppConstants.Font.primaryRegular, size: 20.0)
        commentLabel.textColor = UIColor.white
        commentLabel.textAlignment = NSTextAlignment.center
        commentLabel.text = "Ratings"
        navTitleView.addSubview(commentLabel)
        
        navigationItem.titleView = navTitleView
        
        navigationController?.navigationBar.barTintColor = AppConstants.SminqColors.blackDark100
        navigationController?.navigationBar.isTranslucent = false
        
        // Remove the background color.
        navigationController?.navigationBar.setBackgroundImage(UIColor.clear.as1ptImage(), for: .default)
        
        // Set the shadow color.
        navigationController?.navigationBar.shadowImage = AppConstants.SminqColors.white100.withAlphaComponent(0.1).as1ptImage()
    }
    
    func errorStateVisibility(placesStories: [PlacesStoriesData], error: ErrorObject?) {
        if error != nil {
            self.errorView.animateInViewWithMessage(error?.errorTitle ?? AppConstants.ErrorMessages.opsSomethingWentWrong, error?.errorDescription ?? "")
            self.emptyStateUIView.isHidden = false
            self.postsCollectionView.backgroundView = self.emptyStateUIView
        } else {
            if placesStories.count > 0 {
                self.emptyStateUIView.isHidden = true
                self.postsCollectionView.backgroundView = UIView()
                
            } else {
                self.emptyStateUIView.isHidden = false
                self.postsCollectionView.backgroundView = self.emptyStateUIView
            }
            self.errorView.animateOutView()
        }
       
    }
    
    func getArchievedPosts() {
        
        if InternetConnection.isConnectedToInternet() {
            let userId = Helper().getUserId()
            SVProgressHUD.show()
            
            guard let metric = HTTPMetric(url: URL(string: UserService().getArchivedStoriesAPIEndPoint())!, httpMethod: .get) else { return }
            metric.start()
            
            let getArchivedStoriesAPI = UserService().getArchivedStories(displayName: userId)
                
            getArchivedStoriesAPI.response.validate(statusCode: 200..<300).responseJSON { response in
//                debugPrint(response)
                
                if let httpCode = response.response?.statusCode {
                    metric.responseCode = httpCode
                }
                
                switch response.result {
                case .success:
                    guard let json = response.result.value as? [JSON] else {
                        LogUtil.error(response)
                        AnalyticsHelper.APIFailureEvent(screen: AppConstants.screenNames.archivedPosts, apiName: AppConstants.apiNames.getUserArchivePosts, errorMessage: AppConstants.ErrorLog.jsonParse, userId: userId, errorCode: nil, requestPayload: getArchivedStoriesAPI.requestPayload, requestMethod: getArchivedStoriesAPI.requestMethod.rawValue, requestPathParams: getArchivedStoriesAPI.requestPathParams)
                        
                        let errorObject = ErrorObject.init(apiName: AppConstants.apiNames.getUserArchivePosts, errorType: .jsonParsingError, httpCode: nil, errorTitle: AppConstants.ErrorMessages.opsSomethingWentWrong, errorDescription: AppConstants.ErrorMessages.serverBusyPleaseTryAgain)
                        
                        self.errorStateVisibility(placesStories: self.placesStories, error: errorObject)
                        return
                    }
                    
                    if let tempPlacesStories: [PlacesStoriesData] = [PlacesStoriesData].from(jsonArray: json) {
                        self.placesStories = tempPlacesStories
                        self.postsCollectionView.reloadData()
                        self.errorStateVisibility(placesStories: self.placesStories, error: nil)
                    } else {
                        LogUtil.error(response)
                        
                        
                        AnalyticsHelper.APIFailureEvent(screen: AppConstants.screenNames.archivedPosts, apiName: AppConstants.apiNames.getUserArchivePosts, errorMessage: AppConstants.ErrorLog.jsonParse, userId: userId, errorCode: nil, requestPayload: getArchivedStoriesAPI.requestPayload, requestMethod: getArchivedStoriesAPI.requestMethod.rawValue, requestPathParams: getArchivedStoriesAPI.requestPathParams)
                        
                        let errorObject = ErrorObject.init(apiName: AppConstants.apiNames.getUserArchivePosts, errorType: .jsonParsingError, httpCode: nil, errorTitle: AppConstants.ErrorMessages.opsSomethingWentWrong, errorDescription: AppConstants.ErrorMessages.serverBusyPleaseTryAgain)
                        
                        self.errorStateVisibility(placesStories: self.placesStories, error: errorObject)
                    }
                    
                case .failure(let error):
                    LogUtil.error(error)
                    
                    let error = error as NSError
                    
                    var errorObject: ErrorObject? = ErrorObject(apiName: AppConstants.apiNames.getUserArchivePosts, errorType: .unknown, httpCode: error.code, errorTitle: "", errorDescription: AppConstants.ErrorMessages.serverError)
                    
                    if let httpCode = response.response?.statusCode {
                        errorObject = APIErrorCodes.getErrorObject(apiName: AppConstants.apiNames.getUserArchivePosts, httpCode: httpCode)
                    }
                    
                    self.errorStateVisibility(placesStories: self.placesStories, error: errorObject)
                    
                    if let errorDesc = error.userInfo.valueForKeyPath(keyPath: "NSLocalizedDescription") {
                        AnalyticsHelper.APIFailureEvent(screen: AppConstants.screenNames.archivedPosts, apiName: AppConstants.apiNames.getUserArchivePosts, errorMessage: errorDesc as! String, userId: userId, errorCode: nil, requestPayload: getArchivedStoriesAPI.requestPayload, requestMethod: getArchivedStoriesAPI.requestMethod.rawValue, requestPathParams: getArchivedStoriesAPI.requestPathParams)
                    }
                
                }
                SVProgressHUD.dismiss()
                metric.stop()
            }
        } else {
            
            let errorObject = ErrorObject.init(apiName: AppConstants.apiNames.getUserArchivePosts, errorType: .noNetwork, httpCode: nil, errorTitle: AppConstants.ErrorMessages.noInternet, errorDescription: AppConstants.ErrorMessages.noInterneSubHeading)
            
            self.errorStateVisibility(placesStories: self.placesStories, error: errorObject)
            
            
        }
    }
    
    // MARK: Target methods
    
    @objc func tappedBackButton() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func handleRefresh() {
        getArchievedPosts()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            self.refreshControl.endRefreshing()
        }
    }
    
    // MARK: Button actions
    
    @IBAction func retryButtonTapped(_ sender: UIButton) {
        getArchievedPosts()
    }
}

extension ArchivedPostsViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if self.placesStories.count > 0 {
            return placesStories.count
        }
        
        return 0
    }
    
//    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        guard self.placesStories.count > 0 else { return }
//        guard let stories = self.placesStories[indexPath.row].stories, let place = self.placesStories[indexPath.row].placeDetails, let story = stories.first, story.posts.count > 0 else { return }
//        guard var feed = story.posts.first else { return }
//
//        feed.archived = true
//        feed.placeDetails = place
//
//        let storiesViewController = AppConstants.storyboard.instantiateViewController(withIdentifier: "StoriesViewController") as! StoriesViewController
//
//        storiesViewController.distance = ""
//
//        var tempStoryList: [StoryData] = []
//
//        let dataToAppend: [String: Any] = ["userId": feed.user._id, "placeId": place._id, "archived": true]
//        let tempStory: StoryData = StoryData.init(json: dataToAppend)!
//        tempStoryList.append(tempStory)
//
//        let storyViewModel = StoryViewModel(screen: AppConstants.screenNames.storyView, storyList: tempStoryList)
//
//        storiesViewController.storyViewModel = storyViewModel
//        storiesViewController.city = ""
//        storiesViewController.distance = FIRRemoteConfigService.shared.getNearbyRadiusMeters()
//        storiesViewController.placeId = place._id
//        storiesViewController.feedList = [feed]
//        storiesViewController.isArchivedPost = true
//        storiesViewController.source = AppConstants.screenNames.archivedPosts
//        storiesViewController.delegate = self
//
//        let navController = UINavigationController(rootViewController: storiesViewController)
//        navController.modalPresentationStyle = .overCurrentContext
//        navController.modalPresentationCapturesStatusBarAppearance = true
//        self.hideTabBar()
//        self.present(navController, animated: true, completion: nil)
//
//        self.navigationController?.pushViewController(storiesViewController, animated: false)
//    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ArchivedPostCollectionViewCell", for: indexPath) as! ArchivedPostCollectionViewCell
        
        let placeStory = self.placesStories[indexPath.row]
        
        if indexPath.row > 0 {
            cell.prevPlaceStory = self.placesStories[indexPath.row - 1]
        }
        
        cell.placeStory = placeStory
        cell.setData()
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let totalWidth = collectionView.bounds.size.width
        let numberOfCellsPerRow: CGFloat = 3.0
        let dimension = (totalWidth / numberOfCellsPerRow)
        return CGSize.init(width: dimension, height: 222)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
}

extension ArchivedPostsViewController: ErrorViewDelegate {
    func errorViewTapped() {
        self.errorView.animateOutView()
        self.getArchievedPosts()
    }
}
