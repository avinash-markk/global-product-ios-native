//
//  LoginViewController.swift
//  Sminq
//
//  Created by Bjorn Mascarenhas on 21/02/18.
//  Copyright © 2018 Suraj Sagare. All rights reserved.
//

import UIKit
import FirebaseAuth
import Firebase
import SVProgressHUD
import GoogleSignIn
import AccountKit
import Alamofire
import Gloss
import MapKit
import CleverTapSDK

// MARK: Properties to Save Global Object of User

class LoginViewController: UIViewController {
    var method: String? // used for account kit cancel event
    
    @IBOutlet weak var TCTextView: UITextView!
    //    @IBOutlet weak var googleLoginButton: UIButton!
    //    @IBOutlet weak var facebookLoginButton: UIButton!
    //    @IBOutlet weak var useEmailButton: UIButton!
    //    @IBOutlet weak var mobileNumberBotton: UIButton!
    
    var _inputState: String?
    var _outputState: String?
    var _accountKit: AccountKit?
    
    
    // MARK: - properties
    var accountKit = AccountKit(responseType: .accessToken)
    var _pendingLoginViewController: AKFViewController?
    var _showAccountOnAppear = false
    var _enableSendToFacebook: Bool = true
    
    let errorView = ErrorView()
    
    // MARK: - ViewControllers life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initUI()
        
        if _accountKit == nil {
            _accountKit = AccountKit(responseType: .accessToken)
        }
        
        _showAccountOnAppear = (_accountKit?.currentAccessToken != nil)
        _pendingLoginViewController = _accountKit?.viewControllerForLoginResume() as AKFViewController?
        _enableSendToFacebook = true
        
        // Setting up Google Login
        GIDSignIn.sharedInstance().uiDelegate = self
        
        // UI Tweaks method calls
        //        roundingButtons()
        termsAndConditions()
        self.errorViewSetup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
        setNeedsStatusBarAppearanceUpdate()
        
        if _showAccountOnAppear && UserDefaults.standard.bool(forKey: "AccountKitSession") == true {
            _showAccountOnAppear = false
        } else if let viewController = _pendingLoginViewController {
            _prepareLoginViewController(viewController)
            if let viewController = viewController as? UIViewController {
                UserDefaults.standard.set(true, forKey: "SignIn")
                present(viewController, animated: animated, completion: nil)
                _pendingLoginViewController = nil
            }
        }
        
        if accountKit.currentAccessToken != nil && UserDefaults.standard.bool(forKey: "AccountKitSession") == true {
            self._accountKit = AccountKit.init(responseType: ResponseType.accessToken)
            self._accountKit?.requestAccount( { (account, error) in
                //                let mobileNumber = account?.phoneNumber?.stringRepresentation()
                
                if account != nil {
                    let mobileNumber = account?.phoneNumber?.phoneNumber
                    //                let countryCode = "+\(account?.phoneNumber?.countryCode ?? "")"
                    var countryCode: String = ""
                    
                    if account?.phoneNumber?.countryCode != "" {
                        countryCode = "+" + (account?.phoneNumber?.countryCode)!
                    }
                    
                    if let email = account?.emailAddress {
                        self.signInCheck(network: "account-kit", identity: account?.accountID, name: "", email: email, mobile: mobileNumber, imageUrl: "", countryCode: countryCode)
                    } else {
                        self.signInCheck(network: "account-kit", identity: account?.accountID, name: "", email: "", mobile: mobileNumber, imageUrl: "", countryCode: countryCode)
                    }
                }
            })
        }
    }
    
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        AppConstants.previousScreen = AppConstants.screenNames.signupLanding
    }
    
    override public var prefersStatusBarHidden: Bool {
        return true
    }
    
    override func viewSafeAreaInsetsDidChange() {
        self.errorView.setStackViewTopConstraint(top: self.getSafeAreaInsets().top)
    }
    
    func initUI() {
        self.TCTextView.textContainer.lineFragmentPadding = 0
        self.view.backgroundColor = AppConstants.SminqColors.blackDark100
    }
    
    func errorViewSetup() {
        // TODO: Instead of using '56' calculate two label's height dynamically
        errorView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: self.getSafeAreaInsets().top + 56)
        errorView.setStackViewTopConstraint(top: self.getSafeAreaInsets().top)
        self.view.addSubview(errorView)
        errorView.isHidden = true
        errorView.delegate = self
    }
    
    func termsAndConditions() {
        self.TCTextView.delegate = self
        let termsAndConditionsString = "By using \(UIApplication.shared.getTargetName()), you agree to the Terms and Privacy Policy"
        
        let style = NSMutableParagraphStyle()
        style.alignment = .center
        let attributedString = NSMutableAttributedString(string: termsAndConditionsString,
                                                         attributes: [NSAttributedStringKey.paragraphStyle: style])
        
        let attributes: [NSAttributedStringKey: Any] = [
            NSAttributedStringKey.font: UIFont(name: AppConstants.Font.primaryRegular, size: 12.0) as Any,
            NSAttributedStringKey.foregroundColor: AppConstants.SminqColors.white100.withAlphaComponent(0.5) as Any
        ]
        let fontRange = (attributedString.string as NSString).range(of: termsAndConditionsString)
        attributedString.addAttributes(attributes, range: fontRange)
        
        var foundRange = attributedString.mutableString.range(of: "Terms")
        attributedString.addAttribute(NSAttributedStringKey.link, value: SminqAPI.endPointForTerms(), range: foundRange)
        attributedString.addAttribute(.font, value: UIFont(name: AppConstants.Font.primaryBold, size: 12.0) as Any, range: foundRange)
        
        
        foundRange = attributedString.mutableString.range(of: "Privacy Policy")
        attributedString.addAttribute(NSAttributedStringKey.link, value: SminqAPI.endPointForPrivacy(), range: foundRange)
        attributedString.addAttribute(.font, value: UIFont(name: AppConstants.Font.primaryBold, size: 12.0) as Any, range: foundRange)
        
        TCTextView.attributedText = attributedString
        
        // Set link Color to grey
        let linkAttributes: [String: Any] = [
            NSAttributedStringKey.foregroundColor.rawValue: AppConstants.SminqColors.white100.withAlphaComponent(0.5)
        ]
        TCTextView.linkTextAttributes = linkAttributes
    }
    
    // MARK: All Buttons Actions
    
    @IBAction func googleLoginButton(_ sender: UIButton) {
        LogUtil.info("Tapped on google button")
        
        AnalyticsHelper.signUpOptionsSelectedEvent(method: AppConstants.Login.google)
        
        if InternetConnection.isConnectedToInternet() {
            GIDSignIn.sharedInstance().delegate = self
            GIDSignIn.sharedInstance().signIn()
            SVProgressHUD.show()
        } else {
            self.errorView.animateInViewWithMessage(AppConstants.ErrorMessages.noInternet.uppercased(), AppConstants.ErrorMessages.noInterneSubHeading)
        }
    }
    
    @IBAction func mobileNumberButton(_ sender: UIButton) {
        LogUtil.info("Tapped on Account Kit - Mobile button")
        
        AnalyticsHelper.signUpOptionsSelectedEvent(method: AppConstants.Login.phone)
        
        if InternetConnection.isConnectedToInternet() {
            UserDefaults.standard.set(true, forKey: "AccountKitSession")
            let viewController = accountKit.viewControllerForPhoneLogin(with: nil, state: _inputState) as AKFViewController
            viewController.isSendToFacebookEnabled = _enableSendToFacebook
            viewController.delegate = self
            viewController.isGetACallEnabled = true
            
            viewController.uiManager = SkinManager(skinType: .contemporary, primaryColor: AppConstants.SminqColors.blackDark100)
            
            self.method = AppConstants.Login.phone
            self._prepareLoginViewController(viewController)
            present(viewController as! UIViewController, animated: true, completion: nil)
        } else {
            self.errorView.animateInViewWithMessage(AppConstants.ErrorMessages.noInternet.uppercased(), AppConstants.ErrorMessages.noInterneSubHeading)
        }
    }
    
    // MARK: - SignIn Methods
    
    func commonSignIn(userData: UserDataDict) {
        
        UserDefaults.standard.set(true, forKey: "SignIn")
        SVProgressHUD.dismiss()
        
        AppConstants.appDelegate.tabBarViewController =
            AppConstants.storyboard.instantiateViewController(withIdentifier: "TabbarViewController") as? TabbarViewController
        AppConstants.appDelegate.tabBarViewController?.selectedIndex = 0
        AppConstants.appDelegate.tabBarViewController?.previousController = TimelineViewController()
        
        //        let tabBarViewControllerNav = UINavigationController(rootViewController: tabBarViewController!)
        
        AppConstants.appDelegate.window?.rootViewController = AppConstants.appDelegate.tabBarViewController
        self.navigationController?.checkAndDisplayBadgeForIncompleteProfile(data: userData)
    }
    
    func signInCheck(network: String!, identity: String!, name: String!, email: String!, mobile: String!, imageUrl: String!, countryCode: String) {
        let socialIdentity = [["network": network, "identity": identity]]
        
        let param: Parameters = [
            "email": email,
            "mobile": mobile,
            "countryCode": countryCode,
            "socialIdentity": socialIdentity,
            "imageUrl": imageUrl
        ]
        
        var method: String = ""
        
        if network == "facebook" {
            method = AppConstants.Login.facebook
        } else if network == "google" {
            method = AppConstants.Login.google
        } else if network == "account-kit" {
            if email == "" && mobile != "" {
                method = AppConstants.Login.phone
            } else {
                method = AppConstants.Login.email
            }
        }
        
        if InternetConnection.isConnectedToInternet() {
            SVProgressHUD.show()
            guard let metric = HTTPMetric(url: URL(string: SminqAPI.endpoint("/v1/users") as! String)!, httpMethod: .post) else { return }
            metric.start()
            
            LogUtil.infoAPIPayload(apiName: AppConstants.apiNames.loginUser, payload: param, pathParams: [:], requestMethod: AppConstants.requestMethods.post)
            
            AFManager.request(SminqAPI.endpoint("/v1/users"), method: .post, parameters: param, encoding: JSONEncoding.default, headers: SminqAPI.headers())
                .validate(statusCode: 200..<300)
                .responseJSON { response in
//                    debugPrint(response)
                    
                    self.errorView.animateOutView()
                    
                    if let statusCode = response.response?.statusCode {
                        metric.responseCode = statusCode
                    }
                    
                    switch response.result {
                        
                    case .success(_):
                        guard let json = response.result.value as? JSON else { return }
                        
                        guard let userData: UserDataDict = UserDataDict(json: json) else {
                            AnalyticsHelper.signupOptionsEvent(method: method, success: AppConstants.no, error: AppConstants.ErrorMessages.emptyResponse)
                            AnalyticsHelper.APIFailureEvent(screen: AppConstants.screenNames.login, apiName: AppConstants.apiNames.loginUser, errorMessage: AppConstants.ErrorMessages.emptyResponse, userId: "", errorCode: nil, requestPayload: param, requestMethod: AppConstants.post, requestPathParams: [:])
                            return
                        }
                        
                        let profileDataClass = ProfileDataClass()
                        profileDataClass.saveProfileData(userProfileData: UserProfileData.init(user: userData, userMetrics: nil, stories: [], levelsList: [], streaks: nil, highlight: nil, claims: nil))
                        UserDefaultsUtility.shared.setAuthToken(token: userData.authToken)
                        
                        if userData.existingUser == true {
                            AnalyticsHelper.userLoginToCleverTap(user: userData)
                            
                            BugfenderService.shared.setBugFenderDeviceString(user: userData)
                            
                            AnalyticsHelper.signupOptionsEvent(method: method, success: AppConstants.yes, error: "")
                            
                            UserNoficationService.shared.subscribeToPushNotifications()
                        
                            self.commonSignIn(userData: userData)
                        } else {
                            if let signUpViewController = AppConstants.storyboard.instantiateViewController(withIdentifier: "SignUpViewController") as? SignUpViewController {
                                signUpViewController.usersDataSignUp = userData
                                signUpViewController.network = network
                                signUpViewController.name = name
                                self.navigationController?.pushViewController(signUpViewController, animated: true)
                            }
                        }
                    case .failure(let error):
                        let error = error as NSError
                        
                        if let errorDesc = error.userInfo.valueForKeyPath(keyPath: "NSLocalizedDescription") as? String {
                            AnalyticsHelper.signupOptionsEvent(method: method, success: AppConstants.no, error: errorDesc)
                            AnalyticsHelper.APIFailureEvent(screen: AppConstants.screenNames.login, apiName: AppConstants.apiNames.loginUser, errorMessage: errorDesc, userId: "", errorCode: nil, requestPayload: param, requestMethod: AppConstants.post, requestPathParams: [:])
                        }
                        
                        var errorObject: ErrorObject? = ErrorObject(apiName: AppConstants.apiNames.loginUser, errorType: .unknown, httpCode: error.code, errorTitle: "", errorDescription: AppConstants.ErrorMessages.serverError)
                        
                        if let statusCode = response.response?.statusCode {
                            errorObject = APIErrorCodes.getErrorObject(apiName: AppConstants.apiNames.loginUser, httpCode: statusCode)
                        }
                        
                        self.errorView.animateInViewWithMessage(errorObject?.errorTitle.uppercased() ?? AppConstants.ErrorMessages.opsSomethingWentWrong, errorObject?.errorDescription ?? "")
                    }
                    
                    SVProgressHUD.dismiss()
                    
                    metric.stop()
            }
        } else {
            AnalyticsHelper.signupOptionsEvent(method: method, success: AppConstants.no, error: AppConstants.ErrorMessages.noInternet)
            self.errorView.animateInViewWithMessage(AppConstants.ErrorMessages.opsSomethingWentWrong.uppercased(), AppConstants.ErrorMessages.noInterneSubHeading)
        }
    }
    
    // MARK: - helpers for Account-Kit
    private func _prepareLoginViewController(_ loginViewController: AKFViewController) {
        loginViewController.delegate = self
    }
    
    private func _present(withSegueIdentifier segueIdentifier: String, animated: Bool) {
        if animated {
            self.performSegue(withIdentifier: segueIdentifier, sender: nil)
        } else {
            UIView.performWithoutAnimation {
                self.performSegue(withIdentifier: segueIdentifier, sender: nil)
            }
        }
    }
    
}

// MARK: UITextViewDelegate Functions
extension LoginViewController: UITextViewDelegate {
    
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange) -> Bool {
        
        if InternetConnection.isConnectedToInternet() {
            if let tcWebViewController = AppConstants.storyboard.instantiateViewController(withIdentifier: "TCWebViewController") as? TCWebViewController {
                if URL.absoluteString == SminqAPI.endPointForTerms() {
                    LogUtil.info("Tapped on terms and conditions link")
                    AnalyticsHelper.viewedTermsConditionsEvent(screen: AppConstants.screenNames.login)
                    tcWebViewController.strWebURL = SminqAPI.endPointForTerms()
                    tcWebViewController.strTitle = "Terms"
                    self.navigationController?.navigationBar.isHidden = false
                    self.navigationController?.pushViewController(tcWebViewController, animated: true)
                } else if URL.absoluteString == SminqAPI.endPointForPrivacy() {
                    LogUtil.info("Tapped on privacy policy link")
                    AnalyticsHelper.viewedPrivacyPolicyEvent(screen: AppConstants.screenNames.login)
                    tcWebViewController.strWebURL = SminqAPI.endPointForPrivacy()
                    tcWebViewController.strTitle = "Privacy Policy"
                    self.navigationController?.navigationBar.isHidden = false
                    self.navigationController?.pushViewController(tcWebViewController, animated: true)
                }
            }
        } else {
            self.errorView.animateInViewWithMessage(AppConstants.ErrorMessages.noInternet.uppercased(), AppConstants.ErrorMessages.noInterneSubHeading)
        }
        
        return false
    }
    
}

// MARK: UITextViewDelegate Functions
extension  LoginViewController: GIDSignInUIDelegate, GIDSignInDelegate {
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if error == nil {
            let userId = user.userID                  // For client-side use only!
            let idToken = user.authentication.idToken // Safe to send to the server
            let fullName = user.profile.name
            let givenName = user.profile.givenName
            let familyName = user.profile.familyName
            let email = user.profile.email
            let photoUrl = user.profile.imageURL(withDimension: 1000).absoluteString
            
            signInCheck(network: "google", identity: userId!, name: fullName!, email: email!, mobile: "", imageUrl: photoUrl, countryCode: "")
            
        } else {
            AnalyticsHelper.signupOptionsEvent(method: AppConstants.Login.google, success: AppConstants.no, error: "cancelled")
            SVProgressHUD.dismiss()
        }
    }
    
    func sign(inWillDispatch signIn: GIDSignIn!, error: Error!) {
        print("inWillDispatch")
        SVProgressHUD.dismiss()
    }
    
}

// MARK: - AKFViewControllerDelegate extension
extension LoginViewController: AKFViewControllerDelegate {
    
    private func viewController(_ viewController: UIViewController!, didCompleteLoginWith accessToken: AccessToken!, state: String!) {
        _outputState = state
    }
    
    private func viewController(_ viewController: UIViewController!, didFailWithError error: Error!) {
        self.errorView.animateInViewWithMessage(AppConstants.ErrorMessages.opsSomethingWentWrong.uppercased(), AppConstants.ErrorMessages.troubleConnecting)
    }
    
    func viewControllerDidCancel(_ viewController: (UIViewController & AKFViewController)!) {
        if let method = self.method {
            AnalyticsHelper.signupOptionsEvent(method: method, success: AppConstants.no, error: "cancelled")
            UserDefaults.standard.set(false, forKey: "AccountKitSession")
        }
    }
    
}

extension LoginViewController: ErrorViewDelegate {
    func errorViewTapped() {
        self.errorView.animateOutView()
    }
}
