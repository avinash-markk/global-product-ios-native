//
//  RatingCameraViewController.swift
//  Sminq
//
//  Created by Sebastian Sztemberg on 01/04/2019.
//  Copyright © 2019 Sminq. All rights reserved.
//

import UIKit
import Gloss
import SVProgressHUD
import Firebase
import CleverTapSDK
import Cloudinary
import Alamofire
import UserNotifications
import MarkkCamera
import AVFoundation

protocol RatingCameraViewControllerDelegate: NSObjectProtocol {
    func presentPostSuccessViewController()
}

class RatingCameraViewController: CameraViewController {
    public weak var ratingCameraViewControllerDelegate: RatingCameraViewControllerDelegate?
    
    let locationManager = UserLocationManager.SharedManager
    let errorView = ErrorView()
    var selectedPlaces = [String: PlaceDetail]()
    var screen: String? = ""
    var locationAccuracy: CLLocationAccuracy?
    
    init() {
        super.init(nibName: nil, bundle: nil)
        self.initializeCameraDataSource()
        locationManager.delegate = self
        modalPresentationStyle = .overFullScreen
        modalPresentationCapturesStatusBarAppearance = true
        selectedPlaces.removeAll()
        errorViewSetup()
        locationManager.requestLocationsForPostFlow()
        SVProgressHUD.dismiss()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func initializeCameraDataSource() {
        let cameraDataSource = CameraDataSource()
        cameraDataSource.locationsCallback = locationCallback
        cameraDataSource.nearbyLocationsCallback = nearbyLocationCallback
        cameraDataSource.locationSelectCallback = locationSelectCallback
        cameraDataSource.mediaSelectedCallback = mediaSelectedCallback
        cameraSource = cameraDataSource
        delegate = self
    }
    
    func errorViewSetup() {
        NotificationCenter.default.addObserver(self, selector: #selector(updateNetworkStatus), name: NSNotification.Name(rawValue: AppConstants.AppNotificationNames.networkReachable), object: nil)
        errorView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: self.getSafeAreaInsets().top + 56)
        errorView.setStackViewTopConstraint(top: self.getSafeAreaInsets().top)
        UIApplication.shared.keyWindow?.addSubview(errorView)
        errorView.isHidden = true
        errorView.delegate = self
    }
    
    @objc func updateNetworkStatus(notification: Notification) {
        guard let status = notification.userInfo?["status"] as? Bool else {
            return
        }
        self.showErrorView(show: !status, errorHeading: AppConstants.ErrorMessages.noInternet.uppercased(), errorSubHeading: AppConstants.ErrorMessages.noInterneSubHeading)
    }
    
    func showErrorView(show: Bool, errorHeading: String, errorSubHeading: String) {
        if show {
        self.errorView.animateInViewWithMessage(errorHeading, errorSubHeading)
        } else {
        self.errorView.animateOutView()
        }
    }
    
    func locationCallback(searchQuery: String, callback: @escaping LocationsSourceCallback) -> Void {
        let location = UserDefaultsUtility.shared.getHundredMeterLocationOtherwiseLastKnownLocation()
        guard let latitude = location.latitude, let longitude = location.longitude else { return }
        
        if InternetConnection.isConnectedToInternet() {
            guard let metric = HTTPMetric(url: URL(string: PlaceService().getHybridSearchResultsAPIEndPoint())!, httpMethod: .get) else { return }
            metric.start()
            
            if let cameraLocationView = cameraLocationView {
                cameraLocationView.placesTableView.isHidden = false
            }
            
            let getHybridSearchResultsAPI = PlaceService().getHybridSearchResults(latitude: latitude, longitude: longitude, searchText: searchQuery, type: "places", userId: Helper().getUserId())
            
            getHybridSearchResultsAPI.response.validate(statusCode: 200..<300).responseJSON { response in
                if let httpCode = response.response?.statusCode {
                    metric.responseCode = httpCode
                }
                
                switch response.result {
                case .success:
                    guard let json = response.result.value as? JSON else { return }
                    if let hybridSearch: HybridPlaces = HybridPlaces(json: json) {
                        
                        if hybridSearch.sminqResults != nil {
                            let places = hybridSearch.sminqResults!.map { searchPlace in
                                return CameraPlace(id: searchPlace._id, name: searchPlace.name, address: searchPlace.address)
                            }
                            callback(places, nil)
                        }
                        
                        if hybridSearch.googleResults != nil {
                            let places = hybridSearch.googleResults!.map { searchPlace in
                                return CameraPlace(id: searchPlace._id, name: searchPlace.name, address: searchPlace.address)
                            }
                            callback(places, nil)
                        }
                    } else {
                        LogUtil.error(response)
                        AnalyticsHelper.APIFailureEvent(screen: AppConstants.screenNames.addPost, apiName: AppConstants.apiNames.searchPlacesOrUsers, errorMessage: AppConstants.ErrorLog.jsonParse, userId: Helper().getUserId(), errorCode: nil, requestPayload: getHybridSearchResultsAPI.requestPayload, requestMethod: getHybridSearchResultsAPI.requestMethod.rawValue, requestPathParams: getHybridSearchResultsAPI.requestPathParams)
                      self.showErrorView(show: true, errorHeading: AppConstants.ErrorMessages.opsSomethingWentWrong, errorSubHeading: AppConstants.ErrorMessages.serverError)
                        callback(nil, CameraError(errorDescription: "API error"))
                    }
                    
                    break
                    
                case .failure(let error):
                    LogUtil.error(error)
                    
                    let error = error as NSError
                    let userInfo = error.userInfo
                    if let errorDesc = userInfo.valueForKeyPath(keyPath: "NSLocalizedDescription") as? String {
                        AnalyticsHelper.APIFailureEvent(screen: AppConstants.screenNames.addPost, apiName: AppConstants.apiNames.searchPlacesOrUsers, errorMessage: errorDesc, userId: Helper().getUserId(), errorCode: nil, requestPayload: getHybridSearchResultsAPI.requestPayload, requestMethod: getHybridSearchResultsAPI.requestMethod.rawValue, requestPathParams: getHybridSearchResultsAPI.requestPathParams)
                    }
                  self.showErrorView(show: true, errorHeading: AppConstants.ErrorMessages.opsSomethingWentWrong, errorSubHeading: AppConstants.ErrorMessages.serverError)
                    callback(nil, CameraError(errorDescription: "API error"))
                    break
                }
                
                metric.stop()
            }
        }
        else {
            self.showErrorView(show: true, errorHeading: AppConstants.ErrorMessages.noInternet.uppercased(), errorSubHeading: AppConstants.ErrorMessages.noInterneSubHeading)
            callback(nil, CameraError(errorDescription: "Network error"))
            
        }
    }
    
    func nearbyLocationCallback(callback: @escaping LocationsSourceCallback) -> Void {
        let location = UserDefaultsUtility.shared.getHundredMeterLocationOtherwiseLastKnownLocation()
        guard let latitude = location.latitude, let longitude = location.longitude else { return }
        
        if InternetConnection.isConnectedToInternet() {
            guard let metric = HTTPMetric(url: URL(string: PlaceService().getNearByPlacesAPIEndPoint())!, httpMethod: .get) else { return }
            metric.start()
            if let cameraLocationView = cameraLocationView {
                cameraLocationView.placesTableView.isHidden = false
            }
            
            let getNearByPlacesAPI = PlaceService().getNearByPlaces(latitude: latitude, longitude: longitude, userId: Helper().getUserId())
            
            getNearByPlacesAPI.response.validate(statusCode: 200..<300).responseJSON { response in
                debugPrint(response)
                
                if let httpCode = response.response?.statusCode {
                    metric.responseCode = httpCode
                }
                
                switch response.result {
                case .success:
                    guard let json = response.result.value as? JSON else {
                        LogUtil.error(response)
                        
                        AnalyticsHelper.APIFailureEvent(screen: AppConstants.screenNames.addPost, apiName: AppConstants.apiNames.searchNearbyPlaces, errorMessage: AppConstants.ErrorLog.jsonParse, userId: Helper().getUserId(), errorCode: nil, requestPayload: getNearByPlacesAPI.requestPayload, requestMethod: getNearByPlacesAPI.requestMethod.rawValue, requestPathParams: getNearByPlacesAPI.requestPathParams)
                        
                        return
                    }
                    if let hybridSearch: HybridPlaces = HybridPlaces(json: json) {
                        if hybridSearch.sminqResults != nil {
                            let places = hybridSearch.sminqResults!.map { searchPlace in
                                return CameraPlace(id: searchPlace._id, name: searchPlace.name, address: searchPlace.address)
                            }
                            callback(places, nil)
                        }
                        
                        if hybridSearch.googleResults != nil {
                            let places = hybridSearch.googleResults!.map { searchPlace in
                                return CameraPlace(id: searchPlace._id, name: searchPlace.name, address: searchPlace.address)
                            }
                            callback(places, nil)
                        }
                    } else {
                        LogUtil.error(response)
                        AnalyticsHelper.APIFailureEvent(screen: AppConstants.screenNames.addPost, apiName: AppConstants.apiNames.searchNearbyPlaces, errorMessage: AppConstants.ErrorLog.jsonParse, userId: Helper().getUserId(), errorCode: nil, requestPayload: getNearByPlacesAPI.requestPayload, requestMethod: getNearByPlacesAPI.requestMethod.rawValue, requestPathParams: getNearByPlacesAPI.requestPathParams)
                       self.showErrorView(show: true, errorHeading: AppConstants.ErrorMessages.opsSomethingWentWrong, errorSubHeading: AppConstants.ErrorMessages.serverError)
                        callback(nil, CameraError(errorDescription: "API error"))
                    }
                    
                    break
                    
                case .failure(let error):
                    LogUtil.error(error)
                    
                    let error = error as NSError
                    let userInfo = error.userInfo
                    if let errorDesc = userInfo.valueForKeyPath(keyPath: "NSLocalizedDescription") as? String {
                        AnalyticsHelper.APIFailureEvent(screen: AppConstants.screenNames.addPost, apiName: AppConstants.apiNames.searchNearbyPlaces, errorMessage: errorDesc, userId: Helper().getUserId(), errorCode: nil, requestPayload: getNearByPlacesAPI.requestPayload, requestMethod: getNearByPlacesAPI.requestMethod.rawValue, requestPathParams: getNearByPlacesAPI.requestPathParams)
                    }
                  self.showErrorView(show: true, errorHeading: AppConstants.ErrorMessages.opsSomethingWentWrong, errorSubHeading: AppConstants.ErrorMessages.serverError)
                    callback(nil, CameraError(errorDescription: "API error"))
                    break
                }
                
                metric.stop()
            }
        } else {
            self.showErrorView(show: true, errorHeading: AppConstants.ErrorMessages.noInternet.uppercased(), errorSubHeading: AppConstants.ErrorMessages.noInterneSubHeading)
            callback(nil, CameraError(errorDescription: "Network error"))
        }
    }
    
    func locationSelectCallback(place: CameraPlace, callback: @escaping LocationSourceCallback) -> Void {
        
        if InternetConnection.isConnectedToInternet() {
            guard let metric = HTTPMetric(url: URL(string: PlaceService().getPlaceDetailAPIEndPoint())!, httpMethod: .post) else { return }
            metric.start()
            
            if let cameraLocationView = cameraLocationView {
                cameraLocationView.placesTableView.isHidden = false
            }
            
            let getPlaceDetailAPI = PlaceService().getPlaceDetail(placeId: place.id, userId: Helper().getUserId())
            
            getPlaceDetailAPI.response.validate(statusCode: 200..<300).responseJSON { response in
                debugPrint(response)
                
                if let httpCode = response.response?.statusCode {
                    metric.responseCode = httpCode
                }
                
                switch response.result {
                case .success:
                    guard let json = response.result.value as? JSON else {
                        LogUtil.error(response)
                        return
                    }
                    
                    if let placeDetail: PlaceDetail = PlaceDetail(json: json) {
                        self.selectedPlaces[place.id] = placeDetail
                        var newPlace = place
                        if let stickers = placeDetail.stickers {
                            newPlace.stickers = stickers.map { sticker in
                                return Sticker(id: sticker.id, name: sticker.name, image: URL(string: sticker.image!.url)!, weight: sticker.weight, searchTags: sticker.searchTags, weightGroup: sticker.weightGroup ?? [])
                            }
                        }
                        
                        AnalyticsHelper.addRatingSelectPlace(place: placeDetail.place!)
                        callback(newPlace, nil)
                        
                    } else {
                        LogUtil.error(response)
                        AnalyticsHelper.APIFailureEvent(screen: AppConstants.screenNames.addPost, apiName: AppConstants.apiNames.getPlaceDetail, errorMessage: AppConstants.ErrorMessages.emptyResponse, userId: Helper().getUserId(), errorCode: nil, requestPayload: getPlaceDetailAPI.requestPayload, requestMethod: getPlaceDetailAPI.requestMethod.rawValue, requestPathParams: getPlaceDetailAPI.requestPathParams)
                      self.showErrorView(show: true, errorHeading: AppConstants.ErrorMessages.opsSomethingWentWrong, errorSubHeading: AppConstants.ErrorMessages.serverError)
                        callback(nil, CameraError(errorDescription: "API error"))
                    }
                    
                    break
                    
                case .failure(let error):
                    LogUtil.error(response)
                    
                    let error = error as NSError
                    let userInfo = error.userInfo
                    if let errorDesc = userInfo.valueForKeyPath(keyPath: "NSLocalizedDescription") as? String {
                        AnalyticsHelper.APIFailureEvent(screen: AppConstants.screenNames.addPost, apiName: AppConstants.apiNames.getPlaceDetail, errorMessage: errorDesc, userId: Helper().getUserId(), errorCode: nil, requestPayload: getPlaceDetailAPI.requestPayload, requestMethod: getPlaceDetailAPI.requestMethod.rawValue, requestPathParams: getPlaceDetailAPI.requestPathParams)
                    }
                    
                    self.showErrorView(show: true, errorHeading: AppConstants.ErrorMessages.opsSomethingWentWrong, errorSubHeading: AppConstants.ErrorMessages.serverError)
                    callback(nil, CameraError(errorDescription: "API error"))
                    break
                }
                metric.stop()
            }
        } else {
            self.showErrorView(show: true, errorHeading: AppConstants.ErrorMessages.noInternet.uppercased(), errorSubHeading: AppConstants.ErrorMessages.noInterneSubHeading)
            callback(nil, CameraError(errorDescription: "Network error"))
        }
    }
    
    func mediaSelectedCallback(asset: CameraAsset) -> Void {
        self.mediaSelectedAnalyticEvent(mediaAsset: asset)
    }
    
    func mediaSelectedAnalyticEvent(mediaAsset: CameraAsset) {
        AnalyticsHelper.addRatingMedia(type: mediaAsset.type == .photo ? "photo" : "video", liveShot: mediaAsset.fromGallery, video: nil, image: nil, totalCount: nil)
    }
    
    func multipleMediaSelectedAnalyticEvent() {
        AnalyticsHelper.addRatingMedia(type: nil, liveShot: true, video: nil, image: nil, totalCount: nil)
    }
    
    func setLocationDataToPostDataFor(asset: CameraAsset) -> [Double]? {
        if asset.fromGallery {
            if let location = asset.location {
                return location
            }
        }
//        if let locationDict: NSDictionary = UserDefaults.standard.value(forKey: "UserLocation") as? NSDictionary {
//            let locationData = [locationDict.value(forKey: "latitude"), locationDict.value(forKey: "longitude")] as? [Double]
//            return locationData
//        }
        return nil
    }
    
    func setCreatedTimeToPostDataFor(asset: CameraAsset) -> String {
        if asset.fromGallery {
            if let createdTime = asset.createdTime {
                return createdTime
            }
        }
        return asset.id
    }
    
    func publishStory(asset: CameraAsset, index: Int) {
        let jsonObject: [String: Any] = [ // NOTE: Added to initialize PostData
            "init": "",
        ]
        
        guard let placeId = asset.selectedPlace?.id, let placeDetails = selectedPlaces[placeId], var postData: PostData = PostData(json: jsonObject) else { return }
        
        if let location = self.setLocationDataToPostDataFor(asset: asset) {
            postData.locationData = location
        }
        postData.createdAt = self.setCreatedTimeToPostDataFor(asset: asset)
        postData.questions = placeDetails.questions
        postData.verified = true
        postData.place = placeDetails.place
        postData.liveShot = asset.fromGallery ? "no" : "yes"
        postData.mood = asset.selectedMood
        postData.locationAccuracy = self.locationAccuracy ?? 0.0
        postData.selectedStickers = RatingsHelper().getImageStickersListFrom(cameraAsset: asset, placeDetail: placeDetails)
        
        let stickersString = RatingsHelper().getImageStickersForPost(stickers: postData.selectedStickers!)
        
        guard let postDataJson = postData.toJSON() else { return }
        let postDataJsonStr =  Helper().jsonToStringConversion(jsonObject: postDataJson as Any)
        
        switch asset.type {
        case .photo:
            
            guard let image = asset.image else { return }
            DocDirectoryUtil().saveImageIntoDocDirectory(feedId: asset.id, image: image)
            
            let imageUploadTask = UploadTask(taskId: asset.id, questionsData: postDataJsonStr, placeIDString: placeDetails.place!._id, subPlaceIDString: placeDetails.place!.defaultSubPlace, isVideo: false, taskPublicId: "", videoDuration: 0.0, placeName: placeDetails.place!.name, textListString: RatingsHelper().getTextStickersListFrom(cameraAsset: asset), stickerListString: stickersString, status: "Pending", progress: 0.0, post: "")
            UploadManager.sharedInstance.addTaskToArrayOfUploads(task: imageUploadTask)
            
            self.addRatingDoneAnalyticEvent(postData: postData, isVideo: false, stickerString: RatingsHelper().getImageStickersListForAnalyticsEvent(stickers: postData.selectedStickers!))
            
        case .video:
            
            guard let url = asset.url else { return }
            let localVideoURL = DocDirectoryUtil().saveVideoLocallyAndReturnURL(feedId: asset.id, feedUrl: url.path)
            let videoDuration = AVAsset(url: localVideoURL).duration.seconds
            
            let videoUploadTask = UploadTask(taskId: asset.id, questionsData: postDataJsonStr, placeIDString: placeDetails.place!._id, subPlaceIDString: placeDetails.place!.defaultSubPlace, isVideo: true, taskPublicId: "", videoDuration: videoDuration, placeName: placeDetails.place!.name, textListString: RatingsHelper().getTextStickersListFrom(cameraAsset: asset), stickerListString: stickersString, status: "Pending", progress: 0.0, post: "")
            UploadManager.sharedInstance.addTaskToArrayOfUploads(task: videoUploadTask)
            
            self.addRatingDoneAnalyticEvent(postData: postData, isVideo: true, stickerString: RatingsHelper().getImageStickersListForAnalyticsEvent(stickers: postData.selectedStickers!))
        }
    }
    
    func addRatingDoneAnalyticEvent(postData: PostData, isVideo: Bool, stickerString: String) {
        AnalyticsHelper.addRatingDoneEvent(postData: postData, isVideo: isVideo, stickerString: stickerString, screen: screen ?? "")
    }
    
    func showSuccessVCForFirstPost() {
        let profileDataClass = ProfileDataClass.init()
        let hasDoneRating = UserDefaultsUtility.shared.hasDoneRating()
        if let userCachedData = profileDataClass.getProfileCachedData(), let lifeTimeMetrics = userCachedData.userMetrics?.lifeTimeMetrics, let totalPosts = lifeTimeMetrics.totalPosts, !hasDoneRating {
            if totalPosts == 0 {
                self.ratingCameraViewControllerDelegate?.presentPostSuccessViewController()
                UserDefaultsUtility.shared.setHasDoneRating()
            }
        }
    }
    
}

extension RatingCameraViewController: CameraViewControllerDelegate {
    
    func cameraDidAddTextSticker(stickerText: String) {
        AnalyticsHelper.addRatingTextStickerAddedEvent(stickerText: stickerText)
    }
    func cameraDidSelectSticker(sticker: Sticker) {
        AnalyticsHelper.addRatingImageStickerAddedEvent(stickerName: sticker.name)
    }
    
    func cameraMultipleAssetsFromGallerySelected(videoCount: Int?, imageCount: Int?, totalCount: Int?) {
        AnalyticsHelper.addRatingMedia(type: nil, liveShot: true, video: videoCount, image: imageCount, totalCount: totalCount)
    }
    
    
    func cameraDidTapAction(name: String) {
        switch name {
        case "EditLocation":
            AnalyticsHelper.addRatingEditActionsAnalyticEvent(event: AnalyticsHelper.AddRatingEditEvents.addRatingEditPlace)
        case "EditRating":
            AnalyticsHelper.addRatingEditActionsAnalyticEvent(event: AnalyticsHelper.AddRatingEditEvents.addRatingEditRating)
        case "EditDraw":
            AnalyticsHelper.addRatingEditActionsAnalyticEvent(event: AnalyticsHelper.AddRatingEditEvents.addRatingEditDraw)
        default:
            break
        }
    }
    
    func cameraDiscardRating(multiple: Bool) {
        AnalyticsHelper.discardRatingAnalyticEvent(multiple: multiple)
    }
    
    
    func cameraDidSelectRating(rating: String, asset: CameraAsset?) {
        guard let cameraAsset = asset, let placeId = cameraAsset.selectedPlace?.id, let placeDetails = selectedPlaces[placeId] else { return }
        AnalyticsHelper.addRatingSelectRating(rating: rating, place: placeDetails.place!)
    }
    
    
    func cameraViewDidReceiveStickerSearch(text: String) {
        AnalyticsHelper.stickerSearchEvent(searchTerm: text)
    }
    
    func cameraDidDismiss() {
        errorView.removeFromSuperview()
        self.showSuccessVCForFirstPost()
    }
    
    func cameraDidAcceptAssets(_ assets: [CameraAsset]) {
        for (index, element) in assets.enumerated() {
            publishStory(asset: element, index: index)
        }
    }
    
    func permissionsGrantedFor(type: String, permission: String) {
        if type == "Camera" {
        AnalyticsHelper.appPermissionAnalyticEvent(eventName: AnalyticsHelper.PermissionsEventName.cameraPermission, permission: permission)
        } else {
          AnalyticsHelper.appPermissionAnalyticEvent(eventName: AnalyticsHelper.PermissionsEventName.microphonePermission, permission: permission)
        }
    }
    
}

extension RatingCameraViewController: LocationServiceProtocol {
    
    func didUpdateLocationsForBestAccuracy(locations: [CLLocation]) {
        if let loc = locations.last {
            self.locationAccuracy = loc.horizontalAccuracy
        }
    }
    
    func didUpdateLocationsForLessAccuracy(locations: [CLLocation]) {
        if let loc = locations.last {
            self.locationAccuracy = loc.horizontalAccuracy
        }
    }
    
}

extension RatingCameraViewController: ErrorViewDelegate {
    
    func errorViewTapped() {
        self.errorView.animateOutView()
        cameraLocationView?.retrySearch()
    }
}
