//
//  PostSuccessViewController.swift
//  Sminq
//
//  Created by Avinash Thakur on 08/03/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import UIKit
import Lottie
import SVProgressHUD

class PostSuccessViewController: UIViewController {
    
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var subHeaderLabel: UILabel!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet var stackView: UIStackView!
    
    @IBOutlet weak var levelLabel: UILabel!
    @IBOutlet weak var moderationLabel: UILabel!
    @IBOutlet weak var badgeImageView: UIImageView!
    @IBOutlet weak var claimRewardButton: UIButton!
    
    @IBOutlet weak var enableUpdatesButton: UIButton!
    
    var isBadgeUnlocked = false
    var newBadge: Badge?
    
    fileprivate lazy var claimRewardModalView = ClaimRewardModalView()
    private var profileModel: ProfileViewModel?
    let errorView = ErrorView()
    let claimRewardSuccessPopup = ClaimRewardPopupView()
    
    fileprivate lazy var animationView: AnimationView = {
        let animationView = AnimationView(name: "congrats-spark")
        animationView.frame = self.view.frame
        animationView.height = (self.view.frame.height * 0.42)
        animationView.width = 248
        animationView.center = self.view.center
        animationView.loopMode = .playOnce
        animationView.backgroundColor = .clear
        return animationView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        self.claimRewardsButtonUI()
        self.initUI()
        self.errorViewSetup()
        self.initObservers()
    }
    
    override public var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    fileprivate func initObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(handleUIApplicationWillEnterForeground), name: NSNotification.Name.UIApplicationWillEnterForeground, object: nil)
    }
    
    func initUI() {
        enableUpdatesButtonUI()
        checkEnableUpdatesButtonVisibility()
        
        levelLabel.isHidden = true
        moderationLabel.isHidden = true
        self.view.backgroundColor = AppConstants.SminqColors.blackDark100
        if isBadgeUnlocked {
            badgeImageView.isHidden = false
            headerLabel.isHidden = true
            levelLabel.isHidden = false
            moderationLabel.isHidden = false
        } else {
            badgeImageView.isHidden = true
            headerLabel.isHidden = false
        }
        
//        let newBadgeURL = URL(string: "https://www.getmarkk.com/wp-content/themes/markk-v1/img/logo.svg")
//        print("newBadgeURL:", newBadgeURL)
//
//        self.badgeImageView.sd_setImage(with: newBadgeURL) { (_, _, _, _) in
//            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
//                UIView.transition(with: self.badgeImageView, duration: 2, options: .transitionFlipFromRight, animations: {
//                    self.badgeImageView.sd_setImage(with: newBadgeURL)
//                })
//            }
//        }
//
//        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
//            UIView.transition(with: self.badgeImageView, duration: 2, options: .transitionFlipFromRight, animations: {
//                self.badgeImageView.sd_setImage(with: newBadgeURL)
//            })
//        }
        
        flipBadgeImage()
        
        stackView.addBackground(color: .clear, cornerRadius: 0)
        
        headerLabel.font = UIFont(name: AppConstants.Font.primaryRegular, size: 24)
        headerLabel.textColor = AppConstants.SminqColors.white100
        
        subHeaderLabel.font = UIFont(name: AppConstants.Font.primaryRegular, size: 16)
        subHeaderLabel.textColor = AppConstants.SminqColors.white100
        
        headerLabel.text = "Congrats on your \n first live rating!"
        subHeaderLabel.isHidden = true
        
//        levelLabel.text = "Congrats on unlocking \n Markker Level 1!"
//        levelLabel.font = UIFont(name: AppConstants.Font.primaryRegular, size: 14)
        levelLabel.textColor = AppConstants.SminqColors.white100
        levelLabel.numberOfLines = 2
        
        moderationLabel.text = "Ratings are subject to moderation \n and may affect the levels unlocked."
        moderationLabel.font = UIFont(name: AppConstants.Font.primaryRegular, size: 12)
        moderationLabel.textColor = AppConstants.SminqColors.white100.withAlphaComponent(0.5)
        moderationLabel.numberOfLines = 2

        self.animationView.play()
        self.view.addSubview(self.animationView)
    }
    
    fileprivate func checkEnableUpdatesButtonVisibility() {
        UserNoficationService.shared.getNotificationSettings { (settings) in
            var shouldShowEnableUpdatesButton = false
            switch settings {
            case .notDetermined, .denied:
                shouldShowEnableUpdatesButton = true
            default: break
                
            }
            
            if self.isBadgeUnlocked {
                self.enableUpdatesButton.isHidden = true
            } else {
                self.enableUpdatesButton.isHidden = !shouldShowEnableUpdatesButton
            }
            
        }
    }
    
    func claimRewardsButtonUI() {
        claimRewardButton.setTitle("CLAIM REWARDS", for: .normal)
        claimRewardButton.titleLabel?.font = UIFont(name: AppConstants.Font.primaryBold, size: 14)
        claimRewardButton.setTitleColor(AppConstants.SminqColors.white100, for: .normal)
        claimRewardButton.backgroundColor = AppConstants.SminqColors.green200
        claimRewardModalView.delegate = self
        claimRewardSuccessPopup.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        claimRewardSuccessPopup.isHidden = true
        claimRewardSuccessPopup.delegate = self
        self.view.addSubview(claimRewardSuccessPopup)
        self.getCachedProfileDetails()
    }
    
    func enableUpdatesButtonUI() {
        enableUpdatesButton.isHidden = true
        enableUpdatesButton.setTitle("ENABLE UPDATES", for: .normal)
        enableUpdatesButton.titleLabel?.font = UIFont(name: AppConstants.Font.primaryBold, size: 14)
        enableUpdatesButton.setTitleColor(AppConstants.SminqColors.white100, for: .normal)
        enableUpdatesButton.backgroundColor = AppConstants.SminqColors.green200
    }
    
    func errorViewSetup() {
        errorView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: self.getSafeAreaInsets().top + 56)
        errorView.setStackViewTopConstraint(top: self.getSafeAreaInsets().top)
        self.view.addSubview(errorView)
        errorView.isHidden = true
        errorView.delegate = self
    }
    
    func getCachedProfileDetails() {
            self.profileModel = ProfileViewModel.init(displayName: Helper().getUserDisplayName(), isMyProfile: true)
            self.profileModel?.getUserProfileDetails(enableCache: true, completion: { (apiResponseState, networkError, error) in
                if error != nil {
                    self.errorView.animateInViewWithMessage(error?.errorTitle ?? AppConstants.ErrorMessages.opsSomethingWentWrong, error?.errorDescription ?? "")
                } else {
                    self.checkAndShowRewardClaimButton()
                    self.errorView.animateOutView()
                }
            })
    }
    
    func checkAndShowRewardClaimButton() {
        claimRewardButton.isHidden = true
        if let (claim, _) = profileModel?.checkIfUserHasRewardsToClaim() {
            if claim {
                claimRewardButton.isHidden = false
            }
        }
    }
    
    // MARK: Fileprivate functions
    
    fileprivate func flipBadgeImage() {
        
        guard let badge = newBadge else { return }
        print("the badge name: \(badge.name)")
        levelLabel.attributedText = getAttributedTextForLevel(newBadgeLevelName: badge.name)
        guard let activeImage = badge.activeImage, let url = URL(string: activeImage) else { return }
        self.badgeImageView.sd_setImage(with: url)
        
        // NOTE: New badge image
        guard let newBadgeURL = URL(string: badge.activeImage ?? "") else { return }
//        let newBadgeURL = URL(string: "https://www.getmarkk.com/wp-content/themes/markk-v1/img/logo.svg")
        print("newBadgeURL:", newBadgeURL)
        
        
        // Animate current with the new image by fip
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            UIView.transition(with: self.badgeImageView, duration: 2, options: .transitionFlipFromRight, animations: {
                self.badgeImageView.sd_setImage(with: newBadgeURL)
            })
        }
    }
    
    fileprivate func getAttributedTextForLevel(newBadgeLevelName: String) -> NSAttributedString {
        let attributedText = NSMutableAttributedString(string: "Congrats on unlocking", attributes: [.font: UIFont.init(name: AppConstants.Font.primaryRegular, size: 14) as Any])
            
        attributedText.append(NSMutableAttributedString(string: "\n \(newBadgeLevelName)", attributes: [.font: UIFont.init(name: AppConstants.Font.primaryBold, size: 14) as Any]))
        
        return attributedText
    }
    
    @IBAction func closeButtonTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func enableUpdatesButtonTapped(_ sender: Any) {
        UserNoficationService.shared.getNotificationSettings { (settings) in
            switch settings {
            case .authorized, .provisional: break
            case .notDetermined: UserNoficationService.shared.authorize(completion: { (granted) in
                if granted {
                    AnalyticsHelper.appPermissionAnalyticEvent(eventName: AnalyticsHelper.PermissionsEventName.notificationPermission, permission: AppConstants.granted)
                    DispatchQueue.main.async {
                        self.claimRewardButton.isHidden = true
                    }
                }
            })
            case .denied: UIApplication.openAppSettings()
                AnalyticsHelper.appPermissionAnalyticEvent(eventName: AnalyticsHelper.PermissionsEventName.notificationPermission, permission: AppConstants.denied)
            }
        }
    }
    
    @IBAction func claimRewardButtonTapped(_ sender: UIButton) {
        AnalyticsHelper.claimRewardTapAnalyticsEvent(screen: AppConstants.screenNames.congrats)
        guard let user = Helper().getUser() else { return }
        self.showClaimRewardPopup(email: user.email)
    }
    
    func showClaimRewardPopup(email: String) {
        guard let window = UIApplication.shared.keyWindow else { return }
        window.addSubview(claimRewardModalView)
        claimRewardModalView.animateInView(email: email, screen: AppConstants.screenNames.congrats)
    }
    
    func claimRewardsFor(email: String) {
        SVProgressHUD.show()
        self.profileModel?.claimRewards(emailID: email, completion: { (apiResponseState, networkError, error) in
            SVProgressHUD.dismiss()
            if error != nil {
                self.errorView.animateInViewWithMessage(error?.errorTitle ?? AppConstants.ErrorMessages.opsSomethingWentWrong, error?.errorDescription ?? "")
            } else {
                self.claimRewardSuccessPopup.animateInView()
                self.claimRewardButton.isHidden = true
            }
        })
    }
    
    // MARK: @objc functions
    @objc func handleUIApplicationWillEnterForeground() {
        checkEnableUpdatesButtonVisibility()
    }
    
}

extension PostSuccessViewController: ClaimRewardModalViewDelegate {
    
    func claimReward(forEmail: String) {
        self.claimRewardsFor(email: forEmail)
    }
    
    func didOverlayTap() {
    
    }
    
    func didViewRemove() {
    }
    
    func didAnimateOutInitiate() {
    }
    
    func didEnterInvalidEmail(email: String) {
        let cancelButtonAction = UIAlertAction(title: AppConstants.cancel, style: .cancel)
        self.presentAlertSheet(title: "", message: "Please enter a valid email", options: [cancelButtonAction], controllerStyle: .alert, onWindow: true) { (action) in
        }
    }
}

extension PostSuccessViewController: ErrorViewDelegate {
    
    func errorViewTapped() {
        
    }
    
}

extension PostSuccessViewController: ClaimRewardPopupViewDelegate {
    
    func popupViewTapped() {
        self.claimRewardSuccessPopup.animateOutView()
    }
    
}

