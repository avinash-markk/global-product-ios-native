//
//  ProfileViewController.swift
//  Sminq
//
//  Created by Avinash Thakur on 04/03/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import UIKit
import AccountKit
import GoogleSignIn
import FirebaseAuth
import MarkkCamera
import SVProgressHUD
import MessageUI

protocol ProfileViewControllerDelegate: NSObjectProtocol {
    func resumeStoriesFromProfileViewController()
    func removeFullScreenOverlayFromStories()
    func dismissedProfile()
}

extension ProfileViewControllerDelegate {
    // Optional method
    func resumeStoriesFromProfileViewController() {
    }
    
    func dismissedProfile() {
    }
    
}

class ProfileViewController: UIViewController {
    
    @IBOutlet weak var profileTableView: UITableView!
    @IBOutlet weak var settingsButton: UIButton!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var topBar: UIView!
    @IBOutlet weak var topCenterView: UIView!
    
    var refreshControl = UIRefreshControl()
    let errorView = ErrorView()
    var ratingsSectionHeight: Float = 298.0
    var accountKit: AccountKit?
    var isMyProfile: Bool = true
    var displayName: String = Helper().getUserDisplayName()
    var source: String? = ""
    var presentingController: Bool? = false
    var navigationTitleLabel = UILabel()
    let claimRewardSuccessPopup = ClaimRewardPopupView()
    let enlargedDPView = EnlargedDPView()
    var isToolTipShown: Bool = false
    var checkIncompleteProfileToolTip: Bool = true
    var lastContentOffset: CGFloat = 0
    var currentTabIndex: Int = 0
    
    public weak var delegate: ProfileViewControllerDelegate?
    
    private var profileModel: ProfileViewModel?
    private var archiveRatingsModel: ArchiveRatingsViewModel?
    
    lazy var overlayLabel: UILabel = {
        let label = UILabel()
        label.frame = CGRect(x: 0, y: 0, width: self.view.width, height: 40)
        label.text = "WE SHOW ARCHIVED RATINGS FROM THE LAST 30 DAYS ONLY."
        label.font = UIFont(name: AppConstants.Font.primaryBold, size: 12)
        label.textColor = AppConstants.SminqColors.white.withAlphaComponent(0.5)
        label.numberOfLines = 0
        label.textAlignment = NSTextAlignment.center
        return label
    }()
    
    fileprivate let gradientView = CustomArchiveGradient(colors: [AppConstants.SminqColors.pinkishRed.withAlphaComponent(0).cgColor, AppConstants.SminqColors.pinkishRed.withAlphaComponent(0.25).cgColor, AppConstants.SminqColors.pinkishRed.withAlphaComponent(0.50).cgColor], locations: [0.0, 0.69, 1.0])
    
    fileprivate lazy var claimRewardModalView = ClaimRewardModalView()
    
    override public var prefersStatusBarHidden: Bool {
        return false
    }
    
    override public var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.setupNavigationBar()
        setNeedsStatusBarAppearanceUpdate()
        getProfileDetails()
        getArchivedRatings()
        Helper().setVisibleScreen(screen: AppConstants.screenNames.profile)
        self.navigationController?.checkAndDisplayBadgeForIncompleteProfile(data: profileModel?.userProfileData)
        checkIncompleteProfileToolTip = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        checkForAuthTokenNullity()
    }
    
    func initUI() {
        self.initTopBarUI()
        self.addProfileTabAnalytics()
        self.profileTableViewSetup()
        self.errorViewSetup()
        self.addNotificationObservers()
        self.addGradientOverlay()
        self.claimRewardViewSetup()
        self.enlargedDPViewSetup()
    }
    
    func initTopBarUI() {
        self.view.backgroundColor = AppConstants.SminqColors.blackDark100
        self.topCenterView.backgroundColor = UIColor.clear
         backButton.isHidden = true
        settingsButton.isHidden = !isMyProfile
        if (navigationController?.viewControllers.count)! > 1 || self.presentingController! {
            backButton.isHidden = false
        }
        topBar.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(topBarTapped)))
    }
    
    func addNotificationObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(addProfileTabAnalytics), name: NSNotification.Name(rawValue: AppConstants.AppNotificationNames.profileTab), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(profileTabSelectedTwice), name: NSNotification.Name(rawValue: AppConstants.AppNotificationNames.profileTabSelectedTwice), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(updateNetworkStatus), name: NSNotification.Name(rawValue: AppConstants.AppNotificationNames.networkReachable), object: nil)
    }
    
    private func scrollToTop() {
        self.profileTableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: true)
    }
    
    @objc func profileTabSelectedTwice(_ notification: NSNotification) {
        self.scrollToTop()
    }
    
    @objc func addProfileTabAnalytics() {
        if isMyProfile {
            navigationTitleLabel.text = ""
            AnalyticsHelper.viewProfileEvent(screen: AppConstants.screenNames.profile, source: source!, userId: Helper().getUserId(), isSelf: true)
        } else {
            navigationTitleLabel.text = "\(displayName)"
            AnalyticsHelper.viewProfileEvent(screen: AppConstants.screenNames.profile, source: source!, userId: self.profileModel?.userProfileData?._id ?? "", isSelf: false)
        }
    }
    
    @objc func updateNetworkStatus(notification: Notification) {
        guard let status = notification.userInfo?["status"] as? Bool else {
            return
        }
        if status {
            self.errorView.animateOutView()
        } else {
            self.errorView.animateInViewWithMessage(AppConstants.ErrorMessages.noInternet.uppercased(), AppConstants.ErrorMessages.noInterneSubHeading)
        }
    }
    
    func profileTableViewSetup() {
        profileTableView.addSubview(refreshControl)
        refreshControl.addTarget(self, action: #selector(refresh(_:)), for: .valueChanged)
        profileTableView.estimatedRowHeight = 600
        profileTableView.rowHeight = UITableViewAutomaticDimension
        profileTableView.separatorStyle = UITableViewCellSeparatorStyle.none
        self.profileTableView.backgroundColor = AppConstants.SminqColors.blackDark100
        self.registerTableViewCells()
    }
    
    func registerTableViewCells() {
        profileTableView.register(UINib.init(nibName: "ProfileBioTableViewCell", bundle: nil), forCellReuseIdentifier: "ProfileBioTableViewCell")
        profileTableView.register(UINib.init(nibName: "ProfileTabsCell", bundle: nil), forCellReuseIdentifier: "ProfileTabsCell")
    }
    
    func errorViewSetup() {
        errorView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: self.getSafeAreaInsets().top + 56)
        errorView.setStackViewTopConstraint(top: self.getSafeAreaInsets().top)
        self.view.addSubview(errorView)
        errorView.isHidden = true
        errorView.delegate = self
    }
    
    func claimRewardViewSetup() {
        claimRewardModalView.delegate = self
        claimRewardSuccessPopup.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        claimRewardSuccessPopup.isHidden = true
        claimRewardSuccessPopup.delegate = self
        self.view.addSubview(claimRewardSuccessPopup)
    }
    
    func enlargedDPViewSetup() {
        enlargedDPView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        enlargedDPView.isHidden = true
    }
    
    func setupNavigationBar() {
        // Setting Button
        self.navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.barTintColor = AppConstants.SminqColors.blackDark100
        navigationTitleLabel = UILabel(frame: CGRect(x: 44, y: 0, width: self.view.frame.width - (88), height: 44))
        
        if (navigationController?.viewControllers.count)! > 1 || self.presentingController! {
            let backButton: UIButton = UIButton(type: UIButtonType.custom)
            backButton.setImage(UIImage(named: "iconBack24White100"), for: .normal)
            backButton.frame = CGRect(x: -20, y: 0, width: 56, height: 44)
            backButton.isUserInteractionEnabled = false
            
            let view = UIView(frame: CGRect(origin: .zero, size: CGSize(width: 56, height: 44)))
            view.addSubview(backButton)
            view.isUserInteractionEnabled = true
            view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tappedBackButton)))
            
            navigationItem.leftBarButtonItem = UIBarButtonItem(customView: view)
            
            navigationTitleLabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.view.frame.width - (140), height: 44))
            self.navigationController?.isNavigationBarHidden = false
            self.hideTabBar()
        } else {
            self.navigationController?.isNavigationBarHidden = true
            self.showTabBar()
        }
        self.navigationController?.isNavigationBarHidden = true
        navigationTitleLabel.font = UIFont(name: AppConstants.Font.primaryRegular, size: 20.0)
        navigationTitleLabel.textColor = AppConstants.SminqColors.white100
        navigationTitleLabel.textAlignment = NSTextAlignment.center
        navigationItem.titleView = navigationTitleLabel
        navigationTitleLabel.text = "Profile"
    }
    
    @objc func refresh(_ sender: AnyObject) {
        self.getProfileDetails()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            self.refreshControl.endRefreshing()
        }
    }
    
    @objc func tappedBackButton() {
        if self.presentingController! {
            self.dismiss(animated: true, completion: nil)
        } else {
            // TODO: Check TOP controller is StoriesViewController. If yes then only call resume delegate method
            self.delegate?.resumeStoriesFromProfileViewController()
            self.delegate?.removeFullScreenOverlayFromStories()
            self.delegate?.dismissedProfile()
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @objc func topBarTapped(touch: UITapGestureRecognizer) {
        let tableOffset = profileTableView.contentOffset
        if tableOffset.y == 0.0 {
            let touchPoint = touch.location(in: self.view)
            let rect = CGRect(x: topBar.centerX - 40, y: topBar.y, width: 80, height: topBar.height)
            if rect.contains(touchPoint) {
                self.loadEnlargedDP()
            }
        }
    }
    
    func loadEnlargedDP() {
        if let imageUrl = self.profileModel?.userProfileData?.imageUrl {
            if imageUrl != "" {
                guard let window = UIApplication.shared.keyWindow else { return }
                window.addSubview(enlargedDPView)
                self.enlargedDPView.animateInView(imageUrl: imageUrl)
            }
        }
    }
    
    @IBAction func backButtonTapped(_ sender: Any) {
        if self.presentingController! {
            self.dismiss(animated: true, completion: nil)
        } else {
            // TODO: Check TOP controller is StoriesViewController. If yes then only call resume delegate method
            self.delegate?.resumeStoriesFromProfileViewController()
            self.delegate?.removeFullScreenOverlayFromStories()
            self.delegate?.dismissedProfile()
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func addGradientOverlay() {
        gradientView.isUserInteractionEnabled = false
        self.gradientView.backgroundColor = .clear
        gradientView.frame = CGRect(x: self.view.frame.origin.x, y: self.view.frame.height - 314 - self.getBottomSafeArea() - 50, width: self.view.frame.size.width, height: 314)
        self.view.addSubview(gradientView)
        
        overlayLabel.frame = CGRect(x: 90, y: self.gradientView.frame.height - 32 - 30, width: self.gradientView.frame.size.width - 180, height: 30)
        gradientView.isHidden = true
        self.gradientView.addSubview(overlayLabel)
    }
    
    func showGradientOverlay(value: Bool) {
        gradientView.isHidden = !value
    }
    
     @IBAction func settingsButtonTapped(_ sender: Any) {
        let actionSheetController: UIAlertController = UIAlertController(title: "v\(UIApplication.shared.getAppVersion())", message: nil, preferredStyle: .actionSheet)
    
        actionSheetController.addAction(UIAlertAction(title: "Cancel", style: .cancel) { _ in
        })
        actionSheetController.addAction(UIAlertAction(title: "Edit Profile", style: .default) { _ in
            self.editProfileButtonTapped()
        })
        actionSheetController.addAction(UIAlertAction(title: "FAQs", style: .default) { _ in
            self.faqButtonTapped()
        })
        actionSheetController.addAction(UIAlertAction(title: "Feedback", style: .default) { _ in
            self.feedBackButtonTapped()
        })
        actionSheetController.addAction(UIAlertAction(title: "Logout", style: .destructive) { _ in
            self.logOutButtonTapped()
        })
        
        self.present(actionSheetController, animated: true, completion: nil)
    }
    
    func getProfileDetails() {
        if isMyProfile {
            self.displayName = Helper().getUserDisplayName()
        } else {
            SVProgressHUD.show()
        }
        
        self.profileModel = ProfileViewModel.init(displayName: "\(self.displayName)", isMyProfile: self.isMyProfile)
        
        self.profileModel?.getUserProfileDetails(enableCache: true, completion: { (apiResponseState, networkError, error) in
            if error != nil {
                self.errorView.animateInViewWithMessage(error?.errorTitle ?? AppConstants.ErrorMessages.opsSomethingWentWrong, error?.errorDescription ?? "")
            } else {
                self.profileTableView.reloadData()
                self.errorView.animateOutView()
            }
            
            if !self.isMyProfile {
                SVProgressHUD.dismiss()
            }
        })
    }
    
    func getArchivedRatings() {
        self.archiveRatingsModel = ArchiveRatingsViewModel(displayName: self.displayName)
        self.archiveRatingsModel?.getArchiveRatings(displayName: self.displayName, completion: { (apiResponseState, networkError, error) in
            if error != nil {
                self.errorView.animateInViewWithMessage(error?.errorTitle ?? AppConstants.ErrorMessages.opsSomethingWentWrong, error?.errorDescription ?? "")
            } else {
                if self.currentTabIndex == 2 {
                self.profileTableView.reloadData()
                }
            }
        })
    }
    
    func claimRewardsFor(email: String) {
        SVProgressHUD.show()
        self.profileModel?.claimRewards(emailID: email, completion: { (apiResponseState, networkError, error) in
            SVProgressHUD.dismiss()
            if error != nil {
                self.errorView.animateInViewWithMessage(error?.errorTitle ?? AppConstants.ErrorMessages.opsSomethingWentWrong, error?.errorDescription ?? "")
            } else {
                self.claimRewardSuccessPopup.animateInView()
                self.getProfileDetails()
            }
        })
    }
    
    func feedBackButtonTapped() {
        let mailComposer: MFMailComposeViewController = MFMailComposeViewController()
        mailComposer.mailComposeDelegate = self
        mailComposer.setSubject("Feedback for Markk from @\(displayName)")
        mailComposer.setToRecipients(["support@markk.app"])
        self.present(mailComposer, animated: true, completion: nil)
    }
    
    func logOutUser() {
        AnalyticsHelper.logoutEvent()
        UserSessionUtility.shared.clearUserSession()
        AppConstants.appDelegate.window?.rootViewController = UINavigationController(rootViewController: AppConstants.storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController)
    }
    
    func logOutButtonTapped() {
        let logoutAlertController: UIAlertController = UIAlertController(title: "Are you sure you want to logout ?", message: nil, preferredStyle: .actionSheet)
        logoutAlertController.addAction(UIAlertAction(title: "Cancel", style: .cancel) { _ in
        })
        logoutAlertController.addAction(UIAlertAction(title: "Logout", style: .default) { _ in
            self.logOutUser()
        })
        self.present(logoutAlertController, animated: true, completion: nil)
    }
    
    func editProfileButtonTapped() {
        AnalyticsHelper.editProfileEvent()
        
        if let userProfileData = self.profileModel?.userProfileData {
            if let editProfileViewController = AppConstants.storyboard.instantiateViewController(withIdentifier: "EditProfileViewController") as? EditProfileViewController {
                editProfileViewController.mobileVerified = userProfileData.mobileVerified ?? false
                editProfileViewController.emailVerified = userProfileData.emailVerified ?? false
                editProfileViewController.bio = userProfileData.bio
                editProfileViewController.hidesBottomBarWhenPushed = true
                
                self.hideTabBar()
                self.navigationController?.pushViewController(editProfileViewController, animated: true)
            }
        }
    }
    
    func faqButtonTapped() {
        if InternetConnection.isConnectedToInternet() {
            if let tcWebViewController = AppConstants.storyboard.instantiateViewController(withIdentifier: "TCWebViewController") as? TCWebViewController {
                tcWebViewController.strWebURL = Config.getFaqUrl()
                tcWebViewController.strTitle = "FAQ"
                self.navigationController?.navigationBar.isHidden = false
                self.navigationController?.pushViewController(tcWebViewController, animated: true)
            }
        } else {
            self.errorView.animateInViewWithMessage(AppConstants.ErrorMessages.noInternet.uppercased(), AppConstants.ErrorMessages.noInterneSubHeading)
        }
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.lastContentOffset = scrollView.contentOffset.y
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let height = scrollView.frame.size.height
        if (self.lastContentOffset < scrollView.contentOffset.y) {
            self.profileScrolled(upwards: true)
            let distanceFromBottom = scrollView.contentSize.height - self.lastContentOffset
            if distanceFromBottom < height {
                self.profileScrolledToBottom()
            } else {
                showGradientOverlay(value: false)
            }
        } else if (self.lastContentOffset > scrollView.contentOffset.y) {
            self.profileScrolled(upwards: false)
        } else {
            // didn't move
        }
    }
    
    func profileScrolled(upwards: Bool) {
        if upwards {
            UIView.animate(withDuration: 0.3) {
                self.topBar.backgroundColor = AppConstants.SminqColors.blackLight100
            }
            self.topCenterView.backgroundColor = AppConstants.SminqColors.white.withAlphaComponent(0.1)
        } else {
            UIView.animate(withDuration: 0.3) {
                self.topBar.backgroundColor = UIColor.clear
            }
            self.topCenterView.backgroundColor = UIColor.clear
        }
    }
    
    func profileScrolledToBottom() {
        if !isMyProfile && currentTabIndex == 2 && archiveRatingsModel?.archiveRatingsList.count ?? 0 > 0 {
            showGradientOverlay(value: true)
        } else {
            showGradientOverlay(value: false)
        }
    }
    
    func navigateToLiveRatings(place: LivePlaces) {
        let storyViewManager = StoryViewManager.init(screen: AppConstants.screenNames.profile)
        let storyViewController = StoryViewController.init(storyViewManager: storyViewManager)
        
        storyViewController.delegate = self
        
        let storyViewModel = StoryViewModel(screen: AppConstants.screenNames.storyView, storyList: (self.profileModel?.userRatingsData)!)!
        
        navigateToStoriesViewController(place: place, storyViewModel: storyViewModel, section: AppConstants.ViewRatingsSection.live)
    }

    func navigateToArchiveRatings(place: LivePlaces, feed: [UserDataPlaceFeed]) {
        var placeDetails: LivePlaces = place
        placeDetails.placeMoodScore = feed[0].moodScore

        let feedViewModel = FeedViewModel.init(screen: AppConstants.screenNames.storyView)
        feedViewModel?.setFeedList(feedList: feed)
        
        let storyData = StoryData.init(userId: nil, placeId: nil, placeDetails: placeDetails, archived: false, posts: feed, feedViewModel: feedViewModel)
        let storyViewModel = StoryViewModel.init(screen: AppConstants.screenNames.storyView, storyList: [storyData])
        
        navigateToStoriesViewController(place: placeDetails, storyViewModel: storyViewModel, section: AppConstants.ViewRatingsSection.archived)
    }

    private func navigateToStoriesViewController(place: LivePlaces, storyViewModel: StoryViewModel?, section: String) {
        let storyViewManager = StoryViewManager.init(screen: AppConstants.screenNames.profile)
        storyViewManager.section = section
        let storyViewController = StoryViewController.init(storyViewManager: storyViewManager)
        
        storyViewController.delegate = self
        
        if let storyViewModel = storyViewModel {
            storyViewController.storyViewModel = storyViewModel
        }
        
        storyViewController.selectedPlace = place
        
        let navController = UINavigationController(rootViewController: storyViewController)
        navController.modalPresentationStyle = .overFullScreen
        navController.modalPresentationCapturesStatusBarAppearance = true
        
        self.present(navController, animated: true, completion:  {
            self.hideTabBar()
        })
    }
    
    private func navigateToRatingCameraViewController() {
        SVProgressHUD.show()
        let cameraViewController = RatingCameraViewController()
        cameraViewController.galleryInterval = FIRRemoteConfigService.shared.getGalleryMediaAccessInterval()
        cameraViewController.ratingCameraViewControllerDelegate = self
        cameraViewController.screen = AppConstants.screenNames.home
        present(cameraViewController, animated: true, completion: nil)
    }
    
    
    // MARK: Fileprivate functions
    
    fileprivate func checkForAuthTokenNullity() {
        if UserDefaultsUtility.shared.getAuthToken() == "" {
            let alertService = AlertService()
            alertService.delgate = self
            
            alertService.showSessionExpieryWithOkButton(viewController: self)
        }
    }
    
    func reloadProfileTabSectionContents(index: Int) {
        currentTabIndex = index
        self.sendAnalyticsEvent()
        if index != 2 {
            self.showGradientOverlay(value: false)
        }
        profileTableView.reloadSections(IndexSet(integer: 1), with: .none)
    }
    
    func sendAnalyticsEvent() {
        switch currentTabIndex {
        case 0:
            break
        case 1:
            AnalyticsHelper.profileBadgesTabEvent()
        case 2:
            AnalyticsHelper.profileArchivesTabEvent()
        default:
            break
        }
    }
    
}

extension ProfileViewController: ErrorViewDelegate {
    
    func errorViewTapped() {
        self.errorView.animateOutView()
        self.getProfileDetails()
    }
    
}

extension ProfileViewController: StoryViewControllerDelegate {
    func didDismissStoryView() {
        
        if !self.hidesBottomBarWhenPushed {
            self.showTabBar()
        }
        
        self.getProfileDetails() // Call profile details API to update counts
    }
    
    func didUpvotePost(post: UserDataPlaceFeed) {
//        profileModel?.updatePost(post: post)
    }
}

extension ProfileViewController: UITableViewDelegate, UITableViewDataSource {
    
    public func numberOfSections(in tableView: UITableView) -> Int {
            return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return 1
        } else if section == 1 {
            return 40
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 1 {
            if let height = self.heightForTabSection() {
                return CGFloat(height)
            }
            return CGFloat(ratingsSectionHeight)
        }
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 1 {
            return getHeaderViewForProfileTabSection()
        }
        return nil
    }
    
    func heightForTabSection() -> CGFloat? {
        guard let profileModel = self.profileModel else { return 0 }
        switch currentTabIndex {
        case 0:
            return profileModel.getRatingsSectionHeight()
        case 1:
            return profileModel.getBadgesSectionHeight()
        case 2:
            guard let archiveModel = self.archiveRatingsModel else { return 0 }
            return archiveModel.getArchivesSectionHeight()
        default:
           return CGFloat(298)
        }
    }
    
    func getHeaderViewForProfileTabSection() -> UIView {
        let headerView = UIView.init(frame: CGRect(x: 0, y: 0, width: self.profileTableView.width, height: 40))
        let sectionView = ProfileSectionHeaderView.init(frame: CGRect(x: 0, y: 0, width: self.profileTableView.width, height: 40))
       sectionView.tabsMenuView.tabsArray = ["LIVE", "BADGES", "ARCHIVED"]
        sectionView.tabsMenuView.tabsMenuDelegate = self
        sectionView.tabsMenuView.tabsCollectionView.selectItem(at: IndexPath.init(item: currentTabIndex, section: 0), animated: true, scrollPosition: .centeredVertically)
        headerView.addSubview(sectionView)
        return headerView
    }
    
    func getProfileTabsCell(indexPath: IndexPath) -> UITableViewCell {
        guard let cell = profileTableView.dequeueReusableCell(withIdentifier: "ProfileTabsCell", for: indexPath) as? ProfileTabsCell else {
            return UITableViewCell()
        }
        cell.setProfileTabsData(profileModel: profileModel, archiveModel: archiveRatingsModel, tabIndex: currentTabIndex)
        cell.delegate = self
        return cell
    }
    
    func getProfileBioCell(indexPath: IndexPath) -> UITableViewCell {
        guard let cell = profileTableView.dequeueReusableCell(withIdentifier: "ProfileBioTableViewCell", for: indexPath) as? ProfileBioTableViewCell else {
            return UITableViewCell()
        }
        cell.delegate = self
        cell.updateProfileBioDetails(data: self.profileModel?.userProfileData, userMetrics: self.profileModel?.userMetricsData, myProfile: self.isMyProfile, incompleteProfileCheck: checkIncompleteProfileToolTip)
        cell.selectionStyle = .none
        cell.checkAndShowClaimReward(profileModel: self.profileModel)
        cell.toolTipVisibility(show: isToolTipShown)
        return cell
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
                if indexPath.section == 0 {
                    return self.getProfileBioCell(indexPath: indexPath)
                } else if indexPath.section == 1 {
                    return self.getProfileTabsCell(indexPath: indexPath)
        }
        return UITableViewCell()
    }
    
}

extension ProfileViewController: ProfileTabsCellDelegate {
    
    func createRatingTapped() {
        SVProgressHUD.show()
        let cameraViewController = RatingCameraViewController()
        cameraViewController.galleryInterval = FIRRemoteConfigService.shared.getGalleryMediaAccessInterval()
        cameraViewController.ratingCameraViewControllerDelegate = self
        cameraViewController.screen = AppConstants.screenNames.profile
        present(cameraViewController, animated: true, completion: nil)
        AnalyticsHelper.addRatingIntend(screen: AppConstants.screenNames.profile, section: "\(AppConstants.RatingsIntendSection.liveRatingsSection)", addToStory: "no", place: nil)
    }
    
    func archiveRatingsTapped(place: LivePlaces, placeFeed: [UserDataPlaceFeed]) {
        self.navigateToArchiveRatings(place: place, feed: placeFeed)
    }
    
    func updateTabOnSwipe(index: Int) {
       self.reloadProfileTabSectionContents(index: index)
    }
    
    func ratingsTapped(place: LivePlaces) {
        self.navigateToLiveRatings(place: place)
    }
    
}

extension ProfileViewController: TabsMenuDelegate {
    
    func menuTabBarDidSelectItemAt(menu: TabsMenuView, index: Int) {
        self.reloadProfileTabSectionContents(index: index)
    }
    
}

extension ProfileViewController: ProfileRatingsTableViewCellDelegate {
    
    func liveRatingsTapped(ratings: StoryData) {
        guard let place = ratings.placeDetails, let profileModel = profileModel else { return }
        let storyViewModel = StoryViewModel.init(screen: AppConstants.screenNames.storyView, storyList: profileModel.userRatingsData)
        self.navigateToStoriesViewController(place: place, storyViewModel: storyViewModel, section: AppConstants.ViewRatingsSection.live)
//        self.navigateToLiveRatings(place: place)
    }

}

extension ProfileViewController: ProfileBioTableViewCellDelegate {
    
    func profileImageTapped() {
        self.loadEnlargedDP()
    }
    
    func editProfileTooltipTapped() {
         self.editProfileButtonTapped()
    }
    
    func infoToolTipTapped() {
        
        self.isToolTipShown = !isToolTipShown
        if self.isToolTipShown {
            checkIncompleteProfileToolTip = false
        } else {
            checkIncompleteProfileToolTip = true
        }
        UIView.performWithoutAnimation {
            profileTableView.beginUpdates()
            profileTableView.reloadSections(IndexSet(integer: 0), with: .none)
            profileTableView.endUpdates()
        }
        
    }
    
    func claimRewardsButtonTapped(sender: UITableViewCell) {
        AnalyticsHelper.claimRewardTapAnalyticsEvent(screen: AppConstants.screenNames.profile)
        guard let user = Helper().getUser() else { return }
        self.showClaimRewardPopup(email: user.email)
    }
    
    func showClaimRewardPopup(email: String) {
        guard let window = UIApplication.shared.keyWindow else { return }
        window.addSubview(claimRewardModalView)
        claimRewardModalView.animateInView(email: email, screen: AppConstants.screenNames.profile)
    }
    
}

extension ProfileViewController: RatingCameraViewControllerDelegate {
    
    func presentPostSuccessViewController() {
        self.presentPostSuccessVC()
    }
    
}

extension ProfileViewController: AlertServiceDelegate {
    
    func didOkTap() {
        print("Ok button tapped")
        UserSessionUtility.shared.clearUserSession()
        AppConstants.appDelegate.window?.rootViewController = UINavigationController(rootViewController: AppConstants.storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController)
    }
    
}

extension ProfileViewController: ClaimRewardModalViewDelegate {
    
    func claimReward(forEmail: String) {
       self.claimRewardsFor(email: forEmail)
    }
    
    func didEnterInvalidEmail(email: String) {
        let cancelButtonAction = UIAlertAction(title: AppConstants.cancel, style: .cancel)
        self.presentAlertSheet(title: "", message: "Please enter a valid email", options: [cancelButtonAction], controllerStyle: .alert, onWindow: true) { (action) in
        }
    }
    
}

extension ProfileViewController: ClaimRewardPopupViewDelegate {
    
    func popupViewTapped() {
        self.claimRewardSuccessPopup.animateOutView()
    }
    
}

extension ProfileViewController: MFMailComposeViewControllerDelegate {
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        switch result {
        case MFMailComposeResult.cancelled:
            print("Mail cancelled")
        case MFMailComposeResult.saved:
            print("Mail saved")
        case MFMailComposeResult.sent:
            print("Mail sent")
        case MFMailComposeResult.failed:
            print("Mail sent failure")
        default:
            break
        }
        self.dismiss(animated: true, completion: nil)
    }
    
}
