//
//  TimelineViewController.swift
//  Sminq
//
//  Created by Pushkar Deshmukh on 05/03/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import UIKit
import CoreLocation
import Firebase
import MarkkCamera
import SafariServices
import PusherSwift

class TimelineViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    fileprivate lazy var subscribeModalView = SubscribeModalView()
    
    var nearByStoriesCardHeight: CGFloat = 298
    var sectionHeaderHeight: CGFloat = 40
    
    let locationPermissionView = LocationPermissionView()
    let errorView = ErrorView()
    
    let locationManager = UserLocationManager.SharedManager
    var placeStoryViewModel = PlaceStoryViewModel.init(screen: AppConstants.screenNames.home)!
    var bannerViewModel = BannerViewModel.init(screen: AppConstants.screenNames.home)
    var userStoriesViewModel = ProfileViewModel.init(displayName: Helper().getUserDisplayName(), isMyProfile: true)
    var storyViewTrackViewModel = StoryViewTrackViewModel(screen: AppConstants.screenNames.storyView)!
    var refreshControl = UIRefreshControl()
    
    var pusher: Pusher?
    
    var loadingNearByStories = false
    var errorLoadingNearByStories = false
    
    var loadingBanners = false
    var errorLoadingBanners = false
    
    var loadingUserStories = false
    var errorLoadingUserStories = false
    var showSubscribeBanner: Bool = false
    
    // NOTE: loadedAllData flag
    // Reset this flag/ Set to false, when authorisation is changed.
    // didChangeAuthorization is called whenever CLlocation is initialised and authorisation is not changed.
    // So to prevent calling loadAllData(), this flag is added
    var loadedAllData = false
    
    var nearByCellSpacing: CGFloat = 16
    
    let subscribeCardImageHeight:CGFloat = ((UIScreen.main.bounds.width - 32)/2.17)
    
    var viewDidAppearFlag: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.initDelegates()
        self.initUI()
        self.locationPermissionViewSetup()
        self.errorViewSetup()
        self.initObservers()
        self.setupPusher()
        
        if locationManager.isLocationServiceEnabled() && locationManager.isLocationServiceAuthorized() {
            // NOTE: This check is added for devices where location permission are cached even after reinstalling app. This is observed only for Pushkar's phone so far
            // So set flag for nearByStories so that modal doesn't show up
            UserDefaults.standard.set(true, forKey: AppConstants.UserDefaultsKeys.nearByStoriesTapped)
        }
        
        self.checkLocationPermission()
        checkSubscribeBannerVisibility()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        Helper().setVisibleScreen(screen: AppConstants.screenNames.home)
        self.navigationController?.navigationBar.isHidden = true
        
        if locationManager.isLocationServiceEnabled() && locationManager.isLocationServiceAuthorized() {
            showTabBar()
        }
       self.checkForIncompleteProfile()
    }
    
    func checkForIncompleteProfile() {
        let profile = ProfileDataClass()
        let profileData = profile.getProfileCachedData()
        self.navigationController?.checkAndDisplayBadgeForIncompleteProfile(data: profileData?.user)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.navigationController?.navigationBar.isHidden = true
        
        checkForAuthTokenNullity()
        
        viewDidAppearFlag = true
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        viewDidAppearFlag = false
    }
    
    override public var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewSafeAreaInsetsDidChange() {
        self.locationPermissionView.setBottomViewBottomConstraint(bottomConstraintConstant: self.getSafeAreaInsets().bottom)
        self.errorView.setStackViewTopConstraint(top: self.getSafeAreaInsets().top)
    }
    
    // MARK: Private functions
    
    
    fileprivate func concatenateChunks(chunks: [Int: String]?) -> String? {
        guard let chunks = chunks else { return "" }
        
        var tempString = ""
        let sortedArrayChunks = sortChunksIntoArray(chunks: chunks)
        
        for (_, chunk) in sortedArrayChunks {
            tempString = tempString + chunk
        }
        
        return tempString
    }
    
    fileprivate func sortChunksIntoArray(chunks: [Int: String]) -> [(key: Int, value: String)] {
        return chunks.sorted(by: { $0.0 < $1.0 })
    }
    
    fileprivate func setupPusher() {
        let options = PusherClientOptions(
            host: .cluster(Config.getPusherAppCluster())
        )
        
        pusher = Pusher(key: Config.getPusherAppKey(), options: options)
        pusher?.connect()
//        subscribeToPusherEvent()
        bindWithChunking(channel: AppConstants.PusherChannels.markkTrending, eventName: AppConstants.PusherEvents.trending) { [weak self] (dataString) in
            guard let this = self else { return }
            print("bindWithChunking")
//            print(dataString)
            
            if let dataString = dataString, let json = Helper().stringToJsonDictionaryConversion(inputString: dataString) {
                this.handlePusherData(data: json)
            }
        }
    }
    
//    fileprivate func subscribeToPusherEvent() {
//        guard let pusher = pusher else { return }
//        let bannerChannel = pusher.subscribe(AppConstants.PusherChannels.markkTrending)
//        let _ = bannerChannel.bind(eventName: AppConstants.PusherEvents.trending) { [weak self] (data) in
//            guard let this = self else { return }
//            DispatchQueue.main.async {
//                this.handlePusherData(data: data)
//
//                if this.shouldReloadTableViewForPusher() {
//                    UIView.performWithoutAnimation {
//                        this.tableView.reloadSections(IndexSet(integer: 2), with: UITableViewRowAnimation.automatic)
//                    }
//                }
//            }
//        }
//    }
    
    fileprivate func bindWithChunking(channel: String, eventName: String, completion: @escaping(_ dataString: String?) -> Void) {
        guard let pusher = pusher else { return }
        
        let channel = pusher.subscribe(channel)
        var events: [Int: [String: Any]] = [:]
        
        let _ = channel.bind(eventName: "\(eventName)") { [weak self] (data) in
            guard let this = self else { return }
            guard let pusherChunk = PusherChunk.init(data: data) else { return }
            
            if events[pusherChunk.messageId] == nil {
                events[pusherChunk.messageId] = ["chunks": [Int: String](), "receivedFinal": false, "totalChunks": Int()]
            }
            
            var ev = events[pusherChunk.messageId]
            var chunks = ev?["chunks"] as? [Int: String]

            chunks?[pusherChunk.offset] = pusherChunk.chunk
            
            events[pusherChunk.messageId]?["chunks"] = chunks
            events[pusherChunk.messageId]?["totalChunks"] = pusherChunk.totalChunks
            
            if pusherChunk.eof {
                events[pusherChunk.messageId]?["receivedFinal"] = true
            }
            
            if let receivedFinal = events[pusherChunk.messageId]?["receivedFinal"] as? Bool, receivedFinal, chunks?.count == pusherChunk.totalChunks {
                completion(this.concatenateChunks(chunks: chunks))
                events.removeValue(forKey: pusherChunk.messageId)
            }
            
        }
    }
    
    fileprivate func shouldReloadTableViewForPusher() -> Bool {
        var shouldReload = true
        
        if viewDidAppearFlag {
            let cells = tableView.visibleCells
            
            cells.forEach { (cell) in
                if let _ = cell as? TrendingTableViewCell {
                    shouldReload = false
                }
            }
        }
        
        return shouldReload
    }
    
    fileprivate func handlePusherData(data: Any?) {
        guard let data = data else { return }
        print("data from pusher channel")
        if let banner = Helper().anyToBanner(data: data) {
            print("================================ ================================ ================================")
            print(banner.type, " ", banner.label, " ", banner.threshold, " ", banner.icon)
//            print(banner.label)
//            print(banner.threshold)
            
            if self.shouldReloadTableViewForPusher() {
                bannerViewModel.handlePusherUpdate(newBanner: banner)
                
                UIView.performWithoutAnimation {
                    self.tableView.reloadSections(IndexSet(integer: 2), with: UITableViewRowAnimation.automatic)
                }
            }

            
        }
    }
    
    private func scrollToTop() {
        self.tableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: true)
    }

    
    private func initObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(updateNotificationCount), name: NSNotification.Name(rawValue: AppConstants.AppNotificationNames.updateNotification), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(timelineTabSelectedTwice), name: NSNotification.Name(rawValue: AppConstants.AppNotificationNames.timelineTabSelectedTwice), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(reloadUserStoriesAndNearbyStories), name: NSNotification.Name(rawValue: AppConstants.AppNotificationNames.deletedFeedObserver), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(reloadUserStoriesAndNearbyStories(_:)), name: NSNotification.Name(AppConstants.AppNotificationNames.uploadDone), object: nil)
         NotificationCenter.default.addObserver(self, selector: #selector(updateNetworkStatus), name: NSNotification.Name(rawValue: AppConstants.AppNotificationNames.networkReachable), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(addHomeTabAnalytics), name: NSNotification.Name(rawValue: AppConstants.AppNotificationNames.homeTab), object: nil)
    }
    
    private func checkLocationPermission() {
        let hadUserTappedOnNearByStories = UserDefaults.standard.value(forKey: AppConstants.UserDefaultsKeys.nearByStoriesTapped)
        
        if hadUserTappedOnNearByStories != nil {
            if !locationManager.isLocationServiceEnabled() || !locationManager.isLocationServiceAuthorized() {
                // TODO: Block user here
                self.showLocationPermissionView()
                self.locationPermissionView.updateUIForPermissionDenied()
            } else {
                locationManager.setCurrentUserLocationToDefaults()
                self.loadAllData(fromPullDownToRefresh: false)
                
                locationManager.requestLocationWithAccuracy(accuracy: kCLLocationAccuracyBest) // Update best accurate location and cache it asynchronously
            }
        } else {
            self.showLocationPermissionView()
            self.locationPermissionView.defaultUIForLcationPermission()
        }
    }
    
    private func initDelegates() {
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        self.locationManager.delegate = self
        
        subscribeModalView.delegate = self
    }
    
    private func initUI() {
        self.view.backgroundColor = AppConstants.SminqColors.blackDark100
        
        self.tableView.backgroundColor = AppConstants.SminqColors.blackDark100
        
        self.tableView.register(UINib(nibName: "BannerTableViewCell", bundle: nil), forCellReuseIdentifier: "BannerTableViewCell")
        self.tableView.register(UINib(nibName: "AchievedBadgesTableViewCell", bundle: nil), forCellReuseIdentifier: "AchievedBadgesTableViewCell")
        
        self.tableView.register(UINib(nibName: "EmptyTableViewCell", bundle: nil), forCellReuseIdentifier: "EmptyTableViewCell")
        self.tableView.register(UINib(nibName: "ExpiredStoryTableViewCell", bundle: nil), forCellReuseIdentifier: "ExpiredStoryTableViewCell")
        self.tableView.register(UINib(nibName: "EmptyStoryStateTableViewCell", bundle: nil), forCellReuseIdentifier: "EmptyStoryStateTableViewCell")
        self.tableView.register(UINib(nibName: "UserLiveStoriesTableViewCell", bundle: nil), forCellReuseIdentifier: "UserLiveStoriesTableViewCell")
        self.tableView.register(UINib(nibName: "MoodsTableViewCell", bundle: nil), forCellReuseIdentifier: "MoodsTableViewCell")
        self.tableView.register(UINib(nibName: "NearByStoriesTableViewCell", bundle: nil), forCellReuseIdentifier: "NearByStoriesTableViewCell")
        self.tableView.register(UINib(nibName: "UserStoriesSkeletonTableViewCell", bundle: nil), forCellReuseIdentifier: "UserStoriesSkeletonTableViewCell")
        self.tableView.register(UINib(nibName: "MoodsSkeletonViewTableViewCell", bundle: nil), forCellReuseIdentifier: "MoodsSkeletonViewTableViewCell")
        self.tableView.register(UINib(nibName: "NoNearbyStoriesForSelectedFilters", bundle: nil), forCellReuseIdentifier: "NoNearbyStoriesForSelectedFilters")
        self.tableView.register(UINib(nibName: "SubscribeTableViewCell", bundle: nil), forCellReuseIdentifier: "SubscribeTableViewCell")
        self.tableView.register(UINib(nibName: "WeekEndHighlightsTableViewCell", bundle: nil), forCellReuseIdentifier: "WeekEndHighlightsTableViewCell")
        self.tableView.register(UINib(nibName: "TrendingTableViewCell", bundle: nil), forCellReuseIdentifier: "TrendingTableViewCell")
        self.tableView.register(UINib(nibName: "BannerEmptyState", bundle: nil), forCellReuseIdentifier: "BannerEmptyState")
        
    
        self.tableView.addSubview(refreshControl)
        self.refreshControl.addTarget(self, action: #selector(refresh(_:)), for: .valueChanged)
        
        NotificationCenter.default.addObserver(self, selector: #selector(handleUIApplicationWillEnterForeground), name: NSNotification.Name.UIApplicationWillEnterForeground, object: nil)
        
        viewDidAppearFlag = true
    }

    // MARK: Target methods
    
    @objc func handleUIApplicationWillEnterForeground() {
        checkSubscribeBannerVisibility()
        subscribeModalView.updateButtonStates()
    }
    
    @objc func handleNotificationTap() {
        navigateToNotificationsViewController()
    }
    
    @objc func handleSearchButtonTap() {
        navigateToSearchViewController()
    }
    
    @objc func reloadUserStories(_ notification: NSNotification) {
        self.getUserStories(fromPullDownToRefresh: true)
    }
    
    @objc func reloadUserStoriesAndNearbyStories(_ notification: NSNotification) {
        // Feed deleted. Hence reload API call to update nearby section.
        self.getNearbyAndUserStories(fromPullDownToRefresh: true)
    }
    
    @objc func timelineTabSelectedTwice(_ notification: NSNotification) {
        self.scrollToTop()
    }
    
    @objc func updateNotificationCount() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            self.tableView.reloadSections([0], with: .none)
        }
    }
    
    @objc func refresh(_ sender: AnyObject) {
        self.loadAllData(fromPullDownToRefresh: true)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            self.refreshControl.endRefreshing()
        }
    }
    
    @objc func updateNetworkStatus(notification: Notification) {
        guard let status = notification.userInfo?["status"] as? Bool else {
            return
        }
        if status {
            self.errorView.animateOutView()
        } else {
            self.errorView.animateInViewWithMessage(AppConstants.ErrorMessages.noInternet.uppercased(), AppConstants.ErrorMessages.noInterneSubHeading)
        }
    }
    
    @objc func addHomeTabAnalytics(notification: Notification) {
        AnalyticsHelper.viewHomeEvent()
    }
    
    // MARK: Public functions
    
    func loadAllData(fromPullDownToRefresh: Bool) {
        checkSubscribeBannerVisibility()
        checkForAuthTokenNullity()
        updateStoryViewCounts()
        
        if let userLocation = UserDefaults.standard.value(forKey: "UserLocation") {
            let latitude = (userLocation as AnyObject).value(forKey: "latitude") as? Double
            let longitude = (userLocation as AnyObject).value(forKey: "longitude") as? Double
            
            self.hideLocationPermissionView()
            
            let loc = CLLocation(latitude: latitude!, longitude: longitude!)
            
            let distance = FIRRemoteConfigService.shared.getNearyByDistanceInMeteres()
            
            if let lat = latitude, let long = longitude {
                self.getNearyByStories(latitude: lat, longitude: long, distance: distance, fromPullDownToRefresh: fromPullDownToRefresh)
            }
            
            self.getBanners(fromPullDownToRefresh: fromPullDownToRefresh)
            
            self.getUserStories(fromPullDownToRefresh: fromPullDownToRefresh)
            
            self.reverseGeocoding(location: loc)
            
            self.loadedAllData = true
            
            locationManager.requestLocationsForPostFlow()
//            locationManager.requestLocationWithAccuracy(accuracy: kCLLocationAccuracyBest) // Update best accurate location and cache it asynchronously
        }
        
    }
    
    fileprivate func updateStoryViewCounts() {
        // NOTE: Process all cached story view
        storyViewTrackViewModel.postStoryMetrics { (apiResponseState, networkError) in
        }
    }
    
    func getNearyByStories(latitude: Double, longitude: Double, distance: String, fromPullDownToRefresh: Bool) {
        if !fromPullDownToRefresh {
            self.loadingNearByStories = true
            
            UIView.performWithoutAnimation {
                self.tableView.beginUpdates()
                self.tableView.reloadSections(IndexSet(integer: 2), with: .none)
                self.tableView.reloadSections(IndexSet(integer: 3), with: .none)
                self.tableView.endUpdates()
            }
            
            self.errorLoadingNearByStories = false
        }
        
        placeStoryViewModel.getTimeline(latitude: latitude, longitude: longitude, distance: distance, enableCache: true, completion: { (places, apiResponseState, networkError, error) in
            
            if error != nil {
                self.errorLoadingNearByStories = true
                self.errorView.animateInViewWithMessage(error?.errorTitle ?? AppConstants.ErrorMessages.opsSomethingWentWrong, error?.errorDescription ?? "")
            } else {
                self.errorLoadingNearByStories = false
            }
            
            if !self.errorLoadingUserStories && !self.errorLoadingNearByStories {
                self.errorView.animateOutView()
            }
            
            self.loadingNearByStories = false
            
            UIView.performWithoutAnimation {
                self.tableView.beginUpdates()
                self.tableView.reloadSections(IndexSet(integer: 2), with: .none)
                self.tableView.reloadSections(IndexSet(integer: 3), with: .none)
                self.tableView.endUpdates()
            }
           
        })
    }
    
    func getBanners(fromPullDownToRefresh: Bool) {
        if !fromPullDownToRefresh {
            self.loadingBanners = true
            self.tableView.reloadSections(IndexSet(integer: 2), with: UITableViewRowAnimation.automatic)
        }
        
        self.errorLoadingBanners = false
        
        bannerViewModel.getBanners(completion: { (banners, apiResponseState, networkError) in
            switch apiResponseState {
                
            case .success: break
            case .apiError, .networkError, .jsonParseError, .noInternet:
                self.errorLoadingBanners = true
            }
            
            self.loadingBanners = false
            self.tableView.reloadSections(IndexSet(integer: 2), with: UITableViewRowAnimation.automatic)
            
        })
    }
    
    func getUserStories(fromPullDownToRefresh: Bool) {
        if !fromPullDownToRefresh {
            self.loadingUserStories = true
            
            UIView.performWithoutAnimation {
                self.tableView.beginUpdates()
                self.tableView.reloadSections(IndexSet(integer: 0), with: .none)
                self.tableView.reloadSections(IndexSet(integer: 3), with: .none)
                self.tableView.endUpdates()
            }
           
        }
        
        self.userStoriesViewModel.getUserProfileDetails(enableCache: true, completion: { (apiResponseState, networkError, error) in
            
            if error != nil {
                self.errorLoadingUserStories = true
                self.errorView.animateInViewWithMessage(error?.errorTitle ?? AppConstants.ErrorMessages.opsSomethingWentWrong, error?.errorDescription ?? "")
            } else {
                self.errorLoadingUserStories = false
            }
            
            if !self.errorLoadingUserStories && !self.errorLoadingNearByStories {
                self.errorView.animateOutView()
            }
            
            self.loadingUserStories = false
            
            UIView.performWithoutAnimation {
                self.tableView.beginUpdates()
                self.tableView.reloadSections(IndexSet(integer: 0), with: .none)
                self.tableView.reloadSections(IndexSet(integer: 3), with: .none)
                self.tableView.endUpdates()
            }
            
        })
    }
    
    func getNearbyAndUserStories(fromPullDownToRefresh: Bool) {
        self.getUserStories(fromPullDownToRefresh: fromPullDownToRefresh)
        
        if let userLocation = UserDefaults.standard.value(forKey: "UserLocation") {
            let latitude = (userLocation as AnyObject).value(forKey: "latitude") as? Double
            let longitude = (userLocation as AnyObject).value(forKey: "longitude") as? Double
            
            if let lat = latitude, let long = longitude {
                self.getNearyByStories(latitude: lat, longitude: long, distance: FIRRemoteConfigService.shared.getNearyByDistanceInMeteres(), fromPullDownToRefresh: fromPullDownToRefresh)
            }
        }
    }
    
    func reverseGeocoding(location: CLLocation) {
        let reverseGeocodingTrace = Performance.startTrace(name: AppConstants.Traces.reverseGeocoding)
        reverseGeocodingTrace?.start()
        
        locationManager.fetchPlacemarkFromLocation(location: location) { placemark, error in
            reverseGeocodingTrace?.stop()
            
            if error != nil {
                LogUtil.error(error)
            } else {
                // Save country to user defaults
                if let country = placemark?.country {
                    UserDefaults.standard.set(country, forKey: "UserLocationCountry")
                }
            }
        }
    }
    
    func locationPermissionViewSetup() {
        locationPermissionView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        self.view.addSubview(locationPermissionView)
        locationPermissionView.delegate = self
        
        self.hideLocationPermissionView()
    }
    
    func showLocationPermissionView() {
        self.locationPermissionView.isHidden = false
        self.hideTabBar()
    }
    
    func hideLocationPermissionView() {
        self.locationPermissionView.isHidden = true
        self.showTabBar()
    }
    
    func errorViewSetup() {
        // TODO: Instead of using '56' calculate two label's height dynamically
        errorView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: self.getSafeAreaInsets().top + 56)
        errorView.setStackViewTopConstraint(top: self.getSafeAreaInsets().top)
        self.view.addSubview(errorView)
        errorView.isHidden = true
        errorView.delegate = self
    }
    
    func addRatingsIntendAnalytics(addToStory: String, place: LivePlaces?) {
        AnalyticsHelper.addRatingIntend(screen: "\(AppConstants.screenNames.home)", section: "\(AppConstants.RatingsIntendSection.liveRatingsSection)", addToStory: addToStory, place: place)
    }
    
    // MARK: Fileprivate functions
    
    fileprivate func checkSubscribeBannerVisibility() {
        UserNoficationService.shared.getNotificationSettings { (settings) in
            var shouldShowBannerTemp = false
            switch settings {
            case .notDetermined, .denied:
                shouldShowBannerTemp = true
            default: break
            
            }
            if let user = Helper().getUser(), user.email == "" {
                shouldShowBannerTemp = true
            }
            
            self.showSubscribeBanner = shouldShowBannerTemp
            self.tableView.reloadSections(IndexSet(integer: 1), with: .none)
        }
    }
    
    fileprivate func updateNotification(notificationButton: UIButton) {
        let unreadCount = UserDefaults.standard.integer(forKey: "notificationUnreadCount")
        
        if unreadCount > 0 {
            notificationButton.setImage(UIImage(named: "iconNotificationFilled24White100")?.withRenderingMode(.alwaysOriginal), for: .normal)
            
        } else {
            notificationButton.setImage(UIImage(named: "iconNotificationOutline24White100")?.withRenderingMode(.alwaysOriginal), for: .normal)
        }
    }
    
    fileprivate func checkForAuthTokenNullity() {
        if UserDefaultsUtility.shared.getAuthToken() == "" {
            let alertService = AlertService()
            alertService.delgate = self
            
            alertService.showSessionExpieryWithOkButton(viewController: self)
        }
    }
    
    // MARK: Private functions
    private func navigateToPlaceFeedScreen(place: LivePlaces) {
        if let placeFeedViewController = AppConstants.storyboard.instantiateViewController(withIdentifier: "PlaceFeedViewController") as? PlaceFeedViewController {
            viewDidAppearFlag = false
            placeFeedViewController.place = place
            
            if let last = self.navigationController?.viewControllers.last, last != placeFeedViewController {
                // NOTE: So that same view controller is not pushed multiple times
                self.navigationController?.pushViewController(placeFeedViewController, animated: true)
            }
        }
    }
    
    private func navigateToStoriesViewController(place: LivePlaces?, storyViewModel: StoryViewModel?, section: String) {
        let storyViewManager = StoryViewManager.init(screen: AppConstants.screenNames.home)
        storyViewManager.section = section
        
        let storyViewController = StoryViewController.init(storyViewManager: storyViewManager)
        
        storyViewController.delegate = self
        
        if let storyViewModel = storyViewModel {
            storyViewController.storyViewModel = storyViewModel
        }
        
        if let place = place {
            storyViewController.selectedPlace = place
        }
        
        let navController = UINavigationController(rootViewController: storyViewController)
        navController.modalPresentationStyle = .overFullScreen
        navController.modalPresentationCapturesStatusBarAppearance = true
        
        
        self.present(navController, animated: true, completion: {
            self.hideTabBar()
            // Whenever story is opened refresh list to update view counts!
//            self.getNearbyAndUserStories(fromPullDownToRefresh: false)
        })
    }
    
    fileprivate func navigateToPublicProfileViewController(viewerDisplayName: String, source: String) {
        if let profileViewController = AppConstants.storyboard.instantiateViewController(withIdentifier: "ProfileViewController") as? ProfileViewController {
            viewDidAppearFlag = false
            
            if viewerDisplayName == Helper().getUserDisplayName() {
                profileViewController.isMyProfile = true
            } else {
                profileViewController.isMyProfile = false
            }
            
            profileViewController.displayName = viewerDisplayName
            profileViewController.source = source
            profileViewController.delegate = self
            profileViewController.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(profileViewController, animated: true)
        }
    }
    
    private func navigateToRatingCameraViewController(place: LivePlaces?) {
        self.locationManager.requestLocationsForPostFlow()
        
        let cameraViewController = RatingCameraViewController()
        
        if let selectedPlace = place {
            let cameraPlace = CameraPlace(id: selectedPlace._id, name: selectedPlace.name, address: selectedPlace.address)
            cameraViewController.initialPlace = cameraPlace
            if let mood = selectedPlace.placeMoodScore {
                cameraViewController.initialMood = "\(mood)"
            }
        }
        cameraViewController.galleryInterval = FIRRemoteConfigService.shared.getGalleryMediaAccessInterval()
        cameraViewController.ratingCameraViewControllerDelegate = self
        cameraViewController.screen = AppConstants.screenNames.home
        present(cameraViewController, animated: true, completion: nil)
    }
    
    private func getNearbyStoriesCellHeight() -> CGFloat {
        var rowsCount = 0
        var totalLineSpacing: CGFloat = 0
        let placesCount = self.placeStoryViewModel.getPlacesCount()
        
        if placesCount == 1 {
            rowsCount = 1
        } else if placesCount > 1 {
            let rows = Double(placesCount) / 2.0
            rowsCount = Int(rows.rounded())
            
            totalLineSpacing = CGFloat(rowsCount - 1)  * (self.nearByCellSpacing / 2)
        }
        
        return (self.nearByStoriesCardHeight * CGFloat(rowsCount)) + 16 + 32 + totalLineSpacing
    }
    
    fileprivate func navigateToNotificationsViewController() {
        if let notificationsViewController = AppConstants.storyboard.instantiateViewController(withIdentifier: "NotificationsViewController") as? NotificationsViewController {
            viewDidAppearFlag = false
            notificationsViewController.delegate = self
            self.navigationController?.navigationBar.isHidden = false
            self.navigationController?.pushViewController(notificationsViewController, animated: true)
        }
    }
    
    fileprivate func navigateToSearchViewController() {
        if let viewController = AppConstants.storyboard.instantiateViewController(withIdentifier: "SearchViewController") as? SearchViewController {
            viewDidAppearFlag = false
            viewController.modalPresentationStyle = .overFullScreen
            viewController.modalPresentationCapturesStatusBarAppearance = true
            viewController.delegate = self
            
            self.present(viewController, animated: false, completion: nil)
        }
    }
    
    fileprivate func handleBannerTap(banner: Banner) {
        
        if let cta = banner.cta {
            switch cta {
            case .user:
                if let user = banner.users.first, let displayName = user.displayName.first {
                    navigateToPublicProfileViewController(viewerDisplayName: displayName, source: AnalyticsHelper.PublicProfileSource.home)
                }
                
            case .story:
                var bannerSection = ""
                if let bannerData = bannerViewModel.bannerData, let type = bannerData.type {
                    bannerSection = type.rawValue
                }
                let storyViewModel = StoryViewModel.init(screen: AppConstants.screenNames.storyView, storyList: ArrayUtil.getStoryDataFromPlacesStoriesData(placesStoriesData: banner.places))
                navigateToStoriesViewController(place: banner.places.first?.placeDetails, storyViewModel: storyViewModel, section: bannerSection)
                
            }
        }
    }
    
}

extension TimelineViewController: LocationPermissionViewDelegate {
    
    func askLocationPermissionView() {
        self.locationManager.delegate = self
        self.locationManager.requestWhenInUseAuthorization()
        UserDefaults.standard.set(true, forKey: AppConstants.UserDefaultsKeys.nearByStoriesTapped)
    }
    
    func removeLocationPermissionView() {
        self.hideLocationPermissionView()
    }
    
}

extension TimelineViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = getAchievedBadgesTableViewCell(indexPath: indexPath)
            return cell
        } else if indexPath.section == 1 {
            if showSubscribeBanner {
                let cell = getSubscribeTableViewCell(indexPath: indexPath)
                return cell
            }
        } else if indexPath.section == 2 {
            if let bannerData = bannerViewModel.bannerData, let type = bannerData.type, bannerData.banners.count > 0, bannerData.banners.count >= FIRRemoteConfigService.shared.getTrendingMinBannersCount(), bannerData.banners.count <= FIRRemoteConfigService.shared.getTrendingMaxBannersCount()  {
                if type == .trending {
                    let cell = getTrendingTableViewCell(indexPath: indexPath)
                    return cell
                } else if type == .highlights {
                    let cell = getHighlightsTableViewCell(indexPath: indexPath)
                    return cell
                }
            } else {
                if errorLoadingBanners || loadingBanners {
                    let cell = getEmptyStateForBanner(indexPath: indexPath)
                    return cell
                }
            }
        } else if indexPath.section == 3 {
            let cell = getNearbyStoriesTableViewCell(indexPath: indexPath)
            return cell
        }
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "EmptyTableViewCell", for: indexPath) as? EmptyTableViewCell else {
            return UITableViewCell()
        }
        cell.backgroundColor = .red
        return cell
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.section == 0 {
        } else if indexPath.section == 1 {
            if showSubscribeBanner {
                return 32 + subscribeCardImageHeight
            } else {
                return 0
            }
        } else if indexPath.section == 2 {
            if let bannerData = bannerViewModel.bannerData, let type = bannerData.type, bannerData.banners.count > 0, bannerData.banners.count >= FIRRemoteConfigService.shared.getTrendingMinBannersCount(), bannerData.banners.count <= FIRRemoteConfigService.shared.getTrendingMaxBannersCount()  {
                if type == .trending {
                    return 144 + 26 + 8
                } else if type == .highlights {
                    return 330 + 32
                }
            } else {
                if errorLoadingBanners || loadingBanners {
                    return 144 + 32 + 8
//                    158 + 32
                } else {
                    return 0
                }
                
            }
        } else if indexPath.section == 3 {
            if !self.loadingNearByStories && !self.loadingUserStories && self.placeStoryViewModel.getPlacesCount() == 0, self.userStoriesViewModel.userRatingsData.count == 0 {
                return 0
            }
            
            if indexPath.row == 0 {
                if self.loadingNearByStories {
                    return (self.nearByStoriesCardHeight) + 16 + 32
                } else {
                    if self.placeStoryViewModel.getPlacesCount() > 0 {
                        return self.getNearbyStoriesCellHeight()
                    }
                }
            }
            
        }
        
        return UITableViewAutomaticDimension
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if section == 0 {
            return sectionHeaderHeight + 88
        } else if section == 2 {
            if let bannerData = bannerViewModel.bannerData, let type = bannerData.type, bannerData.banners.count > 0, bannerData.banners.count >= FIRRemoteConfigService.shared.getTrendingMinBannersCount(), bannerData.banners.count <= FIRRemoteConfigService.shared.getTrendingMaxBannersCount()  {
                if type == .trending {
                    return sectionHeaderHeight
                }
            } else {
                if errorLoadingBanners || loadingBanners {
                    return sectionHeaderHeight
                }
            }
        } else if section == 3 {
            if !self.loadingNearByStories && !self.loadingUserStories && self.placeStoryViewModel.getPlacesCount() == 0, self.userStoriesViewModel.userRatingsData.count == 0 {
                return 0
            }
            return sectionHeaderHeight
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 0 {
            return self.getHeaderViewForArchivements()
        } else if section == 2 {
            if let bannerData = bannerViewModel.bannerData, let type = bannerData.type, bannerData.banners.count > 0, bannerData.banners.count >= FIRRemoteConfigService.shared.getTrendingMinBannersCount(), bannerData.banners.count <= FIRRemoteConfigService.shared.getTrendingMaxBannersCount()  {
                if type == .trending {
                    return getHeaderViewForTrendingSection()
                }
            } else {
                if errorLoadingBanners || loadingBanners {
                    return getHeaderViewForTrendingSection()
                }
            }
        } else if section == 3 {
            if let headerView = getHeaderViewForNearbyStories() {
                return headerView
            }
        }
        return nil
    }
    
    func getAchievedBadgesTableViewCell(indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "AchievedBadgesTableViewCell", for: indexPath) as? AchievedBadgesTableViewCell else {
            return UITableViewCell()
        }
        cell.delegate = self
        if let highlight = self.userStoriesViewModel.userHighlight {
           cell.setUserAchievementData(highlight: highlight)
        }
        
        cell.setLoadingState(loading: loadingUserStories)
        return cell
    }
    
    func getHighlightsTableViewCell(indexPath: IndexPath) -> UITableViewCell {
        guard let cell = self.tableView.dequeueReusableCell(withIdentifier: "WeekEndHighlightsTableViewCell", for: indexPath) as? WeekEndHighlightsTableViewCell else { return UITableViewCell() }
        
        cell.delegate = self
        cell.setBannerViewModel(bannerViewModel: bannerViewModel)
        
        return cell
    }
    
    func getTrendingTableViewCell(indexPath: IndexPath) -> UITableViewCell {
        guard let cell = self.tableView.dequeueReusableCell(withIdentifier: "TrendingTableViewCell", for: indexPath) as? TrendingTableViewCell else { return UITableViewCell() }
        
        cell.delegate = self
        cell.setBannerViewModel(bannerViewModel: bannerViewModel)
        return cell
    }
    
    func getEmptyStateForBanner(indexPath: IndexPath) -> UITableViewCell {
        guard let cell = self.tableView.dequeueReusableCell(withIdentifier: "BannerEmptyState", for: indexPath) as? BannerEmptyState else { return UITableViewCell() }
        cell.setLoading(isLoading: self.loadingBanners)
        return cell
    }
    
    
    func getSubscribeTableViewCell(indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "SubscribeTableViewCell", for: indexPath) as? SubscribeTableViewCell else { return UITableViewCell() }
        cell.updateCardHeightConstraint(constant: subscribeCardImageHeight)
        cell.delegate = self
        cell.updateData()
        return cell
    }
    
    func getNearbyStoriesTableViewCell(indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            if self.loadingNearByStories {
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "NearByStoriesTableViewCell", for: indexPath) as? NearByStoriesTableViewCell else {
                    return UITableViewCell()
                }
                cell.spacing = self.nearByCellSpacing
                cell.cardHeight = self.nearByStoriesCardHeight
                cell.delegate = self
                cell.setStories(stories: self.placeStoryViewModel.places)
                cell.loadingState(state: self.loadingNearByStories)
                return cell
            } else {
                if self.placeStoryViewModel.getPlacesCount() > 0 {
                    guard let cell = tableView.dequeueReusableCell(withIdentifier: "NearByStoriesTableViewCell", for: indexPath) as? NearByStoriesTableViewCell else {
                        return UITableViewCell()
                    }
                    cell.delegate = self
                    cell.setStories(stories: self.placeStoryViewModel.places)
                    cell.loadingState(state: self.loadingNearByStories)
                    return cell
                } else {
                    guard let cell = tableView.dequeueReusableCell(withIdentifier: "EmptyStoryStateTableViewCell", for: indexPath) as? EmptyStoryStateTableViewCell else {
                        return UITableViewCell()
                    }
                    cell.imageVisibility(hide: true)
                    cell.headingLabelVisibility(hide: false)
                    cell.subHeadingLabelVisibility(hide: false)
                    cell.setHeadingLabelBySource(source: AppConstants.screenNames.home)
                    return cell
                }
            }
        }
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "EmptyTableViewCell", for: indexPath) as? EmptyTableViewCell else {
            return UITableViewCell()
        }
        return cell
    }
    
    func getHeaderViewForArchivements() -> UIView {
        let headerView = UIView()
//        headerView.backgroundColor = .red
        
        let searchButton = UIButton(type: .custom)
        searchButton.setImage(UIImage(named: "iconSearch24White100")?.withRenderingMode(.alwaysOriginal), for: .normal)
        searchButton.addTarget(self, action: #selector(handleSearchButtonTap), for: .touchUpInside)
        
        let notificationButton = UIButton(type: .custom)
        notificationButton.addTarget(self, action: #selector(handleNotificationTap), for: .touchUpInside)
        updateNotification(notificationButton: notificationButton)
        
        let achievementLabel = UILabel()
        achievementLabel.clipsToBounds = true
        achievementLabel.text = "Your Achievements"
        achievementLabel.font = UIFont(name: AppConstants.Font.primaryRegular, size: 16)
        achievementLabel.textColor = AppConstants.SminqColors.white100

        headerView.addSubview(searchButton)
        headerView.addSubview(notificationButton)
        headerView.addSubview(achievementLabel)
        
        searchButton.anchor(top: headerView.topAnchor, leading: headerView.leadingAnchor, bottom: nil, trailing: nil, padding: .init(top: 16, left: 4, bottom: 0, right: 0), size: .init(width: 48, height: 48))
        notificationButton.anchor(top: headerView.topAnchor, leading: nil, bottom: nil, trailing: headerView.trailingAnchor, padding: .init(top: 16, left: 0, bottom: 0, right: 0), size: .init(width: 48, height: 48))
        
        if loadingUserStories {
            achievementLabel.frame = CGRect.init(x: 16, y: 88, width: 72, height: 20)
            achievementLabel.showAnimatedGradientAnimation()
            achievementLabel.layer.cornerRadius = 10
        } else {
            achievementLabel.frame = CGRect.init(x: 16, y: 88, width: tableView.width, height: sectionHeaderHeight)
            achievementLabel.hideAnimatedGratientAnimation()
            achievementLabel.layer.cornerRadius = 0
        }
        
        return headerView
    }
    
  
    
    func getHeaderViewForNearbyStories() -> UIView? {
        
        let headerView = UIView.init(frame: CGRect(x: 0, y: 0, width: self.tableView.width, height: sectionHeaderHeight))
        let subView = CommonHeaderView.init(frame: CGRect(x: 0, y: 0, width: headerView.width, height: headerView.height), isBlur: false)
        subView.headerLabel.text = "Nearby Live Ratings"
        subView.notificationButtonVisibility(hide: true)
        if !self.loadingNearByStories {
            headerView.addSubview(subView)
            
            if !self.loadingNearByStories && !self.loadingUserStories && self.placeStoryViewModel.getPlacesCount() == 0, self.userStoriesViewModel.userRatingsData.count == 0 {
                return nil
            }
        }
        return headerView
        
    }
    
    func getHeaderViewForTrendingSection() -> UIView? {
        let headerView = UIView.init(frame: CGRect(x: 0, y: 0, width: self.tableView.width, height: sectionHeaderHeight))
        let subView = CommonHeaderView.init(frame: CGRect(x: 0, y: 0, width: headerView.width, height: headerView.height), isBlur: false)
        subView.headerLabel.text = "Trending Now"
        subView.notificationButtonVisibility(hide: true)
        headerView.addSubview(subView)
        return headerView
    }
    
}

extension TimelineViewController: SFSafariViewControllerDelegate {
    
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
    
}

extension TimelineViewController: LocationServiceProtocol {
    
    func didChangeAuthorization(authStatus: CLAuthorizationStatus) {
        if authStatus == CLAuthorizationStatus.authorizedAlways || authStatus == CLAuthorizationStatus.authorizedWhenInUse {
            // NOTE: store location to user defaults
            let location = locationManager.getCurrentLocation()
            if location != nil {
                locationManager.setUserLocationToDefaults(latitude: (location?.latitude)!, longitude: (location?.longitude)!)
                
                self.hideLocationPermissionView()
                
                if !self.loadedAllData {
                    self.loadAllData(fromPullDownToRefresh: false)
                }
            }
            
            NotificationCenter.default.post(name: Notification.Name(rawValue: "locationAuthorized"), object: nil, userInfo: nil)
            AnalyticsHelper.appPermissionAnalyticEvent(eventName: AnalyticsHelper.PermissionsEventName.locationPermission, permission: AppConstants.granted)
        } else if authStatus == CLAuthorizationStatus.restricted || authStatus == CLAuthorizationStatus.denied {
            self.showLocationPermissionView()
            self.locationPermissionView.updateUIForPermissionDenied()
            
            self.loadedAllData = false
            AnalyticsHelper.appPermissionAnalyticEvent(eventName: AnalyticsHelper.PermissionsEventName.locationPermission, permission: AppConstants.denied)
        }
        
    }
    
}

extension TimelineViewController: NearByStoriesTableViewCellDelegate {
    
    func nearByPlaceDidSelect(placeStory: PlacesStoriesData) {
        if let place = placeStory.placeDetails {
            let storyViewModel = StoryViewModel.init(screen: AppConstants.screenNames.storyView, storyList: placeStoryViewModel.convertToStoryDataList(filterByPostId: nil))
            self.navigateToStoriesViewController(place: place, storyViewModel: storyViewModel, section: AppConstants.ViewRatingsSection.nearbyRatings)
        }
    }
    
}

extension TimelineViewController: ErrorViewDelegate {
    
    func errorViewTapped() {
       self.errorView.animateOutView()
        
//        if self.errorLoadingUserStories {
//            self.getUserStories(fromPullDownToRefresh: true)
//        }
//
//        if self.errorLoadingNearByStories {
//            if let userLocation = UserDefaults.standard.value(forKey: "UserLocation") {
//                let latitude = (userLocation as AnyObject).value(forKey: "latitude") as? Double
//                let longitude = (userLocation as AnyObject).value(forKey: "longitude") as? Double
//
//                if let lat = latitude, let long = longitude {
//                    self.getNearyByStories(latitude: lat, longitude: long, distance: FIRRemoteConfigService.shared.getNearyByDistanceInMeteres(), fromPullDownToRefresh: true)
//                }
//            }
//        }
    }
    
}

extension TimelineViewController: UserLiveStoriesTableViewCellDelegate {
    
    func didUserStorySelect(selectedStory: StoryData) {
        guard let place = selectedStory.placeDetails else { return }

        let storyViewModel = StoryViewModel.init(screen: AppConstants.screenNames.storyView, storyList: self.userStoriesViewModel.userRatingsData)
        
        self.navigateToStoriesViewController(place: place, storyViewModel: storyViewModel, section: AppConstants.ViewRatingsSection.liveRatings)
    }
    
    func didRateTap(place: LivePlaces) {
        self.addRatingsIntendAnalytics(addToStory: "yes", place: place)
        self.navigateToRatingCameraViewController(place: place)
    }
    
    func didEmptyStoryCellSelect() {
        self.addRatingsIntendAnalytics(addToStory: "no", place: nil)
        self.navigateToRatingCameraViewController(place: nil)
    }
    
}

extension TimelineViewController: CommonHeaderViewDelegate {
    
    func didTapOnNotificationIcon() {
        navigateToNotificationsViewController()
    }
    
}

extension TimelineViewController: NotificationsViewControllerDelegate {
    func didBackButtonTap() {
        self.tableView.reloadSections([0], with: .none)
    }
}

extension TimelineViewController: ExpiredStoryTableViewCellDelegate {
    
    func addALiveRatingButtonTapped() {
        self.addRatingsIntendAnalytics(addToStory: "no", place: nil)
        self.navigateToRatingCameraViewController(place: nil)
    }
    
    func expiredStoryStackImageViewDidTap() {
        self.addRatingsIntendAnalytics(addToStory: "no", place: nil)
        self.navigateToRatingCameraViewController(place: nil)
    }
    
}

extension TimelineViewController: RatingCameraViewControllerDelegate {
    
    func presentPostSuccessViewController() {
        self.presentPostSuccessVC()
    }
    
}

extension TimelineViewController: StoryViewControllerDelegate {
    func didDismissStoryView() {
        self.showTabBar()
        viewDidAppearFlag = true
//        getUserStories(fromPullDownToRefresh: true)
    }
    
    func didUpvotePost(post: UserDataPlaceFeed) {
        print("didUpvotePost")
        print(post._id)
        
        bannerViewModel.updateBannerFor(post: post)
        placeStoryViewModel.updatePost(post: post)
        // TODO: Update trending/highbanner and nearby view models
    }
}

extension TimelineViewController: AlertServiceDelegate {
    func didOkTap() {
        print("Ok button tapped")
        UserSessionUtility.shared.clearUserSession()
        AppConstants.appDelegate.window?.rootViewController = UINavigationController(rootViewController: AppConstants.storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController)
    }
}

extension TimelineViewController: AchievedBadgesTableViewCellDelegate {
    
    func achievementsMoreViewTapped() {
        sendHomeBadgeTapAnalyticEvent()
        if let navController = AppConstants.appDelegate.tabBarViewController?.childViewControllers[1]  {
            if let profileViewController = navController.childViewControllers[0] as? ProfileViewController {
                profileViewController.currentTabIndex = 1
                AppConstants.appDelegate.tabBarViewController?.selectedIndex = 1
            }
        }
        
    }
    
    func sendHomeBadgeTapAnalyticEvent() {
        if let highlight = self.userStoriesViewModel.userHighlight {
            let level: [AchievementLevel] = highlight.achievementlevels
            if level.count > 0 {
                if let badge = level[0].badge?.name {
                    AnalyticsHelper.homeBadgeTappedEvent(badgeName: "\(badge)")
                }
            }
        }
    }
    
}

extension TimelineViewController: SearchViewControllerDelegate {
    func didCancelButtonTap() {
        showTabBar()
        viewDidAppearFlag = true
    }
}

extension TimelineViewController: SubscribeTableViewCellDelegate {
    func didBannerImageViewTap() {
        AnalyticsHelper.homeSubscribeEvent()
        guard let window = UIApplication.shared.keyWindow else { return }
        window.addSubview(self.subscribeModalView)
        
        subscribeModalView.updateButtonStates()
        subscribeModalView.animateInView()
    }
}

extension TimelineViewController: SubscribeModalViewDelegate {
    func didOverlayTap() {
    }
    
    func didViewRemove() {
        checkSubscribeBannerVisibility()
    }
    
    func didAnimateOutInitiate() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            self.showTabBar()
        }
    }
    
    func didUserUpdateFailWithError(error: ErrorObject?) {
        guard let error = error else { return }
        self.errorView.animateInViewWithError(errorObject: error, autoDismiss: true)
    }
    
    func didEnterInvalidEmail(email: String) {
        let cancelButtonAction = UIAlertAction(title: AppConstants.cancel, style: .cancel)
        self.presentAlertSheet(title: "", message: "Please enter a valid email", options: [cancelButtonAction], controllerStyle: .alert, onWindow: true) { (action) in
            
        }
    }
}

extension TimelineViewController: WeekEndHighlightsTableViewCellDelegate {
    
    func didSelectItemAt(indexPath: IndexPath, banner: Banner) {
        handleHighlightsAnalyticEvent(banner: banner)
        handleBannerTap(banner: banner)
    }
    
    func handleHighlightsAnalyticEvent(banner: Banner) {
        if let type = banner.type {
            switch type {
            case .maxLikes:
                AnalyticsHelper.highlightTrendingAnalyticEvents(event: AnalyticsHelper.HighlightsTrendingAnalyticsEventName.highlightsMostLiked)
            case .maxViews:
                AnalyticsHelper.highlightTrendingAnalyticEvents(event: AnalyticsHelper.HighlightsTrendingAnalyticsEventName.highlightsMostViewed)
            case .maxRatings:
                AnalyticsHelper.highlightTrendingAnalyticEvents(event: AnalyticsHelper.HighlightsTrendingAnalyticsEventName.highlightsMostRated)
            case .maxUserRatings, .maxUserPlaces, .newBadge, .claimedReward:
                break
            case .totalDope:
                AnalyticsHelper.highlightTrendingAnalyticEvents(event: AnalyticsHelper.HighlightsTrendingAnalyticsEventName.highlightsDopePlaces)
            case .totalStickers:
                self.getStickerDataForAnalyticsFrom(eventName: BannerSectionType.highlights.rawValue, banner: banner)
            case .showcase:
                if let placeStoriesData = banner.places.first, let place = placeStoriesData.placeDetails {
                    AnalyticsHelper.highlightShowcasePlaceTapAnalyticEvent(eventName: BannerSectionType.highlights.rawValue, place: place)
                }
            }
        }
    }
    
}

extension TimelineViewController: ProfileViewControllerDelegate {
    func removeFullScreenOverlayFromStories() {
        showTabBar()
        viewDidAppearFlag = true
    }
}

extension TimelineViewController: TrendingTableViewCellDelegate {
    
    func didSelectTrendingItemAt(indexPath: IndexPath, banner: Banner) {
        handleTrendingAnalyticEvents(banner: banner)
        handleBannerTap(banner: banner)
    }
    
    func handleTrendingAnalyticEvents(banner: Banner) {
        if let type = banner.type {
            switch type {
            case .totalStickers:
                self.getStickerDataForAnalyticsFrom(eventName: BannerSectionType.trending.rawValue, banner: banner)
            case .totalDope:
                AnalyticsHelper.highlightTrendingAnalyticEvents(event: AnalyticsHelper.HighlightsTrendingAnalyticsEventName.trendingDopePlaces)
            case .maxLikes:
                AnalyticsHelper.highlightTrendingAnalyticEvents(event: AnalyticsHelper.HighlightsTrendingAnalyticsEventName.trendingMostLiked)
            case .maxViews:
                AnalyticsHelper.highlightTrendingAnalyticEvents(event: AnalyticsHelper.HighlightsTrendingAnalyticsEventName.trendingMostViewed)
            case .maxRatings:
                AnalyticsHelper.highlightTrendingAnalyticEvents(event: AnalyticsHelper.HighlightsTrendingAnalyticsEventName.trendingMostRated)
            case .maxUserRatings:
                AnalyticsHelper.highlightTrendingAnalyticEvents(event: AnalyticsHelper.HighlightsTrendingAnalyticsEventName.trendingUserMaxRatings)
            case .maxUserPlaces:
                AnalyticsHelper.highlightTrendingAnalyticEvents(event: AnalyticsHelper.HighlightsTrendingAnalyticsEventName.trendingUserMaxPlaces)
            case .newBadge:
                AnalyticsHelper.highlightTrendingAnalyticEvents(event: AnalyticsHelper.HighlightsTrendingAnalyticsEventName.trendingUserUnlockedBadge)
            case .claimedReward:
                AnalyticsHelper.highlightTrendingAnalyticEvents(event: AnalyticsHelper.HighlightsTrendingAnalyticsEventName.trendingUserClaimReward)
            case .showcase:
                break
            }
        }
    }
    
    func getStickerDataForAnalyticsFrom(eventName: String, banner: Banner) {
        guard let placeStoriesData = banner.places.first, let userStory = placeStoriesData.stories?.first, let feed = userStory.posts.first else { return }
        guard let contributions = feed.getOtherContext() else { return }
        guard let stickerData = contributions.mediaMetadata?.stickerList?.first else { return }
        AnalyticsHelper.highlightTrendingStickersTapAnalyticEvent(eventName: eventName, stickerName: stickerData.sticker!)
    }
    
}
