//
//  ForceUpgradeModalViewController.swift
//  Sminq
//
//  Created by Suraj on 3/26/18.
//  Copyright © 2018 Suraj Sagare. All rights reserved.
//

import UIKit
import FirebaseRemoteConfig

class ForceUpgradeModalViewController: UIViewController {

    @IBOutlet weak var headingLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupUI()
    }

    // MARK: Private functions
    
    func setupUI() {
        self.headingLabel.text = "A version of \(UIApplication.shared.getTargetName()) is available"
    }
    
    @IBAction func updateTap(_ sender: UIButton) {
        if let appUrl = RemoteConfig.remoteConfig().configValue(forKey: "force_update_ios_appstoreurl").stringValue{
        if let url = URL(string: appUrl) {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                // Fallback on earlier versions
                UIApplication.shared.openURL(url)
            }
        }
    }
    }
    
}
