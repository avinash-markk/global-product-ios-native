//
//  TabbarViewController.swift
//  Sminq
//
//  Created by Bjorn Mascarenhas on 22/02/18.
//  Copyright © 2018 Suraj Sagare. All rights reserved.
//
import UIKit
import Gloss
import SVProgressHUD
import Firebase
import CleverTapSDK
import Cloudinary
import Alamofire
import UserNotifications
import MarkkCamera

protocol TabBarUploadDelegate: NSObjectProtocol {
    func updateUploadList()
    func updateUploadProgress(progressTask: UploadTask)
    func updateUploadStateFor(task: UploadTask)
}

class TabbarViewController: UITabBarController {
    
    let cameraButton = UIButton.init(type: .custom)
    let LocationMgr = UserLocationManager.SharedManager
    let networkManager = NetworkManager.sharedInstance
    var asyncSubview = UIView()
    var uploadStatusView = UploadStatusView()
    let mainScreenBounds = UIScreen.main.bounds
    var previousController = UIViewController()
    weak var uploadDelegate: TabBarUploadDelegate?
    var isUploadsViewVisible: Bool = false
    let separatorView = UIView()
    
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        if item.tag == 0 {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: AppConstants.AppNotificationNames.homeTab), object: nil)
        } else {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: AppConstants.AppNotificationNames.profileTab), object: nil)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
        self.tabBarSetup()
        self.centerAlignCameraButton()
        self.uploadBarViewSetup()
        self.addNotificationObservers()
        self.clearCachedStories()
        networkManager.startNetworkReachabilityObserver()
    }
    
    override func viewSafeAreaInsetsDidChange() {
        self.updateUploadBarAsPerDevice()
        self.centerAlignCameraButton()
        self.updateSafeAreaViews()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        cameraButton.addTarget(self, action: #selector(openRatingsCamera), for: .touchUpInside)
        cameraButton.layer.cornerRadius = cameraButton.frame.size.height/2
        self.view.insertSubview(cameraButton, aboveSubview: self.tabBar)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if let userInfo = UserDefaults.standard.value(forKey: AppConstants.UserDefaultsKeys.notificationUserInfo) as? [AnyHashable: Any] {
            NotificationCenter.default.post(name: Notification.Name(AppConstants.AppNotificationNames.notificationTappedFromTray), object: nil, userInfo: userInfo)
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.view.bringSubview(toFront: cameraButton)
    }
    
    fileprivate func updateSafeAreaViews() {
        separatorView.frame = CGRect.init(x: tabBar.x, y: tabBar.y - 1 - self.getSafeAreaInsets().bottom, width: view.width, height: 1)
    }
    
    func tabBarSetup() {
        self.tabBar.barTintColor = AppConstants.SminqColors.blackLight100
        self.tabBar.tintColor = UIColor.white
        self.tabBar.itemPositioning = .fill
        self.tabBar.itemSpacing = -40.0
        self.tabBar.itemWidth = -100.0
        cameraButton.frame = CGRect(x: 0.0, y: 0.0, width: 40, height: 40)
        cameraButton.setBackgroundImage(UIImage(named: "iconAddPostNav40"), for: .normal)
        
        setupSeparator()
    }
    
    func setupSeparator() {
        separatorView.backgroundColor = AppConstants.SminqColors.white100.withAlphaComponent(0.1)
        separatorView.isHidden = false
        view.addSubview(separatorView)
        
        updateSafeAreaViews()
    }
    
    func clearCachedStories() {
        // NOTE: Invalidate cache - 1. Delete posts older than 2 days 2. Delete posts if cache size is exceeded
        MediaCacheManagerService.shared.invalidateFeedCachedMediaByHour(beforeHour: FIRRemoteConfigService.shared.getFeedCacheInvalidateInHours())
        MediaCacheManagerService.shared.invalidateFeedCachedMediaByBufferSize(bufferSizeString: FIRRemoteConfigService.shared.getFeedCacheMaxBufferSizeInMbs())
    }
    
    func centerAlignCameraButton() {
        if self.getSafeAreaInsets().bottom > 0 {
            cameraButton.center = self.tabBar.center
            cameraButton.origin.y = self.tabBar.origin.y - 26
        } else {
            cameraButton.center = self.tabBar.center
        }
    }
    
    func uploadBarViewSetup() {
        self.uploadStatusView.tag = 100
        self.uploadStatusView.delegate = self
        self.updateUploadBarAsPerDevice()
        asyncSubview.addSubview(self.uploadStatusView)
        self.view.insertSubview(asyncSubview, aboveSubview: self.tabBar)
        self.asyncSubview.isHidden = true
    }
    
    func addNotificationObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(addUploadProcess), name: NSNotification.Name("Add_Upload_Process"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(updateMessage), name: NSNotification.Name("Update_Message"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(updateStatusProgress), name: NSNotification.Name("Update_Upload_Progress"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(updateStateRecieved), name: NSNotification.Name(AppConstants.AppNotificationNames.uploadDone), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(checkandUploadFailedStories), name: NSNotification.Name("Check_For_Failed_Uploads"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(openRatingsCamera), name: NSNotification.Name("Add_Ratings"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.showBottomBar(_:)), name: NSNotification.Name(rawValue: AppConstants.AppNotificationNames.showBottomBar), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.hideBottomBar(_:)), name: NSNotification.Name(rawValue: AppConstants.AppNotificationNames.hideBottomBar), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.catchIt(_:)), name: NSNotification.Name(AppConstants.AppNotificationNames.notificationTappedFromTray), object: nil)
    }
    
    func updateUploadBarAsPerDevice() {
        let yValue = self.hasNotch() == true ? self.mainScreenBounds.size.height - 130 : self.mainScreenBounds.size.height - 99
        asyncSubview.frame = CGRect(x: 0, y: yValue, width: self.mainScreenBounds.size.width, height: 50)
        self.uploadStatusView.frame = CGRect(x: 0, y: 0, width: asyncSubview.frame.size.width, height: 50)
    }
    
    @objc func showBottomBar(_ notification: NSNotification) {
        self.tabBarController?.tabBar.isHidden = false
        self.cameraButton.isHidden = false
        self.separatorView.isHidden = false
        self.hidesBottomBarWhenPushed = false
    }
    
    @objc func hideBottomBar(_ notification: NSNotification) {
        self.tabBarController?.tabBar.isHidden = true
        self.cameraButton.isHidden = true
        self.separatorView.isHidden = true
        self.hidesBottomBarWhenPushed = true
    }
    
    @objc func checkandUploadFailedStories() {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name("Check_For_Failed_Uploads"), object: nil)
        UserDefaults.standard.set(false, forKey: "UploadInProgress")
        
        let retryObjects = UploadDatabaseService.shared.getFailedFeedsToRetry()
        if retryObjects.count > 0 {
            UploadManager.sharedInstance.deleteAllRecords()
            for index in 0..<retryObjects.count {
                let retryDict = retryObjects[index] as! NSDictionary
                let feedID = retryDict.value(forKey: "feedID") as! String
                let feedData = retryDict.value(forKey: "feedData") as! String
                let feedPlaceId = retryDict.value(forKey: "feedPlaceId") as! String
                let feedSubPlaceId = retryDict.value(forKey: "feedSubPlaceId") as! String
                let feedIsVideo = retryDict.value(forKey: "feedIsVideo") as! String
                let feedPlaceName = retryDict.value(forKey: "feedPlaceName") as! String
                let textListString = retryDict.value(forKey: "textList") as! String
                let stickerListString = retryDict.value(forKey: "stickerList") as! String
                
                if DocDirectoryUtil().checkifFileExistInDocDirectory(feedId: feedID, isVideo: feedIsVideo == "true") {
                    let retryUploadTask = UploadTask(taskId: feedID, questionsData: feedData, placeIDString: feedPlaceId, subPlaceIDString: feedSubPlaceId, isVideo: feedIsVideo == "true", taskPublicId: "", videoDuration: 0.0, placeName: feedPlaceName, textListString: textListString, stickerListString: stickerListString, status: "Pending", progress: 0.0, post: "")
                    UploadManager.sharedInstance.addTaskToArrayOfUploads(task: retryUploadTask)
                }
            }
        }
    }
    
    @objc func addUploadProcess(notification: Notification) {
        guard let task = notification.userInfo?["task"] as? UploadTask else {
            return
        }
        let (progress, progressIndex) = UploadManager.sharedInstance.checkIfAnyUploadIsInProgress()
        if progress {
            DispatchQueue.main.async {
                UIView.animate(withDuration: 0.2, animations: {
                    
                    // If tabbar is not hidden then show a progress
                    if let tabBar = AppConstants.appDelegate.tabBarViewController, !tabBar.tabBar.isHidden {
                        self.asyncSubview.isHidden = false
                    }
                    
                    self.setUploadBarThumbail(taskId: task.taskId!, isVideo: task.isVideo)
                    self.uploadStatusView.uploadStatus.text = "Uploading \(progressIndex + 1) of \(UploadManager.sharedInstance.operationsArray.count)..."
                    self.uploadStatusView.progressView.progress = 0.0
                }, completion: nil)
            }
        }
    }
    
    func setUploadBarThumbail(taskId: String, isVideo: Bool) {
        if isVideo {
            if let thumbnailPath = Helper().returnSavedThumbnailForVideoAt(taskId: taskId) {
                self.uploadStatusView.uploadStoryImage.image = UIImage(contentsOfFile: thumbnailPath)!
            }
        } else {
            let fileUrlPath = DocDirectoryUtil().getLocalUrlForTask(id: taskId, isVideo: false)
            self.uploadStatusView.uploadStoryImage.image = UIImage(contentsOfFile: fileUrlPath)!
        }
    }
    
    @objc func updateMessage() {
        
        if UploadManager.sharedInstance.operationsArray.count > 0 {
            let (progress, progressIndex) = UploadManager.sharedInstance.checkIfAnyUploadIsInProgress()
            if progress {
                DispatchQueue.main.async {
                    // If tabbar is not hidden then show a progress
                    if let tabBar = AppConstants.appDelegate.tabBarViewController, !tabBar.tabBar.isHidden {
                        self.asyncSubview.isHidden = false
                    }
                    
                    self.uploadStatusView.uploadStatus.text = "Uploading \(progressIndex + 1) of \(UploadManager.sharedInstance.operationsArray.count)..."
                    self.uploadDelegate?.updateUploadList()
                }
            } else {
                if UploadManager.sharedInstance.checkIfAnyUploadFailed() {
                    DispatchQueue.main.async {
                        self.uploadStatusView.uploadStatus.text = "Upload failed..."
                    }
                } else {
                    if isUploadsViewVisible == false {
                        UploadDatabaseService.shared.deleteUploadedFeeds()
                        self.removeUploadedFeedsFromList()
                        self.dismissUploadBar()
                    }
                }
            }
        } else {
            self.dismissUploadBar()
        }
        
    }
    
    func dismissUploadBar() {
        DispatchQueue.main.async {
            self.uploadStatusView.uploadStatus.text = "Upload completed!"
        }
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.5, animations: {
                self.asyncSubview.isHidden = true
                UserDefaults.standard.set(false, forKey: "UploadInProgress")
            }, completion: nil)
        }
    }
    
    @objc func updateStatusProgress(notification: Notification) {
        guard let task = notification.userInfo?["progress"] as? UploadTask else { return }
        let progressFLoatValue: Float = task.progress
        let previousProgress = self.uploadStatusView.progressView.progress
        let previousInt  = Int(previousProgress * 100)
        let currentInt: Int = Int(progressFLoatValue * 100)
        if currentInt < previousInt {
            return
        }
        
        DispatchQueue.main.async {
            self.uploadStatusView.progressView.setProgress(progressFLoatValue, animated: true)
            self.uploadDelegate?.updateUploadProgress(progressTask: task)
        }
    }
    
    func updateUploadsArray(task: UploadTask) {
        UploadDatabaseService.shared.updateStatusForFeed(feedID: task.taskId!, newStatus: task.status)
        let currentTaskId = task.taskId!
        for (index, item) in UploadManager.sharedInstance.operationsArray.enumerated() {
            if let task = item as? UploadTask {
                if task.taskId == currentTaskId {
                    UploadManager.sharedInstance.operationsArray.replaceObject(at: index, with: task)
                }
            }
        }
    }
    
    @objc func updateStateRecieved(notification: Notification) {
        
        self.uploadStatusView.progressView.setProgress(0.0, animated: false)
        guard let uploadTask = notification.userInfo?["task"] as? UploadTask else { return }
        self.updateUploadsArray(task: uploadTask)
        
        switch uploadTask.status {
        case "Completed":
            self.uploadDelegate?.updateUploadStateFor(task: uploadTask)
            self.showLocalNotificationOnStoryUpload(task: uploadTask, success: true)
            if UploadManager.sharedInstance.checkForItemsInUploadsList() {
                UploadManager.sharedInstance.checkAndUploadNext()
            } else {
                if isUploadsViewVisible == false {
                    UploadManager.sharedInstance.operationsArray.removeAllObjects()
                }
                DispatchQueue.main.async {
                    self.uploadStatusView.uploadStatus.text = "Upload completed!"
                }
                DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                    UIView.animate(withDuration: 0.5, animations: {
                        self.asyncSubview.isHidden = true
                        UserDefaults.standard.set(false, forKey: "UploadInProgress")
                    }, completion: { (value: Bool) in
                        self.showLocalNotificationOnStoryUpload(task: uploadTask, success: true)
                    })
                }
            }
            
        case "Failed":
            DispatchQueue.main.async {
                self.uploadStatusView.uploadStatus.text = "Upload failed..."
            }
            if UploadManager.sharedInstance.checkForItemsInUploadsList() {
                UploadManager.sharedInstance.checkAndUploadNext()
            }
            self.uploadDelegate?.updateUploadStateFor(task: uploadTask)
            self.showLocalNotificationOnStoryUpload(task: uploadTask, success: false)
            
        case "Cancelled":
            UploadDatabaseService.shared.deleteFeedWithFeed(feedID: uploadTask.taskId!)
            if UploadManager.sharedInstance.checkForItemsInUploadsList() {
                UploadManager.sharedInstance.checkAndUploadNext()
            } else {
                DispatchQueue.main.async {
                    UIView.animate(withDuration: 0.2, animations: {
                        self.asyncSubview.isHidden = true
                    })
                }
            }
            
        default:
            break
        }
    }
    
    @objc func openRatingsCamera() {
        checkForAuthTokenNullity()
        let cameraViewController = RatingCameraViewController()
        cameraViewController.initialPlace = nil
        cameraViewController.initialMood = nil
        cameraViewController.galleryInterval = FIRRemoteConfigService.shared.getGalleryMediaAccessInterval()
        cameraViewController.ratingCameraViewControllerDelegate = self
        cameraViewController.screen = "\(Helper().getVisibleScreen())"
        present(cameraViewController, animated: true, completion: nil)
        
        self.addRatingsIntendAnalytics()
        LogUtil.info("Tapped on camera icon")
    }
    
    func addRatingsIntendAnalytics() {
        AnalyticsHelper.addRatingIntend(screen: "\(Helper().getVisibleScreen())", section: "\(AppConstants.RatingsIntendSection.cameraSection)", addToStory: "no", place: nil)
    }
    
    func showLocalNotificationOnStoryUpload(task: UploadTask, success: Bool) {
        let center = UNUserNotificationCenter.current()
        let options: UNAuthorizationOptions = [.alert, .sound]
        
        center.requestAuthorization(options: options) { (granted, error) in
            if !granted {
                LogUtil.error(error)
            } }
        
        let content = UNMutableNotificationContent()
        content.title = task.placeName!
        content.body = "\(task.isVideo ? "Video" : "Image") \(success ? "uploaded successfully" : "uploading failed")"
        content.sound = UNNotificationSound.default()
        
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1, repeats: false)
        let identifier = "UYLLocalNotification"
        let request = UNNotificationRequest(identifier: identifier, content: content, trigger: trigger)
        center.add(request, withCompletionHandler: { (error) in
        })
    }
    
    // MARK: Fileprivate functions
    
    fileprivate func checkForAuthTokenNullity() {
        if UserDefaultsUtility.shared.getAuthToken() == "" {
            let alertService = AlertService()
            alertService.delgate = self
            
            alertService.showSessionExpieryWithOkButton(viewController: self)
        }
    }
    
}

extension TabbarViewController: UploadStatusViewDelegate {
    
    func viewUploadsButtonTapped(_ sender: UploadStatusView) {
        if let uploadsViewController = AppConstants.storyboard.instantiateViewController(withIdentifier: "UploadsViewController") as? UploadsViewController {
            uploadsViewController.tabBarViewController = self
            uploadsViewController.delegate = self
            self.isUploadsViewVisible = true
            let navController = UINavigationController(rootViewController: uploadsViewController)
            self.present(navController, animated: true, completion: nil)
        }
    }
    
}

extension UINavigationController {
    
    open override var preferredStatusBarStyle: UIStatusBarStyle {
        return topViewController?.preferredStatusBarStyle ?? .lightContent
    }
    
}

extension TabbarViewController: UITabBarControllerDelegate {
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        if let vc = tabBarController.selectedViewController?.childViewControllers.first {
            if previousController == vc {
                if let _ = vc as? TimelineViewController {
                    NotificationCenter.default.post(name: NSNotification.Name(AppConstants.AppNotificationNames.timelineTabSelectedTwice), object: nil)
                } else if let _ = vc as? ProfileViewController {
                    NotificationCenter.default.post(name: NSNotification.Name(AppConstants.AppNotificationNames.profileTabSelectedTwice), object: nil)
                }
            }
            self.previousController = vc
        }
    }
    
}

extension TabbarViewController: UploadsViewControllerDelegate {
    
    func uploadsViewDismissed() {
        UploadDatabaseService.shared.deleteUploadedFeeds()
        self.removeUploadedFeedsFromList()
        self.updateMessage()
    }
    
    func removeUploadedFeedsFromList() {
        if UploadManager.sharedInstance.operationsArray.count > 0 {
            let operationsArrayCopy = NSMutableArray()
            for item in UploadManager.sharedInstance.operationsArray {
                if let task = item as? UploadTask, task.status != "Completed" {
                    operationsArrayCopy.add(task)
                }
            }
            UploadManager.sharedInstance.operationsArray = operationsArrayCopy
        }
    }
    
}

extension TabbarViewController: RatingCameraViewControllerDelegate {
    
    func presentPostSuccessViewController() {
        self.presentPostSuccessVC()
    }
    
}

extension TabbarViewController: AlertServiceDelegate {
    func didOkTap() {
        UserSessionUtility.shared.clearUserSession()
        AppConstants.appDelegate.window?.rootViewController = UINavigationController(rootViewController: AppConstants.storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController)
    }
}
