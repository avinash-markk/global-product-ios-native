//
//  StoryViewController+DataSource.swift
//  Sminq
//
//  Created by Pushkar Deshmukh on 18/05/19.
//  Copyright © 2019 Sminq. All rights reserved.
//
import UIKit
import Foundation

extension StoryViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return storyViewModel.getStoryCount()
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//        print("cellForItemAt", indexPath.row)
        
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "StoryCollectionViewCell", for: indexPath) as? StoryCollectionViewCell else { return UICollectionViewCell() }
        
        cell.cellIndex = indexPath.row
        cell.setStory(story: storyViewModel.storyList[indexPath.row], storyViewManager: storyViewManager, isCellEndedDisplaying: false)
        cell.delegate = self
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
//        print("StoryViewController didEndDisplaying: ", indexPath.row)
        
        // NOTE: Stop image/video download tasks. Pause players, pause progress
        guard let endedCell = cell as? StoryCollectionViewCell else { return }
        endedCell.pause()
        
        DispatchQueue.main.async {
            self.pause()
            
            let visibleCells = collectionView.visibleCells.sortedArrayByPosition()
            let visibleCell = visibleCells.first as? StoryCollectionViewCell
            guard let vCell = visibleCell else { return }
            guard let vCellIndexPath = collectionView.indexPath(for: vCell) else {
                return
            }
            
            // NOTE: Resume visible cell's progress            
            self.storyViewManager.isCompletelyVisible = true
            vCell.createProgressViews()
            vCell.setMediaPlaybackLock(lock: false)
            self.storyIndex = vCellIndexPath.row
            vCell.scrollToRating(index: 0, animate: false)
            
//            print("Setting story!")
            
            vCell.setStory(story: self.storyViewModel.storyList[self.storyIndex], storyViewManager: self.storyViewManager, isCellEndedDisplaying: true)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        // Resume image/video downloading services
//        print("StoryViewController willDisplay: ", indexPath.row)
        
        guard let cell = cell as? StoryCollectionViewCell else { return }
        cell.createProgressViews()
        cell.setMediaPlaybackLock(lock: false)
        
        self.pause()
        
        storyViewManager.isCompletelyVisible = false
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
//        print("StoryViewController scrollViewWillBeginDragging", scrollView.contentOffset.x)
        // NOTE: Stop image/video download tasks. Pause players, pause progress
        
        self.pauseVisibleStoryCollectionViewCell()
        self.pause()
        
        storyViewManager.isCompletelyVisible = false
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        let sortedCells = collectionView.visibleCells.sortedArrayByPosition()
        guard let firstCell = sortedCells.first as? StoryCollectionViewCell else { return }
        guard let lastCell = sortedCells.last as? StoryCollectionViewCell else { return }
        
        guard let firstIndexPath = collectionView.indexPath(for: firstCell) else { return }
        guard let lastIndexPath = collectionView.indexPath(for: lastCell) else { return }
        
        if firstIndexPath.row == storyViewModel.getStoryCount() - 1 {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                self.dismissStoryViewController()
            }
        } else if lastIndexPath.row == 0 {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                self.dismissStoryViewController()
            }
        }
    }
}
