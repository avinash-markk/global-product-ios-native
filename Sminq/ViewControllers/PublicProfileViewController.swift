//
//  PublicProfileViewController.swift
//  Sminq
//
//  Created by Bjorn Mascarenhas on 01/08/18.
//  Copyright © 2018 Suraj Sagare. All rights reserved.
//

import UIKit

import Gloss
import SVProgressHUD
import CleverTapSDK
import AccountKit
import FirebaseAuth
import GoogleSignIn
import Firebase
import CoreData
import FBSDKLoginKit
import Toast_Swift

protocol PublicProfileViewControllerDelegate: NSObjectProtocol {
    func resumeStoriesFromPublicProfileViewController()
    func removeFullScreenOverlayFromStories()
}

extension PublicProfileViewControllerDelegate {
    
    func resumeStoriesFromPublicProfileViewController() {
        // Optional method
    }
    
}

class PublicProfileViewController: UIViewController, UIGestureRecognizerDelegate {
    public weak var delegate: PublicProfileViewControllerDelegate?
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var storiesCollectionView: UICollectionView!
    @IBOutlet weak var storiesCollectionViewFlowLayout: UICollectionViewFlowLayout!
    
    @IBOutlet weak var userStatsUIView: UIView!
    @IBOutlet weak var userInfoUIView: UIView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var userDisplayNameLabel: UILabel!
    @IBOutlet weak var editProfileButton: UIButton!
    @IBOutlet weak var postCountLabel: UILabel!
    @IBOutlet weak var followingCountLabel: UILabel!
    @IBOutlet weak var userProfileImageView: UIImageView!
    @IBOutlet weak var userProfileInitialLabel: UILabel!
    
    @IBOutlet weak var userOwnProfileSettingsStackView: UIStackView!
    
    @IBOutlet weak var userPublicProfileSettingsStackView: UIStackView!
    
    @IBOutlet weak var userFollowingStack: UIStackView!
    @IBOutlet weak var userPostsStack: UIStackView!
    @IBOutlet weak var userFollowersStack: UIStackView!
    
    
    @IBOutlet weak var followButton: UIButton!
    
    @IBOutlet weak var followerCountLabel: UILabel!
    @IBOutlet weak var storyEmptyStateUIView: UIView!
    @IBOutlet weak var storyEmptyStateUILabel: UILabel!
    @IBOutlet weak var storyEmptyStateImageView: UIImageView!
    
    @IBOutlet weak var storyEmptyStateSubHeadingUILabel: UILabel!
    
    @IBOutlet weak var userBioLabel: UILabel!
    @IBOutlet weak var userJoinedAtLabel: UILabel!
    
    @IBOutlet weak var storiesCollectionViewHeightConstraint: NSLayoutConstraint!
    var navigationTitleLabel = UILabel()
    
    var userLocation: CLLocationCoordinate2D! = CLLocationCoordinate2D(latitude: 39.099427, longitude: -94.594345)
    
    var storyPlaceCardHeight: Int = 230
    var isMyProfile: Bool? = true
    var userId: String?
    var userDetails: UserDataDict?
    
    var storyList = [StoryData]()
    let LocationMgr = UserLocationManager.SharedManager
    var accountKit: AKFAccountKit?
    var presentingController: Bool? = false
    
    var source: String? = ""
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!

        isMyProfile = true
        userId = ""
    }
    
    override public var prefersStatusBarHidden: Bool {
        return false
    }
    
    override public var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        self.storiesCollectionView.register(UINib(nibName: "PlaceStoryCardCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "PlaceStoryCardCollectionViewCell")
        self.storiesCollectionView.register(UINib(nibName: "EmptyTableViewCell", bundle: nil), forCellWithReuseIdentifier: "EmptyTableViewCell")
        
        NotificationCenter.default.addObserver(self, selector: #selector(addProfileTabAnalytics), name: NSNotification.Name("Add_Profile_Tab_Analytics"), object: nil)
        
        self.addProfileTabAnalytics()
    }
    
    @objc func addProfileTabAnalytics() {
        if isMyProfile! {
            AnalyticsHelper.viewProfileEvent(screen: AppConstants.screenNames.profile, source: source!, userId: Helper().getUserId(), isSelf: true)
        } else {
            AnalyticsHelper.viewProfileEvent(screen: AppConstants.screenNames.profile, source: source!, userId: userId!, isSelf: false)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    
        setNeedsStatusBarAppearanceUpdate()
        
        if isMyProfile! {
//            getUserData(userId: Helper().getUserId())
            self.editProfileButton.isHidden = false
            self.userOwnProfileSettingsStackView.isHidden = false
            self.userPublicProfileSettingsStackView.isHidden = true
        } else {
//            getUserData(userId: userId!)
            self.editProfileButton.isHidden = true
            self.userOwnProfileSettingsStackView.isHidden = true
            self.userPublicProfileSettingsStackView.isHidden = false
        }
        
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.isHidden = false
        setupNavigationBar()
        //        self.setNeedsStatusBarAppearanceUpdate()
        
        showTabBar()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.isHidden = false
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    // MARK: Private functions
    
    func updateUserFollowingButton(userFollowing: Bool) {
        if userFollowing {
            self.followButton.setTitle("FOLLOWING", for: .normal)
            self.followButton.backgroundColor = AppConstants.SminqColors.purpleyGrey10
            self.followButton.setTitleColor(AppConstants.SminqColors.purpleyGrey, for: .normal)
            
        } else {
            self.followButton.setTitle("FOLLOW", for: .normal)
            self.followButton.backgroundColor = AppConstants.SminqColors.pinkishRed
            self.followButton.setTitleColor(UIColor.white, for: .normal)
        }
    }
    
    func setupUI() {
        scrollView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(feedImageViewTapped(_:))))
        
        scrollView.isUserInteractionEnabled = true
        // Collection View Layout
        storiesCollectionViewFlowLayout.sectionInset = UIEdgeInsets(top: 16, left: 16, bottom: 16, right: 16)
        storiesCollectionViewFlowLayout.minimumLineSpacing = 10
        
        self.followButton.titleLabel?.font = UIFont(name: AppConstants.Font.primaryBold, size: 14.0)
    }
    
    func setupNavigationBar() {
        // Setting Button
        
        navigationController?.navigationBar.barTintColor = AppConstants.SminqColors.pinkishRed
        navigationTitleLabel = UILabel(frame: CGRect(x: 44, y: 0, width: self.view.frame.width - (88), height: 44))
        
        if (navigationController?.viewControllers.count)! > 1 || self.presentingController! {
            let backButton: UIButton = UIButton(type: UIButtonType.custom)
            backButton.setImage(UIImage(named: "iconBack24White100"), for: .normal)
            backButton.frame = CGRect(x: -20, y: 0, width: 56, height: 44)
            backButton.isUserInteractionEnabled = false
            
            let view = UIView(frame: CGRect(origin: .zero, size: CGSize(width: 56, height: 44)))
            view.addSubview(backButton)
            view.isUserInteractionEnabled = true
            view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tappedBackButton)))
            
            navigationItem.leftBarButtonItem = UIBarButtonItem(customView: view)
            
            navigationTitleLabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.view.frame.width - (140), height: 44))
        }
        
        // Navbar title label
        
        navigationTitleLabel.font = UIFont(name: AppConstants.Font.primaryBold, size: 20.0)
        navigationTitleLabel.textColor = UIColor.white
        navigationTitleLabel.textAlignment = NSTextAlignment.center
        navigationItem.titleView = navigationTitleLabel
        navigationTitleLabel.text = "Profile"
    }
    
    func updateScrollViewHeight() {
        if self.storyList.count == 1 {
            self.storiesCollectionViewHeightConstraint.constant = CGFloat(storyPlaceCardHeight + 100)
        } else if self.storyList.count >= 2 {
            var roundedPlaceCount = CGFloat(self.storyList.count) / CGFloat(2)
            roundedPlaceCount.round()
            self.storiesCollectionViewHeightConstraint.constant = CGFloat((Int(roundedPlaceCount) * storyPlaceCardHeight) + 100)
            
        }
        
        self.view.layoutIfNeeded()
        
        self.scrollView.contentSize = CGSize(width: view.width, height: self.userStatsUIView.height + self.userInfoUIView.height + self.storiesCollectionView.height + 200)
    }
    
//    func getUserData(userId: String) {
//        if InternetConnection.isConnectedToInternet() {
//            SVProgressHUD.show()
//
//            guard let metric = HTTPMetric(url: URL(string: UserService().getUserAPIEndPoint(userId: userId))!, httpMethod: .get) else { return }
//            metric.start()
//
//            let getUserAPI = UserService().getUser(userId: userId, viewerId: Helper().getUserId())
//
//            getUserAPI.response.responseJSON { response in
//                switch response.result {
//                case .success:
//                    guard let json = response.result.value as? JSON else { return }
//
//                    if let userStoriesData: UserStoriesData = UserStoriesData(json: json) {
//                        if let httpCode = userStoriesData.httpCode {
//                            metric.responseCode = httpCode
//                        }
//
//                        if userStoriesData.success {
//                            if let stories = userStoriesData.status?.stories, stories.count > 0 {
//                                self.storyList = stories
//
//                                for (index, story) in self.storyList.enumerated() {
//                                    self.storyList[index].posts = story.copyPlaceDetailsToPostsAndReturnList()
//                                }
//
//                                self.storiesCollectionView.reloadData()
//                                self.updateScrollViewHeight()
//
//                                self.storyEmptyStateUIView.isHidden = true
//                            } else {
//                                self.storyEmptyStateUIView.isHidden = false
//                            }
//
//                            self.userDetails = userStoriesData.status?.user
//
//                            self.userNameLabel.text = self.userDetails?.name
//
//                            if let displayName = self.userDetails?.displayName[0] {
//                                self.userDisplayNameLabel.text = "@\(displayName)"
//                            }
//
//                            if self.userDetails?.bio != nil && self.userDetails?.bio != "" {
//                                self.userBioLabel.text = self.userDetails?.bio
//                            } else {
//                                self.userBioLabel.text = ""
//                            }
//
//                            // Get join formatted date
//                            let date = Date(timeIntervalSince1970: Double((self.userDetails?.createdAt)!))
//                            let dateFormatter = DateFormatter()
//
//                            //                        dateFormatter.locale = NSLocale.current
//                            dateFormatter.dateFormat = "MMM yyyy" // Specify your format that you want
//                            let joinedDate = dateFormatter.string(from: date)
//                            self.userJoinedAtLabel.text = "Joined \(joinedDate)"
//
//                            if self.isMyProfile! {
//                                self.navigationTitleLabel.text = "Profile"
//
//                                self.storyEmptyStateUILabel.text = "Show us what’s happening around you!"
//                                self.storyEmptyStateSubHeadingUILabel.text = "You’ve got no Live Stories right now."
//                                self.storyEmptyStateImageView.image = UIImage(named: "iconLogoHappy112")
//                            } else {
//                                if let displayName  = self.userDetails?.displayName[0] {
//                                    self.navigationTitleLabel.text = "@\(displayName)"
//
//                                    self.storyEmptyStateSubHeadingUILabel.text = "You’ll see @\(displayName)'s stories here when they post."
//                                }
//
//                                self.storyEmptyStateUILabel.text = "No Live Stories"
//                                self.storyEmptyStateImageView.image = UIImage(named: "iconLogoSad112")
//                            }
//
//                            self.userProfileImageView.sd_setImage(with: URL(string: (self.userDetails?.imageUrl)!), placeholderImage: nil)
//                            self.userProfileInitialLabel.text = "\(self.userDetails?.displayName[0].prefix(1).uppercased() ?? "")"
//
//                            self.updateUserFollowingButton(userFollowing: (self.userDetails?.userFollowing)!)
//
//                            if userStoriesData.status?.userMetrics != nil {
//                                if let postCount = userStoriesData.status?.userMetrics.posts {
//                                    self.postCountLabel.text = "\(postCount)"
//                                }
//
//                                if var followingCount = userStoriesData.status?.userMetrics.followed {
//                                    if followingCount <= 0 {
//                                        followingCount = 0
//                                    }
//
//                                    self.followingCountLabel.text = "\(followingCount)"
//                                }
//
//                                if var followersCount = userStoriesData.status?.userMetrics.followers {
//                                    if followersCount <= 0 {
//                                        followersCount = 0
//                                    }
//
//                                    self.followerCountLabel.text = "\(followersCount)"
//                                }
//                            }
//                        } else {
//                            LogUtil.error(response)
//                            // Error handling
//                            self.view.makeToast(AppConstants.ErrorMessages.apologiesMsg)
//
//                            if let error = userStoriesData.error {
//                                AnalyticsHelper.APIFailureEvent(screen: AppConstants.screenNames.profile, apiName: AppConstants.apiNames.getUserDetails, errorMessage: error.message!, userId: Helper().getUserId(), errorCode: error.code, requestPayload: getUserAPI.requestPayload, requestMethod: getUserAPI.requestMethod.rawValue, requestPathParams: getUserAPI.requestPathParams)
//                            }
//                        }
//
//                    } else {
//                        LogUtil.error(response)
//                        AnalyticsHelper.APIFailureEvent(screen: AppConstants.screenNames.profile, apiName: AppConstants.apiNames.getUserDetails, errorMessage: AppConstants.ErrorMessages.emptyResponse, userId: Helper().getUserId(), errorCode: nil, requestPayload: getUserAPI.requestPayload, requestMethod: getUserAPI.requestMethod.rawValue, requestPathParams: getUserAPI.requestPathParams)
//                    }
//                    break
//
//                case .failure(let error):
//                    LogUtil.error(error)
//
//                    self.view.makeToast(AppConstants.ErrorMessages.apologiesMsg)
//
//                    let error = error as NSError
//                    let userInfo = error.userInfo
//                    if let errorDesc = userInfo.valueForKeyPath(keyPath: "NSLocalizedDescription") as? String {
//                        AnalyticsHelper.APIFailureEvent(screen: AppConstants.screenNames.profile, apiName: AppConstants.apiNames.getUserDetails, errorMessage: errorDesc, userId: Helper().getUserId(), errorCode: nil, requestPayload: getUserAPI.requestPayload, requestMethod: getUserAPI.requestMethod.rawValue, requestPathParams: getUserAPI.requestPathParams)
//                    }
//
//                    break
//                }
//
//                SVProgressHUD.dismiss()
//                metric.stop()
//            }
//        } else {
//            self.view.makeToast(AppConstants.ErrorMessages.apologiesMsg)
//        }
//    }
    
    @objc func followingTapped(_ sender: UITapGestureRecognizer) {
    }
    
    @objc func followersTapped(_ sender: UITapGestureRecognizer) {
    }
    
    // MARK: Button actions
    
    @IBAction func userProfileSettingsButtonTapped(_ sender: UIButton) {
        let actionSheetController: UIAlertController = UIAlertController(title: "v\(AppConstants.appVersion)", message: nil, preferredStyle: .actionSheet)
        
        let cancelActionButton = UIAlertAction(title: "Cancel", style: .cancel) { _ in
            
        }
        actionSheetController.addAction(cancelActionButton)
        
        let feedBackButton = UIAlertAction(title: "Feedback", style: .default) { _ in
            //            AnalyticsHelper.feedbackEvent()
            if InternetConnection.isConnectedToInternet() {
                if let tcWebViewController = AppConstants.storyboard.instantiateViewController(withIdentifier: "TCWebViewController") as? TCWebViewController {
                    tcWebViewController.strWebURL = UIApplication.shared.getFeedBackURL()
                    tcWebViewController.strTitle = "Share Feedback"
                    tcWebViewController.hidesBottomBarWhenPushed = true
                    self.navigationController?.pushViewController(tcWebViewController, animated: true)
                }
            }
        }
        actionSheetController.addAction(feedBackButton)
        
        let logoutButton = UIAlertAction(title: "Logout", style: .destructive) { _ in
            let actionSheetController: UIAlertController = UIAlertController(title: "Are you sure you want to logout ?", message: nil, preferredStyle: .actionSheet)
            
            let cancelActionButton = UIAlertAction(title: "CANCEL", style: .cancel) { _ in
            }
            actionSheetController.addAction(cancelActionButton)
            
            let logOutActionButton = UIAlertAction(title: "LOGOUT", style: .default) { _ in
                self.accountKit?.logOut()
                UserDefaults.standard.set(false, forKey: "AccountKitSession")
                UserDefaults.standard.set(false, forKey: "SignIn")
                UserDefaults.standard.removeObject(forKey: "MyProfile")
                NotificationCenter.default.removeObserver(self)
                
                NotificationCacheService.shared.clearCachedNotifications(completion: {})
                
                UploadManager.sharedInstance.deleteAllRecords()
                
                // Register for sminq APNS Server
                if UIImagePickerController.isSourceTypeAvailable(.camera) {
                    PushNotificationAPI.registerForSminqPushNotifications(userId: Helper().getUserId(), fcmToken: UserDefaults.standard.string(forKey: "FCMToken")!, register: AppConstants.disablePush)
                    //   UserDefaults.standard.removeObject(forKey: "FCMToken")
                }
                
                let firebaseAuth = Auth.auth()
                do {
                    try firebaseAuth.signOut()
                } catch let signOutError as NSError {
                    LogUtil.error(signOutError)
                }
                
                GIDSignIn.sharedInstance().disconnect()
                GIDSignIn.sharedInstance().signOut()
                
                FBSDKAccessToken.current()
                FBSDKLoginManager().logOut()
                
                AnalyticsHelper.logoutEvent()
                // Make it a root controller
                AppConstants.appDelegate.window?.rootViewController = UINavigationController(rootViewController: AppConstants.storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController)
            }
            
            actionSheetController.addAction(logOutActionButton)
            self.present(actionSheetController, animated: true, completion: nil)
        }
        actionSheetController.addAction(logoutButton)
        
        self.present(actionSheetController, animated: true, completion: nil)
    }
    
    @IBAction func followButtonTapped(_ sender: UIButton) {
        if self.userDetails == nil {
            return
        }
        
        if (self.userDetails?.userFollowing)! {
            confirmBeforeUnfollowing()
        } else {
//            followUser()
        }
    }
    
    func confirmBeforeUnfollowing() {
        let actionSheetController: UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let cancelActionButton = UIAlertAction(title: "Cancel", style: .cancel) { _ in
        }
        actionSheetController.addAction(cancelActionButton)
        let actionButton = UIAlertAction(title: "Unfollow", style: .default) { _ in
//            self.unFollowUser()
        }
        actionSheetController.addAction(actionButton)
        self.present(actionSheetController, animated: true, completion: nil)
    }
    
    @IBAction func editButtonTapped(_ sender: UIButton) {
        AnalyticsHelper.editProfileEvent()
        
        if let editProfileViewController = AppConstants.storyboard.instantiateViewController(withIdentifier: "EditProfileViewController") as? EditProfileViewController {
            self.hideTabBar()
            self.navigationController?.pushViewController(editProfileViewController, animated: true)
        }
    }
    
    // MARK: Target methods
    
    @objc func tappedBackButton() {
        if self.presentingController! {
            self.dismiss(animated: true, completion: nil)
        } else {
            // TODO: Check TOP controller is StoriesViewController. If yes then only call resume delegate method
            self.delegate?.resumeStoriesFromPublicProfileViewController()
            self.delegate?.removeFullScreenOverlayFromStories()
            self.navigationController?.popViewController(animated: true)
        }
    }

}

extension PublicProfileViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    internal func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.storyList.count
    }
    
    internal func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PlaceStoryCardCollectionViewCell", for: indexPath) as? PlaceStoryCardCollectionViewCell {
            
            let story = self.storyList[indexPath.row]
            
            cell.setStory(story: story)
            cell.updateDataOnCard()
            
            return cell
        } else {
            return UICollectionViewCell()
        }
    }
    
    func loadArchivedPostsView() {
        if let archivedPostsViewController = AppConstants.storyboard.instantiateViewController(withIdentifier: "ArchivedPostsViewController") as? ArchivedPostsViewController {
        self.navigationController?.pushViewController(archivedPostsViewController, animated: true)
        }
    }
    
    @objc func feedImageViewTapped(_ sender: UITapGestureRecognizer) {
        let touchLocation: CGPoint = sender.location(ofTouch: 0, in: storiesCollectionView)
        
        let postStackPoint = userPostsStack.convert(touchLocation, from: storiesCollectionView)
        if userPostsStack.point(inside: postStackPoint, with: nil) {
            if self.isMyProfile! {
                self.loadArchivedPostsView()
            }
            
            return
        }
        
        let indexPath: IndexPath? = storiesCollectionView.indexPathForItem(at: touchLocation)
        
        if let tempIndexPath = indexPath {
            if let storiesViewController = AppConstants.storyboard.instantiateViewController(withIdentifier: "StoriesViewController") as? StoriesViewController {
                storiesViewController.distance = ""
                
                guard let place = self.storyList[tempIndexPath.row].placeDetails else { return }
                
                AnalyticsHelper.visitedStoryView(screen: AppConstants.screenNames.profile, action: "tap", tab: "", place: place)
                
                let storyViewModel = StoryViewModel(screen: AppConstants.screenNames.storyView, storyList: self.storyList)
                
                storiesViewController.storyViewModel = storyViewModel
                storiesViewController.placeId =  place._id
                storiesViewController.source = AppConstants.screenNames.profile
                
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: AppConstants.AppNotificationNames.hideBottomBar), object: nil, userInfo: nil)
                
                self.navigationController?.pushViewController(storiesViewController, animated: false)
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let totalWidth = collectionView.bounds.size.width
        let numberOfCellsPerRow = 2
        let dimension = CGFloat(Int(totalWidth) / numberOfCellsPerRow) - 20
        return CGSize.init(width: dimension, height: CGFloat(storyPlaceCardHeight))
    }
    
}
