//
//  SearchViewController.swift
//  Markk
//
//  Created by Pushkar Deshmukh on 27/06/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import UIKit

protocol SearchViewControllerDelegate: NSObjectProtocol {
    func didCancelButtonTap()
}

class SearchViewController: UIViewController {
    public weak var delegate: SearchViewControllerDelegate?
    
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var tableView: UITableView!
    
    fileprivate lazy var errorView: ErrorView = {
        let view = ErrorView()
        view.delegate = self
        view.isHidden = true
        return view
    }()
    
    private var finishedLoadingInitialTableCells = false
    
    private let googlePlacesViewModel = GooglePlaceViewModel.init(screen: AppConstants.screenNames.search, places: [])!
    
    var activityIndicator: UIActivityIndicatorView = {
        let indicator = UIActivityIndicatorView()
        indicator.stopAnimating()
        indicator.activityIndicatorViewStyle = .whiteLarge
        return indicator
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        setupUI()
        getNearbyPlaces()
    }
    
    override public var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewSafeAreaInsetsDidChange() {
        updateSafeAreaViews()
    }
    
    // MARK: Private functions
    
    fileprivate func setupUI() {
        view.backgroundColor = AppConstants.SminqColors.blackDark100
        
        tableView.register(UINib(nibName: "SearchTableViewCell", bundle: nil), forCellReuseIdentifier: "SearchTableViewCell")
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorColor = AppConstants.SminqColors.white100.withAlphaComponent(0.1)
        tableView.backgroundColor = .clear
        tableView.keyboardDismissMode = .interactive
        
        searchTextField.backgroundColor = AppConstants.SminqColors.blackLight100
        searchTextField.layer.borderColor = AppConstants.SminqColors.white100.withAlphaComponent(0.1).cgColor
        searchTextField.layer.borderWidth = 1
        searchTextField.layer.cornerRadius = 20
        searchTextField.clipsToBounds = true
        searchTextField.attributedPlaceholder = NSAttributedString(string: "Search for a place", attributes: [NSAttributedStringKey.foregroundColor: AppConstants.SminqColors.white100.withAlphaComponent(0.5)])
        searchTextField.font = UIFont(name: AppConstants.Font.primaryRegular, size: 14)
        searchTextField.textColor = AppConstants.SminqColors.white100
        searchTextField.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        searchTextField.delegate = self
        
        cancelButton.setTitleColor(AppConstants.SminqColors.white100.withAlphaComponent(0.75), for: .normal)
        cancelButton.titleLabel?.font = UIFont(name: AppConstants.Font.primaryRegular, size: 14)
        cancelButton.setTitle("Cancel", for: .normal)
        
        view.addSubview(errorView)
        updateSafeAreaViews()
        
        view.addSubview(activityIndicator)
        
        activityIndicator.snp.makeConstraints { (make) in
            make.centerX.centerY.equalTo(self.view)
        }
        
    }
    
    fileprivate func updateSafeAreaViews() {
        let errorViewHeight = self.getSafeAreaInsets().top + 64
        errorView.frame = CGRect(x: 0, y: 0, width: view.width, height: errorViewHeight)
        errorView.setStackViewTopConstraint(top: self.getSafeAreaInsets().top)
    }
    
    fileprivate func getNearbyPlaces() {
        let location = UserDefaultsUtility.shared.getHundredMeterLocationOtherwiseLastKnownLocation()
        
        if let latitude = location.latitude, let longitude = location.longitude {
            activityIndicator.startAnimating()
            tableView.reloadData()
            
            googlePlacesViewModel.getGoogleNearbyPlaces(latitude: latitude, longitude: longitude) { (googlePlaces, apiResponseState, networkError, error) in
                if let error = error {
                    self.errorView.animateInViewWithError(errorObject: error, autoDismiss: true)
                    return
                }
                
                self.activityIndicator.stopAnimating()
                self.tableView.reloadData()
                
                
            }
        }
    }
    
    fileprivate func getPlacesBySearch(text: String) {
        let location = UserDefaultsUtility.shared.getHundredMeterLocationOtherwiseLastKnownLocation()
        
        if let latitude = location.latitude, let longitude = location.longitude {
            activityIndicator.startAnimating()
            tableView.reloadData()
            
            googlePlacesViewModel.getGooglePlacesBySearch(latitude: latitude, longitude: longitude, input: text) { (places, apiResponseState, networkError, error) in
                
                if let error = error {
                    self.errorView.animateInViewWithError(errorObject: error, autoDismiss: true)
                    return
                }
                
                AnalyticsHelper.searchEvent(screen: AppConstants.screenNames.search, searchedFor: text, resultsReturned: self.googlePlacesViewModel.getPlacesCount() == 0 ? AppConstants.no : AppConstants.yes)
                
                self.activityIndicator.stopAnimating()
                self.tableView.reloadData()
            }
        }
    }
    
    fileprivate func didPlaceSelect(googlePlace: GooglePlace) {
        let placeDataClass = PlaceDataClass.init(screen: AppConstants.screenNames.search)!
        activityIndicator.startAnimating()
        placeDataClass.getPlaceDetails(identity: googlePlace.place_id) { (placeDetail, apiResponseState, networkError, error) in
            
            DispatchQueue.main.async {
                self.activityIndicator.stopAnimating()
                
                if let error = error {
                    self.errorView.animateInViewWithError(errorObject: error, autoDismiss: true)
                    return
                }
                
                guard let placeDetail = placeDetail, let place = placeDetail.place else { return }
                self.navigateToPlaceFeedController(place: place)
                
                AnalyticsHelper.tappedSearchResultEvent(searchedString: self.searchTextField.text ?? "", resultsReturned: self.googlePlacesViewModel.getPlacesCount() == 0 ? AppConstants.no : AppConstants.yes, place: place)
            }
            
        }
    }
    
    fileprivate func navigateToPlaceFeedController(place: LivePlaces) {
        if let placeFeedViewController = AppConstants.storyboard.instantiateViewController(withIdentifier: "PlaceFeedViewController") as? PlaceFeedViewController {
            
            placeFeedViewController.place = place
            
            let navController = UINavigationController(rootViewController: placeFeedViewController)
            navController.modalPresentationStyle = .overCurrentContext
            navController.modalPresentationCapturesStatusBarAppearance = true
            
            self.present(navController, animated: true, completion: nil)
        }
    }

    // MARK: Button actions
  
    @IBAction func cancelButtonAction(_ sender: Any) {
        delegate?.didCancelButtonTap()
        self.dismiss(animated: true, completion: nil)
    }
    
    
    // MARK: @objc functions
    
    @objc fileprivate func textFieldDidChange() {
        guard let text = searchTextField.text else { return }
        
        if text.count == 0 {
            getNearbyPlaces()
        } else  if text.count > 1 {
            getPlacesBySearch(text: text)
        }
        
    }
    
    func getHeaderText() -> String {
        var headerText = ""
        guard let searchText = searchTextField.text else { return headerText }
        
        if activityIndicator.isAnimating {
            headerText = "FETCHING PLACES..."
        } else if googlePlacesViewModel.getPlacesCount() == 0 && searchText.count == 0 {
            headerText = "START TYPING TO FIND A PLACE NEAR YOU..."
        } else if googlePlacesViewModel.getPlacesCount() > 0 && searchText.count == 0 {
            headerText = "NEARBY PLACES"
        } else if googlePlacesViewModel.getPlacesCount() == 0 && searchText.count > 0 {
            headerText = "NO RESULTS FOUND FOR \"\(searchText)\""
        }
        
        return headerText
    }
    
}

extension SearchViewController: UITableViewDelegate {
    
}

extension SearchViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return googlePlacesViewModel.getPlacesCount()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "SearchTableViewCell", for: indexPath) as? SearchTableViewCell else { return UITableViewCell() }
        
        if let place = googlePlacesViewModel.getPlaceAt(index: indexPath.row) {
            cell.setGooglePlace(place: place)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 0 {
            let headerView = UIView()
            
            let label = UILabel()
            label.text = getHeaderText()
            label.font = UIFont(name: AppConstants.Font.primaryBold, size: 12)
            label.textColor = AppConstants.SminqColors.white100.withAlphaComponent(0.75)
            
            headerView.addSubview(label)
            label.anchor(top: nil, leading: headerView.leadingAnchor, bottom: headerView.bottomAnchor, trailing: headerView.trailingAnchor, padding: .init(top: 0, left: 16, bottom: 1, right: 0), size: .init(width: 0, height: 31))
            
            let separatorView = UIView()
            separatorView.backgroundColor = AppConstants.SminqColors.white100.withAlphaComponent(0.1)

            headerView.addSubview(separatorView)
            separatorView.anchor(top: nil, leading: headerView.leadingAnchor, bottom: headerView.bottomAnchor, trailing: headerView.trailingAnchor, padding: .zero, size: .init(width: 0, height: 1))
            
            return headerView
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return 12 + 32
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        var lastInitialDisplayableCell = false
        
        //change flag as soon as last displayable cell is being loaded (which will mean table has initially loaded)
        if true && !finishedLoadingInitialTableCells {
            if let indexPathsForVisibleRows = tableView.indexPathsForVisibleRows,
                let lastIndexPath = indexPathsForVisibleRows.last, lastIndexPath.row == indexPath.row {
                lastInitialDisplayableCell = true
            }
        }
        
        if !finishedLoadingInitialTableCells {
            
            if lastInitialDisplayableCell {
                finishedLoadingInitialTableCells = true
            }
            
            //animates the cell as it is being displayed for the first time
            cell.transform = CGAffineTransform(translationX: 0, y: 100)
            cell.alpha = 0
            
            UIView.animate(withDuration: 0.5, delay: 0.05 * Double(indexPath.row), options: [.curveEaseInOut], animations: {
                cell.transform = CGAffineTransform(translationX: 0, y: 0)
                cell.alpha = 1
            }, completion: nil)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        guard let place = googlePlacesViewModel.getPlaceAt(index: indexPath.row) else { return }
        searchTextField.resignFirstResponder()
        didPlaceSelect(googlePlace: place)
    }
    
}

extension SearchViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

extension SearchViewController: ErrorViewDelegate {
    func errorViewTapped() {
        errorView.isHidden = true
        getNearbyPlaces()
    }
}
