//
//  StoryViewController.swift
//  Sminq
//
//  Created by Pushkar Deshmukh on 17/05/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import UIKit
import SDWebImage
import SVProgressHUD
import Toaster
import MessageUI

protocol StoryViewControllerDelegate: NSObjectProtocol {
    func didDismissStoryView()
    func didUpvotePost(post: UserDataPlaceFeed)
}

extension StoryViewControllerDelegate {
//    func didUpvotePost(post: UserDataPlaceFeed) {
//        
//    }
}

class StoryViewController: UIViewController {
    
    weak var delegate: StoryViewControllerDelegate?
    
    public lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = 0
        
        let collectionView = UICollectionView.init(frame: .zero, collectionViewLayout: layout)
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.isPagingEnabled = true
        collectionView.isDirectionalLockEnabled = true
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.register(StoryCollectionViewCell.self, forCellWithReuseIdentifier: "StoryCollectionViewCell")
        
        return collectionView
    }()
    
    fileprivate lazy var storyCommentsView: StoryCommentsView = {
        let view = StoryCommentsView()
        view.delegate = self
        return view
    }()
    
    fileprivate lazy var storyShareView: StoryShareView = {
        let view = StoryShareView()
        view.delegate = self
        return view
    }()
    
    fileprivate lazy var errorView: ErrorView = {
        let view = ErrorView()
        view.delegate = self
        view.isHidden = true
        return view
    }()
    
    fileprivate var storyCommentsViewTopConstraint: CGFloat = 0 {
        didSet {
            updateGestures(enable: storyCommentsViewTopConstraint > 0)
        }
    }
    
    fileprivate var storyShareViewTopConstraint: CGFloat = 0 {
        didSet {
            updateGestures(enable: storyShareViewTopConstraint > 0)
        }
    }
    
    public let storyViewManager: StoryViewManager
    fileprivate var messageUIService = MessageUIService.shared
    
    var storyViewModel = StoryViewModel.init(screen: AppConstants.screenNames.storyView)!
    var storyViewTrackViewModel = StoryViewTrackViewModel(screen: AppConstants.screenNames.storyView, storyViewList: [])!
    
    public var storyIndex: Int = 0 {
        didSet {
            storyViewModel.setStorySnapIndex(index: storyIndex)
            storyViewManager.snappedIndex = storyIndex
            scrollToStory(index: storyIndex, animated: true)
        }
    }
    
    public var selectedPlace: LivePlaces?
    public var openWithStoryCommentsView = false
    public var feedItemId: String? // Note: If this id is present then all other feed items from feedList is removed
    
    init(storyViewManager: StoryViewManager) {
        self.storyViewManager = storyViewManager
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
//        setupStoryViewConfig()
        loadSelectedPlacePosts()
        setupObservers()
        initDelegates()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        disableNavigationBar()
    }

    override public var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewSafeAreaInsetsDidChange() {
        updateSafeAreaViews()
    }
    
    @objc fileprivate func handlePan(gesture: UIPanGestureRecognizer) {
        switch gesture.state {
        case .began: handlePanBeganState(gesture: gesture)
        case .changed: handlePanChangedState(gesture: gesture)
        case .ended: handlePanEndedState(gesture: gesture)
        default:
            break
        }
    }
    
    // MARK: Fileprivate functions
    
    func updateStoryViewCounts() {
        if storyViewTrackViewModel.getStoryViewListCount() > 0 {
            storyViewTrackViewModel.postStoryMetrics { (apiResponseState, networkError) in
            }
        }
    }
    
    fileprivate func initDelegates() {
        messageUIService.delegate = self
    }
    
    fileprivate func setupObservers() {
        NotificationCenter.default.removeObserver(self)
        
        NotificationCenter.default.addObserver(self, selector: #selector(handleUIApplicationDidBecomeActive), name: NSNotification.Name.UIApplicationDidBecomeActive, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleUIApplicationWillResignActive), name: NSNotification.Name.UIApplicationWillResignActive, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleUIApplicationWillEnterForeground), name: NSNotification.Name.UIApplicationWillEnterForeground, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleUIApplicationDidEnterBackground), name: NSNotification.Name.UIApplicationDidEnterBackground, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(handleUIApplicationWillTerminate), name: NSNotification.Name.UIApplicationWillTerminate, object: nil)
        
    }
    
    fileprivate func updateGestures(enable: Bool) {
        removeViewGestureRecognizers()
        
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(handlePan))
        
        if enable {
            collectionView.isScrollEnabled = true
            view.addGestureRecognizer(panGesture)
        } else {
            collectionView.isScrollEnabled = false
            view.removeGestureRecognizer(panGesture)
        }
    }
    
    fileprivate func disableNavigationBar() {
        navigationController?.isNavigationBarHidden = true
    }
    
    fileprivate func removeViewGestureRecognizers() {
        if let recognizers = view.gestureRecognizers {
            recognizers.forEach({ (recognizer) in
                view.removeGestureRecognizer(recognizer)
            })
        }
    }
    
//    fileprivate func setupStoryViewConfig() {
//        if let feedId = feedItemId {
//            storyViewManager.feedItemId = feedId
//        }
//
//        collectionView.reloadData()
//    }
    
    fileprivate func loadSelectedPlacePosts() {
        guard let place = selectedPlace, let index = storyViewModel.getPlaceIndexInStory(place: place) else { return }
        
//        let indexPath = IndexPath.init(row: index, section: 0)
        self.storyViewModel.setStorySnapIndex(index: index)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            UIView.animate(withDuration: 0.1, animations: {
                self.scrollToStory(index: index, animated: false)
            }) { (finished) in
                if finished {
                    self.storyViewManager.isCompletelyVisible = true
                    guard let cell = self.collectionView.visibleCells.first as? StoryCollectionViewCell else { return }
                    
                    cell.createProgressViews()
                    cell.setMediaPlaybackLock(lock: false)
                    self.storyIndex = index
                    cell.setStory(story: self.storyViewModel.storyList[self.storyIndex], storyViewManager: self.storyViewManager, isCellEndedDisplaying: true)
                }
            }
        }
    }
    
    fileprivate func handlePanBeganState(gesture: UIPanGestureRecognizer) {
        guard let cell = collectionView.visibleCells.first as? StoryCollectionViewCell else { return }
        cell.setMediaPlaybackLock(lock: true)
        cell.pause()
    }
    
    fileprivate func handlePanChangedState(gesture: UIPanGestureRecognizer) {
        let translation = gesture.translation(in: nil)
        
        if translation.y > 0 {
            view.frame.origin = CGPoint(x: 0, y: translation.y)
        }
        
    }
    
    fileprivate func handlePanEndedState(gesture: UIPanGestureRecognizer) {
        let translation = gesture.translation(in: nil)
        
        if translation.y > StoryViewConfig.panGestureOffset {
            dismissStoryViewController()
        } else if translation.y < 0 && abs(translation.y) > StoryViewConfig.panGestureOffset {
            guard let storyData = storyViewModel.getCurrentSnappedStory(), let place = storyData.placeDetails else { return }
            navigateToPlaceFeedViewController(place: place)
        } else {
            UIView.animate(withDuration: AppConstants.defaultAnimateDuration) { [weak self] in
                guard let this = self else { return }
                
                this.view.frame.origin = CGPoint(x: 0, y: 0)
                
                guard let cell = this.collectionView.visibleCells.first as? StoryCollectionViewCell else { return }
                cell.setMediaPlaybackLock(lock: false)
                cell.resume()
            }
        }
    }
    
    fileprivate func setupUI() {
        disableNavigationBar()
        
        view.backgroundColor = .black
        view.addSubview(collectionView)
        
        view.addSubview(storyCommentsView)
        view.addSubview(errorView)
        view.addSubview(storyShareView)
        
        storyCommentsView.snp.makeConstraints { (make) in
            make.left.right.equalTo(view)
            make.height.equalTo(view)
            make.top.equalTo(view).offset(view.height)
        }
        
        storyCommentsViewTopConstraint = view.height
        
        storyShareView.snp.makeConstraints { (make) in
            make.left.right.equalTo(view)
            make.height.equalTo(view)
            make.top.equalTo(view).offset(view.height)
        }
        
        storyShareViewTopConstraint = view.height
        
        updateSafeAreaViews()
    }
    
    fileprivate func updateSafeAreaViews() {
        collectionView.anchor(top: view.topAnchor, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor, padding: self.getSafeAreaInsets())
        
        let errorViewHeight = self.getSafeAreaInsets().top + 64
        errorView.frame = CGRect(x: 0, y: 0, width: view.width, height: errorViewHeight)
        errorView.setStackViewTopConstraint(top: self.getSafeAreaInsets().top)
    }
    
    fileprivate func animateStoryCommentsView(animate: Bool) {
        UIView.animate(withDuration: AppConstants.defaultAnimateDuration) {
            self.storyCommentsView.snp.updateConstraints({ [weak self] (make) in
                guard let this = self else { return }
                if animate {
                    this.storyCommentsViewTopConstraint = 0
                } else {
                    this.storyCommentsViewTopConstraint = this.view.height
                }
                
                make.top.equalTo(this.storyCommentsViewTopConstraint)
            })
            self.view.layoutIfNeeded()
        }
    }
    
    fileprivate func animateStoryShareView(animate: Bool) {
        UIView.animate(withDuration: AppConstants.defaultAnimateDuration) {
            self.storyShareView.snp.updateConstraints({ [weak self] (make) in
                guard let this = self else { return }
                if animate {
                    this.storyShareViewTopConstraint = 0
                } else {
                    this.storyShareViewTopConstraint = this.view.height
                }
                
                make.top.equalTo(this.storyShareViewTopConstraint)
            })
            self.view.layoutIfNeeded()
        }
    }
    
    fileprivate func navigateToPlaceFeedViewController(place: LivePlaces?) {
        if let placeFeedViewController = AppConstants.storyboard.instantiateViewController(withIdentifier: "PlaceFeedViewController") as? PlaceFeedViewController {
            placeFeedViewController.delegate = self
            if let place = place {
                placeFeedViewController.place = place
            }
            
            let navController = UINavigationController(rootViewController: placeFeedViewController)
            navController.modalPresentationStyle = .overCurrentContext
            navController.modalPresentationCapturesStatusBarAppearance = true
            
            self.present(navController, animated: true, completion: nil)
        }
    }
    
    fileprivate func navigateToPublicProfileViewController(viewerDisplayName: String, source: String) {
        if let profileViewController = AppConstants.storyboard.instantiateViewController(withIdentifier: "ProfileViewController") as? ProfileViewController {
            SVProgressHUD.dismiss()
            
            if viewerDisplayName == Helper().getUserDisplayName() {
                profileViewController.isMyProfile = true
            } else {
                profileViewController.isMyProfile = false
            }
            
            profileViewController.displayName = viewerDisplayName
            profileViewController.source = source
            profileViewController.delegate = self
            self.navigationController?.pushViewController(profileViewController, animated: true)
        }
    }

    fileprivate func scrollToStory(index: Int, animated: Bool) {
        self.collectionView.scrollToItem(at: IndexPath.init(row: index, section: 0), at: .centeredHorizontally, animated: animated)
    }
    
    fileprivate func deletedFeed(_ feedResponse: UserDataPlaceFeed?, _ apiResponseState: APIResponseState, _ updatedFeed: UserDataPlaceFeed?, _ error: ErrorObject?) {
        guard let cell = collectionView.visibleCells.first as? StoryCollectionViewCell else { return }
        
        SVProgressHUD.dismiss()
        
        if let error = error {
            errorView.animateInViewWithError(errorObject: error, autoDismiss: true)
        } else {
            if let post = updatedFeed {
                cell.updateDeletedRatingViewState(post: post)
            }
        }
        
        cell.setMediaPlaybackLock(lock: false)
        cell.resume()
    }
    
    fileprivate func downvotedFeed(_ feedResponse: UserDataPlaceFeed?, _ apiResponseState: APIResponseState, _ networkError: Error?, _ updatedFeed: UserDataPlaceFeed, _ error: ErrorObject?) {
        guard let cell = collectionView.visibleCells.first as? StoryCollectionViewCell else { return }
        
        SVProgressHUD.dismiss()
        
        if let error = error {
            errorView.animateInViewWithError(errorObject: error, autoDismiss: true)
        } else {
            cell.updateDeletedRatingViewState(post: updatedFeed)
            AnalyticsHelper.reportRatingEvent(feed: updatedFeed)
        }
        
        cell.setMediaPlaybackLock(lock: false)
        cell.resume()
    }
 
    fileprivate func getAlertActionsForSettings(for post: UserDataPlaceFeed) -> [UIAlertAction] {
        let deleteRatingAction = UIAlertAction(title: AppConstants.deleteRating, style: .destructive)
        let cancelButtonAction = UIAlertAction(title: AppConstants.cancel, style: .cancel)
        let reportRatingAction = UIAlertAction(title: AppConstants.reportRating, style: .destructive)
        
        if post.user._id == Helper().getUserId() {
            return [deleteRatingAction, cancelButtonAction]
        } else {
            return [reportRatingAction, cancelButtonAction]
        }
    }
    
    fileprivate func getAlertActionsForReport() -> [UIAlertAction] {
        let cancelButtonAction = UIAlertAction(title: AppConstants.cancel, style: .cancel)
        let reportRatingAction = UIAlertAction(title: AppConstants.report, style: .destructive)
        return [reportRatingAction, cancelButtonAction]
    }
    
    fileprivate func getAlertActionsForDeleteRating() -> [UIAlertAction] {
        let cancelButtonAction = UIAlertAction(title: AppConstants.cancel, style: .cancel)
        let deleteAction = UIAlertAction(title: AppConstants.delete, style: .destructive)
        return [deleteAction, cancelButtonAction]
    }
    
    fileprivate func getAlertActionsForDeleteComment() -> [UIAlertAction] {
        let cancelButtonAction = UIAlertAction(title: AppConstants.cancel, style: .cancel)
        let deleteButtonAction = UIAlertAction(title: AppConstants.delete, style: .destructive)
        
        return [cancelButtonAction, deleteButtonAction]
    }
    
    fileprivate func updatePostViewCount(ratingIndex: Int) {
        guard let storyData = storyViewModel.getCurrentSnappedStory(),
            let place = storyData.placeDetails,
            let feedViewModel = storyData.feedViewModel
            else { return }
        
        guard let post = feedViewModel.getFeedAt(index: ratingIndex) else { return }
        guard let archived = post.archived else { return }
        
        if !archived && !post.isPostDeletedLocalFlag && storyViewManager.isCompletelyVisible  {
            guard let storyViewData = StoryViewData.init(json: ["id": post._id, "user": Helper().getUserId(), "place": place._id, "views": 1, "owner": post.user._id]) else { return }
            storyViewTrackViewModel.addStoryView(storyView: storyViewData)
            
        }
    }
    
    fileprivate func generateFIRLinkAndShareTo(post: UserDataPlaceFeed, place: LivePlaces, storyShareOption: StoryShareOption) {
        pauseVisibleStoryCollectionViewCell()
        SVProgressHUD.show()
        
        FIRDynamicLinksService.shared.generateFirebaseDynamicURLForPost(post: post, place: place) { [weak self] (firebaseURL) in
            guard let this = self else { return }
            
            SVProgressHUD.dismiss()
            
            if let url = firebaseURL {
                AnalyticsHelper.shareEvent(sharedTo: "", userId: Helper().getUserId(), url: url, feed: post)
                
                switch storyShareOption.id {
                    
                case .instagramStories: break
                case .messages: MessageUIService.shared.sendSMSText(phoneNumbers: nil, body: Helper().generateSMSShareText(place: place, url: url), controller: this)
                case .twitter: this.openTwitterApp(url: url)
                case .copyLink: this.copyUrlToPasteboardAndShowToast(url: url)
                case .more: this.openShareActivity(post: post, place: place, url: url)
                    
                }
            } else {
                this.errorView.animateInViewWithMessage(AppConstants.ErrorMessages.opsSomethingWentWrong, "")
            }
            
        }
    }
    
    fileprivate func openShareActivity(post: UserDataPlaceFeed, place: LivePlaces, url: URL) {
        let isVideo = post.getFeedMediaType() == MediaType.video ? true : false
        
        self.presentShareActivity(text: Helper().generateShareText(place: place, url: url, isVideo: isVideo)) { [weak self] (error) in
            guard let this = self else { return }
            
            if let _ = error {
                this.errorView.animateInViewWithMessage(AppConstants.ErrorMessages.opsSomethingWentWrong, "")
                return
            }
            
            this.resumeVisibleStoryCollectionViewCell()
        }
    }
    
    fileprivate func openTwitterApp(url: URL) {
        guard let urlScheme = URL(string: "\(OtherAppShareKeys.twitterPost.rawValue)?message=\(url.absoluteString)"),
            UIApplication.shared.canOpenURL(urlScheme) else {
                UIApplication.shared.open(NSURL(string: "http://twitter.com/share?text=\(url.absoluteString)")! as URL)
                return
        }
        
        UIApplication.shared.open(urlScheme)
    }
    
    fileprivate func openInstagramApp(mediaData: Data?, attributionURL: String?, post: UserDataPlaceFeed) {
        guard let mediaData = mediaData, let urlScheme = URL(string: OtherAppShareKeys.instagramStories.rawValue),
            UIApplication.shared.canOpenURL(urlScheme) else { return }
        
        var backgroundKey = ""
        if let type = post.getFeedMediaType() {
            if type == .image {
                backgroundKey = "com.instagram.sharedSticker.backgroundImage"
            } else {
                backgroundKey = "com.instagram.sharedSticker.backgroundVideo"
            }
        }
        
        let pasteboardItems = [
            [
                "\(backgroundKey)": mediaData,
                "com.instagram.sharedSticker.contentURL": attributionURL ?? "",
                "com.instagram.sharedSticker.backgroundTopColor": "#e09b07",
                "com.instagram.sharedSticker.backgroundBottomColor": "#ba8206"
            ]
        ]
    
        let pasteboardOptions: [UIPasteboard.OptionsKey: Any] = [.expirationDate: Date().addingTimeInterval(60 * 5)]
        
        // This call is iOS 10+, can use 'setItems' depending on what versions you support
        UIPasteboard.general.setItems(pasteboardItems, options: pasteboardOptions)
        
        UIApplication.shared.open(urlScheme)
    }
    
    fileprivate func copyUrlToPasteboardAndShowToast(url: URL) {
        UIPasteboard.general.string = url.absoluteString
        
        if let currentToast = ToastCenter.default.currentToast {
            currentToast.cancel()
        }
        
        ToastView.appearance().backgroundColor = AppConstants.SminqColors.white100
        ToastView.appearance().textColor = AppConstants.SminqColors.blackDark100
        
        let toast = Toast(text: AppConstants.Messages.copiedToClipboard, delay: 0, duration: 1)
        toast.show()
        
        resumeVisibleStoryCollectionViewCell()
    }
    
    fileprivate func handleInstagramStoriesSelect(post: UserDataPlaceFeed, place: LivePlaces) {
        
        if let url = MediaCacheManagerService.shared.getFeedMediaDocumentURL(feed: post) {
            // NOTE: Media is already cached
            if let type = post.getFeedMediaType() {
                if type == .image {
                    do {
                        let imageData = try Data(contentsOf: url)
                        if let image = ShareTemplateService.shared.createShareImageTemplate(post: post, place: place, postImage: UIImage.init(data: imageData)), let imageData = UIImagePNGRepresentation(image) {
                            self.openInstagramApp(mediaData: imageData, attributionURL: nil, post: post)
                        }
                        
                    } catch {
                        LogUtil.error(error)
                    }
                } else {
                    do {
                        let videoData = try Data(contentsOf: url)
                        self.openInstagramApp(mediaData: videoData, attributionURL: nil, post: post)
                    } catch {
                        LogUtil.error(error)
                    }
                }
            }
        } else {
            pauseVisibleStoryCollectionViewCell()
            // NOTE: Cache video and then share
            SVProgressHUD.show()
            MediaCacheManagerService.shared.saveFeedMediaInDocumentDirectory(feed: post) { [weak self] (savedFileURL, data) in
                guard let this = self else { return }
                SVProgressHUD.dismiss()
                
                if let type = post.getFeedMediaType() {
                    if type == .image {
                        if let image = ShareTemplateService.shared.createShareImageTemplate(post: post, place: place, postImage: UIImage.init(data: data)), let imageData = UIImagePNGRepresentation(image) {
                            this.openInstagramApp(mediaData: imageData, attributionURL: nil, post: post)
                        }
                    } else {
                        do {
                            let videoData = try Data(contentsOf: savedFileURL)
                            this.openInstagramApp(mediaData: videoData, attributionURL: nil, post: post)
                        } catch {
                            LogUtil.error(error)
                        }
                    }
                }
                
            }
        }
    }
    
    // MARK: @objc functions
    
    @objc fileprivate func handleUIApplicationDidBecomeActive() {
        print("handleUIApplicationDidBecomeActive")
        resumeVisibleStoryCollectionViewCell()
    }
    
    @objc fileprivate func handleUIApplicationWillResignActive() {
        print("handleUIApplicationWillResignActive")
        pauseVisibleStoryCollectionViewCell()
    }
    
    @objc fileprivate func handleUIApplicationWillEnterForeground() {
        print("handleUIApplicationWillEnterForeground")
        updateStoryViewCounts()
        reStartVisibleStoryCollectionViewCell()
    }
    
    @objc fileprivate func handleUIApplicationDidEnterBackground() {
        print("handleUIApplicationDidEnterBackground")
        updateStoryViewCounts()
        pauseVisibleStoryCollectionViewCell()
    }
    
    @objc fileprivate func handleUIApplicationWillTerminate() {
        print("handleUIApplicationWillTerminate")
        updateStoryViewCounts()
    }
    
    
    
    @objc public func dismissStoryViewController() {
        self.delegate?.didDismissStoryView()
        updateStoryViewCounts()
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: Public functions
    public func pause() {
        let visibleCells = collectionView.visibleCells.sortedArrayByPosition()
        visibleCells.forEach { (cell) in
            if let cell = cell as? StoryCollectionViewCell {
                cell.pause()
            }
        }
    }
    
    public func pauseVisibleStoryCollectionViewCell() {
        guard let cell = collectionView.visibleCells.first as? StoryCollectionViewCell else { return }
        cell.setMediaPlaybackLock(lock: true)
        cell.pause()
        
    }
    
    public func resumeVisibleStoryCollectionViewCell() {
        guard let cell = collectionView.visibleCells.first as? StoryCollectionViewCell else { return }
        cell.setMediaPlaybackLock(lock: false)
        cell.resume()
    }
    
    public func reStartVisibleStoryCollectionViewCell() {
        guard let cell = collectionView.visibleCells.first as? StoryCollectionViewCell else { return }
        cell.setMediaPlaybackLock(lock: false)
        cell.reStart()
    }
}

extension StoryViewController: UICollectionViewDelegateFlowLayout, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return collectionView.bounds.size
    }
}

extension StoryViewController: StoryCollectionViewCellDelegate {
    func didUpvotePost(post: UserDataPlaceFeed) {
        delegate?.didUpvotePost(post: post)
    }
    
    func didImageLoad(ratingCellIndex: Int) {
        updatePostViewCount(ratingIndex: ratingCellIndex)
        
    }
    
    func didVideoLoad(ratingCellIndex: Int) {
        updatePostViewCount(ratingIndex: ratingCellIndex)
    }
    
    func didRatingImageLoadWithError(error: Error?) {
        guard let _ = error else { return }
        errorView.animateInViewWithMessage(AppConstants.ErrorMessages.opsSomethingWentWrong, "Could not load the rating.", autoDismiss: true)
    }
    
    func didRatingVideoLoadWithError() {
        errorView.animateInViewWithMessage(AppConstants.ErrorMessages.opsSomethingWentWrong, "Could not load the rating.", autoDismiss: true)
    }
    
    func shouldGoToPreviousStory(currentCellIndex: Int) {
        if currentCellIndex <= 0 {
            return
        }
        
        scrollToStory(index: currentCellIndex - 1, animated: true)
        
    }
    
    func shouldGoToNextStory(currentCellIndex: Int) {
        if currentCellIndex >= storyViewModel.getStoryCount() - 1 {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                self.dismissStoryViewController()
            }
            
            return
        }
        
        scrollToStory(index: currentCellIndex + 1, animated: true)
    }
    
    func didCloseTap() {
        self.dismissStoryViewController()
    }
    
    func didSettingsTap(feedViewModel: FeedViewModel, post: UserDataPlaceFeed) {
        guard let cell = collectionView.visibleCells.first as? StoryCollectionViewCell else { return }
        cell.setMediaPlaybackLock(lock: true)
        cell.pause()
        
        let options = getAlertActionsForSettings(for: post)
        
        self.presentAlertSheet(title: nil, message: nil, options: options, controllerStyle: .actionSheet) { [weak self] (alertAction) in
            guard let actionTitle = alertAction.title, let self = self else { return }
            
            switch actionTitle {
            case AppConstants.deleteRating:
                
                self.presentAlertSheet(title: "Delete Rating?", message: "This cannot be undone. Are you sure?", options: self.getAlertActionsForDeleteRating(), controllerStyle: .alert, completion: { [weak self] (alertAction) in
                    guard let actionTitle = alertAction.title, let this = self else { return }
                    
                    switch actionTitle {
                    case AppConstants.delete:
                        SVProgressHUD.show()
                        feedViewModel.deleteFeed(feed: post, completion: this.deletedFeed)
                    case AppConstants.cancel:
                        cell.setMediaPlaybackLock(lock: false)
                        cell.resume()
                    default: break
                    }
                })
                
            case AppConstants.reportRating:
                self.presentAlertSheet(title: "Report rating?", message: "This post will be flagged for inappropriate content and will be moderated by us.", options: self.getAlertActionsForReport(), controllerStyle: .alert, completion: { [weak self] (alertAction) in
                    guard let actionTitle = alertAction.title, let this = self else { return }
                    
                    switch actionTitle {
                    case AppConstants.report:
                        SVProgressHUD.show()
                        feedViewModel.downvoteFeed(feed: post, reason: "Spam", completion: this.downvotedFeed)
                        
                    case AppConstants.cancel:
                        cell.setMediaPlaybackLock(lock: false)
                        cell.resume()
                    default: break
                    }
                })
                
            case AppConstants.cancel:
                print("Cancel rating selected")
                cell.setMediaPlaybackLock(lock: false)
                cell.resume()
            default:
                break
            }
        }
    }
    
    func didShareTap(feedViewModel: FeedViewModel, post: UserDataPlaceFeed, place: LivePlaces) {
        guard let cell = collectionView.visibleCells.first as? StoryCollectionViewCell else { return }
        cell.setMediaPlaybackLock(lock: true)
        cell.pause()
        
        print("Show share modal here!")
        storyShareView.setData(post: post, place: place)
        animateStoryShareView(animate: true)
    
    }
    
    func didCommentTap(storyData: StoryData, feedViewModel: FeedViewModel, post: UserDataPlaceFeed, storyLatestCommentsView: StoryLatestCommentsView, storyBottomControlsView: StoryBottomControlsView) {
        storyCommentsView.setData(storyData: storyData, post: post, storyLatestCommentsView: storyLatestCommentsView, storyBottomControlsView: storyBottomControlsView)
        
        if let archived = post.archived, !archived {
            storyCommentsView.keyboard(show: true)
        }
        
        animateStoryCommentsView(animate: true)
    }
    
    func didUserInitialLabelTap(post: UserDataPlaceFeed) {
        guard let cell = collectionView.visibleCells.first as? StoryCollectionViewCell else { return }
        cell.setMediaPlaybackLock(lock: true)
        cell.pause()
        
        navigateToPublicProfileViewController(viewerDisplayName: post.displayName, source: AnalyticsHelper.PublicProfileSource.post)
    }
    
    func didUserImageViewTap(post: UserDataPlaceFeed) {
        guard let cell = collectionView.visibleCells.first as? StoryCollectionViewCell else { return }
        cell.setMediaPlaybackLock(lock: true)
        cell.pause()
        
        navigateToPublicProfileViewController(viewerDisplayName: post.displayName, source: AnalyticsHelper.PublicProfileSource.post)
    }
    
    func didUserDisplayNameTap(post: UserDataPlaceFeed) {
        guard let cell = collectionView.visibleCells.first as? StoryCollectionViewCell else { return }
        cell.setMediaPlaybackLock(lock: true)
        cell.pause()
        
        navigateToPublicProfileViewController(viewerDisplayName: post.displayName, source: AnalyticsHelper.PublicProfileSource.post)
    }
    
    func didPlaceNameLabelTap(storyData: StoryData) {
        guard let place = storyData.placeDetails else { return }
        
        navigateToPlaceFeedViewController(place: place)
    }
}

extension StoryViewController: StoryCommentsViewDelegate {
    func didCloseStoryCommentsView() {
        animateStoryCommentsView(animate: false)
        guard let cell = collectionView.visibleCells.first as? StoryCollectionViewCell else { return }
        cell.setMediaPlaybackLock(lock: false)
        cell.resume()
    }
    
    func didDeleteCommentTap(commentToDelete: Replies) {
        self.presentAlertSheet(title: "Delete comment?", message: "This cannot be undone. Are you sure?", options: getAlertActionsForDeleteComment(), controllerStyle: .alert) { [weak self] (alertAction) in
            guard let this = self else { return }
            guard let actionTitle = alertAction.title else { return }
            switch actionTitle {
            case AppConstants.cancel:   break
            case AppConstants.delete:
                this.storyCommentsView.deleteComment(commentToDelete: commentToDelete)
            default: break
            }
        }
    }
    
    func didFailToDeleteComment(error: ErrorObject) {
        errorView.animateInViewWithError(errorObject: error, autoDismiss: true)
    }
    
    func didFailToAddComment(error: ErrorObject) {
        errorView.animateInViewWithError(errorObject: error, autoDismiss: true)
    }
    
    func didStoryRatingsFetch(storyData: StoryData, storyLatestCommentsView: StoryLatestCommentsView, storyBottomControlsView: StoryBottomControlsView) {
        if openWithStoryCommentsView {
            guard let cell = collectionView.visibleCells.first as? StoryCollectionViewCell else { return }
            guard let feedViewModel = storyData.feedViewModel else { return }
            
            cell.setMediaPlaybackLock(lock: true)
            cell.pause()
        
            if let feedId = feedItemId {
                feedViewModel.filterFeedById(feedId: feedId)
            }
            
            // NOTE: After filtering by feedItemId there will be always single post inside feed view model
            guard let post = feedViewModel.getFeedAt(index: 0) else { return }
            
            storyCommentsView.setData(storyData: storyData, post: post, storyLatestCommentsView: storyLatestCommentsView, storyBottomControlsView: storyBottomControlsView)
            
            if let archived = post.archived, !archived {
                storyCommentsView.keyboard(show: true)
            }
            
            animateStoryCommentsView(animate: true)
            
            openWithStoryCommentsView = false
        }
    }
    
    func didStoryRatingsFetchFail(error: ErrorObject) {
        errorView.animateInViewWithError(errorObject: error, autoDismiss: true)
    }
    
    func didCommentUserImageViewTap(comment: Replies) {
        pauseVisibleStoryCollectionViewCell()
        navigateToPublicProfileViewController(viewerDisplayName: comment.displayName, source: AnalyticsHelper.PublicProfileSource.comments)
    
        storyCommentsView.keyboard(show: false)
        animateStoryCommentsView(animate: false)
    }
    
    func didCommentUserInitialLabelTap(comment: Replies) {
        pauseVisibleStoryCollectionViewCell()
        navigateToPublicProfileViewController(viewerDisplayName: comment.displayName, source: AnalyticsHelper.PublicProfileSource.comments)
        
        storyCommentsView.keyboard(show: false)
        animateStoryCommentsView(animate: false)
    }
    
    func didCommentUserDisplayNameLabelTap(comment: Replies) {
        pauseVisibleStoryCollectionViewCell()
        navigateToPublicProfileViewController(viewerDisplayName: comment.displayName, source: AnalyticsHelper.PublicProfileSource.comments)
        
        storyCommentsView.keyboard(show: false)
        animateStoryCommentsView(animate: false)
    }
}

extension StoryViewController: ErrorViewDelegate {
    func errorViewTapped() {
        errorView.isHidden = true
    }
}

extension StoryViewController: PlaceFeedViewControllerDelegate {
    func didDismissPlaceFeedViewController() {
        guard let cell = collectionView.visibleCells.first as? StoryCollectionViewCell else { return }
        cell.setMediaPlaybackLock(lock: false)
        cell.reStart()
    }
}

extension StoryViewController: ProfileViewControllerDelegate {
    func removeFullScreenOverlayFromStories() {
        
    }
    
    func dismissedProfile() {
        guard let cell = collectionView.visibleCells.first as? StoryCollectionViewCell else { return }
        cell.setMediaPlaybackLock(lock: false)
        cell.reStart()
    }
}

extension StoryViewController: StoryShareViewDelegate {
    func didStoryShareViewOverlayTap() {
        animateStoryShareView(animate: false)
        
        guard let cell = collectionView.visibleCells.first as? StoryCollectionViewCell else { return }
        cell.setMediaPlaybackLock(lock: false)
        cell.resume()
    }
    
    func didStoryViewOptionSelect(storyViewOption: StoryShareOption, post: UserDataPlaceFeed, place: LivePlaces) {
        animateStoryShareView(animate: false)
        resumeVisibleStoryCollectionViewCell()
        
        switch storyViewOption.id {
        case .instagramStories: handleInstagramStoriesSelect(post: post, place: place)
        case .twitter, .more, .copyLink, .messages: generateFIRLinkAndShareTo(post: post, place: place, storyShareOption: storyViewOption)
        }
    }
}

extension StoryViewController: MessageUIServiceDelegate {
    func didFinishWith() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            self.reStartVisibleStoryCollectionViewCell()
        }
    }
}

