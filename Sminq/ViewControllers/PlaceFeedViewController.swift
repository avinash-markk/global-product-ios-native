//
//  PlaceFeedViewController.swift
//  Sminq
//
//  Created by Pushkar Deshmukh on 04/03/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import UIKit
import MarkkCamera
import SVProgressHUD

protocol PlaceFeedViewControllerDelegate: NSObjectProtocol {
    func didDismissPlaceFeedViewController()
}

class PlaceFeedViewController: UIViewController {
    public weak var delegate: PlaceFeedViewControllerDelegate?
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var addLiveRatingButton: UIButton!
    let errorView = ErrorView()
    
    var placeStoryViewModel = PlaceStoryViewModel.init(screen: AppConstants.screenNames.placeDetails)!
    let LocationMgr = UserLocationManager.SharedManager
    var place: LivePlaces?
    var moodExplainerView = PlaceMoodModalView()
    var refreshControl = UIRefreshControl()
    
    private(set) var loadingStories = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.initObservers()
        self.errorViewSetup()
        self.initDelegates()
        self.setupUI()
        self.getPlaceStories(showSkeleton: true)
        self.triggerAnalyticsEvents()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        Helper().setVisibleScreen(screen: AppConstants.screenNames.placeDetails)
    }
    
    override public var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewSafeAreaInsetsDidChange() {
        self.errorView.setStackViewTopConstraint(top: self.getSafeAreaInsets().top)
    }
    
    // MARK: Private functions
    
    private func initObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(uploadDoneNotification), name: NSNotification.Name(AppConstants.AppNotificationNames.uploadDone), object: nil)
    }
    
    private func triggerAnalyticsEvents() {
        if let place = self.place {
            AnalyticsHelper.visitedPlaceDetails(place: place)
        }
    }
    
    private func setupUI() {
        self.view.backgroundColor = AppConstants.SminqColors.blackDark100
        
        self.addLiveRatingButton.clipsToBounds = true
        self.addLiveRatingButton.layer.cornerRadius = self.addLiveRatingButton.height / 2
        self.addLiveRatingButton.backgroundColor = AppConstants.SminqColors.white100
        self.addLiveRatingButton.setTitleColor(AppConstants.SminqColors.blackDark100, for: .normal)
        self.addLiveRatingButton.titleLabel?.font = UIFont(name: AppConstants.Font.primaryBold, size: 14)
        self.addLiveRatingButton.setImage(UIImage(named: "iconAddRegular24BlackDark100")?.withRenderingMode(.alwaysOriginal), for: .normal)
        self.addLiveRatingButton.addTarget(self, action: #selector(addLiveRating), for: .touchUpInside)
        self.addLiveRatingButton.borderColor = AppConstants.SminqColors.white100.withAlphaComponent(0.1)
        self.addLiveRatingButton.borderWidth = 1
        
        self.tableView.backgroundColor = AppConstants.SminqColors.blackDark100
        
        self.tableView.register(UINib(nibName: "PlaceHeaderTableViewCell", bundle: nil), forCellReuseIdentifier: "PlaceHeaderTableViewCell")
        self.tableView.register(UINib(nibName: "PlaceFeedStoriesTableViewCell", bundle: nil), forCellReuseIdentifier: "PlaceFeedStoriesTableViewCell")
        self.tableView.register(UINib(nibName: "PlaceAddressAndPhoneTableViewCell", bundle: nil), forCellReuseIdentifier: "PlaceAddressAndPhoneTableViewCell")
        self.tableView.register(UINib(nibName: "PlaceHoursTableViewCell", bundle: nil), forCellReuseIdentifier: "PlaceHoursTableViewCell")
        self.tableView.register(UINib(nibName: "PowerByGoogleTableViewCell", bundle: nil), forCellReuseIdentifier: "PowerByGoogleTableViewCell")
        self.tableView.register(UINib(nibName: "EmptyTableViewCell", bundle: nil), forCellReuseIdentifier: "EmptyTableViewCell")
        self.tableView.register(UINib(nibName: "EmptyStoryStateTableViewCell", bundle: nil), forCellReuseIdentifier: "EmptyStoryStateTableViewCell")
        self.tableView.estimatedRowHeight = 90
        self.tableView.addSubview(refreshControl)
        self.refreshControl.addTarget(self, action: #selector(refresh(_:)), for: .valueChanged)
        self.tableView.reloadData()
        
        self.navigationController?.navigationBar.isHidden = true
        
        if let tempPlaceMoodModalView: PlaceMoodModalView = Bundle.main.loadNibNamed("PlaceMoodModalView", owner: self, options: nil)?.first as? PlaceMoodModalView {
            self.moodExplainerView = tempPlaceMoodModalView
            self.moodExplainerView.frame = view.safeRect
            self.moodExplainerView.isHidden = true
            
            if let moodScore = self.place?.placeMoodScore {
                self.moodExplainerView.enableMoodIcon(placeMood: moodScore)
            } else {
                self.moodExplainerView.enableMoodIcon(placeMood: 3)
            }
            
            self.view.addSubview(self.moodExplainerView)
        }
        
    }
    
    private func initDelegates() {
        self.tableView.delegate = self
        self.tableView.dataSource = self
    }
    
    private func getPlaceStories(showSkeleton: Bool) {
        let location = LocationMgr.getCachedLocationOtherwiseFallBackLocation()
        
        guard let place = self.place else { return }
        
        if showSkeleton {
            self.loadingStories = true
        }
        
        UIView.performWithoutAnimation {
            self.tableView.beginUpdates()
            self.tableView.reloadSections([0, 1, 2, 3, 4, 5, 6], with: .automatic)
            self.tableView.endUpdates()
        }
        
        placeStoryViewModel.getPlaceStories(placeId: place._id, latitude: location.latitude, longitude: location.longitude, userId: Helper().getUserId()) { (placesStoriesData, apiResponseState, networkError, error) in
            
            if error != nil {
                self.errorView.animateInViewWithMessage(error?.errorTitle ?? AppConstants.ErrorMessages.opsSomethingWentWrong, error?.errorDescription ?? "")
            } else {
                if let placeStories = placesStoriesData?.first, let place = placeStories.placeDetails {
                    self.place = place
                }
            }
            
            self.loadingStories = false
            
            UIView.performWithoutAnimation {
                self.tableView.beginUpdates()
                self.tableView.reloadSections([0, 1, 2, 3, 4, 5, 6], with: .automatic)
                self.tableView.endUpdates()
            }
        }
    }
    
    private func navigateToStoriesViewController(placeStories: PlacesStoriesData, storyIndex: Int) {
        guard let stories = placeStories.stories, stories.count > 0, stories[storyIndex].posts.count > 0 else { return }
        guard let place = place else { return }
        let userStory = stories[storyIndex]
        
        let storyViewManager = StoryViewManager.init(screen: AppConstants.screenNames.placeFeed)
        let storyViewController = StoryViewController.init(storyViewManager: storyViewManager)
        
        let feedViewModel = FeedViewModel.init(screen: AppConstants.screenNames.storyView)
        feedViewModel?.setFeedList(feedList: userStory.posts)
        
        let storyData = StoryData.init(userId: nil, placeId: place._id, placeDetails: place, archived: false, posts: userStory.posts, feedViewModel: feedViewModel)
        
        let storyViewModel = StoryViewModel.init(screen: AppConstants.screenNames.storyView, storyList: [storyData])
        
        storyViewController.selectedPlace = place
        storyViewController.delegate = self
        
        if let storyViewModel = storyViewModel {
            storyViewController.storyViewModel = storyViewModel
        }
        
        let navController = UINavigationController(rootViewController: storyViewController)
        navController.modalPresentationStyle = .overFullScreen
        navController.modalPresentationCapturesStatusBarAppearance = true
        self.hideTabBar()
        
        self.present(navController, animated: true, completion: nil)
    }
    
    private func errorViewSetup() {
        // TODO: Instead of using '56' calculate two label's height dynamically
        errorView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: self.getSafeAreaInsets().top + 56)
        errorView.setStackViewTopConstraint(top: self.getSafeAreaInsets().top)
        self.view.addSubview(errorView)
        errorView.isHidden = true
        errorView.delegate = self
    }
    
    // MARK: Target functions
    
    @objc func addLiveRating() {
        if let selectedPlace = self.place {
            SVProgressHUD.show()
            let cameraViewController = RatingCameraViewController()
            let cameraPlace = CameraPlace(id: selectedPlace._id, name: selectedPlace.name, address: selectedPlace.address)
            cameraViewController.initialPlace = cameraPlace
            cameraViewController.galleryInterval = FIRRemoteConfigService.shared.getGalleryMediaAccessInterval()
            cameraViewController.screen = AppConstants.screenNames.placeDetails
            present(cameraViewController, animated: true, completion: nil)
        }
        AnalyticsHelper.addRatingIntend(screen: AppConstants.screenNames.placeFeed, section: "\(AppConstants.RatingsIntendSection.cameraSection)", addToStory: "no", place: nil)
    }
    
    @objc func uploadDoneNotification(_ notification: NSNotification) {
        guard let uploadTask = notification.userInfo?["task"] as? UploadTask else { return }
        
        if uploadTask.placeIDString == self.place?._id {
            self.getPlaceStories(showSkeleton: false)
        }
    }
    
    @objc func refresh(_ sender: AnyObject) {
        self.getPlaceStories(showSkeleton: false)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            self.refreshControl.endRefreshing()
        }
    }
}

extension PlaceFeedViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 7
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0, 1, 2, 3, 4, 5, 6:
            return 1
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "PlaceHeaderTableViewCell", for: indexPath) as? PlaceHeaderTableViewCell else {
                return UITableViewCell()
            }
            
            cell.delegate = self
            
            if let place = self.place {
                cell.setPlace(place: place)
                cell.updateData()
            }
            cell.placeStoryViewModel = self.placeStoryViewModel
            
            return cell
        } else if indexPath.section == 1 {
            let stories = self.placeStoryViewModel.getPlaceAt(index: 0)?.stories ?? []
            
            if self.loadingStories || stories.count > 0 {
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "PlaceFeedStoriesTableViewCell", for: indexPath) as? PlaceFeedStoriesTableViewCell else {
                    return UITableViewCell()
                }
                
                cell.delegate = self
                cell.placeStoryViewModel = self.placeStoryViewModel
                cell.loadingState(state: self.loadingStories)
                return cell
            } else {
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "EmptyStoryStateTableViewCell", for: indexPath) as? EmptyStoryStateTableViewCell else { return UITableViewCell() }
                cell.delegate = self
                cell.imageVisibility(hide: false)
                cell.headingLabelVisibility(hide: true)
                cell.subHeadingLabelVisibility(hide: false)
                cell.subHeadingLabel.text = "Be the first to add a live rating \nto this place and be the Discoverer!"
                return cell
            }
        
        } else if indexPath.section == 2 {
            if let place = self.place, place.address != "" {
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "PlaceAddressAndPhoneTableViewCell", for: indexPath) as? PlaceAddressAndPhoneTableViewCell else {
                    return UITableViewCell()
                }
                
                cell.delegate = self
                cell.setCellType(type: .address, place: place)
                
                return cell
            }
        } else if indexPath.section == 3 {
            if let place = self.place, let internationalPhoneNumber = place.internationalPhoneNumber, internationalPhoneNumber != "" {
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "PlaceAddressAndPhoneTableViewCell", for: indexPath) as? PlaceAddressAndPhoneTableViewCell else {
                    return UITableViewCell()
                }
                
                cell.delegate = self
                cell.setCellType(type: .phone, place: place)
                
                return cell
            }
        }
        else if indexPath.section == 4 {
            if let place = self.place, let openingHours = place.openingHours, let hours = openingHours.weekdayText, hours.count > 0 {
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "PlaceHoursTableViewCell", for: indexPath) as? PlaceHoursTableViewCell else { return UITableViewCell() }
                
                cell.hours = hours
                return cell
            }
            
        } else if indexPath.section == 5 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "PowerByGoogleTableViewCell", for: indexPath) as? PowerByGoogleTableViewCell else { return UITableViewCell() }
            return cell
        } else if indexPath.section == 6 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "EmptyTableViewCell", for: indexPath) as? EmptyTableViewCell else { return UITableViewCell() }
            cell.backgroundColor = AppConstants.SminqColors.blackLight100
            return cell
        }
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "EmptyTableViewCell", for: indexPath) as? EmptyTableViewCell else { return UITableViewCell() }
        cell.backgroundColor = AppConstants.SminqColors.blackLight100
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 1 {
            let stories = self.placeStoryViewModel.getPlaceAt(index: 0)?.stories ?? []
            
            if self.loadingStories || stories.count > 0 {
                var height: CGFloat = 284 + 40
                
                if placeStoryViewModel.isArchived {
                    height = height + 40
                }
                return height
            }
            return 298
        } else if indexPath.section == 2 {
            guard let place = self.place, place.address != "" else { return 0 }
            
        } else if indexPath.section == 3 {
            guard let place = self.place, let internationalPhoneNumber = place.internationalPhoneNumber, internationalPhoneNumber != "" else { return 0 }
        } else if indexPath.section == 4 {
            guard let place = self.place, let openingHours = place.openingHours, let hours = openingHours.weekdayText, hours.count > 0 else { return 0 }
        } else if indexPath.section == 6 {
            // Empty height so that 'RATING' CTA is shown
            return 100
        }
        
        return UITableViewAutomaticDimension
        
    }
}

extension PlaceFeedViewController: PlaceHeaderTableViewCellDelegate {
    func backButtonTapped() {
        if let place = self.place {
            AnalyticsHelper.closePlaceEvent(place: place)
        }
        
        self.navigationController?.dismiss(animated: true, completion: nil)
        self.delegate?.didDismissPlaceFeedViewController()
    }
    
    func placeMoodIconTapped() {
        self.moodExplainerView.isHidden = false
        
        if let moodScore = self.place?.placeMoodScore {
            self.moodExplainerView.enableMoodIcon(placeMood: moodScore)
        } else {
            self.moodExplainerView.enableMoodIcon(placeMood: 3)
        }
    }
}

extension PlaceFeedViewController: PlaceAddressAndPhoneTableViewCellDelegate {
    func directionsButtonDidSelect(place: LivePlaces?) {
        let actionSheetController: UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let cancelActionButton = UIAlertAction(title: "Cancel", style: .cancel) { _ in
            //print("Cancel")
        }
        actionSheetController.addAction(cancelActionButton)
        
        let appleMapsButton = UIAlertAction(title: "Apple Maps", style: .default) { _ in
            LogUtil.info("Apple Maps Selected")
            if let placeLatitude = self.place?.geoPoint.coordinates[1], let placeLongitude = self.place?.geoPoint.coordinates[0] {
                if let userLocation = UserDefaults.standard.value(forKey: "UserLocation") {
                    let userLatitude = (userLocation as AnyObject).value(forKey: "latitude") as? Double ?? 0
                    let userLongitude = (userLocation as AnyObject).value(forKey: "longitude") as? Double ?? 0
                    
                    UIApplication.shared.open(NSURL(string:
                        "http://maps.apple.com/?saddr\((userLatitude)),\((userLongitude))&daddr=\(placeLatitude),\(placeLongitude)&directionsmode=driving")! as URL)
                    
                }
            }
        }
        
        actionSheetController.addAction(appleMapsButton)
        
        let googleMapsButton = UIAlertAction(title: "Google Maps", style: .default) { _ in
            LogUtil.info("Google Maps Selected")
            
            if (UIApplication.shared.canOpenURL(URL(string: "comgooglemaps://")!)) {
                if let placeLatitude = self.place?.geoPoint.coordinates[1], let placeLongitude = self.place?.geoPoint.coordinates[0] {
                    if let userLocation = UserDefaults.standard.value(forKey: "UserLocation") {
                        let userLatitude = (userLocation as AnyObject).value(forKey: "latitude") as? Double ?? 0
                        let userLongitude = (userLocation as AnyObject).value(forKey: "longitude") as? Double ?? 0
                        
                        UIApplication.shared.open(NSURL(string:
                            "comgooglemaps://?saddr=\((userLatitude)),\((userLongitude))&daddr=\(placeLatitude),\(placeLongitude)&directionsmode=driving")! as URL)
                    }
                }
            } else {
                if let placeLatitude = self.place?.geoPoint.coordinates[1], let placeLongitude = self.place?.geoPoint.coordinates[0] {
                    if let userLocation = UserDefaults.standard.value(forKey: "UserLocation") {
                        let userLatitude = (userLocation as AnyObject).value(forKey: "latitude") as? Double ?? 0
                        let userLongitude = (userLocation as AnyObject).value(forKey: "longitude") as? Double ?? 0
                        UIApplication.shared.open(NSURL(string:
                            "https://www.google.co.in/maps/dir/?saddr\((userLatitude)),\((userLongitude))&daddr=\(placeLatitude),\(placeLongitude)&directionsmode=driving")! as URL)
                    }
                }
            }
        }
        
        actionSheetController.addAction(googleMapsButton)
        
        self.present(actionSheetController, animated: true, completion: nil)
    }
}

extension PlaceFeedViewController: PlaceFeedStoriesTableViewCellDelegate {
    func didEmptyStoryCellSelect() {
        self.addLiveRating()
    }
    
    func placeStoryDidSelect(placeStories: PlacesStoriesData?, storyIndex: Int?) {
        if let tempPlaceStories = placeStories, let index = storyIndex {
            self.navigateToStoriesViewController(placeStories: tempPlaceStories, storyIndex: index)
        }
    }
}

extension PlaceFeedViewController: ErrorViewDelegate {
    
    func errorViewTapped() {
        self.errorView.animateOutView()
        self.getPlaceStories(showSkeleton: true)
    }
    
}

extension PlaceFeedViewController: StoryViewControllerDelegate {
    
    func didDismissStoryView() {
        
    }
    
    func didUpvotePost(post: UserDataPlaceFeed) {
        placeStoryViewModel.updatePost(post: post)
    }

}

extension PlaceFeedViewController: EmptyStoryStateTableViewCellDelegate {
    
    func plusIconCellImageViewTapped() {
        self.addLiveRating()
    }
    
}
