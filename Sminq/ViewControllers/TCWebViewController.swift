//
//  TCWebViewController.swift
//  Sminq
//
//  Created by Bjorn Mascarenhas on 22/02/18.
//  Copyright © 2018 Suraj Sagare. All rights reserved.
//

import UIKit
import WebKit
import SVProgressHUD

class TCWebViewController: UIViewController {
    var strWebURL = String()
    var strTitle = String()
    var webView: WKWebView!
    
    // MARK: View controllers life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Title for Webview
        self.title = strTitle
        
        // Init and load request in webview.
        guard let url = URL(string: strWebURL) else { return }
        webView = WKWebView(frame: self.view.frame)
        webView.navigationDelegate = self
        webView.load(URLRequest(url: url))
        self.view.addSubview(webView)
        self.view.sendSubview(toBack: webView)
        
        self.view.backgroundColor = AppConstants.SminqColors.blackDark100
    }
    
    override func viewWillAppear(_ animated: Bool){
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
        setupNavigationBar()
        setNeedsStatusBarAppearanceUpdate()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.navigationBar.barTintColor = .white
    }

    override func viewDidDisappear(_ animated: Bool){
        super.viewDidDisappear(animated)
        SVProgressHUD.dismiss()
    }
    
    override public var prefersStatusBarHidden: Bool {
        return false
    }
    
    override public var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    private func setupNavigationBar() {
        // Create up arrow left navigation button
        let backButton = UIButton(frame: CGRect(x: 0, y: 0, width: 35, height: 35))
        let upArrow = UIImage(named: "iconBack24White100")?.withRenderingMode(.alwaysTemplate)
        backButton.setImage(upArrow, for: UIControlState())
        backButton.tintColor = UIColor.white
        backButton.imageEdgeInsets = UIEdgeInsetsMake(0, -25, 0, 2)
        backButton.addTarget(self, action: #selector(tappedBackButton), for: .touchUpInside)
        navigationItem.leftBarButtonItems = [.init(customView: backButton)]
        
        // Navbar title label
        let navigationTitleLabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.view.frame.width - (100 * 2), height: 44))
        navigationTitleLabel.font = UIFont(name: AppConstants.Font.primaryRegular, size: 20.0)
        navigationTitleLabel.textColor = UIColor.white
        navigationTitleLabel.textAlignment = NSTextAlignment.center
        navigationItem.titleView = navigationTitleLabel
        navigationTitleLabel.text = strTitle
        
        navigationController?.navigationBar.barTintColor = AppConstants.SminqColors.blackDark100
        navigationController?.navigationBar.isTranslucent = false
        
        // Remove the background color.
        navigationController?.navigationBar.setBackgroundImage(UIColor.clear.as1ptImage(), for: .default)
        
        // Set the shadow color.
        navigationController?.navigationBar.shadowImage = AppConstants.SminqColors.white100.withAlphaComponent(0.1).as1ptImage()
    }
    
    @objc func tappedBackButton() {
        self.navigationController?.popViewController(animated: true)
    }
    
}

// MARK: TCWebViewController UIWebViewDelegate Functions
extension  TCWebViewController: WKNavigationDelegate {
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        SVProgressHUD.show()
    }

    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        SVProgressHUD.dismiss()
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        SVProgressHUD.dismiss()
    }

    
    func webView(_ webView: WKWebView,
                 didReceive challenge: URLAuthenticationChallenge,
                 completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        if(challenge.protectionSpace.authenticationMethod == NSURLAuthenticationMethodServerTrust) {
            let cred = URLCredential(trust: challenge.protectionSpace.serverTrust!)
            completionHandler(.useCredential, cred)
        } else {
            completionHandler(.performDefaultHandling, nil)
        }
    }
}


