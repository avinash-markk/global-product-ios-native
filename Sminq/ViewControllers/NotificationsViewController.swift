//
//  NotificationsViewController.swift
//  Sminq
//
//  Created by SMINQ iOS on 02/04/18.
//  Copyright © 2018 Suraj Sagare. All rights reserved.
//

import UIKit
import CoreData
import Firebase
import CleverTapSDK

protocol NotificationsViewControllerDelegate: NSObjectProtocol {
    func didBackButtonTap()
}

class NotificationsViewController: UIViewController {
    public weak var delegate: NotificationsViewControllerDelegate?
    
    @IBOutlet weak var notificationsTableView: UITableView!
//    @IBOutlet weak var emptyStateUIView: UIView!
    
    let notificationsCountLabel = UILabel()
    var notifications: [NSManagedObject] = []
    var showCameraButton: Bool = false
    var clearButton = UIButton()
    var emptyStateUIView = EmptyState()
    
//    var notifications  = ["e Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have", "t is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here,"]
    
    required init(coder aDecoder: NSCoder) {
        // Tabbar Item SetUp
        super.init(coder: aDecoder)!
        let customTabBarItem:UITabBarItem = UITabBarItem(title: nil, image: UIImage(named: "NotificationsTab")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal), selectedImage: UIImage(named: "NotificationsTabSelected")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal))
        self.tabBarItem = customTabBarItem
        self.title = "Notifications"
    }

    override public var prefersStatusBarHidden: Bool {
        return false
    }

    override public var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        NotificationCenter.default.addObserver(self, selector: #selector(refreshNotifications), name: Notification.Name(AppConstants.AppNotificationNames.refreshNotificationList), object: nil)
        
        self.initUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        showCameraButton = false
        
        self.navigationController?.tabBarController?.delegate = self
        self.navigationController?.isNavigationBarHidden = false
       
        reloadData()
        self.tabBarItem.badgeValue = nil
        UserDefaults.standard.set(0, forKey: "notificationUnreadCount")
        
        setupNavigationBar()
        
        setNeedsStatusBarAppearanceUpdate()
        showTabBar()
    }
    
    override func viewWillDisappear(_ animated: Bool){
        super.viewWillDisappear(animated)
//        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidDisappear(_ animated: Bool){
        super.viewDidDisappear(animated)

        AppConstants.previousScreen = AppConstants.screenNames.notifications
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.isHidden = false
    }
    
    // MARK: Private functions
    private func initUI() {
        self.view.backgroundColor = AppConstants.SminqColors.blackDark100
        
        self.notificationsTableView.backgroundColor = AppConstants.SminqColors.blackDark100
        self.notificationsTableView.tableFooterView = UIView()
        
        self.emptyStateViewSetup()
    }
    
    private func setupNavigationBar() {
        // Back Button
        let backButton = UIButton(frame: CGRect(x: 0, y: 0, width: 35, height: 35))
        let upArrow = UIImage(named: "iconBack24White100")?.withRenderingMode(.alwaysTemplate)
        backButton.setImage(upArrow, for: UIControlState())
        backButton.tintColor = UIColor.white
        backButton.imageEdgeInsets = UIEdgeInsetsMake(0, -25, 0, 2)
        backButton.addTarget(self, action: #selector(tappedBackButton), for: .touchUpInside)
        navigationItem.leftBarButtonItems = [.init(customView: backButton)]
        
        clearButton = UIButton(frame: CGRect(x: 100, y: 100, width: 65, height: 24))
        clearButton.setTitle("CLEAR", for: .normal)
        clearButton.clipsToBounds = true
        clearButton.layer.cornerRadius = 12
        clearButton.backgroundColor = AppConstants.SminqColors.white100.withAlphaComponent(0.25)
        clearButton.titleLabel?.font = UIFont(name: AppConstants.Font.primaryBold, size: 12)
        clearButton.setTitleColor(AppConstants.SminqColors.white100.withAlphaComponent(0.75), for: .normal)
        clearButton.addTarget(self, action: #selector(clearNotifications), for: .touchUpInside)
        clearButton.layer.borderWidth = 1
        clearButton.layer.borderColor = AppConstants.SminqColors.white100.withAlphaComponent(0.1).cgColor
        navigationItem.rightBarButtonItems = [.init(customView: clearButton)]
        
        // Navbar title label
        let navTitleView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width-105, height: 44))
        
        let commentLabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.view.frame.width-105, height: 44))
        commentLabel.font = UIFont(name: AppConstants.Font.primaryRegular, size: 20.0)
        commentLabel.textColor = UIColor.white
        commentLabel.textAlignment = NSTextAlignment.center
        commentLabel.text = "Notifications"
        navTitleView.addSubview(commentLabel)
        
        navigationItem.titleView = navTitleView
        
        navigationController?.navigationBar.barTintColor = AppConstants.SminqColors.blackLight100
        navigationController?.navigationBar.isTranslucent = false
        
        // Remove the background color.
        navigationController?.navigationBar.setBackgroundImage(UIColor.clear.as1ptImage(), for: .default)
        
        // Set the shadow color.
        navigationController?.navigationBar.shadowImage = AppConstants.SminqColors.white100.withAlphaComponent(0.1).as1ptImage()
    }
    
    @objc func tappedBackButton() {
        self.navigationController?.popViewController(animated: true)
        
        self.delegate?.didBackButtonTap()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func refreshNotifications() {
        reloadData()
    }
    
    func emptyStateViewSetup() {
        self.emptyStateUIView.isHidden = false
        self.emptyStateUIView.frame = self.view.bounds
        self.emptyStateUIView.autoresizingMask = [.flexibleWidth, .flexibleHeight] // So that subview matches parent's height width
        self.emptyStateUIView.headingLabel.text = "All caught up!"
        self.emptyStateUIView.headingLabel.textColor = AppConstants.SminqColors.white100
        self.emptyStateUIView.headingLabel.font = UIFont(name: AppConstants.Font.primaryRegular, size: 24)
        self.emptyStateUIView.subHeadingLabel.text = "There are no new notifications for you right now"
        self.emptyStateUIView.subHeadingLabel.textColor = AppConstants.SminqColors.white100
        self.emptyStateUIView.subHeadingLabel.font = UIFont(name: AppConstants.Font.primaryRegular, size: 16)
        self.emptyStateUIView.setContentViewBackgroundColor(color: AppConstants.SminqColors.blackDark100)
        
        self.view.insertSubview(self.emptyStateUIView, belowSubview: self.notificationsTableView)
    }
    
    func showNotifications() {
        if notifications.count > 1 {
            notificationsCountLabel.text = "\(notifications.count) Notifications"
        } else {
            notificationsCountLabel.text = "\(notifications.count) Notification"
        }
        self.notificationsTableView.isHidden = false
        self.emptyStateUIView.isHidden = true
        self.clearButton.isHidden = false
    }
    
    func hideNotifications() {
        self.notificationsTableView.isHidden = true
        self.emptyStateUIView.isHidden = false
        self.clearButton.isHidden = true
    }
    
    func reloadData() {
        NotificationCacheService.shared.getAllCachedNotifications { (cachedNotifications) in
            self.notifications = cachedNotifications
            self.notifications = self.notifications.reversed()
            
            if self.notifications.count == 0 {
                self.hideNotifications()
            } else {
                self.showNotifications()
                AnalyticsHelper.visitedNotificationsEvent(screen: AppConstants.screenNames.notifications)
            }
            
            self.notificationsTableView.reloadData()
        }
    }
    
    @objc func clearNotifications() {
        AnalyticsHelper.notificationClearClickEvent()
        
        NotificationCacheService.shared.clearCachedNotifications {
            self.reloadData()
        }
    }
}

extension NotificationsViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notifications.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let notification = notifications[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationsCell", for: indexPath) as! NotificationsCell
        
        cell.bodyLabel.text = notification.value(forKeyPath: "body") as? String
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy hh:mm:ss a"
        let date = dateFormatter.date(from: (notification.value(forKeyPath: "time") as? String)!)
        
        let title = notification.value(forKeyPath: "title") as! String
        
        if date != nil {
            let dateString = PushNotificationAPI.timeAgoSinceDate(date!)
            cell.titleLabel.text = "\(title) • \(dateString)"
        }
        
        let mediaType = notification.value(forKeyPath: "mediaType") as? String
        
        cell.feedImageView.isHidden = true
        
        if let tempMediaType = mediaType, tempMediaType != "" {
            if let image = notification.value(forKeyPath: "image") as? String {
                if tempMediaType == AppConstants.image {
                    // image found
                    if let url = CloudinaryService.shared.generateCloudinaryImageURL(folder: FIRRemoteConfigService.shared.getCloudinaryOriginalImageFolder(), fileName: image) {
                        cell.feedImageView.sd_setImage(with: url, placeholderImage: nil)
                        cell.feedImageView.isHidden = false
                    }
                } else {
                    
                    // video found
                    if let url = CloudinaryService.shared.generateCloudinaryVideoURL(folder: FIRRemoteConfigService.shared.getCloudinaryVideoFolder(), fileName: image) {
                        cell.feedImageView.sd_setImage(with: url, placeholderImage: nil)
                        cell.feedImageView.isHidden = false
                    }
                }
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let notification = notifications[indexPath.row]
        let notificationType = notification.value(forKeyPath: "type") as? String
        //        let feedItemId = notification.value(forKeyPath: "feedItemId") as? String
        
        if notificationType?.lowercased() == AppConstants.newReply || notificationType?.lowercased() == AppConstants.newVote {
            let feedItemId = notification.value(forKeyPath: "feedItemId") as? String
            let feedItemUserId = notification.value(forKeyPath: "feedItemUserId") as? String
            let placeString = notification.value(forKeyPath: "place") as? String
            guard let place = Helper().stringToPlaceConversion(place: placeString!) else { return }
            
            let storyViewManager = StoryViewManager.init(screen: AppConstants.screenNames.notifications, feedItemId: feedItemId)
            let storyViewController = StoryViewController.init(storyViewManager: storyViewManager)
            
            storyViewController.delegate = self
            
            var tempStoryList: [StoryData] = []
            let storyData = StoryData.init(userId: feedItemUserId, placeId: nil, placeDetails: place, archived: false, posts: [], feedViewModel: nil)
            tempStoryList.append(storyData)
            
            let storyViewModel = StoryViewModel.init(screen: AppConstants.screenNames.storyView, storyList: tempStoryList)!
            
            storyViewController.storyViewModel = storyViewModel
            storyViewController.openWithStoryCommentsView = notificationType?.lowercased() == AppConstants.newReply
            
            let navController = UINavigationController(rootViewController: storyViewController)
            navController.modalPresentationStyle = .overFullScreen
            navController.modalPresentationCapturesStatusBarAppearance = true
            self.hideTabBar()
            self.present(navController, animated: true, completion: nil)
        } else if notificationType?.lowercased() == AppConstants.newFeed {
            showCameraButton = false
            
            let placeString = notification.value(forKeyPath: "place") as? String
            guard let place = Helper().stringToPlaceConversion(place: placeString!) else { return }
            
            let placeFeedViewController = AppConstants.storyboard.instantiateViewController(withIdentifier: "PlaceFeedViewController") as! PlaceFeedViewController
            placeFeedViewController.place = place
            
            let navController = UINavigationController(rootViewController: placeFeedViewController)
            self.present(navController, animated: true, completion: nil)
        } else if notificationType?.lowercased() == AppConstants.newBadge {
            let viewController = AppConstants.storyboard.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
            viewController.currentTabIndex = 1
            viewController.hidesBottomBarWhenPushed = true
            hideTabBar()
            self.navigationController?.pushViewController(viewController, animated: true)
        }
        
    }
}

extension NotificationsViewController : UITabBarControllerDelegate {
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        
        let tabBarIndex = tabBarController.selectedIndex
        
        if tabBarIndex == 2 {
            if notifications.count != Int(0){
            let indexPath = IndexPath(row: 0, section: 0)
            self.notificationsTableView.scrollToRow(at: indexPath, at: .top, animated: true)
            }
        }
    }
}

extension NotificationsViewController: StoryViewControllerDelegate {
    func didDismissStoryView() {
        self.showTabBar()
    }
    
    func didUpvotePost(post: UserDataPlaceFeed) {
        
    }
}

