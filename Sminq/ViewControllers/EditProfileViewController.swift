//
//  EditProfileViewController.swift
//  Sminq
//
//  Created by Pushkar Deshmukh on 30/04/18.
//  Copyright © 2018 Suraj Sagare. All rights reserved.
//

import UIKit
import GooglePlaces
import SVProgressHUD
import Gloss
import Cloudinary
import Firebase

class EditProfileViewController: UIViewController, UIGestureRecognizerDelegate {
    
    @IBOutlet weak var userEditTableView: UITableView!
    
    var imagePicker = UIImagePickerController()
    let errorView = ErrorView()
    
    var name: String?
    var displayName: String?
    var email: String?
    var mobile: String?
    var city: String?
    var country: String?
    var countryCode: String? // Ex. +91
    //    var countryShortName: String?
    var birthDate: String?
    var gender: String?
    var mobileVerified: Bool = false
    var emailVerified: Bool = false
    var bio: String?
    var imageUrl: String?
    
    var latitude: CLLocationDegrees?
    var longitude: CLLocationDegrees?
    
    let datePicker = UIDatePicker()
    let genderPickerView = UIPickerView()
    
    var textField = UITextField()
    let genderDropdown = ["Male", "Female", "Prefer Not To Answer"]
    var bioTextLimit: Int = 200
    var bioCellHeight: CGFloat = 48
    
    var INPUT_VIEW_NAME_KEY = "name"
    var INPUT_VIEW_USERNAME_KEY = "username"
    var INPUT_VIEW_BIO_KEY = "bio"
    var INPUT_VIEW_EMAIL_KEY = "email"
    var INPUT_VIEW_MOBILE_KEY = "mobile"
    var INPUT_VIEW_CITY_KEY = "city"
    var INPIT_VIEW_DOB_KEY = "dob"
    var INPIT_VIEW_GENDER_KEY = "gender"
    
    var inputViewTags: [String: Int] = [:]
    
    var showCameraButton:Bool = false
    var selectedProfilePicture: Bool = false // Use this flag for cloudinary image upload
    var selectedUserImage: UIImage?
    var profileUpdateTrace: Trace?
    
    var userData: UserProfileData?
    
    required init(coder aDecoder: NSCoder) {
        // Tabbar Item SetUp
        super.init(coder: aDecoder)!
        
        inputViewTags = [INPUT_VIEW_NAME_KEY: 1, INPUT_VIEW_USERNAME_KEY: 2, INPUT_VIEW_BIO_KEY: 3, INPUT_VIEW_EMAIL_KEY: 4, INPUT_VIEW_MOBILE_KEY: 5, INPUT_VIEW_CITY_KEY: 6, INPIT_VIEW_DOB_KEY: 7, INPIT_VIEW_GENDER_KEY: 8]
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        genderPickerView.delegate = self
        // Do any additional setup after loading the view.
        
        self.userEditTableView.backgroundColor = AppConstants.SminqColors.blackDark100
        self.userEditTableView.register(UINib(nibName: "EmptyTableViewCell", bundle: nil), forCellReuseIdentifier: "EmptyTableViewCell")
        self.userEditTableView.register(UINib(nibName: "UserEditProfileBioCell", bundle: nil), forCellReuseIdentifier: "UserEditProfileBioCell")
//        self.userEditTableView.rowHeight = UITableViewAutomaticDimension
//        self.userEditTableView.estimatedRowHeight = 48
        NotificationCenter.default.addObserver(self, selector: #selector(countrySelected), name: NSNotification.Name("countrySelected"), object: nil)
        self.errorViewSetup()
        self.initUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        showCameraButton = false
        setNeedsStatusBarAppearanceUpdate()
        setupNavigationBar()
    }
    
    func errorViewSetup() {
        // TODO: Instead of using '56' calculate two label's height dynamically
        errorView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 8 + 56)
        errorView.setStackViewTopConstraint(top: 8)
        self.view.addSubview(errorView)
        errorView.isHidden = true
        errorView.delegate = self
    }
    
    func initUI() {
        let profileDataClass = ProfileDataClass()
        if let profileData = profileDataClass.getProfileCachedData() {
            userData = profileData
            
            self.name = userData?.user.name
            self.displayName = userData?.user.displayName.first
            self.email = userData?.user.email
            self.mobile = userData?.user.mobile
            
            if userData?.user.userCity != nil && userData?.user.userCity.name != nil {
                self.city = userData?.user.userCity.name
            }
            
            if userData?.user.userCountry != nil && userData?.user.userCountry.name != nil {
                self.country = userData?.user.userCountry.name
            }
            
            self.birthDate = userData?.user.birthDate
            self.gender = userData?.user.gender
            self.imageUrl = userData?.user.imageUrl
            
            self.bio = userData?.user.bio
            self.mobileVerified = userData?.user.mobileVerified ?? false
            self.emailVerified = userData?.user.emailVerified ?? false
            
            self.getCountryCode(code: userData?.user.countryCode ?? "")
        }

    }
    
    func getCountryCode(code: String) {
        if self.countryCode == nil || self.countryCode == "" {
            if code != "" {
                self.countryCode = Helper().prependPlusToCountryCode(countryCode: code)
            } else {
                if let regionCode = NSLocale.current.regionCode {
                    if let tempCountryCodeObject: CountryCode = getCountryForCountryCode(query: regionCode, type: "code") {
                        self.countryCode = Helper().prependPlusToCountryCode(countryCode: tempCountryCodeObject.dial_code)
                    }
                }
            }
        }
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if !self.selectedProfilePicture {
            if let imageUrl = self.imageUrl
            {
                setProfilePicFromUrl(url: imageUrl)
            }
        }
    }
    
    override public var prefersStatusBarHidden: Bool {
        return false
    }
    
    override public var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: Private functions
    
    func setProfilePicFromUrl(url: String) {
        let indexPath = IndexPath(row: 0, section: 0)
        self.userEditTableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
            if let imageCell = self.userEditTableView.cellForRow(at: IndexPath(row: 0, section: 0)) as? EditProfileImageViewTableViewCell {
            imageCell.userImageView.sd_setImage(with: URL(string: url), placeholderImage: nil)
            imageCell.userImageView.isHidden = false
            imageCell.addUserImageView.image = UIImage(named: "iconAddPhoto80White100")
            }
        })
    }
    
    func setProfilePicFromImageResource(image: UIImage) {
        let indexPath = IndexPath(row: 0, section: 0)
        self.userEditTableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
        
        // Set global variables
        self.selectedProfilePicture = true
        self.selectedUserImage = image
        
        if let imageCell = self.userEditTableView.cellForRow(at: IndexPath(row: 0, section: 0)) as? EditProfileImageViewTableViewCell {
            imageCell.userImageView.image = image
            imageCell.userImageView.isHidden = false
            imageCell.addUserImageView.image = UIImage(named: "iconAddPhoto80White100")
        }
    }
    
    //MARK: - Open the camera
    func openCamera() {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)) {
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            //If you dont want to edit the photo then you can set allowsEditing to false
            imagePicker.allowsEditing = true
            imagePicker.delegate = self
            self.present(imagePicker, animated: true, completion: nil)
        } else {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    //MARK: - Choose image from camera roll
    func openGallary() {
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        //If you dont want to edit the photo then you can set allowsEditing to false
        imagePicker.allowsEditing = true
        imagePicker.delegate = self
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    func getCountryForCountryCode(query: String, type: String) -> CountryCode? {
        var tempCountryCodeObject: CountryCode?
        
        if let path = Bundle.main.path(forResource: "countries", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
                
                let countryCodeList: [CountryCode] = [CountryCode].from(jsonArray: jsonResult as! [JSON])!
                
                for countryCodeObject in countryCodeList {
                    if type == "code" {
                        if countryCodeObject.code == query {
                            tempCountryCodeObject = countryCodeObject
                        }
                    } else if type == "dial_code" {
                        if countryCodeObject.dial_code == query {
                            tempCountryCodeObject = countryCodeObject
                        }
                    }
                    
                }
            } catch {
                LogUtil.error(error)
            }
        }
        
        return tempCountryCodeObject
    }
    
    private func setupNavigationBar() {
        // Back Button
        let backButton = UIButton(frame: CGRect(x: 0, y: 0, width: 35, height: 35))
        let upArrow = UIImage(named: "iconBack24White100")?.withRenderingMode(.alwaysTemplate)
        backButton.setImage(upArrow, for: UIControlState())
        backButton.tintColor = UIColor.white
        backButton.imageEdgeInsets = UIEdgeInsetsMake(0, -25, 0, 2)
        backButton.addTarget(self, action: #selector(tappedBackButton), for: .touchUpInside)
        navigationItem.leftBarButtonItems = [.init(customView: backButton)]
        
        // Navbar title label
        let navigationTitleLabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.view.frame.width - (100 * 2), height: 44))
        navigationTitleLabel.font = UIFont(name: AppConstants.Font.primaryRegular, size: 20.0)
        navigationTitleLabel.textColor = UIColor.white
        navigationTitleLabel.textAlignment = NSTextAlignment.center
        navigationItem.titleView = navigationTitleLabel
        navigationTitleLabel.text = "Edit Profile"
        
        navigationController?.navigationBar.barTintColor = AppConstants.SminqColors.blackLight100
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.isNavigationBarHidden = false
        
        // Remove the background color.
        navigationController?.navigationBar.setBackgroundImage(UIColor.clear.as1ptImage(), for: .default)
        
        // Set the shadow color.
        navigationController?.navigationBar.shadowImage = AppConstants.SminqColors.white100.withAlphaComponent(0.1).as1ptImage()
    }
    
    func updateUser(userId: String, name: String, displayName: String, email: String, mobile: String, latitude: CLLocationDegrees, longitude: CLLocationDegrees, countryName: String, cityName: String, countryCode: String, gender: String, birthDate: String, isSignUp: Int, isExistingUser: Bool, bio: String, image: String) {
        
       // SVProgressHUD.show()
        
        guard let metric = HTTPMetric(url: URL(string: UserService().editUserAPIEndPoint(userId: userId))!, httpMethod: .put) else { return }
        metric.start()
        
        let updateUserAPI = UserService().editUser(userId: userId, name: name, displayName: displayName, email: email, mobile: mobile, latitude: latitude, longitude: longitude, countryName: countryName, cityName: cityName, countryCode: countryCode, gender: gender, birthDate: birthDate, isSignUp: 0, isExistingUser: isExistingUser, bio: bio, image: image)
        
        updateUserAPI.response.validate(statusCode: 200..<300).responseJSON { response in
//            debugPrint(response)
            
            switch response.result {
            case .success:
                guard let json = response.result.value as? JSON else { return }
                if let editResponse: UserDataDict = UserDataDict(json: json) {
                    
                    let profileDataClass = ProfileDataClass()
                    
                    if let userCachedData = profileDataClass.getProfileCachedData() {
                        // Only user is updated. Hence update user object from cache. And preserve old userMetrics and stories cache
                        profileDataClass.saveProfileData(userProfileData: UserProfileData.init(user: editResponse, userMetrics: userCachedData.userMetrics, stories: userCachedData.stories, levelsList: userCachedData.levelsList, streaks: userCachedData.streaks, highlight: userCachedData.highlight, claims: userCachedData.claims))
                    }
                    
                    self.navigationController?.checkAndDisplayBadgeForIncompleteProfile(data: editResponse)

                    AnalyticsHelper.pushUserProfileToCleverTap(user: editResponse)
                    
                    self.navigationController?.popViewController(animated: true)
                } else {
                    LogUtil.error(response)
                    self.errorView.animateInViewWithMessage(AppConstants.ErrorMessages.opsSomethingWentWrong, AppConstants.ErrorMessages.failedConnectingServer)
                    self.view.endEditing(true)
                    
                    AnalyticsHelper.APIFailureEvent(screen: AppConstants.screenNames.editProfile, apiName: AppConstants.apiNames.updateUserProfile, errorMessage: AppConstants.ErrorMessages.emptyResponse, userId: Helper().getUserId(), errorCode: nil, requestPayload: updateUserAPI.requestPayload, requestMethod: updateUserAPI.requestMethod.rawValue, requestPathParams: updateUserAPI.requestPathParams)
                    
                }
                
                SVProgressHUD.dismiss()
                
                self.profileUpdateTrace?.incrementMetric("\(AppConstants.apiNames.updateUserProfile) API Success", by: 1)
                self.profileUpdateTrace?.stop()
                
                break
                
            case .failure(let error):
                LogUtil.error(error)
                
                let error = error as NSError
                
                if let statusCode = response.response?.statusCode {
                    guard let errorObject = APIErrorCodes.getErrorObject(apiName: AppConstants.apiNames.updateUserProfile, httpCode: statusCode) else { return }
                    self.errorView.animateInViewWithMessage(errorObject.errorTitle.uppercased(), errorObject.errorDescription.uppercased())
                } else {
                    self.errorView.animateInViewWithMessage(AppConstants.ErrorMessages.opsSomethingWentWrong, AppConstants.ErrorMessages.failedConnectingServer)
                }
                
                if let errorDesc = error.userInfo.valueForKeyPath(keyPath: "NSLocalizedDescription") {
                    AnalyticsHelper.APIFailureEvent(screen: AppConstants.screenNames.editProfile, apiName: AppConstants.apiNames.updateUserProfile, errorMessage: errorDesc as! String, userId: Helper().getUserId(), errorCode: nil, requestPayload: updateUserAPI.requestPayload, requestMethod: updateUserAPI.requestMethod.rawValue, requestPathParams: updateUserAPI.requestPathParams)
                    return
                }
                
                self.view.endEditing(true)
                SVProgressHUD.dismiss()
                
                self.profileUpdateTrace?.incrementMetric("\(AppConstants.apiNames.updateUserProfile) API Failure", by: 1)
                self.profileUpdateTrace?.stop()
                
                break
            }
            
            metric.stop()
        }
    }
    
    // MARK: Button outlet actions
    
    func validateName(name: String) -> Bool {
        if name.count < 3 {
            let indexPath = IndexPath(row: 1, section: 0)
            self.userEditTableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
            
            let nameCell = self.userEditTableView.cellForRow(at: IndexPath(row: 1, section: 0)) as! UserEditProfileCell
            nameCell.rowInput.rightViewMode = UITextFieldViewMode.always
            let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
            let image = UIImage(named: "iconCrossCircle")
            imageView.image = image
            nameCell.rowInput.rightView = imageView
            
            return false
        }
        return true
    }
    
    func validateMobileNumber(mobile: String) -> Bool {
        var mobileValidationMessage = ""
        if mobile.count < 5 {
            mobileValidationMessage = "Mobile number is too short."
        } else if mobile.count > 15 {
            mobileValidationMessage = "Mobile number is too long."
        }
        if mobile.count < 5 || mobile.count > 15  {
            let indexPath = IndexPath(row: 1, section: 1)
            self.userEditTableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
            
            let mobileCell = self.userEditTableView.cellForRow(at: IndexPath(row: 1, section: 1)) as! CountryCodeEditProfileTableViewCell
            mobileCell.rowInput.rightViewMode = UITextFieldViewMode.always
            let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
            let image = UIImage(named: "iconCrossCircle")
            imageView.image = image
            mobileCell.rowInput.rightView = imageView
            return false
        }
        return true
    }
    
    func validateEmail(email: String) -> Bool {
        if email.count > 0 && !Helper().isValidEmail(email: email) {
            let indexPath = IndexPath(row: 0, section: 1)
            self.userEditTableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
            
            let emailCell = self.userEditTableView.cellForRow(at: IndexPath(row: 0, section: 1)) as! UserEditProfileCell
            emailCell.rowInput.rightViewMode = UITextFieldViewMode.always
            let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
            let image = UIImage(named: "iconCrossCircle")
            imageView.image = image
            emailCell.rowInput.rightView = imageView
            return false
        }
        return true
    }
    
    func validteDisplayName(displayName: String) -> Bool {
        if displayName.count < 2 {
            let indexPath = IndexPath(row: 2, section: 0)
            self.userEditTableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
            
            if let displayNameCell = self.userEditTableView.cellForRow(at: IndexPath(row: 2, section: 0)) as? UserEditProfileCell {
                displayNameCell.rowInput.rightViewMode = UITextFieldViewMode.always
                let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
                let image = UIImage(named: "iconCrossCircle")
                imageView.image = image
                displayNameCell.rowInput.rightView = imageView
                return false
            }
        }
        return true
    }
    
    @IBAction func editProfileButtonTap(_ sender: UIButton) {
        self.view.endEditing(true)
        
        guard let userProfileData = self.userData, let userData = userProfileData.user else { return }
        
        var name: String = ""
        if let tempName = self.name {
            name = tempName
        }
        
        var displayName: String = ""
        if let tempDisplayName = self.displayName {
            displayName = tempDisplayName
        }
        
        var bio: String = ""
        if let tempBio = self.bio {
            bio = tempBio
        }
        
        var email: String = ""
        if let tempEmail = self.email {
            email = tempEmail
        }
        
        var mobile: String = ""
        if let tempMobile = self.mobile {
            mobile = tempMobile
        }
        
        var birthDateTemp: String = ""
        var countryName: String = ""
        var cityName: String = ""
        var code: String = ""
        var userGender: String = ""
        var lat: CLLocationDegrees = 0
        var long: CLLocationDegrees = 0
        
        mobile = self.mobile!
        code = self.countryCode!
        
        // Pass non formatted bday here
        birthDateTemp = self.birthDate!
        
        userGender = self.gender!
        
        if (self.country != nil) {
            countryName = self.country!
        }
        if (self.city != nil) {
            cityName = self.city!
        }
        
        if (self.countryCode != nil) {
            code = self.countryCode!
        }
        
        if (self.latitude != nil) {
            lat = self.latitude!
        }
        if (self.longitude != nil) {
            long = self.longitude!
        }
        
        if !self.validateName(name: name) {
            return
        }
        
        if !self.validteDisplayName(displayName: displayName) {
            return
        }
        
        // NOTE: If mobile is verified then is no validation required for email id entered by user & vice versa
        
        if mobileVerified != true {
            if !self.validateEmail(email: email) {
                return
            }
        } 
        
        if emailVerified != true {
            if !self.validateMobileNumber(mobile: mobile) {
                return
            }
        }
        
        if InternetConnection.isConnectedToInternet() {
            SVProgressHUD.show()
            
            guard let verifyDisplayNameMetric = HTTPMetric(url: URL(string: UserService().verifyDisplayNameAPIEndPoint())!, httpMethod: .post) else { return }
            verifyDisplayNameMetric.start()
            
            self.profileUpdateTrace = Performance.startTrace(name: AppConstants.Traces.profileUpdating)
            
            let verifyDisplayNameAPI = UserService().verifyDisplayName(userId: userData._id, displayName: displayName)
            verifyDisplayNameAPI.response.validate(statusCode: 200..<300).responseJSON { response in
//                debugPrint(response)
                switch response.result {
                case .success:
                    let response = response.result.value as! NSDictionary
                    let imageConfig = CLDConfiguration(cloudName: SminqAPI.cloudinaryCloudName(), apiKey: SminqAPI.cloudinaryApiKey(), apiSecret: SminqAPI.cloudinaryApiSecret())
                            let cloudinaryImageConfig = CLDCloudinary(configuration: imageConfig)
                            let params = CLDUploadRequestParams()
                            params.setResourceType("image")
                            
                            if self.selectedProfilePicture {
                                if let imageData = UIImageJPEGRepresentation(self.selectedUserImage!, 0.5) {
                                    
                                    let progressHandler = { (progress: Progress) in
                                        
//                                        DispatchQueue.main.async {
//                                            let percentageCompleted = String(format:"%.0f", progress.fractionCompleted * 100.0)
//                                            SVProgressHUD.showProgress(Float(progress.fractionCompleted as Double), status: percentageCompleted.appending("%"))
//                                        }
                                    }
                                    
                                    cloudinaryImageConfig.createUploader().upload(data: imageData as Data, uploadPreset: FIRRemoteConfigService.shared.getCloudinaryUserProfileImagePreset(), params: params, progress: progressHandler) { (response, error) in
                                        
                                        DispatchQueue.main.async {
                                            SVProgressHUD.dismiss()
                                            if error == nil {
                                                self.profileUpdateTrace?.incrementMetric("\(AppConstants.IntermediateTraceEvents.cloudinaryImageUpload) Success", by: 1)
                                                self.updateUser(userId: userData._id, name: name, displayName: displayName, email: email, mobile: mobile, latitude: lat, longitude: long, countryName: countryName, cityName: cityName, countryCode: code, gender: userGender, birthDate: birthDateTemp, isSignUp: 0, isExistingUser: true, bio: bio, image: (response?.url)!)
                                            } else {
                                                self.profileUpdateTrace?.incrementMetric("\(AppConstants.IntermediateTraceEvents.cloudinaryImageUpload) Failure", by: 1)
                                                self.profileUpdateTrace?.stop()
                                                SVProgressHUD.dismiss()
                                            }
                                            
                                        }
                                        
                                    }
                                }
                            } else {
                                // Update API CALL
                                self.updateUser(userId: userData._id, name: name, displayName: displayName, email: email, mobile: mobile, latitude: lat, longitude: long, countryName: countryName, cityName: cityName, countryCode: code, gender: userGender, birthDate: birthDateTemp, isSignUp: 0, isExistingUser: true, bio: bio, image: userData.imageUrl)
                            }
                    
                   // SVProgressHUD.dismiss()
                    
                    self.profileUpdateTrace?.incrementMetric("\(AppConstants.apiNames.verifyDisplayName) API Success", by: 1)
                    self.profileUpdateTrace?.stop()
                    break
                    
                case .failure(let error):
                    LogUtil.error(error)
                    
                    if let statusCode = response.response?.statusCode {
                        if statusCode == 409 {
                            let indexPath = IndexPath(row: 2, section: 0)
                            self.userEditTableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
                            
                            let displayNameCell = self.userEditTableView.cellForRow(at: IndexPath(row: 2, section: 0)) as! UserEditProfileCell
                            displayNameCell.rowInput.rightViewMode = UITextFieldViewMode.always
                            let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
                            let image = UIImage(named: "iconCrossCircle")
                            imageView.image = image
                            displayNameCell.rowInput.rightView = imageView
                        } else {
                            guard let errorObject = APIErrorCodes.getErrorObject(apiName: AppConstants.apiNames.verifyDisplayName, httpCode: statusCode) else { return }
                            self.errorView.animateInViewWithMessage(errorObject.errorTitle.uppercased(), errorObject.errorDescription.uppercased())
                        }
                    } else {
                        self.errorView.animateInViewWithMessage(AppConstants.ErrorMessages.opsSomethingWentWrong, "")
                    }
               
                    let error = error as NSError
                    
                    if let errorDesc = error.userInfo.valueForKeyPath(keyPath: "NSLocalizedDescription") as? String {
                        AnalyticsHelper.APIFailureEvent(screen: AppConstants.screenNames.editProfile, apiName: AppConstants.apiNames.verifyDisplayName, errorMessage: errorDesc, userId: userData._id, errorCode: error.code, requestPayload: verifyDisplayNameAPI.requestPayload, requestMethod: verifyDisplayNameAPI.requestMethod.rawValue, requestPathParams: verifyDisplayNameAPI.requestPathParams)
                    }
                    self.view.endEditing(true)
                    SVProgressHUD.dismiss()
                    
                    self.profileUpdateTrace?.incrementMetric("\(AppConstants.apiNames.verifyDisplayName) API Failure", by: 1)
                    self.profileUpdateTrace?.stop()
                    break
                }
                
                verifyDisplayNameMetric.stop()
            }
        } else {
            self.errorView.animateInViewWithMessage(AppConstants.ErrorMessages.noInternet.uppercased(), AppConstants.ErrorMessages.noInterneSubHeading)
        }
    }
    
    // MARK: Target methods
    
    @objc func imageUIViewTapped (_ sender: UITapGestureRecognizer) {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Take Photo", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Choose From Library", style: .default, handler: { _ in
            self.openGallary()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc func countrySelected(_ notification: NSNotification) {
        if let country = notification.userInfo!["country"], let code = notification.userInfo!["code"], let flag = notification.userInfo!["flag"] {
            self.country = country as? String
            self.countryCode = code as? String
            
            self.userEditTableView.reloadRows(at: [IndexPath(row: 1, section: 1)], with: UITableViewRowAnimation.automatic)
            
        }
    }
    
    @objc func tappedBackButton() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func navigateToCountryCodeSelectionScreen(_ sender: UIButton) {
        showCameraButton = true
        if let countryCodeSelectionViewController = AppConstants.storyboard.instantiateViewController(withIdentifier: "CountryCodeSelectionViewController") as? CountryCodeSelectionViewController {
            self.navigationController?.pushViewController(countryCodeSelectionViewController, animated: true)
        }
    }
  
}

extension EditProfileViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 && indexPath.row == 0 {
            return 112
        } else if indexPath.section == 0 && indexPath.row == 3 {
            return self.bioCellHeight + 12 + 16
        } else if indexPath.section == 0 && indexPath.row == 4 {
            return 40
        } else if indexPath.section == 1 && indexPath.row == 5 {
            return 80
        }
        return 48
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "EditProfileImageViewTableViewCell", for: indexPath) as! EditProfileImageViewTableViewCell
                
                cell.outerCircleUIView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(imageUIViewTapped(_:))))
                
                return cell
            } else if indexPath.row == 1 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "UserEditProfileCell", for: indexPath) as! UserEditProfileCell
                //                cell.rowInput.tag = indexPath.row
                cell.rowInput.tag = inputViewTags[INPUT_VIEW_NAME_KEY]!
                
                cell.rowLabel.text = "NAME"
                cell.rowInput.placeholder = "Your name..."
                cell.rowInput.text = name
                return cell
            } else if indexPath.row == 2 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "UserEditProfileCell", for: indexPath) as! UserEditProfileCell
                //                cell.rowInput.tag = indexPath.row
                cell.rowInput.tag = inputViewTags[INPUT_VIEW_USERNAME_KEY]!
                
                cell.rowLabel.text = "USERNAME"
                cell.rowInput.placeholder = "Your username..."
                cell.rowInput.text = displayName
                return cell
            } else if indexPath.row == 3 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "UserEditProfileBioCell", for: indexPath) as! UserEditProfileBioCell
//                cell.rowInput.tag = inputViewTags[INPUT_VIEW_BIO_KEY]!
//
//                cell.rowLabel.text = "BIO"
//                cell.rowInput.placeholder = "Your bio..."
//                cell.rowInput.text = bio
                cell.setBioLabel(bio: bio ?? "")
                cell.delegate = self
                return cell
            } else if indexPath.row == 4 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "EditProfileHeaderLabelTableViewCell", for: indexPath) as! EditProfileHeaderLabelTableViewCell
                
                return cell
            }
        } else {
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "UserEditProfileCell", for: indexPath) as! UserEditProfileCell
                //                cell.rowInput.tag = indexPath.row
                cell.rowInput.tag = inputViewTags[INPUT_VIEW_EMAIL_KEY]!
                
                cell.rowLabel.text = "EMAIL"
                cell.rowInput.placeholder = "Your email ID..."
                cell.rowInput.text = email
                
                if emailVerified == true {
                    cell.rowInput.isEnabled = false
                } else {
                    cell.rowInput.isEnabled = true
                }
                
                return cell
            } else if indexPath.row == 1 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "CountryCodeEditProfileTableViewCell", for: indexPath) as! CountryCodeEditProfileTableViewCell
                //                cell.rowInput.tag = indexPath.row
                cell.rowInput.tag = inputViewTags[INPUT_VIEW_MOBILE_KEY]!
                
                cell.rowLabel.text = "MOBILE"
                cell.rowInput.placeholder = "Your mobile number..."
                cell.rowInput.text = mobile
                cell.countryCodeLabel.text = self.countryCode
                
                cell.countryCodeDropdownButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(navigateToCountryCodeSelectionScreen(_:))))
                cell.countryCodeLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(navigateToCountryCodeSelectionScreen(_:))))
                
                if mobileVerified == true {
                    cell.rowInput.isEnabled = false
                } else {
                    cell.rowInput.isEnabled = true
                }
                
                return cell
                
            } else if indexPath.row == 2 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "UserEditProfileCell", for: indexPath) as! UserEditProfileCell
                //                cell.rowInput.tag = indexPath.row
                cell.rowInput.tag = inputViewTags[INPUT_VIEW_CITY_KEY]!
                
                cell.rowLabel.text = "CITY"
                cell.rowInput.placeholder = "Your city..."
                cell.rowInput.text = city
                
                if (country != nil && country != "" && city != nil && city != "") {
                    cell.rowInput.text = city! + ", " + country!
                }
                return cell
                
            } else if indexPath.row == 3 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "UserEditProfileCell", for: indexPath) as! UserEditProfileCell
                //                cell.rowInput.tag = indexPath.row
                cell.rowInput.tag = inputViewTags[INPIT_VIEW_DOB_KEY]!
                
                cell.rowLabel.text = "D.O.B"
                cell.rowInput.placeholder = "Your date of birth..."
                cell.rowInput.text = Helper().formattedBirthdate(birthDate: birthDate!)
                return cell
                
            } else if indexPath.row == 4 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "UserEditProfileCell", for: indexPath) as! UserEditProfileCell
                //                cell.rowInput.tag = indexPath.row
                cell.rowInput.tag = inputViewTags[INPIT_VIEW_GENDER_KEY]!
                
                cell.rowLabel.text = "GENDER"
                cell.rowInput.placeholder = "Your gender..."
                cell.rowInput.text = gender
                return cell
                
            } else if indexPath.row == 5 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "EditProfileSaveButtonTableViewCell", for: indexPath) as! EditProfileSaveButtonTableViewCell
                
                cell.saveButton.addTarget(self, action: #selector(editProfileButtonTap(_:)), for: .touchUpInside)
                return cell
            }
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "EmptyTableViewCell", for: indexPath) as! EmptyTableViewCell
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 4 + 1 // Exta row for header
        } else {
            return 5 + 1 // Extra row for Save button
        }
    }
    
}

extension EditProfileViewController: UIPickerViewDataSource, UIPickerViewDelegate {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return genderDropdown.count
    }
    
    // This function sets the text of the picker view to the content of the "salutations" array
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return genderDropdown[row]
    }
    
}

extension EditProfileViewController: UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        switch textField.tag {
        case inputViewTags[INPUT_VIEW_NAME_KEY]:
            name = textField.text
            break
            
        case inputViewTags[INPUT_VIEW_USERNAME_KEY]:
            displayName = textField.text
            break
            
        case inputViewTags[INPUT_VIEW_BIO_KEY]:
            bio = textField.text
            break
            
        case inputViewTags[INPUT_VIEW_MOBILE_KEY]:
            mobile = textField.text
            break
        
        case inputViewTags[INPUT_VIEW_EMAIL_KEY]:
            email = textField.text
            
        default:
            break
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        switch textField.tag {
        case inputViewTags[INPUT_VIEW_NAME_KEY]:
            let allowedCharacters = CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ. ")
            let characterSet = CharacterSet(charactersIn: string)
            
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            
            return allowedCharacters.isSuperset(of: characterSet) && newLength <= 25
            
        case inputViewTags[INPUT_VIEW_USERNAME_KEY]:
            let allowedCharacters = CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ01234567890_.")
            let characterSet = CharacterSet(charactersIn: string)
            
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            
            return allowedCharacters.isSuperset(of: characterSet) && newLength <= 15
            
        case inputViewTags[INPUT_VIEW_BIO_KEY]:
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            
            return newLength <= bioTextLimit
            
        case inputViewTags[INPUT_VIEW_MOBILE_KEY]:
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 10
        default:
            break
        }
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField.tag {
        case inputViewTags[INPUT_VIEW_NAME_KEY]:
            guard let cell = self.userEditTableView.cellForRow(at: IndexPath(row: 2, section: 0)) as? UserEditProfileCell else { return true }
            cell.rowInput?.becomeFirstResponder()
            
        case inputViewTags[INPUT_VIEW_USERNAME_KEY]:
            guard let cell = self.userEditTableView.cellForRow(at: IndexPath(row: 3, section: 0)) as? UserEditProfileBioCell else { return true }
            cell.bioTextView?.becomeFirstResponder()
            
        case inputViewTags[INPUT_VIEW_EMAIL_KEY]:
            guard let cell = self.userEditTableView.cellForRow(at: IndexPath(row: 1, section: 1)) as? CountryCodeEditProfileTableViewCell else { return true}
            cell.rowInput?.becomeFirstResponder()
            
        default:
            textField.resignFirstResponder()
        }
        
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        textField.text = ""
        textField.resignFirstResponder()
        return false
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        switch textField.tag {
        case inputViewTags[INPUT_VIEW_NAME_KEY]:
            textField.keyboardType = UIKeyboardType.alphabet
            textField.returnKeyType = .next
            
            break
        case inputViewTags[INPUT_VIEW_USERNAME_KEY]:
            textField.returnKeyType = .next
            break
            
        case inputViewTags[INPUT_VIEW_EMAIL_KEY]:
            textField.keyboardType = UIKeyboardType.emailAddress
            textField.returnKeyType = .next
            break;
        case inputViewTags[INPUT_VIEW_MOBILE_KEY]:
            textField.keyboardType = .numberPad
            break;
        case inputViewTags[INPUT_VIEW_CITY_KEY]:
            // CITY
            let autocompleteController = GMSAutocompleteViewController()
            autocompleteController.delegate = self
            
            let filter = GMSAutocompleteFilter()
            filter.type = .city
            
            autocompleteController.autocompleteFilter = filter
            present(autocompleteController, animated: true, completion: nil)
            
            return false
        //            break;
        case inputViewTags[INPIT_VIEW_DOB_KEY]:
            datePicker.datePickerMode = .date
            
            let currentDate = Date()
            var dateComponents = DateComponents()
            let calendar = Calendar.init(identifier: .gregorian)
            
            dateComponents.year = -12
            let maxDate = calendar.date(byAdding: dateComponents, to: currentDate)
            datePicker.maximumDate = maxDate
            
            textField.inputView = datePicker
            
            let toolbar = UIToolbar()
            toolbar.sizeToFit()
            let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(EditProfileViewController.donedatePicker(_:)))
            let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
            let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: #selector(EditProfileViewController.cancelDatePicker))
            
            toolbar.setItems([doneButton,spaceButton, cancelButton], animated: false)
            
            textField.inputAccessoryView = toolbar
            break
            
        case inputViewTags[INPIT_VIEW_GENDER_KEY]:
            textField.inputView = genderPickerView
            
            let toolbar = UIToolbar()
            toolbar.sizeToFit()
            let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(EditProfileViewController.doneGenderListPicker(_:)))
            let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
            let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: #selector(EditProfileViewController.cancelGenderListPicker))
            
            toolbar.setItems([doneButton,spaceButton, cancelButton], animated: false)
            
            textField.inputAccessoryView = toolbar
            break
            
        default:
            break
        }
        return true
    }
    
    @objc func donedatePicker(_ textField: UITextField) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM.dd.yyyy"
        self.birthDate = dateFormatter.string(from: datePicker.date)
        
        self.birthDate = Helper().nonFormattedBirthdate(birthDate: self.birthDate!)
        
        let placeIndexPath = IndexPath(row: 3, section: 1)
        self.userEditTableView.reloadRows(at: [placeIndexPath], with: UITableViewRowAnimation.automatic)
        
        self.view.endEditing(true)
    }
    
    @objc func doneGenderListPicker(_ textField: UITextField) {
        self.gender = genderDropdown[genderPickerView.selectedRow(inComponent: 0)]
        
        let placeIndexPath = IndexPath(row: 4, section: 1)
        self.userEditTableView.reloadRows(at: [placeIndexPath], with: UITableViewRowAnimation.automatic)
        
        self.view.endEditing(true)
    }
    
    @objc func cancelDatePicker() {
        self.view.endEditing(true)
    }
    
    @objc func cancelGenderListPicker() {
        self.view.endEditing(true)
    }
}

extension EditProfileViewController: GMSAutocompleteViewControllerDelegate {
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        guard let _ = place.placeID else {
            self.errorView.animateInViewWithMessage(AppConstants.ErrorMessages.opsSomethingWentWrong, "")
            self.dismiss(animated: true, completion: nil)
            return
        }
        
        self.latitude = place.coordinate.latitude
        self.longitude = place.coordinate.longitude
        self.city = place.name
        
        if let addressComponents = place.addressComponents {
            for addressComponent in addressComponents {
                let types = addressComponent.type
                
                if (types.contains("country")) {
                    self.country = addressComponent.name
                }
            }
        }
        
        let placeIndexPath = IndexPath(row: 2, section: 1)
        self.userEditTableView.reloadRows(at: [placeIndexPath], with: UITableViewRowAnimation.automatic)
        
        self.dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        LogUtil.error(error)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}

//MARK: - UIImagePickerControllerDelegate
extension EditProfileViewController:  UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
    
        if let editedImage = info[UIImagePickerControllerEditedImage] as? UIImage {
            self.setProfilePicFromImageResource(image: editedImage)
        }
        
        //Dismiss the UIImagePicker after selection
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
}

extension EditProfileViewController: ErrorViewDelegate {
    func errorViewTapped() {
        self.errorView.animateOutView()
    }
}

extension EditProfileViewController: UserEditProfileBioCellDelegate {
    func didCellHeightChange(height: CGFloat, cell: UserEditProfileBioCell, bio: String) {
        self.bioCellHeight = height
        self.bio = bio
        
        UIView.performWithoutAnimation {
            self.userEditTableView.beginUpdates()
            self.userEditTableView.endUpdates()
        }
        
        cell.bioTextView.becomeFirstResponder()
    }
}
