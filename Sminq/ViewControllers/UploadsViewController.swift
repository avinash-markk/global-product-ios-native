//
//  UploadsViewController.swift
//  Sminq
//
//  Created by Avinash Thakur on 14/03/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import UIKit
import Gloss

protocol UploadsViewControllerDelegate: NSObjectProtocol {
    func uploadsViewDismissed()
}

class UploadsViewController: UIViewController {
    weak var delegate: UploadsViewControllerDelegate?
    @IBOutlet weak var uploadsTableView: UITableView!
    var tabBarViewController: TabbarViewController?
    let errorView = ErrorView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        uploadsTableView.backgroundColor = AppConstants.SminqColors.blackDark100
        self.view.backgroundColor = AppConstants.SminqColors.blackDark100
        uploadsTableView.register(UINib.init(nibName: "UploadsTableViewCell", bundle: nil), forCellReuseIdentifier: "UploadsTableViewCell")
        tabBarViewController?.uploadDelegate = self
        self.errorViewSetup()
        NotificationCenter.default.addObserver(self, selector: #selector(updateNetworkStatus), name: NSNotification.Name(rawValue: AppConstants.AppNotificationNames.networkReachable), object: nil)
        if !NetworkManager.isConnectedToInternet() {
            self.errorView.animateInViewWithMessage(AppConstants.ErrorMessages.noInternet.uppercased(), AppConstants.ErrorMessages.noInterneSubHeading)
        }
    }
    
    @objc func updateNetworkStatus(notification: Notification) {
        guard let status = notification.userInfo?["status"] as? Bool else { return }
        if status {
            self.errorView.animateOutView()
        } else {
            self.errorView.animateInViewWithMessage(AppConstants.ErrorMessages.noInternet.uppercased(), AppConstants.ErrorMessages.noInterneSubHeading)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.setupNavigationBar()
        setNeedsStatusBarAppearanceUpdate()
    }
    
    override public var prefersStatusBarHidden: Bool {
        return false
    }
    
    override public var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func errorViewSetup() {
        errorView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: self.getSafeAreaInsets().top + 56)
        errorView.setStackViewTopConstraint(top: self.getSafeAreaInsets().top)
        self.view.addSubview(errorView)
        errorView.isHidden = true
        errorView.delegate = self
    }
    
    private func setupNavigationBar() {
        
        let closeButton = UIButton(frame: CGRect(x: 0, y: 0, width: 32, height: 32))
        let closeIcon = UIImage(named: "iconClose24White100")?.withRenderingMode(.alwaysTemplate)
        closeButton.setImage(closeIcon, for: UIControlState())
        closeButton.tintColor = AppConstants.SminqColors.white100
        closeButton.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0)
        closeButton.addTarget(self, action: #selector(tappedCloseButton), for: .touchUpInside)
        navigationItem.rightBarButtonItems = [.init(customView: closeButton)]
        
        let navigationTitleLabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.view.frame.width - (100 * 2), height: 44))
        navigationTitleLabel.font = UIFont(name: AppConstants.Font.primaryRegular, size: 20.0)
        navigationTitleLabel.textColor = UIColor.white
        navigationTitleLabel.textAlignment = NSTextAlignment.center
        navigationItem.titleView = navigationTitleLabel
        navigationTitleLabel.text = "Uploads"
        navigationController?.navigationBar.barTintColor = AppConstants.SminqColors.blackLight100
        navigationController?.navigationBar.isTranslucent = false
    }
    
    @objc func tappedCloseButton() {
        self.tabBarViewController?.isUploadsViewVisible = false
        self.delegate?.uploadsViewDismissed()
        self.dismiss(animated: true, completion: nil)
    }
    
    func viewUploadedRatings(task: UploadTask) {
        let feedDataJson = Helper().stringToJsonDictionaryConversion(inputString: task.post!)
        guard let json = feedDataJson as? JSON else {
            return
        }
        if let feedData: UserDataPlaceFeed = UserDataPlaceFeed(json: json) {
            let userfeed: [UserDataPlaceFeed] = [feedData]
            
            
            let storyViewManager = StoryViewManager.init(screen: AppConstants.screenNames.uploads)
            let storyViewController = StoryViewController.init(storyViewManager: storyViewManager)
            
            storyViewController.delegate = self
            
            let feedViewModel = FeedViewModel.init(screen: AppConstants.screenNames.storyView)!
            feedViewModel.setFeedList(feedList: userfeed)
            let storyData = StoryData.init(userId: nil, placeId: nil, placeDetails: feedData.placeDetails, archived: false, posts: userfeed, feedViewModel: feedViewModel)
            
            let storyViewModel = StoryViewModel(screen: AppConstants.screenNames.uploads, storyList: [storyData])!
            storyViewController.storyViewModel = storyViewModel
            storyViewController.selectedPlace = feedData.placeDetails
            
            let navController = UINavigationController(rootViewController: storyViewController)
            navController.modalPresentationStyle = .overFullScreen
            navController.modalPresentationCapturesStatusBarAppearance = true
            self.present(navController, animated: true, completion: nil)
        }
    }

}

extension UploadsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return UploadManager.sharedInstance.operationsArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 116
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "UploadsTableViewCell", for: indexPath) as? UploadsTableViewCell else {
            return UITableViewCell()
        }
        cell.delegate = self
        if let task = UploadManager.sharedInstance.operationsArray[indexPath.row] as? UploadTask {
          cell.updateDataForUploads(task: task)
          cell.uploadActionButton.tag = indexPath.row
        }
        
        return cell
    }
    
}

extension UploadsViewController: UploadsTableViewCellDelegate {
    
    func uploadActionButtonTapped(sender: UIButton) {
        let index = sender.tag
        if let buttonAction = sender.title(for: .normal) {
            switch buttonAction {
            case "VIEW":
                self.viewButtonTappedFor(index: index)
                AnalyticsHelper.uploadsAnalyticsEvent(eventName: AnalyticsHelper.UploadActionsEventName.viewUpload)
            case "RETRY":
                self.retryButtonTappedFor(index: index)
                AnalyticsHelper.uploadsAnalyticsEvent(eventName: AnalyticsHelper.UploadActionsEventName.retryUpload)
            case "CANCEL":
                self.cancelButtonTappedFor(index: index)
                AnalyticsHelper.uploadsAnalyticsEvent(eventName: AnalyticsHelper.UploadActionsEventName.cancelUpload)
                
            default:
                break
            }
        }
    }
    
    func viewButtonTappedFor(index: Int) {
        if let task = UploadManager.sharedInstance.operationsArray.object(at: index) as? UploadTask {
            self.viewUploadedRatings(task: task)
        }
    }
    
    func retryButtonTappedFor(index: Int) {
        self.refreshAndRetryUploadList(index: index)
    }
    
    func refreshAndRetryUploadList(index: Int) {
        if let retryTask = UploadManager.sharedInstance.operationsArray.object(at: index) as? UploadTask {
            retryTask.status = "Pending"
            retryTask.progress = 0.0
            UploadManager.sharedInstance.operationsArray.replaceObject(at: index, with: retryTask)
            self.reloadCellAtIndex(index: index)
            }
            UploadManager.sharedInstance.checkAndUploadNext()
    }
    
    func cancelButtonTappedFor(index: Int) {
        if let task = UploadManager.sharedInstance.operationsArray.object(at: index) as? UploadTask {
            if task.status == "InProgress" {
                self.confirmBeforeDeletingInProgressUpload(index: index)
            } else {
                self.confirmBeforeDeletingPendingUpload(index: index)
            }
        }
    }
    
    func confirmBeforeDeletingInProgressUpload(index: Int) {
        UploadManager.sharedInstance.suspendCurrentUpload()
        let alertController = UIAlertController(title: "", message: "Are you sure?", preferredStyle: .alert)
        let yesButtonAction = UIAlertAction(title: "Yes", style: .default) { _ in
            self.cancelInProgressUploadFor(index: index)
        }
        let noButtonAction = UIAlertAction(title: "No", style: .cancel) { _ in
            UploadManager.sharedInstance.resumeCurrentUpload()
        }
        alertController.addAction(yesButtonAction)
        alertController.addAction(noButtonAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func cancelInProgressUploadFor(index: Int) {
        if let task = UploadManager.sharedInstance.operationsArray.object(at: index) as? UploadTask {
            UploadManager.sharedInstance.cancelCurrentUpload()
            UploadManager.sharedInstance.operationsArray.removeObject(at: index)
            UploadDatabaseService.shared.deleteFeedWithFeed(feedID: task.taskId!)
            self.uploadsTableView.reloadData()
        }
    }
    
    func confirmBeforeDeletingPendingUpload(index: Int) {
        let alertController = UIAlertController(title: "", message: "Are you sure?", preferredStyle: .alert)
        let yesButtonAction = UIAlertAction(title: "Yes", style: .default) { _ in
            self.cancelPendingUploadFor(index: index)
        }
        let noButtonAction = UIAlertAction(title: "No", style: .cancel) { _ in
        }
        alertController.addAction(yesButtonAction)
        alertController.addAction(noButtonAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func cancelPendingUploadFor(index: Int) {
        if let task = UploadManager.sharedInstance.operationsArray.object(at: index) as? UploadTask {
            UploadManager.sharedInstance.operationsArray.removeObject(at: index)
            UploadDatabaseService.shared.deleteFeedWithFeed(feedID: task.taskId!)
            self.uploadsTableView.reloadData()
        }
    }
    
}

extension UploadsViewController: TabBarUploadDelegate {
    
    func updateUploadList() {
       self.uploadsTableView.reloadData()
    }
    
    func updateUploadProgress(progressTask: UploadTask) {
        self.reloadCellForCurrentUpload(task: progressTask)
    }

    func updateUploadStateFor(task: UploadTask) {
        UploadDatabaseService.shared.updateStatusForFeed(feedID: task.taskId!, newStatus: task.status)
        self.reloadCellForCurrentUpload(task: task)
    }
    
    func reloadCellForCurrentUpload(task: UploadTask) {
        let currentTaskId = task.taskId!
        for (index, item) in UploadManager.sharedInstance.operationsArray.enumerated() {
            if let task = item as? UploadTask {
                if task.taskId == currentTaskId {
                    UploadManager.sharedInstance.operationsArray.replaceObject(at: index, with: task)
                    self.reloadCellAtIndex(index: index)
                }
            }
        }
    }
    
    func reloadCellAtIndex(index: Int) {
        let indexPath = IndexPath(row: index, section: 0)
        uploadsTableView.reloadRows(at: [indexPath], with: UITableViewRowAnimation.none)
    }

}

extension UploadsViewController: StoryViewControllerDelegate {
    func didDismissStoryView() {
        showTabBar()
    }
    
    func didUpvotePost(post: UserDataPlaceFeed) {
        
    }
}



extension UploadsViewController: ErrorViewDelegate {
    
    func errorViewTapped() {
        self.errorView.animateOutView()
    }
    
}
