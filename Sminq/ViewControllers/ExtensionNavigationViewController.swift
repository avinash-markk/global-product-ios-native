//
//  ExtensionNavigationViewController.swift
//  Sminq
//
//  Created by Pushkar Deshmukh on 28/03/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import UIKit


extension UIViewController {
    func navigateToLoginViewController() {
        if let loginViewController = AppConstants.storyboard.instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController {
            self.navigationController?.pushViewController(loginViewController, animated: true)
        }
    }
    
    func presentPostSuccessVC() {
        if let postSuccessViewController = AppConstants.storyboard.instantiateViewController(withIdentifier: "PostSuccessViewController") as? PostSuccessViewController {
            self.present(postSuccessViewController, animated: true, completion: nil)
        }
    }
}
