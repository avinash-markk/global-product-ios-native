//
//  ViewController1.swift
//  Sminq
//
//  Created by Bjorn Mascarenhas on 04/05/18.
//  Copyright © 2018 Suraj Sagare. All rights reserved.
//

import UIKit

class ViewController1: UIViewController, UITextViewDelegate {

    let otherTextView = UITextView()
    let imageView = UIImageView()

    @IBOutlet weak var scrollView: UIScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
       otherTextView.frame = CGRect(x: 0, y: 0, width: 375, height: 35)
       otherTextView.delegate = self
       otherTextView.backgroundColor = UIColor.green
//       otherTextView.placeholderString = "Click pic"
       scrollView.addSubview(otherTextView)
        
        imageView.frame = CGRect(x: 0, y: otherTextView.frame.origin.y + otherTextView.frame.size.height, width: 375, height: 320)
        imageView.image = UIImage(named: "imgOnboarding1")
        imageView.isUserInteractionEnabled = true
        scrollView.addSubview(imageView)
        
        let closeButton = UIButton(frame: CGRect(x: imageView.frame.origin.x + imageView.frame.size.width - 30, y: 10, width: 24, height: 24))
        let upArrow = UIImage(named: "iconRemoveImage")?.withRenderingMode(.alwaysTemplate)
        closeButton.setImage(upArrow, for: UIControlState())
//        closeButton.tintColor = UIColor.white
        closeButton.imageEdgeInsets = UIEdgeInsetsMake(0, -25, 0, 2)
        closeButton.addTarget(self, action: #selector(tappedClosedButton), for: .touchUpInside)
        imageView.addSubview(closeButton)
    }
    
    @objc func tappedClosedButton() {
        self.imageView.image = UIImage(named: "")
    }


    func adjustUITextViewHeight(arg: UITextView) {
        arg.translatesAutoresizingMaskIntoConstraints = false
        arg.sizeToFit()
        arg.isScrollEnabled = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func textViewDidChange(_ textView: UITextView) {
        let fixedWidth = textView.frame.size.width
        textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
        let newSize = textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
        var newFrame = textView.frame
        newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
        textView.frame = newFrame
        
        imageView.frame = CGRect(x: 0, y: otherTextView.frame.origin.y + otherTextView.frame.size.height, width: 375, height: 320)
        
        scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: (imageView.frame.size.height + otherTextView.frame.size.height))
    }
    
}
