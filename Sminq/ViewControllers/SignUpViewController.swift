//
//  SignUpViewController.swift
//  Sminq
//
//  Created by Bjorn Mascarenhas on 27/02/18.
//  Copyright © 2018 Suraj Sagare. All rights reserved.
//

import UIKit
import Alamofire
import Gloss
import SVProgressHUD
import Firebase
import CleverTapSDK

class SignUpViewController: UIViewController {
    
    @IBOutlet var nameTextField: DesignableUITextField!
    @IBOutlet weak var displayNameTextField: UITextField!
    @IBOutlet var emailTextField: DesignableUITextField!
    @IBOutlet var signUp: UIButton!
    @IBOutlet weak var displayNameErrorLabel: UILabel!
    
    let errorView = ErrorView()
    
    var signUpData: SignUpData?
    var usersDataSignUp : UserDataDict?
    var network: String?
    var name: String?
    
    var nameTag: Int = 0
    var displayNameTag: Int = 1
    var emailTag: Int = 2
    
    // Note: 'Name' field is not returned in response of /login POST API even after passing in the body payload. Which is why it's passed from LoginViewController to this SignUpViewController
    
    // MARK: View controllers life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.title = "Profile Details"
        
        view.backgroundColor = AppConstants.SminqColors.blackDark100
        
        nameTextField.delegate = self
        nameTextField.setLeftPaddingPoints(16.0)
        nameTextField.setRightPaddingPoints(16.0)
        nameTextField.tag = nameTag
        nameTextField.delegate = self
        
        displayNameTextField.setLeftPaddingPoints(16.0)
        displayNameTextField.setRightPaddingPoints(16.0)
        displayNameTextField.tag = displayNameTag
        displayNameTextField.delegate = self
        
        emailTextField.setLeftPaddingPoints(16.0)
        emailTextField.setRightPaddingPoints(16.0)
        emailTextField.tag = emailTag
        emailTextField.delegate = self
        
        signUp.setTitleColor(AppConstants.SminqColors.white100, for: .disabled)
        signUp.setTitleColor(AppConstants.SminqColors.white100, for: .normal)
        signUp.backgroundColor = AppConstants.SminqColors.green200
        signUp.titleLabel?.font = UIFont(name: AppConstants.Font.primaryBold, size: 14)
        
        self.navigationController?.navigationBar.isTranslucent = false
        
        self.errorViewSetup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
        setupNavigationBar()
        setNeedsStatusBarAppearanceUpdate()
        nameTextField.becomeFirstResponder()
        
        if let name = name {
            nameTextField.text = name
            
            // Auto populate displayName
            
            var displayName: String = ""
            let nameTxtFieldCount = nameTextField.text?.count
            let nameTextString = nameTextField.text!
            
            if (nameTxtFieldCount)! >= Int(8) {
                let trimmedString = nameTextString.removingWhitespaces().lowercased()
                displayName = String(trimmedString.prefix(8))
            } else {
                let trimmedString = nameTextString.removingWhitespaces().lowercased()
                displayName = trimmedString
            }
            
            displayNameTextField.text = "\(displayName)"
            displayNameErrorLabel.text = "2-15 characters. _ and . are allowed."
            displayNameErrorLabel.font = UIFont(name: AppConstants.Font.primaryRegular, size: 14.0)
            displayNameErrorLabel.textColor = AppConstants.SminqColors.white100.withAlphaComponent(0.5)
        }
        
        if usersDataSignUp?.email != nil && usersDataSignUp?.email != "" {
            emailTextField.text = usersDataSignUp?.email
            emailTextField.isEnabled = false
        } else {
            emailTextField.isEnabled = true
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        AppConstants.previousScreen = AppConstants.screenNames.signupComplete
    }
    
    override public var prefersStatusBarHidden: Bool {
        return false
    }
    
    override public var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func errorViewSetup() {
        // TODO: Instead of using '56' calculate two label's height dynamically
        errorView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 8 + 56)
        errorView.setStackViewTopConstraint(top: 8)
        self.view.addSubview(errorView)
        errorView.isHidden = true
        errorView.delegate = self
    }
    
    private func setupNavigationBar() {
        // Back Button
        let backButton = UIButton(frame: CGRect(x: 0, y: 0, width: 35, height: 35))
        let upArrow = UIImage(named: "iconBack24White100")?.withRenderingMode(.alwaysTemplate)
        backButton.setImage(upArrow, for: UIControlState())
        backButton.tintColor = UIColor.white
        backButton.imageEdgeInsets = UIEdgeInsetsMake(0, -25, 0, 2)
        backButton.addTarget(self, action: #selector(tappedBackButton), for: .touchUpInside)
        navigationItem.leftBarButtonItems = [.init(customView: backButton)]
        
        // Navbar title label
        let navigationTitleLabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.view.frame.width - (100), height: 44))
        navigationTitleLabel.font = UIFont(name: AppConstants.Font.primaryRegular, size: 20)
        navigationTitleLabel.textColor = AppConstants.SminqColors.white100
        navigationTitleLabel.textAlignment = NSTextAlignment.center
        navigationItem.titleView = navigationTitleLabel
        navigationTitleLabel.text = "Profile Details"
        
        navigationController?.navigationBar.barTintColor = AppConstants.SminqColors.blackLight100
    }
    
    @objc func tappedBackButton() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func showAlertWithError(message: String) {
        let alertController = UIAlertController(title: "Sign up error", message: message, preferredStyle: .alert)
        let okayAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        alertController.addAction(okayAction)
        self.present(alertController, animated: true, completion: nil)
    }
    // MARK: All Buttons Actions
    @IBAction func signUpButton(_ sender: UIButton) {
        let nameTxtFieldCount = nameTextField.text?.count
        let nameTextString = nameTextField.text!
        
        if (nameTxtFieldCount! == 0 || nameTextString.isEmpty || (nameTextString.trimmingCharacters(in: .whitespaces).isEmpty)) {
            showAlertWithError(message: "Please enter name")
            nameTextField.borderColor = AppConstants.SminqColors.pinkishRed
            return
        } else if (nameTxtFieldCount! > 0 && nameTxtFieldCount! < 3) {
            showAlertWithError(message: "Please enter valid name")
            nameTextField.borderColor = AppConstants.SminqColors.pinkishRed
            return
        } else if (nameTxtFieldCount! > 25) {
            showAlertWithError(message: "Please enter valid name")
            nameTextField.borderColor = AppConstants.SminqColors.pinkishRed
            return
        }
        
        if displayNameTextField.text == "" || (displayNameTextField.text?.isEmpty)! {
            showAlertWithError(message: "Please enter username")
            displayNameTextField.borderColor = AppConstants.SminqColors.pinkishRed
            return
        }
        
        if displayNameTextField.text != "" && (displayNameTextField.text?.count)! < 2 {
            showAlertWithError(message: "Username can be between 2 to 15 characters, only letters and numbers allowed.")
            displayNameTextField.borderColor = AppConstants.SminqColors.pinkishRed
            return
        }
        
        if emailTextField.text != "" && (emailTextField.text?.count)! > 0  && !Helper().isValidEmail(email: emailTextField.text!) {
            showAlertWithError(message: "Invalid email format")
            emailTextField.borderColor = AppConstants.SminqColors.pinkishRed
            return
        }
        
        if InternetConnection.isConnectedToInternet() {
            signUpCall(name: nameTextField.text , displayName: displayNameTextField.text, id: (usersDataSignUp?._id)!, mobile: (usersDataSignUp?.mobile)!, email: emailTextField.text)
        } else {
            self.errorView.animateInViewWithMessage(AppConstants.ErrorMessages.noInternet.uppercased(), AppConstants.ErrorMessages.noInterneSubHeading)
        }
        
    }
    
    func signUpCall(name: String! , displayName: String! ,id: String, mobile: String!, email: String!) {
        SVProgressHUD.show()
        
        guard let verifyDisplayNameMetric = HTTPMetric(url: URL(string: UserService().verifyDisplayNameAPIEndPoint())!, httpMethod: .post) else { return }
        verifyDisplayNameMetric.start()
        
        let verifyDisplayNameAPI = UserService().verifyDisplayName(userId: id, displayName: displayName)
        
        verifyDisplayNameAPI.response.validate(statusCode: 200..<300).responseJSON { response in
//            debugPrint(response)
            
            if let statusCode = response.response?.statusCode {
                verifyDisplayNameMetric.responseCode = statusCode
            }
            
            switch response.result {
            case .success:
                SVProgressHUD.show()
                
                let param = [
                    "displayName": [displayName],
                    "name": name,
                    "email": email,
                    //                    "isSignUp": 1,
                    "id": id
                    ] as [String : Any]
                
                guard let signUpMetric = HTTPMetric(url: URL(string: SminqAPI.endpoint("/v1/users") as! String)!, httpMethod: .put) else { return }
                signUpMetric.start()
                
                LogUtil.infoAPIPayload(apiName: AppConstants.apiNames.updateUserProfile, payload: param, pathParams: [:], requestMethod: AppConstants.requestMethods.put)
                
                AFManager.request(SminqAPI.endpoint("/v1/users"), method: .put, parameters: param, encoding: JSONEncoding.default, headers: SminqAPI.headers())
                    .validate(statusCode: 200..<300)
                    .responseJSON { response in
//                        debugPrint(response)
                        
                        if let statusCode = response.response?.statusCode {
                            signUpMetric.responseCode = statusCode
                        }
                        
                        SVProgressHUD.dismiss()
                        
                        switch response.result {
                        case .success:
                            guard let json = response.result.value as? JSON else { return }
                            if let userData: UserDataDict = UserDataDict(json: json) {
                                let profileDataClass = ProfileDataClass()
                                profileDataClass.saveProfileData(userProfileData: UserProfileData.init(user: userData, userMetrics: nil, stories: [], levelsList: [], streaks: nil, highlight: nil, claims: nil))
                                UserDefaultsUtility.shared.setAuthToken(token: userData.authToken)
                                
                                AnalyticsHelper.userLoginToCleverTap(user: userData)
                                BugfenderService.shared.setBugFenderDeviceString(user: userData)
                                AnalyticsHelper.signupEvent(success: AppConstants.yes, error: "")
                                
                                // Register for sminq APNS Serve
                                UserNoficationService.shared.subscribeToPushNotifications()
//                                PushNotificationAPI.registerForSminqPushNotifications(userId: userData._id, fcmToken: UserDefaults.standard.string(forKey: "FCMToken")!, register: AppConstants.enablePush)
                                
                                self.commonSignIn(userData: userData)
                                
                            } else {
                                LogUtil.error(response)
                                self.errorView.animateInViewWithMessage(AppConstants.ErrorMessages.opsSomethingWentWrong, AppConstants.ErrorMessages.failedConnectingServer)
                                self.view.endEditing(true)
                                
                                guard let json = response.result.value as? JSON else { return }
                                if let errorData: APIErrorResponse = APIErrorResponse(json: json) {
                                    
                                    AnalyticsHelper.signupEvent(success: AppConstants.yes, error: errorData.error.message!)
                                    AnalyticsHelper.APIFailureEvent(screen: AppConstants.screenNames.signup, apiName: AppConstants.apiNames.updateUserProfile, errorMessage: errorData.errorMessage ?? "", userId: id, errorCode: errorData.error.code, requestPayload: param, requestMethod: AppConstants.requestMethods.put, requestPathParams: [:])
                                }
                            }
                            
                        case .failure(let error):
                            LogUtil.error(error)
                            
                            if let statusCode = response.response?.statusCode {
                                guard let errorObject = APIErrorCodes.getErrorObject(apiName: AppConstants.apiNames.updateUserProfile, httpCode: statusCode) else { return }
                                self.errorView.animateInViewWithMessage(errorObject.errorTitle.uppercased(), errorObject.errorDescription.uppercased())
                            } else {
                                self.errorView.animateInViewWithMessage(AppConstants.ErrorMessages.opsSomethingWentWrong, AppConstants.ErrorMessages.failedConnectingServer)
                            }
                            
                            let error = error as NSError
                            
                            let userInfo = error.userInfo
                            if let errorDesc = userInfo.valueForKeyPath(keyPath: "NSLocalizedDescription") as? String {
                                AnalyticsHelper.signupEvent(success: AppConstants.yes, error: errorDesc)
                                AnalyticsHelper.APIFailureEvent(screen: AppConstants.screenNames.signup, apiName: AppConstants.apiNames.updateUserProfile, errorMessage: errorDesc, userId: id, errorCode: nil, requestPayload: param, requestMethod: AppConstants.requestMethods.put, requestPathParams: [:])
                                return
                            }
                            
                            self.view.endEditing(true)
                            
                        }
                        
                        signUpMetric.stop()
                }
                
            case .failure(let error):
                LogUtil.error(error)
                
                if let statusCode = response.response?.statusCode {
                    if statusCode == 409 {
                        let arrow = UIImageView(image: UIImage(named: "iconCrossCircle"))
                        if let size = arrow.image?.size {
                            arrow.frame = CGRect(x: 0.0, y: 0.0, width: size.width + 20.0, height: size.height)
                        }
                        arrow.contentMode = UIViewContentMode.center
                        self.displayNameTextField.rightView = arrow
                        self.displayNameTextField.rightViewMode = UITextFieldViewMode.always
                        
                        self.displayNameErrorLabel.text = "This username already exists."
                        self.displayNameErrorLabel.font = UIFont(name: AppConstants.Font.primaryRegular, size: 14.0)
                        self.displayNameErrorLabel.textColor = AppConstants.SminqColors.pinkishRed
                        
                    } else {
                        guard let errorObject = APIErrorCodes.getErrorObject(apiName: AppConstants.apiNames.verifyDisplayName, httpCode: statusCode) else { return }
                        self.errorView.animateInViewWithMessage(errorObject.errorTitle.uppercased(), errorObject.errorDescription.uppercased())
                    }
                } else {
                    self.errorView.animateInViewWithMessage(AppConstants.ErrorMessages.opsSomethingWentWrong, "")
                }
                
                let error = error as NSError
                let userInfo = error.userInfo
                if let errorDesc = userInfo.valueForKeyPath(keyPath: "NSLocalizedDescription") as? String {
                    self.errorView.animateInViewWithMessage(AppConstants.ErrorMessages.opsSomethingWentWrong.uppercased(), AppConstants.ErrorMessages.failedConnectingServer)
                    
                    AnalyticsHelper.APIFailureEvent(screen: AppConstants.screenNames.signup, apiName: AppConstants.apiNames.verifyDisplayName, errorMessage: errorDesc, userId: id, errorCode: error.code, requestPayload: verifyDisplayNameAPI.requestPayload, requestMethod: verifyDisplayNameAPI.requestMethod.rawValue, requestPathParams: verifyDisplayNameAPI.requestPathParams)
                }
                
                SVProgressHUD.dismiss()
                self.view.endEditing(true)
            }
            
            verifyDisplayNameMetric.stop()
        }
    }
    
    func commonSignIn(userData: UserDataDict) {
        
        UserDefaults.standard.set(true, forKey: "SignIn")
        SVProgressHUD.dismiss()
        
        AppConstants.appDelegate.tabBarViewController =
            AppConstants.storyboard.instantiateViewController(withIdentifier :"TabbarViewController") as? TabbarViewController
        AppConstants.appDelegate.tabBarViewController?.selectedIndex = 0
        AppConstants.appDelegate.tabBarViewController?.previousController = TimelineViewController()
        AppConstants.appDelegate.window?.rootViewController = AppConstants.appDelegate.tabBarViewController
        self.navigationController?.checkAndDisplayBadgeForIncompleteProfile(data: userData)
    }
    
}
// MARK: UITextViewDelegate Functions
extension  SignUpViewController: UITextViewDelegate {
    
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange) -> Bool {
        let tcWebViewController = AppConstants.storyboard.instantiateViewController(withIdentifier: "TCWebViewController") as! TCWebViewController
        
        if (URL.absoluteString == SminqAPI.endPointForTerms()) {
            LogUtil.info("Tapped on terms and conditions link")
            AnalyticsHelper.viewedTermsConditionsEvent(screen: AppConstants.screenNames.signup)
            tcWebViewController.strWebURL = SminqAPI.endPointForTerms()
            tcWebViewController.strTitle = "Terms and Conditions"
            self.navigationController?.navigationBar.isHidden = false
            self.navigationController?.pushViewController(tcWebViewController, animated: true)
        } else if (URL.absoluteString == SminqAPI.endPointForPrivacy()) {
            LogUtil.info("Tapped on privacy policy link")
            AnalyticsHelper.viewedPrivacyPolicyEvent(screen: AppConstants.screenNames.signup)
            tcWebViewController.strWebURL = SminqAPI.endPointForPrivacy()
            tcWebViewController.strTitle = "Privacy Policy"
            self.navigationController?.navigationBar.isHidden = false
            self.navigationController?.pushViewController(tcWebViewController, animated: true)
        }
        return false
    }
    
}

extension SignUpViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField.tag {
        case nameTag:
            displayNameTextField.becomeFirstResponder()
        
        case displayNameTag:
            if usersDataSignUp?.email != nil && usersDataSignUp?.email != "" {
                textField.resignFirstResponder()
            } else {
                emailTextField.becomeFirstResponder()
            }
        default:
            textField.resignFirstResponder()
        }
        
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        switch textField.tag {
        case nameTag:
            textField.keyboardType = UIKeyboardType.alphabet
            textField.returnKeyType = .next
    
        case displayNameTag:
            textField.keyboardType = UIKeyboardType.alphabet
            textField.returnKeyType = .next
            
            if usersDataSignUp?.email != nil && usersDataSignUp?.email != "" {
                textField.returnKeyType = .done
            }
            
        case emailTag:
            textField.keyboardType = UIKeyboardType.emailAddress
            textField.returnKeyType = .done
            
        default:
            break;
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        switch textField.tag {
        case nameTag:
            nameTextField.borderColor = AppConstants.SminqColors.white100.withAlphaComponent(0.5)
            
            let allowedCharacters = CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ. ")
            let characterSet = CharacterSet(charactersIn: string)
            
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            
            return allowedCharacters.isSuperset(of: characterSet) && newLength <= 25
            
        case displayNameTag:
            displayNameTextField.borderColor = AppConstants.SminqColors.white100.withAlphaComponent(0.5)
            let allowedCharacters = CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ01234567890_.")
            let characterSet = CharacterSet(charactersIn: string)
            
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            
            return allowedCharacters.isSuperset(of: characterSet) && newLength <= 15
            
        case emailTag:
            emailTextField.borderColor = AppConstants.SminqColors.white100.withAlphaComponent(0.5)
            
        default:
            break
        }
        
        return true
    }
    
}


extension SignUpViewController: ErrorViewDelegate {
    func errorViewTapped() {
        self.errorView.animateOutView()
    }
}
