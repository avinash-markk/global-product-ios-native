//
//  OnBoardingViewController.swift
//  Sminq
//
//  Created by Pushkar Deshmukh on 27/03/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import UIKit

class OnBoardingViewController: UIViewController {
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var continueButton: UIButton!
    
    @IBOutlet weak var continueButtonTopConstraint: NSLayoutConstraint!
    
    var slides = [OnBoardingSlide]()
    
    var isSlide1Animated = false
    
    override public var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLayoutSubviews() {
        // NOTE: So that layout is updated for iPhone 5/SE

       updateSafeAreaViews()
    }

    override func viewSafeAreaInsetsDidChange() {
        updateSafeAreaViews()
    }
    
    fileprivate func updateSafeAreaViews() {
        if #available(iOS 11.0, *) {
            print(view.safeAreaInsets)
            
            if slides.count > 0 {
                for i in 0 ..< slides.count {
                    slides[i].frame = CGRect(x: view.frame.width * CGFloat(i), y: view.safeAreaInsets.top, width: view.frame.width, height: scrollView.height - (view.safeAreaInsets.top * 2.0))
                }
            }
        } else {
            // Fallback on earlier versions
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.initUI()
        self.initDelegates()
        
        self.navigationController?.isNavigationBarHidden = true
        // Do any additional setup after loading the view.
    }
    
    // MARK: Private functions
    
    private func initUI() {
        self.slides = createSlides()
        self.setupSlideScrollView(slides: slides)
        
        self.pageControl.numberOfPages = slides.count
        self.pageControl.currentPage = 0
        
        self.continueButton.setTitleColor(AppConstants.SminqColors.white100, for: .normal)
        self.continueButton.titleLabel?.font = UIFont(name: AppConstants.Font.primaryBold, size: 14)
        self.continueButton.cornerRadius = self.continueButton.height / 2.0
        self.continueButton.backgroundColor = AppConstants.SminqColors.green200
        
        self.pageControl.isHidden = true
        
        self.view.backgroundColor = AppConstants.SminqColors.blackDark100
        
        self.continueButtonTopConstraint.constant = UIScreen.main.bounds.height / 17.7
    }
    
    private func initDelegates() {
        self.scrollView.delegate = self
    }
    
    private func setupSlideScrollView(slides : [OnBoardingSlide]) {
        scrollView.backgroundColor = AppConstants.SminqColors.blackDark100
        scrollView.contentSize.width = view.frame.width * CGFloat(slides.count)
        scrollView.isPagingEnabled = true
        
        for i in 0 ..< slides.count {
            slides[i].frame = CGRect(x: view.frame.width * CGFloat(i), y: self.getSafeAreaInsets().top, width: view.frame.width, height: scrollView.height - (self.getSafeAreaInsets().top * 2.0))
            slides[i].updateAnimationViewFrame()
            scrollView.addSubview(slides[i])
        }
    }
    
    private func createSlides() -> [OnBoardingSlide] {
        guard let slide1 = Bundle.main.loadNibNamed("OnBoardingSlide", owner: self, options: nil)?.first as? OnBoardingSlide else { return [] }
        slide1.headingLabel.text = "The dopest places nearby"
        slide1.subHeadingLabel.text = "Markk shows you the current vibe at places around you"
        slide1.imageView.image = UIImage(named: "onboarding1")
       // slide1.setupAnimationView(animationName: "onb-1")

        guard let slide2 = Bundle.main.loadNibNamed("OnBoardingSlide", owner: self, options: nil)?.first as? OnBoardingSlide else { return [] }
        slide2.headingLabel.text = "Rate places,\n win rewards"
        slide2.subHeadingLabel.text = "Use pics & videos to rate places quickly. Unlock badges and win rewards in real-life!"
        slide2.imageView.image = UIImage(named: "onboarding2")
       // slide2.setupAnimationView(animationName: "onb-2")
        
        guard let slide3 = Bundle.main.loadNibNamed("OnBoardingSlide", owner: self, options: nil)?.first as? OnBoardingSlide else { return [] }
        slide3.headingLabel.text = "Ratings last for 24hrs"
        slide3.subHeadingLabel.text = "Every day is a new day, so our ratings start fresh every day"
        slide3.imageView.image = UIImage(named: "onboarding3")
       // slide3.setupAnimationView(animationName: "onb-3")
        
        return [slide1, slide2, slide3]
    }
    
    private func setButtonText(slideIndex: Int) {
        switch slideIndex {
        case 0, 1, 2:
            self.continueButton.setTitle("CONTINUE", for: .normal)
        default:
            self.continueButton.setTitle("CONTINUE", for: .normal)
        }
    }
    
    private func trackContinueButtonEvent(onBoardingIndex: Int) {
        switch onBoardingIndex {
        case 0:
            AnalyticsHelper.tappedObContinueEvent(eventText: AnalyticsHelper.OnBoardingContinueEventText.card1, eventName: AnalyticsHelper.OnBoardingContinueEventName.card1Tapped)
        case 1:
            AnalyticsHelper.tappedObContinueEvent(eventText: AnalyticsHelper.OnBoardingContinueEventText.card2, eventName: AnalyticsHelper.OnBoardingContinueEventName.card2Tapped)
        case 2:
            AnalyticsHelper.tappedObContinueEvent(eventText: AnalyticsHelper.OnBoardingContinueEventText.card3, eventName: AnalyticsHelper.OnBoardingContinueEventName.card3Tapped)
            
        default: break
        }
    }
    
    // MARK: Button actions
    
    @IBAction func continueButtonTapped(_ sender: UIButton) {
        self.trackContinueButtonEvent(onBoardingIndex: self.pageControl.currentPage)
        
        let nextPageIndex = self.pageControl.currentPage + 1
        
        if nextPageIndex == self.slides.count {
            UserDefaultsUtility.shared.setOnBoardingDone()
            
            self.navigateToLoginViewController()
        } else {
            UIView.animate(withDuration: AppConstants.defaultAnimateDuration, animations: {
                self.scrollView.contentOffset.x = CGFloat(nextPageIndex) * self.view.width
            })
        }
    }
}

extension OnBoardingViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let pageIndex = round(scrollView.contentOffset.x/view.frame.width)
        self.pageControl.currentPage = Int(pageIndex)
        
        let maximumHorizontalOffset: CGFloat = scrollView.contentSize.width - scrollView.frame.width
        let currentHorizontalOffset: CGFloat = scrollView.contentOffset.x
        
        // vertical
        let maximumVerticalOffset: CGFloat = scrollView.contentSize.height - scrollView.frame.height
        let currentVerticalOffset: CGFloat = scrollView.contentOffset.y
        
        let percentageHorizontalOffset: CGFloat = currentHorizontalOffset / maximumHorizontalOffset
        let percentageVerticalOffset: CGFloat = currentVerticalOffset / maximumVerticalOffset
        
        let percentOffset: CGPoint = CGPoint(x: percentageHorizontalOffset, y: percentageVerticalOffset)
        
        print(percentOffset)
        
        if (percentOffset.x >= 0 && percentOffset.x < 0.50) {
            slides[0].animationView.transform = CGAffineTransform(scaleX: (0.50 - percentOffset.x) / 0.50, y: (0.50 - percentOffset.x) / 0.50)
            slides[1].animationView.transform = CGAffineTransform(scaleX: percentOffset.x / 0.40, y: percentOffset.x / 0.40)
            
//            if percentOffset.x < 0.45 {
                slides[1].animationView.currentProgress = percentOffset.x
//            }
            
            slides[0].headingLabel.alpha = (0.30 - percentOffset.x) / 0.30
            slides[1].headingLabel.alpha = percentOffset.x / 0.30
            
            slides[0].subHeadingLabel.alpha = (0.30 - percentOffset.x) / 0.30
            slides[1].subHeadingLabel.alpha = percentOffset.x / 0.30
            
        } else if (percentOffset.x > 0.50 && percentOffset.x <= 1) {
            
            slides[1].animationView.transform = CGAffineTransform(scaleX: (1 - percentOffset.x) / 0.40, y: (1 - percentOffset.x) / 0.40)
            slides[2].animationView.transform = CGAffineTransform(scaleX: percentOffset.x, y: percentOffset.x)
            
//            if percentOffset.x > 0.55 {
                slides[1].animationView.currentProgress = percentOffset.x
//            }
            
            slides[1].headingLabel.alpha = (0.80 - percentOffset.x) / 0.80
            slides[2].headingLabel.alpha = percentOffset.x
            
            slides[1].subHeadingLabel.alpha = (0.80 - percentOffset.x) / 0.80
            slides[2].subHeadingLabel.alpha = percentOffset.x
            
        }
        
        if scrollView.contentOffset.x == (view.frame.width * CGFloat(pageControl.currentPage)) {
            // Note: When horizontal scroll is snapped completely
            
            switch self.pageControl.currentPage {
            case 0:
                UIView.animate(withDuration: AppConstants.defaultAnimateDuration) {
                    self.slides[0].headingLabel.alpha = 1
                    self.slides[0].subHeadingLabel.alpha = 1
                }

            case 1:
                self.slides[1].animationView.transform = CGAffineTransform(scaleX: percentOffset.x / 0.40, y: percentOffset.x / 0.40)
                self.slides[1].animationView.play(toProgress: 0.5)
                
                UIView.animate(withDuration: AppConstants.defaultAnimateDuration) {
                    self.slides[1].headingLabel.alpha = 1
                    self.slides[1].subHeadingLabel.alpha = 1
                }
                
            case 2:
                UIView.animate(withDuration: AppConstants.defaultAnimateDuration) {
                    self.slides[2].headingLabel.alpha = 1
                    self.slides[2].subHeadingLabel.alpha = 1
                }
            default:
                break
            }
            
            if self.pageControl.currentPage == 1 {
                self.isSlide1Animated = true
            }

            UIView.animate(withDuration: AppConstants.defaultAnimateDuration, animations: {
                self.setButtonText(slideIndex: self.pageControl.currentPage)
            }, completion: nil)
        }
    }
}
