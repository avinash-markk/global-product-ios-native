//
//  ExtensionViewController.swift
//  Sminq
//
//  Created by Bjorn Mascarenhas on 11/04/18.
//  Copyright © 2018 Suraj Sagare. All rights reserved.
//

import UIKit
import Firebase

extension UIViewController {
    
    @objc func catchIt(_ userInfo: Notification) {
        if userInfo.userInfo != nil {
            let cachedUserInfo = UserDefaults.standard.value(forKey: AppConstants.UserDefaultsKeys.notificationUserInfo)
            
            if let tempNotificationType = userInfo.userInfo!["type"], cachedUserInfo != nil {
                let notificationType = tempNotificationType as! String
                
                if notificationType.lowercased() == AppConstants.newReply || notificationType.lowercased() == AppConstants.newVote {
                    let feedItemId = userInfo.userInfo!["feedItemId"] as? String
                    let feedItemUserId = userInfo.userInfo!["feedItemUserId"] as? String
                    let placeString = userInfo.userInfo!["place"] as? String
                    guard let place = Helper().stringToPlaceConversion(place: placeString!) else { return }
                    
                    let storyViewManager = StoryViewManager.init(screen: AppConstants.screenNames.notifications, feedItemId: feedItemId)
                    let storyViewController = StoryViewController.init(storyViewManager: storyViewManager)
                    
                    var tempStoryList: [StoryData] = []
                    let storyData = StoryData.init(userId: feedItemUserId, placeId: nil, placeDetails: place, archived: false, posts: [], feedViewModel: nil)
                    tempStoryList.append(storyData)
                    
                    let storyViewModel = StoryViewModel.init(screen: AppConstants.screenNames.storyView, storyList: tempStoryList)!
                    
                    storyViewController.storyViewModel = storyViewModel
                    
                    storyViewController.openWithStoryCommentsView = notificationType.lowercased() == AppConstants.newReply
                    
                    let navController = UINavigationController(rootViewController: storyViewController)
                    navController.modalPresentationStyle = .overFullScreen
                    navController.modalPresentationCapturesStatusBarAppearance = true
                    
                    AppConstants.appDelegate.window?.rootViewController?.present(navController, animated:true, completion: {
                        UserDefaults.standard.removeObject(forKey: AppConstants.UserDefaultsKeys.notificationUserInfo)
                    })
                } else  if notificationType.lowercased() == AppConstants.newFeed {
                    if userInfo.userInfo!["place"] != nil, let place = userInfo.userInfo?["place"] as? String {
                        if let placeFeedViewController = AppConstants.storyboard.instantiateViewController(withIdentifier: "PlaceFeedViewController") as? PlaceFeedViewController {
                            if let place = Helper().stringToPlaceConversion(place: place) {
                                placeFeedViewController.place = place
                            }
                            
                            let navController = UINavigationController(rootViewController: placeFeedViewController)
                            AppConstants.appDelegate.window?.rootViewController?.present(navController, animated: true, completion: {
                                UserDefaults.standard.removeObject(forKey: AppConstants.UserDefaultsKeys.notificationUserInfo)
                            })
                        }
                    }
                } else if notificationType.lowercased() == AppConstants.newBadge {
                    if let viewController = AppConstants.storyboard.instantiateViewController(withIdentifier: "PostSuccessViewController") as? PostSuccessViewController {
                        
                        if let userInfo = userInfo.userInfo {
                            print("the user info: \(userInfo)")
                            guard let badgeString = userInfo["badge"] as? String else { return }
                            guard let badge = Helper().stringToBadgeConversion(badgeString: badgeString) else { return }
                            viewController.isBadgeUnlocked = true
                            viewController.newBadge = badge
                            let navController = UINavigationController(rootViewController: viewController)
                            AppConstants.appDelegate.window?.rootViewController?.present(navController, animated: true, completion: {
                                UserDefaults.standard.removeObject(forKey: AppConstants.UserDefaultsKeys.notificationUserInfo)
                            })
                        }
                    
                    }
                }
            }
        }
    }
    
    func triggerAlert(_ title: String) {
        let alert = UIAlertController(title: "Alert", message: title, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func hideTabBar() {
        AppConstants.appDelegate.tabBarViewController?.tabBar.isHidden = true
        AppConstants.appDelegate.tabBarViewController?.cameraButton.isHidden = true
        AppConstants.appDelegate.tabBarViewController!.asyncSubview.isHidden = true
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: AppConstants.AppNotificationNames.hideBottomBar), object: nil, userInfo: nil)
    }
    
    func showTabBar() {
        AppConstants.appDelegate.tabBarViewController?.tabBar.isHidden = false
        AppConstants.appDelegate.tabBarViewController?.cameraButton.isHidden = false
        
        if UserDefaults.standard.bool(forKey: "UploadInProgress") == true {
            AppConstants.appDelegate.tabBarViewController!.asyncSubview.isHidden = false
        } else {
            AppConstants.appDelegate.tabBarViewController!.asyncSubview.isHidden = true
        }
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: AppConstants.AppNotificationNames.showBottomBar), object: nil, userInfo: nil)
    }

    func getTopSafeArea() -> CGFloat {
        var topSafeArea: CGFloat
        
        if #available(iOS 11.0, *) {
            topSafeArea = self.view.safeAreaInsets.top
        } else {
            // Fallback on earlier versions
            topSafeArea = topLayoutGuide.length
        }
        
        return topSafeArea
    }
    
    func getBottomSafeArea() -> CGFloat {
        var bottomSafeArea: CGFloat
        
        if #available(iOS 11.0, *) {
            bottomSafeArea = self.view.safeAreaInsets.bottom
        } else {
            // Fallback on earlier versions
            bottomSafeArea = bottomLayoutGuide.length
        }
        
        return bottomSafeArea
    }
    
    func getSafeAreaInsets() -> UIEdgeInsets {
        if let window = UIApplication.shared.keyWindow {
            if #available(iOS 11.0, *) {
                return window.safeAreaInsets
            }
        } else {
            if #available(iOS 11.0, *) {
                return view.safeAreaInsets
            }
        }
        
        return UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
    }
    
    func hasNotch() -> Bool {
        var bottomSafeArea: CGFloat
        
        if #available(iOS 11.0, *) {
            bottomSafeArea = self.view.safeAreaInsets.bottom
        } else {
            // Fallback on earlier versions
            bottomSafeArea = bottomLayoutGuide.length
        }
        
        return bottomSafeArea > 0
    }
    
    func presentAlertSheet(title: String?, message: String?, options: [UIAlertAction], controllerStyle: UIAlertControllerStyle, onWindow: Bool = false, completion: @escaping (UIAlertAction) -> Void) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: controllerStyle)
        
        for (_, option) in options.enumerated() {
            alertController.addAction(UIAlertAction.init(title: option.title, style: option.style, handler: { (action) in
                completion(option)
            }))
        }
        
        if !onWindow {
            self.present(alertController, animated: true, completion: nil)
        } else {
            let alertWindow = UIWindow(frame: UIScreen.main.bounds)
            alertWindow.rootViewController = UIViewController()
            alertWindow.windowLevel = UIWindowLevelAlert + 1;
            alertWindow.makeKeyAndVisible()
            alertWindow.rootViewController?.present(alertController, animated: true, completion: nil)
        }
    }
    
    func presentShareActivity(text: String, completion: @escaping(Error?) -> Void) {
        let activityViewController = UIActivityViewController(activityItems: [text], applicationActivities: nil)
        activityViewController.excludedActivityTypes = [.mail]
        activityViewController.popoverPresentationController?.sourceView = self.view
        activityViewController.completionWithItemsHandler = { [weak self] (activity, success, items, error) in
            guard let _ = self else { return }
            completion(error)
            
        }
        
        self.present(activityViewController, animated: true, completion: nil)
    }
    
}

extension UIViewController {
    
    func showToast(message: String) {
        let toastLabel = UILabel(frame: CGRect(x: self.view.frame.size.width/2 - 75, y: self.view.frame.size.height/2-20, width: 150, height: 40))
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = .center
        toastLabel.font = UIFont(name: AppConstants.Font.primaryRegular, size: 10.0)
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 4.0, delay: 0.1, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
    
}
