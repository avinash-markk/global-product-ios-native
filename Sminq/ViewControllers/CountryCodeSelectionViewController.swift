//
//  CountryCodeSelectionViewController.swift
//  Sminq
//
//  Created by Pushkar Deshmukh on 25/06/18.
//  Copyright © 2018 Suraj Sagare. All rights reserved.
//

import UIKit
import Gloss

class CountryCodeSelectionViewController: UIViewController, UIGestureRecognizerDelegate {

    var countryList: [CountryCode]? = []
    var countryListTemp: [CountryCode]? = []
    var navigationTitleLabel = UILabel()
    var presentingController: Bool? = false
    
    @IBOutlet weak var countryListTableView: UITableView!
    
    @IBOutlet weak var searchCountryTextField: DesignableUITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.initUI()
        
        getCountryList()
        
        self.countryListTableView.register(UINib(nibName: "CountryCodeSelectionTableViewCell", bundle: nil), forCellReuseIdentifier: "CountryCodeSelectionTableViewCell")
        
        searchCountryTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        setupNavigationBar()
        setNeedsStatusBarAppearanceUpdate()
    }
    
    override public var prefersStatusBarHidden: Bool {
        return false
    }
    
    override public var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func initUI() {
        self.view.backgroundColor = AppConstants.SminqColors.blackDark100
        
        self.countryListTableView.backgroundColor = AppConstants.SminqColors.blackDark100
        
        self.searchCountryTextField.backgroundColor = AppConstants.SminqColors.blackLight100
        self.searchCountryTextField.clipsToBounds = true
        self.searchCountryTextField.borderColor = AppConstants.SminqColors.white100.withAlphaComponent(0.1)
        self.searchCountryTextField.borderWidth = 1
        self.searchCountryTextField.cornerRadius = 8
        self.searchCountryTextField.font = UIFont(name: AppConstants.Font.primaryRegular, size: 16)
        self.searchCountryTextField.textColor = AppConstants.SminqColors.white100
        
    }

    @objc func textFieldDidChange(_ textField: UITextField) {
        filterCountries()
    }

    func setupNavigationBar() {
        // Setting Button
        navigationTitleLabel = UILabel(frame: CGRect(x: 44, y: 0, width: self.view.frame.width - (88), height: 44))
        
        if (navigationController?.viewControllers.count)! > 1 || self.presentingController! {
            let backButton = UIButton(frame: CGRect(x: 0, y: 0, width: 44, height: 44))
            let upArrow = UIImage(named: "iconBack24White100")?.withRenderingMode(.alwaysTemplate)
            backButton.setImage(upArrow, for: UIControlState())
            backButton.tintColor = UIColor.white
            backButton.imageEdgeInsets = UIEdgeInsetsMake(0, -25, 0, 2)
            backButton.addTarget(self, action: #selector(tappedBackButton), for: .touchUpInside)
            navigationItem.leftBarButtonItems = [.init(customView: backButton)]
            
            self.navigationController?.interactivePopGestureRecognizer?.delegate = self
            navigationTitleLabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.view.frame.width - (140), height: 44))
        }
        
        // Navbar title label
        
        navigationController?.navigationBar.barTintColor = AppConstants.SminqColors.blackLight100
        navigationController?.navigationBar.isTranslucent = false
        
        // Remove the background color.
        navigationController?.navigationBar.setBackgroundImage(UIColor.clear.as1ptImage(), for: .default)
        
        // Set the shadow color.
        navigationController?.navigationBar.shadowImage = AppConstants.SminqColors.white100.withAlphaComponent(0.1).as1ptImage()
        
        navigationTitleLabel.font = UIFont(name: AppConstants.Font.primaryRegular, size: 20.0)
        navigationTitleLabel.textColor = UIColor.white
        navigationTitleLabel.textAlignment = NSTextAlignment.center
        navigationItem.titleView = navigationTitleLabel
        navigationTitleLabel.text = "Select Country Code"
        
    }
    
    @objc func tappedBackButton() {
        self.navigationController?.isNavigationBarHidden = false
        if self.presentingController! {
            self.dismiss(animated: true, completion: nil)
        } else {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    // MARK: Private functions
    
    func filterCountries() {
        if let text = searchCountryTextField.text {
            
            let searchText = text.lowercased()
            
            //            print(self.searchText)
            
            if (searchText.isEmpty) {
                // should return all data and return
                self.countryList = self.countryListTemp
                self.countryListTableView.reloadData()
                
            } else {
                self.countryList = self.countryListTemp?.filter { country in
                    if country.name.lowercased().range(of:searchText) != nil {
                        return true
                    }
                    return false
                }
                
                self.countryListTableView.reloadData()
            }
        }
    }
    
    func getCountryList() {
        if let path = Bundle.main.path(forResource: "countries", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
                
                self.countryList = [CountryCode].from(jsonArray: jsonResult as! [JSON])!
                self.countryListTemp = self.countryList
                
            } catch {
                LogUtil.error(error)
            }
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension CountryCodeSelectionViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (self.countryList?.count)!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 48
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let country: CountryCode = self.countryList![indexPath.row]
        
        let userInfo: [AnyHashable: Any] = ["country": country.name, "code": country.dial_code, "flag": country.flag]
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "countrySelected"), object: nil, userInfo: userInfo)
        
        self.navigationController?.popViewController(animated: true)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CountryCodeSelectionTableViewCell", for: indexPath) as! CountryCodeSelectionTableViewCell
        
        let country: CountryCode = self.countryList![indexPath.row]
        cell.countryCodeLabel.text = country.dial_code
        cell.countryNameLabel.text = country.name
        
        return cell
    }

}

// MARK: - Textfield delegates

extension CountryCodeSelectionViewController:  UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
}
