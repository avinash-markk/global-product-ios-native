//
//  LocationPermissionView.swift
//  Sminq
//
//  Created by Avinash Thakur on 07/03/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import UIKit

protocol LocationPermissionViewDelegate: NSObjectProtocol {
    func askLocationPermissionView()
    func removeLocationPermissionView()
}

class LocationPermissionView: UIView {
    
    var contentView: UIView!
    weak var delegate: LocationPermissionViewDelegate?
    @IBOutlet weak var locationPermissionButton: UIButton!
    @IBOutlet weak var locationTitleLabel: UILabel!
    @IBOutlet weak var locationSubTitleLabel: UILabel!
    
    @IBOutlet weak var bottomViewBottomConstraint: NSLayoutConstraint!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        contentView.frame = self.frame
    }
    
    func xibSetup() {
        backgroundColor = UIColor.clear
        contentView = loadNib()
        contentView.frame = bounds
        addSubview(contentView)
        self.initUI()
        self.defaultUIForLcationPermission()
    }
    
    func initUI() {
        locationPermissionButton.backgroundColor = AppConstants.SminqColors.green200
        locationPermissionButton.titleLabel?.font = UIFont(name: AppConstants.Font.primaryBold, size: 14)
        locationPermissionButton.setTitleColor(AppConstants.SminqColors.white100, for: .normal)
        
        locationTitleLabel.font = UIFont(name: AppConstants.Font.primaryBold, size: 14)
        locationTitleLabel.textColor = AppConstants.SminqColors.white100
        
        locationSubTitleLabel.font = UIFont(name: AppConstants.Font.primaryRegular, size: 14)
        locationSubTitleLabel.textColor = AppConstants.SminqColors.white100.withAlphaComponent(0.75)
        
        self.bottomViewBottomConstraint.constant = 0
    }
    
    func updateUIForPermissionDenied() {
        locationPermissionButton.setTitle("ENABLE LOCATION ACCESS", for: .normal)
        locationTitleLabel.text = "MARKK NEEDS YOUR LOCATION"
        locationSubTitleLabel.text = "Markk cannot show you places nearby without knowing your location."
    }
    
    func defaultUIForLcationPermission() {
        locationPermissionButton.setTitle("ENABLE LOCATION ACCESS", for: .normal)
        locationTitleLabel.text = "MARKK YOUR TERRITORY"
        locationSubTitleLabel.text = "Share and see the Dopest places around you"
    }
    
    func updateUIForLocationFailure() {
        locationPermissionButton.setTitle("RETRY", for: .normal)
        locationTitleLabel.text = "UNABLE TO FIND YOUR LOCATION..."
        locationSubTitleLabel.text = "Markk shows you what’s happening around you."
    }
    
    func openLocationSetting () {
        if let url = URL(string: UIApplicationOpenSettingsURLString) {
            // NOTE: If general location settings are enabled then open location settings for the app
            UIApplication.shared.open(url)
        }
    }
    
    func setBottomViewBottomConstraint(bottomConstraintConstant: CGFloat) {
        self.bottomViewBottomConstraint.constant = bottomConstraintConstant
    }
    
    @IBAction func locationPermissionButtonTapped(_ sender: UIButton) {
        if locationTitleLabel.text == "MARKK YOUR TERRITORY" {
            self.delegate?.askLocationPermissionView()
        } else if locationTitleLabel.text == "MARKK NEEDS YOUR LOCATION" {
            self.openLocationSetting()
        }
    }
    
}
