//
//  AddRatingView.swift
//  Sminq
//
//  Created by Avinash Thakur on 06/03/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import UIKit


class AddRatingView: UIView {
    
    var contentView: UIView!
    @IBOutlet weak var ratingsSubview: UIView!
    @IBOutlet weak var ratingsLabel: UILabel!
   
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        contentView.frame = self.frame
    }
    
    func xibSetup() {
        backgroundColor = UIColor.clear
        contentView = loadNib()
        contentView.frame = bounds
        addSubview(contentView)
        
        ratingsLabel.font = UIFont(name: AppConstants.Font.primaryRegular, size: 16)
        ratingsLabel.textColor = AppConstants.SminqColors.white100
        ratingsLabel.text = "To add a live rating, \n tap on the camera icon."
    }
    
}
