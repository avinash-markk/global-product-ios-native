//
//  EnlargedDPView.swift
//  Markk
//
//  Created by Avinash Thakur on 26/08/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import UIKit

class EnlargedDPView: UIView {

    var contentView: UIView!
    var profileImageView = UIImageView()
    fileprivate let panGestureOffset: CGFloat = 70.0
    @IBOutlet weak var closeButton: UIButton!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupUI()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        contentView.frame = self.frame
    }
    
    fileprivate func setupUI() {
        contentView = loadNib()
        contentView.frame = bounds
        closeButton.isHidden = true
        self.backgroundColor = UIColor.rgb(red: 8/255, green: 7/255, blue: 7/255)
        self.setDPToDefaultPosition()
        profileImageView.clipsToBounds = true
        contentView.addSubview(profileImageView)
        addSubview(contentView)
        self.addGestures()
    }
    
    func setDPToDefaultPosition() {
        profileImageView.frame = CGRect(x: self.center.x - 40, y: 20, width: 80, height: 80)
        profileImageView.layer.cornerRadius = 40.0
    }
    
    func addGestures() {
        self.addGestureRecognizer(UIPanGestureRecognizer(target: self, action: #selector(handlePan)))
        self.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleOverlayViewTap)))
        
    }
    
    @objc fileprivate func handlePan(gesture: UIPanGestureRecognizer) {
        switch gesture.state {
        case .began: handlePanBeganState(gesture: gesture)
        case .changed: handlePanChangedState(gesture: gesture)
        case .ended: handlePanEndedState(gesture: gesture)
        default: break
        }
    }
    
    fileprivate func handlePanBeganState(gesture: UIPanGestureRecognizer) {
    }
    
    fileprivate func handlePanChangedState(gesture: UIPanGestureRecognizer) {
        let translation = gesture.translation(in: nil)
        self.transform = CGAffineTransform(translationX: 0, y: translation.y)
    }
    
    fileprivate func handlePanEndedState(gesture: UIPanGestureRecognizer) {
        let translation = gesture.translation(in: nil)
        if translation.y < 0 {
            if translation.y < -panGestureOffset {
               animateOutAndRemove()
            } else {
                self.transform = .identity
            }
        } else if translation.y > 0 {
            if translation.y > panGestureOffset {
                animateOutAndRemove()
            } else {
                self.transform = .identity
            }
        }
    }
    
    public func animateInView(imageUrl: String) {
        profileImageView.sd_setImage(with: URL(string: imageUrl), completed: nil)
        self.setDPToDefaultPosition()
        self.isHidden = false
        UIView.animate(withDuration: AppConstants.defaultAnimateDuration, animations: {
            self.profileImageView.frame = CGRect(x: self.center.x - 160, y: self.center.y - 160, width: 320, height: 320)
            self.profileImageView.layer.cornerRadius = 160.0
        }, completion: { value in
            self.closeButton.isHidden = false
        })
    }
    
    public func animateOutAndRemove() {
        UIView.animate(withDuration: AppConstants.defaultAnimateDuration, animations: {
            self.transform = .identity
            self.setDPToDefaultPosition()
        }) { (finished) in
            if finished {
                self.isHidden = true
                self.removeFromSuperview()
            }
        }
    }
    
    @IBAction func closeButtonTapped(_sender: UIButton) {
        self.animateOutAndRemove()
    }
    
    @objc fileprivate func handleOverlayViewTap() {
        self.animateOutAndRemove()
    }
    
}
