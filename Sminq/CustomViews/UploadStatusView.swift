//  UploadStatusView.swift
//  Sminq
//
//  Created by Bjorn Mascarenhas on 07/09/18.
//  Copyright © 2018 Suraj Sagare. All rights reserved.
//

import UIKit

protocol UploadStatusViewDelegate: NSObjectProtocol {
    func viewUploadsButtonTapped(_ sender: UploadStatusView)
}

class UploadStatusView: UIView {
    var contentView: UIView!
    
    @IBOutlet weak var uploadProgressView: UIView!
  
    @IBOutlet weak var uploadStatus: UILabel!
    @IBOutlet weak var uploadStoryImage: UIImageView!
    @IBOutlet weak var viewUploadsButton: UIButton!
    
    var progressView = UIProgressView()
    weak var delegate: UploadStatusViewDelegate?
   
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
        
    override func layoutSubviews() {
        super.layoutSubviews()
        contentView.frame = self.frame
        
        progressView = UIProgressView(progressViewStyle: .bar)
        progressView.setProgress(0.0, animated: false)
        progressView.tintColor = AppConstants.SminqColors.green100
        progressView.frame = CGRect(x: 0, y: 0.0, width: self.contentView.frame.width, height: 2.0)
        progressView.layer.cornerRadius = 2
        progressView.layer.masksToBounds = true
      
       self.uploadProgressView.addSubview(progressView)
    }
    
    func xibSetup() {
        backgroundColor = UIColor.clear
        contentView = loadNib()
        contentView.frame = bounds
        addSubview(contentView)
        
        uploadStatus.font = UIFont(name: AppConstants.Font.primaryRegular, size: 14)
        uploadStatus.textColor = AppConstants.SminqColors.white100
        
        viewUploadsButton.titleLabel?.font = UIFont(name: AppConstants.Font.primaryBold, size: 14)
        viewUploadsButton.setTitleColor(AppConstants.SminqColors.white100.withAlphaComponent(0.75), for: .normal)
    }
    
    @IBAction func viewUploadsBtnPressed(_ sender: UIButton) {
       delegate?.viewUploadsButtonTapped(self)
    }
    
}
