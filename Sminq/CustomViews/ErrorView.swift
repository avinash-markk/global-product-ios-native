//
//  ErrorView.swift
//  Sminq
//
//  Created by Avinash Thakur on 14/03/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import UIKit

protocol ErrorViewDelegate: NSObjectProtocol {
    func errorViewTapped()
}

class ErrorView: UIView {

    var contentView: UIView!
    @IBOutlet weak var errorHeaderLabel: UILabel!
    @IBOutlet weak var errorMessageLabel: UILabel!
    
    @IBOutlet weak var stackView: UIStackView!
    
    @IBOutlet weak var stackViewTopConstraint: NSLayoutConstraint!
    weak var delegate: ErrorViewDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        contentView.frame = self.frame
    }
    
    func xibSetup() {
        backgroundColor = UIColor.clear
        
        contentView = loadNib()
        contentView.frame = bounds
        contentView.backgroundColor = AppConstants.SminqColors.red100
        addSubview(contentView)
        
        errorHeaderLabel.font = UIFont(name: AppConstants.Font.primaryBold, size: 12)
        errorHeaderLabel.textColor = AppConstants.SminqColors.white100
       
        errorMessageLabel.font = UIFont(name: AppConstants.Font.primaryRegular, size: 12)
        errorMessageLabel.textColor = AppConstants.SminqColors.white100
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleTap))
        self.addGestureRecognizer(tap)
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        self.delegate?.errorViewTapped()
    }
    
    func setStackViewTopConstraint(top: CGFloat) {
        self.stackViewTopConstraint.constant = top
    }
    
    func animateInViewWithMessage(_ header: String, _ subHeader: String, autoDismiss: Bool = false) {
        self.isHidden = false
        self.errorHeaderLabel.text = header.uppercased()
        self.errorMessageLabel.text = subHeader
        
        if header.isEmpty || header == "" {
            self.errorHeaderLabel.isHidden = true
        } else {
            self.errorHeaderLabel.isHidden = false
        }
        
        if subHeader.isEmpty || subHeader == "" {
            self.errorMessageLabel.isHidden = true
        } else {
            self.errorMessageLabel.isHidden = false
        }
        
        self.frame.origin.y = 0
        
        UIView.animate(withDuration: 1, animations: {
            self.alpha = 1
        }, completion: { done in
            self.isHidden = false
        })
        
        if autoDismiss {
            DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
                self.isHidden = true
                self.alpha = 0
            }
        }
    }
    
    func animateOutView() {
        UIView.animate(withDuration: 1, animations: {
            self.alpha = 0
        }, completion: { done in
            self.isHidden = true
        })
    }
    
    func animateInViewWithError(errorObject: ErrorObject, autoDismiss: Bool = false) {
        animateInViewWithMessage(errorObject.errorTitle, errorObject.errorDescription, autoDismiss: autoDismiss)
    }

}
