//
//  TabsMenuView.swift
//  Markk
//
//  Created by Avinash Thakur on 12/06/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import UIKit

protocol TabsMenuDelegate {
    func menuTabBarDidSelectItemAt(menu: TabsMenuView, index: Int)
}

class TabsMenuView: UIView {

    var tabsMenuDelegate: TabsMenuDelegate?
    
    lazy var tabsCollectionView: UICollectionView = {
        let flowLayout = UICollectionViewFlowLayout()
        let collectionView = UICollectionView.init(frame: CGRect.zero, collectionViewLayout: flowLayout)
        collectionView.showsHorizontalScrollIndicator = false
        flowLayout.scrollDirection = .horizontal
        collectionView.backgroundColor = AppConstants.SminqColors.blackDark100
        collectionView.delegate = self
        collectionView.dataSource = self
        return collectionView
    }()
    
    var tabsArray: [String] = [] {
        didSet {
            self.tabsCollectionView.reloadData()
        }
    }
    
    var isSizeToFitCellsNeeded: Bool = false {
        didSet {
            self.tabsCollectionView.reloadData()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        intializeTabsMenu()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        intializeTabsMenu()
    }
    
    func intializeTabsMenu() {
        tabsCollectionView.register(TabsCell.self, forCellWithReuseIdentifier: "TabsCell")
        tabsCollectionView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(tabsCollectionView)
        tabsCollectionView.snp.makeConstraints { (make) in
            make.top.left.right.bottom.equalTo(self)
        }
    }
    
}

extension TabsMenuView: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return tabsArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TabsCell", for: indexPath) as? TabsCell {
            cell.titleLabel.text = tabsArray[indexPath.item]
            cell.backgroundColor = AppConstants.SminqColors.blackDark100
            return cell
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if isSizeToFitCellsNeeded {
            let itemSize = CGSize.init(width: 500, height: self.frame.height)
            let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
            let str = tabsArray[indexPath.item]
            let estimatedRect = NSString.init(string: str).boundingRect(with: itemSize, options: options, attributes: [NSAttributedStringKey.font : UIFont.boldSystemFont(ofSize: 12)], context: nil)
            
            return CGSize.init(width: estimatedRect.size.width, height: self.frame.height)
        }
        
        return CGSize.init(width: (self.frame.width - 10)/CGFloat(tabsArray.count), height: self.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let index = Int(indexPath.item)
        tabsMenuDelegate?.menuTabBarDidSelectItemAt(menu: self, index: index)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(0, 5, 0, 5)
    }
    
}

class TabsCell: UICollectionViewCell {
    
    let titleLabel: UILabel = {
        let lbl = UILabel()
        lbl.font = UIFont.init(name: AppConstants.Font.primaryBold, size: 12)
        lbl.textColor = AppConstants.SminqColors.white100.withAlphaComponent(0.5)
        return lbl
    }()
    
    let bottomView: UIView = {
        let view = UIView()
        view.backgroundColor = AppConstants.SminqColors.white100.withAlphaComponent(0.1)
        return view
    }()
    
    var indicatorView: UIView!
    
    override var isSelected: Bool {
        
        didSet {
            UIView.animate(withDuration: 0.20) {
                self.indicatorView.backgroundColor = self.isSelected ? AppConstants.SminqColors.white100 : .clear
                self.titleLabel.textColor = self.isSelected ? AppConstants.SminqColors.white100 : AppConstants.SminqColors.white100.withAlphaComponent(0.5)
                self.layoutIfNeeded()
            }
        }
        
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        titleLabel.textAlignment = .center
        addSubview(titleLabel)
       
        titleLabel.snp.makeConstraints { (make) in
            make.left.right.equalTo(self)
            make.height.equalTo(20)
            make.top.equalTo(10)
        }
        
        addSubview(bottomView)
        bottomView.snp.makeConstraints { make in
            make.left.right.equalTo(0)
            make.top.equalTo(39)
            make.height.equalTo(1)
        }
        setupIndicatorView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        titleLabel.text = ""
    }
    
    func setupIndicatorView() {
        indicatorView = UIView()
        addSubview(indicatorView)

        indicatorView.snp.makeConstraints { (make) in
            make.left.right.bottom.equalTo(self)
            make.top.equalTo(36)
            make.height.equalTo(4)
        }
    }
  
}
