//
//  ClaimRewardModalView.swift
//  Markk
//
//  Created by Avinash Thakur on 22/07/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import UIKit

protocol ClaimRewardModalViewDelegate: NSObjectProtocol {
    func didEnterInvalidEmail(email: String)
    func claimReward(forEmail: String)
}

class ClaimRewardModalView: UIView {

    var contentView: UIView!
    public weak var delegate: ClaimRewardModalViewDelegate?
    @IBOutlet weak var overlayView: UIView!
    @IBOutlet weak var rewardContentView: UIView!
    
    @IBOutlet weak var emailButton: UIButton!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var contentLabel: UILabel!
    
    @IBOutlet weak var emailButtonBottomConstraint: NSLayoutConstraint!
    var userEmail: String = ""
    var userScreen: String = ""
    fileprivate let panGestureOffset: CGFloat = 70.0

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupUI()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        contentView.frame = self.frame
    }
    
    @IBAction func emailButtonTapped(_ sender: UIButton) {
        if emailButton.title(for: .normal) == "CONFIRM EMAIL" {
            self.validateEmail(forEvent: AnalyticsHelper.ClaimRewardEmailActionEventName.confirmEmailTapped.rawValue)
        } else {
            self.validateEmail(forEvent: AnalyticsHelper.ClaimRewardEmailActionEventName.okayEmailTapped.rawValue)
        }
    }
    
    func validateEmail(forEvent: String) {
        guard let email = emailTextField.text else { return }
        if !Helper().isValidEmail(email: email) {
            delegate?.didEnterInvalidEmail(email: email)
            return
        }
        self.endEditing(true)
        AnalyticsHelper.claimRewardEmailActionAnalyticEvent(screen: userScreen, email: email, eventName: forEvent)
        self.delegate?.claimReward(forEmail: email)
        animateOutAndRemove()
    }
    
    fileprivate func setupUI() {
        contentView = loadNib()
        contentView.frame = bounds
        addSubview(contentView)
        self.setupUIComponents()
        self.addGestures()
        self.setupNotificationObservers()
    }
    
    fileprivate func setupUIComponents() {
        emailButton.backgroundColor = AppConstants.SminqColors.green200
        emailButton.setTitleColor(AppConstants.SminqColors.white100, for: .normal)
        emailButton.titleLabel?.font = UIFont.init(name: AppConstants.Font.primaryBold, size: 14)
        emailButton.setTitle("CONFIRM EMAIL", for: .normal)
        emailButtonBottomConstraint.constant = self.getWindowSafeAreaInsets().bottom + 16
        overlayView.backgroundColor = AppConstants.SminqColors.shadow.withAlphaComponent(0.75)
        self.setupEmailTextFieldUI()
    }
    
    func setupEmailTextFieldUI() {
        emailTextField.textColor = AppConstants.SminqColors.white100
        emailTextField.font = UIFont(name: AppConstants.Font.primaryRegular, size: 16)
        emailTextField.backgroundColor = AppConstants.SminqColors.white100.withAlphaComponent(0.1)
        emailTextField.textAlignment = .center
        emailTextField.layer.borderColor = AppConstants.SminqColors.white100.withAlphaComponent(0.1).cgColor
        emailTextField.layer.borderWidth = 1
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .center
        emailTextField.attributedPlaceholder = NSAttributedString(string: "Enter email ID...", attributes: [
            NSAttributedStringKey.foregroundColor: AppConstants.SminqColors.white100.withAlphaComponent(0.5),
            NSAttributedStringKey.paragraphStyle: paragraphStyle
            ])
    }
    
    func addGestures() {
        addGestureRecognizer(UIPanGestureRecognizer(target: self, action: #selector(handlePan)))
        overlayView.isUserInteractionEnabled = true
        overlayView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleOverlayViewTap)))
         rewardContentView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleMainViewTap)))
    }
    
    fileprivate func setupNotificationObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardHide(notification:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    @objc fileprivate func handlePan(gesture: UIPanGestureRecognizer) {
        switch gesture.state {
        case .began: handlePanBeganState(gesture: gesture)
        case .changed: handlePanChangedState(gesture: gesture)
        case .ended: handlePanEndedState(gesture: gesture)
        default: break
        }
    }
    
    fileprivate func handlePanBeganState(gesture: UIPanGestureRecognizer) {
        endEditing(true)
    }
    
    fileprivate func handlePanChangedState(gesture: UIPanGestureRecognizer) {
        let translation = gesture.translation(in: nil)
        
        if translation.y > 0 {
            print("Changing y:", translation.y)
            self.transform = CGAffineTransform(translationX: 0, y: translation.y)
        }
    }
    
    fileprivate func handlePanEndedState(gesture: UIPanGestureRecognizer) {
        let translation = gesture.translation(in: nil)
        if translation.y > panGestureOffset {
            animateOutAndRemove()
        } else {
            self.transform = .identity
        }
    }
    
    public func animateInView(email: String, screen: String) {
        emailTextField.text = email
        if email == "" {
            emailTextField.isUserInteractionEnabled = true
            emailButton.setTitle("CONFIRM EMAIL", for: .normal)
        } else {
            emailTextField.isUserInteractionEnabled = false
            emailButton.setTitle("OKAY", for: .normal)
        }
        userScreen = screen
        self.frame = CGRect(x: 0, y: UIScreen.main.bounds.height, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        UIView.animate(withDuration: AppConstants.defaultAnimateDuration) {
            self.frame.origin = .zero
        }
    }
    
    public func animateOutAndRemove() {
        UIView.animate(withDuration: AppConstants.defaultAnimateDuration, animations: {
            self.transform = CGAffineTransform(translationX: 0, y: UIScreen.main.bounds.height)
        }) { (finished) in
            if finished {
                print("Remove from parent!")
                self.transform = .identity
                self.removeFromSuperview()
            }
        }
    }
    
    @objc func handleKeyboardShow(notification: NSNotification) {
        if let userInfo = notification.userInfo as? Dictionary<String, AnyObject> {
            let frame = userInfo[UIKeyboardFrameEndUserInfoKey]
            let keyboardRect = frame?.cgRectValue
            
            if let height = keyboardRect?.height {
                UIView.animate(withDuration: AppConstants.defaultAnimateDuration, animations: {
                    self.transform = CGAffineTransform(translationX: 0, y: -height / 2)
                    self.setNeedsLayout()
                })
            }
        }
    }
    
    @objc func handleKeyboardHide(notification: NSNotification) {
        UIView.animate(withDuration: AppConstants.defaultAnimateDuration, animations: {
            self.transform = .identity
            self.setNeedsLayout()
        })
    }
    
    @objc fileprivate func handleOverlayViewTap() {
        animateOutAndRemove()
    }
    
    @objc fileprivate func handleMainViewTap() {
        self.endEditing(true)
        return
    }
    
}
