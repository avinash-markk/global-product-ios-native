//
//  ClaimRewardPopupView.swift
//  Markk
//
//  Created by Avinash Thakur on 31/07/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import UIKit

protocol ClaimRewardPopupViewDelegate: NSObjectProtocol {
    func popupViewTapped()
}

class ClaimRewardPopupView: UIView {
    
    var contentView: UIView!
    @IBOutlet weak var popupSubview: UIView!
    @IBOutlet weak var popupImage: UIImageView!
    @IBOutlet weak var popupLabel: UILabel!
    
    weak var delegate: ClaimRewardPopupViewDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        contentView.frame = self.frame
    }
    
    func xibSetup() {
        backgroundColor = UIColor.clear
        contentView = loadNib()
        contentView.frame = bounds
        addSubview(contentView)
        self.setSubviewUI()
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleTap))
        self.addGestureRecognizer(tap)
    }
    
    func setSubviewUI() {
        popupLabel.font = UIFont(name: AppConstants.Font.primaryRegular, size: 16)
        popupLabel.textColor = AppConstants.SminqColors.white100
        popupLabel.text = "Awesome! We’ll email you details on the reward."
        popupSubview.backgroundColor = AppConstants.SminqColors.grey200
        popupSubview.layer.cornerRadius = 8.0
        popupSubview.layer.borderWidth  = 1.0
        popupSubview.layer.borderColor = AppConstants.SminqColors.white100.withAlphaComponent(0.1).cgColor
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        self.delegate?.popupViewTapped()
    }
    
    func animateOutView() {
        UIView.animate(withDuration: 0.5, animations: {
            self.alpha = 0
        }, completion: { done in
            self.isHidden = true
        })
    }
    
    func animateInView() {
        UIView.animate(withDuration: 0.5, animations: {
            self.alpha = 1
        }, completion: { done in
            self.isHidden = false
        })
    }

}
