//
//  Extensions+NSDictionary.swift
//  Markk
//
//  Created by Pushkar Deshmukh on 17/07/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import CoreLocation

extension NSDictionary {
    func toCLLocation() -> CLLocation? {
        guard let latitude = self["latitude"] as? CLLocationDegrees,
        let longitude = self["longitude"] as? CLLocationDegrees,
        let timestamp = self["timestamp"] as? Date,
        let horizontalAccuracy = self["horizontalAccuracy"] as? CLLocationAccuracy,
        let verticalAccuracy = self["verticalAccuracy"] as? CLLocationAccuracy,
        let altitude = self["altitude"] as? CLLocationDistance else { return nil }
        
        let location = CLLocation.init(coordinate: CLLocationCoordinate2D.init(latitude: latitude, longitude: longitude), altitude: altitude, horizontalAccuracy: horizontalAccuracy, verticalAccuracy: verticalAccuracy, timestamp: timestamp)
        return location
    }
    
    func toStoryShareOption() -> StoryShareOption? {
        guard let id = self["id"] as? String,
            let imageString = self["imageString"] as? String,
            let label = self["label"] as? String else { return nil }
        
        return StoryShareOption(id: StoryShareOptionID(rawValue: id)!, imageString: imageString, label: label)
    }
}


