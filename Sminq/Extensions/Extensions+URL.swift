//
//  Extensions+URL.swift
//  Markk
//
//  Created by Pushkar Deshmukh on 24/07/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import Foundation

extension URL {
    func getQueryString(parameter: String) -> String? {
        return URLComponents(url: self, resolvingAgainstBaseURL: false)?
            .queryItems?
            .first { $0.name == parameter }?
            .value
    }
    
    func getDecodedURL() -> URL? {
        let absoluteString = self.absoluteString
        
        if let decodedUrl = absoluteString.decodeUrl() {
            return URL(string: decodedUrl)
        }
        
        return nil
    }
}
