//
//  Extensions+UIButton.swift
//  Markk
//
//  Created by Pushkar Deshmukh on 10/07/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import UIKit

extension UIButton {
    /**
     Sets background color to UIButton
     
     - Parameters:
     - color: `UIColor`
     - forState: `UIControl.State`
     */
    
    func setBackgroundColor(_ color: UIColor, forState: UIControl.State) {
        self.clipsToBounds = true  // add this to maintain corner radius
        UIGraphicsBeginImageContext(CGSize(width: 1, height: 1))
        if let context = UIGraphicsGetCurrentContext() {
            context.setFillColor(color.cgColor)
            context.fill(CGRect(x: 0, y: 0, width: 1, height: 1))
            let colorImage = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            self.setBackgroundImage(colorImage, for: forState)
        }
    }
    
    /**
     Aligns title and image content to center
     
     - Parameters:
     - spacing: `CGFloat`
     */
    
    func centerTextAndImage(spacing: CGFloat) {
        let insetAmount = spacing / 2
        imageEdgeInsets = UIEdgeInsets(top: 0, left: -insetAmount, bottom: 0, right: insetAmount)
        titleEdgeInsets = UIEdgeInsets(top: 0, left: insetAmount, bottom: 0, right: -insetAmount)
        contentEdgeInsets = UIEdgeInsets(top: 0, left: insetAmount, bottom: 0, right: insetAmount)
    }
    
    /**
     Updates count on button. If count is present then shows the button. Otherwises hides it
     
     - Parameters:
     - count: count
     */
    
    func updateCount(count: Int?) {
        if let count = count, count > 0 {
            self.setTitle("\(count)", for: .normal)
            self.isHidden = false
        } else {
            self.setTitle("", for: .normal)
            self.isHidden = true
        }
    }
    
    func moveImageLeftTextCenter(image : UIImage, imagePadding: CGFloat, renderingMode: UIImage.RenderingMode){
        self.setImage(image.withRenderingMode(renderingMode), for: .normal)
        guard let imageViewWidth = self.imageView?.frame.width else { return }
        guard let titleLabelWidth = self.titleLabel?.intrinsicContentSize.width else{return}
        self.contentHorizontalAlignment = .left
        let imageLeft = imagePadding - imageViewWidth / 2
        let titleLeft = (bounds.width - titleLabelWidth) / 2 - imageViewWidth
        imageEdgeInsets = UIEdgeInsets(top: 0.0, left: imageLeft, bottom: 0.0, right: 0.0)
        titleEdgeInsets = UIEdgeInsets(top: 0.0, left: titleLeft , bottom: 0.0, right: 0.0)
    }
}
