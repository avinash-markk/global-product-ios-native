//
//  Extensions+CLLocation.swift
//  Markk
//
//  Created by Pushkar Deshmukh on 17/07/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import CoreLocation

extension CLLocation {
    func toDictionary() -> [String: Any] {
        let dictionary = [
            "latitude": self.coordinate.latitude,
            "longitude": self.coordinate.longitude,
            "timestamp": self.timestamp,
            "horizontalAccuracy": self.horizontalAccuracy,
            "verticalAccuracy": self.verticalAccuracy,
            "altitude": self.altitude
            ] as [String : Any]
        
        return dictionary
    }
}
