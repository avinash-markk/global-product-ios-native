//
//  Extensions+NSAttributedString.swift
//  Markk
//
//  Created by Pushkar Deshmukh on 21/08/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import Foundation

extension NSAttributedString {
    func uppercased() -> NSAttributedString {
        
        let result = NSMutableAttributedString(attributedString: self)
        
        result.enumerateAttributes(in: NSRange(location: 0, length: length), options: []) {_, range, _ in
            result.replaceCharacters(in: range, with: (string as NSString).substring(with: range).uppercased())
        }
        
        return result
    }
}
