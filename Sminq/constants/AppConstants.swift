//  AppConstants.swift
//  Sminq
//
//  Created by SMINQ iOS on 09/03/18.
//  Copyright © 2018 Suraj Sagare. All rights reserved.
//

import Foundation
import UIKit
public class AppConstants {
    static let storyboard = UIStoryboard(name: "Main", bundle: nil)
    static let appDelegate = UIApplication.shared.delegate as! AppDelegate
    static let appVersion = UIApplication.shared.applicationVersion() // 1
    static let appBuild = UIApplication.shared.applicationBuild() // 80
    static let appVersionBuild = UIApplication.shared.versionBuild()

    static let appStoreVersion = "1.0.0"
    static var previousScreen = ""
    
    static let isRemoteLoggingEnabled = true
    static let apiRequestTimeout = 60.0
    static let fullscreenOverlayViewTagConstant = 8934
    static let defaultAnimateDuration = 0.3
    static let storyLongPressTimeout = 0.7
    static let defaultMoodScore = 1
    
    struct UserDefaultLocation {
        static let latitude = 20.5937
        static let longitude = 78.9629
    }

    struct SminqColors {
        static let purpleColor = UIColor(red: 111.0 / 255.0, green: 84.0 / 255.0, blue: 250.0 / 255.0, alpha: 1.0)
        static let darkBlueGrey = UIColor(red: 35.0 / 255.0, green: 51.0 / 255.0, blue: 76.0 / 255.0, alpha: 1.0)
        static let blueberry = UIColor(red: 91.0 / 255.0, green: 77.0 / 255.0, blue: 160.0 / 255.0, alpha: 1.0)
        static let brownishGreyColor = UIColor(red: 107.0 / 255.0, green: 106.0 / 255.0, blue: 106.0 / 255.0, alpha: 0.1)
        static let brownishGrey = UIColor(red: 107.0 / 255.0, green: 106.0 / 255.0, blue: 106.0 / 255.0, alpha: 1.0)
        static let tomatoColor = UIColor(red: 230.0 / 255.0, green: 55.0 / 255.0, blue: 46.0 / 255.0, alpha: 1.0)
        static let greenyBlueColor = UIColor(red: 63.0 / 255.0, green: 179.0 / 255.0, blue: 157.0 / 255.0, alpha: 1.0)
        static let blackColor = UIColor(red: 0.0 / 255.0, green: 0.0 / 255.0, blue: 0.0 / 255.0, alpha: 1.0)
        static let black10 = UIColor(white: 0.0, alpha: 0.1)
        static let black24 = UIColor(red: 0.0 / 255.0, green: 0.0 / 255.0, blue: 0.0 / 255.0, alpha: 0.24)
        static let whiteColor = UIColor(red: 255.0 / 255.0, green: 255.0 / 255.0, blue: 255.0 / 255.0, alpha: 1.0)
        static let greenyBlue = UIColor(red: 63.0 / 255.0, green: 179.0 / 255.0, blue: 157.0 / 255.0, alpha: 1.0)
        static let grey = UIColor(red: 0.0 / 255.0, green: 0.0 / 255.0, blue: 25.0 / 255.0, alpha: 0.25)
        static let grey100 = UIColor(red: 61.0 / 255.0, green: 61.0 / 255.0, blue: 61.0 / 255.0, alpha: 1)
        static let grey200 = UIColor(red: 31.0 / 255.0, green: 31.0 / 255.0, blue: 31.0 / 255.0, alpha: 1)
        static let yellow = UIColor(red: 240.0 / 255.0, green: 202.0 / 255.0, blue: 62.0 / 255.0, alpha: 1.0)
        static let yellow100 = UIColor(red: 248.0 / 255.0, green: 178.0 / 255.0, blue: 22.0 / 255.0, alpha: 1.0)
        static let red = UIColor(red: 225.0 / 255.0, green: 89.0 / 255.0, blue: 68.0 / 255.0, alpha: 1.0)
        static let red100 = UIColor(red: 241.0 / 255.0, green: 85.0 / 255.0, blue: 53.0 / 255.0, alpha: 1.0)
        static let red200 = UIColor(red: 227.0 / 255.0, green: 51.0 / 255.0, blue: 16.0 / 255.0, alpha: 1.0)
        static let black40 = UIColor(white: 0.0, alpha: 0.4)
        static let black50 = UIColor(white: 0.0, alpha: 0.5)
        static let black75 = UIColor(white: 0.0, alpha: 0.75)
        static let placeHolderGray = UIColor(red: 107.0 / 255.0, green: 106.0 / 255.0, blue: 106.0 / 255.0, alpha: 0.75)
        static let darkGray = UIColor(red: 107.0 / 255.0, green: 106.0 / 255.0, blue: 106.0 / 255.0, alpha: 1.0)
        static let tabbarDefaultColor  = UIColor(red: 35.0/255.0, green: 51.0/255.0, blue: 76.0/255.0, alpha: 0.65)
        static let tabbarSelectedColor = UIColor(red: 35.0/255.0, green: 51.0/255.0, blue: 76.0/255.0, alpha: 0.75)
        static let darkBlueGrey5 = UIColor(red: 244.0 / 255.0, green: 245.0 / 255.0, blue: 246.0 / 255.0, alpha: 1)
        static let darkBlueGrey50 = UIColor(red: 35.0 / 255.0, green: 51.0 / 255.0, blue: 76.0 / 255.0, alpha: 0.5)
        static let darkBlueGrey10 = UIColor(red: 35.0 / 255.0, green: 51.0 / 255.0, blue: 76.0 / 255.0, alpha: 0.10)
        static let darkBlueGrey20 = UIColor(red: 35.0 / 255.0, green: 51.0 / 255.0, blue: 76.0 / 255.0, alpha: 0.20)
        static let darkBlueGrey25 = UIColor(red: 35.0 / 255.0, green: 51.0 / 255.0, blue: 76.0 / 255.0, alpha: 0.25)
        static let darkBlueGrey75 = UIColor(red: 35.0 / 255.0, green: 51.0 / 255.0, blue: 76.0 / 255.0, alpha: 0.75)
        static let darkBlueGrey65 = UIColor(red: 35.0 / 255.0, green: 51.0 / 255.0, blue: 76.0 / 255.0, alpha: 0.65)
        static let placeClubbingBorder = UIColor(red: 184.0 / 255.0, green: 184.0 / 255.0, blue: 184.0 / 255.0, alpha: 1)
        static let pinkishRed = UIColor(red: 236.0 / 255.0, green: 34.0 / 255.0, blue: 39.0 / 255.0, alpha: 1)
        static let pinkishRed10 = UIColor(red: 236.0 / 255.0, green: 34.0 / 255.0, blue: 39.0 / 255.0, alpha: 0.1)
        static let pinkishRed75 = UIColor(red: 236.0 / 255.0, green: 34.0 / 255.0, blue: 39.0 / 255.0, alpha: 0.75)
        static let white = UIColor(red: 245.0 / 255.0, green: 245.0 / 255.0, blue: 245.0 / 255.0, alpha: 1)
//        static let whiteTwo = UIColor(red: 251.0 / 255.0, green: 251.0 / 255.0, blue: 251.0 / 255.0, alpha: 1)
//        static let white25 = UIColor(red: 255.0 / 255.0, green: 255.0 / 255.0, blue: 255.0 / 255.0, alpha: 0.25)
        static let white50 = UIColor(red: 255.0 / 255.0, green: 255.0 / 255.0, blue: 255.0 / 255.0, alpha: 0.5)
//        static let white35 = UIColor(red: 255.0 / 255.0, green: 255.0 / 255.0, blue: 255.0 / 255.0, alpha: 0.35)
        static let white75 = UIColor(red: 255.0 / 255.0, green: 255.0 / 255.0, blue: 255.0 / 255.0, alpha: 0.75)
        static let darkBlueGray = UIColor(red: 35.0/255.0, green: 51.0/255.0, blue: 76.0/255.0, alpha: 100.0)
        static let darkBlueGray10 = UIColor(red: 35.0/255.0, green: 51.0/255.0, blue: 76.0/255.0, alpha: 0.10)
        static let blue50 = UIColor(red: 90.0/255.0, green: 102.0/255.0, blue: 120.0/255.0, alpha: 100.0)
        static let oceanGreen = UIColor(red: 63.0/255.0, green: 145.0/255.0, blue: 126.0/255.0, alpha: 100.0)
        static let paleTeal = UIColor(red: 159.0/255.0, green: 200.0/255.0, blue: 190.0/255.0, alpha: 100.0)
        static let azul = UIColor(red: 34.0/255.0, green: 140.0/255.0, blue: 236.0/255.0, alpha: 100.0)
        static let white0 = UIColor(white: 239.0 / 255.0, alpha: 0.0)
        static let white1 = UIColor(white: 239.0 / 255.0, alpha: 1.0)
        static let white050 = UIColor(white: 239.0 / 255.0, alpha: 0.5)
        static let fadedPink = UIColor(red: 195.0 / 255.0, green: 181.0 / 255.0, blue: 182.0 / 255.0, alpha: 1.0)
        static let purpleyGrey = UIColor(red: 144.0 / 255.0, green: 126.0 / 255.0, blue: 127.0 / 255.0, alpha: 1.0)
        static let purpleyGrey0 = UIColor(red: 144.0 / 255.0, green: 126.0 / 255.0, blue: 127.0 / 255.0, alpha: 0.0)
        static let purpleyGrey10 = UIColor(red: 144.0 / 255.0, green: 126.0 / 255.0, blue: 127.0 / 255.0, alpha: 0.1)
        static let purpleyGrey50 = UIColor(red: 144.0 / 255.0, green: 126.0 / 255.0, blue: 127.0 / 255.0, alpha: 0.5)
        static let purpleyGrey75 = UIColor(red: 144.0 / 255.0, green: 126.0 / 255.0, blue: 127.0 / 255.0, alpha: 0.75)
        static let greyishBrown = UIColor(red: 103.0 / 255.0, green: 89.0 / 255.0, blue: 89.0 / 255.0, alpha: 1.0)
        static let greyishBrown25 = UIColor(red: 103.0 / 255.0, green: 89.0 / 255.0, blue: 89.0 / 255.0, alpha: 0.25)

        static let purple10075 = UIColor(red: 91.0 / 255.0, green: 44.0 / 255.0, blue: 74.0 / 255.0, alpha: 0.75)
        
        static let blackDark100 = UIColor(red: 0.0 / 255.0, green: 0.0 / 255.0, blue: 0.0 / 255.0, alpha: 1)
        static let blackLight100 = UIColor(red: 13.0 / 255.0, green: 13.0 / 255.0, blue: 13.0 / 255.0, alpha: 1)
        static let purple100 = UIColor(red: 91.0 / 255.0, green: 44.0 / 255.0, blue: 74.0 / 255.0, alpha: 1)
        static let violet100 = UIColor(red: 77.0 / 255.0, green: 84.0 / 255.0, blue: 146.0 / 255.0, alpha: 1)
        static let white100 = UIColor(red: 243.0 / 255.0, green: 243.0 / 255.0, blue: 244.0 / 255.0, alpha: 1)
        static let green100 = UIColor(red: 90.0 / 255.0, green: 194.0 / 255.0, blue: 154.0 / 255.0, alpha: 1)
        static let green200 = UIColor(red: 61.0 / 255.0, green: 164.0 / 255.0, blue: 125.0 / 255.0, alpha: 1)
        static let shadow = UIColor(red: 8.0 / 255.0, green: 7.0 / 255.0, blue: 7.0 / 255.0, alpha: 1)
        
        static let gradientYellowLight = UIColor(red: 224.0 / 255.0, green: 157.0 / 255.0, blue: 7.0 / 255.0, alpha: 1)
        static let gradientYellowDark = UIColor(red: 186 / 255.0, green: 130.0 / 255.0, blue: 6.0 / 255.0, alpha: 1)
        
        static let gradientBlueLight = UIColor(red: 43.0 / 255.0, green: 147.0 / 255.0, blue: 182.0 / 255.0, alpha: 1)
        static let gradientBlueDark = UIColor(red: 35 / 255.0, green: 121.0 / 255.0, blue: 150.0 / 255.0, alpha: 1)
        
        static let gradientRedLight = UIColor(red: 238.0 / 255.0, green: 54.0 / 255.0, blue: 17.0 / 255.0, alpha: 1)
        static let rgradientRedDark = UIColor(red: 202.0 / 255.0, green: 46.0 / 255.0, blue: 14.0 / 255.0, alpha: 1)
    }
    
    struct AppTarget {
        static let sminq = "Sminq"
        static let markk = "Markk"
    }
    
    struct IphoneDeviceSizes {
        static let iphone5Height: CGFloat = 568.0
        static let iphone6Height = 667.0
        static let iphone6PlusHeight = 736.0
        static let iphoneXHeight = 812.0
    }
    
    struct ErrorMessages {
        static let placeNotFoundFromPlacePicker = "Apologies, Markk currently does not recognize this as a place. We are taking this as a feedback, please try to search for a place again."
        static let apologiesMsg = "Apologies ! Something went wrong, please try again later"
        static let upvote = "Apologies ! Something went wrong, please try again later"
        static let downvote = "Apologies ! Something went wrong, please try again later"
        static let postComment = "Apologies ! Something went wrong, please try again later"
        static let questionsFetch = "Apologies ! Something went wrong, please try again later"
        static let emptyResponse = "Response is empty"
        static let noInternet = "No internet connection"
        static let opsSomethingWentWrong = "Oops! Something went wrong."
        static let failedConnectingServer = "We’re having trouble connecting to the server."
        static let unauthoriseRequest = "Oops! Please login to continue"
        static let couldNotFetchData = "Could not fetch data. Please try again"
        static let serverBusyPleaseTryAgain = "Oops! we can't find what you are looking for."
        static let noInterneSubHeading = "You're offline. Please check your connection and try again."
        static let troubleConnecting = "We’re having trouble connecting to the server."
        static let troubleConnectingAndTapToRetry = "We’re having trouble connecting to the server. Please tap to retry."
        static let facingConnectivity = "Facing connectivity problem!"
        static let cantFindWhatYouLookingFor = "Oops! we can't find what you are looking for."
        static let serverError = "Oops! Our server is acting up. Please try again."
        static let timeout = "Oops! We’re having trouble connecting to the server."
        static let alreadyLikedRating = "You have already liked this rating."
        static let alreadyReporedRating = "You have already reported this rating."
        static let displayNameNotAvailable = "This display name is not available. Please try another name."
    }
    
    struct ErrorUserInfoKeys {
        static let errorType = "ErrorType"
        static let errorCode = "ErrorCode"
        static let errorTitle = "ErrorTitle"
        static let errorDescription = "ErrorDescription"
    }
    
    struct ErrorLog {
        static let emptyResponse = "Response is empty"
        static let jsonParse = "Failed to parse json"
    }
    
    struct SuccessMessages {
        static let deletePost = "Your post has been successfully deleted!"
    }
    
    struct Messages {
        static let postNoLongerAvailable = "This post will no longer be visible"
        static let postExpired = "Sorry, this post no longer exists."
        static let localChampionAcceptedMsg = "Congrats you are now a Local champion"
        static let localChampionEnrolledMsg = "Thanks for submitting application"
        static let localChampionEnrollMsg = "MAKE YOUR CITY GO LIVE – ENROL NOW"
        static let unknownLocation = "Posted from unknown location"
        static let copiedToClipboard = "Copied to clipboard"
    }
        
    struct Font {
        static let primaryRegular = "Inter-Regular"
        static let primaryBold = "Inter-Bold"
        static let montSerratBlack  = "Montserrat-Black"
    }
    
    struct DownvoteReasons {
        static let offensive = "Offensive"
        static let misleading = "Misleading"
        static let irrelevant = "Irrelevant"
        static let hide = "Hide"
    }
    
    struct RolesStatus {
        static let accepted = "accepted"
        static let enrolled = "enrolled"
    }
    
    struct Devices {
        static let iPhoneX = "iPhone X"
        static let iPhone6Plus = "iPhone 6 Plus"
        static let iPhone7Plus = "iPhone 7 Plus"
    }
    
    struct screenNames {
        static let explore = "explore"
        static let login = "login"
        static let signup = "signup"
        static let home = "home"
        static let storyView = "story_view"
        static let placeDetails = "place_details"
        static let myPlaces = "my places"
        static let notifications = "notifications"
        static let profile = "profile"
        static let editProfile = "edit_profile"
        static let addPost = "add post"
        static let placeFeed = "place feed"
        static let badges = "badges"
        static let subPlacesList = "sub places list"
        static let comments = "comments"
        static let signupLanding = "signup landing"
        static let signupComplete = "signup complete"
        static let search = "search"
        static let groupedPlacesFeed = "grouped places feed"
        static let mypost = "mypost"
        static let livePostMyPost = "Live post mypost"
        static let expiredPostMyPost = "Expired post mypost"
        static let expiredPostFeedMyPost = "Expired post feed mypost"
        static let following = "following"
        static let follower = "follower"
        static let archivedPosts = "archived_posts"
        static let uploads = "uploads"
        static let congrats = "congrats"
    }
    
    struct RatingsIntendSection {
        static let liveRatingsSection = "liveRatingsSection"
        static let cameraSection = "cameraSection"
    }
    
    struct ViewRatingsSection {
        static let liveRatings = "yourLiveRatings"
        static let nearbyRatings = "nearbyRatings"
        static let highlights = "highlights"
        static let trending = "trending"
        static let live = "live"
        static let archived = "archived"
    }
    
    struct Login {
        static let facebook = "facebook"
        static let google = "google"
        static let email = "email"
        static let phone = "phone"
    }
    
    struct UserDefaultsOnBoardingCardKeys {
        static let onboardingWelcomeCardWithOutImage = "OnboardingWelcomeCardWithOutImage"
        static let onboardingPostExpires = "OnboardingPostExpires"
        static let onboardingLocationVerified = "OnboardingLocationVerified"
        static let onboardingAddPost = "OnboardingAddPost"
    }
    
    struct OnboardingCardValues {
        static let onboardingWelcomeCardWithOutImageValue = "OnboardingWelcomeCardWithOutImageValue"
        static let onboardingPostExpiresValue = "OnboardingPostExpiresValue"
        static let onboardingLocationVerifiedValue = "OnboardingLocationVerifiedValue"
        static let onboardingAddPostValue = "OnboardingAddPostValue"
    }
    
    struct UserDefaultsKeys {
        static let nearByStoriesTapped = "NearByStoriesTapped"
        static let notificationUserInfo = "UotificationUserInfo"
    }
    

    struct AppNotificationNames {
        static let updateNotification = "updateNotificationCount"
        static let refreshNotificationList = "refreshNotificationList"
        static let placeDetailsDismissed = "placeDetailsDismissed"
        static let userProfileTapped = "userProfileTapped"
        static let userProfileTappedOnComments = "userProfileTappedOnComments"
        static let userHandleTappedOnComments = "userHandleTappedOnComments"
        static let getSearchResultsInPostTagging = "getSearchResultsInPostTagging"
        static let userSuggestionListVisibilityInPostTagging = "userSuggestionListVisibilityInPostTagging"
        static let showBottomBar = "showBottomBar"
        static let hideBottomBar = "hideBottomBar"
        static let notificationTappedFromTray = "notificationTappedFromTray"
        static let timelineTabSelectedTwice = "timelineTabSelectedTwice"
        static let profileTabSelectedTwice = "profileTabSelectedTwice"
        static let deletedFeedObserver = "deletedFeedObserver"
        static let openCameraViewController = "openCameraViewController"
        static let uploadDone = "Upload_State_Received"
        static let resumeStory = "resumeStory"
        static let pauseStory = "pauseStory"
        static let networkReachable = "networkReachable"
        static let profileTab = "Add_Profile_Tab_Analytics"
        static let homeTab = "Add_Home_Tab_Analytics"
    }

    struct KeyPathConstants {
        static let duration = "duration"
        static let playbackBufferEmpty = "playbackBufferEmpty"
        static let playbackLikelyToKeepUp = "playbackLikelyToKeepUp"
        static let playbackBufferFull = "playbackBufferFull"
    }
    
    struct ViewTags {
        static let nearByStoriesBottomModal = 1
        static let locationBottomModal = 1212
    }
    
    struct StoryGroup {
        static let nearby = "nearby"
        static let allFollowing = "allFollowing"
        static let archive = "archive"
    }
    
    struct SuggestionType {
        static let places = "places"
        static let friends = "friends"
    }
    
    struct apiNames {
        static let deleteComment = "delete_comment"
        static let getSminqPlaceDetails = "get_sminq_place_details"
        static let searchPlacesOrUsers = "search_places_or_users"
        static let searchNearbyPlaces = "search_nearby_places"
        static let googleSearchNearbyPlaces = "google_nearby_places"
        static let googleSearchAutocomplete = "google_autocomplete_search"
        static let getCategoryQuestions = "get_category_questions"
        static let verifyUserLocation = "verify_user_location"
        static let getUserDetails = "get_public_profile_details"
        static let getOwnUserDetails = "get_own_user_details"
        static let createNewPlace = "create_new_place"
        static let addComment = "add_comment"
        static let deletePost = "delete_post"
        static let loginUser = "login_user"
        static let followPlace = "follow_place"
        static let unfollowPlace = "unfollow_place"
        static let addPost = "add_post"
        static let viewPostCount = "view_post_count"
        static let downvotePost = "downvote_post"
        static let upvotePost = "upvote_post"
        static let sharePost = "share_post"
        static let updateUserProfile = "update_user_profile"
        static let verifyDisplayName = "verify_display_name"
        static let followUser = "follow_user"
        static let unfollowUser = "unfollow_user"
        static let getFollowingList = "get_following_list"
        static let getFollowersList = "get_followers_list"
        static let getUserArchivePosts = "get_user_archive_posts"
        static let shortenUrl = "shorten_url"
        static let getGooglePlaceDetails = "get_google_place_details"
        static let registerForNotifications = "register_for_notifications"
        static let deregisterForNotifications = "deregister_for_notifications"
        static let getLiveStoriesList = "get_live_stories_list"
        static let getStoryDetails = "get_story_details"
        static let getPlaceStories = "get_place_stories"
        static let getPlaceStoriesBySlug = "get_place_stories_by_slug"
        static let getPlaceStoriesByPostId = "get_place_stories_by_post_id"
        static let getSuggestionsUserPlace = "get_suggestions_user_place"
        static let getUserTimeline = "get_user_timeline"
        static let getPlaceDetail = "create_place_and_get_place_details"
        static let getTrends = "get_trends"
        static let claimRewards = "claim_rewards"
        static let getUnlockedLevel = "get_unlocked_level"
    }
    
    struct Traces {
        static let homeLoading = "home loading"
        static let storyLoading = "story loading"
        static let profileUpdating = "profile updating"
        static let reverseGeocoding = "reverse geocoding"
        static let imageLoading = "image loading"
        static let videoLoading = "video loading"
    }
    
    struct IntermediateTraceEvents {
        static let cloudinaryImageUpload = "Cloudinary image upload Success"
        static let reverseGeocodingFailed = "Reverse geocoding failed"
    }
    
    struct QueryParamKeys {
        static let storyId = "s"
    }
    
    static let yes = "yes"
    static let no = "no"
    static let image = "image"
    static let video = "video"
    static let noResults = "no results"
    static let follow = "follow"
    static let places = "places"
    static let friends = "friends"
    static let newFeed = "new_feed"
    static let newReply = "new_reply"
    static let newPostTag = "new_post_tag"
    static let newVote = "new_vote"
    static let newBadge = "new_badge"
    static let comment = "comment"
    static let post = "post"
    static let sminq = "sminq"
    static let sminqResults = "sminqResults"
    static let googleResults = "googleResults"

    static let lessAccurateLocation = "Less accurate location"
    static let bestAccurateLocation = "Best accurate location"

    static let ios = "ios"
    static let share = "share"
    static let appShare = "AppShare"
    static let enablePush = "enable"
    static let disablePush = "disable"
    static let deleteRating = "Delete rating"
    static let reportRating = "Report rating"
    static let cancel = "Cancel"
    static let report = "Report"
    static let delete = "Delete"
    static let granted = "granted"
    static let denied = "denied"
    
    // Remote config default constats
    
    
    
    struct requestMethods {
        static let get = "get"
        static let post = "post"
        static let put = "put"
        static let delete = "delete"
    }
    
    struct LogStrings {
        static let discardMedia = "Tapped on discard media icon ”"
    }
    
    struct PusherChannels {
        static let markkTrending = "markk-trending"
    }
    
    struct PusherEvents {
        static let trending = "trending"
    }
    
}

extension UIApplication {
    
    class func openAppSettings() {
        if UIApplication.shared.canOpenURL(URL(string: UIApplicationOpenSettingsURLString)!) {
            UIApplication.shared.open((URL(string: UIApplicationOpenSettingsURLString)!), options: [:], completionHandler: nil)
        }
    }
    
    
    func applicationVersion() -> String {
        if let versionString = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as? String {
            return versionString
        } else {
            return ""
        }
    }
    
    func applicationBuild() -> String {
        if let buildString = Bundle.main.object(forInfoDictionaryKey: kCFBundleVersionKey as String) as? String {
            return buildString
        } else {
            return ""
        }
    }
    
    func versionBuild() -> String {
        let version = self.applicationVersion()
        let build = self.applicationBuild()
        
        return "\(version)(\(build))"
    }
    
    func getTargetName() -> String {
        if let target = Bundle.main.object(forInfoDictionaryKey: "TargetName") as? String {
            return target
        } else {
            return ""
        }
    }
    
    func getProductionGatewayProxyURL() -> String {
        if let proxyUrl = Bundle.main.object(forInfoDictionaryKey: "ProductionGatewayProxyURL") as? String {
            return proxyUrl
        } else {
            return ""
        }
    }

    func getStagingGatewayProxyURL() -> String {
        if let proxyUrl = Bundle.main.object(forInfoDictionaryKey: "StagingGatewayProxyURL") as? String {
            return proxyUrl
        } else {
            return ""
        }
    }

    func getDevelopmentGatewayProxyURL() -> String {
        if let proxyUrl = Bundle.main.object(forInfoDictionaryKey: "DevelopmentGatewayProxyURL") as? String {
            return proxyUrl
        } else {
            return ""
        }
    }
    
    
    
    func getStagingTermsAndConditionsURL() -> String {
        if let stagingTerms = Bundle.main.object(forInfoDictionaryKey: "StagingTermsAndCondtionsURL") as? String {
            return stagingTerms
        } else {
            return ""
        }
    }
    
    func getProductionTermsAndConditionsURL() -> String {
        if let prodTerms = Bundle.main.object(forInfoDictionaryKey: "ProductionTermsAndCondtionsURL") as? String {
            return prodTerms
        } else {
            return ""
        }
    }
    
    func getStagingPrivacyPolicyURL() -> String {
        if let stagingPrivacy = Bundle.main.object(forInfoDictionaryKey: "StagingPrivacyPolicyURL") as? String {
            return stagingPrivacy
        } else {
            return ""
        }
    }
    
    func getProductionPrivacyPolicyURL() -> String {
        if let prodPrivacy = Bundle.main.object(forInfoDictionaryKey: "ProductionPrivacyPolicyURL") as? String {
            return prodPrivacy
        } else {
            return ""
        }
    }
    
    func getStagingGoogleAPIKey() -> String {
        return Bundle.main.object(forInfoDictionaryKey: "StagingGoogleAPIKey") as! String
    }
    
    func getProductionGoogleAPIKey() -> String {
        return Bundle.main.object(forInfoDictionaryKey: "ProductionGoogleAPIKey") as! String
    }
    
    func getStagingAppWebURL() -> String {
        return Bundle.main.object(forInfoDictionaryKey: "StagingAppWebURL") as! String
    }
    
    func getProductionAppWebURL() -> String {
        return Bundle.main.object(forInfoDictionaryKey: "ProductionAppWebURL") as! String
    }
    
    func getAppWebURL() -> String {
        switch ApiConfig.appEnvironment {
        case .staging, .development:
            return UIApplication.shared.getStagingAppWebURL()
        case .production:
            return UIApplication.shared.getProductionAppWebURL()
        }
    }
    
    func getStagingCleverTapToken() -> String {
        return Bundle.main.object(forInfoDictionaryKey: "StagingCleverTapToken") as! String
    }
    
    func getProductionCleverTapToken() -> String {
        return Bundle.main.object(forInfoDictionaryKey: "ProductionCleverTapToken") as! String
    }
    
    func getStagingCleverTapAccountId() -> String {
        return Bundle.main.object(forInfoDictionaryKey: "StagingCleverTapAccountId") as! String
    }
    
    func getProductionCleverTapAccountId() -> String {
        return Bundle.main.object(forInfoDictionaryKey: "ProductionCleverTapAccountId") as! String
    }
    
    func getFeedBackURL() -> String {
        return Bundle.main.object(forInfoDictionaryKey: "ShareFeedBackURL") as! String
    }
    
    func getStagingFaqURL() -> String {
        return Bundle.main.object(forInfoDictionaryKey: "StagingFaqUrl") as! String
    }
    
    func getProductionFaqURL() -> String {
        return Bundle.main.object(forInfoDictionaryKey: "ProductionFaqUrl") as! String
    }
    
    func getStagingAppsFlyerDevKey() -> String {
        return Bundle.main.object(forInfoDictionaryKey: "StagingAppsFlyerDevKey") as! String
    }
    
    func getProductionAppsFlyerDevKey() -> String {
        return Bundle.main.object(forInfoDictionaryKey: "ProductionAppsFlyerDevKey") as! String
    }
    
    func getStagingAppsFlyerAppleAppID() -> String {
        return Bundle.main.object(forInfoDictionaryKey: "StagingAppsFlyerAppleAppID") as! String
    }
    
    func getProductionAppsFlyerAppleAppID() -> String {
        return Bundle.main.object(forInfoDictionaryKey: "ProductionAppsFlyerAppleAppID") as! String
    }
    
    func getStagingBugfenderAppKey() -> String {
        return Bundle.main.object(forInfoDictionaryKey: "StagingBugfenderAppKey") as! String
    }
    
    func getProductionBugfenderAppKey() -> String {
        return Bundle.main.object(forInfoDictionaryKey: "ProductionBugfenderAppKey") as! String
    }
    
    func getStagingDynamicLinksDomain() -> String? {
        return Bundle.main.object(forInfoDictionaryKey: "StagingDynamicLinksDomain") as? String
    }
    
    func getProductionDynamicLinksDomain() -> String? {
        return Bundle.main.object(forInfoDictionaryKey: "ProductionDynamicLinksDomain") as? String
    }
    
    func getProductionPusherAppKey() -> String {
        return Bundle.main.object(forInfoDictionaryKey: "ProductionPusherKey") as! String
    }
    
    func getStagingPusherAppKey() -> String {
        return Bundle.main.object(forInfoDictionaryKey: "StagingPusherKey") as! String
    }
    
    func getPusherAppCluster() -> String {
        return Bundle.main.object(forInfoDictionaryKey: "PusherAppCluster") as! String
    }

    func getAppVersion() -> String {
        switch ApiConfig.appEnvironment {
            
        case .development, .staging:
            return AppConstants.appVersionBuild
        case .production:
            return AppConstants.appVersion
        }
    }
}
