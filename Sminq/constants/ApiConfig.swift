//
//  ApiConfig.swift
//  Sminq
//
//  Created by SMINQ iOS on 13/03/18.
//  Copyright © 2018 Suraj Sagare. All rights reserved.
//

import Foundation
import FirebaseRemoteConfig

enum ENVIRONMENT {
    case development
    case staging
    case production
}

public class ApiConfig {
    static let appEnvironment: ENVIRONMENT = ENVIRONMENT.production

    static let mapsGoogleAPI = "https://maps.googleapis.com"
    
    static let queueingAPIEndPoint = "https://queues.markk.io"
    
    // staging
    static let stagingCloudinaryCloudName = RemoteConfig.remoteConfig().configValue(forKey: "cloudinary_cloud_name_ios").stringValue
    static let stagingCloudinaryAPIKey = RemoteConfig.remoteConfig().configValue(forKey: "cloudinary_api_key_ios").stringValue
    static let stagingCloudinarySecretKey = RemoteConfig.remoteConfig().configValue(forKey: "cloudinary_api_secret_ios").stringValue
    
    // production
    static let productionCloudinaryCloudName = RemoteConfig.remoteConfig().configValue(forKey: "cloudinary_cloud_name_ios").stringValue
    static let productionCloudinaryAPIKey = RemoteConfig.remoteConfig().configValue(forKey: "cloudinary_api_key_ios").stringValue
    static let productionCloudinarySecretKey = RemoteConfig.remoteConfig().configValue(forKey: "cloudinary_api_secret_ios").stringValue
    
    static let stagingGoogleKey = "AIzaSyDIzrqGxRf_fAsNdN1crUduCZXml7ZOH1o"
//    static let productionGoogleKey = ""
    
    static let remoteconfigDefaults = [
        "force_update_ios_appstoreurl": RemoteConfigAppDefaults.shared.getDefaultAppStoreURLForRemoteConfig(),
        "force_update_ios_versioncode": RemoteConfigAppDefaults.shared.getDefaultAppVersionForRemoteConfig(),
        "cloudinary_cloud_name_ios": RemoteConfigAppDefaults.shared.getDefaultCloudinaryCloudNameIOS(),
        "cloudinary_api_key_ios": RemoteConfigAppDefaults.shared.getDefaultCloudinaryAPIKeyIOS(),
        "cloudinary_api_secret_ios": RemoteConfigAppDefaults.shared.getDefaultCloudinaryAPISecretIOS(),
        "countries_for_miles": RemoteConfigDefaults.countriesForMiles,
        "homescreen_nearby_radius_meters": RemoteConfigDefaults.homeScreenNearbyRadiusMeters,
        "app_initial_location_home_screen": RemoteConfigAppDefaults.shared.getAppInitialLocationHomeScreen(),
        "feed_media_cache_invalidate_in_hours_ios": RemoteConfigDefaults.feedCacheInvalidateInHours,
        "feed_media_cache_max_buffer_size_in_mbs_ios": RemoteConfigDefaults.feedCacheMaxBufferSizeInMbs,
        "feed_media_cache_delete_count_ios": "\(RemoteConfigDefaults.feedCacheDeleteCount)",
        "cloudinary_original_img_folder" : RemoteConfigDefaults.cloudinaryOriginalImageFolder,
        "cloudinary_video_folder": RemoteConfigDefaults.cloudinaryVideoFolder,
        "cloudinary_user_profile_image_preset": RemoteConfigAppDefaults.shared.getDefaultCloudinaryUserProfileImagePreset(),
        "cloudinary_rating_video_preset": RemoteConfigAppDefaults.shared.getDefaultCloudinaryRatingVideoPreset(),
        "cloudinary_rating_image_preset": RemoteConfigAppDefaults.shared.getDefaultCloudinaryRatingImagePreset(),
        "gallery_media_in_hours": String(RemoteConfigDefaults.galleryMediaAccessInterval),
        "new_post_ttl_minutes": String(RemoteConfigDefaults.pendingPostDiscardingInterval),
        "google_api_key": ApiConfig.stagingGoogleKey,
        "subscribe_banner_image": RemoteConfigDefaults.subscribeBannerImage
        
    ] as [String: NSObject]
}
