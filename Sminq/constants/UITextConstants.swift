//
//  UITextConstants.swift
//  Sminq
//
//  Created by Pushkar Deshmukh on 07/03/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import Foundation

class UITextConstants {
    static let addLiveRating = "To add a live rating,\n tap on the camera icon."
    static let userAddLiveRating = "Rate a place live on Markk.\n Tap on the camera icon."
}
