//
//  Config.swift
//  Markk
//
//  Created by Pushkar Deshmukh on 27/06/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import Foundation
import UIKit

class Config {
    static func getGoogleAPIKey() -> String {
        return FIRRemoteConfigService.shared.getGoogleAPIKey()
    }
    
    static func getAppsFlyerDevKey() -> String {
        switch ApiConfig.appEnvironment {
        case .staging, .development:
            return UIApplication.shared.getStagingAppsFlyerDevKey()
        case .production:
            return UIApplication.shared.getProductionAppsFlyerDevKey()
        }
    }
    
    static func getAppsFlyerAppleId() -> String {
        switch ApiConfig.appEnvironment {
        case .staging, .development:
            return UIApplication.shared.getStagingAppsFlyerAppleAppID()
        case .production:
            return UIApplication.shared.getProductionAppsFlyerAppleAppID()
        }
    }
    
    static func getPusherAppKey() -> String {
        switch ApiConfig.appEnvironment {
        case .staging, .development:
            return UIApplication.shared.getStagingPusherAppKey()
        case .production:
            return UIApplication.shared.getProductionPusherAppKey()
        }
    }
    
    static func getPusherAppCluster() -> String {
        return UIApplication.shared.getPusherAppCluster()
    }
    
    static func getFaqUrl() -> String {
        switch ApiConfig.appEnvironment {
        case .staging, .development:
            return UIApplication.shared.getStagingFaqURL()
        case .production:
            return UIApplication.shared.getProductionFaqURL()
        }
    }
    
}
