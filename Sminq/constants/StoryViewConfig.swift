//
//  StoryViewConfig.swift
//  Markk
//
//  Created by Pushkar Deshmukh on 27/05/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import Foundation
import UIKit

enum SnapMovementDirectionState {
    case forward
    case backward
}

class StoryViewConfig {
    public static let panGestureOffset: CGFloat = 70.0
    public static let storyViewLatestCommentsCount = 3
    public static let commentTextViewMinHeight: CGFloat = 72.0
    public static let commentTextViewMaxHeight: CGFloat = 132.0
}

struct StoryShareOption {
    var id: StoryShareOptionID
    var imageString: String
    var label: String
    
    func toDictionary() -> [String : Any] {
        let dictionary = [
            "id": self.id.rawValue,
            "imageString": self.imageString,
            "label": self.label
            ] as [String : Any]
        
        return dictionary
    }
}

enum StoryShareOptionID: String {
    case instagramStories = "instagram-stories"
    case messages = "messages"
    case twitter = "twitter"
    case copyLink = "copy-link"
    case more = "more"
    
}
