//
//  APIResponseState.swift
//  Sminq
//
//  Created by Pushkar Deshmukh on 19/02/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import Foundation

enum APIResponseState: Int {
    case success = 1, apiError, networkError, jsonParseError, noInternet
}

//enum APIError: Error {
//    case timeout
//    case unknown
//    
//    static func checkErrorCode(_ errorCode: Int) -> APIError {
//        switch errorCode {
//        case 408: return .timeout
//        default: return .unknown
//        }
//    }
//}

enum APIErrorType: String {
    case jsonParsingError = "Parsing Error",
    noNetwork = "No Network",
    timeout = "Timeout",
    authorization = "Authorization",
    validation = "Validation",
    serverError = "Server Error",
    unknown = "Unknown"
}
