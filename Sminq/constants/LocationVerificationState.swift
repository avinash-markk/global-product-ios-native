//
//  LocationVerificationState.swift
//  Sminq
//
//  Created by Pushkar Deshmukh on 12/02/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import Foundation

enum LocationVerificationState: String, Codable {
    case pending = "PENDING"
    case success = "SUCCESS"
    case failed = "FAILED"
}
