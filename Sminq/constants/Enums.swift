//
//  Enums.swift
//  Sminq
//
//  Created by Pushkar Deshmukh on 18/05/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import Foundation

enum MediaType: String {
    case image = "image"
    case video = "video"
}

enum OtherAppShareKeys: String {
    case instagramStories = "instagram-stories://share"
    case twitterPost = "twitter://post"
}
