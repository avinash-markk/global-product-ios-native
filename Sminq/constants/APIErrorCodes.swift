//
//  APIErrorCodes.swift
//  Sminq
//
//  Created by Pushkar Deshmukh on 25/03/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import Foundation

struct ErrorObject {
    public var apiName: String?
    public var errorType: APIErrorType
    public var httpCode: Int?
    public var errorTitle: String
    public var errorDescription: String
}

class APIErrorCodes {
    static func getErrorObject(apiName: String, httpCode: Int) -> ErrorObject? {
        var errors = [String: [Int: [String: Any]]]()
        
        errors["\(apiName)"] = self.getErrorDictionary(apiName: apiName, httpCode: httpCode)
        
        var errorIntDict: [Int: [String: Any]] = errors.valueForKeyPath(keyPath: apiName) as? [Int : [String : Any]] ?? [:]
        let errorStringDict: [String: Any] = errorIntDict[httpCode] ?? [:]
        
        guard let errorType = errorStringDict.valueForKeyPath(keyPath: AppConstants.ErrorUserInfoKeys.errorType) as? String,
            let httpCode = errorStringDict.valueForKeyPath(keyPath: AppConstants.ErrorUserInfoKeys.errorCode) as? Int,
            let errorTitle = errorStringDict.valueForKeyPath(keyPath: AppConstants.ErrorUserInfoKeys.errorTitle) as? String,
            let errorDescription = errorStringDict.valueForKeyPath(keyPath: AppConstants.ErrorUserInfoKeys.errorDescription) as? String else { return nil }
        
        let errorObject = ErrorObject.init(apiName: apiName, errorType: APIErrorType(rawValue: errorType) ?? .unknown, httpCode: httpCode, errorTitle: errorTitle, errorDescription: errorDescription)
        
        return errorObject
    }
    
    
//    static func getErrorDictionary(apiName: String, httpCode: Int) -> [Int: [String: Any]] {
//        var errorDict = [Int: [String: Any]]()
//
//        switch  apiName {
//        case AppConstants.apiNames.verifyDisplayName:
//            switch httpCode {
//            case 408:
//                errorDict.updateValue([:], forKey: httpCode)
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorCode] = httpCode
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorType] = APIErrorType.timeout.rawValue
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorTitle] = AppConstants.ErrorMessages.opsSomethingWentWrong
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorDescription] = AppConstants.ErrorMessages.troubleConnectingAndTapToRetry
//
//            case 401:
//                errorDict.updateValue([:], forKey: httpCode)
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorCode] = httpCode
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorType] = APIErrorType.authorization.rawValue
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorTitle] = ""
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorDescription] = AppConstants.ErrorMessages.unauthoriseRequest
//            case 400:
//                errorDict.updateValue([:], forKey: httpCode)
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorCode] = httpCode
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorType] = APIErrorType.validation.rawValue
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorTitle] = ""
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorDescription] = AppConstants.ErrorMessages.couldNotFetchData
//            case 409:
//                errorDict.updateValue([:], forKey: httpCode)
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorCode] = httpCode
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorType] = APIErrorType.validation.rawValue
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorTitle] = ""
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorDescription] = "This display name is not available. Please try another name."
//            case 500:
//                errorDict.updateValue([:], forKey: httpCode)
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorCode] = httpCode
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorType] = APIErrorType.validation.rawValue
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorTitle] = ""
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorDescription] = AppConstants.ErrorMessages.serverBusyPleaseTryAgain
//
//            default:
//                errorDict.updateValue([:], forKey: httpCode)
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorCode] = httpCode
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorType] = APIErrorType.unknown.rawValue
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorTitle] = AppConstants.ErrorMessages.opsSomethingWentWrong
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorDescription] = AppConstants.ErrorMessages.serverBusyPleaseTryAgain
//            }
//
//        case AppConstants.apiNames.getUserTimeline:
//            switch httpCode {
//            case 408:
//                errorDict.updateValue([:], forKey: httpCode)
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorCode] = httpCode
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorType] = APIErrorType.timeout.rawValue
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorTitle] = AppConstants.ErrorMessages.opsSomethingWentWrong
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorDescription] = AppConstants.ErrorMessages.troubleConnectingAndTapToRetry
//
//            case 401:
//                errorDict.updateValue([:], forKey: httpCode)
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorCode] = httpCode
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorType] = APIErrorType.authorization.rawValue
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorTitle] = ""
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorDescription] = AppConstants.ErrorMessages.unauthoriseRequest
//            case 400:
//                errorDict.updateValue([:], forKey: httpCode)
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorCode] = httpCode
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorType] = APIErrorType.validation.rawValue
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorTitle] = ""
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorDescription] = "Sorry, place ratings could not be loaded. Please try again."
//            case 500:
//                errorDict.updateValue([:], forKey: httpCode)
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorCode] = httpCode
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorType] = APIErrorType.validation.rawValue
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorTitle] = ""
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorDescription] = "Too many ratings! Server busy. Please try again."
//
//            default:
//                errorDict.updateValue([:], forKey: httpCode)
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorCode] = httpCode
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorType] = APIErrorType.unknown.rawValue
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorTitle] = AppConstants.ErrorMessages.opsSomethingWentWrong
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorDescription] = AppConstants.ErrorMessages.serverBusyPleaseTryAgain
//            }
//
//        case AppConstants.apiNames.getUserDetails:
//            switch httpCode {
//            case 408:
//                errorDict.updateValue([:], forKey: httpCode)
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorCode] = httpCode
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorType] = APIErrorType.timeout.rawValue
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorTitle] = AppConstants.ErrorMessages.opsSomethingWentWrong
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorDescription] = AppConstants.ErrorMessages.troubleConnectingAndTapToRetry
//
//            case 401:
//                errorDict.updateValue([:], forKey: httpCode)
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorCode] = httpCode
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorType] = APIErrorType.authorization.rawValue
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorTitle] = ""
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorDescription] = AppConstants.ErrorMessages.unauthoriseRequest
//            case 400:
//                errorDict.updateValue([:], forKey: httpCode)
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorCode] = httpCode
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorType] = APIErrorType.validation.rawValue
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorTitle] = ""
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorDescription] = AppConstants.ErrorMessages.couldNotFetchData
//            case 404:
//                errorDict.updateValue([:], forKey: httpCode)
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorCode] = httpCode
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorType] = APIErrorType.validation.rawValue
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorTitle] = ""
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorDescription] = "Sorry username could not be found."
//            case 500:
//                errorDict.updateValue([:], forKey: httpCode)
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorCode] = httpCode
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorType] = APIErrorType.serverError.rawValue
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorTitle] = ""
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorDescription] = AppConstants.ErrorMessages.serverBusyPleaseTryAgain
//
//            default:
//                errorDict.updateValue([:], forKey: httpCode)
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorCode] = httpCode
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorType] = APIErrorType.unknown.rawValue
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorTitle] = AppConstants.ErrorMessages.opsSomethingWentWrong
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorDescription] = AppConstants.ErrorMessages.serverBusyPleaseTryAgain
//            }
//
//        case AppConstants.apiNames.loginUser:
//            switch httpCode {
//            case 408:
//                errorDict.updateValue([:], forKey: httpCode)
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorCode] = httpCode
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorType] = APIErrorType.timeout.rawValue
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorTitle] = AppConstants.ErrorMessages.opsSomethingWentWrong
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorDescription] = AppConstants.ErrorMessages.troubleConnecting
//
//            case 401:
//                errorDict.updateValue([:], forKey: httpCode)
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorCode] = httpCode
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorType] = APIErrorType.authorization.rawValue
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorTitle] = ""
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorDescription] = AppConstants.ErrorMessages.unauthoriseRequest
//            case 400:
//                errorDict.updateValue([:], forKey: httpCode)
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorCode] = httpCode
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorType] = APIErrorType.validation.rawValue
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorTitle] = ""
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorDescription] = AppConstants.ErrorMessages.couldNotFetchData
//            case 500:
//                errorDict.updateValue([:], forKey: httpCode)
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorCode] = httpCode
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorType] = APIErrorType.serverError.rawValue
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorTitle] = ""
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorDescription] = AppConstants.ErrorMessages.serverBusyPleaseTryAgain
//
//            default:
//                errorDict.updateValue([:], forKey: httpCode)
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorCode] = httpCode
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorType] = APIErrorType.unknown.rawValue
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorTitle] = AppConstants.ErrorMessages.opsSomethingWentWrong
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorDescription] = AppConstants.ErrorMessages.serverBusyPleaseTryAgain
//            }
//
//        case AppConstants.apiNames.updateUserProfile:
//            switch httpCode {
//            case 408:
//                errorDict.updateValue([:], forKey: httpCode)
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorCode] = httpCode
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorType] = APIErrorType.timeout.rawValue
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorTitle] = AppConstants.ErrorMessages.facingConnectivity
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorDescription] = AppConstants.ErrorMessages.troubleConnecting
//
//            case 401:
//                errorDict.updateValue([:], forKey: httpCode)
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorCode] = httpCode
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorType] = APIErrorType.authorization.rawValue
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorTitle] = ""
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorDescription] = AppConstants.ErrorMessages.unauthoriseRequest
//            case 400:
//                errorDict.updateValue([:], forKey: httpCode)
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorCode] = httpCode
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorType] = APIErrorType.validation.rawValue
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorTitle] = ""
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorDescription] = AppConstants.ErrorMessages.couldNotFetchData
//            case 409:
//                errorDict.updateValue([:], forKey: httpCode)
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorCode] = httpCode
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorType] = APIErrorType.validation.rawValue
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorTitle] = "This display name is not available. Please try another name."
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorDescription] = AppConstants.ErrorMessages.couldNotFetchData
//            case 500:
//                errorDict.updateValue([:], forKey: httpCode)
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorCode] = httpCode
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorType] = APIErrorType.serverError.rawValue
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorTitle] = ""
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorDescription] = AppConstants.ErrorMessages.serverBusyPleaseTryAgain
//
//            default:
//                errorDict.updateValue([:], forKey: httpCode)
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorCode] = httpCode
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorType] = APIErrorType.unknown.rawValue
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorTitle] = AppConstants.ErrorMessages.opsSomethingWentWrong
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorDescription] = AppConstants.ErrorMessages.serverBusyPleaseTryAgain
//            }
//
//        case AppConstants.apiNames.upvotePost:
//            switch httpCode {
//            case 408:
//                errorDict.updateValue([:], forKey: httpCode)
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorCode] = httpCode
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorType] = APIErrorType.timeout.rawValue
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorTitle] = AppConstants.ErrorMessages.opsSomethingWentWrong
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorDescription] = AppConstants.ErrorMessages.troubleConnecting
//
//            case 401:
//                errorDict.updateValue([:], forKey: httpCode)
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorCode] = httpCode
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorType] = APIErrorType.authorization.rawValue
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorTitle] = ""
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorDescription] = AppConstants.ErrorMessages.unauthoriseRequest
//            case 400:
//                errorDict.updateValue([:], forKey: httpCode)
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorCode] = httpCode
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorType] = APIErrorType.validation.rawValue
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorTitle] = ""
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorDescription] = "Could not like the post, please try again."
//            case 404:
//                errorDict.updateValue([:], forKey: httpCode)
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorCode] = httpCode
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorType] = APIErrorType.validation.rawValue
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorTitle] = ""
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorDescription] = "Rating no longer exists. Looks like someone changed their mind!"
//            case 409:
//                errorDict.updateValue([:], forKey: httpCode)
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorCode] = httpCode
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorType] = APIErrorType.validation.rawValue
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorTitle] = ""
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorDescription] = "You have already liked this post."
//            case 500:
//                errorDict.updateValue([:], forKey: httpCode)
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorCode] = httpCode
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorType] = APIErrorType.serverError.rawValue
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorTitle] = ""
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorDescription] = "Too many ratings! Server busy. Please try again."
//
//            default:
//                errorDict.updateValue([:], forKey: httpCode)
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorCode] = httpCode
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorType] = APIErrorType.unknown.rawValue
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorTitle] = AppConstants.ErrorMessages.opsSomethingWentWrong
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorDescription] = AppConstants.ErrorMessages.serverBusyPleaseTryAgain
//            }
//
//        case AppConstants.apiNames.downvotePost:
//            switch httpCode {
//            case 408:
//                errorDict.updateValue([:], forKey: httpCode)
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorCode] = httpCode
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorType] = APIErrorType.timeout.rawValue
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorTitle] = AppConstants.ErrorMessages.opsSomethingWentWrong
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorDescription] = AppConstants.ErrorMessages.troubleConnecting
//
//            case 401:
//                errorDict.updateValue([:], forKey: httpCode)
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorCode] = httpCode
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorType] = APIErrorType.authorization.rawValue
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorTitle] = ""
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorDescription] = AppConstants.ErrorMessages.unauthoriseRequest
//            case 400:
//                errorDict.updateValue([:], forKey: httpCode)
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorCode] = httpCode
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorType] = APIErrorType.validation.rawValue
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorTitle] = ""
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorDescription] = "Could not report the post, please try again."
//            case 404:
//                errorDict.updateValue([:], forKey: httpCode)
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorCode] = httpCode
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorType] = APIErrorType.validation.rawValue
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorTitle] = ""
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorDescription] = "Rating no longer exists. Looks like someone changed their mind!"
//            case 409:
//                errorDict.updateValue([:], forKey: httpCode)
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorCode] = httpCode
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorType] = APIErrorType.validation.rawValue
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorTitle] = ""
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorDescription] = "You have already reported for this post."
//            case 500:
//                errorDict.updateValue([:], forKey: httpCode)
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorCode] = httpCode
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorType] = APIErrorType.serverError.rawValue
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorTitle] = ""
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorDescription] = "Too many ratings! Server busy. Please try again."
//
//            default:
//                errorDict.updateValue([:], forKey: httpCode)
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorCode] = httpCode
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorType] = APIErrorType.unknown.rawValue
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorTitle] = AppConstants.ErrorMessages.opsSomethingWentWrong
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorDescription] = AppConstants.ErrorMessages.serverBusyPleaseTryAgain
//            }
//
//        case AppConstants.apiNames.deletePost:
//            switch httpCode {
//            case 408:
//                errorDict.updateValue([:], forKey: httpCode)
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorCode] = httpCode
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorType] = APIErrorType.timeout.rawValue
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorTitle] = AppConstants.ErrorMessages.opsSomethingWentWrong
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorDescription] = AppConstants.ErrorMessages.troubleConnecting
//
//            case 401:
//                errorDict.updateValue([:], forKey: httpCode)
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorCode] = httpCode
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorType] = APIErrorType.authorization.rawValue
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorTitle] = ""
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorDescription] = AppConstants.ErrorMessages.unauthoriseRequest
//            case 400:
//                errorDict.updateValue([:], forKey: httpCode)
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorCode] = httpCode
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorType] = APIErrorType.validation.rawValue
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorTitle] = ""
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorDescription] = "Sorry deletion failed. Please try again."
//            case 404:
//                errorDict.updateValue([:], forKey: httpCode)
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorCode] = httpCode
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorType] = APIErrorType.validation.rawValue
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorTitle] = ""
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorDescription] = "Rating no longer exists. Looks like someone changed their mind!"
//            case 500:
//                errorDict.updateValue([:], forKey: httpCode)
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorCode] = httpCode
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorType] = APIErrorType.serverError.rawValue
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorTitle] = ""
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorDescription] = "Too many ratings! Server busy. Please try again."
//
//            default:
//                errorDict.updateValue([:], forKey: httpCode)
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorCode] = httpCode
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorType] = APIErrorType.unknown.rawValue
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorTitle] = AppConstants.ErrorMessages.opsSomethingWentWrong
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorDescription] = AppConstants.ErrorMessages.serverBusyPleaseTryAgain
//            }
//
//        case AppConstants.apiNames.sharePost:
//            switch httpCode {
//            case 408:
//                errorDict.updateValue([:], forKey: httpCode)
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorCode] = httpCode
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorType] = APIErrorType.timeout.rawValue
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorTitle] = AppConstants.ErrorMessages.opsSomethingWentWrong
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorDescription] = AppConstants.ErrorMessages.troubleConnecting
//
//            case 401:
//                errorDict.updateValue([:], forKey: httpCode)
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorCode] = httpCode
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorType] = APIErrorType.authorization.rawValue
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorTitle] = ""
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorDescription] = AppConstants.ErrorMessages.unauthoriseRequest
//            case 400:
//                errorDict.updateValue([:], forKey: httpCode)
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorCode] = httpCode
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorType] = APIErrorType.validation.rawValue
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorTitle] = ""
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorDescription] = "could not share the rating, please try again."
//            case 404:
//                errorDict.updateValue([:], forKey: httpCode)
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorCode] = httpCode
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorType] = APIErrorType.validation.rawValue
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorTitle] = ""
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorDescription] = "Rating no longer exists. Looks like someone changed their mind!"
//            case 500:
//                errorDict.updateValue([:], forKey: httpCode)
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorCode] = httpCode
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorType] = APIErrorType.serverError.rawValue
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorTitle] = ""
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorDescription] = "Too many ratings! Server busy. Please try again."
//
//            default:
//                errorDict.updateValue([:], forKey: httpCode)
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorCode] = httpCode
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorType] = APIErrorType.unknown.rawValue
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorTitle] = AppConstants.ErrorMessages.opsSomethingWentWrong
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorDescription] = AppConstants.ErrorMessages.serverBusyPleaseTryAgain
//            }
//
//        case AppConstants.apiNames.getStoryDetails:
//            switch httpCode {
//            case 408:
//                errorDict.updateValue([:], forKey: httpCode)
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorCode] = httpCode
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorType] = APIErrorType.timeout.rawValue
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorTitle] = AppConstants.ErrorMessages.opsSomethingWentWrong
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorDescription] = AppConstants.ErrorMessages.troubleConnecting
//
//            case 401:
//                errorDict.updateValue([:], forKey: httpCode)
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorCode] = httpCode
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorType] = APIErrorType.authorization.rawValue
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorTitle] = ""
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorDescription] = AppConstants.ErrorMessages.unauthoriseRequest
//            case 400:
//                errorDict.updateValue([:], forKey: httpCode)
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorCode] = httpCode
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorType] = APIErrorType.validation.rawValue
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorTitle] = ""
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorDescription] = "Sorry, place ratings could not be loaded. Please try again."
//            case 404:
//                errorDict.updateValue([:], forKey: httpCode)
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorCode] = httpCode
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorType] = APIErrorType.validation.rawValue
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorTitle] = ""
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorDescription] = "Rating no longer exists. Looks like someone changed their mind!"
//            case 500:
//                errorDict.updateValue([:], forKey: httpCode)
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorCode] = httpCode
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorType] = APIErrorType.serverError.rawValue
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorTitle] = ""
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorDescription] = "Too many ratings! Server busy. Please try again."
//
//            default:
//                errorDict.updateValue([:], forKey: httpCode)
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorCode] = httpCode
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorType] = APIErrorType.unknown.rawValue
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorTitle] = AppConstants.ErrorMessages.opsSomethingWentWrong
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorDescription] = AppConstants.ErrorMessages.serverBusyPleaseTryAgain
//            }
//
//        case AppConstants.apiNames.getLiveStoriesList:
//            switch httpCode {
//            case 408:
//                errorDict.updateValue([:], forKey: httpCode)
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorCode] = httpCode
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorType] = APIErrorType.timeout.rawValue
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorTitle] = AppConstants.ErrorMessages.opsSomethingWentWrong
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorDescription] = AppConstants.ErrorMessages.troubleConnecting
//
//            case 401:
//                errorDict.updateValue([:], forKey: httpCode)
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorCode] = httpCode
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorType] = APIErrorType.authorization.rawValue
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorTitle] = ""
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorDescription] = AppConstants.ErrorMessages.unauthoriseRequest
//            case 400:
//                errorDict.updateValue([:], forKey: httpCode)
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorCode] = httpCode
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorType] = APIErrorType.validation.rawValue
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorTitle] = ""
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorDescription] = "Sorry, place ratings could not be loaded. Please try again."
//            case 500:
//                errorDict.updateValue([:], forKey: httpCode)
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorCode] = httpCode
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorType] = APIErrorType.serverError.rawValue
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorTitle] = ""
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorDescription] = "Too many ratings! Server busy. Please try again."
//
//            default:
//                errorDict.updateValue([:], forKey: httpCode)
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorCode] = httpCode
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorType] = APIErrorType.unknown.rawValue
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorTitle] = AppConstants.ErrorMessages.opsSomethingWentWrong
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorDescription] = AppConstants.ErrorMessages.serverBusyPleaseTryAgain
//            }
//
//        case AppConstants.apiNames.getPlaceStories:
//            switch httpCode {
//            case 408:
//                errorDict.updateValue([:], forKey: httpCode)
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorCode] = httpCode
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorType] = APIErrorType.timeout.rawValue
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorTitle] = AppConstants.ErrorMessages.opsSomethingWentWrong
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorDescription] = AppConstants.ErrorMessages.troubleConnecting
//
//            case 401:
//                errorDict.updateValue([:], forKey: httpCode)
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorCode] = httpCode
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorType] = APIErrorType.authorization.rawValue
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorTitle] = ""
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorDescription] = AppConstants.ErrorMessages.unauthoriseRequest
//            case 400:
//                errorDict.updateValue([:], forKey: httpCode)
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorCode] = httpCode
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorType] = APIErrorType.validation.rawValue
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorTitle] = ""
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorDescription] = "Sorry, place ratings could not be loaded. Please try again."
//            case 404:
//                errorDict.updateValue([:], forKey: httpCode)
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorCode] = httpCode
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorType] = APIErrorType.validation.rawValue
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorTitle] = ""
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorDescription] = "Sorry! We couldn't find this place."
//            case 500:
//                errorDict.updateValue([:], forKey: httpCode)
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorCode] = httpCode
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorType] = APIErrorType.serverError.rawValue
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorTitle] = ""
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorDescription] = "Too many ratings! Server busy. Please try again."
//
//            default:
//                errorDict.updateValue([:], forKey: httpCode)
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorCode] = httpCode
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorType] = APIErrorType.unknown.rawValue
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorTitle] = AppConstants.ErrorMessages.opsSomethingWentWrong
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorDescription] = AppConstants.ErrorMessages.serverBusyPleaseTryAgain
//            }
//
//        case AppConstants.apiNames.getUserArchivePosts:
//            switch httpCode {
//            case 408:
//                errorDict.updateValue([:], forKey: httpCode)
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorCode] = httpCode
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorType] = APIErrorType.timeout.rawValue
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorTitle] = AppConstants.ErrorMessages.opsSomethingWentWrong
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorDescription] = AppConstants.ErrorMessages.troubleConnecting
//
//            case 401:
//                errorDict.updateValue([:], forKey: httpCode)
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorCode] = httpCode
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorType] = APIErrorType.authorization.rawValue
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorTitle] = ""
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorDescription] = AppConstants.ErrorMessages.unauthoriseRequest
//            case 400:
//                errorDict.updateValue([:], forKey: httpCode)
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorCode] = httpCode
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorType] = APIErrorType.validation.rawValue
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorTitle] = ""
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorDescription] = "Sorry, place ratings could not be loaded. Please try again."
//            case 404:
//                errorDict.updateValue([:], forKey: httpCode)
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorCode] = httpCode
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorType] = APIErrorType.validation.rawValue
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorTitle] = ""
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorDescription] = "Sorry! We couldn't find this place."
//            case 500:
//                errorDict.updateValue([:], forKey: httpCode)
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorCode] = httpCode
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorType] = APIErrorType.serverError.rawValue
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorTitle] = ""
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorDescription] = "Too many ratings! Server busy. Please try again."
//
//            default:
//                errorDict.updateValue([:], forKey: httpCode)
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorCode] = httpCode
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorType] = APIErrorType.unknown.rawValue
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorTitle] = AppConstants.ErrorMessages.opsSomethingWentWrong
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorDescription] = AppConstants.ErrorMessages.serverBusyPleaseTryAgain
//            }
//
//        case AppConstants.apiNames.addComment:
//            switch httpCode {
//            case 408:
//                errorDict.updateValue([:], forKey: httpCode)
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorCode] = httpCode
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorType] = APIErrorType.timeout.rawValue
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorTitle] = AppConstants.ErrorMessages.opsSomethingWentWrong
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorDescription] = AppConstants.ErrorMessages.troubleConnecting
//
//            case 401:
//                errorDict.updateValue([:], forKey: httpCode)
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorCode] = httpCode
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorType] = APIErrorType.authorization.rawValue
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorTitle] = ""
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorDescription] = AppConstants.ErrorMessages.unauthoriseRequest
//            case 400:
//                errorDict.updateValue([:], forKey: httpCode)
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorCode] = httpCode
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorType] = APIErrorType.validation.rawValue
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorTitle] = ""
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorDescription] = "could not add comment, please try again."
//            case 404:
//                errorDict.updateValue([:], forKey: httpCode)
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorCode] = httpCode
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorType] = APIErrorType.validation.rawValue
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorTitle] = ""
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorDescription] = "Rating no longer exists. Looks like someone changed their mind!"
//            case 500:
//                errorDict.updateValue([:], forKey: httpCode)
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorCode] = httpCode
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorType] = APIErrorType.serverError.rawValue
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorTitle] = ""
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorDescription] = "Too many ratings! Server busy. Please try again."
//
//            default:
//                errorDict.updateValue([:], forKey: httpCode)
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorCode] = httpCode
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorType] = APIErrorType.unknown.rawValue
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorTitle] = AppConstants.ErrorMessages.opsSomethingWentWrong
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorDescription] = AppConstants.ErrorMessages.serverBusyPleaseTryAgain
//            }
//
//        case AppConstants.apiNames.deleteComment:
//            switch httpCode {
//            case 408:
//                errorDict.updateValue([:], forKey: httpCode)
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorCode] = httpCode
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorType] = APIErrorType.timeout.rawValue
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorTitle] = AppConstants.ErrorMessages.opsSomethingWentWrong
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorDescription] = AppConstants.ErrorMessages.troubleConnecting
//
//            case 401:
//                errorDict.updateValue([:], forKey: httpCode)
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorCode] = httpCode
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorType] = APIErrorType.authorization.rawValue
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorTitle] = ""
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorDescription] = AppConstants.ErrorMessages.unauthoriseRequest
//            case 400:
//                errorDict.updateValue([:], forKey: httpCode)
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorCode] = httpCode
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorType] = APIErrorType.validation.rawValue
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorTitle] = ""
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorDescription] = "Comment deletion failed! Please try again."
//            case 404:
//                errorDict.updateValue([:], forKey: httpCode)
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorCode] = httpCode
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorType] = APIErrorType.validation.rawValue
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorTitle] = ""
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorDescription] = "Rating no longer exists. Looks like someone changed their mind!"
//            case 500:
//                errorDict.updateValue([:], forKey: httpCode)
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorCode] = httpCode
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorType] = APIErrorType.serverError.rawValue
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorTitle] = ""
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorDescription] = "Too many ratings! Server busy. Please try again."
//
//            default:
//                errorDict.updateValue([:], forKey: httpCode)
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorCode] = httpCode
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorType] = APIErrorType.unknown.rawValue
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorTitle] = AppConstants.ErrorMessages.opsSomethingWentWrong
//                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorDescription] = AppConstants.ErrorMessages.serverBusyPleaseTryAgain
//            }
//
//
//        default:
//            errorDict.updateValue([:], forKey: 0)
//            errorDict[0]?[AppConstants.ErrorUserInfoKeys.errorCode] = 0
//            errorDict[0]?[AppConstants.ErrorUserInfoKeys.errorType] = APIErrorType.unknown.rawValue
//            errorDict[0]?[AppConstants.ErrorUserInfoKeys.errorTitle] = AppConstants.ErrorMessages.opsSomethingWentWrong
//            errorDict[0]?[AppConstants.ErrorUserInfoKeys.errorDescription] = AppConstants.ErrorMessages.serverBusyPleaseTryAgain
//        }
//
//        return errorDict
//    }
    
    static func getErrorDictionary(apiName: String, httpCode: Int) -> [Int: [String: Any]] {
        var errorDict = [Int: [String: Any]]()
        errorDict.updateValue([:], forKey: httpCode)
        errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorCode] = httpCode
        errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorType] = APIErrorType.serverError.rawValue
        errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorTitle] = ""
        
        switch httpCode {
        case 400, 401:
            errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorDescription] = AppConstants.ErrorMessages.cantFindWhatYouLookingFor
        case 403:
            errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorDescription] = AppConstants.ErrorMessages.unauthoriseRequest
        case 500:
            errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorDescription] = AppConstants.ErrorMessages.serverError
        case 409:
            switch apiName {
            case AppConstants.apiNames.verifyDisplayName, AppConstants.apiNames.updateUserProfile:
                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorDescription] = AppConstants.ErrorMessages.displayNameNotAvailable
            case AppConstants.apiNames.upvotePost:
                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorDescription] = AppConstants.ErrorMessages.alreadyLikedRating
            case AppConstants.apiNames.downvotePost:
                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorDescription] = AppConstants.ErrorMessages.alreadyReporedRating
            default:
                errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorDescription] = AppConstants.ErrorMessages.serverError
            }
        case 408:
            errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorDescription] = AppConstants.ErrorMessages.timeout
        default:
            errorDict[httpCode]?[AppConstants.ErrorUserInfoKeys.errorDescription] = AppConstants.ErrorMessages.serverError
        }
        
        return errorDict
    }
}
