//
//  RemoteConfigAppDefaults.swift
//  Sminq
//
//  Created by Pushkar Deshmukh on 08/02/19.
//  Copyright © 2019 Sminq. All rights reserved.
//
import UIKit
import Foundation

class RemoteConfigAppDefaults {
    private init() {}
    
    static let shared = RemoteConfigAppDefaults()
    
    func getAppInitialLocationHomeScreen() -> String {
        switch ApiConfig.appEnvironment {
        case .staging, .development:
            return RemoteConfigDefaults.sminqAppInitialLocationHomeScreen
        case .production:
            return RemoteConfigDefaults.markkAppInitialLocationHomeScreen
        }
    }
    
    func getDefaultAppVersionForRemoteConfig() -> String {
        return Bundle.main.infoDictionary?["CFBundleShortVersionString"] as! String
    }
    
    func getDefaultAppStoreURLForRemoteConfig() -> String {
        return RemoteConfigDefaults.markkAppStoreUrl
    }
    
    func getDefaultCloudinaryCloudNameIOS() -> String {
        switch ApiConfig.appEnvironment {
        case .staging, .development:
            return RemoteConfigDefaults.stagingCloudinaryCloudNameIOS
        case .production:
            return RemoteConfigDefaults.productionCloudinaryCloudNameIOS
        }
    }
    
    func getDefaultCloudinaryAPIKeyIOS() -> String {
        switch ApiConfig.appEnvironment {
        case .staging, .development:
            return RemoteConfigDefaults.stagingCloudinaryAPIKeyIOS
        case .production:
            return RemoteConfigDefaults.productionCloudinaryAPIKeyIOS
        }
    }
    
    func getDefaultCloudinaryAPISecretIOS() -> String {
        switch ApiConfig.appEnvironment {
        case .staging, .development:
            return RemoteConfigDefaults.stagingCloudinaryAPISecretIOS
        case .production:
            return RemoteConfigDefaults.productionCloudinaryAPISecretIOS
        }
    }
    
    func getDefaultCloudinaryUserProfileImagePreset() -> String {
        switch ApiConfig.appEnvironment {
        case .staging, .development:
            return RemoteConfigDefaults.stagingCloudinaryUserProfileImagePreset
        case .production:
            return RemoteConfigDefaults.productionCloudinaryUserProfileImagePreset
        }
    }
    
    func getDefaultCloudinaryRatingVideoPreset() -> String {
        switch ApiConfig.appEnvironment {
        case .staging, .development:
            return RemoteConfigDefaults.stagingCloudinaryRatingVideoPreset
        case .production:
            return RemoteConfigDefaults.productionCloudinaryRatingVideoPreset
        }
    }
    
    func getDefaultCloudinaryRatingImagePreset() -> String {
        switch ApiConfig.appEnvironment {
        case .staging, .development:
            return RemoteConfigDefaults.stagingCloudinaryRatingImagePreset
        case .production:
            return RemoteConfigDefaults.productionCloudinaryRatingImagePreset
        }
    }
    
}
