//
//  CodeDataTables.swift
//  Sminq
//
//  Created by Pushkar Deshmukh on 07/02/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import Foundation

enum CoreDataTables: String {
    case notificationTable = "NotificationTable"
    case feedTable = "FeedTable"
}
