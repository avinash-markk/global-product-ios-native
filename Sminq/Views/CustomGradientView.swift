//
//  CustomGradientView.swift
//  Markk
//
//  Created by Pushkar Deshmukh on 27/05/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import UIKit

class CustomGradientView: UIView {
    var gradientLayer = CAGradientLayer()
    
    var colors = [CGColor]()
    var locations = [NSNumber]()
    
    init(colors: [CGColor], locations: [NSNumber]) {
        self.colors = colors
        self.locations = locations
        
        super.init(frame: .zero)
        
        setupLayout()
    }
    
    override func layoutSubviews() {
        gradientLayer.frame = self.bounds
    }
    
    
    // MARK: Fileprivate functions
    
    fileprivate func setupLayout() {
        gradientLayer.colors = self.colors
        gradientLayer.locations = self.locations
        layer.addSublayer(gradientLayer)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
