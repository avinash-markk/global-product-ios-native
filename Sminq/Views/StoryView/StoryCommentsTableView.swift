//
//  StoryCommentsTableView.swift
//  Markk
//
//  Created by Pushkar Deshmukh on 28/05/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import UIKit

protocol StoryCommentsTableViewDelegate: NSObjectProtocol {
    func didDeleteCommentTap(commentToDelete: Replies)
    func didCommentUserImageViewTap(comment: Replies)
    func didCommentUserInitialLabelTap(comment: Replies)
    func didCommentUserDisplayNameLabelTap(comment: Replies)
    func didComemntEmptyStateViewTap()
}

class StoryCommentsTableView: UIView {
    public weak var delegate: StoryCommentsTableViewDelegate?
    
    fileprivate lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.register(StoryCommentTableViewCell.self, forCellReuseIdentifier: "StoryCommentTableViewCell")
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        tableView.keyboardDismissMode = .onDrag
        tableView.separatorStyle = .none
        tableView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleTableViewTap)))
        tableView.contentInset = UIEdgeInsets.init(top: 0, left: 0, bottom: 16, right: 0)
        return tableView
    }()
    
    fileprivate lazy var emptyStateView: EmptyStateView = {
        let view = EmptyStateView()
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleEmptyStateViewTap)))
        view.setLabels(headingText: "", subHeadingText: "Have a question \nor a comment?")
        return view
    }()
    
    fileprivate var comments = [Replies]() {
        didSet {
            if comments.count > 0 {
                emptyStateView.isHidden = true
                
            } else {
                emptyStateView.isHidden = false
            }
            
            tableView.reloadData()
            scrollToBottom()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupLayout()
//        setupComments()
    }
    
    // MARK: Fileprivate functions
    
    fileprivate func setupLayout() {
        addSubview(tableView)
        tableView.anchor(top: topAnchor, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor)
        
        addSubview(emptyStateView)
        emptyStateView.anchor(top: topAnchor, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor, padding: .init(top: 16, left: 16, bottom: 16, right: 16))
    }
    
    fileprivate func scrollToBottom() {
        if comments.count > 0 {
            let count = comments.count
            let indexPath = IndexPath(row: count - 1, section: 0)
            tableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
        }
    }
    
    // MARK: Public functions
    
    public func addComment(comment: Replies) {
        comments.append(comment)
        scrollToBottom()
    }
    
    public func setComments(comments: [Replies]) {
        self.comments = comments
    }
    
    public func deleteComment(comment: Replies) {
        comments = comments.filter({ (reply) -> Bool in
            if reply._id == comment._id {
                return false
            } else {
                return true
            }
        })
    }
    
    // MARK: Selector functions
    
    @objc fileprivate func handleTableViewTap() {
        return
    }
    
    @objc fileprivate func handleEmptyStateViewTap() {
        delegate?.didComemntEmptyStateViewTap()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension StoryCommentsTableView: UITableViewDelegate {
    
}

extension StoryCommentsTableView: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return comments.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "StoryCommentTableViewCell", for: indexPath) as? StoryCommentTableViewCell else { return UITableViewCell() }
        cell.setComment(comment: comments[indexPath.row])
        cell.delegate = self
        return cell
    }
}

extension StoryCommentsTableView: StoryCommentTableViewCellDelegate {
    func didDeleteCommentTap(commentToDelete: Replies) {
//        deleteComment(comment: commentToDelete)
        delegate?.didDeleteCommentTap(commentToDelete: commentToDelete)
    }
    
    func didCommentUserImageViewTap(comment: Replies) {
        delegate?.didCommentUserImageViewTap(comment: comment)
    }
    
    func didCommentUserInitialLabelTap(comment: Replies) {
        delegate?.didCommentUserInitialLabelTap(comment: comment)
    }
    
    func didCommentUserDisplayNameLabelTap(comment: Replies) {
        delegate?.didCommentUserDisplayNameLabelTap(comment: comment)
    }
    
}
