//
//  StoryRatingExpiredStateView.swift
//  Markk
//
//  Created by Pushkar Deshmukh on 17/06/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import UIKit

enum StoryViewAction {
    case comment
    case like
    case share
}

class StoryRatingExpiredStateView: UIView {

    fileprivate lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "iconStoryDeleted48")
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()
    
    fileprivate lazy var label: UILabel = {
       let label = UILabel()
        label.numberOfLines = 2
        label.textAlignment = .center
        label.textColor = AppConstants.SminqColors.white100
        label.font = UIFont(name: AppConstants.Font.primaryRegular, size: 16)
        return label
    }()
    
    fileprivate var post: UserDataPlaceFeed? {
        didSet {
            updateData()
        }
    }
    
    fileprivate let deletedText = "Looks like this rating \n has been deleted."
    fileprivate let reportedText = "This rating has been reported \n for inappropriate content."
    fileprivate let expiredText = "Looks like this rating \n has expired."
    fileprivate let autoDismissTimeout: TimeInterval = 3
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupLayout()
        setupGestures()
    }
    
    // MARK: Fileprivate functions
    
    fileprivate func setupLayout() {
        backgroundColor = .clear
        isUserInteractionEnabled = true
        
        imageView.snp.makeConstraints { (make) in
            make.height.width.equalTo(48)
        }
        
        let stackView = UIStackView(arrangedSubviews: [imageView, label])
        stackView.axis = .vertical
        stackView.distribution = .fill
        stackView.alignment = .center
        stackView.spacing = 16
        stackView.isLayoutMarginsRelativeArrangement = true
        stackView.layoutMargins = .init(top: 32, left: 0, bottom: 32, right: 0)
        
        stackView.addBackground(color: AppConstants.SminqColors.grey100, cornerRadius: 8, borderColor: AppConstants.SminqColors.white100.withAlphaComponent(0.1), borderWidth: 1)
        
        addSubview(stackView)
        
        stackView.snp.makeConstraints { (make) in
            make.width.equalTo(283)
            make.centerX.centerY.equalTo(self)
        }
    }
    
    fileprivate func setupGestures() {
        addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(viewTapped)))
    }
    
    fileprivate func updateData() {
        guard let post = post else { return }
        if post.isPostDeletedLocalFlag {
            imageView.image = UIImage(named: "iconStoryDeleted48")
            label.text = deletedText
        } else if post.isPostReportedLocalFlag {
            imageView.image = UIImage(named: "iconStoryReported48")
            label.text = reportedText
        } else if post.expired {
            imageView.image = UIImage(named: "iconStoryExpired48")
            label.text = expiredText
        }
        
//        backgroundColor = AppConstants.SminqColors.blackLight100
    }
    
    // MARK: Public functions
    
    public func setPost(post: UserDataPlaceFeed) {
        self.post = post
    }
    
    public func show(storyViewAction: StoryViewAction) {
        switch storyViewAction {
        case .comment:
            label.text = "You can't comment on a rating \n once it has expired"
        case .like:
            label.text = "You can't like a rating \n once it has expired"
        case .share:
            label.text = "You can't share a rating \n once it has expired"
        }
        
        isHidden = false
        
        DispatchQueue.main.asyncAfter(deadline: .now() + autoDismissTimeout) {
            self.isHidden = true
        }
    }
    
    // MARK: @objc functions
    
    @objc fileprivate func viewTapped() {
        isHidden = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
