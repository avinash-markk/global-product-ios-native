//
//  StoryCommentsView+StoryCommentsTableViewDelegate.swift
//  Markk
//
//  Created by Pushkar Deshmukh on 21/06/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import Foundation

extension StoryCommentsView: StoryCommentsTableViewDelegate {
    func didComemntEmptyStateViewTap() {
        textView.resignFirstResponder()
    }
    
    func didDeleteCommentTap(commentToDelete: Replies) {
        delegate?.didDeleteCommentTap(commentToDelete: commentToDelete)
    }
    
    func didCommentUserImageViewTap(comment: Replies) {
        delegate?.didCommentUserImageViewTap(comment: comment)
    }
    
    func didCommentUserInitialLabelTap(comment: Replies) {
        delegate?.didCommentUserInitialLabelTap(comment: comment)
    }
    
    func didCommentUserDisplayNameLabelTap(comment: Replies) {
        delegate?.didCommentUserDisplayNameLabelTap(comment: comment)
    }
}
