//
//  StoryBaseProgressView.swift
//  Markk
//
//  Created by Pushkar Deshmukh on 30/05/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import UIKit

class StoryBaseProgressView: UIView {
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupLayout()
    }

    // MARK: Fileprivate functions
    
    fileprivate func setupLayout() {
        backgroundColor = UIColor.red.withAlphaComponent(0.1)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }


}
