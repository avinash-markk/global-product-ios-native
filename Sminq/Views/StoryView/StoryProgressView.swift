//
//  StoryProgressView.swift
//  Markk
//
//  Created by Pushkar Deshmukh on 30/05/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import UIKit

import Foundation
import UIKit

protocol StoryProgressViewDelegate: class {
    func storyProgressBarChangedIndex(index: Int)
    func storyProgressBarFinished()
    func storyProgressBarFinishedAt(index: Int)
}

class StoryProgressView: UIView {
    
    weak var delegate: StoryProgressViewDelegate?
    
    var topColor = UIColor.gray {
        didSet {
            self.updateColors()
        }
    }
    var bottomColor = UIColor.gray.withAlphaComponent(0.25) {
        didSet {
            self.updateColors()
        }
    }
    
    var durationArray = [TimeInterval]()
//    var animationProgressFlag = [Bool]()
    var duration: TimeInterval = 5
    
    var padding: CGFloat = 2.0
    var isPaused: Bool = false {
        didSet {
            if isPaused {
                for segment in segments {
                    let layer = segment.topSegmentView.layer
                    let pausedTime = layer.convertTime(CACurrentMediaTime(), from: nil)
                    layer.speed = 0.0
                    layer.timeOffset = pausedTime
                }
            } else {
                let segment = segments[currentAnimationIndex]
                let layer = segment.topSegmentView.layer
                let pausedTime = layer.timeOffset
                layer.speed = 1.0
                layer.timeOffset = 0.0
                layer.beginTime = 0.0
                let timeSincePause = layer.convertTime(CACurrentMediaTime(), from: nil) - pausedTime
                layer.beginTime = timeSincePause
            }
        }
    }
    
    private var segments = [Segment]()
    private var hasDoneLayout = false // hacky way to prevent layouting again
    private var currentAnimationIndex = 0
    
    init(numberOfSegments: Int, durationArray: [TimeInterval]) {
        self.durationArray = durationArray
        
        super.init(frame: CGRect.zero)
        
        for index in 0..<numberOfSegments {
            let segment = Segment()
            addSubview(segment.bottomSegmentView)
            addSubview(segment.topSegmentView)
            segments.append(segment)
//            animationProgressFlag.append(false)
        }
        self.updateColors()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        if hasDoneLayout {
            return
        }
        let width = (frame.width - (padding * CGFloat(segments.count - 1)) ) / CGFloat(segments.count)
        for (index, segment) in segments.enumerated() {
            let segFrame = CGRect(x: CGFloat(index) * (width + padding), y: 0, width: width, height: frame.height)
            segment.bottomSegmentView.frame = segFrame
            segment.topSegmentView.frame = segFrame
            segment.topSegmentView.frame.size.width = 0
            
            let cr = frame.height / 2
            segment.bottomSegmentView.layer.cornerRadius = cr
            segment.topSegmentView.layer.cornerRadius = cr
        }
        hasDoneLayout = true
    }
    
    func layoutProgressViews() {
        layoutSubviews()
    }
    
    func animate(animationIndex: Int = 0) {
        guard animationIndex < segments.count else { return }
//        print("in animate()", animationIndex)
//        print("Inherited animation, ", UIView.inheritedAnimationDuration)
        // NOTE: Clear all previous animation and fill till then current passed index
        clearAll()
        fillTill(index: animationIndex)
        let nextSegment = segments[animationIndex]
        currentAnimationIndex = animationIndex
        
        nextSegment.isAnimationPlaying = true
        self.isPaused = false // no idea why we have to do this here, but it fixes everything :D
//        print("Starting animation at index, ", animationIndex)
        UIView.animate(withDuration: durationArray[animationIndex], delay: 0.0, options: .curveLinear, animations: {
            nextSegment.topSegmentView.frame.size.width = nextSegment.bottomSegmentView.frame.width
        }) { (finished) in
            if !finished {
                return
            }
            
            nextSegment.isAnimationPlaying = false
            self.delegate?.storyProgressBarFinishedAt(index: animationIndex)
        }
    }
    
    public func getAnimateProgress(index: Int) -> CGFloat {
        if index < segments.count {
            return segments[index].topSegmentView.frame.size.width
        }
        return 0
    }
    
    private func updateColors() {
        for segment in segments {
            segment.topSegmentView.backgroundColor = topColor
            segment.bottomSegmentView.backgroundColor = bottomColor
        }
    }
    
    private func next() {
        let newIndex = self.currentAnimationIndex + 1
        if newIndex < self.segments.count {
            self.animate(animationIndex: newIndex)
            self.delegate?.storyProgressBarChangedIndex(index: newIndex)
        } else {
            self.delegate?.storyProgressBarFinished()
        }
    }
    
    func fillTill(index: Int) {
        for (segmentIndex, segment) in segments.enumerated() {
            if segmentIndex < index {
                segment.topSegmentView.frame.size.width = segment.bottomSegmentView.frame.width
            } else {
                segment.topSegmentView.frame.size.width = 0
            }
            segment.topSegmentView.layer.removeAllAnimations()
        }
        
        currentAnimationIndex = index
    }
    
    func skip() {
        let currentSegment = segments[currentAnimationIndex]
        currentSegment.topSegmentView.frame.size.width = currentSegment.bottomSegmentView.frame.width
        currentSegment.topSegmentView.layer.removeAllAnimations()
        self.next()
    }
    
    func rewind() {
        let currentSegment = segments[currentAnimationIndex]
        currentSegment.topSegmentView.layer.removeAllAnimations()
        currentSegment.topSegmentView.frame.size.width = 0
        let newIndex = max(currentAnimationIndex - 1, 0)
        let prevSegment = segments[newIndex]
        prevSegment.topSegmentView.frame.size.width = 0
        self.animate(animationIndex: newIndex)
        self.delegate?.storyProgressBarChangedIndex(index: newIndex)
    }
    
    func clearAll() {
        if segments.count > 0 {
            segments.forEach { (segment) in
                segment.topSegmentView.layer.removeAllAnimations()
                segment.topSegmentView.frame.size.width = 0
            }
        }
        currentAnimationIndex = 0
    }
    
    func pause() {
        let currentSegment = segments[currentAnimationIndex]
//        print("Animation pausing")
        currentSegment.topSegmentView.layer.pause()
        currentSegment.isAnimationPlaying = false
    }
    
    func resume() {
        // NOTE: If animation is already in progress then simply don't do anything
        
        let currentSegment = segments[currentAnimationIndex]
        if currentSegment.isAnimationPlaying { return }
//        print("Animation resuming")
        currentSegment.topSegmentView.layer.resume()
        currentSegment.isAnimationPlaying = true
    }
}

fileprivate class Segment {
    let bottomSegmentView = UIView()
    let topSegmentView = UIView()
    var isAnimationPlaying: Bool = false
    init() {
    }
}

