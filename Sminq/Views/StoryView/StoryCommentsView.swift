//
//  StoryCommentsView.swift
//  Markk
//
//  Created by Pushkar Deshmukh on 27/05/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import UIKit
import SVProgressHUD

protocol StoryCommentsViewDelegate: NSObjectProtocol {
    func didCloseStoryCommentsView()
    func didDeleteCommentTap(commentToDelete: Replies)
    func didFailToDeleteComment(error: ErrorObject)
    func didFailToAddComment(error: ErrorObject)
    func didCommentUserImageViewTap(comment: Replies)
    func didCommentUserInitialLabelTap(comment: Replies)
    func didCommentUserDisplayNameLabelTap(comment: Replies)
}

class StoryCommentsView: UIView {
    public weak var delegate: StoryCommentsViewDelegate?
    
    fileprivate lazy var overlayView: UIView = {
        let view = UIView()
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTap))
        view.addGestureRecognizer(tapGesture)
        return view
    }()
    
    fileprivate lazy var commentsTableView: StoryCommentsTableView = {
        let view = StoryCommentsTableView()
        view.delegate = self
        return view
    }()
    
    fileprivate lazy var bottomView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        
        view.shadowColor = AppConstants.SminqColors.greyishBrown
        view.shadowOpacity = 0.25
        view.shadowOffset = CGSize(width: 0.0, height: 0.0)
        view.shadowRadius = 8
        view.clipsToBounds = false
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(emptyTapGesture))
        view.addGestureRecognizer(tapGesture)
        return view
    }()

    fileprivate lazy var commentTextFieldRowView: UIView = {
        let view = UIView()
        view.borderColor = AppConstants.SminqColors.purple100.withAlphaComponent(0.75)
        view.borderWidth = 1
        view.layer.cornerRadius = 20
        view.clipsToBounds = true
        return view
    }()
    
    fileprivate lazy var sendButton: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(UIImage(named: "iconSend24Purple50")?.withRenderingMode(.alwaysOriginal), for: .normal)
        button.imageView?.contentMode = .scaleAspectFill
        button.addTarget(self, action: #selector(handleCommentSendAction), for: .touchUpInside)
        return button
    }()
    
    public lazy var textView: UITextView = {
        let textView = UITextView()
        textView.delegate = self
        textView.font = UIFont(name: AppConstants.Font.primaryRegular, size: 16)
        textView.textColor = AppConstants.SminqColors.purple100.withAlphaComponent(0.5)
        return textView
    }()
    
    fileprivate lazy var placeholderLabel: UILabel = {
        let label = UILabel()
        label.text = "Leave a comment"
        label.font = UIFont(name: AppConstants.Font.primaryRegular, size: 16)
        label.textColor = AppConstants.SminqColors.purple100.withAlphaComponent(0.5)
        return label
    }()
    
    fileprivate var post: UserDataPlaceFeed?
    fileprivate var storyData: StoryData?
    fileprivate var storyLatestCommentsView: StoryLatestCommentsView?
    fileprivate var storyBottomControlsView: StoryBottomControlsView?
    fileprivate var bottomViewHeightConstraintConstant: CGFloat = 0
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupLayout()
        setupNotificationObservers()
    }
    
    override func layoutSubviews() {
        commentsTableView.heightAnchor.constraint(equalToConstant: frame.height * 0.4).isActive = true
    }
    
    // MARK: Selector functions
    
    @objc fileprivate func handleTap(gesture: UITapGestureRecognizer) {
        endEditing(true)
        
        delegate?.didCloseStoryCommentsView()
    }
    
    @objc fileprivate func emptyTapGesture(gesture: UITapGestureRecognizer) {
        return
    }
    
    @objc fileprivate func handleCommentSendAction() {
        guard let text = textView.text, text.count > 0, var storyData = storyData, let feedViewModel = storyData.feedViewModel, let post = post, let storyLatestCommentsView = storyLatestCommentsView, let storyBottomControlsView = storyBottomControlsView  else { return }
        
        let input = StringUtil.sanitizeString(text: text)
        
        SVProgressHUD.show()
        
        feedViewModel.addComment(feed: post, input: input) { [weak self]  (feedResponse, apiResponseState, networkError, updatedFeed, errorObject, reply) in
            guard let this = self else { return }
            
            if let error = errorObject {
                this.delegate?.didFailToAddComment(error: error)
            } else {
                if let reply = reply {
                    this.post = updatedFeed
                    this.commentsTableView.addComment(comment: reply)
                    storyData.feedViewModel = feedViewModel
                    storyLatestCommentsView.setPost(post: updatedFeed)
                    storyBottomControlsView.setPost(post: updatedFeed)
                    this.resetTextView()
                }
            }
            
            AnalyticsHelper.commentSubmitEvent(isSuccess: errorObject == nil ? AppConstants.yes : AppConstants.no, userId: Helper().getUserId(), feed: updatedFeed)
            
            SVProgressHUD.dismiss()
        }
    }
    
    @objc func handleKeyboardShow(notification: NSNotification) {
        if let userInfo = notification.userInfo as? Dictionary<String, AnyObject> {
            let frame = userInfo[UIKeyboardFrameEndUserInfoKey]
            let keyboardRect = frame?.cgRectValue

            if let height = keyboardRect?.height {
                UIView.animate(withDuration: AppConstants.defaultAnimateDuration, animations: {
                    self.bottomView.snp.updateConstraints { (make) in
                        make.bottom.equalTo(-height)
                    }
                    self.layoutIfNeeded()
                })
            }
        }
    }
    
    @objc func handleKeyboardHide(notification: NSNotification) {
        UIView.animate(withDuration: AppConstants.defaultAnimateDuration, animations: {
            self.bottomView.snp.updateConstraints { (make) in
                make.bottom.equalTo(0)
            }
            self.layoutIfNeeded()
        })
    }
    
    // MARK: Fileprivate functions
    
    fileprivate func setupLayout() {
        addSubview(overlayView)
        overlayView.anchor(top: topAnchor, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor)
        
        addSubview(commentsTableView)
        addSubview(bottomView)
        
        commentsTableView.snp.makeConstraints { (make) in
            make.left.right.equalTo(self)
            make.bottom.equalTo(bottomView.snp.top)
        }
        
        bottomView.snp.makeConstraints { (make) in
            make.left.right.equalTo(self)
            make.height.equalTo(StoryViewConfig.commentTextViewMinHeight)
            make.bottom.equalTo(self).offset(0)
        }
        
        bottomView.addSubview(commentTextFieldRowView)
        commentTextFieldRowView.anchor(top: bottomView.topAnchor, leading: bottomView.leadingAnchor, bottom: bottomView.bottomAnchor, trailing: bottomView.trailingAnchor, padding: .init(top: 16, left: 16, bottom: 16, right: 16))
        
        commentTextFieldRowView.addSubview(sendButton)
        sendButton.anchor(top: nil, leading: nil, bottom: commentTextFieldRowView.bottomAnchor, trailing: commentTextFieldRowView.trailingAnchor, padding: .init(top: 0, left: 0, bottom: 0, right: 0), size: .init(width: 40, height: 40))
        
        commentTextFieldRowView.addSubview(textView)
        textView.anchor(top: commentTextFieldRowView.topAnchor, leading: commentTextFieldRowView.leadingAnchor, bottom: commentTextFieldRowView.bottomAnchor, trailing: sendButton.leadingAnchor, padding: .init(top: 2, left: 16, bottom: 2, right: 16))
        
        commentTextFieldRowView.addSubview(placeholderLabel)
        placeholderLabel.anchor(top: commentTextFieldRowView.topAnchor, leading: commentTextFieldRowView.leadingAnchor, bottom: commentTextFieldRowView.bottomAnchor, trailing: sendButton.leadingAnchor, padding: .init(top: 0, left: 20, bottom: 0, right: 16))
    }
    
    fileprivate func setupNotificationObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardHide(notification:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    fileprivate func updateData() {
        guard let post = post, let comments = post.comments else { return }
        commentsTableView.setComments(comments: comments.users)
        
        if let archived = post.archived, archived {
            bottomView.isHidden = true
        } else {
            bottomView.isHidden = false
        }
    }
    
    fileprivate func updateTextViewHeight() {
        let height = getTextViewHeight()
        
        if bottomViewHeightConstraintConstant != height {
            bottomViewHeightConstraintConstant = height
            
            bottomView.snp.updateConstraints { (make) in
                make.height.equalTo(height)
            }
            
            textView.setContentOffset(.zero, animated: false)
            layoutIfNeeded()
        }
    }
    
    fileprivate func getTextViewHeight() -> CGFloat {
        var height = textView.contentSize.height
        
        if height <= StoryViewConfig.commentTextViewMinHeight {
            height = StoryViewConfig.commentTextViewMinHeight
        } else if height >= StoryViewConfig.commentTextViewMaxHeight {
            height = StoryViewConfig.commentTextViewMaxHeight + 16.0 + 16.0 // Top and bottom padding
        } else {
            height = textView.contentSize.height + 16.0 + 16.0 // Top and bottom padding
        }
        
        return height
    }
    
    fileprivate func textViewPlaceholderVisibility() {
        if textView.text.count > 0 {
            placeholderLabel.isHidden = true
        } else {
            placeholderLabel.isHidden = false
        }
    }
    
    fileprivate func resetTextView() {
        textView.text = ""
        bottomView.snp.updateConstraints { (make) in
            make.height.equalTo(StoryViewConfig.commentTextViewMinHeight)
        }
        layoutIfNeeded()
        placeholderLabel.isHidden = false
    }
    
    // MARK: Public function
    
    public func keyboard(show: Bool) {
        _ = show ? textView.becomeFirstResponder() : textView.resignFirstResponder()
    }
    
    public func setData(storyData: StoryData, post: UserDataPlaceFeed, storyLatestCommentsView: StoryLatestCommentsView, storyBottomControlsView: StoryBottomControlsView) {
        self.storyData = storyData
        self.post = post
        self.storyLatestCommentsView = storyLatestCommentsView
        self.storyBottomControlsView = storyBottomControlsView
        
        updateData()
    }
    
    public func deleteComment(commentToDelete: Replies) {
        guard var storyData = storyData, let feedViewModel = storyData.feedViewModel, let post = post else { return }

        SVProgressHUD.show()

        AnalyticsHelper.commentDeleteEvent(userId: Helper().getUserId(), feed: post)
        
        feedViewModel.deleteComment(feed: post, commentToDelete: commentToDelete) { [weak self] (apiResponseState, networkError, updatedFeed, errorObject, deletedReply) in
            guard let this = self else { return }
            if let error = errorObject {
                this.delegate?.didFailToDeleteComment(error: error)
            } else {
                if let feed = updatedFeed {
                    this.post = feed
                    storyData.feedViewModel = feedViewModel
                    
                    this.storyLatestCommentsView?.setPost(post: feed)
                    this.storyBottomControlsView?.setPost(post: feed)
                    this.commentsTableView.deleteComment(comment: commentToDelete)
                }
            }
            
            SVProgressHUD.dismiss()
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


extension StoryCommentsView: UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
        updateTextViewHeight()
        textViewPlaceholderVisibility()
    }
}
