//
//  StoryRatingsCollectionView.swift
//  Markk
//
//  Created by Pushkar Deshmukh on 29/05/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import UIKit
import AVFoundation
import SVProgressHUD

enum CellState {
    case cellForItemAt
    case willDisplay
    case didEndDisplaying
}

protocol StoryRatingsCollectionViewDelegate: NSObjectProtocol {
    func shouldGoToPreviousStory()
    func shouldGoToNextStory()
    func didCollectionViewLongPress(gesture: UILongPressGestureRecognizer)
    func didRatingImageLoad(error: Error?, ratingCellIndex: Int)
    func didVideoLoad(playerStatus: PlayerStatus, ratingCellIndex: Int, playerItem: AVPlayerItem, post: UserDataPlaceFeed, player: AVPlayer)
    func didStoryRatingIndexChange(updatedIndex: Int)
    func didCommentTap(feedViewModel: FeedViewModel, post: UserDataPlaceFeed, storyLatestCommentsView: StoryLatestCommentsView, storyBottomControlsView: StoryBottomControlsView)
    func didCloseStoryCommentsView()
    func didSettingsTap(feedViewModel: FeedViewModel, post: UserDataPlaceFeed)
    func didShareTap(feedViewModel: FeedViewModel, post: UserDataPlaceFeed)
    func didUserPostMoodIconTap(post: UserDataPlaceFeed)
    func didMoodExplainerClose()
    func didPostLocationIconTap(post: UserDataPlaceFeed)
    func didStoryRatingsFetch(newStoryData: StoryData, storyLatestCommentsView: StoryLatestCommentsView, storyBottomControlsView: StoryBottomControlsView)
    func didStoryRatingsFetchFail(error: ErrorObject)
    func didUserInitialLabelTap(post: UserDataPlaceFeed)
    func didUserImageViewTap(post: UserDataPlaceFeed)
    func didUserDisplayNameTap(post: UserDataPlaceFeed)
    func didEndDisplayingRatingCell(endedCell: StoryRatingCollectionViewCell?, visibleCell: StoryRatingCollectionViewCell?)
    func willDisplayRatingCell(cell: StoryRatingCollectionViewCell?)
    func didUpvotePost(post: UserDataPlaceFeed)
}

class StoryRatingsCollectionView: UIView {
    public weak var delegate: StoryRatingsCollectionViewDelegate?
    
    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = 0
        
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.backgroundColor = .clear
        collectionView.register(StoryRatingCollectionViewCell.self, forCellWithReuseIdentifier: "StoryRatingCollectionViewCell")
        
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.bounces = false
        collectionView.isPagingEnabled = true
        collectionView.isScrollEnabled = false
        
        let longPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress))
        longPressGesture.minimumPressDuration = 0.2
        collectionView.addGestureRecognizer(longPressGesture)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTap))
        collectionView.addGestureRecognizer(tapGesture)
        return collectionView
    }()
    
    public var currentShowingIndex: Int = 0 {
        didSet {
            delegate?.didStoryRatingIndexChange(updatedIndex: currentShowingIndex)
            
        }
    }
    
    fileprivate var storyData: StoryData!
    
    fileprivate let locationManager = UserLocationManager.SharedManager
    public var storyViewManager: StoryViewManager?
    
    init(frame: CGRect, storyViewManager: StoryViewManager?) {
        self.storyViewManager = storyViewManager
        super.init(frame: frame)
        setupLayout()
    }
    
    // MARK: Fileprivate functions
    
    fileprivate func setupLayout() {
        backgroundColor = .clear
        clipsToBounds = true
        layer.cornerRadius = 8
        
        addSubview(collectionView)
        collectionView.anchor(top: topAnchor, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor)
    }
    
    fileprivate func getCellAtPoint(point: CGPoint) -> StoryRatingCollectionViewCell? {
        guard let indexPath = collectionView.indexPathForItem(at: point) else { return nil }
        guard let cell = collectionView.cellForItem(at: indexPath) as? StoryRatingCollectionViewCell else { return nil}
        return cell
    }
    
    fileprivate func getRatingsForStory() {
        guard let placeDetails = storyData.placeDetails else { return }
        
        let newFeedViewModel = FeedViewModel.init(screen: AppConstants.screenNames.storyView)!
        let location = locationManager.getDefaultLocationOtherwiseFallbackCachedLocation()
        
        SVProgressHUD.show()
        
        newFeedViewModel.getFeedListInStory(userId: storyData.userId, placeId: placeDetails._id, viewerId: Helper().getUserId(), latitude: location.latitude, longitude: location.longitude, archive: false, completion: { [weak self] (storyData, apiResponseState, networkError, error) in
            guard let this = self else { return }

            if let error = error {
                this.delegate?.didStoryRatingsFetchFail(error: error)
            } else {
                if let storyViewManager = this.storyViewManager, let feedId = storyViewManager.feedItemId {
                    newFeedViewModel.filterFeedById(feedId: feedId)
                }
                
                this.storyData.feedViewModel = newFeedViewModel
                this.collectionView.reloadData()
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
                    guard let cell = this.collectionView.visibleCells.sortedArrayByPosition().first as? StoryRatingCollectionViewCell else { return }
                    
                    this.delegate?.didStoryRatingsFetch(newStoryData: this.storyData, storyLatestCommentsView: cell.storyLatestCommentsView, storyBottomControlsView: cell.storyBottomControlsView)
                })
            }
            
            SVProgressHUD.dismiss()
        })
    }
    
    // MARK: Selector functions
    
    @objc fileprivate func handleLongPress(gesture: UILongPressGestureRecognizer) {
        let point = gesture.location(in: collectionView)
        guard let cell = getCellAtPoint(point: point) else { return }
    
        let state = gesture.state
        
        switch state {
        case .began: cell.fadeUI(show: false)
        case .ended: cell.fadeUI(show: true)
        default: break
        }
        
        delegate?.didCollectionViewLongPress(gesture: gesture)
    }
    
    @objc fileprivate func handleTap(gesture: UITapGestureRecognizer) {
        let point = gesture.location(in: collectionView)
        guard let cell = getCellAtPoint(point: point) else { return }
        
        let currentCellIndex = cell.cellIndex
        
        let tapLocation = gesture.location(in: nil)
        let direction: SnapMovementDirectionState = tapLocation.x > frame.width / 2 ? .forward : .backward
        
        moveToNextOrPrevRating(direction: direction, currentIndex: currentCellIndex)
    }
    
    fileprivate func scrollToIndexPath(indexPath: IndexPath, animate: Bool) {
        // NOTE: When collectionView is scrolled to cell using scrollToItem and animate is false,
        // didEndDisplaying delegate is not called. Hence if animate is disabled then manually call resume to play media
        
        if !animate {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                UIView.animate(withDuration: 0.1, animations: {
                    if self.collectionView.validate(indexPath: indexPath) {
                        self.collectionView.scrollToItem(at: indexPath, at: .left, animated: false)
                    }
                    
                }) { (finished) in
                    if finished {
                        self.storyViewManager?.isCompletelyVisible = true
                        self.currentShowingIndex = indexPath.row
                        self.reStartMedia()
                    }
                }
            }
            
        } else {
            self.collectionView.scrollToItem(at: indexPath, at: .left, animated: animate)
        }
    }
    
    fileprivate func shouldAnimateNextOrPrev(feedViewModel: FeedViewModel, currentIndex: Int, newIndex: Int) -> Bool {
        guard let currentFeed = feedViewModel.getFeedAt(index: currentIndex) else { return false }
        guard let otherFeed = feedViewModel.getFeedAt(index: newIndex) else { return false }
        
//        return true
        return otherFeed.displayName != currentFeed.displayName
    }
    
    fileprivate func handleForwardDirection(feedViewModel: FeedViewModel, currentIndex: Int) {
        if currentIndex == feedViewModel.getFeedCount() - 1 {
//            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
//                self.pauseMedia()
//            })
            
            self.storyViewManager?.isCompletelyVisible = false
            delegate?.shouldGoToNextStory()
        } else {
            let newIndex = currentIndex + 1
            scrollToIndexPath(indexPath: IndexPath.init(row: newIndex, section: 0), animate: shouldAnimateNextOrPrev(feedViewModel: feedViewModel, currentIndex: currentIndex, newIndex: newIndex))
        }
    }
    
    fileprivate func handleBackwardDirection(feedViewModel: FeedViewModel, currentIndex: Int) {
        if currentIndex == 0 {
//            DispatchQueue.main.async {
//                self.pauseMedia()
//            }
            
            self.storyViewManager?.isCompletelyVisible = false
            delegate?.shouldGoToPreviousStory()
        } else {
            let newIndex = currentIndex - 1
            
            scrollToIndexPath(indexPath: IndexPath.init(row: newIndex, section: 0), animate: shouldAnimateNextOrPrev(feedViewModel: feedViewModel, currentIndex: currentIndex, newIndex: newIndex))
        }
    }
    
    fileprivate func addViewRatingEvent(feedViewModel: FeedViewModel, index: Int) {
        guard let post = feedViewModel.getFeedAt(index: index) else { return }
        guard let storyViewManager = storyViewManager else { return }
        
        AnalyticsHelper.viewRatingEvent(screen: storyViewManager.screen, section: storyViewManager.section, feed: post)
    }
    
    // MARK: Public functions
    
    public func scrollToRating(index: Int, animate: Bool) {
        scrollToIndexPath(indexPath: IndexPath.init(row: index, section: 0), animate: animate)
    }
    
    public func setStoryData(storyData: StoryData) {
        self.storyData = storyData
        
        if let _ = self.storyData.feedViewModel {
            collectionView.reloadData()
        } else {
            getRatingsForStory()
        }
    }
    
    public func moveToNextOrPrevRating(direction: SnapMovementDirectionState, currentIndex: Int) {
        guard let feedViewModel = storyData.feedViewModel else { return }
        
        switch direction {
        case .forward: handleForwardDirection(feedViewModel: feedViewModel, currentIndex: currentIndex)
        case .backward: handleBackwardDirection(feedViewModel: feedViewModel, currentIndex: currentIndex)
        }
    }
    
    public func stopMedia() {
        let visibleCells = collectionView.visibleCells.sortedArrayByPosition()
        visibleCells.forEach { (cell) in
            if let cell = cell as? StoryRatingCollectionViewCell {
                cell.stopMedia()
            }
        }
    }
    
    public func pauseMedia() {
        DispatchQueue.main.async {
            
            let visibleCells = self.collectionView.visibleCells.sortedArrayByPosition()
            visibleCells.forEach { (cell) in
                if let cell = cell as? StoryRatingCollectionViewCell {
                    cell.pauseMedia()
                }
            }
            //            guard let visibleCell = self.collectionView.visibleCells.sortedArrayByPosition().first as? StoryRatingCollectionViewCell else { return }
            //            visibleCell.pauseMedia()
        }
        
//        guard let visibleCell = collectionView.visibleCells.sortedArrayByPosition().first as? StoryRatingCollectionViewCell else { return }
//        visibleCell.pauseMedia()
    }
    
    public func resumeMedia() {
        guard let visibleCell = collectionView.visibleCells.sortedArrayByPosition().first as? StoryRatingCollectionViewCell else { return }
        visibleCell.resumeMedia()
    }
    
    public func reStartMedia() {
        guard let visibleCell = collectionView.visibleCells.sortedArrayByPosition().first as? StoryRatingCollectionViewCell else { return }
        
        if let feedViewModel = storyData.feedViewModel {
            visibleCell.setFeedViewModel(feedViewModel: feedViewModel, cellIndex: visibleCell.cellIndex, storyViewManager: storyViewManager, cellState: .didEndDisplaying)
            visibleCell.playMedia()
        }
    }
    
    public func updateDeletedRatingViewState(post: UserDataPlaceFeed) {
        guard let visibleCell = collectionView.visibleCells.sortedArrayByPosition().first as? StoryRatingCollectionViewCell else { return }
        visibleCell.updateDeletedRatingViewState(post: post)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

extension StoryRatingsCollectionView: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let feedViewModel = storyData.feedViewModel else { return 0 }
        return feedViewModel.getFeedCount()
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "StoryRatingCollectionViewCell", for: indexPath) as? StoryRatingCollectionViewCell else { return UICollectionViewCell() }
        
        
        cell.delegate = self
        if let feedViewModel = storyData.feedViewModel {
            cell.setFeedViewModel(feedViewModel: feedViewModel, cellIndex: indexPath.row, storyViewManager: storyViewManager, cellState: .cellForItemAt)
        }

        return cell
    }
}

extension StoryRatingsCollectionView: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return collectionView.bounds.size
    }
}

extension StoryRatingsCollectionView: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        // NOTE: Restart the visible post's media
//        print("willDisplay - StoryRaringsCollectionView: ", indexPath.row)
        guard let cell = cell as? StoryRatingCollectionViewCell else { return }
        guard let storyViewManager = storyViewManager else { return }
        
        if indexPath.row == 0 && storyViewManager.snappedIndex == 0 {
            storyViewManager.isCompletelyVisible = true
            cell.playMedia()
        } else {
            cell.prepareMedia()
        }
        guard let feedViewModel = storyData.feedViewModel else { return }
//        cell.setFeedViewModel(feedViewModel: feedViewModel, cellIndex: cell.cellIndex, storyViewManager: storyViewManager, cellState: .willDisplay)
//        cell.reStartMedia()
        
        delegate?.willDisplayRatingCell(cell: cell)
        
        DispatchQueue.main.async {
            self.addViewRatingEvent(feedViewModel: feedViewModel, index: cell.cellIndex)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
//        print("didEndDisplaying - StoryRaringsCollectionView: ", indexPath.row)
        
        // NOTE: If not ran on main thread then visible cell and ended cell is same
        DispatchQueue.main.async {
            if let endedCell = cell as? StoryRatingCollectionViewCell {
//                print("Pausing ended cell: ", endedCell.cellIndex)
                endedCell.pauseMedia()
            }
            
            // NOTE: Restart the visible post's media
            let visibleCells = collectionView.visibleCells.sortedArrayByPosition()
            let visibleCell = visibleCells.first as? StoryRatingCollectionViewCell
            
            if let cell = visibleCell {
                self.currentShowingIndex = cell.cellIndex
//                print("updated currentShowingIndex in didEndDisplaying: ", self.currentShowingIndex)
            }
            
            guard let feedViewModel = self.storyData.feedViewModel else { return }
            guard let storyViewManager = self.storyViewManager, storyViewManager.isCompletelyVisible else {
//                print("Story is not complete visible!!!")
                return
                
            }
            
            if let cell = visibleCell, feedViewModel.getFeedCount() > 0 {
//                print("starting media for visible cell: ", cell.cellIndex)
//                print("is complete visible flag :", storyViewManager.isCompletelyVisible)
                cell.setFeedViewModel(feedViewModel: feedViewModel, cellIndex: cell.cellIndex, storyViewManager: self.storyViewManager, cellState: .didEndDisplaying)
                cell.playMedia()
                
                self.delegate?.didEndDisplayingRatingCell(endedCell: cell, visibleCell: visibleCell)
                
            }
        }
        
       
        
       
        
    }
    
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
//        print("StoryRatingsCollectionView scrollViewDidEndScrollingAnimation")
//        guard let visibleCell = collectionView.visibleCells.first as? StoryRatingCollectionViewCell else { return }
//        print(visibleCell.cellIndex)
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        print("scrollViewWillBeginDragging -------")
    }
    
    
}

extension StoryRatingsCollectionView: StoryRatingCollectionViewCellDelegate {
    func didUpvotePost(post: UserDataPlaceFeed) {
        delegate?.didUpvotePost(post: post)
    }
    
    func didSettingsTap(feedViewModel: FeedViewModel, post: UserDataPlaceFeed) {
        delegate?.didSettingsTap(feedViewModel: feedViewModel, post: post)
    }
    
    func didShareTap(feedViewModel: FeedViewModel, post: UserDataPlaceFeed) {
        delegate?.didShareTap(feedViewModel: feedViewModel, post: post)
    }
    
    func didUserPostMoodIconTap(post: UserDataPlaceFeed) {
        delegate?.didUserPostMoodIconTap(post: post)
    }
    
    func didMoodExplainerClose() {
        delegate?.didMoodExplainerClose()
    }
    
    func didCommentTap(feedViewModel: FeedViewModel, post: UserDataPlaceFeed, storyLatestCommentsView: StoryLatestCommentsView, storyBottomControlsView: StoryBottomControlsView) {
        delegate?.didCommentTap(feedViewModel: feedViewModel, post: post, storyLatestCommentsView: storyLatestCommentsView, storyBottomControlsView: storyBottomControlsView)
    }
    
    func didCloseStoryCommentsView() {
        delegate?.didCloseStoryCommentsView()
    }
    
    func didVideoLoad(playerStatus: PlayerStatus, ratingCellIndex: Int, playerItem: AVPlayerItem, post: UserDataPlaceFeed, player: AVPlayer) {
        delegate?.didVideoLoad(playerStatus: playerStatus, ratingCellIndex: ratingCellIndex, playerItem: playerItem, post: post, player: player)
    }
    
    func didRatingImageLoad(error: Error?, ratingCellIndex: Int) {
        delegate?.didRatingImageLoad(error: error, ratingCellIndex: ratingCellIndex)
    }
    
    func didPostLocationIconTap(post: UserDataPlaceFeed) {
        delegate?.didPostLocationIconTap(post: post)
    }
    
    func didUserInitialLabelTap(post: UserDataPlaceFeed) {
        delegate?.didUserInitialLabelTap(post: post)
    }
    
    func didUserImageViewTap(post: UserDataPlaceFeed) {
        delegate?.didUserImageViewTap(post: post)
    }
    
    func didUserDisplayNameTap(post: UserDataPlaceFeed) {
        delegate?.didUserDisplayNameTap(post: post)
    }
}

