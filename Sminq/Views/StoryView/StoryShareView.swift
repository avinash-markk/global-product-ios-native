//
//  StoryShareView.swift
//  Markk
//
//  Created by Pushkar Deshmukh on 19/08/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import UIKit

protocol StoryShareViewDelegate: NSObjectProtocol {
    func didStoryShareViewOverlayTap()
    func didStoryViewOptionSelect(storyViewOption: StoryShareOption, post: UserDataPlaceFeed, place: LivePlaces)
}

class StoryShareView: UIView {
    public weak var delegate: StoryShareViewDelegate?
    
    fileprivate lazy var overlayView: UIView = {
        let view = UIView()
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleOverlayTap))
        view.addGestureRecognizer(tapGesture)
        return view
    }()
    
    fileprivate lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.register(StoryShareTableViewCell.self, forCellReuseIdentifier: "StoryShareTableViewCell")
        tableView.delegate = self
        tableView.dataSource = self
        tableView.bounces = false
        tableView.separatorStyle = .none
        tableView.contentInset = UIEdgeInsets.init(top: tableViewTopSpacing, left: 0, bottom: 0, right: 0)
        tableView.layer.cornerRadius = 8
        return tableView
    }()
    
    fileprivate var storyShareOptions: [StoryShareOption] = [] {
        didSet {
            tableView.snp.updateConstraints { (make) in
                make.height.equalTo(getTableViewHeight())
            }
            tableView.reloadData()
        }
    }
    
    fileprivate var post: UserDataPlaceFeed?
    fileprivate var place: LivePlaces?
    
    fileprivate lazy var cellHeight: CGFloat = 48
    fileprivate lazy var tableViewTopSpacing: CGFloat = 8
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupLayout()
    }
    
    // MARK: Fileprivate functions
    
    fileprivate func setupLayout() {
        addSubview(overlayView)
        addSubview(tableView)
        
        overlayView.anchor(top: topAnchor, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor)
        overlayView.backgroundColor = AppConstants.SminqColors.blackDark100.withAlphaComponent(0.75)
        
        tableView.snp.makeConstraints { (make) in
            make.left.right.bottom.equalTo(self)
            make.height.equalTo(0)
        }
        
//        tableView.anchor(top: nil, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor, padding: .zero, size: .init(width: 0, height: getTableViewHeight()))
        
    }
    
    fileprivate func getStoryShareOptions() -> [StoryShareOption] {
        var options = [StoryShareOption]()
        
        if OtherAppShareKeys.instagramStories.rawValue.canOpenStringURL() {
            options.append(StoryShareOption(id: .instagramStories, imageString: "iconSocialsInstagram24", label: "Instagram Stories"))
        }
        
        options.append(StoryShareOption(id: .messages, imageString: "iconTextsms24", label: "Messages"))
        options.append(StoryShareOption(id: .twitter, imageString: "iconSocialsTwitter24", label: "Twitter"))
        
        options = sortStoryShareOptionsByLastUsed(options: options)
        
        options.append(StoryShareOption(id: .copyLink, imageString: "iconLink24BlackDark50", label: "Copy Link"))
        options.append(StoryShareOption(id: .more, imageString: "iconMore24BlackDark50", label: "More"))
        
        return options
    }
    
    fileprivate func sortStoryShareOptionsByLastUsed(options: [StoryShareOption]) -> [StoryShareOption] {
        var sortedOptions = options
        
        if let lastUsedShareOption = UserDefaultsUtility.shared.getLastUsedShareApp() {
            var lastUsedIndex: Int?
            
            for (index, option) in sortedOptions.enumerated() {
                if option.id == lastUsedShareOption.id {
                    lastUsedIndex = index
                }
            }
            
            if let lastUsedIndex = lastUsedIndex {
                let element = sortedOptions.remove(at: lastUsedIndex)
                sortedOptions.insert(element, at: 0)
            }
            
        }
        
        return sortedOptions
    }
    
    fileprivate func getTableViewHeight() -> CGFloat {
        let totalRowHeight: CGFloat = cellHeight * CGFloat(storyShareOptions.count)
        return tableViewTopSpacing + totalRowHeight + getWindowSafeAreaInsets().bottom
    }
    
    // MARK: Objc functions
    @objc fileprivate func handleOverlayTap() {
        delegate?.didStoryShareViewOverlayTap()
    }
    
    // MARK: Public functions
    
    public func setData(post: UserDataPlaceFeed, place: LivePlaces) {
        self.post = post
        self.place = place
        
        self.storyShareOptions = self.getStoryShareOptions()
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension StoryShareView: UITableViewDelegate {
    
}

extension StoryShareView: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return storyShareOptions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "StoryShareTableViewCell", for: indexPath) as? StoryShareTableViewCell else { return UITableViewCell() }
        cell.setData(storyShareOption: storyShareOptions[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return cellHeight
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let post = post, let place = place else { return }
        delegate?.didStoryViewOptionSelect(storyViewOption: storyShareOptions[indexPath.row], post: post, place: place)
        
        UserDefaultsUtility.shared.setLastUsedShareApp(storyShareOption: storyShareOptions[indexPath.row])
    }
    
    
}
