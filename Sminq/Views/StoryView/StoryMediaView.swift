//
//  StoryMediaView.swift
//  Sminq
//
//  Created by Pushkar Deshmukh on 22/05/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import UIKit
import AVFoundation

protocol StoryMediaViewDelegate: NSObjectProtocol {
    func didImageLoad(error: Error?, ratingCellIndex: Int)
    func didVideoLoad(playerStatus: PlayerStatus, ratingCellIndex: Int, playerItem: AVPlayerItem, post: UserDataPlaceFeed, player: AVPlayer)
}

class StoryMediaView: UIView {
    public weak var delegate: StoryMediaViewDelegate?
    
    public var ratingCellIndex: Int = 0
    
    var activityIndicator: UIActivityIndicatorView = {
        let indicator = UIActivityIndicatorView()
        indicator.stopAnimating()
        indicator.activityIndicatorViewStyle = .whiteLarge
        return indicator
    }()
    
    var mediaDownloadTask: MediaDownloadTask?
    
    public var post: UserDataPlaceFeed? {
        didSet {
            guard let post = post else { return }
            
            mediaDownloadTask = MediaDownloadTask.init(feed: post)
        }
    }
    
    fileprivate lazy var videoPlayerView: StoryVideoPlayerView = {
        let videoPlayerView = StoryVideoPlayerView.init(frame: .zero)
        videoPlayerView.delegate = self
        videoPlayerView.isHidden = true
        return videoPlayerView
    }()
    
    fileprivate lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.isHidden = true
        imageView.backgroundColor = .black
        imageView.clipsToBounds = true
        return imageView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupLayout()
        
    }
    
    // MARK: Public functions
    
    public func setPost(post: UserDataPlaceFeed, ratingCellIndex: Int) {
        self.ratingCellIndex = ratingCellIndex
        self.post = post
        
    }
    
    public func prepareMedia() {
        playMedia(prepareMedia: true)
    }
    
    public func playMedia(prepareMedia: Bool = false) {
        guard let post = post else { return }
        
        guard let mediaType = post.getFeedMediaType() else { return }
        
        switch mediaType {
        case .image: handleImage(post: post, prepareMedia: prepareMedia)
        case .video: handleVideo(post: post, prepareMedia: prepareMedia)
        }
    }
    
    public func pauseMedia() {
//        guard let post = post else { return }
//
//        if let mediaType = post.getFeedMediaType() {
//            if mediaType == .video {
//                videoPlayerView.pause()
//            }
//        }
        
        videoPlayerView.pause()
        mediaDownloadTask?.pause()
    }
    
    public func resumeMedia() {
        guard let post = post else { return }
        
        if let mediaType = post.getFeedMediaType() {
            if mediaType == .video {
                videoPlayerView.resume()
            } else {
                videoPlayerView.pause()
            }
        }
        
        mediaDownloadTask?.resume()
    }
    
    public func reStartMedia() {
        guard let post = post else { return }
        
        if let mediaType = post.getFeedMediaType() {
            if mediaType == .video {
                videoPlayerView.play()
            } else {
                videoPlayerView.pause()
            }
        }
        
        mediaDownloadTask?.resume()
    }
    
    public func stopMedia() {
        mediaDownloadTask?.pause()
        videoPlayerView.stop()
        
    }
    
    // MARK: Fileprivate functions
    
    fileprivate func setupLayout() {        
        layer.cornerRadius = 8
        clipsToBounds = true
        
        addSubview(imageView)
        imageView.fillSuperview()
        
        addSubview(videoPlayerView)
        videoPlayerView.fillSuperview()
        
        addSubview(activityIndicator)
        
        activityIndicator.snp.makeConstraints { (make) in
            make.centerX.centerY.equalTo(self)
        }
    }
    
    fileprivate func handleImage(post: UserDataPlaceFeed, prepareMedia: Bool) {
        if let url = post.getImageURL() {
            self.videoPlayerView.isHidden = true
            self.imageView.isHidden = false
            
            if !prepareMedia {
                loaderVisibility(show: true)
            }
            
            
            self.imageView.sd_setImage(with: url, placeholderImage: nil, options: .cacheMemoryOnly) { [weak self] (image, error, cacheType, url)  in
                guard let this = self else { return }
                guard !prepareMedia else { return }
                
                this.loaderVisibility(show: false)
                DispatchQueue.main.async {
                    if let error = error {
                        this.delegate?.didImageLoad(error: error, ratingCellIndex: this.ratingCellIndex)
                    } else {
                        this.delegate?.didImageLoad(error: nil, ratingCellIndex: this.ratingCellIndex)
                    }
                }
            }
        }
    }
    
    fileprivate func handleVideo(post: UserDataPlaceFeed, prepareMedia: Bool) {
        if let url = post.getVideoURL() {
//            print("video load requested, ", url)
            
            self.videoPlayerView.isHidden = false
            self.imageView.isHidden = true
            videoPlayerView.ratingCellIndex = ratingCellIndex
            
            if !prepareMedia {
                videoPlayerView.play(url: url)
            }
            
        }
    }

    fileprivate func loaderVisibility(show: Bool) {
        activityIndicator.isHidden = !show
        
        show ? activityIndicator.startAnimating() : activityIndicator.stopAnimating()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension StoryMediaView: StoryVideoPlayerViewDelegate {
    func videoStatus(playerStatus: PlayerStatus, ratingCellIndex: Int, playerItem: AVPlayerItem, player: AVPlayer) {
        guard let post = post else { return }
        delegate?.didVideoLoad(playerStatus: playerStatus, ratingCellIndex: ratingCellIndex, playerItem: playerItem, post: post, player: player)
    }
    
    func didStartPlaying() {
//        print("StoryCollectionViewCell didStartPlaying")
    }
    
    func didCompletePlay() {
//        print("StoryCollectionViewCell didCompletePlay")
    }
    
    func didTrack(progress: Float) {
//        print("StoryCollectionViewCell didTrack ", progress)
    }
    
    func didFailed(withError error: String, for url: URL?) {
//        print("StoryCollectionViewCell didFailed")
    }
}
