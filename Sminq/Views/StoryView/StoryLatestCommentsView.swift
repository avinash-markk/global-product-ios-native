//
//  StoryLatestCommentsView.swift
//  Markk
//
//  Created by Pushkar Deshmukh on 23/05/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import UIKit
import SnapKit

protocol StoryLatestCommentsViewDelegate: NSObjectProtocol {
    func didCommentSelect(comment: Replies)
}

class StoryLatestCommentsView: UIView {
    public weak var delegate: StoryLatestCommentsViewDelegate?
    
    fileprivate lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.minimumLineSpacing = topCellPadding
        
        let collectionView = UICollectionView.init(frame: .zero, collectionViewLayout: layout)
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.backgroundColor = .clear
    
        collectionView.register(SingleLineCommentCollectionViewCell.self, forCellWithReuseIdentifier: "SingleLineCommentCollectionViewCell")

        collectionView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleCollectionViewTap)))
        collectionView.isUserInteractionEnabled = true
        return collectionView
    }()
    
    fileprivate let topCellPadding: CGFloat = 4
    fileprivate let cellHeight: CGFloat = 32

    fileprivate var post: UserDataPlaceFeed? {
        didSet {
            updateData()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)

        setupLayout()
    }
    
    // MARK: Fileprivate functions
    
    fileprivate func setupLayout() {
        snp.makeConstraints { (make) in
            make.height.equalTo(getCollectionViewHeight())
        }
        
        addSubview(collectionView)
        collectionView.fillSuperview()
        
    }
    
    fileprivate func getCollectionViewHeight() -> CGFloat {
        guard let post = post, let comments = post.comments, comments.users.count > 0 else { return 0 }
        let commentsCount = ArrayUtil.getLatestReplies(replies: comments.users).count
        let labelHeight: CGFloat = (CGFloat(commentsCount) * cellHeight)
        let totalSpacingHeight = (topCellPadding * CGFloat(commentsCount - 1))
        return labelHeight + totalSpacingHeight
    }
    
    fileprivate func updateData() {
        snp.updateConstraints { (make) in
            make.height.equalTo(self.getCollectionViewHeight())
        }
        
        collectionView.reloadData()
    }
    
    // MARK: Public functions
    
    public func setPost(post: UserDataPlaceFeed) {
        self.post = post
    }
    
    // MARK: @objc functions
    
    @objc fileprivate func handleCollectionViewTap(_ gesture: UITapGestureRecognizer) {
        let point = gesture.location(in: collectionView)
        
        guard let indexPath = collectionView.indexPathForItem(at: point) else { return }
        
        if let post = post, let comments = post.comments, comments.users.count > 0 {
            AnalyticsHelper.viewCommentEvent(feed: post)
            delegate?.didCommentSelect(comment: ArrayUtil.getLatestReplies(replies: comments.users)[indexPath.row])
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

extension StoryLatestCommentsView: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let post = post, let comments = post.comments {
            return ArrayUtil.getLatestReplies(replies: comments.users).count
        } else {
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SingleLineCommentCollectionViewCell", for: indexPath) as? SingleLineCommentCollectionViewCell else { return UICollectionViewCell() }
        
        if let post = post, let comments = post.comments, comments.users.count > 0 {
            cell.setComments(comments: ArrayUtil.getLatestReplies(replies: comments.users), index: indexPath.row)
        }
        
        return cell
    }
}

extension StoryLatestCommentsView: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return .init(width: self.frame.width, height: cellHeight)
    }
}
