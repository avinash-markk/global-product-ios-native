//
//  StoryBottomControlsView.swift
//  Sminq
//
//  Created by Pushkar Deshmukh on 22/05/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import UIKit
import Toaster

protocol StoryBottomControlsViewDelegate: NSObjectProtocol {
    func didCommentTap(storyBottomControlsView: StoryBottomControlsView)
    func didUpvoteTap(post: UserDataPlaceFeed)
    func didSettingsTap(post: UserDataPlaceFeed)
    func didShareTap(post: UserDataPlaceFeed)
}

class StoryBottomControlsView: UIView {
    public weak var delegate: StoryBottomControlsViewDelegate?
    
    fileprivate lazy var commentsButton = self.createButton(imageString: "iconComments20White75Shadow", selector: #selector(handleComment))
    fileprivate lazy var likeButton = self.createButton(imageString: "iconHeartFull20White75Shadow", selector: #selector(handleLike))
    fileprivate lazy var shareButton = self.createButton(imageString: "iconShare20White75Shadow", selector: #selector(handleShare))
    
    fileprivate lazy var settingsButton: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(UIImage(named: "iconMoreHorizontal20White75Shadow")?.withRenderingMode(.alwaysOriginal), for: .normal)
        button.titleLabel?.font = UIFont(name: AppConstants.Font.primaryRegular, size: 16)
        button.setTitleColor(AppConstants.SminqColors.white100.withAlphaComponent(0.75), for: .normal)
        button.addTarget(self, action: #selector(handleSettings), for: .touchUpInside)
        button.widthAnchor.constraint(equalToConstant: buttonWidth).isActive = true
        return button
    }()
    
    fileprivate lazy var viewsButton: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(UIImage(named: "iconView16White75Shadow")?.withRenderingMode(.alwaysOriginal), for: .normal)
        button.titleLabel?.font = UIFont(name: AppConstants.Font.primaryRegular, size: 14)
        button.setTitleColor(AppConstants.SminqColors.white100.withAlphaComponent(0.75), for: .normal)
        button.centerTextAndImage(spacing: 4)
        return button
    }()
    
    fileprivate lazy var separatorView: UIView = {
        let view = UIView()
        view.backgroundColor = AppConstants.SminqColors.white100.withAlphaComponent(0.25)
        return view
    }()
    
    fileprivate var post: UserDataPlaceFeed? {
        didSet {
            updateData()
        }
    }
    
    let storyViewManager: StoryViewManager?
    let buttonWidth: CGFloat = 44
    
    init(frame: CGRect, storyViewManager: StoryViewManager?) {
        self.storyViewManager = storyViewManager
        
        super.init(frame: frame)
        
        setupLayout()
    }
    
    // MARK: Fileprivate functions
    
    fileprivate func setupLayout() {
        heightAnchor.constraint(equalToConstant: 104).isActive = true
        
        let controlsStackView = UIStackView(arrangedSubviews: [commentsButton, likeButton, shareButton, settingsButton])
        controlsStackView.distribution = .equalCentering
        controlsStackView.isLayoutMarginsRelativeArrangement = true
        controlsStackView.layoutMargins = .init(top: 0, left: 16, bottom: 0, right: 16)
        addSubview(controlsStackView)
        controlsStackView.anchor(top: nil, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor, padding: .zero, size: .init(width: 0, height: 55))
        
        addSubview(separatorView)
        separatorView.anchor(top: nil, leading: leadingAnchor, bottom: controlsStackView.topAnchor, trailing: trailingAnchor, padding: .zero, size: .init(width: 0, height: 1))
        
        let viewStackView = UIStackView(arrangedSubviews: [viewsButton, UIView()])
        viewStackView.distribution = .equalCentering
        viewStackView.isLayoutMarginsRelativeArrangement = true
        viewStackView.layoutMargins = .init(top: 0, left: 16, bottom: 0, right: 16)
        addSubview(viewStackView)
        viewStackView.anchor(top: nil, leading: leadingAnchor, bottom: separatorView.topAnchor, trailing: trailingAnchor, padding: .zero, size: .init(width: 0, height: 48))
        
        likeButton.snp.makeConstraints { (make) in
            make.width.greaterThanOrEqualTo(buttonWidth)
        }
        
        commentsButton.snp.makeConstraints { (make) in
            make.width.greaterThanOrEqualTo(buttonWidth)
        }
        
        shareButton.snp.makeConstraints { (make) in
            make.width.greaterThanOrEqualTo(buttonWidth)
        }
    }
    
    fileprivate func createButton(imageString: String, selector: Selector) -> UIButton {
        let button = UIButton(type: .system)
        button.setImage(UIImage(named: imageString)?.withRenderingMode(.alwaysOriginal), for: .normal)
        button.titleLabel?.font = UIFont(name: AppConstants.Font.primaryRegular, size: 16)
        button.setTitleColor(AppConstants.SminqColors.white100.withAlphaComponent(0.75), for: .normal)
        button.centerTextAndImage(spacing: 8)
        button.addTarget(self, action: selector, for: .touchUpInside)
        return button
    }
    
    fileprivate func updateData() {
        guard let post = post else { return }
        
        updateCommentCounts(for: post)
        updateLikeCounts(for: post)
        updateShareCounts(for: post)
        updateViewCountButton(for: post)
        updateLikeCountButton(for: post)
        
        if let archived = post.archived, archived {
            settingsButton.isHidden = true
        } else {
            settingsButton.isHidden = false
        }
    }
    
    fileprivate func updateCommentCounts(for post: UserDataPlaceFeed) {
        if let comments = post.comments, comments.users.count > 0 {
            commentsButton.setTitle("\(comments.users.count)", for: .normal)
        } else {
            commentsButton.setTitle("", for: .normal)
        }
    }
    
    fileprivate func updateLikeCounts(for post: UserDataPlaceFeed) {
        if let upvotesCount = post.upVotesCount, upvotesCount > 0 {
            likeButton.setTitle("\(upvotesCount)", for: .normal)
        } else {
            likeButton.setTitle("", for: .normal)
        }
    }
    
    fileprivate func updateShareCounts(for post: UserDataPlaceFeed) {
        if let shareCount = post.sharesCount, shareCount > 0 {
            shareButton.setTitle("\(shareCount)", for: .normal)
        } else {
            shareButton.setTitle("", for: .normal)
        }
    }
    
    fileprivate func updateViewCountButton(for post: UserDataPlaceFeed) {
        viewsButton.setTitle("\(post.getViewsCountLabel())", for: .normal)
        
        if let viewsCount = post.viewCount, viewsCount > 0 {
            viewsButton.isHidden = false
        } else {
            viewsButton.isHidden = true
        }
    }
    
    fileprivate func updateLikeCountButton(for post: UserDataPlaceFeed) {
        if post.hasUserUpVoted {
            likeButton.setImage(UIImage.init(named: "iconHeartFull20Red100BorderWhite75Shadow")?.withRenderingMode(.alwaysOriginal), for: .normal)
        } else {
            likeButton.setImage(UIImage.init(named: "iconHeartFull20White75Shadow")?.withRenderingMode(.alwaysOriginal), for: .normal)
        }
    }
    
    // MARK: Target functions
    
    @objc fileprivate func handleLike() {
        guard let post = post else { return }
        delegate?.didUpvoteTap(post: post)
        
        guard let storyViewManager = storyViewManager else { return }
        AnalyticsHelper.likeEvent(screen: storyViewManager.screen, userId: Helper().getUserId(), feed: post)
    }
    
    @objc fileprivate func handleComment() {
        self.delegate?.didCommentTap(storyBottomControlsView: self)
        
        guard let post = post else { return }
        AnalyticsHelper.viewCommentEvent(feed: post)
    }
    
    @objc fileprivate func handleShare() {
        guard let post = post else { return }
        delegate?.didShareTap(post: post)
        
    }
    
    @objc fileprivate func handleSettings() {
        guard let post = post else { return }
        delegate?.didSettingsTap(post: post)
    }
    
    // MARK: Public functions
    
    public func setPost(post: UserDataPlaceFeed) {
        self.post = post
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
