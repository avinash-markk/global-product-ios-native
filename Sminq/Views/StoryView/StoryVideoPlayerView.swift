//
//  StoryVideoPlayerView.swift
//  Sminq
//
//  Created by Pushkar Deshmukh on 18/05/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import UIKit
import AVFoundation

enum PlayerStatus {
    case unknown
    case playing
    case failed
    case paused
    case readyToPlay
}

protocol StoryVideoPlayerViewDelegate: class {
    func didStartPlaying()
    func didCompletePlay()
    func didTrack(progress: Float)
    func didFailed(withError error: String, for url: URL?)
    func videoStatus(playerStatus: PlayerStatus, ratingCellIndex: Int, playerItem: AVPlayerItem, player: AVPlayer)
}

protocol PlayerControls: class {
    var playerStatus: PlayerStatus { get }
    
    func play(urlString: String)
    func play()
    func resume()
    func pause()
    func stop()
}

class StoryVideoPlayerView: UIView {
    //MARK: - Private Vars
    private var timeObserverToken: AnyObject?
    private var playerItemStatusObserver: NSKeyValueObservation?
    private var playerTimeControlStatusObserver: NSKeyValueObservation?
    
    private lazy var playerLayer: AVPlayerLayer? = {
        let playerLayer = AVPlayerLayer()
        playerLayer.bounds = bounds
        return playerLayer
    }()
    
    private var playerItem: AVPlayerItem? = nil {
        willSet {
            // Remove any previous KVO observer.
            guard let playerItemStatusObserver = playerItemStatusObserver else { return }
            playerItemStatusObserver.invalidate()
        }
        didSet {
            player?.replaceCurrentItem(with: playerItem)
//            playerItemStatusObserver = playerItem?.observe(\AVPlayerItem.status, options: [.new, .initial], changeHandler: { [weak self] (item, _) in
//                guard let strongSelf = self else { return }
//                if item.status == .failed {
//                    strongSelf.loaderVisibility(show: false)
//                    if let item = strongSelf.player?.currentItem, let error = item.error, let url = item.asset as? AVURLAsset {
//                        strongSelf.delegate?.didFailed(withError: error.localizedDescription, for: url.url)
//                    } else {
//                        strongSelf.delegate?.didFailed(withError: "Unknown error", for: nil)
//                    }
//                }
//            })
        }
    }
    
    //MARK: - iVars
    public var ratingCellIndex: Int = 0
    
    var player: AVPlayer? {
        willSet {
            // Remove any previous KVO observer.
            guard let playerTimeControlStatusObserver = playerTimeControlStatusObserver else { return }
            playerTimeControlStatusObserver.invalidate()
        }
        didSet {
            playerTimeControlStatusObserver = player?.observe(\AVPlayer.timeControlStatus, options: [.new, .initial], changeHandler: { [weak self] (player, _) in
                guard let strongSelf = self else { return }

                switch player.timeControlStatus {
                case .playing:
                    strongSelf.loaderVisibility(show: false)
                    strongSelf.delegate?.didStartPlaying()
//                    print("video Status callback playing")
                case .paused, .waitingToPlayAtSpecifiedRate:
                    strongSelf.loaderVisibility(show: true)
//                    print("video Status callback paused")
                }
            })
        }
    }
    
    var activityIndicator: UIActivityIndicatorView!
    
    var currentItem: AVPlayerItem? {
        return player?.currentItem
    }
    
    var currentTime: Float {
        return Float(self.player?.currentTime().value ?? 0)
    }
    
    var error: Error? {
        return player?.currentItem?.error
    }
    
    //MARK: - Public Vars
    public weak var delegate: StoryVideoPlayerViewDelegate?
 
    //MARK:- Init methods
    override init(frame: CGRect) {
        activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
        super.init(frame: frame)
        setupActivityIndicator()
        
        updateLayout()
    }
    
    override func layoutSubviews() {
        updateLayout()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        if let existingPlayer = player, existingPlayer.observationInfo != nil {
            removeObservers()
        }
        debugPrint("Deinit called")
    }
    
    // MARK: - Internal methods
    func setupActivityIndicator() {
        activityIndicator.hidesWhenStopped = true
        
        addSubview(activityIndicator)
        bringSubview(toFront: activityIndicator)
    }
    
    func removeObservers() {
        cleanUpPlayerPeriodicTimeObserver()
    }
    
    func updateLayout() {
        if let playerLayer = playerLayer {
            playerLayer.frame = bounds
        }
        
        activityIndicator.center = center
    }
    
    func cleanUpPlayerPeriodicTimeObserver() {
        if let timeObserverToken = timeObserverToken {
            player?.removeTimeObserver(timeObserverToken)
            self.timeObserverToken = nil
        }
    }
    
    func setupPlayerPeriodicTimeObserver() {
        // Only add the time observer if one hasn't been created yet.
        guard timeObserverToken == nil else { return }

        // Use a weak self variable to avoid a retain cycle in the block.
        timeObserverToken =
            player?.addPeriodicTimeObserver(forInterval: CMTimeMake(1, 100), queue: DispatchQueue.main) {
                [weak self] time in
                guard let strongSelf = self, let player = strongSelf.player, let currentItem = player.currentItem else { return }

                let currentPlayerItemDurationSeconds = CMTimeGetSeconds(currentItem.asset.duration)

                let progressTimeDurationSeconds = CMTimeGetSeconds(time)

                if currentPlayerItemDurationSeconds == progressTimeDurationSeconds {
//                    strongSelf.player?.seek(to: kCMTimeZero)
                    strongSelf.delegate?.didCompletePlay()
                }

                DispatchQueue.main.async {
                    strongSelf.delegate?.videoStatus(playerStatus: strongSelf.playerStatus, ratingCellIndex: strongSelf.ratingCellIndex, playerItem: currentItem, player: player)
                    
                    if currentItem.status == AVPlayerItemStatus.readyToPlay {
//                        print(" -- ready")
                        strongSelf.loaderVisibility(show: false)
                    } else if currentItem.status == AVPlayerItemStatus.failed {
//                        print(" -- failed")
                        strongSelf.loaderVisibility(show: false)
                    } else if currentItem.status == AVPlayerItemStatus.unknown {
//                        print(" -- unknown")
                        strongSelf.loaderVisibility(show: true)
                    }
                    
//                    let url = ((currentItem.asset) as? AVURLAsset)?.url
//                    print("currentItem Url :", url)
                }
                
                strongSelf.delegate?.didTrack(progress: Float(progressTimeDurationSeconds))
            } as AnyObject
    }
    
    // MARK: Private functions
    fileprivate func setupPlayer(url: URL) {
//        print("in setupPlayer()")
        let asset = AVAsset(url: url)
        playerItem = AVPlayerItem(asset: asset)
        player = AVPlayer(playerItem: playerItem)
        playerLayer = AVPlayerLayer(player: player)
        setupPlayerPeriodicTimeObserver()
        
        if let pLayer = playerLayer {
            pLayer.frame = bounds
            pLayer.videoGravity = .resizeAspectFill
            layer.addSublayer(pLayer)
            bringSubview(toFront: activityIndicator)
        }
    }
    
    fileprivate func loaderVisibility(show: Bool) {
        activityIndicator.isHidden = !show
        
        show ? activityIndicator.startAnimating() : activityIndicator.stopAnimating()
    }
}


// MARK: - Protocol | PlayerControls
extension StoryVideoPlayerView: PlayerControls {
    func play(urlString: String) {
        guard let url = URL(string: urlString) else { fatalError("Unable to form URL from resource") }
        
        if player == nil {
            self.setupPlayer(url: url)
        }
        
        self.loaderVisibility(show: true)
        self.play()
    }
    
    func play(url: URL) {
        //        player = nil
//        DispatchQueue.main.async {
            if let existingPlayer = self.player {
                let asset = AVAsset(url: url)
                let playerItem = AVPlayerItem(asset: asset)
                existingPlayer.replaceCurrentItem(with: playerItem)
            } else {
                self.setupPlayer(url: url)
            }
            
            self.loaderVisibility(show: true)
            self.play()
//        }
    }
    
    func play() {
        //We have used this for long press gesture
        if let existingPlayer = self.player {
//            print("Playing existingPlayer")
            //            print("Player playing from ZERO!")
            existingPlayer.seek(to: kCMTimeZero)
            existingPlayer.play()
        }
        
    }
    
    func pause() {
        //control the player
        if let existingPlayer = player {
//            print("Player pausing")
            existingPlayer.pause()
        }
    }
    
    func resume() {
        // TODO: Add a flag if player is paused.
        if let existingPlayer = player {
            existingPlayer.play()
        }
    }
    
    func stop() {
        //control the player
        guard let _ = player else { return }
        
        DispatchQueue.main.async { [weak self] in
            guard let strongSelf = self else { return }
            strongSelf.pause()
            
            if strongSelf.observationInfo != nil {
                strongSelf.removeObservers()
            }
            
            strongSelf.playerItem = nil
            strongSelf.player = nil
            strongSelf.playerLayer?.removeFromSuperlayer()
        }
    }
    
    var playerStatus: PlayerStatus {
        if let p = player {
            switch p.status {
            case .unknown: return .unknown
            case .readyToPlay: return .readyToPlay
            case .failed: return .failed
            }
        }
        return .unknown
    }
}
