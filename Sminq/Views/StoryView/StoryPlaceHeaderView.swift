//
//  StoryPlaceHeaderView.swift
//  Sminq
//
//  Created by Pushkar Deshmukh on 22/05/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import UIKit

protocol StoryPlaceHeaderViewDelegate: NSObjectProtocol {
    func didCloseTap()
    func didPlaceMoodImageTap()
    func didPlaceNameLabelTap(storyData: StoryData)
}
class StoryPlaceHeaderView: UIView {
    public weak var delegate: StoryPlaceHeaderViewDelegate?
    
    fileprivate lazy var moodImageView: UIImageView = {
       let imageView = UIImageView()
        imageView.image = UIImage(named: "iconRating164")?.withRenderingMode(.alwaysOriginal)
        imageView.contentMode = .scaleAspectFill
        imageView.widthAnchor.constraint(equalToConstant: 48).isActive = true
        imageView.heightAnchor.constraint(equalToConstant: 48).isActive = true
        imageView.isUserInteractionEnabled = true
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleMoodImageViewTap))
        imageView.addGestureRecognizer(tap)
        
        return imageView
    }()
    
    fileprivate lazy var placeNameLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 1
        label.font = UIFont(name: AppConstants.Font.primaryBold, size: 16)
        label.textColor = AppConstants.SminqColors.white100
        label.heightAnchor.constraint(equalToConstant: 24).isActive = true
        label.isUserInteractionEnabled = true
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(handlePlaceNameLabelTap))
        label.addGestureRecognizer(tap)
        
        return label
    }()
    
    fileprivate lazy var placeAddressLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 1
        label.text = "Food Truck • Overland Ave • 0.3 mi"
        label.font = UIFont(name: AppConstants.Font.primaryRegular, size: 12)
        label.textColor = AppConstants.SminqColors.white100.withAlphaComponent(0.75)
        label.heightAnchor.constraint(equalToConstant: 16).isActive = true
        return label
    }()
    
    fileprivate lazy var closeButton: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(UIImage(named: "iconClose24WhitePureShadow")?.withRenderingMode(.alwaysOriginal), for: .normal)
        button.widthAnchor.constraint(equalToConstant: 24).isActive = true
        button.heightAnchor.constraint(equalToConstant: 24).isActive = true
        button.addTarget(self, action: #selector(handleClose), for: .touchUpInside)
        return button
    }()
    
    fileprivate var storyData: StoryData? {
        didSet {
            updateData()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupLayout()
    }
    
    override func layoutSubviews() {
        roundCorners([.topLeft, .topRight], radius: 8)
    }
    
    // MARK: Fileprivate functions
    
    fileprivate func setupLayout() {
        backgroundColor = AppConstants.SminqColors.grey100
//        layer.cornerRadius = 8
        roundCorners([.topLeft, .topRight], radius: 8)
        
        addSubview(moodImageView)
        addSubview(placeNameLabel)
        addSubview(placeAddressLabel)
        addSubview(closeButton)
        
        moodImageView.anchor(top: topAnchor, leading: leadingAnchor, bottom: bottomAnchor, trailing: nil, padding: .init(top: 4, left: 16, bottom: 12, right: 0))

        placeNameLabel.anchor(top: topAnchor, leading: moodImageView.trailingAnchor, bottom: nil, trailing: trailingAnchor, padding: .init(top: 8, left: 8, bottom: 0, right: 48 + 8))

        placeAddressLabel.anchor(top: placeNameLabel.bottomAnchor, leading: moodImageView.trailingAnchor, bottom: nil, trailing: trailingAnchor, padding: .init(top: 0, left: 8, bottom: 0, right: 48 + 8))
        
        closeButton.anchor(top: topAnchor, leading: nil, bottom: nil, trailing: trailingAnchor, padding: .init(top: 16, left: 0, bottom: 0, right: 16))
    }
    
    fileprivate func updateData() {
        guard let story = storyData, let place = story.placeDetails else { return }
        
        let placeMoodScore = place.placeMoodScore ?? 0
        moodImageView.image = placeMoodScore.getMoodMeta().image
        
        placeNameLabel.text = place.name
        placeAddressLabel.text = place.getSubCategoryWithDistance()
    }
    
    // MARK: Public functions
    
    public func setStory(storyData: StoryData) {
        self.storyData = storyData
    }
    
    // MARK: @objc functions
    
    @objc fileprivate func handleClose() {
        delegate?.didCloseTap()
    }
    
    @objc fileprivate func handleMoodImageViewTap() {
        delegate?.didPlaceMoodImageTap()
    }
    
    @objc fileprivate func handlePlaceNameLabelTap() {
        guard let storyData = storyData else { return }
        delegate?.didPlaceNameLabelTap(storyData: storyData)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
