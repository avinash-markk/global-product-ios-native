//
//  StoryUserHeaderView.swift
//  Markk
//
//  Created by Pushkar Deshmukh on 24/05/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import UIKit

protocol StoryUserHeaderViewDelegate: NSObjectProtocol {
    func didPlaceMoodIconTap()
    func didLocationIconTap()
    func didUserInitialLabelTap(post: UserDataPlaceFeed)
    func didUserImageViewTap(post: UserDataPlaceFeed)
    func didUserDisplayNameTap(post: UserDataPlaceFeed)
}

class StoryUserHeaderView: UIView {
    public weak var delegate: StoryUserHeaderViewDelegate?
    
    fileprivate lazy var userImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "imgOnboarding1")
        imageView.contentMode = .scaleAspectFill
        imageView.layer.borderWidth = 1
        imageView.layer.borderColor = AppConstants.SminqColors.white100.withAlphaComponent(0.5).cgColor
        imageView.clipsToBounds = true
        imageView.contentMode = .scaleAspectFill
        imageView.layer.cornerRadius = 12
        imageView.widthAnchor.constraint(equalToConstant: 24).isActive = true
        imageView.heightAnchor.constraint(equalToConstant: 24).isActive = true
        imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleUserImageViewTap)))
        imageView.isUserInteractionEnabled = true
        return imageView
    }()
    
    fileprivate lazy var userInitialLabel: UILabel = {
        let label = UILabel()
        label.layer.borderWidth = 1
        label.layer.borderColor = AppConstants.SminqColors.white100.withAlphaComponent(0.1).cgColor
        label.backgroundColor = AppConstants.SminqColors.purple100
        label.text = "T"
        label.textColor = AppConstants.SminqColors.white100.withAlphaComponent(0.75)
        label.font = UIFont(name: AppConstants.Font.primaryBold, size: 12)
        label.clipsToBounds = true
        label.textAlignment = .center
        label.layer.cornerRadius = 12
        label.widthAnchor.constraint(equalToConstant: 24).isActive = true
        label.heightAnchor.constraint(equalToConstant: 24).isActive = true
        label.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleUserInitialLabelTap)))
        label.isUserInteractionEnabled = true
        return label
    }()
    
    fileprivate lazy var userDisplayNameLabel: UILabel = {
        let label = self.createLabels(text: "josephbrant", fontSize: 14)
        label.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleUserDisplayNameTap)))
        label.isUserInteractionEnabled = true
        return label
    }()
    
    fileprivate lazy var userPostTimeLabel: UILabel = self.createLabels(text: "15 mins", fontSize: 12)
    
    fileprivate lazy var moodIconButton: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(UIImage(named: "iconRating264")?.withRenderingMode(.alwaysOriginal), for: .normal)
        button.contentMode = .scaleAspectFill
        button.widthAnchor.constraint(equalToConstant: 32).isActive = true
        button.heightAnchor.constraint(equalToConstant: 32).isActive = true
        button.centerYAnchor.constraint(equalTo: centerYAnchor)
        button.layer.cornerRadius = 16
        button.addTarget(self, action: #selector(handlePlaceMoodTap), for: .touchUpInside)
        return button
    }()
    
//    fileprivate lazy var hourGlassIconButton: UIButton = self.createButtons(imageString: "iconHourglass10024", selector: nil)
    fileprivate lazy var locationIconButton: UIButton = self.createButtons(imageString: "iconMarkerVerified24WhitePure", selector: #selector(handleLocationTap))
    
    fileprivate var post: UserDataPlaceFeed? {
        didSet {
            updateData()
        }
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupLayout()
    }
    
    // MARK: Fileprivate functions
    
    fileprivate func setupLayout() {
        addSubview(userInitialLabel)
        addSubview(userImageView)
//        addSubview(hourGlassIconButton)
        
        userInitialLabel.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: nil, padding: .init(top: 5, left: 0, bottom: 0, right: 0))
        userImageView.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: nil, padding: .init(top: 5, left: 0, bottom: 0, right: 0))
        
        addSubview(userDisplayNameLabel)
        userDisplayNameLabel.anchor(top: topAnchor, leading: userInitialLabel.trailingAnchor, bottom: nil, trailing: trailingAnchor, padding: .init(top: 0, left: 16, bottom: 0, right: 16))
        
        addSubview(userPostTimeLabel)
        userPostTimeLabel.anchor(top: userDisplayNameLabel.bottomAnchor, leading: userInitialLabel.trailingAnchor, bottom: nil, trailing: trailingAnchor, padding: .init(top: 0, left: 16, bottom: 0, right: 16))
        
        addSubview(moodIconButton)
        moodIconButton.anchor(top: nil, leading: nil, bottom: nil, trailing: trailingAnchor, padding: .init(top: 0, left: 0, bottom: 0, right: 0))
        moodIconButton.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        addSubview(locationIconButton)
        locationIconButton.anchor(top: nil, leading: nil, bottom: nil, trailing: moodIconButton.leadingAnchor, padding: .init(top: 0, left: 0, bottom: 0, right: 8))
        locationIconButton.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        
//        addSubview(hourGlassIconButton)
//        hourGlassIconButton.anchor(top: nil, leading: nil, bottom: nil, trailing: locationIconButton.leadingAnchor, padding: .init(top: 0, left: 0, bottom: 0, right: 8))
//        hourGlassIconButton.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
    }
    
    fileprivate func createLabels(text: String, fontSize: CGFloat) -> UILabel {
        let label = UILabel()
        label.text = text
        label.font = UIFont(name: AppConstants.Font.primaryRegular, size: fontSize)
        label.textColor = AppConstants.SminqColors.white100
        return label
    }
    
    fileprivate func createButtons(imageString: String, selector: Selector?) -> UIButton {
        let button = UIButton(type: .system)
        button.setImage(UIImage(named: imageString)?.withRenderingMode(.alwaysOriginal), for: .normal)
        button.contentMode = .scaleAspectFill
        button.widthAnchor.constraint(equalToConstant: 24).isActive = true
        button.heightAnchor.constraint(equalToConstant: 24).isActive = true
        button.centerYAnchor.constraint(equalTo: centerYAnchor)
        
        if let selector = selector {
            button.addTarget(self, action: selector, for: .touchUpInside)
        }
        return button
    }
    
    fileprivate func updateData() {
        guard let post = post else { return }
        userDisplayNameLabel.text = post.displayName
        userPostTimeLabel.text = Helper().getTimeAgoString(for: post.createdAt)
        
        userInitialLabel.text = "\(post.displayName.uppercased().prefix(1))"
        userImageView.sd_setImage(with: URL(string: post.user.imageUrl), placeholderImage: nil)
        
        locationIconButton.setImage(post.getLocationVerificationImageIcon()?.withRenderingMode(.alwaysOriginal), for: .normal)
        
        let moodScore = post.moodScore
        moodIconButton.setImage(moodScore.getMoodMeta().image?.withRenderingMode(.alwaysOriginal), for: .normal)
    }
    
    // MARK: Public functions
    
    public func setPost(post: UserDataPlaceFeed) {
        self.post = post
    }
    
    // MARK: @objc functions
    @objc fileprivate func handlePlaceMoodTap() {
        AnalyticsHelper.storyViewExplainerEvent(eventName: AnalyticsHelper.StoryViewExplainerEventName.ratingEvent)
        delegate?.didPlaceMoodIconTap()
    }
    
    @objc fileprivate func handleLocationTap() {
        AnalyticsHelper.storyViewExplainerEvent(eventName: AnalyticsHelper.StoryViewExplainerEventName.locVerificationEvent)
        delegate?.didLocationIconTap()
    }
    
    @objc fileprivate func handleUserImageViewTap() {
        guard let post = post else { return }
        delegate?.didUserImageViewTap(post: post)
    }
    
    @objc fileprivate func handleUserInitialLabelTap() {
        guard let post = post else { return }
        delegate?.didUserInitialLabelTap(post: post)
    }
    
    @objc fileprivate func handleUserDisplayNameTap() {
        guard let post = post else { return }
        delegate?.didUserDisplayNameTap(post: post)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
