//
//  PlaceMoodModalView.swift
//  Sminq
//
//  Created by Pushkar Deshmukh on 10/04/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import UIKit

protocol PlaceMoodModalViewDelegate: NSObjectProtocol {
    func didCloseButtonTap()
}

class PlaceMoodModalView: UIView {
    public weak var delegate: PlaceMoodModalViewDelegate?
    
    @IBOutlet weak var overlayView: UIView!
    
    @IBOutlet weak var moodView: UIView!
    @IBOutlet weak var moodViewTopConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var selectedMoodIcon: UIImageView!
    
    @IBOutlet weak var mainStackView: UIStackView!
    
    @IBOutlet weak var moodImage1: UIImageView!
    @IBOutlet weak var moodImage2: UIImageView!
    @IBOutlet weak var moodImage3: UIImageView!
    
    
    @IBOutlet weak var placeMoodLabel: UILabel!
    
//    fileprivate lazy var selectedMoodIcon: UIImageView = {
//        let imageView = UIImageView(image: UIImage.init(named: "iconArrowDropUp24White100"))
//        return imageView
//    }()
    
    fileprivate var post: UserDataPlaceFeed? {
        didSet {
            guard let post = post else { return }
            enableMoodIcon(placeMood: post.moodScore)
        }
    }
    
    fileprivate var place: LivePlaces? {
        didSet {
            guard let place = place else { return }
            enableMoodIcon(placeMood: place.placeMoodScore ?? 0)
        }
    }
    
    override func layoutSubviews() {
        self.initUI()
    }
    
    // MARK: Private functions
    
    private func initUI() {
        self.overlayView.backgroundColor = AppConstants.SminqColors.black75
        
        self.moodView.backgroundColor = AppConstants.SminqColors.violet100
        self.moodView.layer.cornerRadius = 8
        self.moodView.borderColor = AppConstants.SminqColors.white100.withAlphaComponent(0.1)
        self.moodView.borderWidth = 1
        
        self.placeMoodLabel.font = UIFont(name: AppConstants.Font.primaryRegular, size: 16)
        self.placeMoodLabel.textColor = AppConstants.SminqColors.white100
        self.placeMoodLabel.text = "People give live ratings to places \n on a scale of Nope to Dope."
        
        
        selectedMoodIcon.snp.makeConstraints { (make) in
            make.center.equalTo(moodImage2)
        }
    }

    // MARK: Button actions
    
    @IBAction func closeButtonTapped(_ sender: UIButton) {
        self.isHidden = true
        
        self.delegate?.didCloseButtonTap()
    }
    
    // MARK: Public functions
    
    func enableMoodIcon(placeMood: Int) {
        switch placeMood {
        case 1:
            let mood1WindowRect = self.moodImage1.convert(self.moodImage1.bounds, to: nil)
            self.selectedMoodIcon.frame = mood1WindowRect
            
        case 2:
            let mood2WindowRect = self.moodImage2.convert(self.moodImage2.bounds, to: nil)
            self.selectedMoodIcon.frame = mood2WindowRect
            
        case 3:
            let mood3WindowRect = self.moodImage3.convert(self.moodImage3.bounds, to: nil)
            self.selectedMoodIcon.frame = mood3WindowRect
            
        default:
            let mood2WindowRect = self.moodImage2.convert(self.moodImage2.bounds, to: nil)
            self.selectedMoodIcon.frame = mood2WindowRect
        }
        
        self.selectedMoodIcon.height = 24
        self.selectedMoodIcon.width = 24
        self.selectedMoodIcon.frame.origin.y = mainStackView.frame.origin.y + mainStackView.height + 8
        self.selectedMoodIcon.frame.origin.x = selectedMoodIcon.frame.origin.x + 16
    
    }
    
    // Public functions
    
    func setMoodViewTopConstraint(top: CGFloat) {
        self.moodViewTopConstraint.constant = top
    }
    
    public func setPost(post: UserDataPlaceFeed) {
        self.post = post
    }
    
    public func setPlace(place: LivePlaces) {
        self.place = place
    }
}
