//
//  EmptyState.swift
//  Sminq
//
//  Created by Pushkar Deshmukh on 06/10/18.
//  Copyright © 2018 SMINQ. All rights reserved.
//

import UIKit

class EmptyState: UIView {

    var contentView: UIView!
    
    @IBOutlet weak var headingLabel: UILabel!
    @IBOutlet weak var subHeadingLabel: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        contentView.frame = self.frame
    }
    
    func xibSetup() {
        contentView = loadNib()
        contentView.frame = bounds
        contentView.backgroundColor = AppConstants.SminqColors.blackDark100
        addSubview(contentView)
        
        self.headingLabel.font = UIFont(name: AppConstants.Font.primaryRegular, size: 24)
        self.headingLabel.textColor = AppConstants.SminqColors.white100
        
        self.subHeadingLabel.font = UIFont(name: AppConstants.Font.primaryRegular, size: 16)
        self.subHeadingLabel.textColor = AppConstants.SminqColors.white100
    }
    
    func setContentViewBackgroundColor(color: UIColor) {
        self.contentView.backgroundColor = color
    }
    
}
