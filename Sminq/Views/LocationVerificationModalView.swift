//
//  LocationVerificationModalView.swift
//  Sminq
//
//  Created by Pushkar Deshmukh on 11/04/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import UIKit

protocol LocationVerificationModalViewDelegate: NSObjectProtocol {
    func didLocationVerificationModalCloseButtonTap()
}

class LocationVerificationModalView: UIView {
    public weak var delegate: LocationVerificationModalViewDelegate?
    
    @IBOutlet weak var overlayView: UIView!
    
    @IBOutlet weak var locationView: UIView!
    
    @IBOutlet weak var locationIconImageView: UIImageView!
    @IBOutlet weak var locationViewTopConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var headingLabel: UILabel!
    @IBOutlet weak var subHeadingLabel: UILabel!
    
    fileprivate var post: UserDataPlaceFeed? {
        didSet {
            guard let post = post else { return }
            updateLocationView(feed: post)
        }
    }
    
    override func layoutSubviews() {
        self.initUI()
    }
    
    // MARK: Private functions
    
    private func initUI() {
        self.overlayView.backgroundColor = AppConstants.SminqColors.black75
        
        self.locationView.backgroundColor = AppConstants.SminqColors.purple100
        self.locationView.layer.cornerRadius = 8
        self.locationView.borderColor = AppConstants.SminqColors.white100.withAlphaComponent(0.1)
        self.locationView.borderWidth = 1
        
        self.headingLabel.font = UIFont(name: AppConstants.Font.primaryBold, size: 16)
        self.headingLabel.textColor = AppConstants.SminqColors.white100
        
        self.subHeadingLabel.font = UIFont(name: AppConstants.Font.primaryRegular, size: 16)
        self.subHeadingLabel.textColor = AppConstants.SminqColors.white100
    }
    
    // MARK: Public functions
    
    func updateLocationView(feed: UserDataPlaceFeed) {
        if let locationVerificationState = feed.locationVerified {
            switch locationVerificationState {
            case .success:
                self.locationView.backgroundColor = AppConstants.SminqColors.green200
                self.headingLabel.text = "Location Verified"
                self.subHeadingLabel.text = "@\(feed.displayName ?? "") added this rating \n while at the place"
                self.locationIconImageView.image = UIImage(named: "iconMarkerVerified48White100")
            case .pending:
                self.locationView.backgroundColor = AppConstants.SminqColors.grey100
                self.headingLabel.text = "Location Verification Pending"
                self.subHeadingLabel.text = "Verifying the distance between \n the place and @\(feed.displayName ?? "")"
                self.locationIconImageView.image = UIImage(named: "iconMarkerPending48White100")
            case .failed:
                // TODO: mi/km/ft/m should be dynamic based on country
                self.locationView.backgroundColor = AppConstants.SminqColors.grey100
                self.headingLabel.text = "Location Not Verified"
                self.subHeadingLabel.text = "@\(feed.displayName ?? "") was more than 1 \(SminqAPI.getDistanceUnit()) \n away while rating this place"
                self.locationIconImageView.image = UIImage(named: "iconMarkerUnverified48White100")
            }
        } else {
            self.locationView.backgroundColor = AppConstants.SminqColors.grey100
            self.headingLabel.text = "Location Verification Pending"
            self.subHeadingLabel.text = "Verifying the distance between \n the place and @\(feed.displayName ?? "")"
            self.locationIconImageView.image = UIImage(named: "iconMarkerPending48White100")
        }
        
    }
    
    public func setPost(post: UserDataPlaceFeed) {
        self.post = post
    }
    
    // MARK: Button actions
    
    @IBAction func closeButtonTapped(_ sender: UIButton) {
        self.isHidden = true
        
        self.delegate?.didLocationVerificationModalCloseButtonTap()
    }
    
    func setLocationViewTopConstraint(top: CGFloat) {
        self.locationViewTopConstraint.constant = top
    }
}
