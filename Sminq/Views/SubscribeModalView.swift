//
//  SubscribeModalView.swift
//  Markk
//
//  Created by Pushkar Deshmukh on 18/07/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import UIKit
import SVProgressHUD

protocol SubscribeModalViewDelegate: NSObjectProtocol {
    func didOverlayTap()
    func didViewRemove()
    func didAnimateOutInitiate()
    func didUserUpdateFailWithError(error: ErrorObject?)
    func didEnterInvalidEmail(email: String)
}

class SubscribeModalView: UIView {
    var contentView: UIView!
    
    public weak var delegate: SubscribeModalViewDelegate?
    @IBOutlet weak var overlayView: UIView!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var pushNotificationButton: UIButton!
    
    @IBOutlet weak var emailAddressButton: UIButton!
    @IBOutlet weak var headingLabel: UILabel!
    @IBOutlet weak var subHeadingLabel: UILabel!
    @IBOutlet weak var mainViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var emailTextField: UITextField!
    
    @IBOutlet weak var pushNotificationButtonBottomConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var confirmEmailButtonBottomConstraint: NSLayoutConstraint!
    
    
    @IBOutlet weak var confirmEmailButton: UIButton!
    fileprivate let panGestureOffset: CGFloat = 70.0
    
    var isEmailFormActive: Bool = false {
        didSet {
            emailAddressButton.isHidden = isEmailFormActive
            pushNotificationButton.isHidden = isEmailFormActive
            confirmEmailButton.isHidden = !isEmailFormActive
            emailTextField.isHidden = !isEmailFormActive
            
        }
    }
    
    var profileViewModel: ProfileViewModel?
   
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupUI()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        contentView.frame = self.frame
        
        updateButtonStates()
    }
    
    
    
    // MARK: Fileprivate functions
    
    fileprivate func setupUI() {
        contentView = loadNib()
        contentView.frame = bounds
        addSubview(contentView)
        
        profileViewModel = ProfileViewModel.init(displayName: "\(Helper().getUserDisplayName())", isMyProfile: true)
        
        addGestureRecognizer(UIPanGestureRecognizer(target: self, action: #selector(handlePan)))
        overlayView.isUserInteractionEnabled = true
        overlayView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleOverlayViewTap)))
        overlayView.backgroundColor = AppConstants.SminqColors.shadow.withAlphaComponent(0.75)
        
        mainView.backgroundColor = AppConstants.SminqColors.green200
        mainView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleMainViewTap)))
        mainView.layer.cornerRadius = 8
        
        pushNotificationButton.layer.cornerRadius = 24
        pushNotificationButton.titleLabel?.font = UIFont.init(name: AppConstants.Font.primaryBold, size: 14)
        pushNotificationButton.addTarget(self, action: #selector(handlePushNotificationTap), for: .touchUpInside)
        
        emailAddressButton.titleLabel?.font = UIFont.init(name: AppConstants.Font.primaryBold, size: 14)
        emailAddressButton.addTarget(self, action: #selector(handleEmailAddressTap), for: .touchUpInside)
        emailAddressButton.layer.cornerRadius = 24
        
        headingLabel.text = "Subscribe to the Weekend Guide"
        headingLabel.font = UIFont.init(name: AppConstants.Font.primaryBold, size: 16)
        headingLabel.textColor = AppConstants.SminqColors.white100
        headingLabel.textAlignment = .center
        headingLabel.numberOfLines = 1
        
        subHeadingLabel.text = "Get notified on Friday evenings\nwhen the community starts sharing\nlive updates from places around DTLA."
        subHeadingLabel.font = UIFont.init(name: AppConstants.Font.primaryRegular, size: 16)
        subHeadingLabel.textColor = AppConstants.SminqColors.white100
        subHeadingLabel.textAlignment = .center
        subHeadingLabel.numberOfLines = 3
        
        confirmEmailButton.setTitle("CONFIRM EMAIL", for: .normal)
        confirmEmailButton.setBackgroundColor(AppConstants.SminqColors.white100, forState: .normal)
        confirmEmailButton.titleLabel?.font = UIFont.init(name: AppConstants.Font.primaryBold, size: 14)
        confirmEmailButton.setTitleColor(AppConstants.SminqColors.green200, for: .normal)
        confirmEmailButton.layer.cornerRadius = 24
        confirmEmailButton.addTarget(self, action: #selector(handleConfirmEmailTap), for: .touchUpInside)
    
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .center
        
        emailTextField.keyboardType = .emailAddress
        emailTextField.layer.masksToBounds = true
        emailTextField.layer.cornerRadius = 24
        emailTextField.borderStyle = .none
        emailTextField.backgroundColor = AppConstants.SminqColors.green200
        emailTextField.textAlignment = .center
        emailTextField.layer.borderColor = AppConstants.SminqColors.white100.cgColor
        emailTextField.layer.borderWidth = 1
        emailTextField.attributedPlaceholder = NSAttributedString(string: "Enter email ID...", attributes: [
            NSAttributedStringKey.foregroundColor: AppConstants.SminqColors.white100.withAlphaComponent(0.75),
            NSAttributedStringKey.paragraphStyle: paragraphStyle
            ])
        emailTextField.font = UIFont(name: AppConstants.Font.primaryRegular, size: 16)
        emailTextField.textColor = AppConstants.SminqColors.white100
        
        updateUIForOtherSizes()
        setupNotificationObservers()
        updateButtonStates()

    }
    
    fileprivate func setupNotificationObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardHide(notification:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    fileprivate func updateUIForOtherSizes() {
        if UIScreen.main.bounds.height <= AppConstants.IphoneDeviceSizes.iphone5Height {
            headingLabel.font = UIFont.init(name: AppConstants.Font.primaryBold, size: 12)
            subHeadingLabel.font = UIFont.init(name: AppConstants.Font.primaryRegular, size: 12)
            if mainViewHeightConstraint != nil {
                _ = mainViewHeightConstraint.setMultiplier(multiplier: 0.92)
            }
            
        }
        
        pushNotificationButtonBottomConstraint.constant = self.getWindowSafeAreaInsets().bottom + 16
        confirmEmailButtonBottomConstraint.constant = self.getWindowSafeAreaInsets().bottom + 16
    }
  
    @objc fileprivate func handlePan(gesture: UIPanGestureRecognizer) {
        switch gesture.state {
        case .began: handlePanBeganState(gesture: gesture)
        case .changed: handlePanChangedState(gesture: gesture)
        case .ended: handlePanEndedState(gesture: gesture)
        default: break
        }
    }
    
    fileprivate func handlePanBeganState(gesture: UIPanGestureRecognizer) {
        endEditing(true)
    }
    
    fileprivate func handlePanChangedState(gesture: UIPanGestureRecognizer) {
        let translation = gesture.translation(in: nil)
        
        if translation.y > 0 {
            print("Changing y:", translation.y)
            self.transform = CGAffineTransform(translationX: 0, y: translation.y)
//            self.frame.origin = CGPoint(x: 0, y: translation.y)
        }
    }
    
    fileprivate func handlePanEndedState(gesture: UIPanGestureRecognizer) {
        let translation = gesture.translation(in: nil)
        if translation.y > panGestureOffset {
            animateOutAndRemove()
        } else {
            UIView.animate(withDuration: AppConstants.defaultAnimateDuration) {
                self.transform = .identity
            }
        }
    }
    
    fileprivate func updateNotificationButtonState() {
        UserNoficationService.shared.getNotificationSettings { (settings) in
            switch settings {

            case .authorized, .provisional:
                self.pushNotificationButton.setTitle("PUSH ENABLED", for: .normal)
                self.pushNotificationButton.setTitleColor(AppConstants.SminqColors.white100, for: .normal)
                self.pushNotificationButton.layer.borderColor = AppConstants.SminqColors.white100.cgColor
                self.pushNotificationButton.layer.borderWidth = 1
                
                self.pushNotificationButton.setBackgroundColor(AppConstants.SminqColors.green200, forState: .normal)
                self.pushNotificationButton.moveImageLeftTextCenter(image: UIImage.init(named: "iconCheckCircleFilled24White100")!, imagePadding: 24, renderingMode: .alwaysOriginal)
                break
            
            case .notDetermined, .denied:
                print("Not determined")
                self.pushNotificationButton.setTitle("ENABLE PUSH NOTIFICATION", for: .normal)
                self.pushNotificationButton.setTitleColor(AppConstants.SminqColors.green200, for: .normal)
                self.pushNotificationButton.layer.borderWidth = 0
                
                self.pushNotificationButton.setBackgroundColor(AppConstants.SminqColors.white100, forState: .normal)
                self.pushNotificationButton.moveImageLeftTextCenter(image: UIImage.init(named: "iconAddCircle24Green100")!, imagePadding: 24, renderingMode: .alwaysOriginal)
            
            }
        }
    }
    
    fileprivate func updateEmailButtonState() {
        guard let user = Helper().getUser() else { return }
        if user.email == "" {
            emailAddressButton.setTitle("ADD EMAIL ADDRESS", for: .normal)
            emailAddressButton.setTitleColor(AppConstants.SminqColors.green200, for: .normal)
            emailAddressButton.layer.borderWidth = 0
            
            emailAddressButton.setBackgroundColor(AppConstants.SminqColors.white100, forState: .normal)
            emailAddressButton.moveImageLeftTextCenter(image: UIImage.init(named: "iconAddCircle24Green100")!, imagePadding: 24, renderingMode: .alwaysOriginal)
        } else {
            emailAddressButton.setTitle("EMAIL VERIFIED", for: .normal)
            emailAddressButton.setTitleColor(AppConstants.SminqColors.white100, for: .normal)
            emailAddressButton.layer.borderWidth = 1
            emailAddressButton.layer.borderColor = AppConstants.SminqColors.white100.cgColor
            
            emailAddressButton.setBackgroundColor(AppConstants.SminqColors.green200, forState: .normal)
            emailAddressButton.moveImageLeftTextCenter(image: UIImage.init(named: "iconCheckCircleFilled24White100")!, imagePadding: 24, renderingMode: .alwaysOriginal)
        }
        
        emailAddressButton.layoutIfNeeded()
    }
    
    
    // MARK: Button actions
    
    @IBAction func handleDownArrowIconTap(_ sender: Any) {
        animateOutAndRemove()
    }
    
    // MARK: @objc functions
    @objc func handleConfirmEmailTap() {
        guard let email = emailTextField.text, let user = Helper().getUser() else { return }
        
        if !Helper().isValidEmail(email: email) {
            delegate?.didEnterInvalidEmail(email: email)
            return
        }
        
        SVProgressHUD.show()
        profileViewModel?.updateUser(userId: user._id, name: user.name, displayName: user.displayName.first, email: email, mobile: nil, latitude: nil, longitude: nil, countryName: nil, cityName: nil, countryCode: nil, gender: nil, birthDate: nil, isSignUp: 0, isExistingUser: true, bio: nil, image: nil, completion: { (userDataDict, apiResponseState, networkError, error) in
            SVProgressHUD.dismiss()
            
            if let error = error {
                self.delegate?.didUserUpdateFailWithError(error: error)
                return
            }
            
            self.endEditing(true)
            self.isEmailFormActive = false
            self.updateEmailButtonState()
        })
    }
    
    @objc func handleKeyboardShow(notification: NSNotification) {
        if let userInfo = notification.userInfo as? Dictionary<String, AnyObject> {
            let frame = userInfo[UIKeyboardFrameEndUserInfoKey]
            let keyboardRect = frame?.cgRectValue
            
            if let height = keyboardRect?.height {
                UIView.animate(withDuration: AppConstants.defaultAnimateDuration, animations: {
                    self.transform = CGAffineTransform(translationX: 0, y: -height / 2)
                    self.setNeedsLayout()
                })
            }
        }
    }
    
    @objc func handleKeyboardHide(notification: NSNotification) {
        UIView.animate(withDuration: AppConstants.defaultAnimateDuration, animations: {
            self.transform = .identity
            self.setNeedsLayout()
        })
    }
    
    @objc fileprivate func handleOverlayViewTap() {
        animateOutAndRemove()
    }
    
    @objc fileprivate func handleMainViewTap() {
        self.endEditing(true)
        return
    }
    
    @objc fileprivate func handlePushNotificationTap() {
        UserNoficationService.shared.getNotificationSettings { (settings) in
            switch settings {
            case .authorized, .provisional: break
            case .notDetermined: UserNoficationService.shared.authorize(completion: { (granted) in
                if granted {
                    AnalyticsHelper.appPermissionAnalyticEvent(eventName: AnalyticsHelper.PermissionsEventName.notificationPermission, permission: AppConstants.granted)
                    self.updateNotificationButtonState()
                    UserNoficationService.shared.subscribeToPushNotifications()
                }
            })
            case .denied: UIApplication.openAppSettings()
                AnalyticsHelper.appPermissionAnalyticEvent(eventName: AnalyticsHelper.PermissionsEventName.notificationPermission, permission: AppConstants.denied)
            }
        }
    }
    
    @objc fileprivate func handleEmailAddressTap() {
        guard let user = Helper().getUser() else { return }
        if user.email != "" {
            // NOTE: Do not do anything if email address is already present
            return
        }
        
        isEmailFormActive = true
    }
    
    // MARK: Public functions
    
    public func animateInView() {
        isEmailFormActive = false
        
        self.frame = CGRect(x: 0, y: UIScreen.main.bounds.height, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        
        UIView.animate(withDuration: AppConstants.defaultAnimateDuration) {
            self.frame.origin = .zero
        }
    }
    
    public func animateOutAndRemove() {
        delegate?.didAnimateOutInitiate()
        
        UIView.animate(withDuration: AppConstants.defaultAnimateDuration, animations: {
            self.transform = CGAffineTransform(translationX: 0, y: UIScreen.main.bounds.height)
        }) { (finished) in
            if finished {
                print("Remove from parent!")
                self.transform = .identity
                
                self.removeFromSuperview()
                self.delegate?.didViewRemove()
            }
        }
    }
    
    public func updateButtonStates() {
        updateNotificationButtonState()
        updateEmailButtonState()
    }
    
}
