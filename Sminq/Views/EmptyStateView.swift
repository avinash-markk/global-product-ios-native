//
//  EmptyStateView.swift
//  Markk
//
//  Created by Pushkar Deshmukh on 28/05/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import UIKit

class EmptyStateView: UIView {

    fileprivate lazy var headingLabel: UILabel = {
        let label = UILabel()
        label.text = "It’s quiet here."
        label.numberOfLines = 0
        label.font = UIFont(name: AppConstants.Font.primaryRegular, size: 24)
        label.textColor = AppConstants.SminqColors.purple100
        label.textAlignment = .center
        return label
    }()
    
    fileprivate lazy var subHeadingLabel: UILabel = {
        let label = UILabel()
        label.text = "Leave a comment \n to start a discussion."
        label.numberOfLines = 0
        label.font = UIFont(name: AppConstants.Font.primaryRegular, size: 16)
        label.textColor = AppConstants.SminqColors.purple100.withAlphaComponent(0.75)
        label.textAlignment = .center
        return label
    }()
    
    fileprivate var headingText: String? {
        didSet {
            guard let headingText = headingText else { return }
            headingLabel.text = headingText
        }
    }
    
    fileprivate var subHeadingText: String? {
        didSet {
            guard let subHeadingText = subHeadingText else { return }
            subHeadingLabel.text = subHeadingText
        }
    }
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupLayout()
        
    }

    // MARK: Fileprivate functions
    
    fileprivate func setupLayout() {
        backgroundColor = .white
        
        layer.cornerRadius = 8
        layer.borderWidth = 1
        layer.borderColor = AppConstants.SminqColors.purple100.withAlphaComponent(0.1).cgColor
        
        let stackView = UIStackView(arrangedSubviews: [headingLabel, subHeadingLabel])
        stackView.axis = .vertical
        stackView.alignment = .center
        stackView.spacing = 16
//        stackView.distribution = .fillEqually
        addSubview(stackView)
        stackView.anchor(top: nil, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor)
        stackView.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
    }

    // MARK: public functions
    
    public func setLabels(headingText: String, subHeadingText: String) {
        self.headingText = headingText
        self.subHeadingText = subHeadingText
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
