//
//  OnBoardingSlide.swift
//  Sminq
//
//  Created by Pushkar Deshmukh on 27/03/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import UIKit
import Lottie

class OnBoardingSlide: UIView {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var headingLabel: UILabel!
    @IBOutlet weak var subHeadingLabel: UILabel!
    
    @IBOutlet weak var imageHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var imageWidthConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var subHeadingBottomConstraint: NSLayoutConstraint!
    
    var animationView = AnimationView()
    
    override func layoutSubviews() {
        self.initUI()
    }
    
    var subHeadingBottomConstraintDefault: CGFloat = 22.0
    // MARK: Private functions
    
    private func initUI() {
        imageView.isHidden = false
        self.headingLabel.textColor = AppConstants.SminqColors.white100
        self.headingLabel.font = UIFont(name: AppConstants.Font.montSerratBlack, size: UIScreen.main.bounds.width / 10.4)
    
        self.subHeadingLabel.textColor = AppConstants.SminqColors.white100
        self.subHeadingLabel.font = UIFont(name: AppConstants.Font.primaryRegular, size: UIScreen.main.bounds.width / 23.4)
        
        self.imageHeightConstraint.constant = UIScreen.main.bounds.width / 1.1
        self.imageWidthConstraint.constant = UIScreen.main.bounds.width / 1.1
        
        self.subHeadingBottomConstraint.constant = UIScreen.main.bounds.height / 36.9
    }
    
//    func setupAnimationView(animationName: String) {
//        animationView = AnimationView(name: animationName)
//
//        if animationName == "onb-1" || animationName == "onb-3" {
//            animationView.loopMode = .loop
//        }
//
//        animationView.animationSpeed = 0.7
//        self.addSubview(animationView)
//        animationView.play()
//    }
    
    func updateAnimationViewFrame() {
        imageView.frame = CGRect(x: 16, y: -16, width: self.width - 32, height: 288)
    }
    
}
