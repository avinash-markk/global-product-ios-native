//
//  EmptyPlaceStoryCollectionViewCell.swift
//  Sminq
//
//  Created by Pushkar Deshmukh on 06/03/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import UIKit

class EmptyPlaceStoryCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var cardView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.initUI()
    }
    
    // MARK: Private functions
    
    func initUI() {
        self.cardView.backgroundColor = AppConstants.SminqColors.blackLight100
        self.cardView.clipsToBounds = true
        self.cardView.layer.cornerRadius = 8
        self.cardView.borderWidth = 1
        self.cardView.borderColor = AppConstants.SminqColors.white100.withAlphaComponent(0.1)
    }

}
