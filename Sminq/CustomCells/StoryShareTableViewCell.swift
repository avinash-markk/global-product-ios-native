//
//  StoryShareTableViewCell.swift
//  Markk
//
//  Created by Pushkar Deshmukh on 19/08/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import UIKit

class StoryShareTableViewCell: UITableViewCell {

    fileprivate lazy var shareIconImageView: UIImageView = {
        let imageView = UIImageView()
        return imageView
    }()
    
    fileprivate lazy var shareOptionLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.init(name: AppConstants.Font.primaryRegular, size: 16)
        label.textColor = AppConstants.SminqColors.blackDark100.withAlphaComponent(0.75)
        return label
    }()
    
    fileprivate lazy var arrowIconImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage.init(named: "iconChevronRight24BlackDark25")
        return imageView
    }()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupLayout()
    }
    
    fileprivate var storyShareOption: StoryShareOption? {
        didSet {
            updateData()
        }
    }

    // Mark: Fileprivate functions
    
    fileprivate func setupLayout() {
        selectionStyle = .none
        
        addSubview(shareIconImageView)
        addSubview(shareOptionLabel)
        addSubview(arrowIconImageView)
        
        shareIconImageView.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: nil, padding: .init(top: 12.5, left: 16, bottom: 0, right: 0), size: .init(width: 24, height: 24))
        
        shareOptionLabel.anchor(top: topAnchor, leading: shareIconImageView.trailingAnchor, bottom: nil, trailing: arrowIconImageView.leadingAnchor, padding: .init(top: 14, left: 16, bottom: 0, right: 16))
        
        arrowIconImageView.anchor(top: topAnchor, leading: nil, bottom: nil, trailing: trailingAnchor, padding: .init(top: 12.5, left: 0, bottom: 0, right: 16), size: .init(width: 24, height: 24))
    }
    
    fileprivate func updateData() {
        guard let storyShareOption = storyShareOption else { return }
        
        shareIconImageView.image = UIImage.init(named: storyShareOption.imageString)
        shareOptionLabel.text = storyShareOption.label
    }
    
    // MARK: Public functions
    
    public func setData(storyShareOption: StoryShareOption) {
        self.storyShareOption = storyShareOption
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
