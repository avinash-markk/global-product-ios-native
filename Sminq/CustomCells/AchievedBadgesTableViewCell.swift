//
//  AchievedBadgesTableViewCell.swift
//  Markk
//
//  Created by Pushkar Deshmukh on 24/06/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import Foundation
import UIKit

protocol AchievedBadgesTableViewCellDelegate: NSObjectProtocol {
     func achievementsMoreViewTapped()
}

class AchievedBadgesTableViewCell: UITableViewCell {
    
    weak var delegate: AchievedBadgesTableViewCellDelegate?
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var skeletonView: UIView!
    
    @IBOutlet weak var badgeIcon: UIImageView!
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var levelLabel: UILabel!
    @IBOutlet weak var moreLabel: UILabel!
    @IBOutlet weak var moreSubview: UIView!
    
    @IBOutlet weak var labelsStackView: UIStackView!
    
    @IBOutlet weak var levelHeadingLabel: UILabel!
    
    @IBOutlet weak var levelSubheadingLabel: UILabel!
    
    var achievementHighlights: AchievementHighlight!
    var levelValue: Int = 0
    
    fileprivate var isLoading: Bool = false {
        didSet {
            if isLoading {
                skeletonView.showAnimatedGradientAnimation()
            } else {
                skeletonView.hideAnimatedGratientAnimation()
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(moreViewTapped))
        self.moreSubview.addGestureRecognizer(tapGesture)
        initUI()
    }
    
    @objc func moreViewTapped() {
        self.delegate?.achievementsMoreViewTapped()
    }
    
    // MARK: Fileprivate functions
    
    fileprivate func initUI() {
        selectionStyle = .none
        
        skeletonView.clipsToBounds = true
        skeletonView.layer.cornerRadius = 8
        
        progressView.layer.cornerRadius = 4
        progressView.clipsToBounds = true
        progressView.progressTintColor = AppConstants.SminqColors.green100
        progressView.trackTintColor = AppConstants.SminqColors.white100.withAlphaComponent(0.24)
        
        levelLabel.font = UIFont(name: AppConstants.Font.primaryRegular, size: 14)
        levelLabel.textColor = .white
        
        moreLabel.text = "MORE"
        moreLabel.backgroundColor = AppConstants.SminqColors.blackDark100
        moreLabel.font = UIFont(name: AppConstants.Font.primaryBold, size: 12)
        moreLabel.textColor = UIColor.white.withAlphaComponent(0.5)
        
        labelsStackView.addBackground(color: AppConstants.SminqColors.blackLight100, cornerRadius: 8, borderColor: UIColor.white.withAlphaComponent(0.1), borderWidth: 1)
        
        labelsStackView.isLayoutMarginsRelativeArrangement = true
        labelsStackView.layoutMargins = .init(top: 16, left: 16, bottom: 16, right: 16)
                
        levelHeadingLabel.font = UIFont(name: AppConstants.Font.primaryBold, size: 12)
        levelHeadingLabel.textColor = AppConstants.SminqColors.white100.withAlphaComponent(0.75)
        levelHeadingLabel.text = "Level up!"
        
        levelSubheadingLabel.font = UIFont(name: AppConstants.Font.primaryRegular, size: 12)
        levelSubheadingLabel.textColor = AppConstants.SminqColors.white100.withAlphaComponent(0.75)
        levelSubheadingLabel.text = "You’re just 1 more rating away from unlocking the Markker Level 1 badge."
    }
    
    // MARK: Public functions
    
    public func setLoadingState(loading: Bool) {
        self.isLoading = loading
    }
    
    func setUserAchievementData(highlight: AchievementHighlight) {
        self.achievementHighlights = highlight
        guard let levelName = self.achievementHighlights.id else { return }
        let level: [AchievementLevel] = self.achievementHighlights.achievementlevels
        if level.count > 0 {
            self.levelHeadingLabel.text = level[0].messageHeader
            self.levelSubheadingLabel.text = level[0].message
            levelValue = level[0].level
            self.levelLabel.attributedText = self.getAttributedLevelString(name: levelName, level: "Level \(levelValue)")
            let progress = self.getProgressValue(progress: level[0].currentProgress)
            progressView.setProgress(progress, animated: true)
            if let badge = level[0].badge {
                self.setBadgeIcon(progress: level[0].currentProgress, forBadge: badge)
            }
        }
    }
    
    func getProgressValue(progress: Int?) -> Float {
        guard let progressValue = progress else { return 0.0 }
        return Float(progressValue) / 100
    }
    
    func setBadgeIcon(progress: Int, forBadge: Badge) {
        if progress > 0 {
            if let imageUrl = forBadge.homeIconActive {
                self.badgeIcon.sd_setImage(with: URL(string: imageUrl), completed: nil)
            }
        } else {
            if let imageUrl = forBadge.homeIconInactive {
                self.badgeIcon.sd_setImage(with: URL(string: imageUrl), completed: nil)
            }
        }
    }
    
    func getAttributedLevelString(name: String, level: String) -> NSMutableAttributedString {
        
        let attributedString = NSMutableAttributedString(string: "\(name) \(level)", attributes: [
            .font: UIFont(name: AppConstants.Font.primaryRegular, size: 14.0)!,
            .foregroundColor: UIColor.white.withAlphaComponent(0.75) ,
            .kern: -0.09
            ])
        attributedString.addAttributes([
            .font: UIFont(name: AppConstants.Font.primaryRegular, size: 14.0)!,
            .foregroundColor: UIColor.white
            ], range: NSRange(location: 0, length: name.count))
        
        return attributedString
    }
    
    
}
