//
//  BannerEmptyState.swift
//  Markk
//
//  Created by Pushkar Deshmukh on 30/07/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import UIKit

class BannerEmptyState: UITableViewCell {
    
    @IBOutlet weak var cardView: UIView!
    
    var isLoading: Bool = false {
        didSet {
            updateLoadingState()
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setupUI()
    }

    // MARK: Private functions
    
    fileprivate func setupUI() {
        contentView.backgroundColor = AppConstants.SminqColors.blackDark100
        
        cardView.backgroundColor = AppConstants.SminqColors.blackLight100
        cardView.layer.cornerRadius = 8
        cardView.layer.borderColor = AppConstants.SminqColors.white100.withAlphaComponent(0.1).cgColor
        cardView.layer.borderWidth = 1
        cardView.clipsToBounds = true
        
    }
    
    fileprivate func updateLoadingState() {
        if isLoading {
            cardView.showAnimatedGradientAnimation()
        } else {
            cardView.hideAnimatedGratientAnimation()
        }
    }
    // MARK: Public functions
    
    public func setLoading(isLoading: Bool) {
        self.isLoading = isLoading
    }
}
