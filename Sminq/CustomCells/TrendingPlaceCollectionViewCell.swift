//
//  TrendingPlaceCollectionViewCell.swift
//  Markk
//
//  Created by Avinash Thakur on 24/07/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import UIKit

class TrendingPlaceCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var placeFeedImageView: UIImageView!
    @IBOutlet weak var placeNameLabel: UILabel!
    @IBOutlet weak var placeCategoryLabel: UILabel!

    @IBOutlet weak var placeDescButton: UIButton!
    
    
    var place: PlacesStoriesData? {
        didSet {
            updateDataOnCard()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }
    
    func setupUI() {
        placeNameLabel.textColor = AppConstants.SminqColors.white100
        placeNameLabel.font = UIFont(name: AppConstants.Font.primaryBold, size: 12)
        
        placeCategoryLabel.textColor = AppConstants.SminqColors.white100
        placeCategoryLabel.font = UIFont(name: AppConstants.Font.primaryRegular, size: 12)
        
        placeDescButton.setTitleColor(AppConstants.SminqColors.white100, for: .normal)
        placeDescButton.titleLabel?.font = UIFont(name: AppConstants.Font.primaryBold, size: 10)
        placeDescButton.setBackgroundColor(AppConstants.SminqColors.red100, forState: .normal)
        placeDescButton.layer.cornerRadius = 10
        
        self.contentView.backgroundColor = AppConstants.SminqColors.blackLight100
        self.contentView.layer.cornerRadius = 8.0
        self.contentView.layer.borderWidth = 1.0
        self.contentView.layer.borderColor = AppConstants.SminqColors.white100.withAlphaComponent(0.1).cgColor
        self.contentView.clipsToBounds = true
    }
    
    func setPlace(place: PlacesStoriesData) {
        self.place = place
    }
    
    func updateDataOnCard() {
        guard let placeStories = self.place, let place = placeStories.placeDetails else { return }
        
        if let subCategory = place.subCategory, let subCategoryName = subCategory.name {
            self.placeCategoryLabel.text = "\(subCategoryName)"
            
            if let distance = place.distance, distance > 0 {
                self.placeCategoryLabel.text = "\(subCategoryName) • \(SminqAPI.distanceFormated(rawDistanceDouble: round((distance))))"
            }
            
            self.placeCategoryLabel.isHidden = false
        }
        
        guard let userStory = placeStories.stories?.first else { return }
        
        guard let feed = userStory.posts.first else { return }
        
        self.placeNameLabel.text = place.name
        self.placeNameLabel.isHidden = false
        
        guard let otherContext = feed.getOtherContext() else {
            LogUtil.error("Could not find other context while showing story card!")
            return
        }
        
        if otherContext.video != "" {
            if let imageName = otherContext.image, let url = CloudinaryService.shared.generateCloudinaryVideoURL(folder: FIRRemoteConfigService.shared.getCloudinaryVideoFolder(), fileName: imageName) {
                self.placeFeedImageView.sd_setImage(with: url, placeholderImage: nil)
            }
        } else {
            if let imageName = otherContext.image, let url = CloudinaryService.shared.generateCloudinaryImageURL(folder: FIRRemoteConfigService.shared.getCloudinaryOriginalImageFolder(), fileName: imageName) {
                self.placeFeedImageView.sd_setImage(with: url, placeholderImage: nil)
            }
        }
    }
    
}
