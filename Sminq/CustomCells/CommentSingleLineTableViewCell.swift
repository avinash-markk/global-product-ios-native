//
//  CommentSingleLineTableViewCell.swift
//  Sminq
//
//  Created by Pushkar Deshmukh on 02/08/18.
//  Copyright © 2018 Suraj Sagare. All rights reserved.
//

import UIKit

class CommentSingleLineTableViewCell: UITableViewCell {

    @IBOutlet weak var commentLabel: UILabel!
    
    @IBOutlet weak var userInitialLabel: UILabel!
    
    @IBOutlet weak var userImageView: UIImageView!
    
    var reply: Replies?
    var comment: Comments?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.initUI()
    }
    
    // MARK: Private functions
    
    private func initUI() {
        self.commentLabel.font = UIFont(name: AppConstants.Font.primaryRegular, size: 12)
        self.commentLabel.textColor = AppConstants.SminqColors.white100.withAlphaComponent(0.75)
    }
    
    func setData() {
        self.commentLabel.text = reply?.input
        
        if let displayName = reply?.displayName {
            self.userInitialLabel.text = "\(displayName.prefix(1).uppercased())"
        }
        
        self.userImageView.sd_setImage(with: URL(string: (reply?.userImage)!), placeholderImage: nil)
    }

}
