//
//  StoryCollectionViewCell.swift
//  Sminq
//
//  Created by Pushkar Deshmukh on 17/05/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import UIKit
import AVFoundation

protocol StoryCollectionViewCellDelegate: NSObjectProtocol {
    func shouldGoToPreviousStory(currentCellIndex: Int)
    func shouldGoToNextStory(currentCellIndex: Int)
    func didCloseTap()
    func didSettingsTap(feedViewModel: FeedViewModel, post: UserDataPlaceFeed)
    func didShareTap(feedViewModel: FeedViewModel, post: UserDataPlaceFeed, place: LivePlaces)
    func didCommentTap(storyData: StoryData, feedViewModel: FeedViewModel, post: UserDataPlaceFeed, storyLatestCommentsView: StoryLatestCommentsView, storyBottomControlsView: StoryBottomControlsView)
    func didStoryRatingsFetchFail(error: ErrorObject)
    func didStoryRatingsFetch(storyData: StoryData, storyLatestCommentsView: StoryLatestCommentsView, storyBottomControlsView: StoryBottomControlsView)
    func didUserInitialLabelTap(post: UserDataPlaceFeed)
    func didUserImageViewTap(post: UserDataPlaceFeed)
    func didUserDisplayNameTap(post: UserDataPlaceFeed)
    func didPlaceNameLabelTap(storyData: StoryData)
    func didRatingImageLoadWithError(error: Error?)
    func didRatingVideoLoadWithError()
    func didImageLoad(ratingCellIndex: Int)
    func didVideoLoad(ratingCellIndex: Int)
    func didUpvotePost(post: UserDataPlaceFeed)
}

class StoryCollectionViewCell: UICollectionViewCell {
    public weak var delegate: StoryCollectionViewCellDelegate?
    
    fileprivate var storyData: StoryData? {
        didSet {
            guard let storyData = storyData else { return }
            storyRatingsCollectionView.setStoryData(storyData: storyData)
            storyPlaceHeaderView.setStory(storyData: storyData)
        }
    }
    
    fileprivate lazy var storyRatingsCollectionView: StoryRatingsCollectionView = {
        let view = StoryRatingsCollectionView.init(frame: .zero, storyViewManager: self.storyViewManager)
        view.delegate = self
        return view
    }()
    
    fileprivate lazy var storyPlaceHeaderView: StoryPlaceHeaderView = {
        let view = StoryPlaceHeaderView()
        view.delegate = self
        return view
    }()
    
    fileprivate lazy var placeMoodModalView: PlaceMoodModalView = {
        let view = Bundle.main.loadNibNamed("PlaceMoodModalView", owner: self, options: nil)?.first as! PlaceMoodModalView
        view.isHidden = true
        view.delegate = self
        return view
    }()
    
    fileprivate lazy var locationExplainerView: LocationVerificationModalView = {
        let view = Bundle.main.loadNibNamed("LocationVerificationModalView", owner: self, options: nil)?.first as! LocationVerificationModalView
        view.isHidden = true
        view.delegate = self
        return view
    }()
    
    fileprivate var storyProgressView: StoryProgressView!
    fileprivate var isMediaPlaybackLocked: Bool = false
    fileprivate var storyViewManager: StoryViewManager? {
        didSet {
            storyRatingsCollectionView.storyViewManager = storyViewManager
        }
    }
    
    public var cellIndex: Int = 0
    public var isCompletelyVisible: Bool = false
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        print("init called StoryCollectionViewCell")
        setupLayout()
    }
    
    // MARK: Fileprivate functions
    
    fileprivate func setupLayout() {
        addSubview(storyPlaceHeaderView)
        storyPlaceHeaderView.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor, padding: .zero, size: .init(width: 0, height: 64))
        
        addSubview(storyRatingsCollectionView)
        storyRatingsCollectionView.anchor(top: storyPlaceHeaderView.bottomAnchor, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor, padding: .init(top: -8, left: 0, bottom: 0, right: 0))
        
        addSubview(placeMoodModalView)
        placeMoodModalView.anchor(top: topAnchor, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor)
        placeMoodModalView.setMoodViewTopConstraint(top: 16)
        
        addSubview(locationExplainerView)
        locationExplainerView.anchor(top: topAnchor, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor)
        locationExplainerView.setLocationViewTopConstraint(top: 16)
    }
    
    fileprivate func setAlphaToUIViews(alpha: CGFloat) {
        if storyProgressView == nil { return }
        storyProgressView.alpha = alpha
    }
    
    fileprivate var isCellEndedDisplaying: Bool = false
    
    // MARK: Public functions
    public func setStory(story: StoryData, storyViewManager: StoryViewManager, isCellEndedDisplaying: Bool) {
        self.storyData = story
        self.storyViewManager = storyViewManager
        self.isCellEndedDisplaying = isCellEndedDisplaying
    }
    
    // NOTE: Set this flag whenever playback needs to be locked. If set to locked and navigated to comments screen, resume timers are prevented from calling
    
    public func setMediaPlaybackLock(lock: Bool) {
        self.isMediaPlaybackLocked = lock
    }
    
    
    public func fadeUI(show: Bool) {
        UIView.animate(withDuration: AppConstants.storyLongPressTimeout) {
            show ? self.setAlphaToUIViews(alpha: 1) : self.setAlphaToUIViews(alpha: 0)
        }
    }
    
    fileprivate var durationArray = [TimeInterval]()
    
    public func createProgressViews() {
        guard let storyData = storyData, let feedViewModel = storyData.feedViewModel, feedViewModel.getFeedCount() > 0 else { return }
        
        deleteProgressViews()
        
        storyProgressView = StoryProgressView(numberOfSegments: feedViewModel.getFeedCount(), durationArray: feedViewModel.getDurationArray())
        storyProgressView.delegate = self
        storyProgressView.topColor = UIColor.white
        storyProgressView.bottomColor = UIColor.white.withAlphaComponent(0.25)
        storyProgressView.padding = 2
        insertSubview(storyProgressView, belowSubview: placeMoodModalView)
        storyProgressView.frame = CGRect(x: 8, y: 64, width: width - 16, height: 4)
        storyProgressView.layoutProgressViews()
    }
    
    public func deleteProgressViews() {
        if storyProgressView != nil {
            storyProgressView.removeFromSuperview()
        }
    }
    
    public func stop() {
        storyRatingsCollectionView.stopMedia()
    }
    
    public func pause() {
        if storyProgressView == nil { return }
        storyProgressView.pause()
        storyRatingsCollectionView.pauseMedia()
    }
    
    public func resume() {
        guard let storyViewManager = storyViewManager else { return }
        
        if storyProgressView == nil || isMediaPlaybackLocked || !storyViewManager.isCompletelyVisible { return }
        
        if storyProgressView.getAnimateProgress(index: storyRatingsCollectionView.currentShowingIndex) <= 0 {
            storyProgressView.animate(animationIndex: storyRatingsCollectionView.currentShowingIndex)
        } else {
            storyProgressView.resume()
        }
                
        storyRatingsCollectionView.resumeMedia()
    }
    
    public func reStart() {
        if storyProgressView == nil || isMediaPlaybackLocked { return }
        storyProgressView.animate(animationIndex: storyRatingsCollectionView.currentShowingIndex)
        storyRatingsCollectionView.reStartMedia()
    }
    
    public func scrollToRating(index: Int, animate: Bool) {
        storyRatingsCollectionView.scrollToRating(index: index, animate: animate)
    }
    
    public func updateDeletedRatingViewState(post: UserDataPlaceFeed) {
        storyRatingsCollectionView.updateDeletedRatingViewState(post: post)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension StoryCollectionViewCell: StoryRatingsCollectionViewDelegate {
    func didUpvotePost(post: UserDataPlaceFeed) {
        delegate?.didUpvotePost(post: post)
    }
    
    func willDisplayRatingCell(cell: StoryRatingCollectionViewCell?) {
//        setMediaPlaybackLock(lock: false)
    }
    
    func didEndDisplayingRatingCell(endedCell: StoryRatingCollectionViewCell?, visibleCell: StoryRatingCollectionViewCell?) {
//        setMediaPlaybackLock(lock: false)
    }
    
    func didSettingsTap(feedViewModel: FeedViewModel, post: UserDataPlaceFeed) {
        delegate?.didSettingsTap(feedViewModel: feedViewModel, post: post)
    }
    
    func didShareTap(feedViewModel: FeedViewModel, post: UserDataPlaceFeed) {
        guard let storyData = storyData, let place = storyData.placeDetails else { return }
        delegate?.didShareTap(feedViewModel: feedViewModel, post: post, place: place)
    }
    
    func didUserPostMoodIconTap(post: UserDataPlaceFeed) {
        placeMoodModalView.isHidden = false
        placeMoodModalView.setPost(post: post)
        setMediaPlaybackLock(lock: true)
        pause()
    }
    
    func didMoodExplainerClose() {
        setMediaPlaybackLock(lock: false)
        resume()
    }
    
    func didPostLocationIconTap(post: UserDataPlaceFeed) {
        locationExplainerView.isHidden = false
        locationExplainerView.setPost(post: post)
        setMediaPlaybackLock(lock: true)
        pause()
    }
    
    func didCollectionViewLongPress(gesture: UILongPressGestureRecognizer) {
        switch gesture.state {
        case .began:
            setMediaPlaybackLock(lock: true)
            pause()
            fadeUI(show: false)
        case .ended:
            setMediaPlaybackLock(lock: false)
            resume()
            fadeUI(show: true)
        default: break
        }
    }
    
    func shouldGoToPreviousStory() {
        delegate?.shouldGoToPreviousStory(currentCellIndex: cellIndex)
    }
    
    func shouldGoToNextStory() {
        delegate?.shouldGoToNextStory(currentCellIndex: cellIndex)
    }
    
    
    
    func didRatingImageLoad(error: Error?, ratingCellIndex: Int) {
        if let error = error {
            delegate?.didRatingImageLoadWithError(error: error)
            return
        }
        
        if storyProgressView == nil || (ratingCellIndex != storyRatingsCollectionView.currentShowingIndex) {
            // If progress bar is empty OR
            // If image loaded cell index is not equal to the current showing index!
            return
        }

        resume()
        delegate?.didImageLoad(ratingCellIndex: ratingCellIndex)
    }
    
    func didVideoLoad(playerStatus: PlayerStatus, ratingCellIndex: Int, playerItem: AVPlayerItem, post: UserDataPlaceFeed, player: AVPlayer) {
        guard let storyData = storyData, let feedViewModel = storyData.feedViewModel else { return }
        guard let currentPost = feedViewModel.getFeedAt(index: storyRatingsCollectionView.currentShowingIndex) else { return }
        
        if (currentPost._id != post._id) || !isCellEndedDisplaying {
            // If loaded post and current viewing post is not same then pause a video
            // If cell is not completly displayed then pause a video
//            pause()
            return
        }
        
        if storyProgressView == nil  {
            // If progress bar is empty OR
            // If video loaded cell index is not equal to the current showing index!
//            pause()
            return
        }
        
        
//        switch player.timeControlStatus {
//
//        case .paused:
//            break
//        case .waitingToPlayAtSpecifiedRate:
//            break
//        case .playing:
//            resume()
//        }
        switch playerItem.status {

        case .unknown: break;
            // DON"T pause timer here. Because if even playerItem is loaded, player still remains in the unknown state
            // And blocks player forever
//            print("Pausing player")
//            pause()
        case .readyToPlay:
//            print("resuming player")
//            print("current post id : ", currentPost._id)
//            print("Loaded post id : ", post._id)
            if player.ready {
                resume()
                delegate?.didVideoLoad(ratingCellIndex: ratingCellIndex)
            }
            
        case .failed: delegate?.didRatingVideoLoadWithError()
        }
        
    }
    
    func didStoryRatingIndexChange(updatedIndex: Int) {
        if storyProgressView == nil { return }
        storyProgressView.fillTill(index: updatedIndex)
    }
    
    func didCommentTap(feedViewModel: FeedViewModel, post: UserDataPlaceFeed, storyLatestCommentsView: StoryLatestCommentsView, storyBottomControlsView: StoryBottomControlsView) {
        guard let storyData = storyData else { return }
        delegate?.didCommentTap(storyData: storyData, feedViewModel: feedViewModel, post: post, storyLatestCommentsView: storyLatestCommentsView, storyBottomControlsView: storyBottomControlsView)
        setMediaPlaybackLock(lock: true)
        pause()
    }
    
    func didCloseStoryCommentsView() {
        resume()
    }
    
    func didStoryRatingsFetch(newStoryData: StoryData, storyLatestCommentsView: StoryLatestCommentsView, storyBottomControlsView: StoryBottomControlsView) {
        self.storyData = newStoryData
        createProgressViews()
        delegate?.didStoryRatingsFetch(storyData: newStoryData, storyLatestCommentsView: storyLatestCommentsView, storyBottomControlsView: storyBottomControlsView)
        
    }
    
    func didStoryRatingsFetchFail(error: ErrorObject) {
        delegate?.didStoryRatingsFetchFail(error: error)
    }
    
    func didUserInitialLabelTap(post: UserDataPlaceFeed) {
        delegate?.didUserInitialLabelTap(post: post)
    }
    
    func didUserImageViewTap(post: UserDataPlaceFeed) {
        delegate?.didUserImageViewTap(post: post)
    }
    
    func didUserDisplayNameTap(post: UserDataPlaceFeed) {
        delegate?.didUserDisplayNameTap(post: post)
    }
    
}

extension StoryCollectionViewCell: StoryProgressViewDelegate {
    func storyProgressBarChangedIndex(index: Int) {
//        print("storyProgressBarChangedIndex Now showing index ,", index)
    }
    
    func storyProgressBarFinished() {
//        print("storyProgressBarFinished")
    }
    
    func storyProgressBarFinishedAt(index: Int) {
//        print("storyProgressBarFinishedAt: ", index)
        storyRatingsCollectionView.moveToNextOrPrevRating(direction: .forward, currentIndex: index)
    }
}

extension StoryCollectionViewCell: StoryPlaceHeaderViewDelegate {
    func didCloseTap() {
        delegate?.didCloseTap()
    }
    
    func didPlaceMoodImageTap() {
        guard let storyData = storyData, let place = storyData.placeDetails else { return }
        
        placeMoodModalView.isHidden = false
        placeMoodModalView.setPlace(place: place)
        setMediaPlaybackLock(lock: true)
        pause()
    }
    
    func didPlaceNameLabelTap(storyData: StoryData) {
        setMediaPlaybackLock(lock: true)
        pause()
        
        delegate?.didPlaceNameLabelTap(storyData: storyData)
    }
   
}

extension StoryCollectionViewCell: PlaceMoodModalViewDelegate {
    func didCloseButtonTap() {
        setMediaPlaybackLock(lock: false)
        resume()
    }
}

extension StoryCollectionViewCell: LocationVerificationModalViewDelegate {
    func didLocationVerificationModalCloseButtonTap() {
        setMediaPlaybackLock(lock: false)
        resume()
    }
}

