//
//  CountryCodeEditProfileTableViewCell.swift
//  Sminq
//
//  Created by Pushkar Deshmukh on 25/06/18.
//  Copyright © 2018 Suraj Sagare. All rights reserved.
//

import UIKit

class CountryCodeEditProfileTableViewCell: UITableViewCell {

    @IBOutlet weak var rowLabel: UILabel!
    
    @IBOutlet weak var rowInput: UITextField!
    
    @IBOutlet weak var countryCodeDropdownButton: UIButton!
    
    @IBOutlet weak var countryCodeLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.initUI()
    }
    
    // MARK: Private functions
    
    private func initUI() {
        self.contentView.backgroundColor = AppConstants.SminqColors.blackDark100
        
        self.rowLabel.font = UIFont(name: AppConstants.Font.primaryRegular, size: 12.0)
        self.rowLabel.textColor = AppConstants.SminqColors.white100.withAlphaComponent(0.75)
        
        self.rowInput.font = UIFont(name: AppConstants.Font.primaryRegular, size: 16.0)
        self.rowInput.textColor = AppConstants.SminqColors.white100
        
        self.countryCodeDropdownButton.isUserInteractionEnabled = true
        self.countryCodeLabel.isUserInteractionEnabled = true
        
        self.countryCodeLabel.font = UIFont(name: AppConstants.Font.primaryRegular, size: 16.0)
        self.countryCodeLabel.textColor = AppConstants.SminqColors.white100
        
//        let attributes = [
//            NSAttributedStringKey.foregroundColor: AppConstants.SminqColors.white100.withAlphaComponent(0.5),
//            NSAttributedStringKey.font : UIFont(name: AppConstants.Font.primaryRegular, size: 16)!
//        ]
//        
//        self.rowInput.attributedPlaceholder = NSAttributedString(string: self.rowInput.placeholder ?? "", attributes:attributes)
        
    }


}
