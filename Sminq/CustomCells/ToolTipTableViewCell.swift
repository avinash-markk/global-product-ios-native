//
//  ToolTipTableViewCell.swift
//  Sminq
//
//  Created by Pushkar Deshmukh on 04/10/18.
//  Copyright © 2018 Suraj Sagare. All rights reserved.
//

import UIKit

class ToolTipTableViewCell: UITableViewCell {
    
    @IBOutlet weak var userDisplayName: UILabel!
    @IBOutlet weak var userInitialLabel: UILabel!
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var rightIconImageView: UIImageView!
    
    var userTag: Tags?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        rightIconImageView.tintImageColor(color: UIColor.white)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setData() {
        if let displayName = userTag?.value {
            self.userDisplayName.text = "@\(displayName)"
            self.userInitialLabel.text = "\(displayName.prefix(1).uppercased())"
        }
        
        if let url = userTag?.imageUrl, url != "" {
            self.userImageView.sd_setImage(with: URL(string: url), placeholderImage: nil)
        }
        
    }
    
}
