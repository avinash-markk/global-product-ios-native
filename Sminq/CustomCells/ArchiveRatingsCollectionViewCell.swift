//
//  ArchiveRatingCollectionViewCell.swift
//  Markk
//
//  Created by Avinash Thakur on 18/06/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import UIKit

class ArchiveRatingsCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var ratingsImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.initUI()
    }
    
    func initUI() {
       self.backgroundColor = AppConstants.SminqColors.blackDark100
    }
    
}
