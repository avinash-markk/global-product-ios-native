//
//  SubscribeTableViewCell.swift
//  Markk
//
//  Created by Pushkar Deshmukh on 18/07/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import UIKit

protocol SubscribeTableViewCellDelegate: NSObjectProtocol {
    func didBannerImageViewTap()
}

class SubscribeTableViewCell: UITableViewCell {
    public weak var delegate: SubscribeTableViewCellDelegate?
    
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var cardViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var offlineIconImageView: UIImageView!
    @IBOutlet weak var bannerImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        setupUI()
    }
    
    // MARK: Fileprivate functions
    
    fileprivate func setupUI() {
        contentView.backgroundColor = AppConstants.SminqColors.blackDark100
        
        cardView.layer.borderWidth = 1
        cardView.layer.cornerRadius = 8
        cardView.layer.borderColor = UIColor.white.withAlphaComponent(0.1).cgColor
        cardView.backgroundColor = AppConstants.SminqColors.blackLight100
        
        bannerImageView.isUserInteractionEnabled = true
        bannerImageView.clipsToBounds = true
        bannerImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleBannerImageViewTap)))
    }

    // MARK: Public functions
    
    public func updateCardHeightConstraint(constant: CGFloat) {
        cardViewHeightConstraint.constant = constant
    }
    
    public func updateData() {
        offlineIconImageView.isHidden = true
        
        if let url = URL(string: FIRRemoteConfigService.shared.getSubScrinbeBannerImage()) {
            bannerImageView.sd_setImage(with: url) { (image, error, cacheType, _) in
                if let _ = error {
                    self.offlineIconImageView.isHidden = false
                    return
                }
            }
        }
    }
    
    // MARK: @objc functions
    
    @objc fileprivate func handleBannerImageViewTap() {
        delegate?.didBannerImageViewTap()
    }
   
    
}
