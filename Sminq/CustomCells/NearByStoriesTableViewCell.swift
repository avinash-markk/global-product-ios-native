//
//  NearByStoriesTableViewCell.swift
//  Sminq
//
//  Created by Pushkar Deshmukh on 06/03/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import UIKit

protocol NearByStoriesTableViewCellDelegate: NSObjectProtocol {
    func nearByPlaceDidSelect(placeStory: PlacesStoriesData)
}

class NearByStoriesTableViewCell: UITableViewCell {
    public weak var delegate: NearByStoriesTableViewCellDelegate?
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var collectionViewFlowLayout: UICollectionViewFlowLayout!
    
    var cardHeight: CGFloat = 298
    var spacing: CGFloat = 16
    private(set) var placeStories = [PlacesStoriesData]()
    private(set) var loadingNearBy = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.initUI()
        self.initDelegates()
    }
    
    // MARK: Private functions
    private func initUI() {
        self.contentView.backgroundColor = AppConstants.SminqColors.blackDark100
        
        self.collectionView.register(UINib(nibName: "PlaceStoryVariant1CollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "PlaceStoryVariant1CollectionViewCell")
        
        self.collectionViewFlowLayout.scrollDirection = .vertical
        self.collectionViewFlowLayout.sectionInset = UIEdgeInsets(top: spacing / 2, left: spacing, bottom: spacing, right: spacing)
        self.collectionViewFlowLayout.minimumLineSpacing = spacing / 2
        
    }
    
    private func initDelegates() {
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
    }
    
    // MARK: Public functions
    func setStories(stories: [PlacesStoriesData]) {
        self.placeStories = stories
    }
    
    func loadingState(state: Bool) {
        self.loadingNearBy = state
        self.collectionView.reloadData()
        self.collectionView.collectionViewLayout.invalidateLayout()
    }
    
}

extension NearByStoriesTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if self.loadingNearBy {
            return 2
        } else {
            return self.placeStories.count
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PlaceStoryVariant1CollectionViewCell", for: indexPath) as? PlaceStoryVariant1CollectionViewCell else { return UICollectionViewCell() }
        
        if self.loadingNearBy {
            cell.showSkeleton()
        } else {
            cell.hideSkeleton()
            cell.setPlace(place: self.placeStories[indexPath.row])
            cell.updateDataOnCard()
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var itemsPerRow: CGFloat = 2
        
        if self.placeStories.count == 1 {
            itemsPerRow = 1
        }
        
        let spacingBetweenCells: CGFloat = 8
        
        let totalSpacing = (2 * self.spacing) + ((itemsPerRow - 1) * spacingBetweenCells) // Amount of total spacing in a row
        
        let width = (collectionView.bounds.width - totalSpacing) / itemsPerRow
        return CGSize(width: width, height: cardHeight)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard !self.loadingNearBy else { return }
        self.delegate?.nearByPlaceDidSelect(placeStory: self.placeStories[indexPath.row])
    }
}
