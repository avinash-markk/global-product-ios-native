//
//  ProfileRatingsTableViewCell.swift
//  Sminq
//
//  Created by Avinash Thakur on 05/03/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import UIKit

protocol ProfileRatingsTableViewCellDelegate: NSObjectProtocol {
    func liveRatingsTapped(ratings: StoryData)
}

class ProfileRatingsTableViewCell: UITableViewCell {
    @IBOutlet weak var ratingsCollectionView: UICollectionView!
    weak var delegate: ProfileRatingsTableViewCellDelegate?
    var liveRatings: [StoryData]?
    var numberOfCellsPerRow = 2
    
    @IBOutlet weak var ratingsCollectionViewFlowLayout: UICollectionViewFlowLayout!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        self.contentView.backgroundColor = AppConstants.SminqColors.blackDark100
        self.initializeRatingsCollectionView()
    }
    
    func initializeRatingsCollectionView() {
        ratingsCollectionView.delegate = self
        ratingsCollectionView.dataSource = self
        ratingsCollectionView.backgroundColor = AppConstants.SminqColors.blackDark100
        self.ratingsCollectionView.register(UINib(nibName: "PlaceStoryCardCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "PlaceStoryCardCollectionViewCell")
        self.ratingsCollectionViewFlowLayout.scrollDirection = .vertical
        self.ratingsCollectionViewFlowLayout.minimumInteritemSpacing = 0
        self.ratingsCollectionViewFlowLayout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0)
    }
    
    func reloadRatingsView() {
        self.ratingsCollectionView.reloadData()
    }
    
    func updateUsersLiveRatings(data: [StoryData]) {
        self.liveRatings = data
        self.reloadRatingsView()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

extension ProfileRatingsTableViewCell: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            return  self.liveRatings?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PlaceStoryCardCollectionViewCell", for: indexPath) as? PlaceStoryCardCollectionViewCell {
            if let story = self.liveRatings?[indexPath.row] {
                cell.setStory(story: story)
                cell.updateDataOnCard()
            }
            return cell
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let totalWidth = collectionView.bounds.size.width
        let dimension = CGFloat(Int(totalWidth) / numberOfCellsPerRow)
        return CGSize.init(width: dimension, height: CGFloat(298))
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let liveRating = self.liveRatings?[indexPath.row] {
            self.delegate?.liveRatingsTapped(ratings: liveRating)
        }
    }
    
}
