//
//  FollowingTableViewCell.swift
//  Sminq
//
//  Created by Bjorn Mascarenhas on 01/10/18.
//  Copyright © 2018 Suraj Sagare. All rights reserved.
//

import UIKit

protocol FollowingTableViewCellDelegate: NSObjectProtocol {
    func followingButtonTapped(sender: UITableViewCell)
}

class FollowingTableViewCell: UITableViewCell {

    @IBOutlet weak var followingLabel: UILabel!
    @IBOutlet weak var followingSubLabel: UILabel!
    @IBOutlet weak var followingUserIcon: UIImageView!
    @IBOutlet weak var followingButton: UIButton!
    @IBOutlet weak var followingInitialLabel: UILabel!
    
   @IBOutlet weak var followBtnWidthConstraint: NSLayoutConstraint!
    weak var delegate: FollowingTableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        followBtnWidthConstraint.constant = 93.0
    }

    @IBAction func followingButtonTapped(sender: UIButton) {
     self.delegate?.followingButtonTapped(sender: self)
    }
    
    func updateFollowingCellUI(userData: UserDataDict!) {
        if let userID: String = userData._id {
            if Helper().getUserId() == userID {
                followingButton.isHidden = true
            } else {
                followingButton.isHidden = false
            }
        }
       
        if let userNameStr: String = userData.name {
        followingLabel.text = userNameStr
        }
        
        if let displayNameArray: [String] = userData.displayName {
            if displayNameArray.count > 0 {
                followingSubLabel.text = "@\(displayNameArray[0])"
            } else {
                followingSubLabel.text = ""
            }
        }
        
        if let userImageUrl: String = userData.imageUrl {
            if userImageUrl == "null" || userImageUrl == "" {
                followingUserIcon.isHidden = true
                followingInitialLabel.isHidden = false
                followingInitialLabel.text =  "\(userData?.displayName[0].prefix(1).uppercased() ?? "")"
            } else {
                followingUserIcon.sd_setImage(with: URL(string: userImageUrl), placeholderImage: nil)
                followingInitialLabel.isHidden = true
                followingUserIcon.isHidden = false
            }
        } else {
         followingUserIcon.isHidden = true
         followingInitialLabel.isHidden = false
         followingInitialLabel.text = "\(userData?.displayName[0].prefix(1).uppercased() ?? "")"
        }
        
        if userData.userFollowing == true {
            followBtnWidthConstraint.constant = 93.0
            followingButton.backgroundColor = UIColor.init(red: 144/255, green: 126/255, blue: 127/255, alpha: 0.1)
            followingButton.setTitleColor(UIColor.init(red: 144/255, green: 126/255, blue: 127/255, alpha: 1.0), for: .normal)
            followingButton.setTitle("FOLLOWING", for: .normal)
        } else {
           followBtnWidthConstraint.constant = 82.0
           followingButton.backgroundColor = AppConstants.SminqColors.pinkishRed
           followingButton.setTitleColor(UIColor.white, for: .normal)
           followingButton.setTitle("FOLLOW", for: .normal)
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
