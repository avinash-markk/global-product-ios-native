//
//  ExpiredStoryTableViewCell.swift
//  Sminq
//
//  Created by Pushkar Deshmukh on 06/03/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import UIKit

protocol ExpiredStoryTableViewCellDelegate: NSObjectProtocol {
    func addALiveRatingButtonTapped()
    func expiredStoryStackImageViewDidTap()
}

class ExpiredStoryTableViewCell: UITableViewCell {
    public weak var delegate: ExpiredStoryTableViewCellDelegate?
    
    @IBOutlet weak var cardView: UIView!
    
    @IBOutlet weak var subHeadingLabel: UILabel!
    
    @IBOutlet weak var addRatingButton: UIButton!
    
    @IBOutlet weak var stackImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.initUI()
    }
    
    private func initUI() {
        self.contentView.backgroundColor = AppConstants.SminqColors.blackDark100
        
        self.cardView.clipsToBounds = true
        self.cardView.layer.cornerRadius = 8
        self.cardView.layer.borderWidth = 1
        self.cardView.borderColor = AppConstants.SminqColors.white100.withAlphaComponent(0.1)
        self.cardView.backgroundColor = AppConstants.SminqColors.blackLight100
    
        let stackImageViewTap = UITapGestureRecognizer(target: self, action: #selector(stackImageViewTapped(_:)))
        self.stackImageView.backgroundColor = AppConstants.SminqColors.white100.withAlphaComponent(0.25)
        self.stackImageView.clipsToBounds = true
        self.stackImageView.layer.cornerRadius = self.stackImageView.height / 2
        self.stackImageView.isUserInteractionEnabled = true
        self.stackImageView.addGestureRecognizer(stackImageViewTap)
        
        self.subHeadingLabel.text = UITextConstants.userAddLiveRating
        self.subHeadingLabel.font = UIFont(name: AppConstants.Font.primaryRegular, size: 16)
        self.subHeadingLabel.textColor = AppConstants.SminqColors.white100
        
        self.addRatingButton.titleLabel?.textColor = AppConstants.SminqColors.white100
        self.addRatingButton.titleLabel?.font = UIFont(name: AppConstants.Font.primaryBold, size: 14)
        self.addRatingButton.layer.cornerRadius = self.addRatingButton.height / 2
        self.addRatingButton.clipsToBounds = true
        self.addRatingButton.backgroundColor = UIColor.clear
        self.addRatingButton.borderColor = AppConstants.SminqColors.white100
        self.addRatingButton.borderWidth = 1
        
    }
    
    func imageVisibility(hide: Bool) {
        self.stackImageView.isHidden = hide
    }
    
    func subHeadingvisibility(hide: Bool) {
        self.subHeadingLabel.isHidden = hide
    }
    
    // MARK: Target functions
    @objc func stackImageViewTapped(_ sender: UITapGestureRecognizer) {
        self.delegate?.expiredStoryStackImageViewDidTap()
    }
    
    // MARK: Button actions
    
    @IBAction func addRatingButtonTapped(_ sender: UIButton) {
        self.delegate?.addALiveRatingButtonTapped()
    }
    
}
