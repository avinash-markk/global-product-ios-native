//
//  PlaceStoryCollectionViewCell.swift
//  Sminq
//
//  Created by Pushkar Deshmukh on 06/03/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import UIKit
import SDWebImage

protocol PlaceStoryCollectionViewCellDelegate: NSObjectProtocol {
    func didRateTap(place: LivePlaces)
}

class PlaceStoryCollectionViewCell: UICollectionViewCell {
    public weak var delegate: PlaceStoryCollectionViewCellDelegate?
    
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var overlayView: UIView!
    
    @IBOutlet weak var feedImageView: UIImageView!
    @IBOutlet weak var moodImageView: UIImageView!
    
    @IBOutlet weak var placeNameLabel: UILabel!
    
    @IBOutlet weak var postCountLabel: UILabel!
    
    @IBOutlet weak var rateButton: UIButton!
    
    var story: StoryData?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.initUI()
        
    }
    
    // MARK: Private functions
    func initUI() {
        self.cardView.clipsToBounds = true
        self.cardView.layer.cornerRadius = 8
        self.cardView.borderWidth = 1
        self.cardView.borderColor = AppConstants.SminqColors.white100.withAlphaComponent(0.1)
        
        self.placeNameLabel.font = UIFont(name: AppConstants.Font.primaryBold, size: 12)
        self.placeNameLabel.textColor = AppConstants.SminqColors.white100
        
        self.postCountLabel.font = UIFont(name: AppConstants.Font.primaryRegular, size: 12)
        self.postCountLabel.textColor = AppConstants.SminqColors.white100
        
        self.rateButton.titleLabel?.text = "RATE"
        self.rateButton.titleLabel?.font = UIFont(name: AppConstants.Font.primaryBold, size: 14)
        self.rateButton.titleLabel?.textColor = AppConstants.SminqColors.purple100.withAlphaComponent(0.75)
        self.rateButton.clipsToBounds = true
        self.rateButton.cornerRadius = self.rateButton.height / 2
    }
    
    // MARK: Public functions
    
    func setStory(story: StoryData) {
        self.story = story
    }
    
    func updateDataOnCard() {
        guard let story = self.story, let place = self.story?.placeDetails, let posts = self.story?.posts, let post = posts.last, let otherContext = post.getOtherContext() else {
            LogUtil.error("Could not render PlaceStoryCollectionViewCell cell because of missing data")
            return
        }
    
        self.placeNameLabel.text = place.name
        self.postCountLabel.text = "\(story.getPostCountsLabel() ?? "")"
        
        let moodScoreMeta = post.moodScore.getMoodMeta()
        self.moodImageView.image = moodScoreMeta.image
        
        if otherContext.video != "" {
            if let imageName = otherContext.image, let url = CloudinaryService.shared.generateCloudinaryVideoURL(folder: FIRRemoteConfigService.shared.getCloudinaryVideoFolder(), fileName: imageName) {
                self.feedImageView.sd_setImage(with: url, placeholderImage: nil)
            }
        } else {
            if let imageName = otherContext.image, let url = CloudinaryService.shared.generateCloudinaryImageURL(folder: FIRRemoteConfigService.shared.getCloudinaryOriginalImageFolder(), fileName: imageName) {
                self.feedImageView.sd_setImage(with: url, placeholderImage: nil)
            }
        }
    }
    
    // MARK: Button actions
    
    @IBAction func rateButtonTapped(_ sender: UIButton) {
        guard let story = self.story, let place = story.placeDetails else { return }
        
        self.delegate?.didRateTap(place: place)
    }
}
