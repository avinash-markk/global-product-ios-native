//
//  PlaceCardCollectionViewCell.swift
//  Sminq
//
//  Created by Pushkar Deshmukh on 01/08/18.
//  Copyright © 2018 Suraj Sagare. All rights reserved.
//

import UIKit

class PlaceCardCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var feedImageView: UIImageView!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var userInitialLabel: UILabel!
    @IBOutlet weak var feedCountLabel: UILabel!
    @IBOutlet weak var placeMoodImageIcon: UIImageView!
    
    @IBOutlet weak var cardView: UIView!
    
    @IBOutlet weak var overlayView: UIView!
    
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var commentButton: UIButton!
    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var postButton: UIButton!
    @IBOutlet weak var countsStackView: UIStackView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.initUI()
    }
    
    var spaceContext: Contributions?
   
    private(set) var userStory: UserStory?
    
    // MARK: Public functions
    
    func initUI() {
        self.clipsToBounds = true
        self.layer.cornerRadius = 8
        
        self.contentView.clipsToBounds = true
        self.contentView.layer.cornerRadius = 8
        
        self.cardView.clipsToBounds = true
        self.cardView.layer.cornerRadius = 8
        self.cardView.setNeedsLayout()
        
        self.overlayView.borderColor = AppConstants.SminqColors.white100.withAlphaComponent(0.1)
        self.overlayView.borderWidth = 1
        self.overlayView.backgroundColor = AppConstants.SminqColors.shadow.withAlphaComponent(0.5)
        self.overlayView.layer.cornerRadius = 8
        self.overlayView.clipsToBounds = true
        
        self.feedImageView.layer.cornerRadius = 8
        self.feedImageView.clipsToBounds = true
        
        self.userImageView.layer.cornerRadius = self.userImageView.height / 2.0
        self.userImageView.clipsToBounds = true
        self.userImageView.borderColor = AppConstants.SminqColors.white100.withAlphaComponent(0.1)
        self.userImageView.borderWidth = 1.0
        
        self.userInitialLabel.layer.cornerRadius = self.userInitialLabel.height / 2.0
        self.userInitialLabel.clipsToBounds = true
        self.userInitialLabel.borderColor = AppConstants.SminqColors.white100.withAlphaComponent(0.1)
        self.userInitialLabel.borderWidth = 1.0
        self.userInitialLabel.backgroundColor = AppConstants.SminqColors.purple100
        
        self.userInitialLabel.font = UIFont(name: AppConstants.Font.primaryBold, size: 10.0)
        self.userInitialLabel.textColor = AppConstants.SminqColors.white100.withAlphaComponent(0.75)
        
        likeButton.setTitleColor(AppConstants.SminqColors.white100.withAlphaComponent(0.75), for: .normal)
        likeButton.titleLabel?.font = UIFont(name: AppConstants.Font.primaryRegular, size: 12)
        likeButton.centerTextAndImage(spacing: 4)
        likeButton.titleLabel?.textDropShadow()
        
        commentButton.setTitleColor(AppConstants.SminqColors.white100.withAlphaComponent(0.75), for: .normal)
        commentButton.titleLabel?.font = UIFont(name: AppConstants.Font.primaryRegular, size: 12)
        commentButton.centerTextAndImage(spacing: 4)
        commentButton.titleLabel?.textDropShadow()
        
        shareButton.setTitleColor(AppConstants.SminqColors.white100.withAlphaComponent(0.75), for: .normal)
        shareButton.titleLabel?.font = UIFont(name: AppConstants.Font.primaryRegular, size: 12)
        shareButton.centerTextAndImage(spacing: 4)
        shareButton.titleLabel?.textDropShadow()
        
        postButton.setTitleColor(AppConstants.SminqColors.white100, for: .normal)
        postButton.titleLabel?.font = UIFont(name: AppConstants.Font.primaryRegular, size: 12)
        postButton.centerTextAndImage(spacing: 4)
        postButton.titleLabel?.textDropShadow()
    }
    
    func showSkeleton() {
        self.cardView.showAnimatedGradientAnimation()
    }
    
    func hideSkeleton() {
        self.cardView.hideAnimatedGratientAnimation()
    }
    
    func setStory(story: UserStory) {
        self.userStory = story
    }
    
    func updateDataOnCard() {
        guard let posts = self.userStory?.posts, posts.count > 0, let feed = posts.last else {
            LogUtil.error("Post array is empty")
            return
        }
        
        guard let otherContext = feed.getOtherContext() else {
            LogUtil.error("Could not find other context while showing story card!")
            return
        }
        
        if let displayName =  feed.displayName {
            self.userInitialLabel.text = "\(displayName.uppercased().prefix(1))"
        }
        
        let moodScoreMeta = feed.moodScore.getMoodMeta()
        self.placeMoodImageIcon.image = moodScoreMeta.image
        
        if let imageUrl = URL(string: feed.user.imageUrl) {
            self.userImageView.sd_setImage(with: imageUrl, placeholderImage: nil)
        } 
        
        if otherContext.video != "" {
            if let imageName = otherContext.image, let url = CloudinaryService.shared.generateCloudinaryVideoURL(folder: FIRRemoteConfigService.shared.getCloudinaryVideoFolder(), fileName: imageName) {
                self.feedImageView.sd_setImage(with: url, placeholderImage: nil)
            }
        } else {
            if let imageName = otherContext.image, let url = CloudinaryService.shared.generateCloudinaryImageURL(folder: FIRRemoteConfigService.shared.getCloudinaryOriginalImageFolder(), fileName: imageName) {
                self.feedImageView.sd_setImage(with: url, placeholderImage: nil)
            }
        }
        
        postButton.updateCount(count: posts.count)
        likeButton.updateCount(count: feed.upVotesCount)
        commentButton.updateCount(count: feed.commentsCount)
        shareButton.updateCount(count: feed.sharesCount)
        
        countsStackView.isHidden = !feed.hasCounts()
    }
}
