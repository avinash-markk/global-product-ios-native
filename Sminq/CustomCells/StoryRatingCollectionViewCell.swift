//
//  StoryRatingCollectionViewCell.swift
//  Markk
//
//  Created by Pushkar Deshmukh on 29/05/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import UIKit
import AVFoundation

protocol StoryRatingCollectionViewCellDelegate: NSObjectProtocol {
    func didRatingImageLoad(error: Error?, ratingCellIndex: Int)
    func didVideoLoad(playerStatus: PlayerStatus, ratingCellIndex: Int, playerItem: AVPlayerItem, post: UserDataPlaceFeed, player: AVPlayer)
    func didCommentTap(feedViewModel: FeedViewModel, post: UserDataPlaceFeed, storyLatestCommentsView: StoryLatestCommentsView, storyBottomControlsView: StoryBottomControlsView)
    func didCloseStoryCommentsView()
    func didSettingsTap(feedViewModel: FeedViewModel, post: UserDataPlaceFeed)
    func didShareTap(feedViewModel: FeedViewModel, post: UserDataPlaceFeed)
    func didUserPostMoodIconTap(post: UserDataPlaceFeed)
    func didMoodExplainerClose()
    func didPostLocationIconTap(post: UserDataPlaceFeed)
    func didUserInitialLabelTap(post: UserDataPlaceFeed)
    func didUserImageViewTap(post: UserDataPlaceFeed)
    func didUserDisplayNameTap(post: UserDataPlaceFeed)
    func didUpvotePost(post: UserDataPlaceFeed)
}

class StoryRatingCollectionViewCell: UICollectionViewCell {
    public weak var delegate: StoryRatingCollectionViewCellDelegate?
    
    fileprivate var feedViewModel: FeedViewModel? {
        didSet {
            guard let feedViewModel = feedViewModel, let post = feedViewModel.getFeedAt(index: cellIndex) else { return }
            storyUserHeaderView.setPost(post: post)
            storyBottomControlsView.setPost(post: post)
            storyLatestCommentsView.setPost(post: post)
            updateDeletedRatingViewState(post: post)
            
            guard let cellState = cellState else { return }
//            if cellState == .didEndDisplaying {
                storyMediaView.setPost(post: post, ratingCellIndex: cellIndex)
//            }
            
        }
    }
    
    fileprivate lazy var storyMediaView: StoryMediaView = {
        let view = StoryMediaView()
        view.delegate = self
        return view
    }()
    
    fileprivate lazy var storyUserHeaderView: StoryUserHeaderView = {
        let view = StoryUserHeaderView()
        view.delegate = self
        return view
    }()
    
    fileprivate lazy var storyRatingExpiredStateView: StoryRatingExpiredStateView = {
        let view = StoryRatingExpiredStateView()
        view.isHidden = true
        return view
    }()
    
    fileprivate let topGradientView = CustomGradientView(colors: [AppConstants.SminqColors.shadow.withAlphaComponent(0.75).cgColor, AppConstants.SminqColors.shadow.withAlphaComponent(0).cgColor], locations: [0, 0.5])
    
    fileprivate let bottomGradientView = CustomGradientView(colors: [AppConstants.SminqColors.shadow.withAlphaComponent(0).cgColor, AppConstants.SminqColors.shadow.withAlphaComponent(0.75).cgColor], locations: [0, 0.5])
    
    fileprivate var storyViewManager: StoryViewManager?
    
    public lazy var storyBottomControlsView: StoryBottomControlsView = {
        let view = StoryBottomControlsView.init(frame: .zero, storyViewManager: storyViewManager)
        view.delegate = self
        return view
    }()
    
    public lazy var storyLatestCommentsView: StoryLatestCommentsView = {
        let view = StoryLatestCommentsView()
        view.delegate = self
        return view
    }()
    
    fileprivate var cellState: CellState?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupLayout()
    }
    
    public var cellIndex: Int = 0
    
    // MARK: Fileprivate functions
    
    fileprivate func setupLayout() {
        backgroundColor = UIColor.red.withAlphaComponent(0.1)
        
        addSubview(storyMediaView)
        storyMediaView.anchor(top: topAnchor, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor)
        
        setupGradientLayer()
        
        addSubview(storyUserHeaderView)
        storyUserHeaderView.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor, padding: .init(top: 23, left: 16, bottom: 0, right: 16), size: .init(width: 0, height: 34))
        
        addSubview(storyBottomControlsView)
        storyBottomControlsView.anchor(top: nil, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor)
        
        addSubview(storyLatestCommentsView)
        storyLatestCommentsView.anchor(top: nil, leading: leadingAnchor, bottom: storyBottomControlsView.topAnchor, trailing: trailingAnchor)
        
        
        addSubview(storyRatingExpiredStateView)
        storyRatingExpiredStateView.snp_makeConstraints { (make) in
            
            make.top.left.bottom.right.equalTo(self)
        }
        
    }
    
    fileprivate func setupGradientLayer() {
        addSubview(topGradientView)
        topGradientView.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor, padding: .init(top: 0, left: 0, bottom: 0, right: 0), size: .init(width: 0, height: 160))
        
        addSubview(bottomGradientView)
        bottomGradientView.anchor(top: nil, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor, padding: .zero, size: .init(width: 0, height: 160))
    }
    
    
    
    fileprivate func setAlphaToUIViews(alpha: CGFloat) {
        topGradientView.alpha = alpha
        bottomGradientView.alpha = alpha
        storyUserHeaderView.alpha = alpha
        storyLatestCommentsView.alpha = alpha
        storyBottomControlsView.alpha = alpha
    }
    
    // MARK: Public functions    
    public func fadeUI(show: Bool) {
        UIView.animate(withDuration: AppConstants.storyLongPressTimeout) {
            show ? self.setAlphaToUIViews(alpha: 1) : self.setAlphaToUIViews(alpha: 0)
        }
    }
    
    public func setFeedViewModel(feedViewModel: FeedViewModel, cellIndex: Int, storyViewManager: StoryViewManager?, cellState: CellState) {
        self.cellState = cellState
        
        self.storyViewManager = storyViewManager
        self.cellIndex = cellIndex
        self.feedViewModel = feedViewModel
        
    }
    
    public func prepareMedia() {
        storyMediaView.playMedia(prepareMedia: true)
    }
    
    public func playMedia() {
        storyMediaView.playMedia()
    }
    
    public func pauseMedia() {
        storyMediaView.pauseMedia()
    }
    
    public func resumeMedia() {
        storyMediaView.resumeMedia()
    }
    
    public func reStartMedia() {
        storyMediaView.reStartMedia()
    }
    
    public func stopMedia() {
        storyMediaView.stopMedia()
    }
    
    public func updateDeletedRatingViewState(post: UserDataPlaceFeed) {
        storyRatingExpiredStateView.setPost(post: post) // Set updated post so that icons are updated
        
        if post.isPostDeletedLocalFlag {
            storyRatingExpiredStateView.isHidden = false
            storyMediaView.isHidden = true
            storyMediaView.pauseMedia()
        } else if post.isPostReportedLocalFlag {
            storyRatingExpiredStateView.isHidden = false
            storyMediaView.isHidden = true
            storyMediaView.pauseMedia()
        } else if post.expired {
            storyRatingExpiredStateView.isHidden = false
            storyMediaView.isHidden = true
            storyMediaView.pauseMedia()
        } else {
            storyRatingExpiredStateView.isHidden = true
            storyMediaView.isHidden = false
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension StoryRatingCollectionViewCell: StoryBottomControlsViewDelegate {
    func didCommentTap(storyBottomControlsView: StoryBottomControlsView) {
        guard let feedViewModel = feedViewModel, let post = feedViewModel.getFeedAt(index: cellIndex) else { return }
        
        if let archived = post.archived, archived {
            storyRatingExpiredStateView.show(storyViewAction: .comment)
            return
        }
        
        delegate?.didCommentTap(feedViewModel: feedViewModel, post: post, storyLatestCommentsView: storyLatestCommentsView, storyBottomControlsView: storyBottomControlsView)
    }
    
    func didUpvoteTap(post: UserDataPlaceFeed) {
        guard let feedViewModel = feedViewModel else { return }
        
        if let archived = post.archived, archived {
            storyRatingExpiredStateView.show(storyViewAction: .like)
            return
        }
        
        feedViewModel.upvoteFeed(feed: post) { [weak self] (feedResponse, responseState, networkError, updatedFeed, upvoteError) in
            guard let this = self else { return }
            this.storyBottomControlsView.setPost(post: updatedFeed)
            this.delegate?.didUpvotePost(post: updatedFeed)
        }
    }
    
    func didSettingsTap(post: UserDataPlaceFeed) {
        guard let feedViewModel = feedViewModel else { return }
        delegate?.didSettingsTap(feedViewModel: feedViewModel, post: post)
    }
    
    func didShareTap(post: UserDataPlaceFeed) {
        guard let feedViewModel = feedViewModel else { return }
        
        if let archived = post.archived, archived {
            storyRatingExpiredStateView.show(storyViewAction: .share)
            return
        }
        
        delegate?.didShareTap(feedViewModel: feedViewModel, post: post)
        
        feedViewModel.shareFeed(feed: post) { [weak self] (shareFeedResponse, apiResponseState, networkError, updatedFeed, error) in
            guard let self = self else { return }
            self.storyBottomControlsView.setPost(post: updatedFeed)
        }
    }
}

extension StoryRatingCollectionViewCell: StoryMediaViewDelegate {
    func didVideoLoad(playerStatus: PlayerStatus, ratingCellIndex: Int, playerItem: AVPlayerItem, post: UserDataPlaceFeed, player: AVPlayer) {
        delegate?.didVideoLoad(playerStatus: playerStatus, ratingCellIndex: ratingCellIndex, playerItem: playerItem, post: post, player: player)
    }
    
    func didImageLoad(error: Error?, ratingCellIndex: Int) {
        delegate?.didRatingImageLoad(error: error, ratingCellIndex: ratingCellIndex)
    }
}

extension StoryRatingCollectionViewCell: StoryUserHeaderViewDelegate {
    func didPlaceMoodIconTap() {
        guard let feedViewModel = feedViewModel, let post = feedViewModel.getFeedAt(index: cellIndex) else { return }
        delegate?.didUserPostMoodIconTap(post: post)
    }
    
    func didLocationIconTap() {
        guard let feedViewModel = feedViewModel, let post = feedViewModel.getFeedAt(index: cellIndex) else { return }
        delegate?.didPostLocationIconTap(post: post)
    }
    
    func didUserInitialLabelTap(post: UserDataPlaceFeed) {
        delegate?.didUserInitialLabelTap(post: post)
    }
    
    func didUserImageViewTap(post: UserDataPlaceFeed) {
        delegate?.didUserImageViewTap(post: post)
    }
    
    func didUserDisplayNameTap(post: UserDataPlaceFeed) {
        delegate?.didUserDisplayNameTap(post: post)
    }
}

extension StoryRatingCollectionViewCell: StoryLatestCommentsViewDelegate {
    func didCommentSelect(comment: Replies) {
        guard let feedViewModel = feedViewModel, let post = feedViewModel.getFeedAt(index: cellIndex) else { return }

        delegate?.didCommentTap(feedViewModel: feedViewModel, post: post, storyLatestCommentsView: storyLatestCommentsView, storyBottomControlsView: storyBottomControlsView)
    }
}
