//
//  PlaceHoursTableViewCell.swift
//  Sminq
//
//  Created by Pushkar Deshmukh on 04/03/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import UIKit

class PlaceHoursTableViewCell: UITableViewCell {

    @IBOutlet weak var rowHeadingLabel: UILabel!
    @IBOutlet weak var hoursRowValueLabel: UILabel!
    
    var hours: [String] = [] {
        willSet {
            self.hoursRowValueLabel.text = "\(self.getHoursLabel(hours: newValue))"
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.initUI()
    }

    // MARK: Private functions
    
    private func initUI() {
        self.contentView.backgroundColor = AppConstants.SminqColors.blackLight100
        
        self.rowHeadingLabel.font = UIFont(name: AppConstants.Font.primaryBold, size: 14)
        self.rowHeadingLabel.textColor = AppConstants.SminqColors.white100.withAlphaComponent(0.75)
        
        self.hoursRowValueLabel.font = UIFont(name: AppConstants.Font.primaryRegular, size: 14)
        self.hoursRowValueLabel.textColor = AppConstants.SminqColors.white100.withAlphaComponent(0.75)
        
        
    }
    
    func getHoursLabel(hours: [String]) -> String {
        var tempString = ""
        
        for (index, hour) in hours.enumerated() {
            if index == hours.count - 1 {
                tempString = tempString + hour
            } else {
                tempString = tempString + hour + "\n"
            }
        }
        
        return tempString
    }
    
}
