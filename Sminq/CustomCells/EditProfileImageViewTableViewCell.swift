//
//  EditProfileImageViewTableViewCell.swift
//  Sminq
//
//  Created by Pushkar Deshmukh on 04/09/18.
//  Copyright © 2018 Suraj Sagare. All rights reserved.
//

import UIKit

class EditProfileImageViewTableViewCell: UITableViewCell {

    @IBOutlet weak var outerCircleUIView: UIView!
    
    @IBOutlet weak var userImageView: UIImageView!
    
    @IBOutlet weak var addUserImageView: UIImageView!
    @IBOutlet weak var overlayView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.initUI()
    }

    // MARK: Private functions
    
    private func initUI() {
        self.contentView.backgroundColor = AppConstants.SminqColors.blackDark100
        
        self.outerCircleUIView.clipsToBounds = true
        self.outerCircleUIView.layer.cornerRadius = self.outerCircleUIView.height / 2
        self.outerCircleUIView.borderColor = AppConstants.SminqColors.white100.withAlphaComponent(0.1)
        self.outerCircleUIView.borderWidth = 1
        
        self.overlayView.clipsToBounds = true
        self.overlayView.backgroundColor = AppConstants.SminqColors.purple100.withAlphaComponent(0.5)
    }

}
