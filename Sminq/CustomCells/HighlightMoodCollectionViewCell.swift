//
//  HighlightMoodCollectionViewCell.swift
//  Markk
//
//  Created by Pushkar Deshmukh on 22/07/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import UIKit

class HighlightMoodCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var headingLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var imageViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var imageViewWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var imageViewTopConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var subHeadingLabel: UILabel!
    
    var banner: Banner? {
        didSet {
            updateData()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        setupUI()
    }
    
    // MARK: Fileprivate functions
    
    fileprivate func setupUI() {
        cardView.backgroundColor = AppConstants.SminqColors.blackLight100
        cardView.clipsToBounds = true
        cardView.layer.cornerRadius = 8
        cardView.borderColor = AppConstants.SminqColors.white100.withAlphaComponent(0.1)
        cardView.borderWidth = 1
        
        headingLabel.font = UIFont.init(name: AppConstants.Font.primaryRegular, size: 48)
        headingLabel.textColor = AppConstants.SminqColors.white100
        headingLabel.text = "25"
        headingLabel.textAlignment = .center
        headingLabel.numberOfLines = 1
        
        subHeadingLabel.font = UIFont.init(name: AppConstants.Font.primaryBold, size: 10)
        subHeadingLabel.textColor = AppConstants.SminqColors.white100.withAlphaComponent(0.75)
        subHeadingLabel.text = "PLACES \n RATED DOPE".uppercased()
        subHeadingLabel.textAlignment = .center
        subHeadingLabel.numberOfLines = 0
        
    }
    
    fileprivate func updateData() {
        guard let banner = banner else { return }
        
        headingLabel.text = "\(banner.threshold)"
        subHeadingLabel.text = banner.label
        
        if let bannerType = banner.type {
            switch bannerType {
            case .totalDope:
                let moodMeta = 3.getMoodMeta()
                imageView.image = moodMeta.imageWithBottomText
                setImageViewConstraints(height: 96, width: 96, top: 26)
            case .maxRatings:
                imageView.image = UIImage.init(named: "iconLogo56")
                setImageViewConstraints(height: 56, width: 56, top: 46)
            case .maxLikes:
                imageView.image = UIImage.init(named: "iconHeartFull56Red100")
                setImageViewConstraints(height: 56, width: 56, top: 46)
            case .maxViews:
                imageView.image = UIImage.init(named: "iconView56Green100")
                setImageViewConstraints(height: 56, width: 56, top: 46)
            case .totalStickers:
                if let url = URL(string: banner.icon) {
                    imageView.sd_setImage(with: url)
                    setImageViewConstraints(height: 96, width: 96, top: 26)
                }
            default: break
            }
        }
    }
    
    fileprivate func setImageViewConstraints(height: CGFloat, width: CGFloat, top: CGFloat) {
        imageViewHeightConstraint.constant = height
        imageViewWidthConstraint.constant = width
        imageViewTopConstraint.constant = top
    }
    
    // MARK: Public functions
    
    public func setBanner(banner: Banner) {
        self.banner = banner
    }

}
