//
//  CommonHeaderView.swift
//  Sminq
//
//  Created by Avinash Thakur on 04/03/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import UIKit

protocol CommonHeaderViewDelegate: NSObjectProtocol {
    func didTapOnNotificationIcon()
}

class CommonHeaderView: UIView {
    public weak var delegate: CommonHeaderViewDelegate?
    
    var contentView: UIView!
    
    @IBOutlet weak var parentView: UIView!
    
    var isBlur: Bool = false
    
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var notificationButton: UIButton!
    
    init(frame: CGRect, isBlur: Bool?) {
        super.init(frame: frame)
        self.isBlur = isBlur ?? false
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        contentView.frame = self.frame
    }
    
    func xibSetup() {
        backgroundColor = UIColor.clear
        contentView = loadNib()
        parentView.backgroundColor = .clear
        contentView.frame = bounds
        if self.isBlur {
            let overlay = UIView.init(frame: self.frame)
            overlay.backgroundColor = AppConstants.SminqColors.blackDark100.withAlphaComponent(0)
            contentView.addSubview(overlay)
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(updateNotificationCount), name: NSNotification.Name(rawValue: AppConstants.AppNotificationNames.updateNotification), object: nil)
      
        headerLabel.textColor = AppConstants.SminqColors.white100
        headerLabel.font = UIFont(name: AppConstants.Font.primaryRegular, size: 16)
        headerLabel.clipsToBounds = true
        
        notificationButton.clipsToBounds = true
        notificationButton.backgroundColor = AppConstants.SminqColors.blackLight100
        notificationButton.borderColor = AppConstants.SminqColors.white100.withAlphaComponent(0.1)
        notificationButton.borderWidth = 1
        notificationButton.cornerRadius = 20
        notificationButton.titleLabel?.text = ""
        notificationButton.titleLabel?.font = UIFont(name: AppConstants.Font.primaryBold, size: 14)
        notificationButton.titleLabel?.textColor = AppConstants.SminqColors.white100
        notificationButtonVisibility(hide: true)
        
        addSubview(contentView)
    }
    
    func loadingState(loading: Bool) {
        if loading {
            headerLabel.showAnimatedGradientAnimation()
            headerLabel.layer.cornerRadius = headerLabel.height / 2
        } else {
            headerLabel.hideAnimatedGratientAnimation()
            headerLabel.layer.cornerRadius = 0
        }
    }
    
    func notificationButtonVisibility(hide: Bool) {
        self.notificationButton.isHidden = hide
    }
    
    // MARK: Target methods
    
    @objc func updateNotificationCount () {
        let unreadCount = UserDefaults.standard.integer(forKey: "notificationUnreadCount")
        
        if unreadCount > 0 {
            self.notificationButton.setTitle("\(unreadCount)", for: .normal)
            self.notificationButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: -8, bottom: 0, right: 0)
            self.notificationButton.contentEdgeInsets = UIEdgeInsets(top: 0, left: 14, bottom: 0, right: 14)
            self.notificationButton.setImage(UIImage(named: "iconNotification24White100"), for: .normal)
            self.notificationButton.backgroundColor = AppConstants.SminqColors.red100
        } else {
            self.notificationButton.setTitle("", for: .normal)
            self.notificationButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            self.notificationButton.contentEdgeInsets = UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 8)
            self.notificationButton.setImage(UIImage(named: "iconNotification24White50"), for: .normal)
            self.notificationButton.backgroundColor = AppConstants.SminqColors.blackLight100
        }
    }
    
    // MARK: Button actions
    
    @IBAction func notificationButtonTapped(_ sender: UIButton) {
        self.delegate?.didTapOnNotificationIcon()
    }
}
