//
//  EditProfileSaveButtonTableViewCell.swift
//  Sminq
//
//  Created by Pushkar Deshmukh on 04/09/18.
//  Copyright © 2018 Suraj Sagare. All rights reserved.
//

import UIKit

class EditProfileSaveButtonTableViewCell: UITableViewCell {

    @IBOutlet weak var saveButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.initUI()
    }

    // MARK: Private functions

    private func initUI() {
        self.contentView.backgroundColor = AppConstants.SminqColors.blackDark100
        
        self.saveButton.setTitleColor(AppConstants.SminqColors.white100, for: .normal)
        self.saveButton.backgroundColor = AppConstants.SminqColors.green200
        self.saveButton.titleLabel?.font = UIFont(name: AppConstants.Font.primaryBold, size: 14)
        
        self.saveButton.cornerRadius = 24
    }
}
