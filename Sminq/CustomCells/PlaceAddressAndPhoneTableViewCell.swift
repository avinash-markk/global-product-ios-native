//
//  PlaceAddressAndPhoneTableViewCell.swift
//  Sminq
//
//  Created by Pushkar Deshmukh on 04/03/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import UIKit

enum PlaceCellType: String {
    case phone = "phone"
    case address = "address"
}

protocol PlaceAddressAndPhoneTableViewCellDelegate: NSObjectProtocol {
    func directionsButtonDidSelect(place: LivePlaces?)
}

class PlaceAddressAndPhoneTableViewCell: UITableViewCell {
    public weak var delegate: PlaceAddressAndPhoneTableViewCellDelegate?
    
    @IBOutlet weak var rowHeadingLabel: UILabel!
    @IBOutlet weak var rowValue: UILabel!
    
    @IBOutlet weak var ctaButton: UIButton!
    @IBOutlet weak var ctaButtonWidthConstraint: NSLayoutConstraint!
    
    let phoneCTAWidth: CGFloat = 93
    let addressCTAWidth: CGFloat = 175
    var callButtonLabelText = "CALL"
    var directionButtonLabelText = "GET DIRECTIONS"
    
    var place: LivePlaces?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.initUI()
    }
    
    // MARK: Private functions
    private func initUI() {
        self.contentView.backgroundColor = AppConstants.SminqColors.blackLight100
        
        self.rowHeadingLabel.font = UIFont(name: AppConstants.Font.primaryBold, size: 14)
        self.rowHeadingLabel.textColor = AppConstants.SminqColors.white100.withAlphaComponent(0.75)
        
        self.rowValue.font = UIFont(name: AppConstants.Font.primaryRegular, size: 14)
        self.rowValue.textColor = AppConstants.SminqColors.white100.withAlphaComponent(0.75)
        
        self.ctaButton.titleLabel?.font = UIFont(name: AppConstants.Font.primaryBold, size: 14)
        self.ctaButton.layer.cornerRadius = self.ctaButton.height / 2.0
        self.ctaButton.clipsToBounds = true
        self.ctaButton.borderWidth = 1
        self.ctaButton.borderColor = AppConstants.SminqColors.white100.withAlphaComponent(0.75)
        self.ctaButton.setTitleColor(AppConstants.SminqColors.white.withAlphaComponent(0.75), for: .normal)
    }
    
    // MARK: Public functions
    
    // NOTE: Call this method once place data is set
    func setCellType(type: PlaceCellType, place: LivePlaces) {
        self.place = place
        
        switch type {
        case .phone:
            self.rowHeadingLabel.text = "PHONE"
            self.ctaButton.setTitle(self.callButtonLabelText, for: .normal)
            self.ctaButton.setImage(UIImage(named: "iconCall16White75"), for: .normal)
            self.rowValue.text = self.place?.internationalPhoneNumber
            self.ctaButtonWidthConstraint.constant = phoneCTAWidth
            
        case .address:
            self.rowHeadingLabel.text = "ADDRESS"
            self.ctaButton.setTitle(self.directionButtonLabelText, for: .normal)
            self.ctaButton.setImage(UIImage(named: "iconDirections16White75"), for: .normal)
            self.rowValue.text = self.place?.address
            self.ctaButtonWidthConstraint.constant = addressCTAWidth
        }
    }
    
    func showCallAlert() {
        if let internationalPhoneNumber = self.place?.internationalPhoneNumber, let url = URL(string: "tel://\(internationalPhoneNumber.onlyDigits()))") {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    
    
    // MARK: Button actions
    
    @IBAction func ctaButtonTapped(_ sender: UIButton) {
        if sender.titleLabel?.text == self.callButtonLabelText {
            if let place = self.place {
                AnalyticsHelper.callEvent(screen: AppConstants.screenNames.placeDetails, place: place)
            }
        
            self.showCallAlert()
        } else if sender.titleLabel?.text == self.directionButtonLabelText {
            if let place = self.place {
                AnalyticsHelper.directionsEvent(screen: AppConstants.screenNames.placeDetails, place: place)
            }
            
            self.delegate?.directionsButtonDidSelect(place: nil)
        }
    }
}
