//
//  ProfileBioTableViewCell.swift
//  Sminq
//
//  Created by Avinash Thakur on 04/03/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import UIKit
import SDWebImage

protocol ProfileBioTableViewCellDelegate: NSObjectProtocol {
    func claimRewardsButtonTapped(sender: UITableViewCell)
    func editProfileTooltipTapped()
    func infoToolTipTapped()
    func profileImageTapped()
}

class ProfileBioTableViewCell: UITableViewCell {

    weak var delegate: ProfileBioTableViewCellDelegate?
    
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var profileImageLabel: UILabel!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var userHandleNameLabel: UILabel!
    @IBOutlet weak var userBioLabel: UILabel!
    @IBOutlet weak var ratingsLabel: UILabel!
    @IBOutlet weak var placesLabel: UILabel!
    @IBOutlet weak var discoveredLabel: UILabel!
    
    @IBOutlet weak var claimRewardSubview: UIView!
    @IBOutlet weak var claimRewardLabel: UILabel!
    @IBOutlet weak var claimRewardButton: UIButton!
    @IBOutlet weak var infoButton: UIButton!

    @IBOutlet weak var toolTipView: UIView!
    @IBOutlet weak var toolTipSubView: UIView!
    @IBOutlet weak var toolTipLabel: UIView!
    @IBOutlet weak var toolTipAnchorView: UIView!
    
    
    @IBOutlet weak var toolTipColoredView: UIView!
    
    @IBOutlet weak var infoToolTipView: UIView!
    @IBOutlet weak var infoToolTipSubView: UIView!
    @IBOutlet weak var infoToolTipAnchorView: UIView!
    
    @IBOutlet weak var claimRewardHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var infoToolTipHeightConstraint: NSLayoutConstraint!
    
    var isMyProfile: Bool = true
    var isToolTipShown: Bool = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.initUI()
    }
    
    func initUI() {
        contentView.backgroundColor = .clear
        userNameLabel.font = UIFont(name: AppConstants.Font.primaryBold, size: 20)
        userNameLabel.textColor = AppConstants.SminqColors.white100
        
        userHandleNameLabel.font = UIFont(name: AppConstants.Font.primaryRegular, size: 16)
        userHandleNameLabel.textColor = AppConstants.SminqColors.white100
        
        userBioLabel.font = UIFont(name: AppConstants.Font.primaryRegular, size: 16)
        userBioLabel.textColor = AppConstants.SminqColors.white100.withAlphaComponent(0.75)
        
        profileImageLabel.textColor = AppConstants.SminqColors.white100
        profileImageLabel.backgroundColor = AppConstants.SminqColors.green100
        profileImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(profileImageTapped)))
        profileImageView.isUserInteractionEnabled = true
        self.selectionStyle = .none
        self.toolTipUISetup()
        self.infoToolTipViewSetup()
        self.claimRewardUISetup()
        self.translatesAutoresizingMaskIntoConstraints = false
    }
    
    func toolTipUISetup() {
        toolTipView.isHidden = true
        toolTipView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(toolTipTapped)))
        
        toolTipAnchorView.transform = CGAffineTransform(rotationAngle: CGFloat(Helper().convertDegreeToRadian(degree: -45)))
        toolTipAnchorView.backgroundColor = AppConstants.SminqColors.red200
        
        toolTipColoredView.backgroundColor = AppConstants.SminqColors.red200
        toolTipColoredView.layer.cornerRadius = 8
        
    }
    
    func infoToolTipViewSetup() {
        infoToolTipSubView.isHidden = true
        infoToolTipHeightConstraint.constant = 0
        infoToolTipSubView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(infoToolTipViewTapped)))
        self.infoToolTipAnchorView.transform = CGAffineTransform(rotationAngle: CGFloat(Helper().convertDegreeToRadian(degree: -45)))
    }
    
    func claimRewardUISetup() {
        claimRewardSubview.backgroundColor = AppConstants.SminqColors.blackDark100
        claimRewardLabel.font = UIFont(name: "Inter-Regular", size: 12)
        claimRewardLabel.textColor = AppConstants.SminqColors.white100.withAlphaComponent(0.75)
        claimRewardButton.backgroundColor = AppConstants.SminqColors.green200
        claimRewardButton.setTitleColor(AppConstants.SminqColors.white100, for: .normal)
    }
    
    @IBAction func claimRewardsButtonTapped(_ sender: Any) {
        self.delegate?.claimRewardsButtonTapped(sender: self)
    }
    

    @IBAction func infoButtonTapped(_ sender: UIButton) {
        self.delegate?.infoToolTipTapped()
    }
    
    func toolTipVisibility(show: Bool) {
        if show {
            print("tool tip show")
            infoToolTipSubView.isHidden = false
            infoToolTipHeightConstraint.constant = 70
        } else {
            infoToolTipSubView.isHidden = true
            infoToolTipHeightConstraint.constant = 0

            print("tool tip hide")
        }
    }
    
    @objc func toolTipTapped() {
        self.delegate?.editProfileTooltipTapped()
    }
    
    @objc func infoToolTipViewTapped() {
         infoToolTipSubView.isHidden = true
        isToolTipShown = false
        infoToolTipHeightConstraint.constant = 0
        self.delegate?.infoToolTipTapped()
    }
    
    @objc func profileImageTapped() {
        self.delegate?.profileImageTapped()
    }

    func getAttributedString(value: String, valueType: String) -> NSMutableAttributedString {
        
        let attributedString = NSMutableAttributedString(string: "\(value) \(valueType)", attributes: [
            .font: UIFont(name: AppConstants.Font.primaryRegular, size: 12.0)!,
            .foregroundColor: UIColor(red: 244.0 / 255.0, green: 242.0 / 255.0, blue: 242.0 / 255.0, alpha: 0.75),
            .kern: -0.09
            ])
        attributedString.addAttributes([
            .font: UIFont(name: AppConstants.Font.primaryBold, size: 12.0)!,
            .foregroundColor: UIColor.white
            ], range: NSRange(location: 0, length: value.count))
        
        return attributedString
        
    }
    
    func updateProfileBioDetails(data: UserDataDict?, userMetrics: UserMetrics?, myProfile: Bool, incompleteProfileCheck: Bool) {
        guard (data != nil) else { return }
        self.isMyProfile = myProfile
         infoButton.isHidden = !isMyProfile
        self.updateRatingsValue(metrics: userMetrics)
        
        self.userNameLabel.text = data?.name ?? ""
        self.userBioLabel.text = data?.bio ?? ""
        
        if let imageUrl = data?.imageUrl, imageUrl != "" {
            self.profileImageView.sd_setImage(with: URL(string: imageUrl), placeholderImage: nil)
        }
        
        if let imageLabelText = data?.displayName[0] {
            self.profileImageLabel.text = "\(imageLabelText.prefix(1).uppercased())"
        }
        
        if let displayName = data?.displayName[0] {
            self.userHandleNameLabel.text = "@\(displayName)"
        }
        
        if incompleteProfileCheck {
        self.checkAndDisplayIncompleteProfileToolTip(data: data)
        }
        
    }
    
    
    func checkAndDisplayIncompleteProfileToolTip(data: UserDataDict?) {
        if self.isMyProfile {
            if data?.isProfileComplete() ?? false {
                toolTipView.isHidden = true
            } else {
                toolTipView.isHidden = false
                DispatchQueue.main.asyncAfter(deadline: .now() + 5, execute: {
                    self.toolTipView.isHidden = true
                })
            }
        }
    }
    
    func checkAndShowClaimReward(profileModel: ProfileViewModel?) {
        guard let model = profileModel, let claim = model.userClaims else {
            self.hideClaimRewardsSubview()
            return
        }
        if claim.claimable && model.isMyProfile {
            claimRewardSubview.isHidden = false
            claimRewardLabel.text = claim.claimMessage
            claimRewardHeightConstraint.constant = 74.0
            claimRewardButton.setTitle("CLAIM REWARD", for: .normal)
        } else {
            self.hideClaimRewardsSubview()
        }
    }
    
    func hideClaimRewardsSubview() {
        claimRewardSubview.isHidden = true
        claimRewardHeightConstraint.constant = 0.0
    }
    
    func updateRatingsValue(metrics: UserMetrics?) {
        guard let userMetrics = metrics, let lifeTimeMetrics = userMetrics.lifeTimeMetrics else { return }
        let ratingsLabelText = lifeTimeMetrics.totalPosts == 1 ? "Rating" : "Ratings"
        self.ratingsLabel.attributedText = self.getAttributedString(value: "\(lifeTimeMetrics.totalPosts ?? 0)", valueType: ratingsLabelText)
        let placesLabelText = lifeTimeMetrics.totalPlaces == 1 ? "Place" : "Places"
        self.placesLabel.attributedText = self.getAttributedString(value: "\(lifeTimeMetrics.totalPlaces ?? 0)", valueType: placesLabelText)
        self.discoveredLabel.attributedText = self.getAttributedString(value: "\(lifeTimeMetrics.totalDiscovered ?? 0)", valueType: "Discovered")
    }
        
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
