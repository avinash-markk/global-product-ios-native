//
//  EmptyStoryStateTableViewCell.swift
//  Sminq
//
//  Created by Pushkar Deshmukh on 06/03/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import UIKit

protocol EmptyStoryStateTableViewCellDelegate: NSObjectProtocol {
    func plusIconCellImageViewTapped()
}

class EmptyStoryStateTableViewCell: UITableViewCell {

    weak var delegate: EmptyStoryStateTableViewCellDelegate?
    @IBOutlet weak var cardView: UIView!

    @IBOutlet weak var cellIconButton: UIButton!
    
    @IBOutlet weak var headingLabel: UILabel!
    
    @IBOutlet weak var subHeadingLabel: UILabel!
    
    @IBOutlet weak var seperatorView: UIView!
    
    var source = ""
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.initUI()
    }
    
    // MARK: Private functions
    
    private func initUI() {
        self.contentView.backgroundColor = AppConstants.SminqColors.blackDark100
        
        self.seperatorView.backgroundColor = AppConstants.SminqColors.white100.withAlphaComponent(0.1)
        
        self.cardView.clipsToBounds = true
        self.cardView.layer.cornerRadius = 8
        self.cardView.layer.borderWidth = 1
        self.cardView.borderColor = AppConstants.SminqColors.white100.withAlphaComponent(0.1)
        self.cardView.backgroundColor = AppConstants.SminqColors.blackLight100
        
        self.cellIconButton.layer.cornerRadius = self.cellIconButton.height / 2.0
        self.cellIconButton.clipsToBounds = true
        self.cellIconButton.borderColor = AppConstants.SminqColors.white100.withAlphaComponent(0.1)
        self.cellIconButton.borderWidth = 1.0
        
        self.imageVisibility(hide: false)
        self.headingLabelVisibility(hide: false)
        self.subHeadingLabelVisibility(hide: false)
        
        self.headingLabel.font = UIFont(name: AppConstants.Font.primaryRegular, size: 24)
        self.headingLabel.textColor = AppConstants.SminqColors.white100.withAlphaComponent(0.75)
        
        self.subHeadingLabel.text = UITextConstants.addLiveRating
        self.subHeadingLabel.textColor = AppConstants.SminqColors.white100.withAlphaComponent(0.75)
        self.subHeadingLabel.font = UIFont(name: AppConstants.Font.primaryRegular, size: 16)
        self.selectionStyle = .none

    }
    
     @objc func plusIconTapped(_ sender: UITapGestureRecognizer) {
        self.delegate?.plusIconCellImageViewTapped()
    }
    
    @IBAction func cellIconButtonTapped(_sender: UIButton) {
        self.delegate?.plusIconCellImageViewTapped()
    }
    
    func imageVisibility(hide: Bool) {
        self.cellIconButton.isHidden = hide
    }
    
    func headingLabelVisibility(hide: Bool) {
        self.headingLabel.isHidden = hide
    }
    
    func subHeadingLabelVisibility(hide: Bool) {
        self.subHeadingLabel.isHidden = hide
    }

    func setHeadingLabelBySource(source: String) {
        switch source {
        case AppConstants.screenNames.home:
            self.headingLabel.text = "No live ratings nearby"
        case AppConstants.screenNames.profile:
            self.headingLabel.text = "No live ratings"
        default:
            self.headingLabel.text = "No live ratings"
        }
    }
    
}
