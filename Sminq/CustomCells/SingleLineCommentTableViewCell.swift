//
//  SingleLineCommentTableViewCell.swift
//  Markk
//
//  Created by Pushkar Deshmukh on 23/05/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import UIKit

class SingleLineCommentTableViewCell: UITableViewCell {

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupLayout()
    }
    
    fileprivate lazy var commentLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.font = UIFont(name: AppConstants.Font.primaryRegular, size: 12)
        label.textColor = AppConstants.SminqColors.white100
        label.backgroundColor = .green
        label.sizeToFit()
        return label
    }()
    
//    fileprivate lazy var containerOverlayView: UIView = {
//        let view = UIView()
//        view.backgroundColor = AppConstants.SminqColors.shadow.withAlphaComponent(0.75)
//        view.layer.cornerRadius = 8
//        return view
//    }()
    
    var comment: String? {
        didSet {
            guard let comment = comment else { return }
            commentLabel.text = comment
        }
    }
    
    // MARK: Fileprivate functions
    
    fileprivate func setupLayout() {
        addSubview(commentLabel)
        commentLabel.fillSuperview()
        
//        addSubview(containerOverlayView)
//
//        containerOverlayView.anchor(top: topAnchor, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor, padding: .init(top: 4, left: 12, bottom: 0, right: 0))
//
//        containerOverlayView.addSubview(commentLabel)
//        commentLabel.anchor(top: nil, leading: containerOverlayView.leadingAnchor, bottom: nil, trailing: containerOverlayView.trailingAnchor)

    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
