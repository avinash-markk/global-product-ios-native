//
//  PlaceStoryCardCollectionViewCell.swift
//  Sminq
//
//  Created by Pushkar Deshmukh on 02/08/18.
//  Copyright © 2018 Suraj Sagare. All rights reserved.
//

import UIKit
import SDWebImage

class PlaceStoryCardCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var feedImageView: UIImageView!
    @IBOutlet weak var feedMoodIcon: UIImageView!
    @IBOutlet weak var placeNameLabel: UILabel!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var overlayView: UIView!
    
    @IBOutlet weak var countsStackView: UIStackView!
    
    @IBOutlet weak var postButton: UIButton!
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var commentButton: UIButton!
    @IBOutlet weak var shareButton: UIButton!
    
    var story: StoryData?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.initUI()
    }
    
    // MARK: Private functions
    
    private func initUI() {
        self.cardView.clipsToBounds = true
        self.cardView.borderWidth = 1
        self.cardView.layer.cornerRadius = 8
        self.cardView.borderColor = AppConstants.SminqColors.white100.withAlphaComponent(0.1)
//        self.overlayView.backgroundColor = AppConstants.SminqColors.shadow.withAlphaComponent(0.5)
        self.placeNameLabel.font = UIFont(name: AppConstants.Font.primaryBold, size: 14)
        self.placeNameLabel.textColor = AppConstants.SminqColors.white100
        self.placeNameLabel.textDropShadow()
        
        self.categoryLabel.font = UIFont(name: AppConstants.Font.primaryRegular, size: 12)
        self.categoryLabel.textColor = AppConstants.SminqColors.white100
        self.categoryLabel.textDropShadow()
        
        self.setButtonsUI()
    }
    
    func setButtonsUI() {
        let textFont = UIFont.init(name: AppConstants.Font.primaryRegular, size: 12)
        
        postButton.setTitleColor(AppConstants.SminqColors.white100, for: .normal)
        postButton.titleLabel?.font = textFont
        postButton.centerTextAndImage(spacing: 4)
        postButton.titleLabel?.textDropShadow()
        postButton.isHidden = true
        
        likeButton.setTitleColor(AppConstants.SminqColors.white100.withAlphaComponent(0.75), for: .normal)
        likeButton.titleLabel?.font = textFont
        likeButton.centerTextAndImage(spacing: 4)
        likeButton.titleLabel?.textDropShadow()
        likeButton.isHidden = true
        
        commentButton.setTitleColor(AppConstants.SminqColors.white100.withAlphaComponent(0.75), for: .normal)
        commentButton.titleLabel?.font = textFont
        commentButton.centerTextAndImage(spacing: 4)
        commentButton.titleLabel?.textDropShadow()
        commentButton.isHidden = true
        
        shareButton.setTitleColor(AppConstants.SminqColors.white100.withAlphaComponent(0.75), for: .normal)
        shareButton.titleLabel?.font = textFont
        shareButton.centerTextAndImage(spacing: 4)
        shareButton.titleLabel?.textDropShadow()
        shareButton.isHidden = true
    }
    
    func setStory(story: StoryData) {
        self.story = story
    }
    
    func updateDataOnCard() {
        guard let story = self.story,
            let posts = story.posts,
            let feed = posts.last,
            let otherContext = feed.getOtherContext(),
            let place = feed.placeDetails else {
            LogUtil.error("Could not find otherContext to show image/video")
            return
        }
        
        self.placeNameLabel.text = place.name
        
        updateDistanceLabel(place: place)
        updateThumbnail(otherContext: otherContext)
        
        let moodScoreMeta = feed.moodScore.getMoodMeta()
        self.feedMoodIcon.image = moodScoreMeta.imageWithBottomText
        
        postButton.updateCount(count: posts.count)
        likeButton.updateCount(count: feed.upVotesCount)
        commentButton.updateCount(count: feed.commentsCount)
        shareButton.updateCount(count: feed.sharesCount)
        
        countsStackView.isHidden = !feed.hasCounts()
    }
    
   
    
    fileprivate func updateDistanceLabel(place: LivePlaces) {
        if let subCategory = place.subCategory, let subCategoryName = subCategory.name {
            self.categoryLabel.text = "\(subCategoryName)"
            
            if let distance = place.distance, distance > 0 {
                self.categoryLabel.text = "\(subCategoryName) • \(SminqAPI.distanceFormated(rawDistanceDouble: round((distance))))"
            }
        }
    }
    
    fileprivate func updateThumbnail(otherContext: Contributions) {
        if otherContext.video != "" {
            if let imageName = otherContext.image, let url = CloudinaryService.shared.generateCloudinaryVideoURL(folder: FIRRemoteConfigService.shared.getCloudinaryVideoFolder(), fileName: imageName) {
                self.feedImageView.sd_setImage(with: url, placeholderImage: nil)
            }
        } else {
            if let imageName = otherContext.image, let url = CloudinaryService.shared.generateCloudinaryImageURL(folder: FIRRemoteConfigService.shared.getCloudinaryOriginalImageFolder(), fileName: imageName) {
                self.feedImageView.sd_setImage(with: url, placeholderImage: nil)
            }
        }
    }
}
