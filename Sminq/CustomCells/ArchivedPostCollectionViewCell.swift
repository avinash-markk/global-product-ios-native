//
//  ArchivedPostCollectionViewCell
//  Sminq
//
//  Created by Pushkar Deshmukh on 05/10/18.
//  Copyright © 2018 Suraj Sagare. All rights reserved.
//

import UIKit

class ArchivedPostCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var placeNameLabel: UILabel!

    @IBOutlet weak var feedImageView: UIImageView!
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var overlayView: UIView!
    @IBOutlet weak var postDateLabel: UILabel!
    @IBOutlet weak var placeMoodImageView: UIImageView!
    
    var placeStory: PlacesStoriesData?
    var prevPlaceStory: PlacesStoriesData?
    var otherContext: Contributions?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.initUI()
    }
    
    // MARK: Private functions
    
    private func initUI() {
        self.cardView.clipsToBounds = true
        self.cardView.borderColor = AppConstants.SminqColors.white100.withAlphaComponent(0.1)
        self.cardView.borderWidth = 1
        
        self.overlayView.backgroundColor = AppConstants.SminqColors.shadow.withAlphaComponent(0.5)
        
        self.placeNameLabel.font = UIFont(name: AppConstants.Font.primaryBold, size: 12)
        self.placeNameLabel.textColor = AppConstants.SminqColors.white100
        self.placeNameLabel.height = 16
        
        self.postDateLabel.font = UIFont(name: AppConstants.Font.primaryRegular, size: 12)
        self.postDateLabel.textColor = AppConstants.SminqColors.white100.withAlphaComponent(0.75)
        self.postDateLabel.height = 16
    }
    
    func setData() {
        guard let place = self.placeStory?.placeDetails, let stories = self.placeStory?.stories, stories.count > 0 else { return }
        guard let story = stories.first, story.posts.count > 0 else { return }
        guard let feed = story.posts.first, let otherContext = feed.getOtherContext(), let upVotesCount = feed.upVotesCount, let commentsCount = feed.commentsCount  else { return }
        
        self.placeNameLabel.text = place.name
        
        self.postDateLabel.isHidden = false
        self.placeNameLabel.isHidden = false
        
        if commentsCount == 0 && upVotesCount == 0 {
        } else if commentsCount > 0 && upVotesCount == 0 {
            // Show comments and hide upvote
        } else if commentsCount == 0 && upVotesCount > 0 {
            // Show comments and hide upvotes
        }
        
        if otherContext.video != "" {
            if let imageName = otherContext.image, let url = CloudinaryService.shared.generateCloudinaryVideoURL(folder: FIRRemoteConfigService.shared.getCloudinaryVideoFolder(), fileName: imageName) {
                self.feedImageView.sd_setImage(with: url, placeholderImage: nil)
            }
        } else {
            if let imageName = otherContext.image, let url = CloudinaryService.shared.generateCloudinaryImageURL(folder: FIRRemoteConfigService.shared.getCloudinaryOriginalImageFolder(), fileName: imageName) {
                self.feedImageView.sd_setImage(with: url, placeholderImage: nil)
            }
        }
        
        if let createdAt = feed.createdAt {
            let feedDate = Date(timeIntervalSince1970: TimeInterval(createdAt))
            let dateFormatterDay = DateFormatter()
            dateFormatterDay.dateFormat = "MM.dd.yyyy"
            
            self.postDateLabel.text = dateFormatterDay.string(from: feedDate)
        }
        
        let placeMoodData = feed.moodScore.getMoodMeta()
        self.placeMoodImageView.image = placeMoodData.imageWithBottomText
        
        cardDataVisibility()
    }
    
    func cardDataVisibility() {
        // Prev feed
        guard let prevPlaceStory = self.prevPlaceStory else { return }
        guard let prevPlace = prevPlaceStory.placeDetails, let prevStories = prevPlaceStory.stories, prevStories.count > 0 else { return }
        guard let prevStory = prevStories.first, prevStory.posts.count > 0 else { return }
        guard let prevFeed = prevStory.posts.first, let prevFeedCreatedAt = prevFeed.createdAt  else { return }
        
        // Current feed
        guard let place = self.placeStory?.placeDetails, let stories = self.placeStory?.stories, stories.count > 0 else { return }
        guard let story = stories.first, story.posts.count > 0 else { return }
        guard let feed = story.posts.first, let feedCreatedAt = feed.createdAt  else { return }
        
        // Current feed date
        let dateFormatterDay = DateFormatter()
        
        let feedDate = Date(timeIntervalSince1970: TimeInterval(feedCreatedAt))
        dateFormatterDay.dateFormat = "dd"
        let feedDateDay = dateFormatterDay.string(from: feedDate)
        
        
        dateFormatterDay.dateFormat = "MMM"
        let feedDateMonth = dateFormatterDay.string(from: feedDate)
        
        // Previous feed date
        let prevFeedDate = Date(timeIntervalSince1970: TimeInterval(prevFeedCreatedAt))
        dateFormatterDay.dateFormat = "dd"
        let prevFeedDateDay = dateFormatterDay.string(from: prevFeedDate)
        
        dateFormatterDay.dateFormat = "MMM"
        let prevFeedDateMonth = dateFormatterDay.string(from: prevFeedDate)
        
        if prevPlace.name == place.name && feedDateDay == prevFeedDateDay && feedDateMonth == prevFeedDateMonth {
            self.postDateLabel.isHidden = true
            self.placeNameLabel.isHidden = true
        }
    }

}
