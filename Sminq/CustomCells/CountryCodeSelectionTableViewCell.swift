//
//  CountryCodeSelectionTableViewCell.swift
//  Sminq
//
//  Created by Pushkar Deshmukh on 25/06/18.
//  Copyright © 2018 Suraj Sagare. All rights reserved.
//

import UIKit

class CountryCodeSelectionTableViewCell: UITableViewCell {

    @IBOutlet weak var countryNameLabel: UILabel!
    @IBOutlet weak var countryCodeLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.initUI()
    }

    // MARK: Private functions
    
    private func initUI() {
        self.contentView.backgroundColor = AppConstants.SminqColors.blackDark100
        
        self.countryNameLabel.font = UIFont(name: AppConstants.Font.primaryRegular, size: 16)
        self.countryNameLabel.textColor = AppConstants.SminqColors.white100
        
        self.countryCodeLabel.font = UIFont(name: AppConstants.Font.primaryRegular, size: 16)
        self.countryCodeLabel.textColor = AppConstants.SminqColors.white100
        
    }

}
