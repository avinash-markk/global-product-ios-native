//
//  SearchTableViewCell.swift
//  Sminq
//
//  Created by Pushkar Deshmukh on 11/08/18.
//  Copyright © 2018 Suraj Sagare. All rights reserved.
//

import UIKit

class SearchTableViewCell: UITableViewCell {

    @IBOutlet weak var headingLabel: UILabel!
    
    @IBOutlet weak var subHeadingLabel: UILabel!
    
    var place: GooglePlace? {
        didSet {
            guard let place = place else { return }
            updateData(place: place)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        setupUI()
    }
    
    // MARK: Fileprivate functions
    
    fileprivate func setupUI() {
        headingLabel.text = "Zero"
        headingLabel.font = UIFont(name: AppConstants.Font.primaryRegular, size: 16)
        headingLabel.textColor = AppConstants.SminqColors.white100.withAlphaComponent(0.75)
        
        subHeadingLabel.text = "Waka Waka, Testing address for multiple lines!!! Lane E, Ragvilas Society, Koregaon Park, Pune, Maharashtra, India"
        subHeadingLabel.font = UIFont(name: AppConstants.Font.primaryRegular, size: 12)
        subHeadingLabel.textColor = AppConstants.SminqColors.white100.withAlphaComponent(0.5)
        
    }
    
    fileprivate func updateData(place: GooglePlace) {
        headingLabel.text = place.name
        subHeadingLabel.text = place.address
    }
    
    // MARK: Public functions
    
    public func setGooglePlace(place: GooglePlace) {
        self.place = place
    }
}
