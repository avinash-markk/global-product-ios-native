//
//  EditProfileHeaderLabelTableViewCell.swift
//  Sminq
//
//  Created by Pushkar Deshmukh on 04/09/18.
//  Copyright © 2018 Suraj Sagare. All rights reserved.
//

import UIKit

class EditProfileHeaderLabelTableViewCell: UITableViewCell {

    @IBOutlet weak var headerLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.initUI()
    }

    // MARK: Private functions
    
    private func initUI() {
        self.contentView.backgroundColor = AppConstants.SminqColors.blackDark100
        
        self.headerLabel.font = UIFont(name: AppConstants.Font.primaryBold, size: 12)
        self.headerLabel.textColor = AppConstants.SminqColors.white100.withAlphaComponent(0.75)
    }
    

}
