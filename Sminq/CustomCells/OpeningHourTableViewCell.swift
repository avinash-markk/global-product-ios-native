//
//  OpeningHourTableViewCell.swift
//  Sminq
//
//  Created by Pushkar Deshmukh on 08/08/18.
//  Copyright © 2018 Suraj Sagare. All rights reserved.
//

import UIKit

class OpeningHourTableViewCell: UITableViewCell {

    @IBOutlet weak var hourLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
