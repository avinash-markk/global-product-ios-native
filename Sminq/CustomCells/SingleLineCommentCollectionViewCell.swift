//
//  SingleLineCommentCollectionViewCell.swift
//  Markk
//
//  Created by Pushkar Deshmukh on 23/05/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import UIKit

class SingleLineCommentCollectionViewCell: UICollectionViewCell {
    fileprivate lazy var commentLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 1
        label.font = UIFont(name: AppConstants.Font.primaryRegular, size: 12)
        label.textColor = AppConstants.SminqColors.white100
        label.lineBreakMode = .byWordWrapping
        return label
    }()
    
    fileprivate lazy var overlayView: UIView = {
        let view = UIView()
        view.backgroundColor = AppConstants.SminqColors.shadow.withAlphaComponent(0.75)
        view.layer.cornerRadius = 8
        view.layer.masksToBounds = true
        return view
    }()
    
    fileprivate lazy var userImageView: UIImageView = {
       let imageView = UIImageView()
        imageView.image = UIImage(named: "imgOnboarding1")
        imageView.contentMode = .scaleAspectFill
        imageView.layer.borderWidth = 1
        imageView.layer.borderColor = AppConstants.SminqColors.white100.withAlphaComponent(0.5).cgColor
        imageView.clipsToBounds = true
        return imageView
    }()
    
    fileprivate lazy var userInitialLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 1
        label.layer.borderWidth = 1
        label.layer.borderColor = AppConstants.SminqColors.white100.withAlphaComponent(0.1).cgColor
        label.backgroundColor = AppConstants.SminqColors.purple100
        label.text = "T"
        label.textColor = AppConstants.SminqColors.white100.withAlphaComponent(0.75)
        label.font = UIFont(name: AppConstants.Font.primaryBold, size: 12)
        label.clipsToBounds = true
        label.textAlignment = .center
        return label
    }()
    
    fileprivate lazy var commentOverlayViewAlpha: [CGFloat] = [0.75, 0.50, 0.25]
    fileprivate lazy var cellLeftPadding: CGFloat = 12
    fileprivate lazy var cellRightPadding: CGFloat = 12
    fileprivate lazy var imageLeftPadding: CGFloat = 8
    fileprivate lazy var imageRightPadding: CGFloat = 8
    fileprivate lazy var commentLabelRightPadding: CGFloat = 8
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupLayout()
    }
    
    var comment: Replies? {
        didSet {
            updateData()
        }
    }
    
    // MARK: Fileprivate functions
    
    fileprivate func setupLayout() {
//        backgroundColor = .yellow
        
        addSubview(overlayView)
        
        overlayView.frame = CGRect.init(x: cellLeftPadding, y: 0.0, width: frame.width - (cellLeftPadding + cellRightPadding), height: 32.0)
        
        overlayView.addSubview(userInitialLabel)
        overlayView.addSubview(userImageView)
        
        userImageView.frame = CGRect.init(x: 8, y: 4, width: 24, height: 24)
        userImageView.layer.cornerRadius = userImageView.frame.height / 2
        userInitialLabel.frame = userImageView.frame
        userInitialLabel.layer.cornerRadius = userInitialLabel.frame.height / 2
        
        overlayView.addSubview(commentLabel)
        commentLabel.frame = CGRect.init(x: userImageView.frame.width + imageLeftPadding + imageRightPadding, y: 8, width: overlayView.frame.width - (imageLeftPadding + imageRightPadding + userImageView.frame.width) - commentLabelRightPadding, height: 16)
    }
    
    fileprivate func updateOverlayViewWidth() {
        var width = commentLabel.intrinsicContentSize.width + userImageView.frame.width + imageLeftPadding + imageRightPadding + commentLabelRightPadding
        
        if width > (frame.width - (cellLeftPadding + cellRightPadding)) {
            width = frame.width - (cellLeftPadding + cellRightPadding)
        }
        
        overlayView.frame.size.width = width
    }
    
    fileprivate func updateData() {
        guard let comment = comment else { return }
        commentLabel.text = comment.input
        updateOverlayViewWidth()
        
        userInitialLabel.text = "\(comment.displayName.prefix(1))"
        userImageView.sd_setImage(with: URL(string: comment.userImage), placeholderImage: nil)
    }
    
    // MARK: Public functions
    public func setComments(comments: [Replies], index: Int) {
        guard index < commentOverlayViewAlpha.count else { return }
        overlayView.alpha = commentOverlayViewAlpha[(comments.count - 1) - index]
        comment = comments[index]
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
