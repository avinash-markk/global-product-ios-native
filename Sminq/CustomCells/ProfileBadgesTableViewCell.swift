//
//  ProfileBadgesTableViewCell.swift
//  Markk
//
//  Created by Avinash Thakur on 13/06/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import UIKit

protocol ProfileBadgesTableViewCellDelegate: NSObjectProtocol {
    func badgeTappedAt(index: Int)
}

class ProfileBadgesTableViewCell: UITableViewCell {

    weak var delegate: ProfileBadgesTableViewCellDelegate?
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var collectionViewFlowLayout: UICollectionViewFlowLayout!
    
    var badgesArray: [String] = []
    var badgesCollectionViewHeight = 600
    var badgesLevels: [Levels] = []
    var levelName: String = ""
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.initCollectionViewUI()
    }
    
    func initCollectionViewUI() {
        self.selectionStyle = .none
        collectionView.backgroundColor = AppConstants.SminqColors.blackDark100
        self.backgroundColor = AppConstants.SminqColors.blackDark100
        self.collectionView.register(UINib(nibName: "BadgesCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "BadgesCollectionViewCell")
        self.collectionViewFlowLayout.scrollDirection = .vertical
        self.collectionViewFlowLayout.minimumInteritemSpacing = 0
        self.collectionViewFlowLayout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0)
        self.collectionView.isScrollEnabled = false
        collectionView.delegate = self
        collectionView.dataSource = self
    }
    
    func reloadBadgesView() {
        self.collectionView.reloadData()
    }
    
    func updateBadges(levels: [Levels], name: String) {
        levelName = name
        badgesLevels = levels
        self.reloadBadgesView()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

extension ProfileBadgesTableViewCell: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return  badgesLevels.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BadgesCollectionViewCell", for: indexPath) as? BadgesCollectionViewCell else {
            return UICollectionViewCell()
        }
        var level = badgesLevels[indexPath.row]
        level.levelName = levelName
        cell.setBadge(level: level)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let totalWidth = collectionView.bounds.size.width
        let dimension = CGFloat(Int(totalWidth) / 3)
        return CGSize.init(width: dimension, height: CGFloat(144))
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
           self.delegate?.badgeTappedAt(index: indexPath.row)
    }
    
}
