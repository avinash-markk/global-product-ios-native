//
//  StoryCommentTableViewCell.swift
//  Markk
//
//  Created by Pushkar Deshmukh on 28/05/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import UIKit

protocol StoryCommentTableViewCellDelegate: NSObjectProtocol {
    func didDeleteCommentTap(commentToDelete: Replies)
    func didCommentUserImageViewTap(comment: Replies)
    func didCommentUserInitialLabelTap(comment: Replies)
    func didCommentUserDisplayNameLabelTap(comment: Replies)
}

class StoryCommentTableViewCell: UITableViewCell {
    public weak var delegate: StoryCommentTableViewCellDelegate?
    
    fileprivate lazy var commentLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.font = UIFont(name: AppConstants.Font.primaryRegular, size: 16)
        label.textColor = AppConstants.SminqColors.purple100.withAlphaComponent(0.75)
        return label
    }()
    
    fileprivate lazy var userImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.layer.borderWidth = 1
        imageView.layer.borderColor = UIColor.white.withAlphaComponent(0.5).cgColor
        imageView.layer.cornerRadius = 16
        imageView.clipsToBounds = true
        imageView.isUserInteractionEnabled = true
        imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleUserImageViewTap)))
        return imageView
    }()
    
    fileprivate lazy var userInitialLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 1
        label.layer.borderWidth = 1
        label.layer.borderColor = AppConstants.SminqColors.white100.withAlphaComponent(0.1).cgColor
        label.layer.cornerRadius = 16
        label.backgroundColor = AppConstants.SminqColors.purple100
        label.text = "T"
        label.textColor = AppConstants.SminqColors.white100.withAlphaComponent(0.75)
        label.font = UIFont(name: AppConstants.Font.primaryBold, size: 12)
        label.clipsToBounds = true
        label.textAlignment = .center
        label.isUserInteractionEnabled = true
        label.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleUserInitialLabelTap)))
        return label
    }()
    
    fileprivate lazy var userDisplayNameLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: AppConstants.Font.primaryRegular, size: 12)
        label.textColor = AppConstants.SminqColors.purple100.withAlphaComponent(0.5)
        label.isUserInteractionEnabled = true
        label.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleUserDisplayNameLabelTap)))
        return label
    }()
    
    fileprivate lazy var deleteButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("• Delete", for: .normal)
        button.setTitleColor(AppConstants.SminqColors.purple100.withAlphaComponent(0.5), for: .normal)
        button.titleLabel?.font = UIFont(name: AppConstants.Font.primaryRegular, size: 12)
        button.addTarget(self, action: #selector(handleDeleteComment), for: .touchUpInside)
//        button.backgroundColor = .red
        return button
    }()
    
    var comment: Replies? {
        didSet {
            updateData()
        }
    }
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupLayout()
    }
    
    // MARK: Fileprivate functions
    
    fileprivate func setupLayout() {
//        backgroundColor = .orange
        selectionStyle = .none
        
        addSubview(userInitialLabel)
        addSubview(userImageView)
        addSubview(userDisplayNameLabel)
        addSubview(commentLabel)
        addSubview(deleteButton)
        
        userInitialLabel.snp.makeConstraints { (make) in
            make.top.equalTo(self).offset(16)
            make.left.equalTo(self).offset(16)
            make.height.width.equalTo(32)
        }
        
        userImageView.snp.makeConstraints { (make) in
            make.top.left.right.equalTo(userInitialLabel)
            make.height.width.equalTo(32)
        }
        
        commentLabel.anchor(top: topAnchor, leading: userInitialLabel.trailingAnchor, bottom: bottomAnchor, trailing: trailingAnchor, padding: .init(top: 14, left: 8, bottom: 16, right: 8))
        
        userDisplayNameLabel.anchor(top: nil, leading: userInitialLabel.trailingAnchor, bottom: bottomAnchor, trailing: nil, padding: .init(top: 0, left: 8, bottom: 0, right: 0))

        deleteButton.anchor(top: nil, leading: userDisplayNameLabel.trailingAnchor, bottom: bottomAnchor, trailing: nil, padding: .init(top: 0, left: 4, bottom: -6, right: 0))
    }
    
    fileprivate func updateData() {
        guard let comment = comment else { return }
        commentLabel.text = comment.input
        
        userInitialLabel.text = "\(comment.displayName.prefix(1))"
        userImageView.sd_setImage(with: URL(string: comment.userImage), placeholderImage: nil)
        userDisplayNameLabel.text = "@\(comment.displayName) • \(Helper().getTimeAgoString(for: comment.createdAt))"
        
        deleteButton.isHidden = comment.userId != Helper().getUser()?._id
        
    }
    
    // MARK: Public functions
    
    public func setComment(comment: Replies) {
        self.comment = comment
    }
    
    // MARK: Selector functions
    
    @objc fileprivate func handleDeleteComment() {
        guard let comment = comment else { return }
        delegate?.didDeleteCommentTap(commentToDelete: comment)
    }
    
    @objc fileprivate func handleUserImageViewTap() {
        guard let comment = comment else { return }
        delegate?.didCommentUserImageViewTap(comment: comment)
    }
    
    @objc fileprivate func handleUserInitialLabelTap() {
        guard let comment = comment else { return }
        delegate?.didCommentUserInitialLabelTap(comment: comment)
    }
    
    @objc fileprivate func handleUserDisplayNameLabelTap() {
        guard let comment = comment else { return }
        delegate?.didCommentUserDisplayNameLabelTap(comment: comment)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
