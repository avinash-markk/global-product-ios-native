//
//  UserLiveStoriesTableViewCell.swift
//  Sminq
//
//  Created by Pushkar Deshmukh on 06/03/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import UIKit

protocol UserLiveStoriesTableViewCellDelegate: NSObjectProtocol {
    func didUserStorySelect(selectedStory: StoryData)
    func didRateTap(place: LivePlaces)
    func didEmptyStoryCellSelect()
}

class UserLiveStoriesTableViewCell: UITableViewCell {
    public weak var delegate: UserLiveStoriesTableViewCellDelegate?
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var collectionViewFlowLayout: UICollectionViewFlowLayout!
    
    var userStoriesViewModel: ProfileViewModel?
    let maxCardsCount = 4
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.initDelegates()
        self.initUI()
    }
    
    private func initDelegates() {
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
    }
    
    private func initUI() {
        self.contentView.backgroundColor = AppConstants.SminqColors.blackDark100
        
        self.collectionView.register(UINib(nibName: "PlaceStoryCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "PlaceStoryCollectionViewCell")
        self.collectionView.register(UINib(nibName: "EmptyPlaceStoryCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "EmptyPlaceStoryCollectionViewCell")
        self.collectionView.register(UINib(nibName: "EmptyCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "EmptyCollectionViewCell")
        
        self.collectionViewFlowLayout.scrollDirection = .horizontal
    }
    
    func setUserStoriesViewModel(dataModel: ProfileViewModel) {
        self.userStoriesViewModel = dataModel
        self.collectionView.reloadData()
        self.collectionView.collectionViewLayout.invalidateLayout()
    }
    
    func reloadData() {
        self.collectionView.reloadData()
        self.collectionView.collectionViewLayout.invalidateLayout()
        
        // NOTE: If not ran of main thread, collection cell is rendered without borders
        DispatchQueue.main.async {
            self.scrollToLast()
        }
    }
    
    func scrollToLast() {
        guard let userStoriesViewModel = self.userStoriesViewModel else { return }
        
        let userStories = userStoriesViewModel.userRatingsData
        
        if userStories.count > 0 && userStories.count >= (maxCardsCount - 1) {
            let indexPath = IndexPath(row: userStories.count - 1, section: 0)
            
            if self.collectionView.validate(indexPath: indexPath) {
                self.collectionView.scrollToItem(at: indexPath, at: .left, animated: true)
            }
        }
    }
    
    func numberOfEmptyCardsToReturn() -> Int {
        guard let userStoriesViewModel = self.userStoriesViewModel else { return 0 }
        
        let userStories = userStoriesViewModel.userRatingsData
        
        let storyCount = userStories.count
        
        if storyCount > 0 && storyCount < maxCardsCount {
            return maxCardsCount - storyCount
        }
        
        return 1
    }
    
}

extension UserLiveStoriesTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 0 {
            if let userStoriesViewModel = self.userStoriesViewModel {
                return userStoriesViewModel.userRatingsData.count
            }
        } else if section == 1 {
            return numberOfEmptyCardsToReturn()
        }
        
        return 0
    }
    

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.section == 0 {
            if let userStoryViewModel = self.userStoriesViewModel, userStoryViewModel.userRatingsData.count > 0, indexPath.row < userStoryViewModel.userRatingsData.count {
                guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PlaceStoryCollectionViewCell", for: indexPath) as? PlaceStoryCollectionViewCell else { return UICollectionViewCell() }
                cell.delegate = self
                cell.setStory(story: userStoryViewModel.userRatingsData[indexPath.row])
                cell.updateDataOnCard()
                return cell
            }
        } else {
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "EmptyPlaceStoryCollectionViewCell", for: indexPath) as? EmptyPlaceStoryCollectionViewCell else { return UICollectionViewCell() }
            return cell
        }
       
        
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "EmptyCollectionViewCell", for: indexPath) as? EmptyCollectionViewCell else { return UICollectionViewCell() }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 120, height: self.collectionView.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let _ = collectionView.cellForItem(at: indexPath) as? PlaceStoryCollectionViewCell {
            guard let userStoryViewModel = self.userStoriesViewModel, userStoryViewModel.userRatingsData.count > 0 else { return }
            
            self.delegate?.didUserStorySelect(selectedStory: userStoryViewModel.userRatingsData[indexPath.row])
        } else if let _ = collectionView.cellForItem(at: indexPath) as? EmptyPlaceStoryCollectionViewCell {
            self.delegate?.didEmptyStoryCellSelect()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        if section == 1 {
            return UIEdgeInsets(top: 0, left: 2, bottom: 0, right: 16)
        }
        
        return UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 8)
        
    }
}

extension UserLiveStoriesTableViewCell: PlaceStoryCollectionViewCellDelegate {
    func didRateTap(place: LivePlaces) {
        self.delegate?.didRateTap(place: place)
    }
}
