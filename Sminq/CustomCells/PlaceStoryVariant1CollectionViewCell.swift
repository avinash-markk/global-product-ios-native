//
//  PlaceStoryVariant1CollectionViewCell.swift
//  Sminq
//
//  Created by Pushkar Deshmukh on 06/03/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import UIKit

class PlaceStoryVariant1CollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var feedImageView: UIImageView!
    
    @IBOutlet weak var placeName: UILabel!
    
    @IBOutlet weak var placeCategory: UILabel!
    
    @IBOutlet weak var cardView: UIView!
    
    @IBOutlet weak var overlayView: UIView!
    
    @IBOutlet weak var placeMoodImageView: UIImageView!
    
    @IBOutlet weak var postButton: UIButton!
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var commentButton: UIButton!
    @IBOutlet weak var shareButton: UIButton!
    
    @IBOutlet weak var countsStackView: UIStackView!
    var place: PlacesStoriesData? {
        didSet {
            updateDataOnCard()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.initUI()
    }
    
    // MARK: Private functions
    
    func setButtonsUI() {
        let textFont = UIFont.init(name: AppConstants.Font.primaryRegular, size: 12)
        
        postButton.setTitleColor(AppConstants.SminqColors.white100, for: .normal)
        postButton.titleLabel?.font = textFont
        postButton.centerTextAndImage(spacing: 4)
        postButton.titleLabel?.textDropShadow()
        postButton.isHidden = true
        
        
        likeButton.setTitleColor(AppConstants.SminqColors.white100.withAlphaComponent(0.75), for: .normal)
        likeButton.titleLabel?.font = textFont
        likeButton.centerTextAndImage(spacing: 4)
        likeButton.titleLabel?.textDropShadow()
        likeButton.isHidden = true
        
        commentButton.setTitleColor(AppConstants.SminqColors.white100.withAlphaComponent(0.75), for: .normal)
        commentButton.titleLabel?.font = textFont
        commentButton.centerTextAndImage(spacing: 4)
        commentButton.titleLabel?.textDropShadow()
        commentButton.isHidden = true
        
        shareButton.setTitleColor(AppConstants.SminqColors.white100.withAlphaComponent(0.75), for: .normal)
        shareButton.titleLabel?.font = textFont
        shareButton.centerTextAndImage(spacing: 4)
        shareButton.titleLabel?.textDropShadow()
        shareButton.isHidden = true
    }
    
    func initUI() {
        self.placeName.font = UIFont(name: AppConstants.Font.primaryBold, size: 14)
        self.placeName.textColor = AppConstants.SminqColors.white100
        self.placeName.isHidden = true
        self.placeName.textDropShadow()
        
        self.placeCategory.font = UIFont(name: AppConstants.Font.primaryRegular, size: 12)
        self.placeCategory.textColor = AppConstants.SminqColors.white100
        self.placeCategory.isHidden = true
        self.placeCategory.textDropShadow()
        
//        self.overlayView.backgroundColor = AppConstants.SminqColors.shadow.withAlphaComponent(0.5)
        
        self.cardView.clipsToBounds = true
        self.cardView.layer.cornerRadius = 8
        self.cardView.borderColor = AppConstants.SminqColors.white100.withAlphaComponent(0.1)
        self.cardView.borderWidth = 1
        
        setButtonsUI()
    }
    
    func setPlace(place: PlacesStoriesData) {
        self.place = place
    }
    
    func updateDataOnCard() {
        guard let placeStories = self.place, let place = placeStories.placeDetails else { return }
        
        if let placeMoodScore = place.placeMoodScore {
            let moodScoreMeta = placeMoodScore.getMoodMeta()
            self.placeMoodImageView.image = moodScoreMeta.imageWithBottomText
        } else {
            let moodScoreMeta = 0.getMoodMeta()
            self.placeMoodImageView.image = moodScoreMeta.imageWithBottomText
        }
        
        if let subCategory = place.subCategory, let subCategoryName = subCategory.name {
            self.placeCategory.text = "\(subCategoryName)"
            
            if let distance = place.distance, distance > 0 {
                self.placeCategory.text = "\(subCategoryName) • \(SminqAPI.distanceFormated(rawDistanceDouble: round((distance))))"
            }
            
            self.placeCategory.isHidden = false
        }
        
        guard let userStory = placeStories.stories?.first else { return }
        
        guard let feed = userStory.posts.first else { return }
        
        self.placeName.text = place.name
        self.placeName.isHidden = false
        
        guard let otherContext = feed.getOtherContext() else {
            LogUtil.error("Could not find other context while showing story card!")
            return
        }
        
        if otherContext.video != "" {
            if let imageName = otherContext.image, let url = CloudinaryService.shared.generateCloudinaryVideoURL(folder: FIRRemoteConfigService.shared.getCloudinaryVideoFolder(), fileName: imageName) {
                self.feedImageView.sd_setImage(with: url, placeholderImage: nil)
            }
        } else {
            if let imageName = otherContext.image, let url = CloudinaryService.shared.generateCloudinaryImageURL(folder: FIRRemoteConfigService.shared.getCloudinaryOriginalImageFolder(), fileName: imageName) {
                self.feedImageView.sd_setImage(with: url, placeholderImage: nil)
            }
        }
        
        updateCounts(placeMetrics: place.placeMetrics)
    }
    
    func updateCounts(placeMetrics: PlaceMetrics?) {
        guard let placeMetrics = placeMetrics else { return }
        var showCountsStackView = false
        
//        guard let placeMetrics = userStory.placeMetrics else { return }
        if let count = placeMetrics.totalUpVotes, count > 0 {
            likeButton.setTitle("\(count)", for: .normal)
            likeButton.isHidden = false
            showCountsStackView = true
        } else {
            likeButton.setTitle("", for: .normal)
            likeButton.isHidden = true
        }
        
        if let count = placeMetrics.totalComments, count > 0 {
            commentButton.setTitle("\(count)", for: .normal)
            commentButton.isHidden = false
            showCountsStackView = true
        } else {
            commentButton.setTitle("", for: .normal)
            commentButton.isHidden = true
        }
        
        if placeMetrics.totalShares > 0 {
            shareButton.setTitle("\(placeMetrics.totalShares)", for: .normal)
            shareButton.isHidden = false
            showCountsStackView = true
        } else {
            shareButton.isHidden = true
        }
        
        countsStackView.isHidden = !showCountsStackView
       
        if let count = placeMetrics.totalPosts, count > 0 {
            postButton.setTitle("\(count)", for: .normal)
            postButton.isHidden = false
        } else {
            postButton.setTitle("", for: .normal)
            postButton.isHidden = true
        }
        
        
    }
    
    // MARK: Public functions
    
    func showSkeleton() {
        self.cardView.showAnimatedGradientAnimation()
    }
    
    func hideSkeleton() {
        self.cardView.hideAnimatedGratientAnimation()
    }

}
