//
//  UploadsTableViewCell.swift
//  Sminq
//
//  Created by Avinash Thakur on 14/03/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import UIKit

protocol UploadsTableViewCellDelegate: NSObjectProtocol {
    func uploadActionButtonTapped(sender: UIButton)
}

class UploadsTableViewCell: UITableViewCell {

    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var uploadPlaceLabel: UILabel!
    @IBOutlet weak var uploadTimeLabel: UILabel!
    @IBOutlet weak var uploadProgressLabel: UILabel!
    @IBOutlet weak var uploadImageView: UIImageView!
    @IBOutlet weak var uploadActionButton: UIButton!
    
    weak var delegate: UploadsTableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.initUI()
    }
    
    func initUI() {
        self.contentView.backgroundColor = AppConstants.SminqColors.blackDark100
        uploadPlaceLabel.font = UIFont(name: AppConstants.Font.primaryBold, size: 20)
        uploadPlaceLabel.textColor = AppConstants.SminqColors.white100
        
        uploadTimeLabel.font = UIFont(name: AppConstants.Font.primaryRegular, size: 14)
        uploadTimeLabel.textColor = AppConstants.SminqColors.white100
        
        uploadProgressLabel.font = UIFont(name: AppConstants.Font.primaryRegular, size: 12)
        uploadProgressLabel.textColor = AppConstants.SminqColors.white100.withAlphaComponent(0.75)
        
        uploadActionButton.titleLabel?.font = UIFont(name: AppConstants.Font.primaryBold, size: 14)
        uploadActionButton.setTitleColor(AppConstants.SminqColors.white100.withAlphaComponent(0.75), for: .normal)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    func updateDataForUploads(task: UploadTask) {
        self.uploadPlaceLabel.text = task.placeName!
        self.uploadTimeLabel.text = Date().getFormattedTimeForUploads(dateString: task.taskId!)
        self.updateUploadThumbnail(task: task)
        
        switch task.status {
        case "Pending":
            updateUIForPendingState()
            
        case "InProgress":
            updateUIForProgressState(task: task)
            
        case "Completed":
            updateUIForCompletedState()
            
        case "Failed":
            updateUIForFailedState()
            
        default:
            break
        }
    }
    
    func updateUploadThumbnail(task: UploadTask) {
        if task.isVideo {
            if let thumbnailPath = Helper().returnSavedThumbnailForVideoAt(taskId: task.taskId!) {
                self.uploadImageView.image = UIImage(contentsOfFile: thumbnailPath)
            }
        } else {
            let imageFileUrlPath: String = DocDirectoryUtil().getLocalUrlForTask(id: task.taskId!, isVideo: task.isVideo)
            self.uploadImageView.image = UIImage(contentsOfFile: imageFileUrlPath)
        }
    }
    
    func updateUIForPendingState() {
        uploadActionButton.setTitle("CANCEL", for: .normal)
        uploadProgressLabel.text = "NEXT..."
        progressView.progress = 0.0
    }
    
    func updateUIForCompletedState() {
        progressView.progress = 1.0
        progressView.progressTintColor = AppConstants.SminqColors.green100
        uploadProgressLabel.text = "UPLOADING...100%"
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            self.uploadActionButton.setTitle("VIEW", for: .normal)
            self.uploadProgressLabel.text = "UPLOADED"
        }
    }
    
    func updateUIForProgressState(task: UploadTask) {
        let progressPercent = self.getPercentageFromFloat(value: task.progress)
        if progressPercent <= 95 {
            uploadProgressLabel.text = "UPLOADING...(\(progressPercent)%)"
            progressView.progress = task.progress
        } else {
            uploadProgressLabel.text = "UPLOADING...(95%)"
            progressView.progress = 0.95
        }
        uploadActionButton.setTitle("CANCEL", for: .normal)
        progressView.progressTintColor = AppConstants.SminqColors.green100
    }
    
    func updateUIForFailedState() {
        uploadActionButton.setTitle("RETRY", for: .normal)
        uploadProgressLabel.text = "UPLOAD FAILED"
        progressView.progress = 1.0
        progressView.progressTintColor = AppConstants.SminqColors.pinkishRed
    }
    
    func getPercentageFromFloat(value: Float) -> Int {
        let percentInt  = Int(value * 100)
        return percentInt
    }
    
    @IBAction func uploadActionButtonTapped(_ sender: UIButton) {
        self.delegate?.uploadActionButtonTapped(sender: sender)
    }

}
