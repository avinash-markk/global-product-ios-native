//
//  NotificationsCell.swift
//  Sminq
//
//  Created by SMINQ iOS on 02/04/18.
//  Copyright © 2018 Suraj Sagare. All rights reserved.
//

import UIKit

class NotificationsCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var bodyLabel: UILabel!
//    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var feedImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.initUI()
    }
    
    // MARK: Private functions
    private func initUI() {
        self.contentView.backgroundColor = AppConstants.SminqColors.blackDark100
        
        self.titleLabel.font = UIFont(name: AppConstants.Font.primaryRegular, size: 12)
        self.titleLabel.textColor = AppConstants.SminqColors.white100.withAlphaComponent(0.5)
        
        self.bodyLabel.font = UIFont(name: AppConstants.Font.primaryRegular, size: 16)
        self.bodyLabel.textColor = AppConstants.SminqColors.white100.withAlphaComponent(0.75)
        
        self.feedImageView.layer.cornerRadius = 8
        self.feedImageView.borderColor = AppConstants.SminqColors.white100.withAlphaComponent(0.1)
        self.feedImageView.borderWidth = 1
    }
    
}
