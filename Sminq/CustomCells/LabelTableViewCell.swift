//
//  LabelTableViewCell.swift
//  Sminq
//
//  Created by Pushkar Deshmukh on 28/06/18.
//  Copyright © 2018 Suraj Sagare. All rights reserved.
//

import UIKit

class LabelTableViewCell: UITableViewCell {

    @IBOutlet weak var postCountLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
