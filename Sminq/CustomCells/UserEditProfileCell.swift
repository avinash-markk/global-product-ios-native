//
//  UserEditProfileCell.swift
//  Sminq
//
//  Created by Pushkar Deshmukh on 30/04/18.
//  Copyright © 2018 Suraj Sagare. All rights reserved.
//

import UIKit

class UserEditProfileCell: UITableViewCell {

    @IBOutlet weak var rowLabel: UILabel!
    
    @IBOutlet weak var rowInput: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.initUI()
    }

    // MARK: Private functions
    
    private func initUI() {
        self.contentView.backgroundColor = AppConstants.SminqColors.blackDark100
        
        self.rowLabel.font = UIFont(name: AppConstants.Font.primaryRegular, size: 12.0)
        self.rowLabel.textColor = AppConstants.SminqColors.white100.withAlphaComponent(0.75)
        
        self.rowInput.font = UIFont(name: AppConstants.Font.primaryRegular, size: 16.0)
        self.rowInput.textColor = AppConstants.SminqColors.white100
    }

}
