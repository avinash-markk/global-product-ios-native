//
//  ProfileDetailsTableViewCell.swift
//  Sminq
//
//  Created by Avinash Thakur on 05/03/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import UIKit

class ProfileDetailsTableViewCell: UITableViewCell {

    @IBOutlet weak var liveRatingsHeaderLabel: UILabel!
    @IBOutlet weak var liveRatingsValueLabel: UILabel!
    
    @IBOutlet weak var placesRatedHeaderLabel: UILabel!
    @IBOutlet weak var placesRatedValueLabel: UILabel!
    
    @IBOutlet weak var totalLikesHeaderLabel: UILabel!
    @IBOutlet weak var totalLikesValueLabel: UILabel!
    
    @IBOutlet weak var totalViewsHeaderLabel: UILabel!
    @IBOutlet weak var totalViewsValueLabel: UILabel!
    
    @IBOutlet weak var totalSharesHeaderLabel: UILabel!
    @IBOutlet weak var totalSharesValueLabel: UILabel!
    
    var ratingSubView = AddRatingView()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.initUI()
        ratingSubView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: self.frame.height)
        ratingSubView.isHidden = true
        self.addSubview(ratingSubView)
    }
    
    func initUI() {
        liveRatingsHeaderLabel.font = UIFont(name: AppConstants.Font.primaryBold, size: 12)
        liveRatingsHeaderLabel.textColor = AppConstants.SminqColors.purple100.withAlphaComponent(0.75)
        
        liveRatingsValueLabel.font = UIFont(name: AppConstants.Font.primaryRegular, size: 56)
        liveRatingsValueLabel.textColor = AppConstants.SminqColors.purple100
        
        placesRatedHeaderLabel.font = UIFont(name: AppConstants.Font.primaryBold, size: 12)
        placesRatedHeaderLabel.textColor = AppConstants.SminqColors.purple100.withAlphaComponent(0.75)
        
        placesRatedValueLabel.font = UIFont(name: AppConstants.Font.primaryRegular, size: 56)
        placesRatedValueLabel.textColor = AppConstants.SminqColors.purple100
        
        totalLikesHeaderLabel.font = UIFont(name: AppConstants.Font.primaryBold, size: 12)
        totalLikesHeaderLabel.textColor = AppConstants.SminqColors.purple100.withAlphaComponent(0.75)
        
        totalLikesValueLabel.font = UIFont(name: AppConstants.Font.primaryRegular, size: 32)
        totalLikesValueLabel.textColor = AppConstants.SminqColors.purple100
        
        totalViewsHeaderLabel.font = UIFont(name: AppConstants.Font.primaryBold, size: 12)
        totalViewsHeaderLabel.textColor = AppConstants.SminqColors.purple100.withAlphaComponent(0.75)
        
        totalViewsValueLabel.font = UIFont(name: AppConstants.Font.primaryRegular, size: 32)
        totalViewsValueLabel.textColor = AppConstants.SminqColors.purple100
        
        totalSharesHeaderLabel.font = UIFont(name: AppConstants.Font.primaryBold, size: 12)
        totalSharesHeaderLabel.textColor = AppConstants.SminqColors.purple100.withAlphaComponent(0.75)
        
        totalSharesValueLabel.font = UIFont(name: AppConstants.Font.primaryRegular, size: 32)
        totalSharesValueLabel.textColor = AppConstants.SminqColors.purple100
    }
    
    func showRatingSubview(show: Bool) {
        if show {
            ratingSubView.isHidden = false
        } else {
            ratingSubView.isHidden = true
        }
    }
    
    func updateUserProfileMetrics(data: UserMetrics?) {
        // Pending binding
//        self.liveRatingsValueLabel.text = "\(data?.monthlyMetrics?.posts ?? 0)"
//        self.placesRatedValueLabel.text = "\(data?.monthlyMetrics?.places ?? 0)"
//        self.totalLikesValueLabel.text = "\(data?.monthlyMetrics?.upVotes ?? 0)"
//        self.totalViewsValueLabel.text = "\(data?.monthlyMetrics?.views ?? 0)"
//        self.totalSharesValueLabel.text = "\(data?.monthlyMetrics?.shares ?? 0)"
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
