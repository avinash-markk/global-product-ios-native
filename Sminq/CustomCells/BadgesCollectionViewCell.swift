//
//  BadgesCollectionViewCell.swift
//  Markk
//
//  Created by Avinash Thakur on 13/06/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import UIKit
import SDWebImage

class BadgesCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var bagdeImageView: UIImageView!
    @IBOutlet weak var badgeValueLabel: UILabel!
    
    var badgeLevel: Levels!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.initUI()
    }
    
    func initUI() {
        self.backgroundColor = AppConstants.SminqColors.blackDark100
        self.badgeValueLabel.font = UIFont(name: AppConstants.Font.primaryRegular, size: 12)
        self.badgeValueLabel.textColor = AppConstants.SminqColors.white100.withAlphaComponent(0.75)
    }
    
    func setBadge(level: Levels) {
        badgeLevel = level
        badgeValueLabel.text = badgeLevel.badge?.name ?? ""
        
        if badgeLevel.completed {
            if let imageUrl = badgeLevel.badge?.activeImage {
                self.setBadgeImage(urlString: imageUrl)
            }
        } else {
            if let imageUrl = badgeLevel.badge?.inActiveImage {
                self.setBadgeImage(urlString: imageUrl)
            }
        }
    }
    
    func setBadgeImage(urlString: String) {
        self.bagdeImageView.sd_setShowActivityIndicatorView(true)
        self.bagdeImageView.sd_setIndicatorStyle(.white)
        self.bagdeImageView.sd_setImage(with: URL(string: urlString), placeholderImage: nil, options: SDWebImageOptions.retryFailed, completed: {
            (image, error, cacheType, imageURL) -> Void in
            self.bagdeImageView.sd_removeActivityIndicator()
        })
    }
    
}
