//
//  PlaceFeedStoriesTableViewCell.swift
//  Sminq
//
//  Created by Pushkar Deshmukh on 04/03/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import UIKit

protocol PlaceFeedStoriesTableViewCellDelegate: NSObjectProtocol {
    func placeStoryDidSelect(placeStories: PlacesStoriesData?, storyIndex: Int?)
    func didEmptyStoryCellSelect()
}

class PlaceFeedStoriesTableViewCell: UITableViewCell {
    public weak var delegate: PlaceFeedStoriesTableViewCellDelegate?
    @IBOutlet weak var storiesCollectionView: UICollectionView!
    @IBOutlet weak var storiesCollectionViewFlowLayout: UICollectionViewFlowLayout!
    
    @IBOutlet weak var seperatorView: UIView!
    
    var placeStoryViewModel: PlaceStoryViewModel? {
        didSet {
            updateUIForArchivedState()
        }
    }
    var loadingStories = false
    let maxCardsCount = 4
    var topPadding: CGFloat = 40
    
    fileprivate lazy var dayRecapButton: UIButton = {
        let button = UIButton()
        button.centerTextAndImage(spacing: 4)
        button.setTitle("30 DAY RECAP", for: .normal)
        button.setTitleColor(AppConstants.SminqColors.white100.withAlphaComponent(0.75), for: .normal)
        button.titleLabel?.font = UIFont.init(name: AppConstants.Font.primaryBold, size: 12)
        button.layer.cornerRadius = 20
        button.clipsToBounds = true
        button.layer.borderColor = AppConstants.SminqColors.white100.withAlphaComponent(0.1).cgColor
        button.layer.borderWidth = 1
        button.backgroundColor = AppConstants.SminqColors.blackLight100
        return button
    }()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.initDelegates()
        self.setupUI()
        self.storiesCollectionView.reloadData()
        
    }

    // MARK: Private methods
    
    private func initDelegates() {
        self.storiesCollectionView.delegate = self
        self.storiesCollectionView.dataSource = self
    }
    
    private func setupUI() {
        self.seperatorView.backgroundColor = AppConstants.SminqColors.white100.withAlphaComponent(0.1)
        
        self.storiesCollectionView.backgroundColor = UIColor.clear
        self.storiesCollectionView.register(UINib(nibName: "PlaceCardCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "PlaceCardCollectionViewCell")
        self.storiesCollectionView.register(UINib(nibName: "EmptyPlaceStoryCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "EmptyPlaceStoryCollectionViewCell")
        self.storiesCollectionView.register(UINib(nibName: "EmptyCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "EmptyCollectionViewCell")
        
        self.storiesCollectionViewFlowLayout.scrollDirection = .horizontal
        self.storiesCollectionViewFlowLayout.sectionInset = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
        
    }
    
    fileprivate func updateUIForArchivedState() {
        guard let placeStoryViewModel = placeStoryViewModel else { return }
        
        if placeStoryViewModel.isArchived {
            addGradientView()
        }
    }
    
    fileprivate func addGradientView() {
        let gradientView = CustomGradientView(colors: [
            AppConstants.SminqColors.red100.withAlphaComponent(0.25).cgColor,
            AppConstants.SminqColors.red100.withAlphaComponent(0.1).cgColor,
            ], locations: [0.5])
        
        contentView.insertSubview(gradientView, at: 0)
        gradientView.fillSuperview()
    }
    
    // MARK: Public functions
    
    func loadingState(state: Bool) {
        self.loadingStories = state
        self.storiesCollectionView.reloadData()
        self.storiesCollectionView.collectionViewLayout.invalidateLayout()
    }
    
    func numberOfEmptyCardsToReturn() -> Int {
        guard let placeStoryViewModel = self.placeStoryViewModel, let stories = placeStoryViewModel.getPlaceAt(index: 0)?.stories, stories.count > 0 else { return 0 }
        
        let storyCount = stories.count
        
        if storyCount > 0 && storyCount < maxCardsCount {
            return maxCardsCount - storyCount
        }
        
        return 1
    }
}

extension PlaceFeedStoriesTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout  {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 0 {
            if self.loadingStories {
                return 3
            } else {
                if let placeStoryViewModel = self.placeStoryViewModel, let stories = placeStoryViewModel.getPlaceAt(index: 0)?.stories, stories.count > 0 {
                    return stories.count
                }
            }
        } else if section == 1 {
            if !self.loadingStories {
                return numberOfEmptyCardsToReturn()
            }
            
        }
        
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.section == 0 {
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PlaceCardCollectionViewCell", for: indexPath) as? PlaceCardCollectionViewCell else { return UICollectionViewCell() }
            
            if loadingStories {
                cell.showSkeleton()
            } else {
                if let placeStoryViewModel = self.placeStoryViewModel, let places = placeStoryViewModel.getPlaceAt(index: 0), let stories = places.stories {
                    cell.setStory(story: stories[indexPath.row])
                    cell.updateDataOnCard()
                }
                
                cell.hideSkeleton()
            }
            
            return cell
        } else if indexPath.section == 1 {
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "EmptyPlaceStoryCollectionViewCell", for: indexPath) as? EmptyPlaceStoryCollectionViewCell else { return UICollectionViewCell() }
            return cell
        }
        
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "EmptyCollectionViewCell", for: indexPath) as? EmptyCollectionViewCell else { return UICollectionViewCell() }
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 160, height: 284)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        var top: CGFloat = 0
        
        if let placeStoryViewModel = placeStoryViewModel, placeStoryViewModel.isArchived {
            top = 40
        }
        
        if section == 1 {
            return UIEdgeInsets(top: top, left: 2, bottom: 40, right: 16)
        }
        
        return UIEdgeInsets(top: top, left: 16, bottom: 40, right: 8)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let _ = collectionView.cellForItem(at: indexPath) as? PlaceCardCollectionViewCell {
            guard let placeStoryViewModel = self.placeStoryViewModel, let placeStories = placeStoryViewModel.places.first else { return }
            self.delegate?.placeStoryDidSelect(placeStories: placeStories, storyIndex: indexPath.row)
        } else if let _ = collectionView.cellForItem(at: indexPath) as? EmptyPlaceStoryCollectionViewCell {
            self.delegate?.didEmptyStoryCellSelect()
        }
    }
}
