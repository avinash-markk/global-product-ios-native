//
//  CommentTableViewCell.swift
//  Sminq
//
//  Created by Pushkar Deshmukh on 26/07/18.
//  Copyright © 2018 Suraj Sagare. All rights reserved.
//

import UIKit

protocol CommentTableViewCellDelegate: NSObjectProtocol {
    func userHandleTapped(reply: Replies, tag: Tags)
    func deleteCommentLabelTapped(_ sender: CommentTableViewCell)
}

class CommentTableViewCell: UITableViewCell {
    public weak var delegate: CommentTableViewCellDelegate?
//    @IBOutlet weak var commentLabel: ActiveLabel!
    
    @IBOutlet weak var commentLabel: UILabel!
    @IBOutlet weak var userInitialLabel: UILabel!
    
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var commentTimeLabel: UILabel!
    
    @IBOutlet weak var userDisplayName: UILabel!
    @IBOutlet weak var commentDeleteLabel: UILabel!
    
    var reply: Replies?
    var isArchivedPost: Bool? = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.setupUI()
    }
    
    func setupUI() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapLabel(tap:)))
        self.commentLabel.addGestureRecognizer(tap)
        self.commentLabel.isUserInteractionEnabled = true
        self.commentDeleteLabel.isUserInteractionEnabled = true
        
        self.userInitialLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(userProfileTappedOnComments(_:))))
        self.userImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(userProfileTappedOnComments(_:))))
        self.userDisplayName.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(userProfileTappedOnComments(_:))))
        self.commentDeleteLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(commentDeleteLabelTapped(_:))))
        
        self.userDisplayName.isUserInteractionEnabled = true
        self.userInitialLabel.isUserInteractionEnabled = true
        self.userImageView.isUserInteractionEnabled = true
        self.commentDeleteLabel.isUserInteractionEnabled = true
        
        self.userDisplayName.font = UIFont(name: AppConstants.Font.primaryRegular, size: 12)
        self.userDisplayName.textColor = AppConstants.SminqColors.purple100.withAlphaComponent(0.5)
        
        self.commentTimeLabel.font = UIFont(name: AppConstants.Font.primaryRegular, size: 12)
        self.commentTimeLabel.textColor = AppConstants.SminqColors.purple100.withAlphaComponent(0.5)
        
        self.commentDeleteLabel.font = UIFont(name: AppConstants.Font.primaryRegular, size: 12)
        self.commentDeleteLabel.textColor = AppConstants.SminqColors.purple100.withAlphaComponent(0.5)
    }
    
    // MARK: Private functions
    
    func setData() {
        // End, parse text and highlight user handles
        
        let commonAttributes: [NSAttributedStringKey: Any] = [
            NSAttributedStringKey.font: UIFont(name: AppConstants.Font.primaryRegular, size: 16.0) as Any,
            NSAttributedStringKey.foregroundColor: AppConstants.SminqColors.purple100.withAlphaComponent(0.75) as Any
        ]
        
        let highlightedAttributes: [NSAttributedStringKey: Any] = [
            NSAttributedStringKey.font: UIFont(name: AppConstants.Font.primaryRegular, size: 16.0) as Any,
            NSAttributedStringKey.foregroundColor: AppConstants.SminqColors.pinkishRed as Any,
            NSAttributedStringKey.underlineStyle: NSUnderlineStyle.styleSingle.rawValue
        ]
        
        self.commentLabel.attributedText = Helper().highlightHandles(text: (reply?.input)!, commonAttributes: commonAttributes, highlightedAttributes: highlightedAttributes, tags: reply?.mediaMetadata?.textList![0].tags)
        
        if let displayName = reply?.displayName {
            self.userInitialLabel.text = "\(displayName.prefix(1).uppercased())"
            self.userDisplayName.text = "@\(displayName)"
        }
        
        self.userImageView.sd_setImage(with: URL(string: (reply?.userImage)!), placeholderImage: nil)
        
        let date = NSDate(timeIntervalSince1970: TimeInterval((reply?.createdAt)!))
        self.commentTimeLabel.text = "• \(PushNotificationAPI.timeAgoSinceDate(date as Date))"
        
        if Helper().getUserId() == reply?.userId, !self.isArchivedPost! {
            self.commentDeleteLabel.isHidden = false
        } else {
            self.commentDeleteLabel.isHidden = true
        }
    }
    
    // MARK: Target methods
    @objc func commentDeleteLabelTapped(_ sender: UITapGestureRecognizer) {
        self.delegate?.deleteCommentLabelTapped(self)
    }
    
    @objc func userProfileTappedOnComments(_ sender: UITapGestureRecognizer) {
        let userInfo: [AnyHashable: Any] = ["reply": reply]
        NotificationCenter.default.post(name: Notification.Name(rawValue: AppConstants.AppNotificationNames.userProfileTappedOnComments), object: nil, userInfo: userInfo)
    }
    
    @objc func tapLabel(tap: UITapGestureRecognizer) {
        let tapLocation = tap.location(in: self.commentLabel)
        let index = self.commentLabel.indexOfAttributedTextCharacterAtPoint(point: tapLocation)
        
        print(index)
        
        
        
//        for (commentIndex, char) in (self.commentLabel.text?.enumerated())! {
//
//            if commentIndex < index  {
//                print(char)
//               print(commentIndex)
//
//            }
//        }
        
        // TODO: Assert here. Check if tapped index is handle..
        var characterCount = 0
        var userHandleCount = 0
        var wordCount = 0
        
        if let commentText = self.commentLabel.text {
            var textArray = commentText.components(separatedBy: .whitespacesAndNewlines)
            
            for mainIndex in 0..<textArray.count {
               
                if characterCount > index {
                    // Break loop if character traverse count exceeds tapped index
                    break
                }
                
                if textArray[mainIndex].first == "@" && textArray[mainIndex].count >= 3 {
                    userHandleCount += 1
                }
                characterCount = characterCount + textArray[mainIndex].count
                characterCount += 1 // for space
                
                wordCount += 1
                
                
            }
            
            print(textArray[wordCount-1])
            
            if textArray[wordCount-1].first == "@" && textArray[wordCount-1].count >= 3 && userHandleCount > 0 {
                if reply?.mediaMetadata?.textList![0].tags![userHandleCount-1].network != "none" {
                    self.delegate?.userHandleTapped(reply: self.reply!, tag: (reply?.mediaMetadata?.textList![0].tags![userHandleCount-1])!)
                }
            }
            
        }
        
    }

}
