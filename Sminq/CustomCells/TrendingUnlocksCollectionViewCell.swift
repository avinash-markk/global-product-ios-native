//
//  TrendingUnlocksCollectionViewCell.swift
//  Markk
//
//  Created by Avinash Thakur on 24/07/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import UIKit

class TrendingUnlocksCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var userIcon: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var userBadge: UIImageView!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var verticalSubview: UIView!
    
    var banner: Banner? {
        didSet {
            updateData()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }
    
    func setupUI() {
        descLabel.font = UIFont(name: AppConstants.Font.primaryRegular, size: 12)
        descLabel.textColor = AppConstants.SminqColors.white100
        self.contentView.backgroundColor = AppConstants.SminqColors.blackLight100
        self.contentView.layer.cornerRadius = 8.0
        self.contentView.layer.borderWidth = 1.0
        self.contentView.layer.borderColor = AppConstants.SminqColors.white100.withAlphaComponent(0.1).cgColor
        self.contentView.clipsToBounds = true
        verticalSubview.backgroundColor = AppConstants.SminqColors.red100
        userIcon.layer.cornerRadius = 24.0
        userIcon.borderWidth = 1.0
        userIcon.borderColor = AppConstants.SminqColors.white100.withAlphaComponent(0.5)
        userIcon.clipsToBounds = true
        userNameLabel.layer.cornerRadius = 24.0
        userNameLabel.borderWidth = 1.0
        userNameLabel.borderColor = AppConstants.SminqColors.white100.withAlphaComponent(0.5)
        userNameLabel.clipsToBounds = true
        userNameLabel.font = UIFont(name: AppConstants.Font.primaryBold, size: 16)
        userNameLabel.textColor = AppConstants.SminqColors.white100
        userNameLabel.backgroundColor = AppConstants.SminqColors.green100
    }
    
    public func setBanner(banner: Banner) {
        self.banner = banner
    }
    
    fileprivate func updateData() {
        guard let banner = banner else { return }
        descLabel.text = banner.label
        if let bannerColor = banner.color {
            verticalSubview.backgroundColor = UIColor().colorFrom(hex: bannerColor)
        }
        if let url = URL(string: banner.icon) {
            userBadge.sd_setImage(with: url)
        }
        
        if let user = banner.users.first {
            if let imageUrl = URL(string: user.imageUrl) {
                self.userIcon.sd_setImage(with: imageUrl, placeholderImage: nil)
            }
            let userNameLabel = user.getUserDisplayName()
            self.userNameLabel.text = "\(userNameLabel.prefix(1).uppercased())"
        }
    }
    
}
