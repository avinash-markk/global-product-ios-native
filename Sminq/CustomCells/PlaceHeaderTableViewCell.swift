//
//  PlaceHeaderTableViewCell.swift
//  Sminq
//
//  Created by Pushkar Deshmukh on 04/03/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import UIKit

protocol PlaceHeaderTableViewCellDelegate: NSObjectProtocol {
    func backButtonTapped()
    func placeMoodIconTapped()
}

class PlaceHeaderTableViewCell: UITableViewCell {
    public weak var delegate: PlaceHeaderTableViewCellDelegate?
    
    @IBOutlet weak var placeNameLabel: UILabel!
    
    @IBOutlet weak var placeSubcategoryLabel: UILabel!
    
    @IBOutlet weak var placeAddressLabel: UILabel!
    
    @IBOutlet weak var placeMoodImageView: UIImageView!
    @IBOutlet weak var dayRecapButton: UIButton!
    
    var placeStoryViewModel: PlaceStoryViewModel? {
        didSet {
            updateUIForArchivedState()
        }
    }
    
    var place: LivePlaces?
    var LocationMgr = UserLocationManager.SharedManager
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.initUI()
    }
    
    // MARK: Public functions
    
    func setPlace(place: LivePlaces) {
        self.place = place
    }
    
    func updateData() {
        guard let place = self.place else { return }
        
        self.placeNameLabel.text = place.name
        
        self.placeSubcategoryLabel.text = "\(place.getSubCategoryName())".uppercased()
        
        if place.getSubCategoryName() == "" {
            self.placeSubcategoryLabel.isHidden = true
        } else {
            self.placeSubcategoryLabel.isHidden = false
        }
        
        if let distance = place.distance, distance > 0, LocationMgr.isLocationServiceEnabled(), LocationMgr.isLocationServiceAuthorized() {
            placeAddressLabel.text = "\(SminqAPI.formattedAddressFromAddressComponentWithoutInitialDots(addressComponent: place.addressComponents)) • \(SminqAPI.distanceFormated(rawDistanceDouble: round((distance))))"
        } else {
            placeAddressLabel.text = "\(SminqAPI.formattedAddressFromAddressComponentWithoutInitialDots(addressComponent: place.addressComponents))"
        }
        
        if let placeMood = place.placeMoodScore {
            let moodScoreMeta = placeMood.getMoodMeta()
            self.placeMoodImageView.image = moodScoreMeta.imageWithBottomText
        } else {
            let moodScoreMeta = 0.getMoodMeta()
            self.placeMoodImageView.image = moodScoreMeta.imageWithBottomText
        }
       
    }
    
    // MARK: Private functions
    
    private func initUI() {
        layer.zPosition = 1
        self.contentView.backgroundColor = AppConstants.SminqColors.blackDark100
        
        self.placeNameLabel.font = UIFont(name: AppConstants.Font.primaryRegular, size: 20)
        self.placeNameLabel.textColor = AppConstants.SminqColors.white100
        
        self.placeSubcategoryLabel.font = UIFont(name: AppConstants.Font.primaryBold, size: 12)
        self.placeSubcategoryLabel.textColor = AppConstants.SminqColors.white100.withAlphaComponent(0.75)
        
        self.placeAddressLabel.font = UIFont(name: AppConstants.Font.primaryRegular, size: 12)
        self.placeAddressLabel.textColor = AppConstants.SminqColors.white100.withAlphaComponent(0.75)
        
        let moodTapGesture = UITapGestureRecognizer(target: self, action: #selector(placeMoodTapped(_:)))
        self.placeMoodImageView.addGestureRecognizer(moodTapGesture)
        self.placeMoodImageView.isUserInteractionEnabled = true
        
        dayRecapButton.setBackgroundColor(AppConstants.SminqColors.blackLight100, forState: .normal)
//        dayRecapButton.backgroundColor = AppConstants.SminqColors.blackLight100
        dayRecapButton.layer.cornerRadius = 16
        dayRecapButton.layer.borderWidth = 1
        dayRecapButton.layer.borderColor = AppConstants.SminqColors.white100.withAlphaComponent(0.1).cgColor
        dayRecapButton.centerTextAndImage(spacing: 4)
        dayRecapButton.titleLabel?.font = UIFont.init(name: AppConstants.Font.primaryBold, size: 12)
        dayRecapButton.isHidden = true
        
    }
    
    fileprivate func updateUIForArchivedState() {
        guard let placeStoryViewModel = placeStoryViewModel else { return }
        
        if placeStoryViewModel.isArchived {
            dayRecapButton.isHidden = false
        } else {
            dayRecapButton.isHidden = true
        }
    }

    // MARK: Button actions
    
    @IBAction func backButtonTapped(_ sender: UIButton) {
        self.delegate?.backButtonTapped()
    }
    
    // MARK: Target functions
    
    @objc func placeMoodTapped(_ sender: UITapGestureRecognizer) {
        self.delegate?.placeMoodIconTapped()
    }
}
