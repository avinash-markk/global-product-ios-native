//
//  UserTaggingSuggestionCollectionViewCell.swift
//  Sminq
//
//  Created by Pushkar Deshmukh on 17/09/18.
//  Copyright © 2018 Suraj Sagare. All rights reserved.
//

import UIKit
import SDWebImage

class UserTaggingSuggestionCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var userInitialLabel: UILabel!
    @IBOutlet weak var userImageView: UIImageView!
    
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var userDisplayNameLabel: UILabel!
    
    var source: String? // Based on source comment labels styles are updated
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    var user: UserDataDict?

    func setData() {
        if source == AppConstants.comment {
            self.userNameLabel.textColor = AppConstants.SminqColors.purpleyGrey
            self.userDisplayNameLabel.textColor = AppConstants.SminqColors.purpleyGrey75
        } else if source == AppConstants.post {
            self.userNameLabel.textColor = UIColor.white
            self.userDisplayNameLabel.textColor = UIColor.white
        }
        
        if let displayName = user?.displayName[0], let name = user?.name {
            self.userInitialLabel.text = "\(displayName.prefix(1).uppercased())"
            self.userDisplayNameLabel.text = "@\(displayName)"
            self.userNameLabel.text = "\(name)"
        }
        
        self.userImageView.sd_setImage(with: URL(string: (user?.imageUrl)!), placeholderImage: nil)
    }
    
}
