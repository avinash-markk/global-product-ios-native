//
//  ProfileTabsCell.swift
//  Markk
//
//  Created by Avinash Thakur on 12/06/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import UIKit

enum TabIndex: Int {
    case ratingsTab, badgesTab, archivesTab
}

protocol ProfileTabsCellDelegate: NSObjectProtocol {
    func ratingsTapped(place: LivePlaces)
    func updateTabOnSwipe(index: Int)
    func archiveRatingsTapped(place: LivePlaces, placeFeed: [UserDataPlaceFeed])
    func createRatingTapped()
}

class ProfileTabsCell: UITableViewCell {
    
    weak var delegate: ProfileTabsCellDelegate?
    @IBOutlet weak var tabTableView: UITableView!
    
    var selectedTab: TabIndex = TabIndex.init(rawValue: 0)!
    
    public var profileTabsModel: ProfileViewModel?
    public var isMyProfile: Bool = true
    public var displayName: String = ""
    var ratingsSectionHeight: Float = 298.0
    
    private var archiveRatingsModel: ArchiveRatingsViewModel?
    var archiveRatingsList: [ArchiveRatingsData]? = []
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.addSwipeGesturesToTabsTableView()
        self.setupTabsTableViewUI()
        self.registerTableViewCells()
        self.selectionStyle = .none
    }
    
    func setupTabsTableViewUI() {
        tabTableView.estimatedRowHeight = 1000
        tabTableView.rowHeight = UITableViewAutomaticDimension
        tabTableView.separatorStyle = UITableViewCellSeparatorStyle.none
        tabTableView.backgroundColor = AppConstants.SminqColors.blackDark100
        tabTableView.isScrollEnabled = false
    }
    
    func registerTableViewCells() {
        tabTableView.register(UINib.init(nibName: "ProfileRatingsTableViewCell", bundle: nil), forCellReuseIdentifier: "ProfileRatingsTableViewCell")
        tabTableView.register(UINib.init(nibName: "EmptyStoryStateTableViewCell", bundle: nil), forCellReuseIdentifier: "EmptyStoryStateTableViewCell")
        tabTableView.register(UINib.init(nibName: "ProfileBadgesTableViewCell", bundle: nil), forCellReuseIdentifier: "ProfileBadgesTableViewCell")
        tabTableView.register(UINib.init(nibName: "StreaksTableViewCell", bundle: nil), forCellReuseIdentifier: "StreaksTableViewCell")
        tabTableView.register(UINib.init(nibName: "ArchiveRatingsTableViewCell", bundle: nil), forCellReuseIdentifier: "ArchiveRatingsTableViewCell")
        tabTableView.register(UINib.init(nibName: "CommonHeaderView", bundle: nil), forHeaderFooterViewReuseIdentifier: "CommonHeaderView")
    }
    
    func addSwipeGesturesToTabsTableView() {
        let swipeLeftRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipeLeft))
        swipeLeftRecognizer.direction = UISwipeGestureRecognizerDirection.left
        swipeLeftRecognizer.delegate = self
        tabTableView.addGestureRecognizer(swipeLeftRecognizer)
        
        let swipeRightRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipeRight))
        swipeRightRecognizer.direction = UISwipeGestureRecognizerDirection.right
        swipeRightRecognizer.delegate = self
        tabTableView.addGestureRecognizer(swipeRightRecognizer)
    }
    
    @objc func handleSwipeLeft() {
        if selectedTab != TabIndex.archivesTab {
            let int = selectedTab.rawValue
            selectedTab = TabIndex.init(rawValue: int + 1)!
            self.delegate?.updateTabOnSwipe(index: int + 1)
        }
    }
    
    @objc func handleSwipeRight() {
        if selectedTab != TabIndex.ratingsTab {
            let int = selectedTab.rawValue
            selectedTab = TabIndex.init(rawValue: int - 1)!
            self.delegate?.updateTabOnSwipe(index: int - 1)
        }
    }
    
    func setProfileTabsData(profileModel: ProfileViewModel?, archiveModel: ArchiveRatingsViewModel?, tabIndex: Int) {
        self.profileTabsModel = profileModel
        self.archiveRatingsModel = archiveModel
        self.archiveRatingsList = archiveModel?.archiveRatingsList
        self.isMyProfile = profileModel?.isMyProfile ?? true
        self.displayName = profileModel?.displayName ?? ""
        selectedTab = TabIndex.init(rawValue: tabIndex)!
        reloadTableContents()
    }
    
    func reloadTableContents() {
        self.tabTableView.reloadData()
    }

}

extension ProfileTabsCell: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        switch selectedTab {
        case TabIndex.ratingsTab:
            return 1
        case TabIndex.badgesTab:
            return profileTabsModel?.userLevelsList.count ?? 0
        case TabIndex.archivesTab:
            if archiveRatingsList?.count ?? 0 > 0 {
                return archiveRatingsList?.count ?? 0
            }
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch selectedTab {
        case TabIndex.ratingsTab:
            return CGFloat.leastNormalMagnitude
        case TabIndex.badgesTab:
            return 56
        case TabIndex.archivesTab:
            if archiveRatingsList?.count ?? 0 > 0 {
                return 56
            } else {
                return CGFloat.leastNormalMagnitude
            }
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        switch selectedTab {
        case TabIndex.ratingsTab:
            return nil
        case TabIndex.badgesTab:
            if let levelName = profileTabsModel?.userLevelsList[section].id {
                
                return self.getHeaderViewForProfileSection(isRatingsSection: false, isBlur: false, headerText: levelName.uppercased())
            } else {
                return self.getHeaderViewForProfileSection(isRatingsSection: false, isBlur: false, headerText: "")
            }
        case TabIndex.archivesTab:
            if archiveRatingsList?.count ?? 0 > 0 {
                if let archiveDateString = archiveRatingsList?[section].archiveDate {
                    return self.getHeaderViewForProfileSection(isRatingsSection: false, isBlur: false, headerText: "\(Helper().getFormattedArchivedHeaderText(dateString: archiveDateString))")
                }
            }
            return nil
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch selectedTab {
        case TabIndex.ratingsTab:
            return 1
        case TabIndex.badgesTab:
            return getBadgesSectionTableRowCount(section: section)
        case TabIndex.archivesTab:
            return getArchivesSectionTableRowCount(section: section)
        }
    }
    
    func getBadgesSectionTableRowCount(section: Int) -> Int {
        if let showStreaks = profileTabsModel?.userLevelsList[section].showProgress {
            if showStreaks {
                return 2
            }
        } else {
            return 1
        }
        return 1
    }
    
    func getArchivesSectionTableRowCount(section: Int) -> Int {
        if archiveRatingsList?.count ?? 0 > 0 {
            return archiveRatingsList?[section].archivePlacesRatings?.count ?? 0
        } else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        guard let profileModel = self.profileTabsModel else { return 0 }
        
        switch selectedTab {
        case TabIndex.ratingsTab:
                return profileModel.getRatingsSectionHeight()
        case TabIndex.badgesTab:
             let showStreaks = profileModel.userLevelsList[indexPath.section].showProgress
                if showStreaks && indexPath.row == 0 {
                    return 56
                }
            return 344
        case TabIndex.archivesTab:
            guard let archiveModel = self.archiveRatingsModel else { return 0 }
            
            if archiveModel.archiveRatingsList.count > 0 {
                return archiveModel.getArchiveCollectionViewHeight(atSection: indexPath.section, atIndex: indexPath.row)
            } else {
               return 298
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch selectedTab {
        case TabIndex.ratingsTab:
            return getProfileRatingsTabCell(indexPath: indexPath)
        case TabIndex.badgesTab:
            return getProfileBadgesTabCell(indexPath: indexPath)
        case TabIndex.archivesTab:
            return getProfileArchiveTabCell(indexPath: indexPath)
        }
    }
    
    func getProfileRatingsTabCell(indexPath: IndexPath) -> UITableViewCell {
        if profileTabsModel?.checkIfUserHasNoLiveRatings() ?? false {
            return getEmptyRatingsStateCell(indexPath: indexPath)
        } else {
            return getRatingsCell(indexPath: indexPath)
        }
    }
    
    func getRatingsCell(indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tabTableView.dequeueReusableCell(withIdentifier: "ProfileRatingsTableViewCell", for: indexPath) as? ProfileRatingsTableViewCell else {
            return UITableViewCell()
        }
        if let liveRatings = self.profileTabsModel?.userRatingsData {
            cell.updateUsersLiveRatings(data: liveRatings)
        }
        cell.delegate = self
        return cell
    }
    
    func getEmptyRatingsStateCell(indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tabTableView.dequeueReusableCell(withIdentifier: "EmptyStoryStateTableViewCell", for: indexPath) as? EmptyStoryStateTableViewCell else {
            return UITableViewCell()
        }
        
        cell.subHeadingLabelVisibility(hide: false)
        if isMyProfile {
            cell.delegate = self
            cell.imageVisibility(hide: false)
            cell.headingLabelVisibility(hide: true)
            cell.subHeadingLabel.text = "To add a live rating, \ntap on the plus icon below."
        } else {
            cell.imageVisibility(hide: true)
            cell.headingLabelVisibility(hide: false)
            cell.headingLabel.text = "No live ratings."
            cell.subHeadingLabel.text = "@\(displayName) has been \nquiet lately."
        }
        return cell
    }
    
    func getProfileBadgesTabCell(indexPath: IndexPath) -> UITableViewCell {
        if let showStreaks = profileTabsModel?.userLevelsList[indexPath.section].showProgress {
            if showStreaks && indexPath.row == 0 {
                return self.getProfileStreaksCell(indexPath: indexPath)
            }
        }
        return self.getBadgesCell(indexPath: indexPath)
    }
    
    func getBadgesCell(indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tabTableView.dequeueReusableCell(withIdentifier: "ProfileBadgesTableViewCell", for: indexPath) as? ProfileBadgesTableViewCell else {
            return UITableViewCell()
        }
        if let level = profileTabsModel?.userLevelsList[indexPath.section] {
            cell.updateBadges(levels: level.levels, name: level.id)
        }
        cell.delegate = self
        return cell
    }
    
    func getProfileStreaksCell(indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tabTableView.dequeueReusableCell(withIdentifier: "StreaksTableViewCell", for: indexPath) as? StreaksTableViewCell else {
            return UITableViewCell()
        }
        if let streaks = self.profileTabsModel?.userStreaks {
        cell.updateUserStreaksData(streak: streaks)
        }
        return cell
    }
    
    func getProfileArchiveTabCell(indexPath: IndexPath) -> UITableViewCell {
        if archiveRatingsList?.count ?? 0 > 0 {
            return getArchiveRatingsCell(indexPath: indexPath)
        } else {
            return getEmptyArchivesStateCell(indexPath: indexPath)
        }
    }
    
    func getEmptyArchivesStateCell(indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tabTableView.dequeueReusableCell(withIdentifier: "EmptyStoryStateTableViewCell", for: indexPath) as? EmptyStoryStateTableViewCell else {
            return UITableViewCell()
        }
        cell.imageVisibility(hide: true)
        cell.headingLabelVisibility(hide: false)
        cell.subHeadingLabelVisibility(hide: false)
        cell.headingLabel.text = "No archived ratings."
        if isMyProfile {
            cell.subHeadingLabel.text = "Your live ratings will show \nhere once they expire."
        } else {
            cell.subHeadingLabel.text = "As @\(displayName) live ratings expire, \nthey’ll show up here."
        }
        return cell
    }
    
    func getArchiveRatingsCell(indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tabTableView.dequeueReusableCell(withIdentifier: "ArchiveRatingsTableViewCell", for: indexPath) as? ArchiveRatingsTableViewCell else { return UITableViewCell() }
        cell.delegate = self
        if let archivesPlaceRatings = archiveRatingsList?[indexPath.section].archivePlacesRatings?[indexPath.row] {
            cell.setArchivesPLacesRatings(data: archivesPlaceRatings)
        }
        return cell
    }
    
    func getHeaderViewForProfileSection(isRatingsSection: Bool, isBlur: Bool, headerText: String) -> UIView {
        let headerView = UIView.init(frame: CGRect(x: 0, y: 0, width: tabTableView.width, height: 56))
        let subView = CommonHeaderView.init(frame: CGRect(x: 0, y: 0, width: headerView.width, height: headerView.height), isBlur: isBlur)
        subView.headerLabel.text = headerText
        subView.headerLabel.font = UIFont(name: AppConstants.Font.primaryBold, size: 14)
        subView.headerLabel.textColor = AppConstants.SminqColors.white100
        headerView.addSubview(subView)
        return headerView
    }

}

extension ProfileTabsCell: ProfileRatingsTableViewCellDelegate {
    
    func liveRatingsTapped(ratings: StoryData) {
        self.delegate?.ratingsTapped(place: ratings.placeDetails!)
        
    }
    
}

extension ProfileTabsCell: ProfileBadgesTableViewCellDelegate {
    
    func badgeTappedAt(index: Int) {
        print("badges at \(index)")
    }
    
}

extension ProfileTabsCell: ArchiveRatingsTableViewCellDelegate {
    
    func archiveRatingTappedAt(index: Int, archivedPlaces: ArchivePlacesRatings) {
        if let userfeed: [UserDataPlaceFeed] = (archivedPlaces.archiveRatings?[index].posts) {
            var allPosts = [UserDataPlaceFeed]()
            
            if let userStories = archivedPlaces.archiveRatings {
                userStories.forEach { (userStory) in
                    userStory.posts.forEach({ (post) in
                        allPosts.append(post)
                    })
                }
            }
            
            self.delegate?.archiveRatingsTapped(place: archivedPlaces.archivePlace!, placeFeed: allPosts)
        }
    }
    
}

extension ProfileTabsCell: EmptyStoryStateTableViewCellDelegate {
    
    func plusIconCellImageViewTapped() {
        self.delegate?.createRatingTapped()
    }

}

