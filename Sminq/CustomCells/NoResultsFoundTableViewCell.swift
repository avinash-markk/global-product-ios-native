//
//  NoResultsFoundTableViewCell.swift
//  Sminq
//
//  Created by SMINQ iOS on 14/11/18.
//  Copyright © 2018 Suraj Sagare. All rights reserved.
//

import UIKit

class NoResultsFoundTableViewCell: UITableViewCell {

    @IBOutlet weak var noResultsLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.setupUI()
    }

    func setupUI() {
        self.noResultsLabel.font = UIFont(name: AppConstants.Font.primaryRegular, size: 16.0)
        self.noResultsLabel.textColor = AppConstants.SminqColors.purpleyGrey
        self.noResultsLabel.text = "No matches found…"
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
