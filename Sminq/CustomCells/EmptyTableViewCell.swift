//
//  EmptyTableViewCell.swift
//  Sminq
//
//  Created by Pushkar Deshmukh on 25/06/18.
//  Copyright © 2018 Suraj Sagare. All rights reserved.
//

import UIKit

class EmptyTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
