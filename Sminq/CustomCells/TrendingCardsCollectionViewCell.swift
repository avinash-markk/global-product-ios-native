//
//  TrendingCardsCollectionViewCell.swift
//  Markk
//
//  Created by Avinash Thakur on 23/07/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import UIKit

enum TrendingCardType: Int {
    case placesRated, placesWithStickers, placesLiked, placesPopular, placesRatedBy, placesRatingsAddedBy
}

class TrendingCardsCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var cardIcon: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    
    @IBOutlet weak var cardIconWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var cardIconHeightConstraint: NSLayoutConstraint!
    
    var trendingCardType: TrendingCardType = TrendingCardType.placesRated
    
    var banner: Banner? {
        didSet {
            updateData()
        }
    }
     
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }
    
    func setupUI() {
        countLabel.font = UIFont(name: AppConstants.Font.primaryRegular, size: 48)
        countLabel.textColor = AppConstants.SminqColors.white100
        descLabel.font = UIFont(name: AppConstants.Font.primaryBold, size: 10)
        descLabel.textColor = AppConstants.SminqColors.white100.withAlphaComponent(0.75)
        self.addCornerRadiusAndBorderForUserNameLabel(value: 48 / 2)
        userNameLabel.font = UIFont(name: AppConstants.Font.primaryBold, size: 16)
        userNameLabel.textColor = AppConstants.SminqColors.white100
        userNameLabel.backgroundColor = AppConstants.SminqColors.green100
        self.contentView.backgroundColor = AppConstants.SminqColors.blackLight100
        self.contentView.layer.cornerRadius = 8.0
        self.contentView.layer.borderWidth = 1.0
        self.contentView.layer.borderColor = AppConstants.SminqColors.white100.withAlphaComponent(0.1).cgColor
        self.translatesAutoresizingMaskIntoConstraints = false
    }
    
    func setWidthHeightConstant(value: Int) {
        cardIconWidthConstraint.constant = CGFloat(value)
        cardIconHeightConstraint.constant = CGFloat(value)
    }
    
    public func setBanner(banner: Banner) {
        self.banner = banner
    }
    
    fileprivate func updateData() {
        guard let banner = banner else { return }
        
        countLabel.text = "\(banner.threshold)"
        descLabel.text = banner.label.uppercased()
        if let bannerType = banner.type {
            switch bannerType {
            case .totalDope:
                self.updateDataForTotalDope()
                break
            case .totalStickers:
                self.updateDataForStickers(banner: banner)
                break
            case .maxUserPlaces:
                self.updateDataForMaxPlacesRated(banner: banner)
                break
            case .maxUserRatings:
                self.updateDataForMaxRatingsAdded(banner: banner)
                break
            default: break
            }
        }
    }
    
    func updateDataForMaxPlacesRated(banner: Banner) {
        if let userData = banner.users.first {
            self.addCornerRadiusAndBorderForCardIcon(value: 48 / 2)
            self.setUserImage(userData: userData)
        }
        self.setWidthHeightConstant(value: 48)
    }
    
    func updateDataForMaxRatingsAdded(banner: Banner) {
        if let userData = banner.users.first {
            self.addCornerRadiusAndBorderForCardIcon(value: 48 / 2)
            self.setUserImage(userData: userData)
        }
        self.setWidthHeightConstant(value: 48)
    }
    
    func setUserImage(userData: UserDataDict) {
        self.cardIcon.image = nil
        if let url = URL(string: userData.imageUrl) {
            self.cardIcon.sd_setImage(with: url, placeholderImage: nil)
        }
        let imageLabelText = userData.getUserDisplayName()
        self.userNameLabel.text = "\(imageLabelText.prefix(1).uppercased())"
        self.hideUserNameLabel(value: false)
    }
    
    func updateDataForStickers(banner: Banner) {
        self.setWidthHeightConstant(value: 72)
        if let url = URL(string: banner.icon) {
            cardIcon.sd_setImage(with: url, placeholderImage: nil)
        }
        self.removeCornerRadiusForCardIcon()
        self.hideUserNameLabel(value: true)
    }
    
    func removeCornerRadiusForCardIcon() {
        cardIcon.layer.borderWidth = 0.0
        cardIcon.layer.borderColor = UIColor.clear.cgColor
        cardIcon.layer.cornerRadius = 0.0
    }
    
    func addCornerRadiusForCardIcon(value: CGFloat) {
        cardIcon.layer.cornerRadius = value
        cardIcon.clipsToBounds = true
    }
    
    func addCornerRadiusAndBorderForCardIcon(value: CGFloat) {
        cardIcon.layer.cornerRadius = value
        cardIcon.clipsToBounds = true
        cardIcon.layer.borderWidth = 1.0
        cardIcon.layer.borderColor = AppConstants.SminqColors.white100.withAlphaComponent(0.5).cgColor
    }
    
    func addCornerRadiusAndBorderForUserNameLabel(value: CGFloat) {
        userNameLabel.layer.cornerRadius = value
        userNameLabel.clipsToBounds = true
        userNameLabel.layer.borderWidth = 1.0
        userNameLabel.layer.borderColor = AppConstants.SminqColors.white100.withAlphaComponent(0.5).cgColor
    }
    
    func updateDataForTotalDope() {
        let moodMeta = 3.getMoodMeta()
        cardIcon.image = moodMeta.image
        self.setWidthHeightConstant(value: 64)
        self.removeCornerRadiusForCardIcon()
        self.hideUserNameLabel(value: true)
    }
    
    func hideUserNameLabel(value: Bool) {
        userNameLabel.isHidden = value
    }
    
}
