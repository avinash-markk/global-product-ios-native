//
//  ArchiveRatingsTableViewCell.swift
//  Markk
//
//  Created by Avinash Thakur on 17/06/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import UIKit
import SDWebImage

protocol ArchiveRatingsTableViewCellDelegate: NSObjectProtocol {
    func archiveRatingTappedAt(index: Int, archivedPlaces: ArchivePlacesRatings)
}

class ArchiveRatingsTableViewCell: UITableViewCell {

    weak var delegate: ArchiveRatingsTableViewCellDelegate?
    
    @IBOutlet weak var placeDetailSubView: UIView!
    @IBOutlet weak var placeName: UILabel!
    @IBOutlet weak var placeRatingIcon: UIImageView!
    @IBOutlet weak var archivesCollectionView: UICollectionView!
    @IBOutlet weak var archivesCollectionViewFlowLayout: UICollectionViewFlowLayout!
    
    var sectionPadding: CGFloat = 40 + 24
    var numberOfCellPerRow = 3
    
    var archiveRatings: [UserStory]? = []
    var archivePlaceRatings: ArchivePlacesRatings!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        if UIScreen.main.bounds.width > 320 {
            numberOfCellPerRow  = 4
        }
        self.initArchivesCollectionViewUI()
    }
    
    func initArchivesCollectionViewUI() {
        self.backgroundColor = AppConstants.SminqColors.blackDark100
        archivesCollectionView.backgroundColor = AppConstants.SminqColors.blackDark100
        placeDetailSubView.backgroundColor = AppConstants.SminqColors.blackDark100
        archivesCollectionView.register(UINib(nibName: "ArchiveRatingsCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ArchiveRatingsCollectionViewCell")
        archivesCollectionViewFlowLayout.scrollDirection = .vertical
        archivesCollectionViewFlowLayout.minimumInteritemSpacing = 0
        archivesCollectionViewFlowLayout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0)
        archivesCollectionView.isScrollEnabled = false
        archivesCollectionView.delegate = self
        archivesCollectionView.dataSource = self
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func reloadArchivedRatingsView() {
        self.archivesCollectionView.reloadData()
    }
    
    func setArchivesPLacesRatings(data: ArchivePlacesRatings) {
        self.archivePlaceRatings = data
        self.archiveRatings = data.archiveRatings
        self.setPlaceDetails()
        self.reloadArchivedRatingsView()
    }
    
    func setPlaceDetails() {
        guard let stories = self.archiveRatings, stories.count > 0 else { return }
        guard let story = stories.first, story.posts.count > 0 else { return }
        guard let feed = story.posts.first else { return }
        let placeMoodData = feed.moodScore.getMoodMeta()
        self.placeRatingIcon.image = placeMoodData.image
        
        var pName = ""
        if let placeName = self.archivePlaceRatings.archivePlace.name {
            pName = placeName
        }
        let locationName = SminqAPI.formattedAddressFromAddressComponentWithoutInitialDots(addressComponent: self.archivePlaceRatings.archivePlace.addressComponents)
        placeName.attributedText = self.getAttributedPlaceString(placeName: "\(pName)", placeAddress: "\(locationName)")
    }
    
    func getAttributedPlaceString(placeName: String, placeAddress: String) -> NSMutableAttributedString {
        
        let attributedString = NSMutableAttributedString(string: "\(placeName) \(placeAddress)", attributes: [
            .font: UIFont(name: AppConstants.Font.primaryRegular, size: 12.0)!,
            .foregroundColor: UIColor(red: 244.0 / 255.0, green: 242.0 / 255.0, blue: 242.0 / 255.0, alpha: 0.5),
            .kern: -0.09
            ])
        attributedString.addAttributes([
            .font: UIFont(name: AppConstants.Font.primaryRegular, size: 14.0)!,
            .foregroundColor: UIColor(red: 244.0 / 255.0, green: 242.0 / 255.0, blue: 242.0 / 255.0, alpha: 1.0)
            ], range: NSRange(location: 0, length: placeName.count))
        
        return attributedString
    }
    
    func setRatingsData(index: Int) -> URL? {
        guard let ratings = self.archiveRatings?[index] else { return nil }
        let posts = ratings.posts
        guard let feed = posts.first, let otherContext = feed.getOtherContext() else { return nil }
        if otherContext.video != "" {
            if let imageName = otherContext.image, let url = CloudinaryService.shared.generateCloudinaryVideoURL(folder: FIRRemoteConfigService.shared.getCloudinaryVideoFolder(), fileName: imageName) {
                return url
            }
        } else {
            if let imageName = otherContext.image, let url = CloudinaryService.shared.generateCloudinaryImageURL(folder: FIRRemoteConfigService.shared.getCloudinaryOriginalImageFolder(), fileName: imageName) {
                return url
            }
        }
        return nil
    }

}

extension ArchiveRatingsTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.archivePlaceRatings.archiveRatings?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ArchiveRatingsCollectionViewCell", for: indexPath) as? ArchiveRatingsCollectionViewCell else {
            return UICollectionViewCell()
        }
        if let imagUrl = self.setRatingsData(index: indexPath.row) {
                cell.ratingsImage.sd_setShowActivityIndicatorView(true)
                cell.ratingsImage.sd_setIndicatorStyle(.white)
                cell.ratingsImage.sd_setImage(with: imagUrl, placeholderImage: nil, options: SDWebImageOptions.retryFailed, completed: {
                    (image, error, cacheType, imageURL) -> Void in
                    cell.ratingsImage.sd_removeActivityIndicator()
                })
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let totalWidth = collectionView.bounds.size.width
        let dimension = CGFloat(Int(totalWidth) / numberOfCellPerRow)
        return CGSize.init(width: dimension, height: CGFloat(93))
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.delegate?.archiveRatingTappedAt(index: indexPath.row, archivedPlaces: archivePlaceRatings)
    }
    
}
