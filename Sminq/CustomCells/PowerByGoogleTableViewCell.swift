//
//  PowerByGoogleTableViewCell.swift
//  Sminq
//
//  Created by Pushkar Deshmukh on 05/03/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import UIKit

class PowerByGoogleTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.initUI()
    }
    
    
    // MARK: Private functions
    
    func initUI () {
        self.contentView.backgroundColor = AppConstants.SminqColors.blackLight100
    }
}
