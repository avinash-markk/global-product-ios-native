//
//  StreaksTableViewCell.swift
//  Markk
//
//  Created by Avinash Thakur on 14/06/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import UIKit

class StreaksTableViewCell: UITableViewCell {
    @IBOutlet weak var streaksView: UIView!
    @IBOutlet weak var streaksBar: UIView!
    
    var streakCenterY: CGFloat!
    var streakPointsArray: [CGPoint] = []
    var streaksLabelArray: [String] = []
    var streakTotalPoints = 5
    var streakSpacing: CGFloat = 0.0
    var largeCircleRadius: CGFloat = 8.0
    var smallCircleRadius: CGFloat = 4.0
    var fillTillPoints: Int = 0
    
    var streaksData: Streaks!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.initUI()
    }
    
    func initUI() {
        self.contentView.backgroundColor = AppConstants.SminqColors.blackDark100
        self.selectionStyle = .none
        self.isUserInteractionEnabled = false
    }
    
    func drawStreakLinePoints() {
        streaksBar.translatesAutoresizingMaskIntoConstraints = false
        streaksBar.backgroundColor = AppConstants.SminqColors.grey100
        self.layoutIfNeeded()
        streakCenterY = streaksBar.frame.height / 2
        streakPointsArray = self.divideStreaksViewIntoSubparts(startPoint: CGPoint(x: streaksBar.frame.origin.x, y: streakCenterY), endPoint: CGPoint(x: streaksBar.frame.size.width, y: streakCenterY), parts: (streakTotalPoints - 1))
        self.getSpaceBetweenTwoPoints()
        self.drawStreakPoints()
    }
    
    func drawStreakPoints() {
        for point in streakPointsArray {
            self.drawCircleWith(center: point, radius: largeCircleRadius, color: AppConstants.SminqColors.grey100)
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.drawStreakLinePoints()
    }
    
    func drawStreakLabels() {
        var startPoint = self.streaksData.currentStreakCount / 4
        startPoint = startPoint * 4
        for (index, item) in streakPointsArray.enumerated() {
            let newPoint = streaksBar.convert(item, to: self.contentView)
            let label = UILabel()
            label.font = UIFont.init(name: AppConstants.Font.primaryRegular, size: 10)
            label.textColor = AppConstants.SminqColors.white100.withAlphaComponent(0.75)
            let weekLabelText = startPoint == 1 ? "week" : "weeks"
            label.text = "\(startPoint) \(weekLabelText)"
            if index == 0 {
                label.textAlignment = NSTextAlignment.left
                label.frame =  CGRect(x: newPoint.x - largeCircleRadius, y: 25, width: streakSpacing, height: 15)
            } else if index == streakPointsArray.count - 1 {
                label.textAlignment = NSTextAlignment.right
                label.frame =  CGRect(x: newPoint.x - streakSpacing + largeCircleRadius, y: 25, width: streakSpacing, height: 15)
            } else {
                label.textAlignment = NSTextAlignment.center
                label.frame =  CGRect(x: newPoint.x - (streakSpacing/2), y: 25, width: streakSpacing, height: 15)
            }
            
            self.contentView.addSubview(label)
            startPoint = startPoint + 1
        }
    }
    
    func drawCircleWith(center: CGPoint, radius: CGFloat, color: UIColor) {
        let circlePath = UIBezierPath(arcCenter: center, radius: radius, startAngle: CGFloat(0), endAngle:CGFloat(Double.pi * 2), clockwise: true)
        
        let shapeLayer = CAShapeLayer()
        shapeLayer.path = circlePath.cgPath
        shapeLayer.fillColor = color.cgColor
        shapeLayer.strokeColor = color.cgColor
        shapeLayer.lineWidth = 1.0
        self.streaksBar.layer.addSublayer(shapeLayer)
    }
    
    func divideStreaksViewIntoSubparts(startPoint: CGPoint, endPoint: CGPoint, parts: Int) -> [CGPoint] {
        let ΔX = (endPoint.x - startPoint.x) / CGFloat(parts)
        let ΔY = (endPoint.y - startPoint.y) / CGFloat(parts)
        return (0...parts).map {
            CGPoint(x: startPoint.x + ΔX * CGFloat($0),
                    y: startPoint.y + ΔY * CGFloat($0))
        }
    }
    
    func addLine(fromPoint start: CGPoint, toPoint end:CGPoint, color: UIColor) {
        let line = CAShapeLayer()
        let linePath = UIBezierPath()
        linePath.move(to: start)
        linePath.addLine(to: end)
        line.path = linePath.cgPath
        line.strokeColor = color.cgColor
        line.lineWidth = 2
        line.lineJoin = kCALineJoinRound
        self.streaksBar.layer.addSublayer(line)
    }
    
    func getSpaceBetweenTwoPoints() {
        streakSpacing = streakPointsArray[1].x - streakPointsArray[0].x - (2 * largeCircleRadius)
    }
    
    func updateUserStreaksData(streak: Streaks) {
        self.streaksData = streak
        self.getStreakStartPoint()
    }
    
    func getStreakStartPoint() {
        self.drawStreakLabels()
        self.drawStreaksProgress()
        self.drawCurrentProgressStreakPoints()
    }
    
    func drawCurrentProgressStreakPoints() {
        for index in 0...(fillTillPoints) {
            let centerPoint = streakPointsArray[index]
            self.drawCircleWith(center: centerPoint, radius: smallCircleRadius, color: AppConstants.SminqColors.green100)
        }
    }
    
    func drawStreaksProgress() {
         fillTillPoints = self.streaksData.currentStreakCount % 4
        let p1 = streakPointsArray[0]
        let p2 = streakPointsArray[fillTillPoints]
        self.addLine(fromPoint: p1, toPoint: p2, color: AppConstants.SminqColors.green100)
        if self.streaksData.currentProgress == 1 {
           let p3 = self.getMidPointBetween(point1: p2, point2: streakPointsArray[fillTillPoints + 1])
            self.addLine(fromPoint: p2, toPoint: p3, color: AppConstants.SminqColors.green100)
        }
    }
    
    func getMidPointBetween(point1: CGPoint, point2: CGPoint) -> CGPoint {
        var midPoint = CGPoint()
        midPoint.x = (point1.x + point2.x) / 2
        midPoint.y = (point1.y + point2.y) / 2
        return midPoint
    }

}
