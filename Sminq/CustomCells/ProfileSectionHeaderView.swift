//
//  ProfileSectionHeaderView.swift
//  Markk
//
//  Created by Avinash Thakur on 13/06/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import UIKit

class ProfileSectionHeaderView: UIView {

    var contentView: UIView!
    @IBOutlet weak var tabsMenuView: TabsMenuView!

    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        contentView.frame = self.frame
        tabsMenuView.frame = self.frame
    }
    
    func xibSetup() {
        backgroundColor = UIColor.clear
        contentView = loadNib()
        contentView.frame = bounds
        addSubview(contentView)
    }
    
}
