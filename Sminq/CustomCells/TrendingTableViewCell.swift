//
//  TrendingTableViewCell.swift
//  Markk
//
//  Created by Avinash Thakur on 23/07/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import UIKit

protocol TrendingTableViewCellDelegate: NSObjectProtocol {
    func didSelectTrendingItemAt(indexPath: IndexPath, banner: Banner)
}

class TrendingTableViewCell: UITableViewCell {
    public weak var delegate: TrendingTableViewCellDelegate?
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var collectionViewFlowLayout: UICollectionViewFlowLayout!
    
    var bannerViewModel: BannerViewModel? {
        didSet {
            updateData()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupUI()
        self.initDelegates()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    fileprivate func setupUI() {
        contentView.backgroundColor = AppConstants.SminqColors.blackDark100
        self.selectionStyle = .none
        collectionView.backgroundColor = AppConstants.SminqColors.blackDark100
        self.registerCollectionViewNibs()
        collectionViewFlowLayout.scrollDirection = .horizontal
        collectionViewFlowLayout.sectionInset = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
        collectionView.showsVerticalScrollIndicator = false
        collectionView.showsHorizontalScrollIndicator = false
    }
    
    func registerCollectionViewNibs() {
        collectionView.register(UINib(nibName: "TrendingCardsCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "TrendingCardsCollectionViewCell")
        collectionView.register(UINib(nibName: "TrendingUnlocksCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "TrendingUnlocksCollectionViewCell")
        collectionView.register(UINib(nibName: "TrendingPlaceCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "TrendingPlaceCollectionViewCell")
        collectionView.register(UINib(nibName: "EmptyCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "EmptyCollectionViewCell")
    }
    
    fileprivate func initDelegates() {
        collectionView.delegate = self
        collectionView.dataSource = self
    }
    
    fileprivate func updateData() {
        guard let bannerViewModel = bannerViewModel else { return }
        
        collectionView.reloadData()
        collectionView.collectionViewLayout.invalidateLayout()
    }
    
    public func setBannerViewModel(bannerViewModel: BannerViewModel) {
        self.bannerViewModel = bannerViewModel
    }

}

extension TrendingTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let bannerViewModel = bannerViewModel, let bannerData = bannerViewModel.bannerData {
            return bannerData.banners.count
        }
        
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let bannerViewModel = bannerViewModel, let bannerData = bannerViewModel.bannerData {
            let banner = bannerData.banners[indexPath.row]
            
            if let bannerType = banner.type {
                switch bannerType {
                case .showcase, .maxLikes, .maxViews, .maxRatings:
                    return self.getTrendingPlaceCollectionViewCell(banner: banner, indexPath: indexPath)
                case .totalDope, .totalStickers, .maxUserRatings, .maxUserPlaces:
                    return self.getTrendingCardsCollectionViewCell(banner: banner, indexPath: indexPath)
                case .newBadge, .claimedReward:
                    return self.getTrendingUnlocksCardCollectionViewCell(banner: banner, indexPath: indexPath)
                }
            }
        }
        
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "EmptyCollectionViewCell", for: indexPath) as? EmptyCollectionViewCell else { return UICollectionViewCell() }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 232, height: 144)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let bannerViewModel = bannerViewModel, let bannerData = bannerViewModel.bannerData {
            let banner = bannerData.banners[indexPath.row]
            delegate?.didSelectTrendingItemAt(indexPath: indexPath, banner: banner)
        }
    }
    
    func getTrendingPlaceCollectionViewCell(banner: Banner, indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TrendingPlaceCollectionViewCell", for: indexPath) as? TrendingPlaceCollectionViewCell else { return UICollectionViewCell() }
        cell.placeDescButton.setTitle(banner.label.uppercased(), for: .normal)
        if let placeStoriesData = banner.places.first {
            cell.setPlace(place: placeStoriesData)
        }
        return cell
    }
    
    func getTrendingCardsCollectionViewCell(banner: Banner, indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TrendingCardsCollectionViewCell", for: indexPath) as? TrendingCardsCollectionViewCell else { return UICollectionViewCell() }
        cell.setBanner(banner: banner)
        return cell
    }
    
    func getTrendingUnlocksCardCollectionViewCell(banner: Banner, indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TrendingUnlocksCollectionViewCell", for: indexPath) as? TrendingUnlocksCollectionViewCell else { return UICollectionViewCell() }
        cell.setBanner(banner: banner)
        return cell
    }
    
}

