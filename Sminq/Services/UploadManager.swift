//
//  UploadManager.swift
//  Sminq
//
//  Created by Bjorn Mascarenhas on 05/09/18.
//  Copyright © 2018 Suraj Sagare. All rights reserved.
//

import UIKit
import CoreData
import Cloudinary

class UploadManager {
    
    var uploaderRequest = CLDUploadRequest()
    var operationsArray = NSMutableArray()
    static let sharedInstance = UploadManager()
    private init() {
    }
    
    func checkAndUploadNext() {
        if operationsArray.count > 0 {
            let (progressUpload, _) = self.checkIfAnyUploadIsInProgress()
            if progressUpload {
                return
            } else {
                let (pendingUpload, index) = self.getTheFirstPendingUpload()
                if pendingUpload {
                    if let newtTask = operationsArray.object(at: index) as? UploadTask {
                        self.addUploadTask(task: newtTask)
                    }
                } else {
                    DispatchQueue.main.async {
                        NotificationCenter.default.post(name: NSNotification.Name("Update_Message"), object: nil)
                    }
                }
            }
        }
    }
    
    func checkIfAnyUploadIsInProgress() -> (Bool, Int) {
        if operationsArray.count > 0 {
            for (index, item) in operationsArray.enumerated() {
                if let task = item as? UploadTask {
                    if task.status == "InProgress" {
                        return (true, index)
                    }
                }
            }
            return (false, 0)
        }
        return (false, 0)
    }
    
    func getTheFirstPendingUpload() -> (Bool, Int) {
        if operationsArray.count > 0 {
            for (index, item) in operationsArray.enumerated() {
                if let task = item as? UploadTask {
                    if task.status == "Pending" {
                        return (true, index)
                    }
                }
            }
            return (false, 0)
        }
        return (false, 0)
    }
    
    func checkIfAnyUploadFailed() -> Bool {
        if operationsArray.count > 0 {
            for item in operationsArray {
                if let task = item as? UploadTask {
                    if task.status == "Failed" {
                        return true
                    }
                }
            }
            return false
        }
        return false
    }
    
    func checkForItemsInUploadsList() -> Bool {
        if operationsArray.count > 0 {
            for item in operationsArray {
                if let task = item as? UploadTask {
                    if task.status == "Pending" || task.status == "InProgress" || task.status == "Failed" || task.status == "Completed" {
                        return true
                    }
                }
                return false
            }
        }
        return false
    }
    
    func addTaskToArrayOfUploads(task: UploadTask) {
        UserDefaults.standard.set(true, forKey: "UploadInProgress")
        UploadDatabaseService.shared.saveFeedToUpload(feedID: task.taskId!, feedData: task.questionsData!, feedPlaceId: task.placeIDString!, feedStatus: "Pending", feedSubPlaceID: task.subPlaceIDString!, feedIsVideo: task.isVideo ? "true" : "false", feedDuration: "0", feedPlaceName: task.placeName!, textListString: task.textListString!, stickerListString: task.stickerListString!)
        
        operationsArray.add(task)
        
        DispatchQueue.main.async {
            NotificationCenter.default.post(name: NSNotification.Name("Update_Message"), object: nil)
        }

        self.checkAndUploadNext()
    }
    
    func addUploadTask(task: UploadTask) {
        task.status = "InProgress"
        if task.isVideo == false {
            self.addImageUploadTask(task: task)
        } else {
            self.addVideoUploadTask(task: task)
        }
    }
    
    func addImageUploadTask(task: UploadTask) {
        UploadDatabaseService.shared.updateStatusForFeed(feedID: task.taskId!, newStatus: "InProgress")
        NotificationCenter.default.post(name: NSNotification.Name("Add_Upload_Process"), object: nil, userInfo: ["task": task])
        
        UploaderClass().uploadImageToCloudinary(imageTask: task, taskProgressHandler: { task in
            self.postProgressNotificationFor(task: task)
        }, taskCompletionHandler: { postTask, error  in
            if error == nil {
                UploaderClass().setDataForAddingPost(postTask: postTask)
            } else {
                self.postFailureNotificationFor(task: postTask, errorDesc: error!)
            }
        })
    }
    
    func addVideoUploadTask(task: UploadTask) {
        UploadDatabaseService.shared.updateStatusForFeed(feedID: task.taskId!, newStatus: "InProgress")
        NotificationCenter.default.post(name: NSNotification.Name("Add_Upload_Process"), object: nil, userInfo: ["task": task])
        
        UploaderClass().uploadVideoToCloudinary(videoTask: task, taskProgressHandler: { task in
            self.postProgressNotificationFor(task: task)
        }, taskCompletionHandler: { postTask, error  in
            if error == nil {
                UploaderClass().setDataForAddingPost(postTask: postTask)
            } else {
                self.postFailureNotificationFor(task: postTask, errorDesc: error!)
            }
        })
    }
    
    func postProgressNotificationFor(task: UploadTask) {
        DispatchQueue.main.async {
            NotificationCenter.default.post(name: NSNotification.Name("Update_Upload_Progress"), object: nil, userInfo: ["progress": task])
        }
    }
    
    func postFailureNotificationFor(task: UploadTask, errorDesc: String) {
        task.status = errorDesc == "cancelled" ? "Cancelled" : "Failed"
            DispatchQueue.main.async {
                NotificationCenter.default.post(name: NSNotification.Name(AppConstants.AppNotificationNames.uploadDone), object: nil, userInfo: ["task": task])
            }
    }
    
    func deleteAllRecords() {
        if  self.operationsArray.count > 0 {
            self.operationsArray.removeAllObjects()
        }
        UploadDatabaseService.shared.deleteAllRecords()
    }
    
    func cancelCurrentUpload() {
        self.uploaderRequest.cancel()
    }
    
    func suspendCurrentUpload() {
        self.uploaderRequest.suspend()
    }
    
    func resumeCurrentUpload() {
        self.uploaderRequest.resume()
    }

}
