//
//  ShareTemplateService.swift
//  Markk
//
//  Created by Pushkar Deshmukh on 20/08/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import UIKit
import CoreGraphics

class ShareTemplateService: NSObject {
    static let shared = ShareTemplateService()
    
    private override init() {}
    
    func createShareImageTemplate(post: UserDataPlaceFeed?, place: LivePlaces?, postImage: UIImage?) -> UIImage? {
        guard let place = place, let post = post else { return nil }
        let getMoodMeta = post.moodScore.getMoodMeta()
        let moodImage = getMoodMeta.imageWithBottomText
        
        if let postImage = postImage, let moodImage = moodImage {
            // NOTE: Canvas is created with 1080 x 1920 for standardisation.
            // Since this needs to be converted to pixel, the value is divided by device scale. Because ios uses point system for drawing
            let contextRect = CGRect(x: 0, y: 0, width: 1080 / UIScreen.main.scale, height: 1920 / UIScreen.main.scale)
            
            let mainImageWidthInPt: CGFloat = 375
            
            let gradientLayer = CAGradientLayer()
            
            gradientLayer.colors = getMoodMeta.gradientColors
            gradientLayer.bounds = contextRect
            gradientLayer.locations = [0, 0.5]
            UIGraphicsBeginImageContextWithOptions(contextRect.size, true, 0.0)
            let context = UIGraphicsGetCurrentContext()!
            gradientLayer.render(in: context)

            let postImageViewWidth: CGFloat = Helper().relativeUnit(sizeInPoint: mainImageWidthInPt, sizeInPixel: contextRect.width, howMuch: 244)
            let postImageViewHeight: CGFloat = Helper().relativeUnit(sizeInPoint: mainImageWidthInPt, sizeInPixel: contextRect.width, howMuch: 434)
            
            let postImageView = UIImageView.init(image: postImage)
            postImageView.contentMode = .scaleAspectFill
            postImageView.layer.cornerRadius = 8
            postImageView.clipsToBounds = true
            postImageView.bounds = CGRect.init(x: (contextRect.width / 2) - (postImageViewWidth / 2), y: (contextRect.height / 2) - (postImageViewHeight / 2), width: postImageViewWidth, height: postImageViewHeight)
            postImageView.layer.render(in: context)
            
            let moodImageWidth: CGFloat = Helper().relativeUnit(sizeInPoint: mainImageWidthInPt, sizeInPixel: contextRect.width, howMuch: 96)
            let moodImageHeight: CGFloat = Helper().relativeUnit(sizeInPoint: mainImageWidthInPt, sizeInPixel: contextRect.width, howMuch: 96)
            
            moodImage.draw(in: CGRect.init(x: (contextRect.width / 2) - (moodImageWidth / 2), y: postImageView.bounds.minY - ((moodImageHeight / 2)), width: moodImageWidth, height: moodImageHeight))
            
            let placeNameLabelFontSize = Helper().relativeUnit(sizeInPoint: mainImageWidthInPt, sizeInPixel: contextRect.width, howMuch: 20)
            let placeNameLabelHeight = Helper().relativeUnit(sizeInPoint: mainImageWidthInPt, sizeInPixel: contextRect.width, howMuch: 24)
            let placeNameLabelTopPadding = Helper().relativeUnit(sizeInPoint: mainImageWidthInPt, sizeInPixel: contextRect.width, howMuch: 8)
            
            let placeNameLabel = UILabel()
            placeNameLabel.text = place.name
            placeNameLabel.font = UIFont.init(name: AppConstants.Font.primaryBold, size: placeNameLabelFontSize)
            placeNameLabel.textDropShadow()
            placeNameLabel.textColor = AppConstants.SminqColors.white100
            placeNameLabel.textAlignment = .center
            placeNameLabel.bounds = CGRect.init(x: 16, y: postImageView.bounds.maxY + placeNameLabelTopPadding, width: contextRect.width - 32, height: placeNameLabelHeight)
            placeNameLabel.layer.draw(in: context)

            let placeAddressLabelFontSize = Helper().relativeUnit(sizeInPoint: mainImageWidthInPt, sizeInPixel: contextRect.width, howMuch: 14)
            let placeAddressLabelHeight = Helper().relativeUnit(sizeInPoint: mainImageWidthInPt, sizeInPixel: contextRect.width, howMuch: 20)
            
            let placeAddressLabel = UILabel()
            placeAddressLabel.text = "\(SminqAPI.formattedAddressFromAddressComponentWithoutInitialDots(addressComponent: place.addressComponents))"
            placeAddressLabel.font = UIFont.init(name: AppConstants.Font.primaryRegular, size: placeAddressLabelFontSize)
            placeAddressLabel.textDropShadow()
            placeAddressLabel.textColor = AppConstants.SminqColors.white100
            placeAddressLabel.textAlignment = .center
            placeAddressLabel.bounds = CGRect.init(x: 16, y: placeNameLabel.bounds.maxY, width: contextRect.width - 32, height: placeAddressLabelHeight)
            placeAddressLabel.layer.draw(in: context)
            
            let ratingLabelBackgroundViewWidth = Helper().relativeUnit(sizeInPoint: mainImageWidthInPt, sizeInPixel: contextRect.width, howMuch: 212)
            let ratingLabelBackgroundViewHeight = Helper().relativeUnit(sizeInPoint: mainImageWidthInPt, sizeInPixel: contextRect.width, howMuch: 28)
            let ratingLabelBackgroundViewTopPadding = Helper().relativeUnit(sizeInPoint: mainImageWidthInPt, sizeInPixel: contextRect.width, howMuch: 4)
            
            let ratingLabelBackgroundView = UIView()
            ratingLabelBackgroundView.layer.cornerRadius = ratingLabelBackgroundViewHeight / 2
            ratingLabelBackgroundView.clipsToBounds = true
            ratingLabelBackgroundView.bounds = CGRect.init(x: (contextRect.width / 2) - (ratingLabelBackgroundViewWidth / 2), y: placeAddressLabel.bounds.maxY + ratingLabelBackgroundViewTopPadding, width: ratingLabelBackgroundViewWidth, height: ratingLabelBackgroundViewHeight)
            ratingLabelBackgroundView.backgroundColor = AppConstants.SminqColors.blackDark100
            ratingLabelBackgroundView.layer.render(in: context)
            
            let ratingLabelFontSize = Helper().relativeUnit(sizeInPoint: mainImageWidthInPt, sizeInPixel: contextRect.width, howMuch: 12)
            
            let ratingLabel = UILabel()
            ratingLabel.layer.cornerRadius = 14
            ratingLabel.cornerRadius = 14
            ratingLabel.clipsToBounds = true
            
            let attributedString = NSMutableAttributedString(string: "rated \(getMoodMeta.text) on", attributes: [
                .font: UIFont(name: AppConstants.Font.primaryBold, size: ratingLabelFontSize)!,
                .foregroundColor: AppConstants.SminqColors.white100,
                .kern: -0.04
                ])
            
            let attributes: [NSAttributedStringKey: Any] = [
                NSAttributedStringKey.font: UIFont(name: AppConstants.Font.primaryBold, size: ratingLabelFontSize) as Any,
                NSAttributedStringKey.foregroundColor: AppConstants.SminqColors.yellow100 as Any
            ]
            
            let handleAttributedString = NSMutableAttributedString(string: " @getmarkk", attributes: attributes)
            attributedString.append(handleAttributedString)
            
            ratingLabel.attributedText = attributedString.uppercased()
            ratingLabel.textAlignment = .center
            ratingLabel.textDropShadow()
            ratingLabel.bounds = ratingLabelBackgroundView.bounds
            ratingLabel.layer.draw(in: context)

            if let result = UIGraphicsGetImageFromCurrentImageContext() {
                UIGraphicsEndImageContext()

                UIImageWriteToSavedPhotosAlbum(result, nil, nil, nil)

                return result
            }


        }
        
        return nil
    }
}
