//
//  UserDefaultsUtility.swift
//  Sminq
//
//  Created by Pushkar Deshmukh on 15/03/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import Foundation
import CoreLocation

class UserDefaultsUtility {
    private init() {
        
    }
    
    static let shared = UserDefaultsUtility()
    
    let userDefaults = UserDefaults.standard
    
    let isOnBoardingDoneKey = "IsOnBoardingDone"
    let authToken = "AppAuthToken"
    let kCLLocationAccuracyBestKey = "kCLLocationAccuracyBest"
    let kCLLocationAccuracyHundredMetersKey = "kCLLocationAccuracyHundredMeters"
    let lastKnownLocationKey = "lastKnownLocation"
    let hasDoneRatingKey = "HasDoneRatingKey"
    let lastStoryShareOptionSelectedKey = "lastStoryShareOptionSelectedKey"
    
    func setOnBoardingDone() {
        userDefaults.set(true, forKey: isOnBoardingDoneKey)
    }
    
    func getOnBoardingDoneFlag() -> Bool {
        return userDefaults.bool(forKey: isOnBoardingDoneKey)
    }
    
    func hasDoneRating() -> Bool {
        return userDefaults.bool(forKey: hasDoneRatingKey)
    }
    
    func setHasDoneRating() {
        userDefaults.set(true, forKey: hasDoneRatingKey)
    }
    
    func setAuthToken(token: String) {
        userDefaults.set(token, forKey: authToken)
    }
    
    func getAuthToken() -> String {
        let token = userDefaults.string(forKey: authToken)
        return token ?? ""
    }
    
    func removeAuthToken() {
        userDefaults.removeObject(forKey: authToken)
    }
    
    func setkCLLocationAccuracyBestLocation(location: CLLocation) {
        print("Best Accurate location: ", location.coordinate.latitude, ":", location.coordinate.longitude)
        userDefaults.set(location.toDictionary(), forKey: kCLLocationAccuracyBestKey)
    }
    
    func getkCLLocationAccuracyBestLocation() -> CLLocation? {
        guard let locationDict = userDefaults.object(forKey: kCLLocationAccuracyBestKey) as? NSDictionary,
            let location = locationDict.toCLLocation() else { return nil }
        return location
    }
    
    func setkCLLocationAccuracyHundredMeters(location: CLLocation) {
        print("Least Accurate location: ", location.coordinate.latitude, ":", location.coordinate.longitude)
        userDefaults.set(location.toDictionary(), forKey: kCLLocationAccuracyHundredMetersKey)
    }
    
    func getkCLLocationAccuracyHundredMeters() -> CLLocation? {
        guard let locationDict = userDefaults.object(forKey: kCLLocationAccuracyHundredMetersKey) as? NSDictionary,
            let location = locationDict.toCLLocation() else { return nil }
        return location
    }
    
    func setLastKnownLocation(location: CLLocation) {
        print("Last known location: ", location.coordinate.latitude, ":", location.coordinate.longitude)
        userDefaults.set(location.toDictionary(), forKey: lastKnownLocationKey)
    }
    
    func getLastKnownLocation() -> CLLocation? {
        guard let locationDict = userDefaults.object(forKey: lastKnownLocationKey) as? NSDictionary,
           let location = locationDict.toCLLocation() else { return nil }
        return location
    }
    
    func getHundredMeterLocationOtherwiseLastKnownLocation() -> (latitude: Double?, longitude: Double?) {
        var lat: Double?
        var long : Double?
        
        if let location = UserDefaultsUtility.shared.getkCLLocationAccuracyHundredMeters() {
            lat = location.coordinate.latitude
            long = location.coordinate.longitude
        } else if let location = UserDefaultsUtility.shared.getLastKnownLocation() {
            lat = location.coordinate.latitude
            long = location.coordinate.longitude
        }
        
        return (latitude: lat, longitude: long)
    }
    
    func setLastUsedShareApp(storyShareOption: StoryShareOption) {
        userDefaults.set(storyShareOption.toDictionary(), forKey: lastStoryShareOptionSelectedKey)
    }
    
    func getLastUsedShareApp() -> StoryShareOption? {
        guard let dict = userDefaults.object(forKey: lastStoryShareOptionSelectedKey) as? NSDictionary,
            let storyShareOption = dict.toStoryShareOption() else { return nil }
        return storyShareOption
    }
}
