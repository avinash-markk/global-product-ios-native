//
//  StoryViewManager.swift
//  Markk
//
//  Created by Pushkar Deshmukh on 19/06/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import Foundation

class StoryViewManager: NSObject {
    /// feedItemId: If this feedItemId is set, then the feed list is filtered and only the matching id is returned!
    /// This is used in notification screen to filter out list.
    public var feedItemId: String?
    
    public var screen: String = ""
    public var section = ""
    public var isCompletelyVisible: Bool = false
    public var snappedIndex: Int = 0
    
    init(screen: String, feedItemId: String? = nil) {
        self.screen = screen
        self.feedItemId = feedItemId
    }
    
}
