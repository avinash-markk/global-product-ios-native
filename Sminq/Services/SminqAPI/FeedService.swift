//
//  FeedService.swift
//  Sminq
//
//  Created by SMINQ iOS on 22/03/18.
//  Copyright © 2018 Pushkar Deshmukh. All rights reserved.
//

import Foundation

import Alamofire

class FeedService {
    var baseURL: String
    var apiURL: String
    
    required init() {
        switch ApiConfig.appEnvironment {
        case .development:
            self.baseURL = UIApplication.shared.getDevelopmentGatewayProxyURL()
            self.apiURL = ApiConfig.queueingAPIEndPoint + "/v1"
        case .staging:
            self.baseURL = UIApplication.shared.getStagingGatewayProxyURL()
            self.apiURL = ApiConfig.queueingAPIEndPoint + "/staging"
        case .production:
            self.baseURL = UIApplication.shared.getProductionGatewayProxyURL()
            self.apiURL = ApiConfig.queueingAPIEndPoint + "/v1"
        }
    }
    
    func upvote(feedItemsId: String, userId: String, status: Int) -> (response: DataRequest, requestPayload: Parameters, requestMethod: HTTPMethod, requestPathParams: [String: Any]) {
        let payload: Parameters = ["userId": userId, "status": status, "postId": feedItemsId]
        
        LogUtil.infoAPIPayload(apiName: AppConstants.apiNames.upvotePost, payload: payload, pathParams: [:], requestMethod: AppConstants.requestMethods.post)
        
        return (response: AFManager.request(self.upvoteAPIEndPoint(), method: .post, parameters: payload, encoding: JSONEncoding.default, headers: SminqAPI.headers()), requestPayload: payload, requestMethod: HTTPMethod.post, requestPathParams: [:])
    }
    
    func downvote(feedItemsId: String, userId: String, status: Int, reason: String) -> (response: DataRequest, requestPayload: Parameters, requestMethod: HTTPMethod, requestPathParams: [String: Any]) {
        let payload: Parameters = ["userId": userId, "status": status, "reason": reason, "postId": feedItemsId]
        
        LogUtil.infoAPIPayload(apiName: AppConstants.apiNames.downvotePost, payload: payload, pathParams: [:], requestMethod: AppConstants.requestMethods.post)
        
        return (response: AFManager.request(self.downvoteAPIEndPoint(), method: .post, parameters: payload, encoding: JSONEncoding.default, headers: SminqAPI.headers()), requestPayload: payload, requestMethod: HTTPMethod.post, requestPathParams: [:])
    }
    
    func deleteFeed(feedItemsId: String) -> (response: DataRequest, requestPayload: Parameters, requestMethod: HTTPMethod, requestPathParams: [String: Any]) {
        let payload: Parameters = ["userId": Helper().getUserId(), "postId": feedItemsId]
        
        LogUtil.infoAPIPayload(apiName: AppConstants.apiNames.deletePost, payload: payload, pathParams: [:], requestMethod: AppConstants.requestMethods.delete)
        
        return (response: AFManager.request(self.deleteFeedAPIEndPoint(), method: .delete, parameters: payload, encoding: JSONEncoding.default, headers: SminqAPI.headers()), requestPayload: [:], requestMethod: HTTPMethod.delete, requestPathParams: [:])
    }
    
    func replyOnFeed(feedItemsId: String, userId: String, defaultSubPlace: String, input: String, tags: [Tags]) -> (response: DataRequest, requestPayload: Parameters, requestMethod: HTTPMethod, requestPathParams: [String: Any]) {
        let testListArray: [[String: Any]] = [["text": input, "tags": tags.toJSONArray() as Any]]
        
        let payload: Parameters = ["userId": userId, "subPlaceId": defaultSubPlace, "input": input, "mediaMetadata": ["textList": testListArray], "postId": feedItemsId]
        
        LogUtil.infoAPIPayload(apiName: AppConstants.apiNames.addComment, payload: payload, pathParams: [:], requestMethod: AppConstants.requestMethods.post)
        
        return (response: AFManager.request(self.replyOnFeedAPIEndPoint(), method: .post, parameters: payload, encoding: JSONEncoding.default, headers: SminqAPI.headers()), requestPayload: payload, requestMethod: HTTPMethod.post, requestPathParams: [:])
    }
    
    func deleteComment(feedItemsId: String, replyId: String, userId: String) -> (response: DataRequest, requestPayload: Parameters, requestMethod: HTTPMethod, requestPathParams: [String: Any]) {
        let payload: Parameters = ["userId": userId, "postId": feedItemsId, "replyId": replyId]
        
        LogUtil.infoAPIPayload(apiName: AppConstants.apiNames.deleteComment, payload: payload, pathParams: [:], requestMethod: AppConstants.requestMethods.delete)
        
        return (response: AFManager.request(self.deleteCommentAPIEndPoint(), method: .delete, parameters: payload, encoding: JSONEncoding.default, headers: SminqAPI.headers()), requestPayload: payload, requestMethod: HTTPMethod.delete, requestPathParams: [:])
    }
    
    func shareFeedItem(userId: String, medium: String, feedItemsId: String) -> (response: DataRequest, requestPayload: Parameters, requestMethod: HTTPMethod, requestPathParams: [String: Any]) {
        let payload: Parameters = ["userId": userId, "medium": medium, "postId": feedItemsId]
        
        LogUtil.infoAPIPayload(apiName: AppConstants.apiNames.sharePost, payload: payload, pathParams: [:], requestMethod: AppConstants.requestMethods.post)
        
        return (response: AFManager.request(self.shareFeedItemAPIEndPoint(), method: .post, parameters: payload, encoding: JSONEncoding.default, headers: SminqAPI.headers()), requestPayload: payload, requestMethod: HTTPMethod.post, requestPathParams: [:])
    }
    
    func addPost(params: NSDictionary) -> (response: DataRequest, requestPayload: Parameters, requestMethod: HTTPMethod, requestPathParams: [String: Any]) {
        let payload: Parameters = ["geoPoint": params.value(forKeyPath: "geoPoint") as Any, "place": params.value(forKeyPath: "place") as Any, "user": params.value(forKeyPath: "user") as Any, "verified": params.value(forKeyPath: "verified") as Any, "feedInputs": params.value(forKeyPath: "feedInputs") as Any, "subPlace": params.value(forKeyPath: "subPlace") as Any, "moodScore": params.value(forKey: "moodScore") as! String, "locationAccuracy": params.value(forKey: "locationAccuracy") as Any]
        
        LogUtil.infoAPIPayload(apiName: AppConstants.apiNames.addPost, payload: payload, pathParams: [:], requestMethod: AppConstants.requestMethods.post)
        
        return (response: AFManager.request(self.addPostAPIEndPoint(), method: .post, parameters: payload, encoding: JSONEncoding.default, headers: SminqAPI.headers()), requestPayload: payload, requestMethod: HTTPMethod.post, requestPathParams: [:])
    }
    
    func postMetrics(storyViewList: [StoryViewData]) -> (response: DataRequest, requestPayload: Parameters, requestMethod: HTTPMethod, requestPathParams: [String: Any]) {
        let payload: Parameters = ["events": storyViewList.toJSONArray() as Any]
        
        LogUtil.infoAPIPayload(apiName: AppConstants.apiNames.viewPostCount, payload: payload, pathParams: [:], requestMethod: AppConstants.requestMethods.post)
        
        return (response: AFManager.request(self.postMetricsAPIEndPoint(), method: .post, parameters: payload, encoding: JSONEncoding.default, headers: SminqAPI.headers()), requestPayload: payload, requestMethod: HTTPMethod.post, requestPathParams: [:])
    }
    
    // MARK: Functions to get API end points
    
    func upvoteAPIEndPoint() -> String {
        return self.baseURL + "/v1/posts/upvotes"
    }
    
    func downvoteAPIEndPoint() -> String {
        return self.baseURL + "/v1/posts/downvotes"
    }
    
    func deleteFeedAPIEndPoint() -> String {
        return self.baseURL + "/v1/posts"
    }
    
    func replyOnFeedAPIEndPoint() -> String {
        return self.baseURL + "/v1/posts/comments"
    }
    
    func deleteCommentAPIEndPoint() -> String {
        return self.baseURL + "/v1/posts/comments"
    }
    
    func shareFeedItemAPIEndPoint() -> String {
        return self.baseURL + "/v1/posts/shares"
    }
    
    func addPostAPIEndPoint() -> String {
        return self.baseURL + "/v1/posts"
    }
    
    func postMetricsAPIEndPoint() -> String {
        return self.apiURL + "/posts/views"
    }
   
}
