import Foundation
import Alamofire
import Gloss
import Firebase

class PushNotificationAPI {
    
    class func registerForSminqPushNotifications(userId: String, fcmToken: String, register: String) {
        var baseURL: String = ""
        
        switch ApiConfig.appEnvironment {
        case .development:
            baseURL = UIApplication.shared.getDevelopmentGatewayProxyURL()
        case .staging:
            baseURL = UIApplication.shared.getStagingGatewayProxyURL()
        case .production:
            baseURL = UIApplication.shared.getProductionGatewayProxyURL()
        }
        
        let param = [
            "registrationToken": fcmToken, "deviceType": "ios", "userId": userId
            ] as [String: Any]
        
        let apiURL: String = baseURL + "/v1/notifications/" + register
        
        guard let metric = HTTPMetric(url: URL(string: apiURL)!, httpMethod: .put) else { return }
        metric.start()
        
        if register == AppConstants.enablePush {
            LogUtil.infoAPIPayload(apiName: AppConstants.apiNames.registerForNotifications, payload: param, pathParams: [:], requestMethod: AppConstants.requestMethods.put)
        } else {
            LogUtil.infoAPIPayload(apiName: AppConstants.apiNames.deregisterForNotifications, payload: param, pathParams: [:], requestMethod: AppConstants.requestMethods.put)
        }
        
        AFManager.request(apiURL, method: .put, parameters: param, encoding: JSONEncoding.default, headers: SminqAPI.headers())
            .validate(statusCode: 200..<300)
            .responseJSON { response in
//                debugPrint(response)
                
                switch response.result {
                case .success:
                    break
                    
                case .failure(let error):
                    LogUtil.error(error)
                    
                    let error = error as NSError
                    
                    let userInfo = error.userInfo
                    if let errorDesc = userInfo.valueForKeyPath(keyPath: "NSLocalizedDescription") as? String {
                        if register == AppConstants.enablePush {
                            AnalyticsHelper.APIFailureEvent(screen: AppConstants.screenNames.login, apiName: AppConstants.apiNames.registerForNotifications, errorMessage: errorDesc, userId: "", errorCode: nil, requestPayload: param, requestMethod: AppConstants.requestMethods.put, requestPathParams: [:])
                        } else if register == AppConstants.disablePush {
                            AnalyticsHelper.APIFailureEvent(screen: AppConstants.screenNames.profile, apiName: AppConstants.apiNames.deregisterForNotifications, errorMessage: errorDesc, userId: "", errorCode: nil, requestPayload: param, requestMethod: AppConstants.requestMethods.put, requestPathParams: [:])
                        }
                
                        return
                    }
                    break
                }
                
               
                
                metric.stop()
        }
    }
    
    class func timeAgoSinceDate(_ date: Date, numericDates: Bool = false) -> String {
        let calendar = NSCalendar.current
        let unitFlags: Set<Calendar.Component> = [.minute, .hour, .day, .weekOfYear, .month, .year, .second]
//        let unitFlags: Set<Calendar.Component> = [.minute, .hour, .second]
        let now = Date()
        let earliest = now < date ? now : date
        let latest = (earliest == now) ? date : now
        let components = calendar.dateComponents(unitFlags, from: earliest, to: latest)
        
        if components.year! >= 2 {
            return "\(components.year!) years ago"
        } else if components.year! >= 1 {
            if numericDates {
                return "1 year ago"
            } else {
                return "last year"
            }
        } else if components.month! >= 2 {
            return "\(components.month!) months ago"
        } else if components.month! >= 1 {
            if numericDates {
                return "1 month ago"
            } else {
                return "last month"
            }
        } else if components.weekOfYear! >= 2 {
            return "\(components.weekOfYear!) weeks ago"
        } else if components.weekOfYear! >= 1 {
            if numericDates {
                return "1 week ago"
            } else {
                return "last week"
            }
        } else if components.day! >= 2 {
            return "\(components.day!) days ago"
        } else if components.day! >= 1 {
            if numericDates {
                return "1 day ago"
            } else {
                return "Yesterday"
            }
        } else if components.hour! >= 2 {
            return "\(components.hour!) hours ago"
        } else if components.hour! >= 1 {
            if numericDates {
                return "1 hour ago"
            } else {
                return "1 hour ago"
            }
        } else if components.minute! >= 2 {
            return "\(components.minute!) mins ago"
        } else if components.minute! >= 1 {
            if numericDates {
                return "1 min ago"
            } else {
                return "1 min ago"
            }
        } else if components.second! >= 3 {
            return "\(components.second!) secs ago"
        } else {
            return "Just now"
        }
    }
    
}
