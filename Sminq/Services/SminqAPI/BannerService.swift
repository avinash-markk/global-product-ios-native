//
//  BannerService.swift
//  Sminq
//
//  Created by Pushkar Deshmukh on 14/03/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import Foundation
import Alamofire

class BannerService {
    var baseURL: String
    var day: Int?
    
    required init() {
        switch ApiConfig.appEnvironment {
        case .development:
            baseURL = UIApplication.shared.getDevelopmentGatewayProxyURL()
            day = 0
        case .staging:
            baseURL = UIApplication.shared.getStagingGatewayProxyURL()
        case .production:
            baseURL = UIApplication.shared.getProductionGatewayProxyURL()
        }
    }
    
    func getBanners() -> (response: DataRequest, requestPayload: Parameters, requestMethod: HTTPMethod, requestPathParams: [String: Any]) {
        LogUtil.infoAPIPayload(apiName: AppConstants.apiNames.getTrends, payload: [:], pathParams: [:], requestMethod: AppConstants.requestMethods.get)
        var payload: Parameters = [:]
        if let day = day {
            payload["day"] = day // TODO: Please remove this for production
        }
        return (response: AFManager.request(self.getBannersAPIEndPoint(), method: .get, parameters: payload, encoding: URLEncoding.default, headers: SminqAPI.headers()), requestPayload: payload, requestMethod: HTTPMethod.get, requestPathParams: [:])
    }
    
    // MARK: Functions to get API end points
    
    func getBannersAPIEndPoint() -> String {
        return self.baseURL + "/v1/trends"
    }
    
}
