//
//  UserService.swift
//  Sminq
//
//  Created by SMINQ iOS on 22/03/18.
//  Copyright © 2018 Pushkar Deshmukh. All rights reserved.
//

import Foundation
import Alamofire
import GooglePlaces

class UserService {
    var baseURL: String
    
    required init() {
        switch ApiConfig.appEnvironment {
        case .development:
            baseURL = UIApplication.shared.getDevelopmentGatewayProxyURL()
        case .staging:
            baseURL = UIApplication.shared.getStagingGatewayProxyURL()
        case .production:
            baseURL = UIApplication.shared.getProductionGatewayProxyURL()
        }
    }
        
    func getUserDetails(userDisplayName: String, latitude: Double?, longitude: Double?) -> (response: DataRequest, requestPayload: Parameters, requestMethod: HTTPMethod, requestPathParams: [String: Any]) {
        let payload: Parameters = [
            "displayName": userDisplayName,
            "latitude": latitude!,
            "longitude": longitude!
        ]
    
        LogUtil.infoAPIPayload(apiName: AppConstants.apiNames.getUserDetails, payload: payload, pathParams: [:], requestMethod: AppConstants.requestMethods.get)
        
        return (response: AFManager.request(self.getUserDetailsAPIEndPoint(user: userDisplayName), method: .get, parameters: payload, encoding: URLEncoding.default, headers: SminqAPI.headers()), requestPayload: payload, requestMethod: HTTPMethod.get, requestPathParams: [:])
    }
    
    func getUserUnlockedLevels() -> (response: DataRequest, requestPayload: Parameters, requestMethod: HTTPMethod, requestPathParams: [String: Any]) {
        
        LogUtil.infoAPIPayload(apiName: AppConstants.apiNames.getUnlockedLevel, payload: [:], pathParams: [:], requestMethod: AppConstants.requestMethods.get)
        
        return (response: AFManager.request(self.getUnlockedLevelAPIEndPoint(), method: .get, parameters: [:], encoding: URLEncoding.default, headers: SminqAPI.headers()), requestPayload: [:], requestMethod: HTTPMethod.get, requestPathParams: [:])
    }
    
    func getOwnUserDetails() -> (response: DataRequest, requestPayload: Parameters, requestMethod: HTTPMethod, requestPathParams: [String: Any]) {
        LogUtil.infoAPIPayload(apiName: AppConstants.apiNames.getOwnUserDetails, payload: [:], pathParams: [:], requestMethod: AppConstants.requestMethods.get)
        
        return (response: AFManager.request(self.getOwnUserDetailsAPIEndPoint(), method: .get, parameters: [:], encoding: URLEncoding.default, headers: SminqAPI.headers()), requestPayload: [:], requestMethod: HTTPMethod.get, requestPathParams: [:])
    }
    
    func editUser(userId: String, name: String?, displayName: String?, email: String?, mobile: String?, latitude: CLLocationDegrees?, longitude: CLLocationDegrees?, countryName: String?, cityName: String?, countryCode: String?, gender: String?, birthDate: String?, isSignUp: Int, isExistingUser: Bool, bio: String?, image: String?) -> (response: DataRequest, requestPayload: Parameters, requestMethod: HTTPMethod, requestPathParams: [String: Any]) {
        var payload: Parameters = [
            "isSignUp": isSignUp,
            "isExistingUser": isExistingUser,
            "id": userId
        ]
        
        if let name = name {
            payload["name"] = name
        }
        if let displayName = displayName {
            payload["displayName"] = [displayName]
        }
        if let email = email {
            payload["email"] = email
        }
        
        if let mobile = mobile, mobile != "" {
            payload["mobile"] = mobile
        }
        
        if let gender = gender, gender != "" {
            payload["gender"] = gender
        }
        
        if let birthDate = birthDate, birthDate != ""  {
            payload["birthDate"] = birthDate
        }
        
        if let countryCode = countryCode {
            payload["countryCode"] = countryCode
        }
        if let bio = bio, bio != "" {
            payload["bio"] = bio
        }
        if let image = image, image != "" {
            payload["imageUrl"] = image
        }
        
        var cityObject: Parameters = [:]
        
        if let cityName = cityName, cityName != "", let latitude = latitude,latitude != 0, let longitude = longitude, longitude != 0 {
            
            cityObject["latitude"] = latitude
            cityObject["longitude"] = longitude
            cityObject["name"] = cityName
            
            if let countryName = countryName, countryName != "" {
                cityObject["countryName"] = countryName
            }
            
            if let countryCode = countryCode, let mobile = mobile, mobile != "" {
                cityObject["code"] = countryCode
            }
        }
        
        if !cityObject.isEmpty {
            payload["city"] = cityObject
        }
        
        
        let requestPathParams: [String: Any] = [:]
        
        print(payload)
        
        LogUtil.infoAPIPayload(apiName: AppConstants.apiNames.updateUserProfile, payload: payload, pathParams: requestPathParams, requestMethod: AppConstants.requestMethods.put)
        
        return (response: AFManager.request(self.editUserAPIEndPoint(userId: userId), method: .put, parameters: payload, encoding: JSONEncoding.default, headers: SminqAPI.headers()), requestPayload: payload, requestMethod: HTTPMethod.put, requestPathParams: requestPathParams)
    }
    
    func verifyDisplayName(userId: String, displayName: String) -> (response: DataRequest, requestPayload: Parameters, requestMethod: HTTPMethod, requestPathParams: [String: Any]) {
        let payload: Parameters = ["userId": userId, "displayName": displayName]
        
        LogUtil.infoAPIPayload(apiName: AppConstants.apiNames.verifyDisplayName, payload: payload, pathParams: [:], requestMethod: AppConstants.requestMethods.post)
        
        return (response: AFManager.request(self.verifyDisplayNameAPIEndPoint(), method: .post, parameters: payload, encoding: JSONEncoding.default, headers: SminqAPI.headers()), requestPayload: payload, requestMethod: HTTPMethod.post, requestPathParams: [:])
    }
    
    func getUserTimeline(userId: String, latitude: Double?, longitude: Double?, distance: String) -> (response: DataRequest, requestPayload: Parameters, requestMethod: HTTPMethod, requestPathParams: [String: Any]) {
        var payload: Parameters = [:]
        
        if let lat = latitude, let long = longitude {
            payload["latitude"] = lat
            payload["longitude"] = long
        }
        
        if distance != "" {
            payload["distance"] = distance
        }
        
//        let requestPathParams: [String: Any] = ["userId": userId]
        
        LogUtil.infoAPIPayload(apiName: AppConstants.apiNames.getUserTimeline, payload: payload, pathParams: [:], requestMethod: AppConstants.requestMethods.get)
        
        return (response: AFManager.request(self.getUserTimelineAPIEndPoint(), method: .get, parameters: payload, encoding: URLEncoding.default, headers: SminqAPI.headers()), requestPayload: payload, requestMethod: .get, requestPathParams: [:])
        
    }
    
    func getArchivedStories(displayName: String) -> (response: DataRequest, requestPayload: Parameters, requestMethod: HTTPMethod, requestPathParams: [String: Any]) {
        let payload: [String: Any] = ["displayName": displayName]
        
        LogUtil.infoAPIPayload(apiName: AppConstants.apiNames.getUserArchivePosts, payload: payload, pathParams: [:], requestMethod: AppConstants.requestMethods.get)
        
        return (response: AFManager.request(self.getArchivedStoriesAPIEndPoint(), method: .get, parameters: payload, encoding: URLEncoding.default, headers: SminqAPI.headers()), requestPayload: payload, requestMethod: HTTPMethod.get, requestPathParams: [:])
    }
    
    func claimRewards(userID: String, emailID: String) -> (response: DataRequest, requestPayload: Parameters, requestMethod: HTTPMethod, requestPathParams: [String: Any]) {
        let payload: [String: Any] = ["user": userID, "email": emailID]
        
        LogUtil.infoAPIPayload(apiName: AppConstants.apiNames.claimRewards, payload: payload, pathParams: [:], requestMethod: AppConstants.requestMethods.post)
        
        return (response: AFManager.request(self.claimRewardsAPIEndPoint(), method: .post, parameters: payload, encoding: JSONEncoding.default, headers: SminqAPI.headers()), requestPayload: payload, requestMethod: HTTPMethod.post, requestPathParams: [:])
    }
    
    
    // MARK: Functions to get API end points
    
    func verifyDisplayNameAPIEndPoint() -> String {
        return self.baseURL + "/v1/users/verify"
    }
    
    func getUserTimelineAPIEndPoint() -> String {
        return self.baseURL + "/v1/timeline"
    }
    
    func getUserDetailsAPIEndPoint(user: String) -> String {
        return self.baseURL + "/v1/users/profile"
    }
    
    func getOwnUserDetailsAPIEndPoint() -> String {
        return self.baseURL + "/v1/users"
    }
    
    func editUserAPIEndPoint(userId: String) -> String {
        return self.baseURL + "/v1/users"
    }
    
    func getArchivedStoriesAPIEndPoint() -> String {
        return self.baseURL + "/v1/users/archive"
    }
    
    func claimRewardsAPIEndPoint() -> String {
        return self.baseURL + "/v1/badges/reward"
    }
    
    func getUnlockedLevelAPIEndPoint() -> String {
        return self.baseURL + "/v1/badges/reward"
    }
    
}
