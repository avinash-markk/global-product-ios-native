//
//  MiscService.swift
//  Sminq
//
//  Created by SMINQ iOS on 24/03/18.
//  Copyright © 2018 Pushkar Deshmukh. All rights reserved.
//

import Foundation
import Alamofire
import FirebaseRemoteConfig

class MiscService {
    var GOOGLE_API_KEY: String
    
    required init() {
        switch ApiConfig.appEnvironment {
        case .staging, .development:
            self.GOOGLE_API_KEY = UIApplication.shared.getStagingGoogleAPIKey()
        case .production:
            self.GOOGLE_API_KEY = UIApplication.shared.getProductionGoogleAPIKey()
        }
    }
    
    func getPlaceDetails(placeId: String) -> (response: DataRequest, requestPayload: Parameters, requestMethod: HTTPMethod, requestPathParams: [String: Any]) {
        let payload: Parameters = ["placeid": placeId, "key": self.GOOGLE_API_KEY]
        
        LogUtil.infoAPIPayload(apiName: AppConstants.apiNames.getGooglePlaceDetails, payload: payload, pathParams: [:], requestMethod: AppConstants.requestMethods.get)
        
        return (response: AFManager.request(self.getPlaceDetailsAPIEndPoint(), method: .get, parameters: payload, encoding: URLEncoding.default, headers: [:]), requestPayload: payload, requestMethod: HTTPMethod.get, requestPathParams: [:])
    }
    
    // MARK: Functions to get API end points
    
    func getPlaceDetailsAPIEndPoint() -> String {
        return ApiConfig.mapsGoogleAPI + "/maps/api/place/details/json"
    }
    
}
