//
//  PlaceService.swift
//  Sminq
//
//  Created by SMINQ iOS on 19/03/18.
//  Copyright © 2018 Suraj Sagare. All rights reserved.
//

import Foundation
import Alamofire

class PlaceService {
    var baseURL: String
    
    required init() {
        switch ApiConfig.appEnvironment {
        case .development:
            baseURL = UIApplication.shared.getDevelopmentGatewayProxyURL()
        case .staging:
            baseURL = UIApplication.shared.getStagingGatewayProxyURL()
        case .production:
            baseURL = UIApplication.shared.getProductionGatewayProxyURL()
        }
    }
    
    func getPlaceDetail(placeId: String, userId: String) -> (response: DataRequest, requestPayload: Parameters, requestMethod: HTTPMethod, requestPathParams: [String: Any]) {
        let payload: Parameters = ["identity": placeId, "userId": userId]
        
        LogUtil.infoAPIPayload(apiName: AppConstants.apiNames.getPlaceDetail, payload: payload, pathParams: [:], requestMethod: AppConstants.requestMethods.post)
        
        return (response: AFManager.request(self.getPlaceDetailAPIEndPoint(), method: .post, parameters: payload, encoding: JSONEncoding.default, headers: SminqAPI.headers()), requestPayload: payload, requestMethod: HTTPMethod.post, requestPathParams: [:])
    }
    
    func getStories(userId: String, city: String, sort: String, limit: Int?, latitude: Double?, longitude: Double?, postType: String, distance: String, placeGroup: String) -> (response: DataRequest, requestPayload: Parameters, requestMethod: HTTPMethod, requestPathParams: [String: Any]) {
        var payload: Parameters = ["userId": userId, "city": city, "sortOrder": sort, "distance": distance]
        
        if let lat = latitude, let long = longitude {
            payload["latitude"] = lat
            payload["longitude"] = long
        }
        
        if postType != "" {
            payload["postType"] = postType
        }
        
        if limit != nil {
            payload["limit"] = limit
        }
        
        if placeGroup != "" {
            payload["storyGroup"] = placeGroup
        }
        
        LogUtil.infoAPIPayload(apiName: AppConstants.apiNames.getLiveStoriesList, payload: payload, pathParams: [:], requestMethod: AppConstants.requestMethods.get)
        
        return (response: AFManager.request(self.getStoriesAPIEndPoint(), method: .get, parameters: payload, encoding: URLEncoding.default, headers: SminqAPI.headers()), requestPayload: payload, requestMethod: HTTPMethod.get, requestPathParams: [:])
    }
    
    func getStory(userId: String, placeId: String, viewerId: String, latitude: Double?, longitude: Double?, archive: Bool) -> (response: DataRequest, requestPayload: Parameters, requestMethod: HTTPMethod, requestPathParams: [String: Any]) {
        var payload: Parameters = ["placeId": placeId, "viewerId": viewerId]
        
        if let lat = latitude, let long = longitude {
            payload["latitude"] = lat
            payload["longitude"] = long
        }
        
        // NOTE: Encoding converts Bool to 1/0, hence it's converted to String 
        if archive {
            payload["archive"] = "true"
        } else {
            payload["archive"] = "false"
        }
        
        let requestPathParams: [String: Any] = ["userId": userId]
        
        LogUtil.infoAPIPayload(apiName: AppConstants.apiNames.getStoryDetails, payload: payload, pathParams: requestPathParams, requestMethod: AppConstants.requestMethods.get)
        
        return (response: AFManager.request(self.getStoryAPIEndPoint(userId: userId), method: .get, parameters: payload, encoding: URLEncoding.default, headers: SminqAPI.headers()), requestPayload: payload, requestMethod: HTTPMethod.get, requestPathParams: requestPathParams)
    }
    
    func getPlaceStories(placeId: String, latitude: Double?, longitude: Double?, userId: String) -> (response: DataRequest, requestPayload: Parameters, requestMethod: HTTPMethod, requestPathParams: [String: Any]) {
        var payload: Parameters = ["userId": userId]
        
        if let lat = latitude, let long = longitude {
            payload["latitude"] = lat
            payload["longitude"] = long
        }
        
        let requestPathParams: [String: Any] = ["placeId": placeId]
        
        LogUtil.infoAPIPayload(apiName: AppConstants.apiNames.getPlaceStories, payload: payload, pathParams: requestPathParams, requestMethod: AppConstants.requestMethods.get)
        
        return (response: AFManager.request(self.getPlaceStoriesAPIEndPoint(placeId: placeId), method: .get, parameters: payload, encoding: URLEncoding.default, headers: SminqAPI.headers()), requestPayload: payload, requestMethod: HTTPMethod.get, requestPathParams: requestPathParams)
    }
    
    func getHybridSearchResults(latitude: Double?, longitude: Double?, searchText: String, type: String, userId: String) -> (response: DataRequest, requestPayload: Parameters, requestMethod: HTTPMethod, requestPathParams: [String: Any]) {
        var payload: Parameters = ["searchText": searchText, "userId": userId]
        
        if let lat = latitude, let long = longitude {
            payload["latitude"] = lat
            payload["longitude"] = long
        }
        
        if type != "" {
            payload["type"] = type
        }
        
        LogUtil.infoAPIPayload(apiName: AppConstants.apiNames.searchPlacesOrUsers, payload: payload, pathParams: [:], requestMethod: AppConstants.requestMethods.get)
        
        return (response: AFManager.request(self.getHybridSearchResultsAPIEndPoint(), method: .get, parameters: payload, encoding: URLEncoding.default, headers: SminqAPI.headers()), requestPayload: payload, requestMethod: HTTPMethod.get, requestPathParams: [:])
    }
    
    func getNearByPlaces(latitude: Double?, longitude: Double?, userId: String) -> (response: DataRequest, requestPayload: Parameters, requestMethod: HTTPMethod, requestPathParams: [String: Any]) {
        var payload: Parameters = ["userId": userId]
        
        if let lat = latitude, let long = longitude {
            payload["latitude"] = lat
            payload["longitude"] = long
        }
        
        LogUtil.infoAPIPayload(apiName: AppConstants.apiNames.searchNearbyPlaces, payload: payload, pathParams: [:], requestMethod: AppConstants.requestMethods.get)
        
        return (response: AFManager.request(self.getNearByPlacesAPIEndPoint(), method: .get, parameters: payload, encoding: URLEncoding.default, headers: SminqAPI.headers()), requestPayload: payload, requestMethod: HTTPMethod.get, requestPathParams: [:])
    }
    
    func getGoogleNearbyPlaces(latitude: Double?, longitude: Double?) -> (response: DataRequest, requestPayload: Parameters, requestMethod: HTTPMethod, requestPathParams: [String: Any]) {
        var payload: Parameters = ["radius": 200, "key": Config.getGoogleAPIKey()]
        
        if let lat = latitude, let long = longitude {
            payload["location"] = "\(lat),\(long)"
        }
        
        LogUtil.infoAPIPayload(apiName: AppConstants.apiNames.googleSearchNearbyPlaces, payload: payload, pathParams: [:], requestMethod: AppConstants.requestMethods.get)
        
        return (response: AFManager.request(self.getGoogleNearbyPlacesAPIEndPoint(), method: .get, parameters: payload, encoding: URLEncoding.default, headers: SminqAPI.headers()), requestPayload: payload, requestMethod: HTTPMethod.get, requestPathParams: [:])
    }
    
    func getGooglePlacesBySearch(latitude: Double?, longitude: Double?, input: String) -> (response: DataRequest, requestPayload: Parameters, requestMethod: HTTPMethod, requestPathParams: [String: Any]) {
        var payload: Parameters = ["radius": 200, "key": Config.getGoogleAPIKey(), "input": input]
        
        if let lat = latitude, let long = longitude {
            payload["location"] = "\(lat),\(long)"
        }
        
        LogUtil.infoAPIPayload(apiName: AppConstants.apiNames.googleSearchAutocomplete, payload: payload, pathParams: [:], requestMethod: AppConstants.requestMethods.get)
        
        return (response: AFManager.request(self.getGooglePlacesBySearchAPIEndPoint(), method: .get, parameters: payload, encoding: URLEncoding.default, headers: SminqAPI.headers()), requestPayload: payload, requestMethod: HTTPMethod.get, requestPathParams: [:])
    }
    
    func getPlaceStories(slug: String) -> (response: DataRequest, requestPayload: Parameters, requestMethod: HTTPMethod, requestPathParams: [String: Any]) {
        let payload: Parameters = ["slug": slug]
        
        LogUtil.infoAPIPayload(apiName: AppConstants.apiNames.getPlaceStoriesBySlug, payload: payload, pathParams: [:], requestMethod: AppConstants.requestMethods.get)
        
        return (response: AFManager.request(self.getPlaceStoriesBySlugAPIEndPoint(), method: .get, parameters: payload, encoding: URLEncoding.default, headers: SminqAPI.headers()), requestPayload: payload, requestMethod: HTTPMethod.get, requestPathParams: [:])
    }
    
    func getPlaceStories(postId: String, displayName: String) -> (response: DataRequest, requestPayload: Parameters, requestMethod: HTTPMethod, requestPathParams: [String: Any]) {
        let payload: Parameters = ["storyId": postId, "displayName": displayName]
        
        LogUtil.infoAPIPayload(apiName: AppConstants.apiNames.getPlaceStoriesByPostId, payload: payload, pathParams: [:], requestMethod: AppConstants.requestMethods.get)
        
        return (response: AFManager.request(self.getPlaceStoriesByPostIdAPIEndPoint(), method: .get, parameters: payload, encoding: URLEncoding.default, headers: SminqAPI.headers()), requestPayload: payload, requestMethod: HTTPMethod.get, requestPathParams: [:])
    }
    
    
    // MARK: Functions to get API end points
    
    func getPlaceDetailAPIEndPoint() -> String {
        return self.baseURL + "/v1/places"
    }
    
    func getStoriesAPIEndPoint() -> String {
        return self.baseURL + "/v1/stories"
    }
    
    func getStoryAPIEndPoint(userId: String) -> String {
        return self.baseURL + "/v1/stories/" + userId
    }
    
    func getPlaceStoriesAPIEndPoint(placeId: String) -> String {
        return self.baseURL + "/v1/stories/places/" + placeId
    }
    
    func getPlaceStoriesBySlugAPIEndPoint() -> String {
        return self.baseURL + "/v1/places/slug"
    }
    
    func getPlaceStoriesByPostIdAPIEndPoint() -> String {
        return self.baseURL + "/v1/stories/shared"
    }
    
    func getHybridSearchResultsAPIEndPoint() -> String {
        return self.baseURL + "/v1/search"
    }
    
    func getNearByPlacesAPIEndPoint() -> String {
        return self.baseURL + "/v1/search/nearby"
    }
    
    func getGoogleNearbyPlacesAPIEndPoint() -> String {
        return ApiConfig.mapsGoogleAPI + "/maps/api/place/nearbysearch/json"
    }
    
    func getGooglePlacesBySearchAPIEndPoint() -> String {
        return ApiConfig.mapsGoogleAPI + "/maps/api/place/autocomplete/json"
    }
    
    
}
