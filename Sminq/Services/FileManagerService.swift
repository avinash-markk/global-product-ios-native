//
//  FileManagerService.swift
//  Sminq
//
//  Created by Pushkar Deshmukh on 28/01/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import Foundation

class FileManagerService {
    private init() {}
    
    static let shared = FileManagerService()
    let fileManager = FileManager.default
    
    /// Creates a folder inside document directory
    
    /// - Parameter folderName - String: Name of the folder
    /// - Returns: URL - Document directory url of created folder
    
    func createFolder(folderName: String) -> URL? {
        guard let documentsUrl = self.fileManager.urls(for: .documentDirectory, in: .userDomainMask).first else { return nil }
        
        let folderURL = documentsUrl.appendingPathComponent(folderName)
        
        if !fileManager.fileExists(atPath: folderURL.path) {
            do {
                try fileManager.createDirectory(atPath: folderURL.path, withIntermediateDirectories: true, attributes: nil)
            } catch {
                LogUtil.error(error)
            }
        }
        
        return folderURL
        
    }
    
    /// Returns file url list inside a folder
    
    /// - Parameter folderName - String: Name of the folder
    /// - Returns: [URL] - Returns all files inside a folder
    
    func getAllFilesInFolder(folderName: DocumentDirectoryFolders) -> [URL] {
        var tempFileURLs = [URL]()
        
        guard let documentsUrl = fileManager.urls(for: .documentDirectory, in: .userDomainMask).first else { return tempFileURLs }
        
        let folderURL = documentsUrl.appendingPathComponent(folderName.rawValue)
        
        do {
            tempFileURLs = try fileManager.contentsOfDirectory(at: folderURL, includingPropertiesForKeys: nil)
        } catch {
            LogUtil.error(error)
        }
        
        return tempFileURLs
    }
    
    /// Calculates and returns size of folder
    
    /// - Parameter folderName - String: Name of the folder
    /// - Returns: Int - Returns size in bytes
    
    func getTotalFizeSizeInBytesInFolder(folderName: DocumentDirectoryFolders) -> Int {
        var tempFileSize = 0
        
        guard let documentsUrl = fileManager.urls(for: .documentDirectory, in: .userDomainMask).first else { return tempFileSize }
        
        let folderURL = documentsUrl.appendingPathComponent(folderName.rawValue)
        
        if fileManager.fileExists(atPath: folderURL.path) {
            do {
                let fileURLs = try fileManager.contentsOfDirectory(at: folderURL, includingPropertiesForKeys: nil)
                
                if fileURLs.count > 0 {
                    for file in fileURLs {
                        do {
                            let fileAttributes = try fileManager.attributesOfItem(atPath: file.path)
                            tempFileSize += fileAttributes[FileAttributeKey.size] as? Int ?? 0
                            
                        } catch {
                            LogUtil.error(error)
                        }
                    }
                }
                
            } catch {
                LogUtil.error(error)
            }
        }
        
        return tempFileSize
    }
    
    /// Convert bytes to megabytes
    
    /// - Parameter bytes - Int: Bytes
    /// - Returns: Double - Returns size in mega bytes
    
    func convertBytesToMegaBytes(bytes: Int) -> Double {
        var tempMegaBytes = 0.0
        
        if bytes > 0 {
            // NOTE: Operand needs to be of same type. Since returned value needs to in Precision ex (100.1 MB). Hence casted to Double.
            // For example: Conversion of 40000 bytes to mbs would return 0 if operands were integers. 40000 bytes / 1000000 -> Output as 0
            tempMegaBytes = Double(bytes) / 1000000.0
        }
        
        return tempMegaBytes
    }
    
    /// Returns the old files based on file creation time inside a folder
    
    /// - Parameter folderName - String: Name of the folder
    /// - Parameter howMany - Int: How many old files needs to returned
    /// - Returns: [URL] - Returns list of old files
    
    func getOldFilesInFolder(folderName: DocumentDirectoryFolders, howMany: Int) -> [URL] {
        var tempFileURLs = [URL]()
        
        guard let documentsUrl = fileManager.urls(for: .documentDirectory, in: .userDomainMask).first else { return tempFileURLs }
        
        let folderURL = documentsUrl.appendingPathComponent(folderName.rawValue)
        
        do {
            tempFileURLs = try fileManager.contentsOfDirectory(at: folderURL, includingPropertiesForKeys: nil)
            
            if tempFileURLs.count > 0 {
                tempFileURLs = sortFileURLsInAscendingOrderOfCreation(fileURLs: tempFileURLs)
            }
            
            // NOTE: Return first 10(howMany variable) old posts to remove
            if tempFileURLs.count >= howMany {
                let URLSlice  = tempFileURLs.prefix(upTo: howMany)
                tempFileURLs = Array(URLSlice)
            }
            
        } catch {
            LogUtil.error(error)
        }
        
        return tempFileURLs
    }
    
    /// Sorts the list in ascending order of file creation
    
    /// - Parameter fileURLs - [URL]: Name of the folder
    /// - Parameter howMany - Int: How many old files needs to returned
    /// - Returns: [URL] - Returns sorted list
    
    func sortFileURLsInAscendingOrderOfCreation(fileURLs: [URL]) -> [URL] {
        var tempURLs = fileURLs
        
        tempURLs.sort { (url1, url2) -> Bool in
            do {
                let fileAttributesForUrl1 = try fileManager.attributesOfItem(atPath: url1.path)
                let fileAttributesForUrl2 = try fileManager.attributesOfItem(atPath: url2.path)
                
                guard let date1 = fileAttributesForUrl1[FileAttributeKey.creationDate] as? Date else { return false }
                guard let date2 = fileAttributesForUrl2[FileAttributeKey.creationDate] as? Date else { return false }
                
                return date1 < date2
                
            } catch {
                LogUtil.error(error)
                return false
            }
        }
        
        return tempURLs
    }
    
}
