//
//  FIRDynamicLinksService.swift
//  Sminq
//
//  Created by Pushkar Deshmukh on 11/01/19.
//  Copyright © 2019 Suraj Sagare. All rights reserved.
//

import Foundation
import FirebaseDynamicLinks

class FIRDynamicLinksService: NSObject {
    static let shared = FIRDynamicLinksService()
    
    private override init() {}
    
    func generateFirebaseDynamicURLForPost(post: UserDataPlaceFeed, place: LivePlaces, completionHandler: @escaping (_ url: URL?) -> Void) {
        let shareURLString = place.shareUrl
        
        if shareURLString == "" {
            completionHandler(nil)
            return
        }
        
        let appURL = "\(FIRDynamicLinksService.getAppWebURL())/story/@\(post.displayName ?? "")"
        
//        let longDynamicLinkString = FIRDynamicLinksService.constructLongDynamicURLString(source: AppConstants.ios, medium: AppConstants.share, campaign: AppConstants.appShare, link: appURL, domainPrefix: FIRDynamicLinksService.getFirebaseDynamicURLString())

//        guard let longDynamicLink = URL(string: longDynamicLinkString) else {
//            completionHandler(nil)
//            return
//        }
        
        guard let link = URL(string: "\(appURL)%3Ftype=place%26utm_campaign=\(AppConstants.appShare)%26utm_medium=\(AppConstants.share)%26utm_source=\(AppConstants.ios)%26\(AppConstants.QueryParamKeys.storyId)=\(post._id ?? "")") else {
            completionHandler(nil)
            return
        }
        
        let linkBuilder = DynamicLinkComponents(link: link, domainURIPrefix: FIRDynamicLinksService.getFirebaseDynamicURLString())
        linkBuilder?.iOSParameters = DynamicLinkIOSParameters(bundleID: "com.markk.app")
        linkBuilder?.androidParameters = DynamicLinkAndroidParameters(packageName: "com.markk.app")
        
        linkBuilder?.shorten() { url, warnings, error in
            DispatchQueue.main.async {
                if let _ = error {
                    completionHandler(nil)
                } else {
                    completionHandler(url)
                }
            }
            
        }
        
//        DynamicLinkComponents.shortenURL(longDynamicLink, options: nil) { (url, warnings, error) in
//            if let _ = error {
//                completionHandler(nil)
//            } else {
//                completionHandler(url)
//            }
//        }
        
    }
    
    static func getFirebaseDynamicURLString() -> String {
        switch ApiConfig.appEnvironment {
        case .staging, .development:
            return UIApplication.shared.getStagingDynamicLinksDomain() ?? ""
        case.production:
            return UIApplication.shared.getProductionDynamicLinksDomain() ?? ""
        }
    }
    
    static func getAppWebURL() -> String {
        switch ApiConfig.appEnvironment {
        case .staging, .development:
            return UIApplication.shared.getStagingAppWebURL()
        case.production:
            return UIApplication.shared.getProductionAppWebURL()
        }
    }
    
    static func constructLongDynamicURLString(source: String, medium: String, campaign: String, link: String, domainPrefix: String) -> String {
        return "\(domainPrefix)?link=\(link)%26utm_campaign=\(campaign)%26utm_medium=\(medium)%26utm_source=\(source)"
    }
    
}
