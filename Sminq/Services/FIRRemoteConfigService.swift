//
//  FIRRemoteConfigService.swift
//  Sminq
//
//  Created by Pushkar Deshmukh on 17/01/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import Foundation
import FirebaseRemoteConfig

struct RemoteConfigDefaults {
    static let sminqAppStoreUrl = "https://itunes.apple.com/us/app/sminq-worlds-places-live-now/id1327305972"
    static let markkAppStoreUrl = "https://testflight.apple.com/join/vH17Gqz3"
    static let feedCacheInvalidateInHours = "48"
    static let feedCacheMaxBufferSizeInMbs = "300"
    static let feedCacheDeleteCount = 10
    static let stagingCloudinaryCloudNameIOS = "markk-media"
    static let productionCloudinaryCloudNameIOS = "sminq-india-solutions-pvt-ltd"
    static let stagingCloudinaryAPIKeyIOS = "516154212784676"
    static let productionCloudinaryAPIKeyIOS = "877926153119493"
    static let stagingCloudinaryAPISecretIOS = "siuvNXxTtottvzW59Ho1Emo2M6I"
    static let productionCloudinaryAPISecretIOS = "KiGYHNkhL1AlzY-D5NpweqbvjUg"
    static let countriesForMiles = "liberia|myanmar|united states|united States|Myanmar (Burma)"
    static let homeScreenNearbyRadiusMeters = "42000"
    static let sminqAppInitialLocationHomeScreen = "12.972442,77.580643"
    static let markkAppInitialLocationHomeScreen = "34.0522,-118.2437"
    static let cloudinaryOriginalImageFolder = "images"
    static let cloudinaryVideoFolder = "videos"
    static let pendingPostDiscardingInterval = 60
    static let galleryMediaAccessInterval = 24
    static let stagingCloudinaryUserProfileImagePreset = "markk_user"
    static let productionCloudinaryUserProfileImagePreset = "markk_user"
    static let stagingCloudinaryRatingVideoPreset = "markk_video"
    static let productionCloudinaryRatingVideoPreset = "markk_video"
    static let stagingCloudinaryRatingImagePreset = "markk_image"
    static let productionCloudinaryRatingImagePreset = "markk_image"
    static let subscribeBannerImage = "https://s3-us-west-1.amazonaws.com/markk.app/banners/subscribe-banner.png"
    static let trendingMinBannersCount = 3
    static let trendingMaxBannersCount = 5
}

class FIRRemoteConfigService {
    private init() {}
    
    static let shared = FIRRemoteConfigService()
    
    func getInitialLocationHomeScreenConfig() -> (latitude: Double, longitude: Double) {
        let locationString = RemoteConfig.remoteConfig().configValue(forKey: "app_initial_location_home_screen").stringValue!
        var locationArray = locationString.components(separatedBy: ",")
        
        return (latitude: Double(locationArray[0]) ?? 0, longitude: Double(locationArray[1]) ?? 0)
    }
    
    func getFeedCacheInvalidateInHours() -> String {
        if let hours = RemoteConfig.remoteConfig().configValue(forKey: "feed_media_cache_invalidate_in_hours_ios").stringValue {
            return hours
        } else {
            return RemoteConfigDefaults.feedCacheInvalidateInHours
        }
    }
    
    func getFeedCacheMaxBufferSizeInMbs() -> String {
        if let bufferSize = RemoteConfig.remoteConfig().configValue(forKey: "feed_media_cache_max_buffer_size_in_mbs_ios").stringValue {
            return bufferSize
        } else {
            return RemoteConfigDefaults.feedCacheMaxBufferSizeInMbs
        }
    }
    
    func getFeedCacheDeleteCount() -> Int {
        if let feedCacheDeleteCount = RemoteConfig.remoteConfig().configValue(forKey: "feed_media_cache_delete_count_ios").stringValue {
            return Int(feedCacheDeleteCount) ?? RemoteConfigDefaults.feedCacheDeleteCount
        } else {
            return RemoteConfigDefaults.feedCacheDeleteCount
        }
    }
    
    func getCloudinaryOriginalImageFolder() -> String {
        if let folder = RemoteConfig.remoteConfig().configValue(forKey: "cloudinary_original_img_folder").stringValue {
            return folder
        } else {
            return RemoteConfigDefaults.cloudinaryOriginalImageFolder
        }
    }
    
    func getCloudinaryVideoFolder() -> String {
        if let folder = RemoteConfig.remoteConfig().configValue(forKey: "cloudinary_video_folder").stringValue {
            return folder
        } else {
            return RemoteConfigDefaults.cloudinaryVideoFolder
        }
    }
    
    func getNearyByDistanceInMeteres() -> String {
        if let meters = RemoteConfig.remoteConfig().configValue(forKey: "homescreen_nearby_radius_meters").stringValue {
            return meters
        } else {
            return RemoteConfigDefaults.homeScreenNearbyRadiusMeters
        }
    }
    
    func getPendingPostDiscardingInterval() -> Int {
        if let interval = RemoteConfig.remoteConfig().configValue(forKey: "new_post_ttl_minutes").stringValue {
            return Int(interval) ?? RemoteConfigDefaults.pendingPostDiscardingInterval
        } else {
            return RemoteConfigDefaults.pendingPostDiscardingInterval
        }
    }
    
    func getGalleryMediaAccessInterval() -> Int {
        if let interval = RemoteConfig.remoteConfig().configValue(forKey: "gallery_media_in_hours").stringValue {
            return Int(interval) ?? RemoteConfigDefaults.galleryMediaAccessInterval
        } else {
            return RemoteConfigDefaults.galleryMediaAccessInterval
        }
    }
    
    func getCloudinaryUserProfileImagePreset() -> String {
        if let preset = RemoteConfig.remoteConfig().configValue(forKey: "cloudinary_user_profile_image_preset").stringValue {
            return preset
        } else {
            return RemoteConfigAppDefaults.shared.getDefaultCloudinaryUserProfileImagePreset()
        }
    }
    
    func getCloudinaryRatingVideoPreset() -> String {
        if let preset = RemoteConfig.remoteConfig().configValue(forKey: "cloudinary_rating_video_preset").stringValue {
            return preset
        } else {
            return RemoteConfigAppDefaults.shared.getDefaultCloudinaryRatingVideoPreset()
        }
    }
    
    func getCloudinaryRatingImagePreset() -> String {
        if let preset = RemoteConfig.remoteConfig().configValue(forKey: "cloudinary_rating_image_preset").stringValue {
            return preset
        } else {
            return RemoteConfigAppDefaults.shared.getDefaultCloudinaryRatingImagePreset()
        }
    }
    
    func getNearbyRadiusMeters() -> String {
        if let meters = RemoteConfig.remoteConfig().configValue(forKey: "homescreen_nearby_radius_meters").stringValue {
            return meters
        } else {
            return RemoteConfigDefaults.homeScreenNearbyRadiusMeters
        }
    }
    
    func getGoogleAPIKey() -> String {
        if let key = RemoteConfig.remoteConfig().configValue(forKey: "google_api_key").stringValue {
            return key
        } else {
            return ApiConfig.stagingGoogleKey
        }
    }
    
    func getSubScrinbeBannerImage() -> String {
        if let key = RemoteConfig.remoteConfig().configValue(forKey: "subscribe_banner_image").stringValue {
            return key
        } else {
            return RemoteConfigDefaults.subscribeBannerImage
        }
    }
    
    func getTrendingMinBannersCount() -> Int {
        if let count = RemoteConfig.remoteConfig().configValue(forKey: "trending_min_banners_count").stringValue {
            return Int(count) ?? RemoteConfigDefaults.trendingMinBannersCount
        } else {
            return RemoteConfigDefaults.trendingMinBannersCount
        }
    }
    
    func getTrendingMaxBannersCount() -> Int {
        if let count = RemoteConfig.remoteConfig().configValue(forKey: "trending_max_banners_count").stringValue {
            return Int(count) ?? RemoteConfigDefaults.trendingMaxBannersCount
        } else {
            return RemoteConfigDefaults.trendingMaxBannersCount
        }
    }
}
