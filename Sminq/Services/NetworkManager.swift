//
//  NetworkManager.swift
//  Sminq
//
//  Created by Avinash Thakur on 30/04/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import UIKit
import Alamofire

class NetworkManager {

static let sharedInstance = NetworkManager()
    private init() {}

    let reachabilityManager = Alamofire.NetworkReachabilityManager(host: "www.apple.com")
    
    func startNetworkReachabilityObserver() {
        reachabilityManager?.listener = { status in
            var networkStatus: Bool = true
            switch status {
                
            case .notReachable:
                networkStatus = false
        
            case .unknown :
                networkStatus = false
               
            case .reachable(.ethernetOrWiFi):
                networkStatus = true
               
            case .reachable(.wwan):
                networkStatus = true
                
            }
            DispatchQueue.main.async {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: AppConstants.AppNotificationNames.networkReachable), object: nil, userInfo: ["status": networkStatus])
            }
        }
        reachabilityManager?.startListening()
    }
    
    class func isConnectedToInternet() -> Bool {
        NetworkReachabilityManager()!.startListening()
        return NetworkReachabilityManager()!.isReachable
    }
    
}

