//
//  CloudinaryService.swift
//  Sminq
//
//  Created by Pushkar Deshmukh on 25/02/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import Foundation
import Cloudinary

class CloudinaryService: NSObject {
    private override init() {}
    
    static let shared = CloudinaryService()
    
    let config = CLDConfiguration(cloudName: cloudinaryCloudName(), apiKey: cloudinaryApiKey(), apiSecret: cloudinaryApiSecret())
    
    func generateCloudinaryImageURL(folder: String, fileName: String) -> URL? {
        let cloudinary = CLDCloudinary(configuration: config)
        
        guard let fileStringUrl = cloudinary.createUrl().generate(folder + "/" + fileName) else { return nil }
        
        guard let url = URL(string: fileStringUrl) else { return nil }
        
        return url
    }
    
    func generateCloudinaryVideoURL(folder: String, fileName: String) -> URL? {
        let cloudinary = CLDCloudinary(configuration: config)
        
        guard let fileStringUrl = cloudinary.createUrl().setResourceType("video").generate(folder + "/" + fileName) else { return nil }
        
        guard let url = URL(string: fileStringUrl) else { return nil }
        
        return url
    }
    
    func getCloudinaryURL(mediaType: String, mediaName: String) -> URL? {
        if mediaType == "image" {
            return generateCloudinaryImageURL(folder: FIRRemoteConfigService.shared.getCloudinaryOriginalImageFolder(), fileName: mediaName)
        } else  {
            return generateCloudinaryVideoURL(folder: FIRRemoteConfigService.shared.getCloudinaryVideoFolder(), fileName: mediaName)
        }
    }
    
     static func cloudinaryCloudName() -> String {
        var cloudinaryCloudName: String
        switch ApiConfig.appEnvironment {
        case .staging, .development:
            cloudinaryCloudName = ApiConfig.stagingCloudinaryCloudName!
        case .production:
            cloudinaryCloudName = ApiConfig.productionCloudinaryCloudName!
        }
        return cloudinaryCloudName
    }
    
     static func cloudinaryApiKey() -> String {
        var cloudinaryApiKey: String
        switch ApiConfig.appEnvironment {
        case .staging, .development:
            cloudinaryApiKey = ApiConfig.stagingCloudinaryAPIKey!
        case .production:
            cloudinaryApiKey = ApiConfig.productionCloudinaryAPIKey!
        }
        return cloudinaryApiKey
    }
    
     static func cloudinaryApiSecret() -> String {
        var cloudinarySecretKey: String
        switch ApiConfig.appEnvironment {
        case .staging, .development:
            cloudinarySecretKey = ApiConfig.stagingCloudinarySecretKey!
        case .production:
            cloudinarySecretKey = ApiConfig.productionCloudinarySecretKey!
        }
        return cloudinarySecretKey
    }
}
