//
//  MessageUIService.swift
//  Markk
//
//  Created by Pushkar Deshmukh on 20/08/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import UIKit
import MessageUI

protocol MessageUIServiceDelegate: NSObjectProtocol {
    func didFinishWith()
}

class MessageUIService: NSObject {
    static let shared = MessageUIService()
    public weak var delegate: MessageUIServiceDelegate?
    
    fileprivate var presentController = UIViewController()
    
    private override init() {}
    
    func sendSMSText(phoneNumbers: [String]?, body: String?, controller: UIViewController) {
        let messageController = MFMessageComposeViewController()
        messageController.body = body
        messageController.recipients = phoneNumbers
        messageController.messageComposeDelegate = self
        
        presentController = controller
        
        presentController.present(messageController, animated: true, completion: nil)
        
    }
}

extension MessageUIService: MFMessageComposeViewControllerDelegate {
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        presentController.dismiss(animated: true, completion: nil)
        delegate?.didFinishWith()
    }
}
