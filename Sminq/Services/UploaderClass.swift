//
//  UploaderClass.swift
//  Sminq
//
//  Created by Bjorn Mascarenhas on 03/09/18.
//  Copyright © 2018 Suraj Sagare. All rights reserved.
//

import UIKit
import SVProgressHUD
import Cloudinary
import MapKit
import Firebase
import CoreData
import Gloss
import AVKit

class UploaderClass: Operation {
    let LocationMgr = UserLocationManager.SharedManager
    
    func getCloudinaryUploadRequestParam(isVideo: Bool) -> CLDUploadRequestParams {
        let uploadType: String = "\(isVideo == true ? "video" : "image")"
        let params = CLDUploadRequestParams()
        params.setResourceType("\(uploadType)")
        return params
    }
    
    func createCloudinaryUploadConfigFile() -> CLDCloudinary {
        let uploadConfig = CLDConfiguration(cloudName: SminqAPI.cloudinaryCloudName(), apiKey: SminqAPI.cloudinaryApiKey(), apiSecret: SminqAPI.cloudinaryApiSecret())
        let cloudinary = CLDCloudinary(configuration: uploadConfig)
        return cloudinary
    }
    
    func uploadImageToCloudinary(imageTask: UploadTask, taskProgressHandler: @escaping (_ postTask: UploadTask) -> Void, taskCompletionHandler: @escaping (_ postTask: UploadTask, _ error: String?) -> Void) {
        var image = UIImage()
        let fileUrlPath = DocDirectoryUtil().getLocalUrlForTask(id: imageTask.taskId!, isVideo: false)
        if let newtempImage = UIImage(contentsOfFile: fileUrlPath) {
            image = newtempImage
        }
        
        if let imageData = UIImageJPEGRepresentation(image, 1.0) {
            let uploader = createCloudinaryUploadConfigFile().createUploader()
            
            let progressHandler = { (progress: Progress) in
                let ratio: CGFloat = CGFloat(progress.completedUnitCount) / CGFloat(progress.totalUnitCount)
                imageTask.progress = Float(ratio)
                taskProgressHandler(imageTask)
            }
            
            let imageUploadRequest = uploader.upload(data: imageData as Data, uploadPreset: FIRRemoteConfigService.shared.getCloudinaryRatingImagePreset(), params: self.getCloudinaryUploadRequestParam(isVideo: false), progress: progressHandler) { (response, error) in
                let imageNamePublicId = response?.publicId
                DispatchQueue.main.async {
                    if error == nil {
                        if (self.getPublicIdFrom(responseString: imageNamePublicId) != nil) {
                            imageTask.taskPublicId = self.getPublicIdFrom(responseString: imageNamePublicId)
                            taskCompletionHandler(imageTask, nil)
                        } else {
                            LogUtil.error(response)
                            AnalyticsHelper.cloudinaryUploadFailedEvent(mediaType: AppConstants.image, reason: self.getPublicIdErrorDescription(error: error))
                            taskCompletionHandler(imageTask, self.getPublicIdErrorDescription(error: error))
                        }
                    } else {
                        LogUtil.error(error)
                        AnalyticsHelper.cloudinaryUploadFailedEvent(mediaType: AppConstants.image, reason: self.getUploadErrorDescription(error: error))
                        taskCompletionHandler(imageTask, self.getUploadErrorDescription(error: error))
                    }
                }
            }
            UploadManager.sharedInstance.uploaderRequest = imageUploadRequest
        }
    }
    
    func uploadVideoToCloudinary(videoTask: UploadTask, taskProgressHandler: @escaping (_ postTask: UploadTask) -> Void, taskCompletionHandler: @escaping (_ videoTask: UploadTask, _ error: String?) -> Void) {
        var videoData = NSData()
        let fileUrlPath = DocDirectoryUtil().getLocalUrlForTask(id: videoTask.taskId!, isVideo: true)
        if let videoDataFromPath = NSData(contentsOf: URL(fileURLWithPath:fileUrlPath)) {
            videoData = videoDataFromPath
            
            let uploader = createCloudinaryUploadConfigFile().createUploader()
            
            let progressHandler = { (progress: Progress) in
                let ratio: CGFloat = CGFloat(progress.completedUnitCount) / CGFloat(progress.totalUnitCount)
                videoTask.progress = Float(ratio)
                taskProgressHandler(videoTask)
            }
            
            let videoUploadRequest = uploader.upload(data: videoData as Data, uploadPreset: FIRRemoteConfigService.shared.getCloudinaryRatingVideoPreset(), params: self.getCloudinaryUploadRequestParam(isVideo: true), progress: progressHandler) { (response, error) in
                DispatchQueue.main.async {
                    let imageNamePublicId = response?.publicId
                    let videoDuration = response?.duration
                    videoTask.videoDuration = videoDuration
                    if error == nil {
                        if (self.getPublicIdFrom(responseString: imageNamePublicId) != nil) {
                            videoTask.taskPublicId = self.getPublicIdFrom(responseString: imageNamePublicId)
                            taskCompletionHandler(videoTask, nil)
                        } else {
                            AnalyticsHelper.cloudinaryUploadFailedEvent(mediaType: AppConstants.video, reason: self.getPublicIdErrorDescription(error: error))
                            taskCompletionHandler(videoTask, self.getPublicIdErrorDescription(error: error))
                        }
                    } else {
                        LogUtil.error(error)
                        AnalyticsHelper.cloudinaryUploadFailedEvent(mediaType: AppConstants.video, reason: self.getUploadErrorDescription(error: error))
                        taskCompletionHandler(videoTask, self.getUploadErrorDescription(error: error))
                    }
                }
            }
            UploadManager.sharedInstance.uploaderRequest = videoUploadRequest
        }
    }
    
    func getUploadErrorDescription(error: NSError?) -> String {
        var errorDescription : String = ""
        if let userInfo = error?.userInfo {
            if let errorDesc = userInfo.valueForKeyPath(keyPath: "NSLocalizedDescription") as? String {
            errorDescription = errorDesc
        }
            if let errorDesc = userInfo.valueForKeyPath(keyPath: "message") as? String {
                errorDescription = errorDesc
            }
        }
        return errorDescription
    }
    
    func getPublicIdErrorDescription(error: NSError?) -> String {
        var errorDescription : String = ""
        if let userInfo = error?.userInfo {
            if let errorDesc = userInfo.valueForKeyPath(keyPath: "NSLocalizedDescription") as? String {
                errorDescription = errorDesc
            } else {
                errorDescription = "Public id is null"
            }
        }
        return errorDescription
    }
    
    func getPublicIdFrom(responseString: String?) -> String? {
        if let publicIdArr = responseString?.components(separatedBy: "/") {
            let publicId = publicIdArr[1]
            return publicId
        }
        return nil
    }
    
    func setDataForAddingPost(postTask: UploadTask) {
        let taskStatus = UploadDatabaseService.shared.getStatusForFeed(feedID: postTask.taskId!)
        if taskStatus == "Cancelled" {
            DispatchQueue.main.async {
                postTask.status = "Cancelled"
                UploadDatabaseService.shared.updateStatusForFeed(feedID: postTask.taskId!, newStatus: postTask.status)
                NotificationCenter.default.post(name: NSNotification.Name(AppConstants.AppNotificationNames.uploadDone), object: nil, userInfo: ["task": postTask])
            }
            return
        }
        
        var textList = [TextList]()
        if postTask.textListString != "" {
            textList = RatingsHelper().createTextStickerListForPost(stickerString: postTask.textListString!)
        }
        
        var stickerList = [PostStickerData]()
        if postTask.stickerListString != "" {
            stickerList = RatingsHelper().createImageStickersListForPost(stickerString: postTask.stickerListString!)
        }
        
      guard let addPostQuestionsData = RatingsHelper().getPostDataFrom(jsonString: postTask.questionsData!) else { return }
        
        if  addPostQuestionsData.questions != nil {
            var feedInputsArray: [[String: Any]] = []
            for subCategoryQuestionObj in (addPostQuestionsData.questions)! {

                let input = ""
                if subCategoryQuestionObj.context == "other" {
                    var feedInput: [String: Any]
                    if postTask.isVideo {
                        feedInput = [
                            "subCategoryQuestion": subCategoryQuestionObj._id,
                            "input": input,
                            "context": subCategoryQuestionObj.context,
                            "image": "\(postTask.taskPublicId!).jpg",
                            "video": "\(postTask.taskPublicId!).mp4",
                            "videoDuration": postTask.videoDuration ?? 0.0,
                            "mediaMetadata": ["textList": textList.toJSONArray() as Any, "stickerList": stickerList.toJSONArray() as Any],
                            "mediaSource": "\(addPostQuestionsData.liveShot == "no" ? "gallery" : "camera")"
                        ]
                    } else {
                        feedInput = [
                            "subCategoryQuestion": subCategoryQuestionObj._id,
                            "input": input,
                            "context": subCategoryQuestionObj.context,
                            "image": "\(postTask.taskPublicId!).jpg",
                            "mediaMetadata": ["textList": textList.toJSONArray() as Any,  "stickerList": stickerList.toJSONArray() as Any],
                            "mediaSource": "\(addPostQuestionsData.liveShot == "no" ? "gallery" : "camera")"
                        ]
                    }
                    
                    feedInputsArray.append(feedInput)
                }
            }
            
            self.addPost(postTask: postTask, fields: feedInputsArray)
        }
    }
    

    
    fileprivate func getCloserLocation(place: LivePlaces) -> (pickedLocation: Location?, availableLocations: [Location]) {
        var locations = [Location]()
        
        if let location = UserDefaultsUtility.shared.getkCLLocationAccuracyBestLocation() {
            var mLocation = Location.init(location: location, accuracy: kCLLocationAccuracyBest)
            mLocation.distanceFromPlace = location.distance(from: CLLocation.init(latitude: place.geoPoint.coordinates[1], longitude: place.geoPoint.coordinates[0]))
            locations.append(mLocation)
        }
        
        if let location = UserDefaultsUtility.shared.getkCLLocationAccuracyHundredMeters() {
            var mLocation = Location.init(location: location, accuracy: kCLLocationAccuracyHundredMeters)
            mLocation.distanceFromPlace = location.distance(from: CLLocation.init(latitude: place.geoPoint.coordinates[1], longitude: place.geoPoint.coordinates[0]))
            locations.append(mLocation)
        }
        
        if let location = UserDefaultsUtility.shared.getLastKnownLocation() {
            var mLocation = Location.init(location: location, accuracy: nil)
            mLocation.distanceFromPlace = location.distance(from: CLLocation.init(latitude: place.geoPoint.coordinates[1], longitude: place.geoPoint.coordinates[0]))
            locations.append(mLocation)
        }
        
        let mLocations = locations.sorted { (location1, location2) -> Bool in
            guard let distance1 = location1.distanceFromPlace, let distance2 = location2.distanceFromPlace else { return false }
            
            return distance1 < distance2
            
        }
        
        print(mLocations)
        
        return (pickedLocation: mLocations.first, availableLocations: mLocations)
        
        
    }

    func addPost(postTask: UploadTask, fields: [[String: Any?]]) {
        guard let addPostQuestionsData = RatingsHelper().getPostDataFrom(jsonString: postTask.questionsData!), let place = addPostQuestionsData.place else { return }
        
        var selectedMood: String = ""
        var coordinates = Array<Double>()
        var ratingSuccess: String = "no"
        
        if let mood: String = addPostQuestionsData.mood {
            selectedMood = mood
        }
        
        var locationAccuracy: Double = 0
        var mLocationTuple: (pickedLocation: Location?, availableLocations: [Location])?
        var isFromGallery = false
        if let location = addPostQuestionsData.locationData, location.count > 0 {
            // Camera asset location
            coordinates = location
            isFromGallery = true
        } else {
            mLocationTuple = getCloserLocation(place: place)
            guard let location = mLocationTuple?.pickedLocation?.location else { return }
            coordinates = [location.coordinate.latitude, location.coordinate.longitude]
            locationAccuracy = location.horizontalAccuracy
        }
        
        AnalyticsHelper.locationDebugInfoEvent(pickedLocation: mLocationTuple?.pickedLocation, isFromGallery: isFromGallery, locationData: addPostQuestionsData.locationData, availableLocations: mLocationTuple?.availableLocations ?? [], place: place)
        
        let group = DispatchGroup()
        group.enter()
        
        var params = [
            "place": addPostQuestionsData.place?._id ?? "",
            "subPlace": addPostQuestionsData.place?.defaultSubPlace ?? "",
            "user": Helper().getUserId(),
            "feedInputs": fields,
            "verified": addPostQuestionsData.verified,
            "locationAccuracy": locationAccuracy
            ] as [String: Any]
        
        let geoPoint = [
            "coordinates": [coordinates[1], coordinates[0]],
            "type": "Point"
            ] as [String: Any]
        
        params["geoPoint"] = geoPoint
        params["moodScore"] = selectedMood
        params["createdAt"] = Date().getTimeStampFrom(dateString: addPostQuestionsData.createdAt)
        
        guard let metric = HTTPMetric(url: URL(string: FeedService().addPostAPIEndPoint())!, httpMethod: .post) else { return }
        metric.start()
        
        let addPostAPI = FeedService().addPost(params: params as NSDictionary)
        
        addPostAPI.response.validate(statusCode: 200..<300).responseJSON { response in
//            debugPrint(response)
            
            if let httpCode = response.response?.statusCode {
                metric.responseCode = httpCode
            }
            
            switch response.result {
                case .success:
                    guard let json = response.result.value as? JSON else { return }
                    
                    if let postFeedResponse: PostFeedResponseStatus = PostFeedResponseStatus(json: json), let feedList = postFeedResponse.feedItemsList, feedList.count > 0 {
                        
                        guard let feed = feedList.first else { return }
                        if let feedDataStr = feed.toJSON() {
                            let feedDataJsonStr =  Helper().jsonToStringConversion(jsonObject: feedDataStr as Any)
                            postTask.post = feedDataJsonStr
                        }
                        
                        if feed.contributions.count > 0 {
                            
                            guard let otherContext = feed.getOtherContext() else {
                                LogUtil.error("Could not find other context in add post API response")
                                return
                            }                    
                        }
                    }
                
                    DispatchQueue.main.async {
                        postTask.status = "Completed"
                        UploadDatabaseService.shared.updateStatusForFeed(feedID: postTask.taskId!, newStatus: postTask.status)
                        NotificationCenter.default.post(name: NSNotification.Name(AppConstants.AppNotificationNames.uploadDone), object: nil, userInfo: ["task": postTask])

                    }
                    
                   ratingSuccess = "yes"
                   self.addRatingSubmitAnalyticEvent(postData: addPostQuestionsData, success: ratingSuccess, isVideo: postTask.isVideo)
                    // Capture user's location data for old and new implementation
                    
                    if let oldLocation = CLLocationManager().location?.coordinate, let newLocation = self.LocationMgr.getCLLocation()?.coordinate {
                        AnalyticsHelper.testUserLocationOnPostEvent(oldLatitude: oldLocation.latitude, oldLongitude: oldLocation.longitude, newLatitude: newLocation.latitude, newLongitude: newLocation.longitude, placeId: addPostQuestionsData.place?._id ?? "", placeName: postTask.placeName!)
                }
                   DispatchQueue.global(qos: .background).async {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2, execute: {
                        self.getUserUnlockedLevels()
                    })
                }
                
                
                case .failure(let error):
                    LogUtil.error(error)
                    
                    DispatchQueue.main.async {
                        postTask.status = "Failed"
                        NotificationCenter.default.post(name: NSNotification.Name(AppConstants.AppNotificationNames.uploadDone), object: nil, userInfo: ["task": postTask])
                
                    }
                    
                    ratingSuccess = "no"
                    self.addRatingSubmitAnalyticEvent(postData: addPostQuestionsData, success: ratingSuccess, isVideo: postTask.isVideo)
                    let error = error as NSError
                    let userInfo = error.userInfo
                    if let errorDesc = userInfo.valueForKeyPath(keyPath: "NSLocalizedDescription") as? String {
                        AnalyticsHelper.APIFailureEvent(screen: AppConstants.screenNames.addPost, apiName: AppConstants.apiNames.addPost, errorMessage: errorDesc, userId: Helper().getUserId(), errorCode: nil, requestPayload: addPostAPI.requestPayload, requestMethod: addPostAPI.requestMethod.rawValue, requestPathParams: addPostAPI.requestPathParams)
                }
            }
            
            group.leave()
            metric.stop()
        }
    }
    
    func addRatingSubmitAnalyticEvent(postData: PostData, success: String, isVideo: Bool) {
        AnalyticsHelper.addRatingSubmit(postData: postData, success: success, isVideo: isVideo, screen: AppConstants.screenNames.home)
    }
    
    func getUserUnlockedLevels() {
        let profileModel = ProfileViewModel.init(displayName: "\(Helper().getUserDisplayName())", isMyProfile: true)
        profileModel.getUserUnlockedLevel(completion: {
            (result, networkError, error) in
            if error == nil {
                self.checkAndCompareForUnlockedLevels(recentLevelsList: result ?? [])
            } else {
            }
        })
    }
    
    func checkAndCompareForUnlockedLevels(recentLevelsList: [LevelsList]) {
        if recentLevelsList.count > 0 {
            let profileDataClass = ProfileDataClass()
            guard var cachedProfileData = profileDataClass.getProfileCachedData() else { return }
            if cachedProfileData.levelsList.count > 0 {
                
                let cachedLevelsList = cachedProfileData.levelsList
                for index in 0...recentLevelsList.count - 1 {
                    let sublevels: LevelsList = recentLevelsList[index]
                    
                    for subindex in 0...sublevels.levels.count - 1 {
                        let level: Levels = sublevels.levels[subindex]
                        if cachedLevelsList.count > index && cachedLevelsList[index].levels.count > subindex {
                            let cachedLevel = cachedLevelsList[index].levels[subindex]
                            if level.completed != cachedLevel.completed && level.completed {
                                var newBadge: Badge = level.badge!
                                newBadge.name = "\(sublevels.id!) Level \(level.level)!"
                                DispatchQueue.main.async {
                                    self.showCongratulationsForBadgeUnlocked(badge: newBadge)
                                }
                                cachedProfileData.levelsList = recentLevelsList
                                profileDataClass.saveProfileData(userProfileData: cachedProfileData)
                                break
                            }
                        }
                    }
                }
            }
        }
    }
    
    func showCongratulationsForBadgeUnlocked(badge: Badge!) {
        if let viewController = AppConstants.storyboard.instantiateViewController(withIdentifier: "PostSuccessViewController") as? PostSuccessViewController {
            viewController.isBadgeUnlocked = true
            viewController.newBadge = badge
            let navController = UINavigationController(rootViewController: viewController)
            AppConstants.appDelegate.window?.rootViewController?.present(navController, animated: true, completion: nil)
        }
    }
    
}

class UploadTask {
    var taskId: String?
    var questionsData: String?
    var placeIDString: String?
    var subPlaceIDString: String?
    var isVideo: Bool = false
    var taskPublicId: String?
    var videoDuration: Double?
    var placeName: String?
    var textListString: String?
    var stickerListString: String?
    var status: String
    var progress: Float
    var post: String?
    
    init(taskId: String, questionsData: String, placeIDString: String, subPlaceIDString: String, isVideo: Bool, taskPublicId: String, videoDuration: Double, placeName: String, textListString: String, stickerListString: String, status: String, progress: Float, post: String) {
        self.taskId = taskId
        self.questionsData = questionsData
        self.placeIDString = placeIDString
        self.subPlaceIDString = subPlaceIDString
        self.isVideo = isVideo
        self.taskPublicId = taskPublicId
        self.videoDuration = videoDuration
        self.placeName = placeName
        self.textListString = textListString
        self.stickerListString = stickerListString
        self.status = status
        self.progress = progress
        self.post = post
    }
    
}
