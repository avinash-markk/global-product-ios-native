//
//  UserNoficationService.swift
//  Sminq
//
//  Created by Pushkar Deshmukh on 05/02/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import UIKit
import Foundation
import UserNotifications
import Firebase

// NOTE: UNUserNotificationCenterDelegate needs to Confirm to NSObject for default methods like 'isEqual', 'hash', 'perform', 'isKind'

class UserNoficationService: NSObject {
    private override init() {} // Singleton class. So no other file can create instance of this class
    
    static let shared = UserNoficationService()
    
    let unCenter = UNUserNotificationCenter.current()
    
    func authorize(completion: @escaping (_ granted: Bool) -> Void) {
        let options: UNAuthorizationOptions = [.badge, .sound, .alert]
        
        unCenter.requestAuthorization(options: options) { (granted, error) in
            LogUtil.info(error ?? "No user notification auth error")
            completion(granted)
            guard granted else { return }
            
            DispatchQueue.main.async {
                self.configure()
            }
        }
    }
    
    func getNotificationSettings(completion: @escaping(_ authorizationStatus: UNAuthorizationStatus) -> Void) {
        unCenter.getNotificationSettings { (settings) in
            DispatchQueue.main.async {
                completion(settings.authorizationStatus)
            }
        }
    }
    
    func configure() {
        unCenter.delegate = self
        
        let application = UIApplication.shared
        application.registerForRemoteNotifications()
    }
    
    func getAllDeliveredNotifications(completion: @escaping(_ notifications: [UNNotification]) -> Void) {
        unCenter.getDeliveredNotifications { (notifications) in
            DispatchQueue.main.async {
                completion(notifications)
            }
        }
    }
    
    func removeAllNotificationsFromTray(completion: @escaping() -> Void) {
        unCenter.removeAllDeliveredNotifications()
        completion()
    }
    
    func subscribeToPushNotifications() {
        InstanceID.instanceID().instanceID { (result, error) in
            if let _ = error {
                print("error fetching remote instance id")
            } else if let result = result, let _ = Helper().getUser() {
                self.configure()
                UserDefaults.standard.set(result.token, forKey: "FCMToken")
                DispatchQueue.global(qos: .background).async {
                    PushNotificationAPI.registerForSminqPushNotifications(userId: Helper().getUserId(), fcmToken: UserDefaults.standard.string(forKey: "FCMToken")!, register: AppConstants.enablePush)
                }
            }
        }
    }
    
   
}

extension UserNoficationService: UNUserNotificationCenterDelegate {
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        print(response)
        
        // NOTE: User Interacted with notification. This is triggered when app is in background/inactive state and not
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            NotificationCacheService.shared.cacheNotificationByUNNotification(unNotification: response.notification)
            self.handleNotification(userInfo: response.notification.request.content.userInfo)
            
            completionHandler()
        }
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        // NOTE: Notification recieved when app is in foreground
        print(notification)
        
        NotificationCacheService.shared.cacheNotificationByUNNotification(unNotification: notification)
        
        if NotificationCacheService.shared.getNotificationType(userInfo: notification.request.content.userInfo) == AppConstants.newBadge {
            DispatchQueue.main.async {
                self.handleNotification(userInfo: notification.request.content.userInfo)
            }
            return
        }
        
        completionHandler([.alert, .sound])
    }
    
    func handleNotification(userInfo: [AnyHashable: Any]) {
        let userDefaults = UserDefaults.standard
        userDefaults.set(userInfo, forKey: AppConstants.UserDefaultsKeys.notificationUserInfo)
        
        NotificationCenter.default.post(name: Notification.Name(AppConstants.AppNotificationNames.notificationTappedFromTray), object: nil, userInfo: userInfo)
    }
}
