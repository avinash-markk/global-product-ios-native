//
//  DataCacheService.swift
//  Sminq
//
//  Created by Avinash Thakur on 27/02/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import UIKit

class DataCacheService {

    var cacheStorage = FileStorage()
    
    func loadDataFromCache(cacheKey: String) -> Data? {
        let data = cacheStorage[cacheKey]
        return data
    }
    
    func saveDataIntoCache(cacheKey: String, cacheData: Data) {
        cacheStorage[cacheKey] = cacheData
    }
    
    func clearAllDataFromCache() {
        cacheStorage.clearAllDataFromDocDirectory()
    }
    
    func removeCacheByKey(key: String) {
        var url = cacheStorage.cacheURL
        url.appendPathComponent(key)
        
        do {
            try FileManager().removeItem(atPath: url.path)
        } catch {
            
        }
    }
    
}

struct FileStorage {
    
    let cacheURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
    
    subscript(pathKey: String) -> Data? {
        get {
            let url = cacheURL.appendingPathComponent(pathKey)
            return try? Data(contentsOf: url)
        }
        set {
            let url = cacheURL.appendingPathComponent(pathKey)
            _ = try? newValue?.write(to: url)
        }
    }
    
    func clearAllDataFromDocDirectory() {
        do {
            let filePaths = try FileManager().contentsOfDirectory(atPath: cacheURL.path)
            for filePath in filePaths {
                let url = cacheURL.appendingPathComponent(filePath)
//                print(url)
                try FileManager().removeItem(atPath: url.path)
            }
        } catch {
            LogUtil.error(error)
        }
    }
    
}
