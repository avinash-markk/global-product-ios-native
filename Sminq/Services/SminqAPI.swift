//
//  SminqAPI.swift
//  Sminq
//
//  Created by Bjorn Mascarenhas on 27/02/18.
//  Copyright © 2018 Suraj Sagare. All rights reserved.
//

import Foundation
import Alamofire
import FirebaseRemoteConfig
enum SminqAPIError: Swift.Error {
    case NullVariable
    case JSONFormatError
    case ParseError
}

class SminqAPI {
    
    init() {}

    class func endpoint(_ endpoint: String) -> URLConvertible {
        var apiConfigURL: String
        switch ApiConfig.appEnvironment {
        case .development:
            apiConfigURL = UIApplication.shared.getDevelopmentGatewayProxyURL()
        case .staging:
            apiConfigURL = UIApplication.shared.getStagingGatewayProxyURL()
        case .production:
            apiConfigURL = UIApplication.shared.getProductionGatewayProxyURL()
        }
        
        return "\(apiConfigURL)\(endpoint)"
    }
    
    class func headers(_ headers: [String: String]? = nil) -> HTTPHeaders? {
        let authHeaders: [String: String] = [
            "Authorization": "Bearer \(Helper().getAuthToken())",
            "Content-Type": "application/json"
        ]
        
        var combinedHeaders = authHeaders
        if let headers = headers {
            for (key, value) in headers {
                combinedHeaders[key] = value
            }
        }
        
//        print(combinedHeaders)
        
        return combinedHeaders
    }
    
    class func endPointForTerms() -> String {
        var termsString: String
        switch ApiConfig.appEnvironment {
        case .staging, .development:
            termsString = UIApplication.shared.getStagingTermsAndConditionsURL() + "?app=ios"
        case .production:
            termsString = UIApplication.shared.getProductionTermsAndConditionsURL() + "?app=ios"
        }
        return termsString
    }
    
    class func endPointForPrivacy() -> String {
        var privacyString: String
        switch ApiConfig.appEnvironment {
        case .staging, .development:
            privacyString = UIApplication.shared.getStagingPrivacyPolicyURL() + "?app=ios"
        case .production:
            privacyString = UIApplication.shared.getProductionPrivacyPolicyURL() + "?app=ios"
        }
        return privacyString
    }
    
    class func sminqWebAppUrl() -> String {
        var webAppURLLink: String
        switch ApiConfig.appEnvironment {
        case .staging, .development:
            webAppURLLink = UIApplication.shared.getStagingAppWebURL() + "?app=ios"
        case .production:
            webAppURLLink = UIApplication.shared.getProductionAppWebURL() + "?app=ios"
        }
        return webAppURLLink
    }
    
    class func cleverTapToken() -> String {
        var cleverTapToken: String
        switch ApiConfig.appEnvironment {
        case .staging, .development:
            cleverTapToken = UIApplication.shared.getStagingCleverTapToken()
        case .production:
            cleverTapToken = UIApplication.shared.getProductionCleverTapToken()
        }
        return cleverTapToken
    }
    
    class func cleverTapAccountId() -> String {
        var cleverTapAccountId: String
        switch ApiConfig.appEnvironment {
        case .staging, .development:
            cleverTapAccountId = UIApplication.shared.getStagingCleverTapAccountId()
        case .production:
            cleverTapAccountId = UIApplication.shared.getProductionCleverTapAccountId()
        }
        return cleverTapAccountId
    }
    
    class func cloudinaryCloudName() -> String {
        var cloudinaryCloudName: String
        switch ApiConfig.appEnvironment {
        case .staging, .development:
            cloudinaryCloudName = ApiConfig.stagingCloudinaryCloudName!
        case .production:
            cloudinaryCloudName = ApiConfig.productionCloudinaryCloudName!
        }
        return cloudinaryCloudName
    }
    
    class func cloudinaryApiKey() -> String {
        var cloudinaryApiKey: String
        switch ApiConfig.appEnvironment {
        case .staging, .development:
            cloudinaryApiKey = ApiConfig.stagingCloudinaryAPIKey!
        case .production:
            cloudinaryApiKey = ApiConfig.productionCloudinaryAPIKey!
        }
        return cloudinaryApiKey
    }
    
    class func cloudinaryApiSecret() -> String {
        var cloudinarySecretKey: String
        switch ApiConfig.appEnvironment {
        case .staging, .development:
            cloudinarySecretKey = ApiConfig.stagingCloudinarySecretKey!
        case .production:
            cloudinarySecretKey = ApiConfig.productionCloudinarySecretKey!
        }
        return cloudinarySecretKey
    }
    
    class func endPointForBuildVersion() -> String {
        return (Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String)!
    }
    
    class func placeFeedFormatedAddress(rawAddressString: String) -> String {
        let rawAddress    = rawAddressString
        var rawAddressArray = rawAddress.components(separatedBy: ",")
        
        if rawAddressArray.count > 0 {
            rawAddressArray = rawAddressArray.map { $0.trimmingCharacters(in: .whitespaces) }
            
            if rawAddressArray.count > Int(5) {
                return "\(rawAddressArray[rawAddressArray.count-4]), \(rawAddressArray[rawAddressArray.count-3])"
            } else if rawAddressArray.count == Int(1) {
                return "\(rawAddressArray[0])"
            } else if rawAddressArray.count <= Int(2) {
                return "\(rawAddressArray[0])"
            } else {
                return "\(rawAddressArray[rawAddressArray.count-3]), \(rawAddressArray[rawAddressArray.count-1])"
            }
        } else {
            return ""
        }
       
    }
    
    class func formattedAddressFromAddressComponent(addressComponent: AddressComponents?) -> String {
        var address: String = ""
        
        if let locality = addressComponent?.locality, let subLocality = addressComponent?.subLocality {
            if subLocality != "" && locality != "" {
                address += "• " + subLocality + ", " + locality
            } else if subLocality == "" && locality != "" {
                address += "• " + locality
            } else if subLocality != "" && locality == "" {
                address += "• " + subLocality
            }
        }
        
        return address
    }
    
    class func formattedAddressFromAddressComponentWithoutInitialDots(addressComponent: AddressComponents?) -> String {
        var address: String = ""
        
        if let locality = addressComponent?.locality, let subLocality = addressComponent?.subLocality {
            if subLocality != locality {
                if subLocality != "" && locality != "" {
                    address += subLocality + ", " + locality
                } else if subLocality == "" && locality != "" {
                    address += locality
                } else if subLocality != "" && locality == "" {
                    address += subLocality
                }
            } else {
                address += locality
            }
            
        }
        
        return address
    }
    
    class func getSubLocalityFromAddressComponent(addressComponent: AddressComponents?) -> String {
        var address: String = ""
        
        if addressComponent?.subLocality != nil && addressComponent?.subLocality != "" {
            if let subLocality = addressComponent?.subLocality {
                address += "• " + subLocality
            }
        }
    
        return address
    }
    
    class func distanceFormatedAsPerCountry(rawDistanceDouble: Double) -> String {
        let possibleCountryName = RemoteConfig.remoteConfig().configValue(forKey: "countries_for_miles").stringValue!
        
        let countryListForMilesFormate = possibleCountryName.components(separatedBy: "|")
        
        if let userLocationCountry = UserDefaults.standard.value(forKey: "UserLocationCountry") {
            let userLocationCountryLowerCase = String(describing: userLocationCountry).lowercased()

            if countryListForMilesFormate.contains(userLocationCountryLowerCase) {
                return UnitConverstionHelper.convertMetersToMiles(distance: rawDistanceDouble)
            } else {
                return UnitConverstionHelper.convertMetersToKilometers(distance: rawDistanceDouble)
            }
        } else {
            return UnitConverstionHelper.getTargetSpecificConversionFromMeters(distance: rawDistanceDouble)
        }
    }
    
    class func getDistanceUnit() -> String {
        let possibleCountryName = RemoteConfig.remoteConfig().configValue(forKey: "countries_for_miles").stringValue!
        
        let countryListForMilesFormate = possibleCountryName.components(separatedBy: "|")
        
        if let userLocationCountry = UserDefaults.standard.value(forKey: "UserLocationCountry") {
            let userLocationCountryLowerCase = String(describing: userLocationCountry).lowercased()
            
            if countryListForMilesFormate.contains(userLocationCountryLowerCase) {
                return "mi"
            } else {
                return "km"
            }
        } else {
            return "mi"
        }
    }
    
    class func distanceFormated(rawDistanceDouble: Double) -> String {        
        return distanceFormatedAsPerCountry(rawDistanceDouble: rawDistanceDouble)
    }
    
    
}
