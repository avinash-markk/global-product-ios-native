//
//  MediaCacheManagerService.swift
//  Sminq
//
//  Created by Pushkar Deshmukh on 25/01/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import Foundation

class MediaCacheManagerService {
    private init() {} // So that other classes don't create object of this class
    
    static let shared = MediaCacheManagerService()
    
    let fileManager = FileManager.default
    
    /// Saves feed media in document directory /storyView folder
    /// And invalidates cache based on hours and buffer size
    
    /// - Parameter feed - UserDataPlaceFeed: Feed Object
    /// - Returns: Nothing
    
    func saveFeedMediaInDocumentDirectory(feed: UserDataPlaceFeed, completion: @escaping(URL, Data) -> Void) {
        _ = FileManagerService.shared.createFolder(folderName: DocumentDirectoryFolders.storyView.rawValue)
        
        guard let otherContext = feed.getOtherContext(),
            let imageName = otherContext.image,
            let videoName = otherContext.video else { return }
        
        var absoluteURL: URL? = nil
        
        var fileExtension: MediaExtensions
        
        if otherContext.video != "" {
            absoluteURL = CloudinaryService.shared.generateCloudinaryVideoURL(folder: FIRRemoteConfigService.shared.getCloudinaryVideoFolder(), fileName: videoName)
            fileExtension = .mp4
        } else {
            absoluteURL = CloudinaryService.shared.generateCloudinaryImageURL(folder: FIRRemoteConfigService.shared.getCloudinaryOriginalImageFolder(), fileName: imageName)
            fileExtension = .jpg
        }
        
        guard let url = absoluteURL else { return }
        
        let session = URLSession.shared
        
        let task = session.dataTask(with: url) { (data, response, error) in
            guard let data = data,
                let documentsUrl = self.fileManager.urls(for: .documentDirectory, in: .userDomainMask).first else { return }
            
            let path = DocumentDirectoryFolders.storyView.rawValue + "/" + feed._id + "." + fileExtension.rawValue
            
            let fileUrl = documentsUrl.appendingPathComponent(path)
            
            do {
                try data.write(to: fileUrl)
                DispatchQueue.main.async {
                    completion(fileUrl, data)
                }
                
                LogUtil.info("Saved \(fileExtension.rawValue) to cache!")
            } catch {
                LogUtil.info(error)
            }
            
            // NOTE: Invalidate cache - 1. Delete posts older than 2 days 2. Delete posts if cache size is exceeded
            MediaCacheManagerService.shared.invalidateFeedCachedMediaByHour(beforeHour: FIRRemoteConfigService.shared.getFeedCacheInvalidateInHours())
            MediaCacheManagerService.shared.invalidateFeedCachedMediaByBufferSize(bufferSizeString: FIRRemoteConfigService.shared.getFeedCacheMaxBufferSizeInMbs())
        }
        
        task.resume()
    }
    
    func saveFeedMediaInDocumentDirectory(feed: UserDataPlaceFeed, with downloadedData: Data, completion: @escaping(URL) -> Void) {
        _ = FileManagerService.shared.createFolder(folderName: DocumentDirectoryFolders.storyView.rawValue)
        
        guard let otherContext = feed.getOtherContext() else { return }
        
        var fileExtension: MediaExtensions
        
        if otherContext.video != "" {
            fileExtension = .mp4
        } else {
            fileExtension = .jpg
        }
        
        guard let documentsUrl = self.fileManager.urls(for: .documentDirectory, in: .userDomainMask).first else { return }
        
        let path = DocumentDirectoryFolders.storyView.rawValue + "/" + feed._id + "." + fileExtension.rawValue
        
        let fileUrl = documentsUrl.appendingPathComponent(path)
        
        do {
            if !FileManager.default.fileExists(atPath: fileUrl.path) {
                try downloadedData.write(to: fileUrl)
                
                DispatchQueue.main.async {
                    completion(fileUrl)
                }
            }
        
            LogUtil.info("Saved \(fileExtension.rawValue) to cache!")
        } catch {
            LogUtil.info(error)
        }
        
        // NOTE: Invalidate cache - 1. Delete posts older than 2 days 2. Delete posts if cache size is exceeded
        MediaCacheManagerService.shared.invalidateFeedCachedMediaByHour(beforeHour: FIRRemoteConfigService.shared.getFeedCacheInvalidateInHours())
        MediaCacheManagerService.shared.invalidateFeedCachedMediaByBufferSize(bufferSizeString: FIRRemoteConfigService.shared.getFeedCacheMaxBufferSizeInMbs())
    }
    
    /// Returns the document directory URL for cached feed media inside /storyView folder 
    
    /// - Parameter feed - UserDataPlaceFeed: Feed Object
    /// - Returns: URL
    
    func getFeedMediaDocumentURL(feed: UserDataPlaceFeed) -> URL? {
        guard let documentsUrl = self.fileManager.urls(for: .documentDirectory, in: .userDomainMask).first else { return nil }
        
        guard let otherContext = feed.getOtherContext() else { return nil }
        
        var fileExtension = ""
        
        if otherContext.video != "" {
            fileExtension = MediaExtensions.mp4.rawValue
        } else {
            fileExtension = MediaExtensions.jpg.rawValue
        }
        
        let fileName = DocumentDirectoryFolders.storyView.rawValue + "/" + feed._id + "." + fileExtension
        
        let fileUrl = documentsUrl.appendingPathComponent(fileName)
        
        if fileManager.fileExists(atPath: fileUrl.path) {
            return fileUrl
        } else {
            return nil
        }
    }
    
    /// Invalidates feed cache based on hour inside /storyView folder
    /// For example: If current time is 6PM, 'beforeHour' is 2 hours then it will delete all files created before (6PM-2) 4PM
    
    /// - Parameter beforeHour - String: Firebase configured value which is set to 48 hours
    /// - Returns: URL
    
    func invalidateFeedCachedMediaByHour(beforeHour: String) {
        guard let hoursTimeInterval = TimeInterval(beforeHour) else { return }
        
        let fileURLs = FileManagerService.shared.getAllFilesInFolder(folderName: .storyView)
        
        if fileURLs.count > 0 {
            for file in fileURLs {
                do {
                    let fileAttributes = try fileManager.attributesOfItem(atPath: file.path)
                    guard let fileCreationDate = fileAttributes[FileAttributeKey.creationDate] as? Date else { continue }
                    let currentTimeInterval = Date().timeIntervalSince1970
                    
                    if fileCreationDate.timeIntervalSince1970 < (currentTimeInterval - (hoursTimeInterval * 60 * 60)) {
                        // NOTE: File creation time is less than configured value. This file can be deleted
                        
                        do {
                            try fileManager.removeItem(at: file)
                        } catch {
                            LogUtil.error(error)
                        }
                    }
                } catch {
                    LogUtil.error(error)
                }
                
            }
            
        }
    }
    
    /// Invalidates feed cache based on buffer size inside /storyView folder
    
    /// - Parameter bufferSizeString - String: Maximum buffer size in string
    /// - Returns: Nothing
    
    func invalidateFeedCachedMediaByBufferSize(bufferSizeString bufferSizeInMb: String) {
        guard let configuredBufferSizeInMb = Int(bufferSizeInMb) else { return }
        
        invalidateFeedCachedMediaByBufferSize(bufferSizeInt: configuredBufferSizeInMb)
    }
    
    /// Invalidates feed cache based on buffer size inside /storyView folder
    /// For example: If cached media size is 301 MBs and max buffer size is set to 300 mb,
    /// then it will invalidate old cache to free up space.
    /// Old cache refers to the first 'X' number of files created. Here we consider file creation time
    
    /// - Parameter bufferSizeInt - Int: Maximum buffer size in Int
    /// - Returns: Nothing
    
    func invalidateFeedCachedMediaByBufferSize(bufferSizeInt bufferSize: Int) {
        let storyViewCachedSizeInBytes = FileManagerService.shared.getTotalFizeSizeInBytesInFolder(folderName: .storyView)
        let storyViewCachedSizeInMegaBytes = FileManagerService.shared.convertBytesToMegaBytes(bytes: storyViewCachedSizeInBytes)
        
        // convertBytesToMegaBytes() returns value in Double to get file size in MBs. Hence storyViewCachedSizeInMegaBytes needs to be converted to Int for checking
        if Int(storyViewCachedSizeInMegaBytes) >= bufferSize {
            // Delete older files
            
            let fileURLs = FileManagerService.shared.getOldFilesInFolder(folderName: .storyView, howMany: FIRRemoteConfigService.shared.getFeedCacheDeleteCount())
            
            if fileURLs.count > 0 {
                for file in fileURLs {
                    do {
                        try fileManager.removeItem(at: file)
                    } catch {
                        LogUtil.error(error)
                    }
                }
                
            }
        }
    }
    
}
