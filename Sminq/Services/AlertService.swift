//
//  AlertService.swift
//  Sminq
//
//  Created by Pushkar Deshmukh on 21/05/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import UIKit

protocol AlertServiceDelegate: NSObjectProtocol {
    func didOkTap()
}

class AlertService {
    public weak var delgate: AlertServiceDelegate?
    
    func showSessionExpieryWithOkButton(viewController: UIViewController) {
        let alertController = UIAlertController(title: "Session expired", message: "Please login again", preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "Ok", style: .default) { (_) in
            self.delgate?.didOkTap()
        }
        
        alertController.addAction(okAction)
        
        viewController.present(alertController, animated: true, completion: nil)
    }
}
