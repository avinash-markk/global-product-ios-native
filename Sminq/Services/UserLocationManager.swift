//
//  UserLocationManager.swift
//  Sminq
//
//  Created by Pushkar Deshmukh on 28/05/18.
//  Copyright © 2018 Suraj Sagare. All rights reserved.
//

import Foundation
import MapKit
import CleverTapSDK

protocol LocationServiceProtocol: NSObjectProtocol {
    func didUpdateLocations(location: CLLocation)
    func didChangeAuthorization(authStatus: CLAuthorizationStatus)
    func didUpdateLocationsForLessAccuracy(locations: [CLLocation])
    func didUpdateLocationsForBestAccuracy(locations: [CLLocation])
    func didFailWithErrorForLessAccuracy(error: Error)
    func didFailWithErrorForBestAccuracy(error: Error)
}

extension LocationServiceProtocol {
    func didUpdateLocationsForLessAccuracy(locations: [CLLocation]) {
    }
    
    func didUpdateLocationsForBestAccuracy(locations: [CLLocation]) {
    }
    
    func didFailWithErrorForLessAccuracy(error: Error) {
    }
    
    func didFailWithErrorForBestAccuracy(error: Error) {
    }
    
}

extension LocationServiceProtocol {
    func didUpdateLocations(location: CLLocation) {
    }
    
    func didChangeAuthorization(authStatus: CLAuthorizationStatus) {
    }
}

class UserLocationManager: NSObject, CLLocationManagerDelegate {
    static let SharedManager = UserLocationManager()
    
    private var locationManager = CLLocationManager()
    
    var currentLocation: CLLocationCoordinate2D?
    
    public weak var delegate: LocationServiceProtocol?
    
    var isLessAccurateLocationFetched = false
    var isBestAccurateLocationFetched = false
    
    var isRequestedLessAccurateLocation = false
    var isRequestedBestAccurateLocation = false
    
    var isErrorFetchingLessAccurateLocation = false
    var isErrorFetchingBestAccurateLocation = false
    
    var isFetchingLessAccurateLocation = false
    var isFetchingBestAccurateLocation = false
    
    var maxAttemptToFetchLessAccurateLocation = 2
    var attemptCountToFetchLessAccurateLocation = 0
    
    private override init() {
        super.init()
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.distanceFilter = kCLLocationAccuracyBest
    }
    
    func requestWhenInUseAuthorization() {
        self.locationManager.requestWhenInUseAuthorization()
        self.locationManager.startUpdatingLocation()
        
        // Stop location service after 3 seconds
        DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
            self.locationManager.stopUpdatingLocation()
        }
    }
    
    func isLocationServiceEnabled() -> Bool {
        if CLLocationManager.locationServicesEnabled() {
            return true
        } else {
            return false
        }
    }
    
//    func requestLocation() {
//        self.locationManager.requestLocation()
//    }
    
    func requestLocationsForPostFlow() {
        print("in requestLocationsForPostFlow")
        attemptCountToFetchLessAccurateLocation = 1
        
        self.resetLocationFlagsForPostFlow()
        
        // TODO: Calculate time to get less accurate location here
        self.locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
        
        self.locationManager.requestLocation()
    }
    
    func resetLocationFlagsForPostFlow() {
        self.isRequestedLessAccurateLocation = true
        self.isRequestedBestAccurateLocation = false
        
        self.isLessAccurateLocationFetched = false
        self.isBestAccurateLocationFetched = false
        
        self.isErrorFetchingLessAccurateLocation = false
        self.isErrorFetchingBestAccurateLocation = false
        
        self.isFetchingLessAccurateLocation = true
        self.isFetchingBestAccurateLocation = false
    }
    
    func requestLocationWithAccuracy(accuracy: CLLocationAccuracy) {
        self.locationManager.desiredAccuracy = accuracy
        self.locationManager.requestLocation()
    }
    
    func isLocationServiceAuthorized() -> Bool {
        if CLLocationManager.locationServicesEnabled() {
            switch(CLLocationManager.authorizationStatus()) {
            case .notDetermined, .restricted, .denied:
                return false
            case .authorizedAlways, .authorizedWhenInUse:
                return true
            }
        } else {
            return false
        }
    }
    
    func getCurrentLocation() -> CLLocationCoordinate2D? {
        return self.locationManager.location?.coordinate
    }
    
    func getCLLocation() -> CLLocation? {
        return self.locationManager.location
    }
    
    func getCachedLocationOtherwiseFallBackLocation() -> (latitude: Double, longitude: Double){
        var lat: Double?
        var long : Double?
        
        if let userLocation = UserDefaults.standard.value(forKey: "UserLocation") {
            lat = (userLocation as AnyObject).value(forKey: "latitude") as? Double
            long = (userLocation as AnyObject).value(forKey: "longitude") as? Double
        } else {
            let location = FIRRemoteConfigService.shared.getInitialLocationHomeScreenConfig()
            lat = location.latitude
            long = location.longitude
        }
        
        return (latitude: lat ?? 0, longitude: long ?? 0)
    }
    
    func getDefaultLocationOtherwiseFallbackCachedLocation() -> (latitude: Double, longitude: Double){
        var lat: Double?
        var long : Double?
        
        if (!self.isLocationServiceEnabled() || !self.isLocationServiceAuthorized()) {
            let location = FIRRemoteConfigService.shared.getInitialLocationHomeScreenConfig()
            lat = location.latitude
            long = location.longitude
        } else {
            if let userLocation = UserDefaults.standard.value(forKey: "UserLocation") {
                lat = (userLocation as AnyObject).value(forKey: "latitude") as? Double
                long = (userLocation as AnyObject).value(forKey: "longitude") as? Double
            }
        }
        
        return (latitude: lat ?? 0, longitude: long ?? 0)
    }
    
    func setCurrentUserLocationToDefaults() {
        if self.isLocationServiceEnabled() && self.isLocationServiceAuthorized() {
            let location = self.locationManager.location?.coordinate
            
            if location != nil {
                if let lat = location?.latitude, let long = location?.longitude {
                    self.setUserLocationToDefaults(latitude: lat, longitude: long)
                }
            }
        }
    }
    
    func setUserLocationToDefaults(latitude: CLLocationDegrees, longitude: CLLocationDegrees) {
        let locationDict: NSDictionary = ["latitude": latitude, "longitude": longitude]
//        print(locationDict)
        UserDefaults.standard.set(locationDict, forKey: "UserLocation")
    }
    
    func fetchPlacemarkFromLocation(location: CLLocation, completion: @escaping (_ placemark: CLPlacemark?, _ error: Error?) -> ()) {
        CLGeocoder().reverseGeocodeLocation(location, completionHandler: { placemarks, error in
           
            completion(placemarks?.first, error)
        })
    }
    
    func fetchCityAndCountry(location: CLLocation, completion: @escaping (_ city: String?, _ country:  String?, _ error: Error?) -> ()) {
        CLGeocoder().reverseGeocodeLocation(location) { placemarks, error in
            completion(placemarks?.first?.locality,
                       placemarks?.first?.country,
                       error)
        }
    }

    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("in didFailWithError: ", error)
        LogUtil.error(error)
        
        if self.isRequestedLessAccurateLocation && !self.isRequestedBestAccurateLocation {
            // Failed to get less accurate location
            attemptCountToFetchLessAccurateLocation += 1
            
            self.isFetchingLessAccurateLocation = false
            self.isErrorFetchingLessAccurateLocation = true
            
            self.delegate?.didFailWithErrorForLessAccuracy(error: error)
            
            
            if attemptCountToFetchLessAccurateLocation <= maxAttemptToFetchLessAccurateLocation {
                self.resetLocationFlagsForPostFlow()
                self.locationManager.requestLocation()
                AnalyticsHelper.locationFetchRetryEvent()
            }
            
        } else if !self.isRequestedLessAccurateLocation && self.isRequestedBestAccurateLocation {
            // Failed to get best accurate location
            
            self.isFetchingBestAccurateLocation = false
            self.isErrorFetchingBestAccurateLocation = true
            
            self.delegate?.didFailWithErrorForBestAccuracy(error: error)
            
        } else if !self.isRequestedLessAccurateLocation && !self.isRequestedLessAccurateLocation {
            // default request
            print("default request")
            
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        print(locations)
        
        if let location = locations.last {
            CleverTap().setLocation(location.coordinate)
            
            self.currentLocation = location.coordinate

            self.setUserLocationToDefaults(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
            UserDefaultsUtility.shared.setLastKnownLocation(location: location)
            
            if self.isRequestedLessAccurateLocation && !self.isRequestedBestAccurateLocation {
                // Requested for less accurate
                print("Found less accurate location")
                self.isLessAccurateLocationFetched = true
                self.isBestAccurateLocationFetched = false
                
                self.isRequestedLessAccurateLocation = false
                self.isRequestedBestAccurateLocation = true
                
                self.delegate?.didUpdateLocationsForLessAccuracy(locations: locations)
                
                self.isFetchingLessAccurateLocation = false
                self.isFetchingBestAccurateLocation = true
                
                // Request again to get best accurate location
                self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
                self.locationManager.requestLocation()
                
                UserDefaultsUtility.shared.setkCLLocationAccuracyHundredMeters(location: location)
                
            } else if !self.isRequestedLessAccurateLocation && self.isRequestedBestAccurateLocation {
                // Requested for best accurate
                print("Found best accurate location")
                self.isBestAccurateLocationFetched = true
                
                self.isRequestedBestAccurateLocation = false
                
                self.isFetchingBestAccurateLocation = false
                
                self.delegate?.didUpdateLocationsForBestAccuracy(locations: locations)
                
                UserDefaultsUtility.shared.setkCLLocationAccuracyBestLocation(location: location)
                
            } else if !self.isRequestedLessAccurateLocation && !self.isRequestedLessAccurateLocation {
                // default request
                
                self.delegate?.didUpdateLocations(location: location)
            }
            
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        DispatchQueue.main.async() { () -> Void in
            if(status == CLAuthorizationStatus.authorizedAlways || status == CLAuthorizationStatus.authorizedWhenInUse || status == CLAuthorizationStatus.restricted || status == CLAuthorizationStatus.denied) {
                // Store latest lat, long in DB
                let location = self.getCLLocation()
                
                if let tempLocation = location {
                    self.setUserLocationToDefaults(latitude: tempLocation.coordinate.latitude, longitude: tempLocation.coordinate.longitude)
                }
                
                self.delegate?.didChangeAuthorization(authStatus: status)
            }
        }
    }
}
