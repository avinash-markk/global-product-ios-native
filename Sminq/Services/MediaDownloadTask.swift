//
//  MediaDownloadTask.swift
//  Markk
//
//  Created by Pushkar Deshmukh on 20/06/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import Foundation

class MediaDownloadTask {
    let feed: UserDataPlaceFeed
    
    let session = URLSession(configuration: URLSessionConfiguration.default)
    
    fileprivate var task: URLSessionDownloadTask?
    private var resumeData: Data?
    
    private var isDownloading = false
    private var isFinishedDownloading = false
    
    init(feed: UserDataPlaceFeed) {
        self.feed = feed
    }
    
    func resume() {
        let url = MediaCacheManagerService.shared.getFeedMediaDocumentURL(feed: feed)
        if url != nil {
            return
        }
        
        if !isDownloading && !isFinishedDownloading {
            isDownloading = true
            
            if let resumeData = resumeData {
                task = session.downloadTask(withResumeData: resumeData, completionHandler: downloadTaskCompletionHandler)
            } else {
                guard let url = getUrl(for: feed) else { return }
                task = session.downloadTask(with: url, completionHandler: downloadTaskCompletionHandler)
            }
            
            task?.resume()
        }
    }
    
    func pause() {
        if isDownloading && !isFinishedDownloading {
            task?.cancel(byProducingResumeData: { (data) in
                self.resumeData = data
            })
            
            self.isDownloading = false
        }
    }
    
    private func downloadTaskCompletionHandler(url: URL?, response: URLResponse?, error: Error?) {
        if let error = error {
//            print("Error downloading: ", error)
            return
        }
        
        guard let url = url else { return }
        guard let data = FileManager.default.contents(atPath: url.path) else { return }
        let cachedUrl = MediaCacheManagerService.shared.getFeedMediaDocumentURL(feed: feed)
        
        if cachedUrl == nil {
            print("Not cached. Hence caching a media")
            MediaCacheManagerService.shared.saveFeedMediaInDocumentDirectory(feed: feed, with: data, completion: { _ in })
        }
        
        self.isFinishedDownloading = true
    }
    
    fileprivate func getUrl(for feed: UserDataPlaceFeed) -> URL? {
        guard let otherContext = feed.getOtherContext(),
            let imageName = otherContext.image,
            let videoName = otherContext.video else { return nil }
        
        var absoluteURL: URL? = nil
        
        if otherContext.video != "" {
            absoluteURL = CloudinaryService.shared.generateCloudinaryVideoURL(folder: FIRRemoteConfigService.shared.getCloudinaryVideoFolder(), fileName: videoName)
        } else {
            absoluteURL = CloudinaryService.shared.generateCloudinaryImageURL(folder: FIRRemoteConfigService.shared.getCloudinaryOriginalImageFolder(), fileName: imageName)
        }
        
        guard let url = absoluteURL else { return nil }
        return url
    }
}
