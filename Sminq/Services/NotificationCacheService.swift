//
//  NotificationCacheService.swift
//  Sminq
//
//  Created by Pushkar Deshmukh on 07/02/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import Foundation
import UserNotifications
import CoreData

class NotificationCacheService {
    private init() {}
    
    static let shared = NotificationCacheService()
//    var notifications: [NSManagedObject] = []
    
    // MARK: - Core Data stack
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "sminqdb")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    func cacheNotificationByUNNotification(unNotification: UNNotification) {
        var userInfo = unNotification.request.content.userInfo
        
        let timestamp = unNotification.date.timeIntervalSince1970
        let date = Date(timeIntervalSince1970: timestamp)
        let dateFormatter = DateFormatter()
        dateFormatter.locale = NSLocale.current
        
        dateFormatter.dateFormat = "dd/MM/yyyy hh:mm:ss a" // Specify your format that you want
        let formattedDate = dateFormatter.string(from: date)
        
        userInfo["notificationIdentifier"] = unNotification.request.identifier
        
        notificationExist(unNotificationIdentifier: unNotification.request.identifier) { (isExist) in
            if !isExist {
                self.saveNotification(userInfo: userInfo, formattedDate: formattedDate)
            }
        }
    }
    
    func cacheNotificationByUserInfo(userInfo: [AnyHashable: Any]) {
        let timestamp = Date().timeIntervalSince1970
        let date = Date(timeIntervalSince1970: timestamp)
        let dateFormatter = DateFormatter()
        dateFormatter.locale = NSLocale.current
        
        dateFormatter.dateFormat = "dd/MM/yyyy hh:mm:ss a" // Specify your format that you want
        let formattedDate = dateFormatter.string(from: date)
        
        saveNotification(userInfo: userInfo, formattedDate: formattedDate)
    }
    
    func getNotificationType(userInfo: [AnyHashable: Any]) -> String {
        if userInfo["type"] != nil {
            return userInfo["type"] as? String ?? ""
        } else {
            return ""
        }
    }
    
    func saveNotification(userInfo: [AnyHashable: Any], formattedDate: String) {
        guard let aps = userInfo["aps"] as? NSDictionary else { return }
        guard let alert = aps["alert"] as? NSDictionary else { return }
        guard let body = alert["body"] as? String else { return }
        guard let title = alert["title"] as? String else { return }
    
        var place: String = ""
        var type: String = ""
        var feedItemId: String = ""
        var feedItemUserId: String = ""
        var follower: String = ""
        var image = ""
        var mediaType = ""
        var notificationIdentifier = ""
        
        if userInfo["place"] != nil {
            place = userInfo["place"] as? String ?? ""
        }
        
        if userInfo["type"] != nil {
            type = userInfo["type"] as? String ?? ""
        }
        
        if userInfo["feedItemId"] != nil {
            feedItemId = userInfo["feedItemId"] as? String ?? ""
        }
        
        if userInfo["feedItemUserId"] != nil {
            feedItemUserId = userInfo["feedItemUserId"] as? String ?? ""
        }
        
        if userInfo["follower"] != nil {
            follower = userInfo["follower"] as? String ?? ""
        }
        
        if userInfo["image"] != nil {
            image = userInfo["image"] as? String ?? ""
        }
        
        if userInfo["mediaType"] != nil {
            mediaType = userInfo["mediaType"] as? String ?? ""
        }
        
        if userInfo["notificationIdentifier"] != nil {
            notificationIdentifier = userInfo["notificationIdentifier"] as? String ?? ""
        }
        
        if type == "" {
            return
        }
        
        let managedContext = self.persistentContainer.viewContext
        
        let entity = NSEntityDescription.entity(forEntityName: CoreDataTables.notificationTable.rawValue,
                                                in: managedContext)!
        
        let notification = NSManagedObject(entity: entity,
                                           insertInto: managedContext)
        
        // Store values to table
        notification.setValue(place, forKeyPath: "place")
        notification.setValue(body, forKeyPath: "body")
        notification.setValue(title, forKeyPath: "title")
        notification.setValue(type, forKeyPath: "type")
        notification.setValue(feedItemId, forKeyPath: "feedItemId")
        notification.setValue(formattedDate, forKeyPath: "time")
        notification.setValue(follower, forKeyPath: "follower")
        notification.setValue(feedItemUserId, forKeyPath: "feedItemUserId")
        notification.setValue(image, forKeyPath: "image")
        notification.setValue(mediaType, forKeyPath: "mediaType")
        notification.setValue(notificationIdentifier, forKeyPath: "notificationIdentifier")
        
        do {
            try managedContext.save()
            let unreadCount = UserDefaults.standard.integer(forKey: "notificationUnreadCount") + 1
            UserDefaults.standard.set(unreadCount, forKey: "notificationUnreadCount")
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: AppConstants.AppNotificationNames.updateNotification), object: nil, userInfo: nil)
            NotificationCenter.default.post(name: Notification.Name(AppConstants.AppNotificationNames.refreshNotificationList), object: self)
        } catch let error as NSError {
            LogUtil.error(error)
        }
    }
    
    func getAllCachedNotifications(completion: @escaping(_ cachedNotifications: [NSManagedObject]) -> Void) {
        let managedContext = self.persistentContainer.viewContext
        var notifications: [NSManagedObject] = []
        
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: CoreDataTables.notificationTable.rawValue)
        
        do {
            notifications = try managedContext.fetch(fetchRequest)
        } catch let error as NSError {
            LogUtil.error(error)
        }
        
        DispatchQueue.main.async {
            completion(notifications)
        }
    }
    
    func clearCachedNotifications(completion: @escaping() -> Void) {
        let context = self.persistentContainer.viewContext
        
        let deleteFetch = NSFetchRequest<NSFetchRequestResult>(entityName: CoreDataTables.notificationTable.rawValue)
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteFetch)
        
        do {
            try context.execute(deleteRequest)
            try context.save()
        } catch {
            LogUtil.error(error)
        }
        
        DispatchQueue.main.async {
            completion()
        }
    }
    
    func notificationExist(unNotificationIdentifier: String, completion: @escaping(_ notificationExist: Bool) -> Void) {
        self.getAllCachedNotifications { (notifications) in
            var notificationFound = false
            
            for notification in notifications {
                guard let notificationIdentifier = notification.value(forKeyPath: "notificationIdentifier") as? String else { return }
                
                if notificationIdentifier == unNotificationIdentifier {
                    notificationFound = true
                }
            }
            
            completion(notificationFound)
        }
    }
}
