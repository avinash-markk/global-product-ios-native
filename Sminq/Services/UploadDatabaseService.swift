//
//  UploadDatabaseService.swift
//  Sminq
//
//  Created by Avinash Thakur on 15/02/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import CoreData

class UploadDatabaseService {
    
    private init() {
    }
    
    static let shared = UploadDatabaseService()
    
    lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "sminqdb")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    func checkIfTaskAlreadyExists(taskID: String) -> Bool {
        let context = self.persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: CoreDataTables.feedTable.rawValue)
        fetchRequest.predicate = NSPredicate(format: "feedID = %@", taskID)
        
        var results: [NSManagedObject] = []
        
        do {
            results = try context.fetch(fetchRequest)
        }
        catch {
            LogUtil.error(error)
        }
        
        return results.count > 0
    }
    
    func deleteAllRecords() {
        let context = self.persistentContainer.viewContext
        let fetch = NSFetchRequest<NSFetchRequestResult>(entityName: CoreDataTables.feedTable.rawValue)
        let request = NSBatchDeleteRequest(fetchRequest: fetch)
        
        do {
            let result = try context.execute(request)
        }
        catch {
            LogUtil.error(error)
        }
    }
    
    func updateStatusForFeed(feedID: String, newStatus: String) {
        let context = self.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: CoreDataTables.feedTable.rawValue)
        fetchRequest.predicate = NSPredicate(format: "feedID = %@", feedID)
        do {
            if let records = try context.fetch(fetchRequest) as? [NSManagedObject] {
                for record in records {
                    record.setValue(newStatus, forKey: "feedStatus")
                }
            }
        } catch {
            LogUtil.error(error)
        }
        
        do {
            try context.save()
        }
        catch {
            print("Saving Core Data Failed: \(error)")
        }
    }
    
    func getStatusForFeed(feedID: String) -> String {
        let context = self.persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: CoreDataTables.feedTable.rawValue)
        fetchRequest.predicate = NSPredicate(format: "feedID = %@", feedID)
        do {
            if let records = try context.fetch(fetchRequest) as? [NSManagedObject] {
                for record in records {
                    return record.value(forKey: "feedStatus") as! String
                }
            }
        } catch {
            LogUtil.error(error)
        }
        return ""
    }
    
    func getFilePathFor(feedID: String) -> (String) {
        var filePath: String = ""
        
        let context = self.persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: CoreDataTables.feedTable.rawValue)
        fetchRequest.predicate = NSPredicate(format: "feedID = %@", feedID)
        do {
            if let records = try context.fetch(fetchRequest) as? [NSManagedObject] {
                for record in records {
                    if let feedLocalPath = record.value(forKey: "feedLocalPath") as? String {
                        filePath = feedLocalPath
                    }
                }
            }
        } catch {
            LogUtil.error(error)
        }
        
        return filePath
    }
    
    func fetchFeedWith(feedID: String) {
        let context = self.persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: CoreDataTables.feedTable.rawValue)
        fetchRequest.predicate = NSPredicate(format: "feedID = %@", feedID)
        do {
            if let records = try context.fetch(fetchRequest) as? [NSManagedObject] {
                for record in records {
                    print("saved feed: \(record)")
                }
            }
            
        } catch {
            LogUtil.error(error)
        }
    }
    
    func deleteFeedWithFeed(feedID: String) {
        let context = self.persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: CoreDataTables.feedTable.rawValue)
        fetchRequest.predicate = NSPredicate(format: "feedID = %@", feedID)
        do {
            if let result = try context.fetch(fetchRequest) as? [NSManagedObject] {
                for object in result {
                    let isVideo = object.value(forKey: "feedIsVideo") as! String
                    self.deleteFromDocDirectory(feedId: feedID, isVideo: isVideo == "true")
                    
                    context.delete(object)
                }
            }
            do {
                try context.save()
            } catch {
                LogUtil.error(error)
            }
        } catch {
            LogUtil.error(error)
        }
    }
    
    func deleteUploadedFeeds() {
        let completedStatus = "Completed"
        let context = self.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: CoreDataTables.feedTable.rawValue)
        fetchRequest.predicate = NSPredicate(format: "feedStatus = %@", completedStatus)
        do {
            if let result = try context.fetch(fetchRequest) as? [NSManagedObject] {
                for object in result {
                    let feedID = object.value(forKey: "FeedID")
                    let isVideo = object.value(forKey: "feedIsVideo") as! String
                    self.deleteFromDocDirectory(feedId: feedID as! String, isVideo: isVideo == "true")
                    context.delete(object)
                }
            }
            do {
                try context.save()
            } catch {
                LogUtil.error(error)
            }
        } catch {
            LogUtil.error(error)
        }
    }
    
    func getFailedFeedsToRetry() -> NSMutableArray {
        let resultArray = NSMutableArray()
        let inProgressStatus = "InProgress"
        let failedStatus = "Failed"
        let pendingStatus = "Pending"
        
        let context = self.persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: CoreDataTables.feedTable.rawValue)
        
        fetchRequest.predicate = NSPredicate(format: "feedStatus = %@", inProgressStatus)
        do {
            if let records = try context.fetch(fetchRequest) as? [NSManagedObject] {
                for record in records {
                    let feedID = record.value(forKey: "FeedID")
                    let feedData = record.value(forKey: "feedData")
                    let feedPlaceId = record.value(forKey: "feedPlaceId")
                    let feedSubPlaceId = record.value(forKey: "feedSubPlaceId")
                    let feedStatus = record.value(forKey: "feedStatus")
                    let feedIsVideo = record.value(forKey: "feedIsVideo")
                    let feedDuration = record.value(forKey: "feedDuration")
                    let feedPlaceName = record.value(forKey: "feedPlaceName")
                    let textListString = record.value(forKey: "textList")
                    let stickerListString = record.value(forKey: "stickerList")
                    
                    let isFeedOld =  Helper().checkWhetherDateIsOlder(givenDateString: feedID as! String)
                    
                    if isFeedOld == true {
                        self.deleteFeedWithFeed(feedID: feedID as! String)
                    } else {
                        let uploadDict = ["feedID": feedID, "feedData": feedData, "feedPlaceId": feedPlaceId, "feedSubPlaceId": feedSubPlaceId, "feedStatus": feedStatus, "feedIsVideo": feedIsVideo, "feedDuration": feedDuration, "feedPlaceName": feedPlaceName, "textList": textListString, "stickerList": stickerListString]
                        resultArray.add(uploadDict)
                    }
                }
            }
        } catch {
            LogUtil.error(error)
        }
        
        let context2 = self.persistentContainer.viewContext
        
        let fetchRequest2 = NSFetchRequest<NSFetchRequestResult>(entityName: CoreDataTables.feedTable.rawValue)
        
        fetchRequest2.predicate = NSPredicate(format: "feedStatus = %@", failedStatus)
        do {
            if let records2 = try context2.fetch(fetchRequest2) as? [NSManagedObject] {
                for record in records2 {
                    let feedID = record.value(forKey: "FeedID")
                    let feedData = record.value(forKey: "feedData")
                    let feedPlaceId = record.value(forKey: "feedPlaceId")
                    let feedSubPlaceId = record.value(forKey: "feedSubPlaceId")
                    let feedStatus = record.value(forKey: "feedStatus")
                    let feedIsVideo = record.value(forKey: "feedIsVideo")
                    let feedDuration = record.value(forKey: "feedDuration")
                    let feedPlaceName = record.value(forKey: "feedPlaceName")
                    let textListString = record.value(forKey: "textList")
                    let stickerListString = record.value(forKey: "stickerList")
                    
                    let isFeedOld =  Helper().checkWhetherDateIsOlder(givenDateString: feedID as! String)
                    if isFeedOld == true {
                        self.deleteFeedWithFeed(feedID: feedID as! String)
                    } else {
                        let uploadDict = ["feedID": feedID, "feedData": feedData, "feedPlaceId": feedPlaceId, "feedSubPlaceId": feedSubPlaceId, "feedStatus": feedStatus, "feedIsVideo": feedIsVideo, "feedDuration": feedDuration, "feedPlaceName": feedPlaceName, "textList": textListString, "stickerList": stickerListString]
                        resultArray.add(uploadDict)
                    }
                }
            }
        } catch {
            LogUtil.error(error)
        }
        
        let context3 = self.persistentContainer.viewContext
        
        let fetchRequest3 = NSFetchRequest<NSFetchRequestResult>(entityName: CoreDataTables.feedTable.rawValue)
        
        fetchRequest3.predicate = NSPredicate(format: "feedStatus = %@", pendingStatus)
        do {
            if let records3 = try context3.fetch(fetchRequest3) as? [NSManagedObject] {
                for record in records3 {
                    let feedID = record.value(forKey: "FeedID")
                    let feedData = record.value(forKey: "feedData")
                    let feedPlaceId = record.value(forKey: "feedPlaceId")
                    let feedSubPlaceId = record.value(forKey: "feedSubPlaceId")
                    let feedStatus = record.value(forKey: "feedStatus")
                    let feedIsVideo = record.value(forKey: "feedIsVideo")
                    let feedDuration = record.value(forKey: "feedDuration")
                    let feedPlaceName = record.value(forKey: "feedPlaceName")
                    let textListString = record.value(forKey: "textList")
                    let stickerListString = record.value(forKey: "stickerList")
                    
                    let isFeedOld =  Helper().checkWhetherDateIsOlder(givenDateString: feedID as! String)
                    if isFeedOld == true {
                        self.deleteFeedWithFeed(feedID: feedID as! String)
                    } else {
                        let uploadDict = ["feedID": feedID, "feedData": feedData, "feedPlaceId": feedPlaceId, "feedSubPlaceId": feedSubPlaceId, "feedStatus": feedStatus, "feedIsVideo": feedIsVideo, "feedDuration": feedDuration, "feedPlaceName": feedPlaceName, "textList": textListString, "stickerList": stickerListString]
                        resultArray.add(uploadDict)
                    }
                }
            }
        } catch {
            LogUtil.error(error)
        }
        return resultArray
    }
    
    func getDetailsForAddToStoryMode() -> NSMutableDictionary? {
        let context = self.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: CoreDataTables.feedTable.rawValue)
        
        let sortDescriptor = NSSortDescriptor(key: "feedID", ascending: false)
        fetchRequest.sortDescriptors = [sortDescriptor]
        fetchRequest.fetchLimit = 1
        
        do {
            if let records = try context.fetch(fetchRequest) as? [NSManagedObject] {
                for record in records {
                    let feedData = record.value(forKey: "feedData")
                    let feedPlaceId = record.value(forKey: "feedPlaceId")
                    let feedSubPlaceId = record.value(forKey: "feedSubPlaceId")
                    let feedPlaceName = record.value(forKey: "feedPlaceName")
                    let textList = record.value(forKey: "textList")
                    
                    let dataDictionary = NSMutableDictionary()
                    dataDictionary.setValue(feedData, forKey: "feedData")
                    dataDictionary.setValue(feedPlaceId, forKey: "feedPlaceId")
                    dataDictionary.setValue(feedSubPlaceId, forKey: "feedSubPlaceId")
                    dataDictionary.setValue(feedPlaceName, forKey: "feedPlaceName")
                    dataDictionary.setValue(textList, forKey: "textList")
                    
                    return dataDictionary
                }
            }
        } catch {
            LogUtil.error(error)
        }
        
        return nil
    }
    
    func saveFeedToUpload(feedID: String, feedData: String, feedPlaceId: String, feedStatus: String, feedSubPlaceID: String, feedIsVideo: String, feedDuration: String, feedPlaceName: String, textListString: String, stickerListString: String) {
        
        let context = self.persistentContainer.viewContext
        
        let entity = NSEntityDescription.entity(forEntityName: CoreDataTables.feedTable.rawValue, in: context)
        let newFeed = NSManagedObject(entity: entity!, insertInto: context)
        
        newFeed.setValue(feedID, forKey: "feedID")
        newFeed.setValue(feedData, forKey: "feedData")
        newFeed.setValue(feedPlaceId, forKey: "feedPlaceId")
        newFeed.setValue(feedSubPlaceID, forKey: "feedSubPlaceId")
        newFeed.setValue(feedStatus, forKey: "feedStatus")
        newFeed.setValue(feedIsVideo, forKey: "feedIsVideo")
        newFeed.setValue(feedDuration, forKey: "feedDuration")
        newFeed.setValue(feedPlaceName, forKey: "feedPlaceName")
        newFeed.setValue(textListString, forKey: "textList")
        newFeed.setValue(stickerListString, forKey: "stickerList")
        
        do {
            try context.save()
        } catch {
            LogUtil.error(error)
        }
    }
    
    func deleteFromDocDirectory(filePath: String!) {
        if filePath != "" {
            if FileManager.default.fileExists(atPath: filePath) {
                do {
                    try FileManager.default.removeItem(atPath: filePath)
                } catch {
                    LogUtil.error(error)
                }
            }
        }
    }
    
    func deleteFromDocDirectory(feedId: String, isVideo: Bool) {
        if isVideo {
            let videofileUrlPath = DocDirectoryUtil().getLocalUrlForTask(id: feedId, isVideo: isVideo)
            self.deleteFromDocDirectory(filePath: videofileUrlPath)
            // delete video thumbnail
            let videoThumbnailUrlPath = DocDirectoryUtil().getLocalUrlForTask(id: feedId, isVideo: !isVideo)
            self.deleteFromDocDirectory(filePath: videoThumbnailUrlPath)
        } else {
            let fileUrlPath = DocDirectoryUtil().getLocalUrlForTask(id: feedId, isVideo: isVideo)
            self.deleteFromDocDirectory(filePath: fileUrlPath)
        }
    }
    
    func deleteOutputFile() {
        var fileURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
        fileURL = fileURL.appendingPathComponent("/Camera")
        do {
            let filePaths = try FileManager().contentsOfDirectory(atPath: fileURL.path)
            for filePath in filePaths {
                let url = fileURL.appendingPathComponent(filePath)
                            print(url)
                try FileManager().removeItem(atPath: url.path)
            }
        } catch {
            LogUtil.error(error)
        }
    }
    
}
