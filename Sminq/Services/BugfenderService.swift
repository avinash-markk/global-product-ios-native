//
//  BugfenderService.swift
//  Sminq
//
//  Created by Pushkar Deshmukh on 11/01/19.
//  Copyright © 2019 Suraj Sagare. All rights reserved.
//

import Foundation
import BugfenderSDK

class BugfenderService: NSObject {
    static let shared = BugfenderService()
    
    private override init() {}
    
    func configure() {
        var bugfenderAppKey: String = ""
        
        switch ApiConfig.appEnvironment {
        case .staging, .development:
            bugfenderAppKey = UIApplication.shared.getStagingBugfenderAppKey()
            Bugfender.setPrintToConsole(true)
        case .production:
            bugfenderAppKey = UIApplication.shared.getProductionBugfenderAppKey()
            Bugfender.setPrintToConsole(false)
        }
        
        Bugfender.enableCrashReporting()
        Bugfender.activateLogger(bugfenderAppKey)
        BugfenderService.shared.setBugFenderDeviceString(user: Helper().getUser())
    }
    
    func setBugFenderDeviceString(user: UserDataDict?) {
        guard let u = user else {
            return
        }
        
        Bugfender.setDeviceString(u._id, forKey: "user.id")
        Bugfender.setDeviceString(u.displayName.first ?? "", forKey: "user.displayName")
    }
    
}
