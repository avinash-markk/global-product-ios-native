//
//  DocDirectoryUtil.swift
//  Sminq
//
//  Created by Avinash Thakur on 12/04/19.
//  Copyright © 2019 Sminq. All rights reserved.
//
import UIKit

class DocDirectoryUtil {
    
    /**
     Saves image into document directory for given feedId & returns the file url for the saved image
     
     - Parameters:
        - feedId: - String: Feed id
        - image: - UIImage: Image to be saved
     
     - Returns: URL: Returns file url for image saved in document directory
     */
    
    func saveImageLocallyAndReturnURL(feedId: String, image: UIImage) -> URL {
        let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        let tempFileURL = documentsDirectory.appendingPathComponent("\(feedId).jpg")
        if let imageData: Data = UIImageJPEGRepresentation(image, 1.0),
            !FileManager.default.fileExists(atPath: tempFileURL.path) {
            do {
                try imageData.write(to: tempFileURL)
            } catch {
                LogUtil.error(error)
            }
        }
        return tempFileURL
    }
    
    /**
     Saves image into document directory for given feedId
     
     - Parameters:
        - feedId: - String: Feed id
        - image: - UIImage: Image to be saved
     
     */
    
    func saveImageIntoDocDirectory(feedId: String, image: UIImage) {
        _ = self.saveImageLocallyAndReturnURL(feedId: feedId, image: image)
    }
    
    /**
     Saves video file into document directory for given feedId & returns the file url for the saved video
     
     - Parameters:
        - feedId: - String: Feed id
        - feedUrl: - String: Url of the video to be saved.
     
     - Returns: URL: Returns file url for video saved in document directory
     */
    
    func saveVideoLocallyAndReturnURL(feedId: String, feedUrl: String) -> URL {
        let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        let tempFileURL = documentsDirectory.appendingPathComponent("\(feedId).mov")
        if let videoData: NSData = NSData(contentsOfFile: feedUrl),
            !FileManager.default.fileExists(atPath: tempFileURL.path) {
            do {
                try videoData.write(to: tempFileURL)
            } catch {
                LogUtil.error(error)
            }
            
            UploadDatabaseService.shared.deleteFromDocDirectory(filePath: feedUrl)
            UploadDatabaseService.shared.deleteOutputFile()
        }
        return tempFileURL
    }
    
    /**
     Returns local file url path for given feed Id stored in document directory
     
     - Parameters:
        - id: - String: Feed id
        - isVideo: - Bool: Media type image / video
     
     - Returns: String: Returns file path
     */
    
    func getLocalUrlForTask(id: String, isVideo: Bool) -> String {
        let fileName = isVideo == true ? "\(id).mov" : "\(id).jpg"
        let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        let newfileURL = documentsDirectory.appendingPathComponent(fileName)
        return newfileURL.path
    }
    
    /**
     Checks if the file with given feed id whether image or video exists in document directory
     
     - Parameters:
        - feedId: - String: feed id
        - isVideo: - Bool: media type image / video
     
     - Returns: Bool: true / false
     */
    
    func checkifFileExistInDocDirectory(feedId: String, isVideo: Bool) -> Bool {
        let fileManager = FileManager.default
        let fileUrlPath = self.getLocalUrlForTask(id: feedId, isVideo: isVideo)
        
        if fileManager.fileExists(atPath: fileUrlPath) {
            return true
        } else {
            return false
        }
    }
    
}
