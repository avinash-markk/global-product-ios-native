//
//  UnitConverstionHelper.swift
//  Sminq
//
//  Created by Pushkar Deshmukh on 24/01/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import UIKit
import Foundation

class UnitConverstionHelper {
    
    /**
     convertMetersToMetersOrKilometers function converts meters Double value to either String meters/kilimeters
     
     - Parameters:
        - distance: - Double: distance in meters
     
     - Returns: String value in meters/kilometers
     */
    
    static func convertMetersToMetersOrKilometers(distance: Double) -> String {
        if distance > 1000.0 {
            let kmString = String(format: "%.f", distance/1000)
            return String(format: "%@ km", kmString)
        } else {
            return String(format: "%.f m", distance)
        }
    }
    
    /**
     convertMetersToKilometers function converts meters Double value to String kilimeters.
     
     - Parameters:
        - distance: - Double: distance in meters
     
     - Returns: String value in kilometers
     */
    
    static func convertMetersToKilometers(distance: Double) -> String {
        if distance > 1000.0 {
            let kmString = String(format: "%.f", distance/1000)
            return String(format: "%@ km", kmString)
        } else {
            let kmString = String(format: "%.2f", distance/1000)
            return String(format: "%@ km", kmString)
        }
    }
 
    /**
     convertMetersToFeetOrMiles function converts meters Double value to either String feet/miles
     
     - Parameters:
        - distance - Double: distance in meters
     
     - Returns: String value in feet/miles
     */
    
    static func convertMetersToFeetOrMiles(distance: Double) -> String {
        let miles =  distance * 0.000621371
        if miles >= 1 {
            let milesString = String(format: "%.f", miles)
            return String(format: "%@ mi", milesString)
        } else {
            let feet =  distance * 3.28084
            return String(format: "%.f ft", feet)
        }
    }
    
    /**
     convertMetersToMiles function converts meters Double value to either miles
     
     - Parameters:
     - distance - Double: distance in meters
     
     - Returns: String value in miles
     */
    
    static func convertMetersToMiles(distance: Double) -> String {
        let miles =  distance * 0.000621371
        if miles >= 1 {
            let milesString = String(format: "%.f", miles)
            return String(format: "%@ mi", milesString)
        } else {
            let milesString = String(format: "%.2f", miles)
            return String(format: "%@ mi", milesString)
        }
    }
    
    /**
     getTargetSpecificConversionFromMeters function checks target - Sminq/Markk and then returns corrospoding unit distance value
     
     - Parameters:
     - distance - Double: distance in meters
     
     - Returns: String value in either in meter/kilometer or feet/miles
     */
    
    static func getTargetSpecificConversionFromMeters(distance: Double) -> String {
        if UIApplication.shared.getTargetName() == AppConstants.AppTarget.sminq {
            return UnitConverstionHelper.convertMetersToKilometers(distance: distance)
        } else {
            return UnitConverstionHelper.convertMetersToMiles(distance: distance)
        }
    }
    
}
