//
//  UserSessionUtility.swift
//  Sminq
//
//  Created by Pushkar Deshmukh on 21/05/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import Foundation
import AccountKit
import FirebaseAuth
import GoogleSignIn

class UserSessionUtility {
    private init() { }
    
    static let shared = UserSessionUtility()
    var accountKit: AccountKit?
    
    // MARK: Public functions
    
    func clearUserSession() {
        self.accountKit?.logOut()
        
        self.removeDataFromUserDefaults()
        self.clearCachedData()
        self.logoutFireBase()
        self.disablePushNotifications()
        UserDefaultsUtility.shared.removeAuthToken()
    }
    
    // MARK: Private functions
    
    fileprivate func removeDataFromUserDefaults() {
        UserDefaults.standard.set(false, forKey: "AccountKitSession")
        UserDefaults.standard.set(false, forKey: "SignIn")
        UserDefaults.standard.removeObject(forKey: "MyProfile")
    }
    
    fileprivate func clearCachedData() {
        NotificationCenter.default.removeObserver(self)
        NotificationCacheService.shared.clearCachedNotifications(completion: {})
        UploadManager.sharedInstance.deleteAllRecords()
        DataCacheService().clearAllDataFromCache()
    }
    
    fileprivate func logoutFireBase() {
        let firebaseAuth = Auth.auth()
        do {
            try firebaseAuth.signOut()
        } catch let signOutError as NSError {
            LogUtil.error(signOutError)
        }
        
        GIDSignIn.sharedInstance().disconnect()
        GIDSignIn.sharedInstance().signOut()
    }
    
    fileprivate func disablePushNotifications() {
        // Register for sminq APNS Server
        if let token = UserDefaults.standard.string(forKey: "FCMToken") {
            PushNotificationAPI.registerForSminqPushNotifications(userId: Helper().getUserId(), fcmToken: token, register: AppConstants.disablePush)
        }

    }
}
