//
//  ArrayUtil.swift
//  Markk
//
//  Created by Pushkar Deshmukh on 13/06/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import Foundation
import CoreLocation

class ArrayUtil {
    static func getLatestReplies(replies: [Replies]) -> [Replies] {
        var tempReplies: [Replies] = []
        
        if replies.count > StoryViewConfig.storyViewLatestCommentsCount {
            let tempReversedReplies = replies.reversed()
            
            for (index, reply) in tempReversedReplies.enumerated() {
                if index < StoryViewConfig.storyViewLatestCommentsCount {
                    tempReplies.append(reply)
                }
            }
            tempReplies = tempReplies.reversed()
        } else {
            tempReplies = replies
        }
        
        return tempReplies
    }
    
    static func getLocationString(locations: [Location]) -> String {
        var tempString = ""
        locations.forEach { (location) in
            tempString = tempString + location.toString() + "| "
        }
        return tempString
    }
    
    static func getStoryDataFromPlacesStoriesData(placesStoriesData: [PlacesStoriesData]) -> [StoryData] {
        var storyDataList = [StoryData]()
        
        placesStoriesData.forEach { (placeStoryData) in
            if let storyData = placeStoryData.convertToStoryDataFromPlaceStoriesData(filterByPostId: nil) {
                storyDataList.append(storyData)
            }
        }
        
        return storyDataList
    }
}
