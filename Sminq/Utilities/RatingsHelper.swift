//
//  RatingsHelper.swift
//  Sminq
//
//  Created by Avinash Thakur on 07/05/19.
//  Copyright © 2019 Sminq. All rights reserved.
//
import UIKit
import Gloss
import MarkkCamera

class RatingsHelper {
    
    /**
     Returns PostData for the given postData json string
     
     - Parameters:
        - jsonString: - String: post data stored in json string format
     
     - Returns: URL: Returns Post Data
     */
    
    func getPostDataFrom(jsonString: String) -> PostData? {
        let questionDataJson = Helper().stringToJsonDictionaryConversion(inputString: jsonString)
        guard let json = questionDataJson as? JSON else {
            return nil
        }
        if let addPostQuestionsData: PostData = PostData(json: json) {
            return addPostQuestionsData
        }
        return nil
    }
    
    /**
     Returns array of image stickers from the given json string of image stickers list
     
     - Parameters:
        - stickerString: - String: stickers list json string
     
     - Returns: URL: Returns array of PostStickerData
     */
    
    func createImageStickersListForPost(stickerString: String) -> [PostStickerData] {
        var stickersArray = [PostStickerData]()
        let stickerListJSON = Helper().stringToJsonDictionaryConversion(inputString: stickerString)
        guard let varTempList = [PostStickerData].from(jsonArray: stickerListJSON as! [JSON]) else {
            return stickersArray
        }
        stickersArray = varTempList
        return stickersArray
    }
    
    /**
     Returns array of text stickers from the given json string of text stickers list
     
     - Parameters:
        - stickerString: - String: text stickers list json string
     
     - Returns: URL: Returns array of TextList
     */
    
    func createTextStickerListForPost(stickerString: String) -> [TextList] {
        var textStickersArray = [TextList]()
        let textListJSON = Helper().stringToJsonDictionaryConversion(inputString: stickerString)
        guard let varTempList = [TextList].from(jsonArray: textListJSON as! [JSON]) else {
            return textStickersArray
        }
        textStickersArray = varTempList
        return textStickersArray
    }
    
    /**
     Returns array of text stickers in json string format from camera asset
     
     - Parameters:
        - cameraAsset: - CameraAsset: Camera Asset
     
     - Returns: String: Returns array of text stickers in json string format
     */
    
    func getTextStickersListFrom(cameraAsset: CameraAsset) -> String {
        var textStickersString = ""
        if let textStickers = cameraAsset.textStickers {
            
            let textList: [TextList] = textStickers.map { textSticker in
                var textPosition = TextPosition()
                textPosition.height = textSticker.position?.height
                textPosition.width = textSticker.position?.width
                textPosition.rotation = textSticker.position?.rotation
                textPosition.topLeft = textSticker.position?.topLeft
                
                var textList = TextList()
                textList.uniqueId = textSticker.uniqueId
                textList.fontSize = textSticker.fontSize
                textList.position = textPosition
                textList.text = textSticker.text
                
                return textList
            }
            if let textListJSONArray = textList.toJSONArray() {
                textStickersString = Helper().jsonToStringConversion(jsonObject: textListJSONArray as Any)
            }
        }
        
        return textStickersString
    }
    
    /**
     Returns array of image stickers from camera asset
     
     - Parameters:
        - cameraAsset: - CameraAsset: Camera Asset
        - placeDetail: - PlaceDetail: PlaceDetail
     
     - Returns: String: Returns array of image stickers
     */
    
    func getImageStickersListFrom(cameraAsset: CameraAsset, placeDetail: PlaceDetail) -> [Stickers] {
        var ratingsStickers: [Stickers] = []
        if let imageStickers = cameraAsset.imageStickers {
            ratingsStickers = imageStickers.compactMap { assetSticker in
                if let sticker = placeDetail.stickers?.first(where: { $0.name == assetSticker.name }) {
                    return sticker
                }
                return nil
            }
        }
        return ratingsStickers
    }
    
    /**
     Returns comma separated names of image stickers added while creating a rating / post
     
     - Parameters:
        - stickers: - [Stickers]: Array of Stickers from Post data
     
     - Returns: String: Returns string of selected stickers names
     */
    
    func getImageStickersListForAnalyticsEvent(stickers: [Stickers]) -> String {
        var stickersNameString = ""
        if stickers.count > 0 {
            for stickerObj in stickers {
                stickersNameString.append(stickerObj.name)
                stickersNameString.append(",")
            }
            stickersNameString = String(stickersNameString.dropLast())
        }
        return stickersNameString
    }
    
    /**
     Returns json string for array of image stickers (containing stickerName, stickerId & stickerWeight) added while creating a rating / post
     
     - Parameters:
        - stickers: - [Stickers]: Array of Stickers from Post data
     
     - Returns: String: Returns Json string for array of stickers
     */
    
    func getImageStickersForPost(stickers: [Stickers]) -> String {
        var stickersString = ""
        let stickersList = stickers.map { return PostStickerData.init(json: ["sticker": $0.name, "id": $0.id, "weight": $0.weight as Any])! }
        
        if let stickersListJSONArray = stickersList.toJSONArray() {
            stickersString = Helper().jsonToStringConversion(jsonObject: stickersListJSONArray as Any)
        }
        return stickersString
    }
    
}
