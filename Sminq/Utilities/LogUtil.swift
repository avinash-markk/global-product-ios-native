//
//  LogUtil.swift
//  Sminq
//
//  Created by Pushkar Deshmukh on 09/01/19.
//  Copyright © 2019 Suraj Sagare. All rights reserved.
//

import Foundation
import BugfenderSDK

class LogUtil {
    static var dateFormat = "yyyy-MM-dd hh:mm:ssSSS"
    static var dateFormatter: DateFormatter {
        let formatter = DateFormatter()
        formatter.dateFormat = dateFormat
        formatter.locale = Locale.current
        formatter.timeZone = TimeZone.current
        return formatter
    }
    
    /**
     Info level logging
     
     - Parameters:
        - object: Object to be logged
     */
    
    class func info(_ object: Any?, fileName: String = #file, line: Int = #line) {
        if let log = object, AppConstants.isRemoteLoggingEnabled {
            Bugfender.print("\(line) \(Date().toString()) [\(sourceFileName(filePath: fileName))] \(log)")
        }
    }
    
    /**
     Warning level logging
     
     - Parameters:
        - object: Object to be logged
     */
    
    class func warning(_ object: Any?, fileName: String = #file, line: Int = #line) {
        if let log = object, AppConstants.isRemoteLoggingEnabled {
            Bugfender.warning("\(line) \(Date().toString()) [\(sourceFileName(filePath: fileName))] \(log)")
        }
    }
    
    /**
     Error level logging
     
     - Parameters:
        - object: Object to be logged
     */
    
    class func error(_ object: Any?, fileName: String = #file, line: Int = #line) {
        if let log = object, AppConstants.isRemoteLoggingEnabled {
            Bugfender.error("\(line) \(Date().toString()) [\(sourceFileName(filePath: fileName))] \(log)")
        }
    }
    
    /**
     Logs all data to BugFender only if remote logging is enabled
     
     - Parameters:
        - apiName: String: API name mapping
        - payload: Dictionary: Query params for GET requests and body for POST requests
        - pathParams: Dictionary: Path parameters
        - requestMethod: String: API request method
     */
    
    static func infoAPIPayload(apiName: String, payload: [String: Any], pathParams: [String: Any], requestMethod: String) {
        if AppConstants.isRemoteLoggingEnabled {
            LogUtil.info(apiName)
            
            if payload.count > 0 {
                if requestMethod.lowercased() == AppConstants.requestMethods.get {
                    LogUtil.info(DictionaryHelper.constructKeyValueStringFromDictionary(requestPayload: payload))
                } else if requestMethod.lowercased() == AppConstants.requestMethods.post || requestMethod.lowercased() == AppConstants.requestMethods.put || requestMethod.lowercased() == AppConstants.requestMethods.delete {
                    LogUtil.info(DictionaryHelper.getJSONStringFromDictionary(dictionary: payload))
                }
            }
            
            if pathParams.count > 0 {
                LogUtil.info(DictionaryHelper.constructKeyValueStringFromDictionary(requestPayload: pathParams))
            }
        }
    }
    
    /**
     Extract the file name from the file path
     
     - Parameters:
        - filePath: Full file path in bundle
     
     - Returns: Full file path in bundle
     */

    private class func sourceFileName(filePath: String) -> String {
        let components = filePath.components(separatedBy: "/")
        return components.isEmpty ? "" : components.last!
    }
    
}

internal extension Date {
    func toString() -> String {
        return LogUtil.dateFormatter.string(from: self as Date)
    }
}
