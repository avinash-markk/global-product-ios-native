//
//  Helper.swift
//  Sminq
//
//  Created by SMINQ iOS on 24/03/18.
//  Copyright © 2018 Suraj Sagare. All rights reserved.
//

import UIKit
import Foundation
import CoreLocation
import AVFoundation
import Gloss

class Helper {
    /**
     Converts string to `LivePlaces` model
    
     - Parameters:
        - place: Stringified `LivePlaces` model
     
     - Returns: `LivePlaces` model
     
     */
    
    func stringToPlaceConversion(place: String) -> LivePlaces? {
        var explorePlace: LivePlaces?
        
        if let data = place.data(using: String.Encoding.utf8) {
            do {
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                    explorePlace = LivePlaces(json: json)!
                }
                
                
            } catch {
                LogUtil.error(error)
            }
        }
        
        return explorePlace
    }
    
    /**
     Converts JSON string to dictionary
     
     - Parameters:
        - inputString: JSON string
     
     - Returns: Dictionary
     
     */
    
    func stringToJsonDictionaryConversion(inputString: String) -> Any? {
        let jsonData = inputString.data(using: .utf8)
        let dictionary = try? JSONSerialization.jsonObject(with: jsonData!, options: .mutableLeaves)
        return dictionary
    }
    
    /**
     Converts JSON to string
     
     - Parameters:
        - jsonObject: JSON object as type `Any`
     
     - Returns: String
     
     */
    
    func jsonToStringConversion(jsonObject: Any) -> String {
        var resultString = ""
        do {
            let data = try JSONSerialization.data(withJSONObject: jsonObject, options: JSONSerialization.WritingOptions.prettyPrinted)
            if let string = String(data: data, encoding: String.Encoding.utf8) {
                resultString = string
            }
        } catch {
            LogUtil.error(error)
        }

         return resultString
    }
    
    /**
     Converts string to `UserDataDict` model
     
     - Parameters:
        - user: Stringified `UserDataDict` model
     
     - Returns: `UserDataDict` model
     
     */
    
    func stringToUserConversion(user: String) -> UserDataDict {
        var tempUser: UserDataDict?
        
        if let data = user.data(using: String.Encoding.utf8) {
            do {
                let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any]
                tempUser = UserDataDict(json: json!)!
                
            } catch {
                LogUtil.error(error)
            }
        }
        
        return tempUser!
    }
    
    /**
     Removes whitespaces and initial character (•) from string if present.
     
     - Parameters:
        - comment: Stringified `UserDataDict` model
     
     - Returns: String without the (•) character at start
     
     */
    
    func removeInitialDotsFromComment(comment: String) -> String {
        var newCommentString: String = comment.trimmingCharacters(in: .whitespaces)
        
        if newCommentString == "" {
            return ""
        }
        
        let index = newCommentString.index(newCommentString.startIndex, offsetBy: 0)
        let firstSpaceIndex = newCommentString.index(newCommentString.startIndex, offsetBy: 1)
        
        if newCommentString[index] == "•" {
            newCommentString = String(newCommentString[firstSpaceIndex...])
        }
        
        return newCommentString
    }
    
    /**
     Converts date format from `yyyy-MM-dd` to `MM.dd.yyyy`
     
     - Parameters:
        - birthDate: String with date format as `yyyy-MM-dd`
     
     - Returns: String in date format as `MM.dd.yyyy`
     
     */
    
    func formattedBirthdate(birthDate: String) -> String {
        var formattedBirthDate: String = ""
        
        if birthDate != "" {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            let date = dateFormatter.date(from: birthDate)
            dateFormatter.dateFormat = "MM.dd.yyyy"
            formattedBirthDate = dateFormatter.string(from: date!)
        }
        return formattedBirthDate
    }
    
    /**
     Converts date format from `MM.dd.yyyy` to `yyyy-MM-dd`
     
     - Parameters:
        - birthDate: String with date format as `MM.dd.yyyy`
     
     - Returns: String in date format as `yyyy-MM-dd`
     
     */
    
    func nonFormattedBirthdate(birthDate: String) -> String {
        var nonFormattedBirthdate: String = ""
        
        if birthDate != "" {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MM.dd.yyyy"
            let date = dateFormatter.date(from: birthDate)
            dateFormatter.dateFormat = "yyyy-MM-dd"
            nonFormattedBirthdate = dateFormatter.string(from: date!)
        }
        return nonFormattedBirthdate
    }
    
    /**
     Checks if email address is valid or not using Regular expression
     
     - Parameters:
        - email: String email address
     
     - Returns: Boolean if email is valid or not
     
     */
    
    func isValidEmail(email: String) -> Bool {
        let emailFormat = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPredicate = NSPredicate(format: "SELF MATCHES %@", emailFormat)
        return emailPredicate.evaluate(with: email)
    }
    
    /**
     Returns a share count string
     
     - Parameters:
        - sharesCount: Integer share count
     
     - Returns: String with share count
     
     */
    
    func shareCountOnFeedCard(sharesCount: Int) -> String {
        if sharesCount == 1 {
            return "• \(sharesCount) Share"
        } else {
            return "• \(sharesCount) Shares"
        }
    }
    
    /**
     Returns a view count string
     
     - Parameters:
        - viewCount: Integer view count
     
     - Returns: String with view count
     
     */
    
    func viewCountOnFeedCard(viewCount: Int) -> String {
        if viewCount == 1 {
            return "\(viewCount) View "
        } else {
            return "\(viewCount) Views "
        }
    }
    
    /// Deprecated
    func createRoundedTriangle(width: CGFloat, height: CGFloat, radius: CGFloat) -> CGPath {
        // Points of the triangle
        let point1 = CGPoint(x: -width / 2, y: height / 2)
        let point2 = CGPoint(x: 0, y: -height / 2)
        let point3 = CGPoint(x: width / 2, y: height / 2)
        
        let path = CGMutablePath()
        path.move(to: CGPoint(x: 0, y: height / 2))
        path.addArc(tangent1End: point1, tangent2End: point2, radius: radius)
        path.addArc(tangent1End: point2, tangent2End: point3, radius: radius)
        path.addArc(tangent1End: point3, tangent2End: point1, radius: radius)
        path.closeSubpath()
        
        return path
    }
    
    /**
     Returns a current timestamp in format `dd-MM-yyyy hh:mm:ss a`
     
     - Returns: String with `dd-MM-yyyy hh:mm:ss a` format
     
     */
    
    func getCurrentTimeStamp() -> String {
        let timestamp = NSDate().timeIntervalSince1970
        let date = Date(timeIntervalSince1970: timestamp)
        let dateFormatter = DateFormatter()
        dateFormatter.locale = NSLocale.current
        
        dateFormatter.dateFormat = "dd-MM-yyyy hh:mm:ss a"
        let timeStampString = dateFormatter.string(from: date)
        return timeStampString
    }
    
    /**
     Checks if the date is older than firebase configured discard post value
     
     - Parameters:
        - givenDateString: String in format `dd-MM-yyyy hh:mm:ss a`
     
     - Returns: Boolean if date is older
     
     */
    
    func checkWhetherDateIsOlder(givenDateString: String) -> Bool {
        let interval = FIRRemoteConfigService.shared.getPendingPostDiscardingInterval()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy hh:mm:ss a"
        dateFormatter.timeZone = NSTimeZone.system
        if let givenDate = dateFormatter.date(from: givenDateString) {
            let minutes = Date().minutes(from: givenDate)
            if minutes > interval {
                return true
            } else {
                return false
            }
        } else {
            return true
        }
    }
    
    /**
     This creates and returns attributed string based on passed attributes
     
     - Parameters:
        - text: String
        - commonAttributes - Dictionary of `NSAttributedStringKey`. These attributes are considered for non-highlighted text
        - highlightedAttributes - Dictionary of `NSAttributedStringKey`. These attributes are considered for hightlighted text
     
     - Returns: Returns the attributed string
     
     */

    func highlightHandles(text: String, commonAttributes: [NSAttributedStringKey: Any], highlightedAttributes: [NSAttributedStringKey: Any], tags: [Tags]?) -> NSMutableAttributedString {
        let attributedStringCombination = NSMutableAttributedString()
        // Start, parse text and highlight user handles
        let tempTextArray = text.components(separatedBy: .whitespacesAndNewlines)
        let plainText = tempTextArray.joined(separator: " ")
        
        var textArray = plainText.components(separatedBy: .whitespacesAndNewlines)
        
        var wordCount = 0
        for mainIndex in 0..<textArray.count {
            let tagCount = tags?.count
            
            if textArray[mainIndex] != "" && textArray[mainIndex].first! == "@" && textArray[mainIndex].count >= 3 && tagCount != nil && (wordCount < tagCount!) && tags![wordCount].network != "none" {
                // Highlight keyword for non sminq networks
                attributedStringCombination.append(NSMutableAttributedString(string: textArray[mainIndex] + " ", attributes: highlightedAttributes))
                
            } else {
                attributedStringCombination.append(NSMutableAttributedString(string: textArray[mainIndex] + " ", attributes: commonAttributes))
            }
            
            if textArray[mainIndex] != "" && textArray[mainIndex].first! == "@" && textArray[mainIndex].count >= 3 && tagCount != nil && (wordCount < tagCount!) {
                // Increment counter for all networks
                wordCount += 1
            }
            
        }
        
        return attributedStringCombination
    }
    
    /**
     This function returns relative position of 'x' in `keyWindow`
     
     - Parameters:
        - x: CGFloat
     
     - Returns: CGFloat percentage
     */
    
    func getXRelativePosition(x: CGFloat) -> CGFloat {
        var relativePos: CGFloat = 0.0
        
        if let window = UIApplication.shared.keyWindow {
            relativePos = (100 * x) / window.width
        }
        
        return relativePos
    }
    
    /**
     This function returns relative position of 'y' in `keyWindow`
     
     - Parameters:
        - y: CGFloat
     
     - Returns: CGFloat percentage
     */
    
    func getYRelativePosition(y: CGFloat) -> CGFloat {
        var relativePos: CGFloat = 0.0
        
        if let window = UIApplication.shared.keyWindow {
            relativePos = (100 * y) / window.height
        }
        
        return relativePos
        
    }
    
    /**
     This function returns absolute position of 'x' in `keyWindow`
     
     - Parameters:
        - x: CGFloat
     
     - Returns: CGFloat
     */
    
    func getRelativeToAbsoluteX(x: CGFloat) -> CGFloat {
        var absolutePos: CGFloat = 0.0
        
        if let window = UIApplication.shared.keyWindow {
            absolutePos = (x / 100) * window.width
        }
        
        return absolutePos
    }
    
    /**
     This function returns absolute position of 'y' in `keyWindow`
     
     - Parameters:
        - y: CGFloat
     
     - Returns: CGFloat
     */
    
    func getRelativeToAbsoluteY(y: CGFloat) -> CGFloat {
        var absolutePos: CGFloat = 0.0
        
        if let window = UIApplication.shared.keyWindow {
            absolutePos = (y / 100) * window.height
        }
        
        return absolutePos
    }
    
    /**
     Prepends `+' character to string if not present
     
     - Parameters:
        - countryCode: String
     
     - Returns: Prepended string with `+`
     */
    
    func prependPlusToCountryCode(countryCode: String) -> String {
        var tempCountryCode = ""
        if countryCode.first != "+" {
            tempCountryCode = "+" + countryCode
        } else {
            tempCountryCode = countryCode
        }
        return tempCountryCode
    }
    
    /**
     Returns userId of logged in user
     
     - Returns: String
     */
    
    func getUserId() -> String {
        let profileDataClass = ProfileDataClass()
        if let profileData = profileDataClass.getProfileCachedData() {
            return profileData.user._id
        }
        
        return ""
    }
    
    /**
     Returns authToken of logged in user
     
     - Returns: String
     */
    
    func getAuthToken() -> String {
        return UserDefaultsUtility.shared.getAuthToken()
    }
    
    /**
     Returns displayName of logged in user
     
     - Returns: String
     */
    
    func getUserDisplayName() -> String {
        let profileDataClass = ProfileDataClass()
        if let profileData = profileDataClass.getProfileCachedData() {
            return profileData.user.displayName.first ?? ""
        }
        
        return ""
    }
    
    /**
     Returns logged in user object
     
     - Returns: Optional object of type `UserDataDict`
     */
    
    func getUser() -> UserDataDict? {
        let profileDataClass = ProfileDataClass()
        if let profileData = profileDataClass.getProfileCachedData() {
            return profileData.user
        }
        
        return nil
    }
    
    
    /**
     Constructs and returns the formatted string for passed locations.
     In format (lat,long:lat,long:lat,long)
     
     - Parameters:
        - locations: `[CLLocation]`
     
     - Returns: String
     */
    
    func convertCLLocationToFormattedString(locations: [CLLocation]) -> String? {
        var tempFormattedString = ""
        
        for (index, location) in locations.enumerated() {
            if index == (locations.count - 1) {
                // Last iteration
                tempFormattedString += tempFormattedString + "\(location.coordinate.latitude),\(location.coordinate.longitude)"
            } else {
                tempFormattedString += tempFormattedString + "\(location.coordinate.latitude),\(location.coordinate.longitude)" + ":"
            }
        }
        
        return tempFormattedString
    }
    
    /**
     Calculates the difference between start and end date
     
     - Parameters:
        - startDate: Date
        - endDate: Date
     
     - Returns: String
     */
    
    func getStartAndEndDateDiffInString(startDate: Date, endDate: Date) -> String {
        var formattedPostTime: String = ""
        
        var currentTimeComponent = Calendar.current.dateComponents([.nanosecond, .second], from: startDate, to: endDate)
        if let nanoSeconds = currentTimeComponent.nanosecond, let seconds = currentTimeComponent.second {
            /* NOTE: Using nano seconds to miliseconds coversion does not work.
             Returned values are in 'int'. and int can have values up to 2^32.
             Hence converted seconds to miliseconds and nano seconds to miliseconds
             */
            
            var miliSeconds = nanoSeconds / 1000000 // Nano seconds to miliseconds
            miliSeconds += (seconds * 1000)
            
            formattedPostTime = "\(miliSeconds)"
            
        }
        
        return formattedPostTime
    }
    
    /**
     Sets the visible screen name to user defaults
     
     - Parameters:
        - screen: Date
     */
    
    func setVisibleScreen(screen: String) {
        UserDefaults.standard.setValue(screen, forKey: "visibleScreen")
    }
    
    /**
     Gets visible screen name from user defaults
     
     - Returns: String
     */
    
    func getVisibleScreen() -> String {
        if let screen = UserDefaults.standard.value(forKey: "visibleScreen") as? String {
            return screen
        }
        return AppConstants.screenNames.home
    }
    
    /**
     Returns formatted date string. Returned format is <Ordinal value> followed by `MMM, yyyy`. Ex 1st Apr, 2019
     
     - Parameters:
        - date: Date
     
     - Returns: String
     */
    
    func formattedJoinDate(date: Date) -> String {
        let calendar = Calendar.current
        let anchorComponents = calendar.dateComponents([.day, .month, .year], from: date)
        
        // Formate
        let dateFormate = DateFormatter()
        dateFormate.dateFormat = "MMM, yyyy"
        let newDate = dateFormate.string(from: date)
        
        var day  = "\(anchorComponents.day!)"
        switch day {
        case "1", "21", "31":
            day.append("st")
        case "2", "22":
            day.append("nd")
        case "3", "23":
            day.append("rd")
        default:
            day.append("th")
        }
        
        let joinDate = day + " " + newDate
        return joinDate.uppercased()
    }
    
    /**
      Returns the thumbnail's file path for given file Id / taskId
     
     - Parameters:
     - taskId: String
     
     - Returns: String
     */
    
    func returnSavedThumbnailForVideoAt(taskId: String) -> String? {
        let fileManager = FileManager.default
        let documentsDirectory = fileManager.urls(for: .documentDirectory, in: .userDomainMask).first!
        
        let videoFileName = "\(taskId).mov"
        let videoFileURL = documentsDirectory.appendingPathComponent(videoFileName)
        
        let thumbnailFileName = "\(taskId).jpg"
        let thumbnailFileURL = documentsDirectory.appendingPathComponent(thumbnailFileName)
        
        if fileManager.fileExists(atPath: thumbnailFileURL.path) {
            return thumbnailFileURL.path
        } else {
            if fileManager.fileExists(atPath: videoFileURL.path) {
                let thumbnail = self.createAndSaveVideoThumbnailForFile(atPath: videoFileURL.path, fileId: taskId)
                return thumbnail
            } else {
                return nil
            }
        }
        
    }
    
    /**
     Creates & saves video's thumbnail for given video file path & fileId, Returns the thumbnail's file path
     
     - Parameters:
     - atPath: String
     - fileId: String
     
     - Returns: String
     */
    
    func createAndSaveVideoThumbnailForFile(atPath: String, fileId: String) -> String? {
        do {
            let asset = AVURLAsset(url: URL(fileURLWithPath: atPath), options: nil)
            let imgGenerator = AVAssetImageGenerator(asset: asset)
            imgGenerator.appliesPreferredTrackTransform = true
            let cgImage = try imgGenerator.copyCGImage(at: CMTimeMake(0, 1), actualTime: nil)
            let thumbnail = UIImage(cgImage: cgImage)
            let thumbnailPath =  DocDirectoryUtil().saveImageLocallyAndReturnURL(feedId: fileId, image: thumbnail)
            return thumbnailPath.path
            
        } catch let error {
            LogUtil.error(error)
            return nil
        }
    }
    
    /**
     Returns subcategory for a place
     
     - Parameters:
        - postData: `PostData` model
     
     - Returns: String
     */
    
    func getSubCategoryFor(postData: PostData?) -> String {
        var subCategory = ""
        if postData?.place?.subCategory != nil &&  postData?.place?.subCategory.name != "" {
            if let subCatString = postData?.place?.subCategory.name {
                subCategory = subCatString
            }
        }
        return subCategory
    }
    
    /**
     Returns radian for given degree value
     
     - Parameters:
        - degree: Double
     
     - Returns: Double
     */
    
    func convertDegreeToRadian(degree: Double) -> Double {
        return degree * .pi / 180
    }
    
    /**
     Returns text for share
     
     - Parameters:
        - place: `LivePlaces`
        - url: URL to share
        - isVideo: Bool if video present or not
     
     - Returns: Returns text for share
     */
    
    func generateShareText(place: LivePlaces, url: URL, isVideo: Bool) -> String {
        var textString = ""
        
        let locationName = SminqAPI.formattedAddressFromAddressComponentWithoutInitialDots(addressComponent: place.addressComponents)
        
        textString = "View this real-time info from \(place.name ?? ""), \(locationName) on @getMarkk. Info expires in few hrs. See now \(url)"
        
        return textString
    }
    
    /**
     Returns text for SMS share
     
     - Parameters:
     - place: `LivePlaces`
     - url: URL to share
     - isVideo: Bool if video present or not
     
     - Returns: Returns text for SMS share
     */
    
    func generateSMSShareText(place: LivePlaces, url: URL) -> String {
        return "Check out \(place.name ?? "") right now, on Markk. Rating expires in a few hours! See now \(url))"
    }
    
    /**
     Returns time ago in string
     
     - Parameters:
        - createdAt: Int(Optional)
     
     - Returns: If passed nil, returns empty string. Otherwise returns time ago string
     */
    
    func getTimeAgoString(for createdAt: Int?) -> String {
        guard let createdAt = createdAt else { return "" }
        let date = Date(timeIntervalSince1970: TimeInterval(createdAt))
        
        return PushNotificationAPI.timeAgoSinceDate(date)
    }
    
    /**
     Returns string from the given date for archived sections header view text
     
     - Parameters:
        - dateString: Date in string format
     
     - Returns: Returns date in string
     */
    
    func getFormattedArchivedHeaderText(dateString: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        dateFormatter.timeZone = NSTimeZone.system
        if let date = dateFormatter.date(from: dateString) {
            if date.days(from: Date()) == -1 {
                return "YESTERDAY"
            } else {
                dateFormatter.dateFormat = "MM.dd.yyyy"
                let newDateString = dateFormatter.string(from: date)
                return newDateString
            }
        }
        return ""
    }
    
    /**
     Converts string to `Badge` model
     
     - Parameters:
     - badge: Stringified `Badge` model
     
     - Returns: `Badge` model
     
     */
    
    func stringToBadgeConversion(badgeString: String) -> Badge? {
        var badge: Badge?
        
        if let data = badgeString.data(using: String.Encoding.utf8) {
            do {
                let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any]
                badge = Badge(json: json!)!
                
            } catch {
                LogUtil.error(error)
            }
        }
        
        return badge
    }
    
    func anyToBanner(data: Any?) -> Banner? {
        guard let data = data else { return nil }
        guard let dataDictionary = data as? [String: Any] else { return nil }
        
       
        
        guard let banner = Banner(json: dataDictionary) else { return nil }
        return banner
        
//        let message = dataDictionary["message"]
//        print(message)
//
//        guard let json = message as? JSON, let banner = Banner(json: json) else { return nil }
//        return banner
    }
    
    /**
     Considers point unit and calculate relative unit proportionate sizeInPoint and returns in pixel/point (Doesn't matter because it's proportional)
     Calculates and returns the relative unit of pixel into points co-ordinate space
     
     - Parameters:
     - sizeInPoint: CGFloat Height or Width in point co-ordinate system
     - sizeInPixel: CGFloat Height or Width in pixels co-ordinate system
     - howMuch: Unit in point co-ordinate system.
     
     - Returns: Calculates and returns the relative unit of pixel into points co-ordinate space
     
     */
    
    func relativeUnit(sizeInPoint: CGFloat, sizeInPixel: CGFloat, howMuch unit: CGFloat) -> CGFloat {
        return sizeInPixel * (unit / sizeInPoint)
    }
}
