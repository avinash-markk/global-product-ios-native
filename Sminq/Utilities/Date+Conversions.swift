//
//  Date+Conversions.swift
//  Sminq
//
//  Created by Bjorn Mascarenhas on 21/11/18.
//  Copyright © 2018 Suraj Sagare. All rights reserved.
//

import Foundation

extension Date {
    
    /**
     Returns the amount of years from another date
     
     - Parameters:
        - from: date as type `Date`
     
     - Returns: Amount of years in Integer
     */
    
    func years(from date: Date) -> Int {
        return Calendar.current.dateComponents([.year], from: date, to: self).year ?? 0
    }
    
    /**
     Returns the amount of months from another date
     
     - Parameters:
        - from: date as type `Date`
     
     - Returns: Amount of months in Integer
     */
    
    func months(from date: Date) -> Int {
        return Calendar.current.dateComponents([.month], from: date, to: self).month ?? 0
    }
    
    /**
     Returns the amount of weeks from another date
     
     - Parameters:
        - from: date as type `Date`
     
     - Returns: Amount of weeks in Integer
     */
    
    func weeks(from date: Date) -> Int {
        return Calendar.current.dateComponents([.weekOfMonth], from: date, to: self).weekOfMonth ?? 0
    }
    
    /**
     Returns the amount of days from another date
     
     - Parameters:
        - from: date as type `Date`
     
     - Returns: Amount of days in Integer
     */
    
    func days(from date: Date) -> Int {
        return Calendar.current.dateComponents([.day], from: date, to: self).day ?? 0
    }
    
    /**
     Returns the amount of hours from another date
     
     - Parameters:
        - from: date as type `Date`
     
     - Returns: Amount of hours in Integer
     */
    
    func hours(from date: Date) -> Int {
        return Calendar.current.dateComponents([.hour], from: date, to: self).hour ?? 0
    }
    
    /**
     Returns the amount of minutes from another date
     
     - Parameters:
        - from: date as type `Date`
     
     - Returns: Amount of minutes in Integer
     */
    
    func minutes(from date: Date) -> Int {
        return Calendar.current.dateComponents([.minute], from: date, to: self).minute ?? 0
    }

    /**
     Returns the amount of seconds from another date
     
     - Parameters:
        - from: date as type `Date`
     
     - Returns: Amount of seconds in Integer
     */
    
    func seconds(from date: Date) -> Int {
        return Calendar.current.dateComponents([.second], from: date, to: self).second ?? 0
    }
    
    /**
     Returns the a custom time interval description from another date
     
     - Parameters:
        - from: date as type `Date`
     
     - Returns: Time interval in String
     */
    
    func offset(from date: Date) -> String {
        
        if years(from: date) == 1 {
            return "\(years(from: date)) year"
        } else if years(from: date) > 1 {
            return "\(years(from: date)) years"
        }
        
        if months(from: date) == 1 {
            return "\(months(from: date)) month"
        } else if months(from: date)  > 1 {
            return "\(months(from: date)) month"
        }
        
        if weeks(from: date) == 1 {
            return "\(weeks(from: date)) week"
        } else if weeks(from: date)   > 1 {
            return "\(weeks(from: date)) weeks"
        }
        
        if days(from: date)    == 1 {
            return "\(days(from: date)) day"
        } else if days(from: date)    > 1 {
            return "\(days(from: date)) days"
        }
        
        if hours(from: date)   == 1 {
            return "\(hours(from: date)) hour"
        } else if hours(from: date)   > 1 {
            return "\(hours(from: date)) hours"
        }
        
        if minutes(from: date) == 1 {
            return "\(minutes(from: date)) minute"
        } else if minutes(from: date) > 1 {
            return "\(minutes(from: date)) minutes"
        }
        
        return ""
    }
    
    /**
     Returns current day of the week. example: sun/mon/tue
     Refer `http://nsdateformatter.com`
     
     - Returns: Day in string
     */
    
    func dayOfWeek() -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEE"
        return dateFormatter.string(from: self).capitalized
        // or use capitalized(with: locale) if you want
    }
    
    /**
     Returns current time in HH:mm
     Refer `http://nsdateformatter.com`
     
     - Returns: Time in string
     */
    
    func dateFormatHHmm() -> String? {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        return dateFormatter.string(from: self)
        // or use capitalized(with: locale) if you want
    }
    
    /**
     Returns time ago from given date
     
     - Parameters:
        - dateString: Date string in format `dd-MM-yyyy hh:mm:ss a`
     
     - Returns: Time ago in string
     */
    
    func getFormattedTimeForUploads(dateString: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy hh:mm:ss a"
        dateFormatter.timeZone = NSTimeZone.system
        
        if let givenDate = dateFormatter.date(from: dateString) {
            let currentDate = Date()
            let hours = currentDate.hours(from: givenDate)
            if hours == 1 {
                return "\(hours) hour ago"
            } else if hours > 1 {
                return "\(hours) hours ago"
            }
            
            let minutes = currentDate.minutes(from: givenDate)
            if minutes == 1 {
                return "\(minutes) min ago"
            } else if minutes > 1 &&  minutes < 60 {
                return "\(minutes) mins ago"
            }
            
            let seconds = currentDate.seconds(from: givenDate)
            if seconds == 1 {
                return "\(seconds) sec ago"
            } else if seconds > 1 &&  seconds < 60 {
                return "\(seconds) secs ago"
            }
        }
        return ""
    }
    
    /**
     Returns time stamp from given date
     
     - Parameters:
        - dateString: Date string in format `dd-MM-yyyy hh:mm:ss a`
     
     - Returns: Time stamp in double
     */
    
    func getTimeStampFrom(dateString: String) -> Double {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = "dd-MM-yyyy hh:mm:ss a"
        if let date = dateFormatter.date(from: dateString) {
        let timeInterval = date.timeIntervalSince1970
        let myInt = Double(timeInterval)
            return myInt
        }
        return 0
    }
    
}
