//
//  UIViewExtension.swift
//  Sminq
//
//  Created by Pushkar Deshmukh on 23/05/18.
//  Copyright © 2018 Suraj Sagare. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    /**
     Create rounded corners for view
     
     - Parameters:
        - corners: Corners of type `UIRectCorner`
        - radius: corner radius CGFloat
     */
    
    func roundCorners(_ corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
    
    /**
     Load a xib file of invoking UIView
     */
    
    func loadNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nibName = type(of: self).description().components(separatedBy: ".").last!
        let nib = UINib(nibName: nibName, bundle: bundle)
        return (nib.instantiate(withOwner: self, options: nil).first as? UIView)!
    }
    
    // Helper to get pre transform frame
    var originalFrame: CGRect {
        let currentTransform = transform
        transform = .identity
        let originalFrame = frame
        transform = currentTransform
        return originalFrame
    }
    
    // Helper to get point offset from center
    func centerOffset(_ point: CGPoint) -> CGPoint {
        return CGPoint(x: point.x - center.x, y: point.y - center.y)
    }
}
