//
//  StringUtil.swift
//  Markk
//
//  Created by Pushkar Deshmukh on 17/06/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import Foundation

class StringUtil {
    static func sanitizeString(text: String) -> String {
        return text.trimmingCharacters(in: .whitespacesAndNewlines)
    }
}
