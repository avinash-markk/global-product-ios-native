//
//  InternetConnection.swift
//  Sminq
//
//  Created by Bjorn Mascarenhas on 07/03/18.
//  Copyright © 2018 Suraj Sagare. All rights reserved.
//

import Foundation
import Alamofire

class InternetConnection {
   
    let reachabilityManager = Alamofire.NetworkReachabilityManager(host: "www.apple.com")
    
    func startNetworkReachabilityObserver() {
        reachabilityManager?.listener = { status in
            
            switch status {
                
            case .notReachable:
                print("The network is not reachable")
                
            case .unknown :
                print("It is unknown whether the network is reachable")
                
            case .reachable(.ethernetOrWiFi):
                print("The network is reachable over the WiFi connection")
                
            case .reachable(.wwan):
                print("The network is reachable over the WWAN connection")
                
            }
        }
        reachabilityManager?.startListening()
    }
    
    /**
     Checks if network is reachable
     
     - Returns: Boolean
     */
    
    class func isConnectedToInternet() -> Bool {
        NetworkReachabilityManager()!.startListening()
        return NetworkReachabilityManager()!.isReachable
    }
    
}


