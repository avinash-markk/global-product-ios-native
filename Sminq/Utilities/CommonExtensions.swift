//
//  CommonExtensions.swift
//  Sminq
//
//  Created by Avinash Thakur on 17/01/19.
//  Copyright © 2019 Sminq. All rights reserved.
//
import UIKit
import AVFoundation

extension Int {
    /**
     Returns meta associated for mood which contains image name, text and image with bottom text
     
     - Returns: Image meta data for mood
     */
    
    func getMoodMeta() -> (image: UIImage?, text: String, imageWithBottomText: UIImage?, gradientColors: [Any]?) {
         let dopeGradientColors: [Any] = [AppConstants.SminqColors.gradientYellowLight.cgColor, AppConstants.SminqColors.gradientYellowDark.cgColor]
         let okayGradientColors: [Any] = [AppConstants.SminqColors.gradientBlueLight.cgColor, AppConstants.SminqColors.gradientBlueDark.cgColor]
         let nopeGradientColors: [Any] = [AppConstants.SminqColors.gradientRedLight.cgColor, AppConstants.SminqColors.rgradientRedDark.cgColor]
        
        switch self {
        case 1:
            return (image: UIImage.init(named: "ratingsNope64"), text: "NOPE", imageWithBottomText: UIImage.init(named: "ratingsLabelNope96"), gradientColors: nopeGradientColors)
        case 2:
            return (image: UIImage.init(named: "ratingsOkay64"), text: "OKAY", imageWithBottomText: UIImage.init(named: "ratingsLabelOkay96"), gradientColors: okayGradientColors)
        case 3:
            return (image: UIImage.init(named: "ratingsDope64"), text: "DOPE", imageWithBottomText: UIImage.init(named: "ratingsLabelDope96"), gradientColors: dopeGradientColors)
        default:
            return (image: UIImage.init(named: "ratingsOkay64"), text: "OKAY", imageWithBottomText: UIImage.init(named: "ratingsLabelOkay96"), gradientColors: okayGradientColors)
        }
    }
    
}

extension String {
    
    func encodeUrl() -> String?
    {
        return self.addingPercentEncoding( withAllowedCharacters: NSCharacterSet.urlQueryAllowed)
    }
    func decodeUrl() -> String?
    {
        return self.removingPercentEncoding
    }
    
    
    /**
     Removes whites spaces from string and returns
     
     - Returns: String without whitespaces
     */
    
    func removingWhitespaces() -> String {
        return components(separatedBy: .whitespaces).joined()
    }
    
    /**
     Removes characters and keeps only digits
     
     - Returns: String with just numbers
     */
    
    func onlyDigits() -> String {
        let filtredUnicodeScalars = unicodeScalars.filter {CharacterSet.decimalDigits.contains($0)}
        return String(String.UnicodeScalarView(filtredUnicodeScalars))
    }
    
    func widthOfString(usingFont font: UIFont) -> CGFloat {
        let fontAttributes = [NSAttributedString.Key.font: font]
        let size = self.size(withAttributes: fontAttributes)
        return size.width
    }
    
    func getFirstPathParam() -> String {
        let splitArray = self.split(separator: "/")
        let first = String(splitArray.first ?? "")
        return first
    }
    
    func getSecondPathParam() -> String {
        let splitArray = self.split(separator: "/")
        
        return splitArray.count >= 1  ? String(splitArray[1]) : ""
    }
    
    func getLastPathParam() -> String {
        let splitArray = self.split(separator: "/")
        let last = String(splitArray.last ?? "")
        
        let splitQueryParamsArray = last.split(separator: "&")
        let firstQuery = String(splitQueryParamsArray.first ?? "")
        return firstQuery
    }
    
    func canOpenStringURL() -> Bool {
        if let urlScheme = URL(string: self), UIApplication.shared.canOpenURL(urlScheme) {
            return true
        }
        return false
    }
    
}

extension UILabel {
    /**
     Finds the index of character (in the attributedText) at point
     
     - Returns: Index in integer
     */
    
    func indexOfAttributedTextCharacterAtPoint(point: CGPoint) -> Int {
        assert(self.attributedText != nil, "This method is developed for attributed string")
        let textStorage = NSTextStorage(attributedString: self.attributedText!)
        let layoutManager = NSLayoutManager()
        textStorage.addLayoutManager(layoutManager)
        let textContainer = NSTextContainer(size: self.frame.size)
        textContainer.lineFragmentPadding = 0
        
        textContainer.maximumNumberOfLines = self.numberOfLines
        textContainer.lineBreakMode = self.lineBreakMode
        layoutManager.addTextContainer(textContainer)
        
        let index = layoutManager.characterIndex(for: point, in: textContainer, fractionOfDistanceBetweenInsertionPoints: nil)
        return index
    }
    
    func textDropShadow() {
        self.layer.shadowColor = AppConstants.SminqColors.blackDark100.withAlphaComponent(0.5).cgColor
//        self.layer.shadowColor = UIColor.red.cgColor
        self.layer.shadowRadius = 0
        self.layer.shadowOpacity = 1.0
        self.layer.shadowOffset = CGSize(width: 0, height: 1)
        self.layer.masksToBounds = false
    }
}

extension UITextField {
    /**
     Adds left padding to the UITextField
     
     - Parameters:
        - amount: Padding to be added
     */
    
    func setLeftPaddingPoints(_ amount: CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    
    /**
     Adds right padding to the UITextField
     
     - Parameters:
        - amount: Padding to be added
     */
    
    func setRightPaddingPoints(_ amount: CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
    
}

extension UITextView {
    /**
     Finds the current word from UITextView
     
     - Returns: String
     */
    
    var currentWord: String? {
        let regex = try! NSRegularExpression(pattern: "\\S+$")
        let textRange = NSRange(location: 0, length: selectedRange.location)
        if let range = regex.firstMatch(in: text, range: textRange)?.range {
            return String(text[Range(range, in: text)!])
        }
        return nil
    }
    
    /**
     Finds the current word range from UITextView
     
     - Returns: `NSRange`
     */
    
    var currentWordRange: NSRange? {
        let regex = try! NSRegularExpression(pattern: "\\S+$")
        let textRange = NSRange(location: 0, length: selectedRange.location)
        if let range = regex.firstMatch(in: text, range: textRange)?.range {
            return range
        }
        return nil
    }
    
}

extension UIView {
    
    /// Getter & setter for x value of view's frame
    var x: CGFloat {
        get {
            return self.frame.origin.x
        }
        set {
            var r = self.frame
            r.origin.x = newValue
            self.frame = r
        }
    }
    
    /// Getter & setter for y value of view's frame
    var y: CGFloat {
        get {
            return self.frame.origin.y
        }
        set {
            var r = self.frame
            r.origin.y = newValue
            self.frame = r
        }
    }
    
    /// Getter & setter for height of view
    var height: CGFloat {
        get {
            return self.frame.size.height
        }
        set {
            var r = self.frame
            r.size.height = newValue
            self.frame = r
        }
    }
    
    /// Getter & setter for width of view
    public var width: CGFloat {
        get {
            return self.frame.size.width
        }
        set {
            var r = self.frame
            r.size.width = newValue
            self.frame = r
        }
    }
    
    /// Getter & setter for center of view
    public var centerX: CGFloat {
        get {
            return self.center.x
        }
        set {
            self.center = CGPoint(x: newValue, y: self.center.y)
        }
    }
    
    /// Getter & setter for origin of view's frame
    public var origin: CGPoint {
        get {
            return self.frame.origin
        }
        set {
            self.x = newValue.x
            self.y = newValue.y
        }
    }
    
    /// Getter for safe rect of view
    var safeRect: CGRect {
        get {
            var inset = UIEdgeInsets.zero
            var rect = self.bounds
            if #available(iOS 11.0, *) {
                inset = UIApplication.shared.delegate?.window??.safeAreaInsets ?? UIEdgeInsets.zero
                rect = CGRect.init(x: 0, y: inset.top, width: self.width, height: self.height - inset.top - inset.bottom)
            }
            return rect
        }
    }
    
    /**
     Creates gradient animation on top of the invoking view
     */
    
    func showAnimatedGradientAnimation() {
        let defaultView = UIView()
        defaultView.frame = self.bounds
        defaultView.backgroundColor = AppConstants.SminqColors.blackLight100
        defaultView.tag = 1
        defaultView.clipsToBounds = self.clipsToBounds
        defaultView.layer.cornerRadius = self.cornerRadius
        
        let shinyView = UIView()
        shinyView.frame = self.bounds
        shinyView.backgroundColor = UIColor(white: 0, alpha: 0.2)
        shinyView.tag = 2
        shinyView.clipsToBounds = self.clipsToBounds
        shinyView.layer.cornerRadius = self.cornerRadius
        
        defaultView.removeFromSuperview()
        shinyView.removeFromSuperview()
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.name = "skeletonLayer"
        gradientLayer.colors = [
            UIColor.clear.cgColor, UIColor.clear.cgColor,
            UIColor.black.cgColor, UIColor.black.cgColor,
            UIColor.clear.cgColor, UIColor.clear.cgColor
        ]
        
        gradientLayer.locations = [0, 0.2, 0.4, 0.6, 0.8, 1]
        gradientLayer.frame = shinyView.bounds
        
        let angle = 90 * CGFloat.pi / 180
        gradientLayer.transform = CATransform3DMakeRotation(angle, 0, 0, 1)
        
        shinyView.layer.mask = gradientLayer
        
        gradientLayer.transform = CATransform3DConcat(gradientLayer.transform, CATransform3DMakeScale(3, 3, 0))
        
        let animation = CABasicAnimation(keyPath: "transform.translation.x")
        animation.fromValue = -1.5 * self.frame.width
        animation.toValue = 1.5 * self.frame.width
        animation.repeatCount = Float.infinity
        animation.duration = 2
        
        self.addSubview(defaultView)
        self.addSubview(shinyView)
        
        gradientLayer.add(animation, forKey: "skeletonAnimation")
    }
    
    /**
     Removes gradient animation from top of the view
     */
    
    func hideAnimatedGratientAnimation() {
        if let defaultView = self.viewWithTag(1) {
            defaultView.removeFromSuperview()
        }
        
        if let shinyView = self.viewWithTag(2) {
            shinyView.layer.mask = nil
            shinyView.removeFromSuperview()
        }
        
        self.layer.removeAllAnimations()
        self.layoutIfNeeded()
    }
    
    func addGradient(colors: [CGColor], locations: [NSNumber]) {
        let gradient = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.colors = colors
        gradient.locations = locations
        
        self.layer.addSublayer(gradient)
//        self.layer.insertSublayer(gradient, at: 0)
    }
    
}

extension UIStackView {
    /**
     Adds background color to UIStackView
     
     - Parameters:
        - color: `UIColor`
        - cornerRadius: `CGFloat`
     */
    
    func addBackground(color: UIColor, cornerRadius: CGFloat, borderColor: UIColor? = nil, borderWidth: CGFloat? = nil) {
        let subView = UIView(frame: bounds)
        subView.backgroundColor = color
        subView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        subView.layer.cornerRadius = cornerRadius
        subView.clipsToBounds = true
        
        if borderColor != nil {
            subView.borderColor = borderColor
        }
        
        if let borderWidth = borderWidth {
            subView.borderWidth = borderWidth
        }
        
        insertSubview(subView, at: 0)
    }
    
}

extension NSMutableAttributedString {
    /**
     Sets color to NSMutableString
     
     - Parameters:
        - color: `UIColor`
        - forText: `String`
     */
    
    func setColor(color: UIColor, forText stringValue: String) {
        let range: NSRange = self.mutableString.range(of: stringValue, options: .caseInsensitive)
        self.addAttribute(NSAttributedStringKey.foregroundColor, value: color, range: range)
    }
    
}

extension NSLayoutConstraint {
    /**
     Change multiplier constraint
     
     - parameter multiplier: CGFloat
     - returns: NSLayoutConstraint
     */
    
    func setMultiplier(multiplier: CGFloat) -> NSLayoutConstraint {
        
        NSLayoutConstraint.deactivate([self])
        
        let newConstraint = NSLayoutConstraint(
            item: firstItem as Any,
            attribute: firstAttribute,
            relatedBy: relation,
            toItem: secondItem,
            attribute: secondAttribute,
            multiplier: multiplier,
            constant: constant)
        
        newConstraint.priority = priority
        newConstraint.shouldBeArchived = self.shouldBeArchived
        newConstraint.identifier = self.identifier
        
        NSLayoutConstraint.activate([newConstraint])
        return newConstraint
    }
    
}

extension UIDevice {
    
    // Depcrecated
    var hasNotch: Bool {
        var bottom = 0.0
        if #available(iOS 11.0, *) {
            bottom = Double(UIApplication.shared.keyWindow?.safeAreaInsets.bottom ?? 0)
        } else {
            
            // Fallback on earlier versions
        }
        return Int(bottom) > 0
    }
    
    // Deprecated
    var isiPhone5OrLess: Bool {
        if  UIScreen.main.bounds.width <= 320 {
            return true
        } else {
            return false
        }
    }
    
}

extension UIColor {
    /**
     Converts this `UIColor` instance to a 1x1 `UIImage` instance and returns it.
     
     - Returns: `self` as a 1x1 `UIImage`.
     */
    
    func as1ptImage() -> UIImage {
        UIGraphicsBeginImageContext(CGSize(width: 1, height: 1))
        setFill()
        UIGraphicsGetCurrentContext()?.fill(CGRect(x: 0, y: 0, width: 1, height: 1))
        let image = UIGraphicsGetImageFromCurrentImageContext() ?? UIImage()
        UIGraphicsEndImageContext()
        return image
    }
    
    /**
     Converts hex string to UIColor.
     
    - Returns: UIColor.
     */
    
    func colorFrom(hex: String) -> UIColor? {
        if hex.hasPrefix("#") {
            let start = hex.index(hex.startIndex, offsetBy: 1)
            let hexColor = String(hex[start...])
            
            if hexColor.count == 8 {
                return getColorForHexWithAlpha(hexString: hexColor)
            } else if hexColor.count == 6 {
                return getColorForHex(hexString: hexColor)
            }
        }
        return nil
    }
    
    private func getColorForHexWithAlpha(hexString: String) -> UIColor? {
        let r, g, b, a: CGFloat
        let scanner = Scanner(string: hexString)
        var hexNumber: UInt64 = 0
        if scanner.scanHexInt64(&hexNumber) {
            r = CGFloat((hexNumber & 0xff000000) >> 24) / 255
            g = CGFloat((hexNumber & 0x00ff0000) >> 16) / 255
            b = CGFloat((hexNumber & 0x0000ff00) >> 8) / 255
            a = CGFloat(hexNumber & 0x000000ff) / 255
            let color = UIColor(red: r, green: g, blue: b, alpha: a)
            return color
        }
        return nil
    }
    
    private func getColorForHex(hexString: String) -> UIColor? {
        let r, g, b: CGFloat
        let scanner = Scanner(string: hexString)
        var hexNumber: UInt64 = 0
        if scanner.scanHexInt64(&hexNumber) {
            r = CGFloat((hexNumber & 0xFF0000) >> 16) / 255.0
            g = CGFloat((hexNumber & 0x00FF00) >> 8) / 255.0
            b = CGFloat(hexNumber & 0x0000FF) / 255.0
            let color = UIColor(red: r, green: g, blue: b, alpha: 1)
            return color
        }
        return nil
    }
    
}

extension UICollectionView {
    /**
     Checks and returns if indexPath is valid inside a collection view
     
     - Returns: Boolean for validity
     */
    
    func validate(indexPath: IndexPath) -> Bool {
        if indexPath.section >= numberOfSections {
            return false
        }
        
        if indexPath.row >= numberOfItems(inSection: indexPath.section) {
            return false
        }
        
        return true
    }
    
}

extension AVPlayer {
    
    /**
     Checks and returns if AVPlayer is ready to play
     Ready to play is, when loaded time is greater than 0 and when status is `readyToPlay`
     
     - Returns: Boolean is player is ready to play
     */
    
    var ready: Bool {
        let timeRange = currentItem?.loadedTimeRanges.first as? CMTimeRange
        guard let duration = timeRange?.duration else { return false }
        let timeLoaded = Int(duration.value) / Int(duration.timescale) // value/timescale = seconds
        let loaded = timeLoaded > 0
        
        return status == .readyToPlay && loaded
    }
    
}

extension Date {
    
    /**
     Returns current time stamp in seconds
     
     - Returns: TimeInterval
     */
    
    var currentTimeStampInSecond: TimeInterval {
        return Date().timeIntervalSince1970
    }
    
}

extension UINavigationController {
    
    func checkAndDisplayBadgeForIncompleteProfile(data: UserDataDict?) {
        guard let userData = data else {
            return
        }
        if let tabItems = AppConstants.appDelegate.tabBarViewController?.tabBar.items as NSArray? {
            let tabItem1 = tabItems[1] as! UITabBarItem
            
            if userData.isProfileComplete() {
                tabItem1.badgeValue = nil
            } else {
                tabItem1.badgeValue = "!"
            }
        }
    }
    
}

extension UIView {
    /**
     Returns window edge insets
     
     - Returns: `UIEdgeInsets`
     */
    
    func getWindowSafeAreaInsets() -> UIEdgeInsets {
        if let window = UIApplication.shared.keyWindow {
            if #available(iOS 11.0, *) {
                return window.safeAreaInsets
            }
        }
        
        return UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0)
    }
}

