//
//  GradientView.swift
//  Sminq
//
//  Created by Pushkar Deshmukh on 12/08/18.
//  Copyright © 2018 Suraj Sagare. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable
class GradientView: UIView {
    
    /// start color for gradient
    @IBInspectable var startColor: UIColor = .black {
        didSet {
            updateColors()
        }
    }
    
    /// end color for gradient
    @IBInspectable var endColor: UIColor = .white {
        didSet {
            updateColors()
        }
    }
    
    /// start location for gradient
    @IBInspectable var startLocation: Double = 0.05 {
        didSet {
            updateLocations()
        }
    }
    
    /// end location for gradient
    @IBInspectable var endLocation: Double = 0.95 {
        didSet {
            updateLocations()
        }
    }
    
    /// is horizontal mode
    @IBInspectable var horizontalMode: Bool = false {
        didSet {
            updatePoints()
        }
    }
    
    /// is diagonal mode
    @IBInspectable var diagonalMode: Bool = false {
        didSet {
            updatePoints()
        }
    }
    
    override class var layerClass: AnyClass {
        return CAGradientLayer.self
    }
    
    var gradientLayer: CAGradientLayer {
        return layer as! CAGradientLayer
    }
    
    
    /**
     Updates start points and end points
     */
    
    func updatePoints() {
        if horizontalMode {
            gradientLayer.startPoint = diagonalMode ? CGPoint(x: 1, y: 0) : CGPoint(x: 0, y: 0.5)
            gradientLayer.endPoint = diagonalMode ? CGPoint(x: 0, y: 1) : CGPoint(x: 1, y: 0.5)
        } else {
            gradientLayer.startPoint = diagonalMode ? CGPoint(x: 0, y: 0) : CGPoint(x: 0.5, y: 0)
            gradientLayer.endPoint = diagonalMode ? CGPoint(x: 1, y: 1) : CGPoint(x: 0.5, y: 1)
        }
    }
    
    /**
     Updates locations with given start and end locations
     */
    
    func updateLocations() {
        gradientLayer.locations = [startLocation as NSNumber, endLocation as NSNumber]
    }
    
    
    /**
     Updates colors with given start and end colors
     */
    
    func updateColors() {
        gradientLayer.colors = [startColor.cgColor, endColor.cgColor]
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        updatePoints()
        updateLocations()
        updateColors()
    }
    
}
