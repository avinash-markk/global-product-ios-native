//
//  AnalyticsHelper.swift
//  Sminq
//
//  Created by Pushkar Deshmukh on 17/05/18.
//  Copyright © 2018 Suraj Sagare. All rights reserved.
//

import Foundation
import CleverTapSDK
import Firebase
import AppsFlyerLib
import Alamofire

class AnalyticsHelper {
    
    struct FollowEventAction {
        static let follow = "follow"
        static let unfollow = "unfollow"
        static let alertsOn = "alerts on"
        static let alertsOff = "alerts off"
    }
    
    struct PublicProfileSource {
        static let post = "post"
        static let search = "search"
        static let notifications = "notifications"
        static let comments = "comments"
        static let home = "home"
    }
    
    struct VoteAction {
        static let up = "up"
        static let down = "down"
    }
    
    struct RevoteAction {
        static let yes = "yes"
        static let no = "no"
    }
    
    enum OnBoardingContinueEventText: String {
        case card1 = "card 1"
        case card2 = "card 2"
        case card3 = "card 3"
    }
    
    enum OnBoardingContinueEventName: String {
        case card1Tapped = "tapped_ob_1"
        case card2Tapped = "tapped_ob_2"
        case card3Tapped = "tapped_ob_3"
    }
    
    enum UploadActionsEventName: String {
        case viewUpload = "view_upload"
        case retryUpload = "retry_upload"
        case cancelUpload = "cancel_upload"
    }
    
    enum ClaimRewardEmailActionEventName: String {
        case confirmEmailTapped = "confirm_email"
        case okayEmailTapped = "okay_email"
    }
    
    enum StoryViewExplainerEventName: String {
        case locVerificationEvent = "story_view_lv"
        case ratingEvent = "story_view_rating"
    }
    
    enum HighlightsTrendingAnalyticsEventName: String {
        case highlightsMostLiked = "highlights_most_liked_places"
        case highlightsMostRated = "highlights_most_rated_places"
        case highlightsMostViewed = "highlights_most_viewed_places"
        case highlightsDopePlaces = "highlights_dope_places"
        case trendingMostLiked = "trending_most_liked_place"
        case trendingMostRated = "trending_most_rated_place"
        case trendingMostViewed = "trending_most_viewed_place"
        case trendingUserClaimReward = "trending_user_claim_reward"
        case trendingUserUnlockedBadge = "trending_user_unlocked_badge"
        case trendingUserMaxRatings = "trending_user_max_rating"
        case trendingUserMaxPlaces = "trending_user_max_places_rated"
        case trendingDopePlaces = "trending_dope_places"
    }
    
    enum PermissionsEventName: String {
        case microphonePermission = "microphone_access"
        case cameraPermission = "camera_access"
        case notificationPermission = "notification_permission"
        case locationPermission = "location_permission"
    }
    
    enum AddRatingEditEvents: String {
        case addRatingEditDraw = "add_rating_add_freehand"
        case addRatingEditPlace = "add_rating_edit_place"
        case addRatingEditRating = "add_rating_edit_rating_selected"
    }
    
    enum DynamicURLType: String {
        case place = "place"
        case ratings = "ratings"
        case userProfileShare = "user_profile_share"
    }
    
    /**
     Pushes user profile to Clevertap by onUserLogin API
     */
    
    static func pushUserProfileToCleverTap(user: UserDataDict) {
        CleverTap.sharedInstance()?.onUserLogin(user.getKeyValueDictionaryForCleverTap())
    }
    
    /**
     Pushees user profile to Clevertap by onUserLogin API
     */
    
    static func userLoginToCleverTap(user: UserDataDict) {
        CleverTap.sharedInstance()?.onUserLogin(user.getKeyValueDictionaryForCleverTap())
    }
  
    /**
     Analytics event for share
     
     - Parameters:
        - sharedTo: platform on which shared
        - userId: logged in user id
        - url: shared url
        - feed - `UserDataPlaceFeed` model
     */
    
    static func shareEvent(sharedTo: String, userId: String, url: URL, feed: UserDataPlaceFeed) {
        
        var eventParams = [
            "post_id": feed._id as NSObject,
            "shared_to": sharedTo as NSObject,
            "user_id": userId as NSObject,
            "url": url.absoluteString as NSObject,
        ] as [String : Any]
        
        if let place = feed.placeDetails {
            eventParams["place_name"] = place.name
            eventParams["place_id"] = place._id
            eventParams["place_subcategory"] = place.getSubCategoryName()
            eventParams["place_city"] = place.addressComponents?.locality
            eventParams["place_area"] = SminqAPI.formattedAddressFromAddressComponentWithoutInitialDots(addressComponent: place.addressComponents)
            eventParams["rating_id"] = place.placeMoodScore ?? 0
        }
        
        Analytics.logEvent("share", parameters: eventParams)
        CleverTap.sharedInstance()?.recordEvent("share", withProps: eventParams)
        
        if ApiConfig.appEnvironment == ENVIRONMENT.production {
            AppsFlyerTracker.shared().trackEvent("share", withValues: eventParams)
        }
    }
    
    /**
     Analytics event for like
     
     - Parameters:
        - screen: screen name from where this event is triggered. This should use struct from `AppConstants.screenNames`
        - userId: logged in user id
        - feed: feed object, `UserDataPlaceFeed` model
     */
    
    static func likeEvent(screen: String, userId: String, feed: UserDataPlaceFeed) {
        
        var eventParams = [
            "screen": screen,
            "post_id": feed._id,
            "user_id": userId,
        ] as [String : Any]
        
        if let place = feed.placeDetails {
            eventParams["place_name"] = place.name
            eventParams["place_id"] = place._id
            eventParams["place_subcategory"] = place.getSubCategoryName()
            eventParams["place_city"] = place.addressComponents?.locality
            eventParams["place_area"] = SminqAPI.formattedAddressFromAddressComponentWithoutInitialDots(addressComponent: place.addressComponents)
            eventParams["rating_id"] = place.placeMoodScore
        }
        
        Analytics.logEvent("like", parameters: eventParams)
        CleverTap.sharedInstance()?.recordEvent("like", withProps: eventParams)
        
        if ApiConfig.appEnvironment == ENVIRONMENT.production {
            AppsFlyerTracker.shared().trackEvent("like", withValues: eventParams)
        }
    }
    
    /**
     Analytics event for comment
     
     - Parameters:
        - isSuccess: if comment is successfully added. If yes/no
        - userId: logged in user id
        - feed: feed object, `UserDataPlaceFeed` model
     */
    
    static func commentSubmitEvent(isSuccess: String, userId: String, feed: UserDataPlaceFeed) {
        var eventParams = [
            "post_id": feed._id as NSObject,
            "success": isSuccess as NSObject,
            "user_id": userId as NSObject,
        ] as [String : Any]
        
        if let place = feed.placeDetails {
            eventParams["place_name"] = place.name
            eventParams["place_id"] = place._id
            eventParams["place_subcategory"] = place.getSubCategoryName()
            eventParams["place_city"] = place.addressComponents?.locality
            eventParams["place_area"] = SminqAPI.formattedAddressFromAddressComponentWithoutInitialDots(addressComponent: place.addressComponents)
            eventParams["rating_id"] = place.placeMoodScore ?? 0
        }
        
        Analytics.logEvent("comment_submit", parameters: eventParams)
        CleverTap.sharedInstance()?.recordEvent("comment_submit", withProps: eventParams)
        
        if ApiConfig.appEnvironment == ENVIRONMENT.production {
            AppsFlyerTracker.shared().trackEvent("comment_submit", withValues: eventParams)
        }
    }
    
    /**
     Analytics event for comment delete
     
     - Parameters:
        - userId: logged in user id
        - feed: feed object, `UserDataPlaceFeed` model
     */
    
    static func commentDeleteEvent(userId: String, feed: UserDataPlaceFeed) {
        var eventParams = [
            "user_id": userId,
        ] as [String : Any]
        
        if let place = feed.placeDetails {
            eventParams["place_name"] = place.name
            eventParams["place_id"] = place._id
            eventParams["place_subcategory"] = place.getSubCategoryName()
            eventParams["place_city"] = place.addressComponents?.locality
            eventParams["place_area"] = SminqAPI.formattedAddressFromAddressComponentWithoutInitialDots(addressComponent: place.addressComponents)
            eventParams["rating_id"] = place.placeMoodScore ?? 0
        }
        
        Analytics.logEvent("delete_comment", parameters: eventParams)
        CleverTap.sharedInstance()?.recordEvent("delete_comment", withProps: eventParams)
        
        if ApiConfig.appEnvironment == ENVIRONMENT.production {
            AppsFlyerTracker.shared().trackEvent("delete_comment", withValues: eventParams)
        }
    }
    
    /**
     Analytics event for logout
     */
    
    static func logoutEvent() {
        let eventParams = [
            "user_id": Helper().getUserId(),
            ] as [String : Any]
        Analytics.logEvent("logout", parameters: eventParams)
        CleverTap.sharedInstance()?.recordEvent("logout", withProps: eventParams)
        AppsFlyerTracker.shared().trackEvent("logout", withValues: eventParams)
    }
    
    /**
     Analytics event for logout
     
     - Parameters:
        - method: sign up method from struct `AppConstants.Login`
     */
    
    static func signUpOptionsSelectedEvent(method: String) {
        let eventParams = [
            "method": method as NSObject
        ]
        
        Analytics.logEvent("signup_options_selected", parameters: eventParams)
        CleverTap.sharedInstance()?.recordEvent("signup_options_selected", withProps: eventParams)
        
        if ApiConfig.appEnvironment == ENVIRONMENT.production {
            AppsFlyerTracker.shared().trackEvent("signup_options_selected", withValues: eventParams)
        }
    }
    
    /**
     Analytics event for sign up
     
     - Parameters:
        - success: if signed up successfully. yes/no
        - error: error on signup
     */
    
    static func signupEvent(success: String, error: String) {
        var eventParams = [
            "success": success as NSObject
        ]
        
        if error != "" {
            eventParams["error"] = error as NSObject
        }
        
        Analytics.logEvent("signup_first_time", parameters: eventParams)
        CleverTap.sharedInstance()?.recordEvent("signup_first_time", withProps: eventParams)
        
        if ApiConfig.appEnvironment == ENVIRONMENT.production {
            AppsFlyerTracker.shared().trackEvent("signup_first_time", withValues: eventParams)
        }
    }
    
    /**
     Analytics event for sign up options selection
     
     - Parameters:
        - method: sign up option from `AppConstants.Login`
        - success: if signed up successfully. yes/no
        - error: error on signup
     */
    
    static func signupOptionsEvent(method: String, success: String, error: String) {
        let eventParams = [
            "method": method as NSObject,
            "success": success as NSObject,
            "error": error as NSObject
        ]
        
        Analytics.logEvent("signup_completed", parameters: eventParams)
        CleverTap.sharedInstance()?.recordEvent("signup_completed", withProps: eventParams)
        
        if ApiConfig.appEnvironment == ENVIRONMENT.production {
            AppsFlyerTracker.shared().trackEvent("signup_completed", withValues: eventParams)
        }
    }
    
    /**
     Analytics event for policy view
     
     - Parameters:
        - screen: screen name
     */

    static func viewedPrivacyPolicyEvent(screen: String) {
        let eventParams = [
            "screen": screen as NSObject
        ]
        
        Analytics.logEvent("viewed_privacy_policy", parameters: eventParams)
        CleverTap.sharedInstance()?.recordEvent("viewed_privacy_policy", withProps: eventParams)
        
        if ApiConfig.appEnvironment == ENVIRONMENT.production {
            AppsFlyerTracker.shared().trackEvent("viewed_privacy_policy", withValues: eventParams)
        }
    }
    
    /**
     Analytics event for terms and conditions
     
     - Parameters:
        - screen: scree name
     */
    
    static func viewedTermsConditionsEvent(screen: String) {
        let eventParams = [
            "screen": screen as NSObject
        ]
        
        Analytics.logEvent("viewed_terms_conditions", parameters: eventParams)
        CleverTap.sharedInstance()?.recordEvent("viewed_terms_conditions", withProps: eventParams)
        
        if ApiConfig.appEnvironment == ENVIRONMENT.production {
            AppsFlyerTracker.shared().trackEvent("viewed_terms_conditions", withValues: eventParams)
        }
    }
    
    /**
     Analytics event for directions
     
     - Parameters:
        - screen: screen name
        - place: place object, `LivePlaces` model
     */
    
    static func directionsEvent(screen: String, place: LivePlaces) {
        let eventParams = [
            "screen": screen as NSObject,
            "place_name": place.name as NSObject,
            "place_id": place._id as NSObject,
            "place_subcategory": place.getSubCategoryName() as NSObject,
            "place_area": SminqAPI.formattedAddressFromAddressComponentWithoutInitialDots(addressComponent: place.addressComponents) as NSObject
        ]
        
        Analytics.logEvent("directions", parameters: eventParams)
        CleverTap.sharedInstance()?.recordEvent("directions", withProps: eventParams)
        
        if ApiConfig.appEnvironment == ENVIRONMENT.production {
            AppsFlyerTracker.shared().trackEvent("directions", withValues: eventParams)
        }
    }
    
    /**
     Analytics event for call
     
     - Parameters:
        - screen: screen name
        - place: place object, `LivePlaces` model
     */
    
    static func callEvent(screen: String, place: LivePlaces) {
        let eventParams = [
            "screen": screen as NSObject,
            "place_name": place.name as NSObject,
            "place_id": place._id as NSObject,
            "place_subcategory": place.getSubCategoryName() as NSObject,
            "place_area": SminqAPI.formattedAddressFromAddressComponentWithoutInitialDots(addressComponent: place.addressComponents) as NSObject
        ]
        
        Analytics.logEvent("call", parameters: eventParams)
        CleverTap.sharedInstance()?.recordEvent("call", withProps: eventParams)
        
        if ApiConfig.appEnvironment == ENVIRONMENT.production {
            AppsFlyerTracker.shared().trackEvent("call", withValues: eventParams)
        }
    }
    
    /**
     Analytics event for delete rating
     
     - Parameters:
        - feed: feed object, `UserDataPlaceFeed` model
        - screen: screen name
     */

    static func deleteRatingEvent(feed: UserDataPlaceFeed, screen: String) {
        let eventParams = [
            "user_id": Helper().getUserId(),
            "screen": screen,
            "place_name": feed.placeDetails.name ?? "",
            "place_id": feed.placeDetails._id ?? "",
            "place_subcategory": feed.placeDetails.getSubCategoryName(),
            "place_city": feed.placeDetails.addressComponents?.locality ?? "",
            "place_area": SminqAPI.formattedAddressFromAddressComponentWithoutInitialDots(addressComponent: feed.placeDetails.addressComponents) as NSObject,
            "rating_id": "\(feed.placeDetails.placeMoodScore ?? 0)"
            ] as [String : Any]
        
        Analytics.logEvent("delete_rating", parameters: eventParams)
        CleverTap.sharedInstance()?.recordEvent("delete_rating", withProps: eventParams)
        
        if ApiConfig.appEnvironment == ENVIRONMENT.production {
            AppsFlyerTracker.shared().trackEvent("delete_rating", withValues: eventParams)
        }
    }
    
    /**
     Analytics event for notifications views
     
     - Parameters:
        - screen: screen name
     */
    
    static func visitedNotificationsEvent(screen: String) {
        let eventParams = [
            "screen": screen,
            "user_id": Helper().getUserId()
        ] as [String : Any]
        
        Analytics.logEvent("visited_notifications", parameters: eventParams)
        CleverTap.sharedInstance()?.recordEvent("visited_notifications", withProps: eventParams)
        
        if ApiConfig.appEnvironment == ENVIRONMENT.production {
            AppsFlyerTracker.shared().trackEvent("visited_notifications", withValues: eventParams)
        }
        
    }
    
    /**
     Analytics event for notifications clear
     */
    
    static func notificationClearClickEvent() {
        Analytics.logEvent("notification_clear_click", parameters: [:])
        CleverTap.sharedInstance()?.recordEvent("notification_clear_click", withProps: [:])
        
        if ApiConfig.appEnvironment == ENVIRONMENT.production {
            AppsFlyerTracker.shared().trackEvent("notification_clear_click", withValues: [:])
        }
    }
    
    /**
     Analytics event for edit profile
     */
    
    static func editProfileEvent() {
        let eventParams = [
            "user_id": Helper().getUserId() as NSObject
        ]
        
        Analytics.logEvent("edit_profile", parameters: eventParams)
        CleverTap.sharedInstance()?.recordEvent("edit_profile", withProps: eventParams)
        
        if ApiConfig.appEnvironment == ENVIRONMENT.production {
            AppsFlyerTracker.shared().trackEvent("edit_profile", withValues: eventParams)
        }
    }
    
    /**
     Analytics event for archive ratings view
     */
    
    static func viewPastPostEvent() {
        let eventParams = [
            "user_id": Helper().getUserId() as NSObject
        ]
        Analytics.logEvent("view_past_post", parameters: [:])
        CleverTap.sharedInstance()?.recordEvent("view_past_post", withProps: eventParams)
        
        if ApiConfig.appEnvironment == ENVIRONMENT.production {
            AppsFlyerTracker.shared().trackEvent("view_past_post", withValues: eventParams)
        }
    }
    
    /**
     Analytics event for comments view
     
     - Parameters:
        - feed: place object, `UserDataPlaceFeed` model
     */
    
    static func viewCommentEvent(feed: UserDataPlaceFeed) {
        var eventParams = [
            "post_id": feed._id as NSObject
            ] as [String : Any]
    
        if let place = feed.placeDetails {
            eventParams["place_name"] = place.name
            eventParams["place_id"] = place._id
            eventParams["place_subcategory"] = place.getSubCategoryName()
            eventParams["place_city"] = place.addressComponents?.locality
            eventParams["place_area"] = SminqAPI.formattedAddressFromAddressComponentWithoutInitialDots(addressComponent: place.addressComponents)
            eventParams["rating_id"] = place.placeMoodScore ?? 0
        }
        
        Analytics.logEvent("view_comment", parameters: eventParams)
        CleverTap.sharedInstance()?.recordEvent("view_comment", withProps: eventParams)
        
        if ApiConfig.appEnvironment == ENVIRONMENT.production {
            AppsFlyerTracker.shared().trackEvent("view_comment", withValues: eventParams)
        }
    }
    
    /**
     Analytics event for session stop
     
     - Parameters:
        - time: Session time in Integer
        - date: Session date in String
     */
    
    static func sessionStoppedEvent(time: Int, date: String) {
        let eventParams = [
            "session_time": "\(time)" as NSObject,
            "session_date": date as NSObject
        ]
        
        // NOTE: This needs to be tracked only for CT
        CleverTap.sharedInstance()?.recordEvent("session_stopped", withProps: eventParams)
    }
    
    /**
     Analytics event for story view
     
     - Parameters:
        - screen: Screen name
        - action: action for this event
        - place: place object, `LivePlaces` model
     */
    
    static func visitedStoryView(screen: String, action: String, tab: String, place: LivePlaces) {
        let eventParams = [
            "screen": screen as NSObject,
            "action": action as NSObject,
            "tab": tab as NSObject,
            "place_name": place.name ?? "",
            "place_id": place._id ?? "",
            "place_subcategory": place.getSubCategoryName(),
            "place_city": place.addressComponents?.locality ?? "",
            "place_area": SminqAPI.formattedAddressFromAddressComponentWithoutInitialDots(addressComponent: place.addressComponents),
        ] as [String : Any]
        
        Analytics.logEvent("visited_story_view", parameters: eventParams)
        CleverTap.sharedInstance()?.recordEvent("visited_story_view", withProps: eventParams)
        
        if ApiConfig.appEnvironment == ENVIRONMENT.production {
            AppsFlyerTracker.shared().trackEvent("visited_story_view", withValues: eventParams)
        }
    }
    
    /**
     Analytics event for ratings view
     
     - Parameters:
        - mediaType: media, image/video
        - source: source of this event
        - section: empty for now
        - feed: feed object, `UserDataPlaceFeed` model
     */
    
    static func viewRatingEvent(screen: String, section: String, feed: UserDataPlaceFeed) {
        var mediaType = ""
        if let type = feed.getFeedMediaType() {
            switch type {
            case .image:
                mediaType = AppConstants.image
            case .video:
                mediaType = AppConstants.video
            }
        }
        
        var eventParams = [
            "user_id": Helper().getUserId(),
            "media_type": mediaType,
            "creator_id": feed.user._id,
            "screen": screen,
            
            "section": section
            ] as [String : Any]
        
        if let place = feed.placeDetails {
            eventParams["place_name"] = place.name
            eventParams["place_id"] = place._id
            eventParams["place_subcategory"] = place.getSubCategoryName()
            eventParams["place_city"] = place.addressComponents?.locality
            eventParams["place_area"] = SminqAPI.formattedAddressFromAddressComponentWithoutInitialDots(addressComponent: place.addressComponents)
            eventParams["rating_id"] = place.placeMoodScore ?? 0
        }
        
        Analytics.logEvent("view_rating", parameters: eventParams)
        CleverTap.sharedInstance()?.recordEvent("view_rating", withProps: eventParams)
        
        if ApiConfig.appEnvironment == ENVIRONMENT.production {
            AppsFlyerTracker.shared().trackEvent("view_rating", withValues: eventParams)
        }
    }
    
    /**
     Analytics event for place details view
     
     - Parameters:
        - place: place object, `LivePlaces` model
     */
    
    static func visitedPlaceDetails(place: LivePlaces) {
        let eventParams = [
            "place_name": place.name as NSObject,
            "place_id": place._id as NSObject,
            "place_subcategory": place.getSubCategoryName() as NSObject,
            "place_area": SminqAPI.formattedAddressFromAddressComponentWithoutInitialDots(addressComponent: place.addressComponents) as NSObject,
            "place_city": (place.addressComponents?.locality ?? "") as NSObject
        ]
        
        Analytics.logEvent("visited_place_details", parameters: eventParams)
        CleverTap.sharedInstance()?.recordEvent("visited_place_details", withProps: eventParams)
        
        if ApiConfig.appEnvironment == ENVIRONMENT.production {
            AppsFlyerTracker.shared().trackEvent("visited_place_details", withValues: eventParams)
        }
    }
    
    /**
     Analytics event for place details dismiss
     
     - Parameters:
        - place: place object, `LivePlaces` model
     */
    
    static func closePlaceEvent(place: LivePlaces) {
        let eventParams = [
            "place_name": place.name as NSObject,
            "place_id": place._id as NSObject,
            "place_subcategory": place.getSubCategoryName() as NSObject,
            "place_area": SminqAPI.formattedAddressFromAddressComponentWithoutInitialDots(addressComponent: place.addressComponents) as NSObject,
            "user_id": Helper().getUserId() as NSObject
        ]
        
        Analytics.logEvent("close_place", parameters: eventParams)
        CleverTap.sharedInstance()?.recordEvent("close_place", withProps: eventParams)
        
        if ApiConfig.appEnvironment == ENVIRONMENT.production {
            AppsFlyerTracker.shared().trackEvent("close_place", withValues: eventParams)
        }
    }
    
    /**
     Analytics event for profile view
     
     - Parameters:
        - screen: screen name
        - source: source of the event
        - userId: logged in user id
        - isSelf: Boolean, if self profile
     */
    
    static func viewProfileEvent(screen: String, source: String, userId: String, isSelf: Bool) {
        let eventParams = [
            "screen": screen as NSObject,
            "source": source as NSObject,
            "user_id": userId as NSObject,
            "self": isSelf as NSObject
        ]
        
        Analytics.logEvent("tapped_profile", parameters: eventParams)
        CleverTap.sharedInstance()?.recordEvent("tapped_profile", withProps: eventParams)
        
        if ApiConfig.appEnvironment == ENVIRONMENT.production {
            AppsFlyerTracker.shared().trackEvent("tapped_profile", withValues: eventParams)
        }
        
    }
    
    /**
     Analytics event for home tab view
     */
    
    static func viewHomeEvent() {
        let eventParams = [
            "user_id": Helper().getUserId(),
        ]
        Analytics.logEvent("tapped_home", parameters: eventParams)
        CleverTap.sharedInstance()?.recordEvent("tapped_home", withProps: eventParams)
        
        if ApiConfig.appEnvironment == ENVIRONMENT.production {
            AppsFlyerTracker.shared().trackEvent("tapped_home", withValues: eventParams)
        }
    }
    
    /**
     Analytics event for report rating
     
     - Parameters:
        - feed: feed object, `UserDataPlaceFeed` model
     */
    
    static func reportRatingEvent(feed: UserDataPlaceFeed) {
        var eventParams = [
            "post_id": feed._id,
            "user_id": Helper().getUserId(),
        ] as [String : Any]
        
        if let place = feed.placeDetails {
            eventParams["place_name"] = place.name
            eventParams["place_id"] = place._id
            eventParams["place_subcategory"] = place.getSubCategoryName()
            eventParams["place_city"] = place.addressComponents?.locality
            eventParams["place_area"] = SminqAPI.formattedAddressFromAddressComponentWithoutInitialDots(addressComponent: place.addressComponents)
            eventParams["rating_id"] = place.placeMoodScore ?? 0
        }
        
        Analytics.logEvent("report_rating", parameters: eventParams)
        CleverTap.sharedInstance()?.recordEvent("report_rating", withProps: eventParams)
        
        if ApiConfig.appEnvironment == ENVIRONMENT.production {
            AppsFlyerTracker.shared().trackEvent("report_rating", withValues: eventParams)
        }
        
    }

    /**
     Analytics event for camera button
     
     - Parameters:
     - screen: screen name
     - section: from which section the event is triggered. Should be from struct `AppConstants.RatingsIntendSection`
     - addToStory: if flow is add-to-story, yes/no
     - place: If flow is add-to-story then place detail should be sent
     */
    
    static func addRatingIntend(screen: String, section: String, addToStory: String, place: LivePlaces?) {
        var eventParams: [String : Any]  = [
            "screen": screen,
            "user_id": Helper().getUserId(),
            "section": section,
            "add_to_story": addToStory
            ] as [String : Any]
        
        if addToStory == "yes" {
            if let place = place {
                eventParams["place_name"] = place.name
                eventParams["place_id"] = place._id
                eventParams["place_subcategory"] = place.getSubCategoryName()
                eventParams["place_city"] = place.addressComponents?.locality
                eventParams["place_area"] = SminqAPI.formattedAddressFromAddressComponentWithoutInitialDots(addressComponent: place.addressComponents)
                eventParams["rating_id"] = place.placeMoodScore ?? 0
            }
        }
        
        Analytics.logEvent("add_rating_intend", parameters: eventParams)
        CleverTap.sharedInstance()?.recordEvent("add_rating_intend", withProps: eventParams)
        
        if ApiConfig.appEnvironment == ENVIRONMENT.production {
            AppsFlyerTracker.shared().trackEvent("add_rating_intend", withValues: eventParams)
        }
    }
    
    /**
     Analytics event for media selection
     
     - Parameters:
        - type: image/video
        - liveShot: is live shot , yes/no
     */
    
    static func addRatingMedia(type: String?, liveShot: Bool, video: Int?, image: Int?, totalCount: Int?) {
        var eventParams = [
            "user_id": Helper().getUserId(),
            "media_type": type ?? "",
            "live_shot": liveShot ? "yes" : "no",
        ]
        
        if liveShot {
        eventParams["number of media"] = "\(totalCount ?? 0)"
        eventParams["video"] = "\(video ?? 0)"
        eventParams["image"] = "\(image ?? 0)"
        }
        
        Analytics.logEvent("add_rating_media", parameters: eventParams)
        CleverTap.sharedInstance()?.recordEvent("add_rating_media", withProps: eventParams)
        
        if ApiConfig.appEnvironment == ENVIRONMENT.production {
            AppsFlyerTracker.shared().trackEvent("add_rating_media", withValues: eventParams)
        }
    }
    
    /**
     Analytics event for place selection
     
     - Parameters:
        - place: place object, `LivePlaces` model
     */
    
    static func addRatingSelectPlace(place: LivePlaces) {
        let eventParams = [
            "user_id": Helper().getUserId(),
            "place_id": place._id ?? "",
            "place_subcategory": place.subCategory.name ?? "",
            "place_area": SminqAPI.formattedAddressFromAddressComponentWithoutInitialDots(addressComponent: place.addressComponents),
            "place_city": place.addressComponents?.locality ?? "",
            "place_name": place.name ?? ""
        ]
        
        Analytics.logEvent("add_rating_select_place", parameters: eventParams)
        CleverTap.sharedInstance()?.recordEvent("add_rating_select_place", withProps: eventParams)
        
        if ApiConfig.appEnvironment == ENVIRONMENT.production {
            AppsFlyerTracker.shared().trackEvent("add_rating_select_place", withValues: eventParams)
        }
    }
    
    static func addRatingSelectRating(rating: String, place: LivePlaces) {
        let eventParams = [
            "user_id": Helper().getUserId(),
            "place_id": place._id ?? "",
            "place_subcategory": place.subCategory.name ?? "",
            "place_area": SminqAPI.formattedAddressFromAddressComponentWithoutInitialDots(addressComponent: place.addressComponents),
            "place_city": place.addressComponents?.locality ?? "",
            "place_name": place.name ?? "",
            "rating_selected": rating
        ]
        
        Analytics.logEvent("add_rating_select_rating", parameters: eventParams)
        CleverTap.sharedInstance()?.recordEvent("add_rating_select_rating", withProps: eventParams)
        
        if ApiConfig.appEnvironment == ENVIRONMENT.production {
            AppsFlyerTracker.shared().trackEvent("add_rating_select_rating", withValues: eventParams)
        }
    }
    
    /**
     Analytics event for succesful rating
     
     - Parameters:
        - postData: postData object, `PostData` model
        - success: yes/no
        - isVideo: Boolean
        - screen: screen name
     */
    
    static func addRatingSubmit(postData: PostData, success: String, isVideo: Bool, screen: String) {
        var eventParams = [
            "user_id": Helper().getUserId(),
            "media_type": "\(isVideo ? "Video" : "Image")",
            "live_shot": postData.liveShot ?? "",
            "place_name": postData.place?.name ?? "",
            "place_id": postData.place?._id ?? "",
            "place_subcategory": postData.place?.subCategory.name ?? "",
            "place_area": SminqAPI.formattedAddressFromAddressComponentWithoutInitialDots(addressComponent: postData.place?.addressComponents),
        ]
        
        eventParams["screen"] = screen
        eventParams["place_city"] = postData.place?.addressComponents?.locality
        eventParams["rating_selected"] = postData.mood ?? ""
        eventParams["success"] = success
        
        Analytics.logEvent("add_rating_submit", parameters: eventParams)
        CleverTap.sharedInstance()?.recordEvent("add_rating_submit", withProps: eventParams)
        
        if ApiConfig.appEnvironment == ENVIRONMENT.production {
            AppsFlyerTracker.shared().trackEvent("add_rating_submit", withValues: eventParams)
        }
    }
    
    /**
     Analytics event for post button in camera flow
     
     - Parameters:
        - postData: postData object, `PostData` model
        - isVideo: Boolean
        - stickerString: comma seperated sticker values
        - screen: Screen name
     */
    
    static func addRatingDoneEvent(postData: PostData, isVideo: Bool, stickerString: String, screen: String) {
        var eventParams = [
            "user_id": Helper().getUserId(),
            "media_type": "\(isVideo ? "Video" : "Image")",
            "live_shot": postData.liveShot ?? "",
            "place_name": postData.place?.name ?? "",
            "place_id": postData.place?._id ?? "",
            "place_subcategory": postData.place?.subCategory.name ?? "",
            "place_area": SminqAPI.formattedAddressFromAddressComponentWithoutInitialDots(addressComponent: postData.place?.addressComponents),
        ]
        
        eventParams["screen"] = screen
        eventParams["place_city"] = postData.place?.addressComponents?.locality ?? ""
        eventParams["stickers"] = stickerString
        eventParams["rating_selected"] = postData.mood ?? ""
        
        Analytics.logEvent("add_rating_done", parameters: eventParams)
        CleverTap.sharedInstance()?.recordEvent("add_rating_done", withProps: eventParams)
        
        if ApiConfig.appEnvironment == ENVIRONMENT.production {
            AppsFlyerTracker.shared().trackEvent("add_rating_done", withValues: eventParams)
        }
    }
    
    /**
     Analytics event for cloudinary failures
     
     - Parameters:
        - mediaType: image/video
        - reason: Failure reason
     */
    
    static func cloudinaryUploadFailedEvent(mediaType: String, reason: String) {
        let eventParams = [
            "media_type": mediaType as NSObject,
            "reason": reason as NSObject
        ]
        
        Analytics.logEvent("cloudinary_upload_failed", parameters: eventParams)
        CleverTap.sharedInstance()?.recordEvent("cloudinary_upload_failed", withProps: eventParams)
        
        if ApiConfig.appEnvironment == ENVIRONMENT.production {
            AppsFlyerTracker.shared().trackEvent("cloudinary_upload_failed", withValues: eventParams)
        }
    }
    
    /**
     Analytics event for test user location
     
     - Parameters:
        - oldLatitude: old user location latitude
        - oldLongitude: old user location longitude
        - newLatitude: new user location latitude
        - newLongitude: new user location latitude
        - placeId - markk place id
        - placeName - place name
     */
    
    static func testUserLocationOnPostEvent(oldLatitude: Double, oldLongitude: Double, newLatitude: Double, newLongitude: Double, placeId: String, placeName: String) {
        let eventParams = [
            "old_latitude": "\(oldLatitude)" as NSObject,
            "old_longitude": "\(oldLongitude)" as NSObject,
            "new_latitude": "\(newLatitude)" as NSObject,
            "new_longitude": "\(newLongitude)" as NSObject,
            "place_name": placeName as NSObject
        ]
        
        Analytics.logEvent("test_user_location_on_post", parameters: eventParams)
        CleverTap.sharedInstance()?.recordEvent("test_user_location_on_post", withProps: eventParams)
        
        if ApiConfig.appEnvironment == ENVIRONMENT.production {
            AppsFlyerTracker.shared().trackEvent("test_user_location_on_post", withValues: eventParams)
        }
    }
    
    /**
     Analytics event for force update
     
     - Parameters:
        - currentVersion: current version of an app
        - remoteConfigVersion: remote configured version of an app
     */
    
    static func forceUpdateEvent(currentVersion: String, remoteConfigVersion: String) {
        let eventParams = [
            "current_app_version": currentVersion as NSObject,
            "remote_config_app_version": remoteConfigVersion as NSObject
        ]
        
        Analytics.logEvent("forceUpdateEvent", parameters: eventParams)
        CleverTap.sharedInstance()?.recordEvent("forceUpdateEvent", withProps: eventParams)
        
        if ApiConfig.appEnvironment == ENVIRONMENT.production {
            AppsFlyerTracker.shared().trackEvent("forceUpdateEvent", withValues: eventParams)
        }
    }
    
    /**
     Analytics event for API failures
     
     - Parameters:
        - screen: screen name
        - apiName: api name. Should be used from struct `AppConstants.apiNames`
        - errorMessage: error message
        - userId: logged in user id
        - errorCode: error code returned from API
        - requestPayload: dictionary for requested payload data
        - requestPathParams: dictionary for requested query params data
     */
    
    static func APIFailureEvent(screen: String, apiName: String, errorMessage: String, userId: String, errorCode: Int?, requestPayload: [String: Any], requestMethod: String, requestPathParams: [String: Any]) {
        var eventParams = [
            "screen": screen as NSObject,
            "api_name": apiName as NSObject,
            "error_message": errorMessage as NSObject,
            "user_id": userId as NSObject
        ]
        
        if let code = errorCode {
            eventParams["error_code"] = "\(code)" as NSObject
        }
        
        if requestMethod.lowercased() == AppConstants.requestMethods.get {
            eventParams["payload"] = DictionaryHelper.constructKeyValueStringFromDictionary(requestPayload: requestPayload) as NSObject
        } else if requestMethod.lowercased() == AppConstants.requestMethods.post || requestMethod.lowercased() == AppConstants.requestMethods.put || requestMethod.lowercased() == AppConstants.requestMethods.delete {
            eventParams["payload"] = DictionaryHelper.getJSONStringFromDictionary(dictionary: requestPayload) as NSObject
        }
        
        eventParams["path_params"] = DictionaryHelper.constructKeyValueStringFromDictionary(requestPayload: requestPathParams) as NSObject
        
        Analytics.logEvent("api_failure", parameters: eventParams)
        CleverTap.sharedInstance()?.recordEvent("api_failure", withProps: eventParams)
        
        if ApiConfig.appEnvironment == ENVIRONMENT.production {
            AppsFlyerTracker.shared().trackEvent("api_failure", withValues: eventParams)
        }
    }
    
    // Depcrecated
    static func postLocationDataEvent(userId: String, placeId: String, placeSubcategory: String, postType: String, videoLength: String, locationVerified: Bool, placeLatitude: Double, placeLongitude: Double, userLocation: CLLocation, userLocations: [CLLocation], totalPostTime: String, locationFetchTime: String, userLocationType: String) {
        
        // TODO: Pass if less accurate or best accurate. Pass time required to fetch location (it could be either best accurate or less accurate)
        let formattedLocations = Helper().convertCLLocationToFormattedString(locations: userLocations) ?? ""
        
        let eventParams = [
            "user_id": userId,
            "place_id": placeId,
            "place_subcategory": placeSubcategory,
            "post_type": postType,
            "video_length": videoLength,
            "verification_status": locationVerified,
            "place_lat_long": "\(placeLatitude),\(placeLongitude)",
            "user_lat_long": "\(userLocation.coordinate.latitude),\(userLocation.coordinate.longitude)",
            "accuracy": "\(userLocation.horizontalAccuracy)",
            "list_of_interim_location_lat_long": "\(formattedLocations)",
            "total_time_post": totalPostTime,
            "user_location_fetch_time": locationFetchTime,
            "user_location_type": userLocationType
            ] as [String: Any]
        
        Analytics.logEvent("post_location_data", parameters: eventParams)
        CleverTap.sharedInstance()?.recordEvent("post_location_data", withProps: eventParams)
        
        if ApiConfig.appEnvironment == ENVIRONMENT.production {
            AppsFlyerTracker.shared().trackEvent("post_location_data", withValues: eventParams)
        }
    }
    
    static func tappedObContinueEvent(eventText: OnBoardingContinueEventText, eventName: OnBoardingContinueEventName) {
        let eventParams = [
            "ob_card": eventText.rawValue as NSObject
        ]
        
        Analytics.logEvent(eventName.rawValue, parameters: eventParams)
        CleverTap.sharedInstance()?.recordEvent(eventName.rawValue, withProps: eventParams)
        
        if ApiConfig.appEnvironment == ENVIRONMENT.production {
            AppsFlyerTracker.shared().trackEvent(eventName.rawValue, withValues: eventParams)
        }
    }
    
    /**
     Analytics event for sticker search in ratings flow
     
     - Parameters:
        - searchTerm: sticker search string
     */
    
    static func stickerSearchEvent(searchTerm: String) {
        let eventParam = ["sticker_search": searchTerm]
        Analytics.logEvent("sticker_search", parameters: eventParam)
        CleverTap.sharedInstance()?.recordEvent("sticker_search", withProps: eventParam)
        if ApiConfig.appEnvironment == ENVIRONMENT.production {
            AppsFlyerTracker.shared().trackEvent("sticker_search", withValues: eventParam)
        }
    }
    
    
    /**
     Analytics event for search
     
     - Parameters:
     - screen: Event triggered from
     - searchedFor: search text
     - resultsReturned: Are results returned for given search query
     */
    
    static func searchEvent(screen: String, searchedFor: String, resultsReturned: String) {
        let eventParam = [
            "screen": screen,
            "searched_for": searchedFor,
            "results_returned": resultsReturned
        ] as [String: Any]
        
        Analytics.logEvent("search", parameters: eventParam)
        CleverTap.sharedInstance()?.recordEvent("search", withProps: eventParam)
        if ApiConfig.appEnvironment == ENVIRONMENT.production {
            AppsFlyerTracker.shared().trackEvent("search", withValues: eventParam)
        }
    }
    
    /**
     Analytics event for search
     
     - Parameters:
     - searchedString: search text
     - resultsReturned: Are results returned for given search query
     */
    
    static func tappedSearchResultEvent(searchedString: String, resultsReturned: String, place: LivePlaces) {
        let eventParam = [
            "searched_string": searchedString,
            "results_returned": resultsReturned,
            "place_id": place._id ?? "",
            "place_subcategory": place.subCategory.name ?? "",
            "place_area": SminqAPI.formattedAddressFromAddressComponentWithoutInitialDots(addressComponent: place.addressComponents),
            "place_city": place.addressComponents?.locality ?? "",
            "place_name": place.name ?? ""
            ] as [String: Any]
        
        Analytics.logEvent("tapped_search_result", parameters: eventParam)
        CleverTap.sharedInstance()?.recordEvent("tapped_search_result", withProps: eventParam)
        if ApiConfig.appEnvironment == ENVIRONMENT.production {
            AppsFlyerTracker.shared().trackEvent("tapped_search_result", withValues: eventParam)
        }
    }
    
    /**
     Analytics event for app permission like location access, notifications, camera access, microphone access
     
     - Parameters:
     
     - permission: If permission granted/denied
     */
    
    static func appPermissionAnalyticEvent(eventName: PermissionsEventName, permission: String) {
        let eventParam = [
            "permission": permission,
            "user_id": Helper().getUserId()
            ] as [String: Any]
        
        Analytics.logEvent(eventName.rawValue, parameters: eventParam)
        CleverTap.sharedInstance()?.recordEvent(eventName.rawValue, withProps: eventParam)
        if ApiConfig.appEnvironment == ENVIRONMENT.production {
            AppsFlyerTracker.shared().trackEvent(eventName.rawValue, withValues: eventParam)
        }
    }
    
    static func locationFetchRetryEvent() {
        let eventParam = [
            "user_id": Helper().getUserId()
            ] as [String: Any]
        
        Analytics.logEvent("location_fetch_retry", parameters: eventParam)
        CleverTap.sharedInstance()?.recordEvent("location_fetch_retry", withProps: eventParam)
        if ApiConfig.appEnvironment == ENVIRONMENT.production {
            AppsFlyerTracker.shared().trackEvent("location_fetch_retry", withValues: eventParam)
        }
    }
    
    static func profileArchivesTabEvent() {
        let eventParam = [
            "user_id": Helper().getUserId()
            ] as [String: Any]
        
        Analytics.logEvent("tab_archived", parameters: eventParam)
        CleverTap.sharedInstance()?.recordEvent("tab_archived", withProps: eventParam)
        if ApiConfig.appEnvironment == ENVIRONMENT.production {
            AppsFlyerTracker.shared().trackEvent("tab_archived", withValues: eventParam)
        }
    }
    
    static func profileBadgesTabEvent() {
        let eventParam = [
            "user_id": Helper().getUserId()
            ] as [String: Any]
        
        Analytics.logEvent("tab_badges", parameters: eventParam)
        CleverTap.sharedInstance()?.recordEvent("tab_badges", withProps: eventParam)
        if ApiConfig.appEnvironment == ENVIRONMENT.production {
            AppsFlyerTracker.shared().trackEvent("tab_badges", withValues: eventParam)
        }
    }
    
    static func homeBadgeTappedEvent(badgeName: String) {
        let eventParam = [
            "user_id": Helper().getUserId(),
            "badge_name":badgeName
            ] as [String: Any]
        
        Analytics.logEvent("tapped_badge", parameters: eventParam)
        CleverTap.sharedInstance()?.recordEvent("tapped_badge", withProps: eventParam)
        if ApiConfig.appEnvironment == ENVIRONMENT.production {
            AppsFlyerTracker.shared().trackEvent("tapped_badge", withValues: eventParam)
        }
    }
    
    static func homeSubscribeEvent() {
        let eventParam = [
            "user_id": Helper().getUserId()
            ] as [String: Any]
        
        Analytics.logEvent("subscribe", parameters: eventParam)
        CleverTap.sharedInstance()?.recordEvent("subscribe", withProps: eventParam)
        if ApiConfig.appEnvironment == ENVIRONMENT.production {
            AppsFlyerTracker.shared().trackEvent("subscribe", withValues: eventParam)
        }
    }
    
    /**
     Analytics event for location permission
     
     - Parameters:
     
     - pickedLocation: `Location` model if location is not picked from gallery
     - isFromGallery: flag if location is picked from gallery
     - locationData: Double array, contains latitude and longitude from gallery asset
     */
    
    static func locationDebugInfoEvent(pickedLocation: Location?, isFromGallery: Bool, locationData: [Double], availableLocations: [Location], place: LivePlaces) {
        var eventParam = [
            "is_from_gallery": isFromGallery ? AppConstants.yes : AppConstants.no,
            "place_latitude": "\(place.geoPoint.coordinates[1])",
            "place_longitude": "\(place.geoPoint.coordinates[0])",
            "place_id": place._id
            ] as [String: Any]
        
        if isFromGallery {
            eventParam["latitude"] = "\(locationData[0])"
            eventParam["longitude"] = "\(locationData[1])"
        } else {
            if let mLocation = pickedLocation {
                eventParam["timestamp"] = "\(mLocation.location.timestamp)"
                eventParam["latitude"] = "\(mLocation.location.coordinate.latitude)"
                eventParam["longitude"] = "\(mLocation.location.coordinate.longitude)"
                eventParam["accuracy_config"] = "\(mLocation.accuracy ?? 0)"
                eventParam["horizontal_accuracy"] = "\(mLocation.location.horizontalAccuracy)"
            }
        }
        
        if availableLocations.count > 0 {
            eventParam["available_locations"] = ArrayUtil.getLocationString(locations: availableLocations)
        }
        
        Analytics.logEvent("location_debug_info", parameters: eventParam)
        CleverTap.sharedInstance()?.recordEvent("location_debug_info", withProps: eventParam)
        if ApiConfig.appEnvironment == ENVIRONMENT.production {
            AppsFlyerTracker.shared().trackEvent("location_debug_info", withValues: eventParam)
        }
    }
    
    static func uploadsAnalyticsEvent(eventName: UploadActionsEventName) {
        let eventParam = [
            "user_id": Helper().getUserId()
            ] as [String: Any]
        
        Analytics.logEvent(eventName.rawValue, parameters: eventParam)
        CleverTap.sharedInstance()?.recordEvent(eventName.rawValue, withProps: eventParam)
        if ApiConfig.appEnvironment == ENVIRONMENT.production {
            AppsFlyerTracker.shared().trackEvent(eventName.rawValue, withValues: eventParam)
        }
    }
    
    static func claimRewardTapAnalyticsEvent(screen: String) {
        let eventParam = [
            "user_id": Helper().getUserId(),
            "screen": screen
            ] as [String: Any]
        
        Analytics.logEvent("claim_reward", parameters: eventParam)
        CleverTap.sharedInstance()?.recordEvent("claim_reward", withProps: eventParam)
        if ApiConfig.appEnvironment == ENVIRONMENT.production {
            AppsFlyerTracker.shared().trackEvent("claim_reward", withValues: eventParam)
        }
    }
    
    static func claimRewardEmailActionAnalyticEvent(screen: String, email: String, eventName: String) {
        let eventParam = [
            "user_id": Helper().getUserId(),
            "screen": screen,
            "email": email
            ] as [String: Any]
        
        Analytics.logEvent(eventName, parameters: eventParam)
        CleverTap.sharedInstance()?.recordEvent(eventName, withProps: eventParam)
        if ApiConfig.appEnvironment == ENVIRONMENT.production {
            AppsFlyerTracker.shared().trackEvent(eventName, withValues: eventParam)
        }
    }
    
    static func storyViewExplainerEvent(eventName: StoryViewExplainerEventName) {
        let eventParam = [
            "user_id": Helper().getUserId()
            ] as [String: Any]
        
        Analytics.logEvent(eventName.rawValue, parameters: eventParam)
        CleverTap.sharedInstance()?.recordEvent(eventName.rawValue, withProps: eventParam)
        if ApiConfig.appEnvironment == ENVIRONMENT.production {
            AppsFlyerTracker.shared().trackEvent(eventName.rawValue, withValues: eventParam)
        }
    }
    
    static func highlightShowcasePlaceTapAnalyticEvent(eventName: String, place: LivePlaces) {
        let eventParam = [
            "user_id": Helper().getUserId(),
            "place_id": place._id ?? "",
            "place_subcategory": place.subCategory.name ?? "",
            "place_area": SminqAPI.formattedAddressFromAddressComponentWithoutInitialDots(addressComponent: place.addressComponents),
            "place_city": place.addressComponents?.locality ?? "",
            "place_name": place.name ?? ""
            ] as [String: Any]
        
        Analytics.logEvent("\(eventName)_showcased_place", parameters: eventParam)
        CleverTap.sharedInstance()?.recordEvent("\(eventName)_showcased_place", withProps: eventParam)
        if ApiConfig.appEnvironment == ENVIRONMENT.production {
            AppsFlyerTracker.shared().trackEvent("\(eventName)_showcased_place", withValues: eventParam)
        }
    }
    
    static func highlightTrendingStickersTapAnalyticEvent(eventName: String, stickerName: String) {
        let eventParam = [
            "user_id": Helper().getUserId(),
            "sticker_name": stickerName
            ] as [String: Any]
        
        Analytics.logEvent("\(eventName)_sticker_places", parameters: eventParam)
        CleverTap.sharedInstance()?.recordEvent("\(eventName)_sticker_places", withProps: eventParam)
        if ApiConfig.appEnvironment == ENVIRONMENT.production {
            AppsFlyerTracker.shared().trackEvent("\(eventName)_sticker_places", withValues: eventParam)
        }
    }
    
    static func highlightTrendingAnalyticEvents(event: HighlightsTrendingAnalyticsEventName) {
        let eventParam = [
            "user_id": Helper().getUserId(),
            ] as [String: Any]
        
        Analytics.logEvent(event.rawValue, parameters: eventParam)
        CleverTap.sharedInstance()?.recordEvent(event.rawValue, withProps: eventParam)
        if ApiConfig.appEnvironment == ENVIRONMENT.production {
            AppsFlyerTracker.shared().trackEvent(event.rawValue, withValues: eventParam)
        }
    }
    
    static func addRatingEditActionsAnalyticEvent(event: AddRatingEditEvents) {
        let eventParam = [
            "user_id": Helper().getUserId(),
            ] as [String: Any]
        
        Analytics.logEvent(event.rawValue, parameters: eventParam)
        CleverTap.sharedInstance()?.recordEvent(event.rawValue, withProps: eventParam)
        if ApiConfig.appEnvironment == ENVIRONMENT.production {
            AppsFlyerTracker.shared().trackEvent(event.rawValue, withValues: eventParam)
        }
    }
    
    static func discardRatingAnalyticEvent(multiple: Bool) {
        let eventParam = [
            "user_id": Helper().getUserId(),
            "multiple": multiple ? "yes" : "no"
            ] as [String: Any]
        
        Analytics.logEvent("add_rating_discard", parameters: eventParam)
        CleverTap.sharedInstance()?.recordEvent("add_rating_discard", withProps: eventParam)
        if ApiConfig.appEnvironment == ENVIRONMENT.production {
            AppsFlyerTracker.shared().trackEvent("add_rating_discard", withValues: eventParam)
        }
    }
    
    static func dynamicLinkOpenedEvent(dynamicUrl: String, type: DynamicURLType, isFirstInstalled: Bool) {
        let eventParam = [
            "dynamic_url": dynamicUrl,
            "user_id": Helper().getUserId(),
            "url_type": type.rawValue,
            "first_installed": isFirstInstalled ? AppConstants.yes : AppConstants.no
            ] as [String: Any]
        
        Analytics.logEvent("dynamic_link_opened", parameters: eventParam)
        CleverTap.sharedInstance()?.recordEvent("dynamic_link_opened", withProps: eventParam)
        if ApiConfig.appEnvironment == ENVIRONMENT.production {
            AppsFlyerTracker.shared().trackEvent("dynamic_link_opened", withValues: eventParam)
        }
    }
    
    static func addRatingImageStickerAddedEvent(stickerName: String) {
        let eventParam = [
            "user_id": Helper().getUserId(),
            "sticker_selected": stickerName
            ] as [String: Any]
        
        Analytics.logEvent("add_rating_select_sticker", parameters: eventParam)
        CleverTap.sharedInstance()?.recordEvent("add_rating_select_sticker", withProps: eventParam)
        if ApiConfig.appEnvironment == ENVIRONMENT.production {
            AppsFlyerTracker.shared().trackEvent("add_rating_select_sticker", withValues: eventParam)
        }
    }
    
    static func addRatingTextStickerAddedEvent(stickerText: String) {
        let eventParam = [
            "user_id": Helper().getUserId(),
            "text": stickerText
            ] as [String: Any]
        
        Analytics.logEvent("add_rating_add_text", parameters: eventParam)
        CleverTap.sharedInstance()?.recordEvent("add_rating_add_text", withProps: eventParam)
        if ApiConfig.appEnvironment == ENVIRONMENT.production {
            AppsFlyerTracker.shared().trackEvent("add_rating_add_text", withValues: eventParam)
        }
    }

}
