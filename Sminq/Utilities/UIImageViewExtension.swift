//
//  UIImageViewExtension.swift
//  Sminq
//
//  Created by SMINQ iOS on 09/03/18.
//  Copyright © 2018 Suraj Sagare. All rights reserved.
//

import UIKit

extension UIImageView {
    /**
     Sets tint color to image
     
     - Parameters:
        - color: UIColor for tint
     */
    
    func tintImageColor(color: UIColor) {
        self.image = self.image!.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
        self.tintColor = color
    }
    
}
