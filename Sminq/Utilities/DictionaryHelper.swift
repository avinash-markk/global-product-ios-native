//
//  DictionaryHelper.swift
//  Sminq
//
//  Created by Pushkar Deshmukh on 03/01/19.
//  Copyright © 2019 Suraj Sagare. All rights reserved.
//

import Foundation

class DictionaryHelper {
    
    /**
     Constructs comma seperated key value pair string for given request payload
     
     - Parameters:
        - requestPayload: Dictionary for request payload
     
     - Returns: Returns string
     */
    
    static func constructKeyValueStringFromDictionary(requestPayload: [String: Any]) -> String {
        var tempString = ""
        
        if requestPayload.count > 0 {
            tempString = (requestPayload.compactMap( { (key, value) -> String in
                return "\(key)=\(value)"
            }) as Array).joined(separator: ",")
        }
        
        return tempString
    }
    
    /**
     Converts dictionary to JSON String
     
     - Parameters:
        - dictionary: Dictionary to be converted
     
     - Returns: Returns json string
     */
    
    static func getJSONStringFromDictionary(dictionary: [String: Any]) -> String {
        var tempString = ""
        
        if dictionary.count > 0 {
            do {
                let jsonData = try JSONSerialization.data(withJSONObject: dictionary, options: .prettyPrinted)
                let decoded = try JSONSerialization.jsonObject(with: jsonData, options: [])
                tempString = Helper().jsonToStringConversion(jsonObject: decoded)
            } catch {
                LogUtil.error(error)
            }
        }
        
        return tempString
    }
    
}
