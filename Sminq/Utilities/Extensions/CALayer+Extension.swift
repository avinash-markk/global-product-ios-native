//
//  CALayer+Extension.swift
//  Markk
//
//  Created by Pushkar Deshmukh on 31/05/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import UIKit

extension CALayer {
    /**
     Pauses the layer animation
     */
    
    func pause() {
        let pausedTime: CFTimeInterval = self.convertTime(CACurrentMediaTime(), from: nil)
        self.speed = 0.0
        self.timeOffset = pausedTime
    }
    
    /**
     Resumes the layer animation
     */
    
    func resume() {
        let pausedTime: CFTimeInterval = self.timeOffset
        self.speed = 1.0
        self.timeOffset = 0.0
        self.beginTime = 0.0
        let timeSincePause: CFTimeInterval = self.convertTime(CACurrentMediaTime(), from: nil) - pausedTime
        self.beginTime = timeSincePause
    }
}
