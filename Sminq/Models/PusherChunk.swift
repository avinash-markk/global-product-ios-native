//
//  PusherChunk.swift
//  Markk
//
//  Created by Pushkar Deshmukh on 01/08/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import Foundation
import Gloss

struct PusherChunk {
    var messageId: Int
    var offset: Int
    var chunk: String
    var totalChunks: Int
    var size: Int
    var eof: Bool
    
    init?(data: Any?) {
        guard let data = data else { return nil }
        guard let dataDictionary = data as? [String: Any] else { return nil }
        
        self.messageId = dataDictionary["messageId"] as? Int ?? 0
        self.offset = dataDictionary["offset"] as? Int ?? 0
        self.chunk = dataDictionary["chunk"] as? String ?? ""
        self.totalChunks = dataDictionary["totalChunks"] as? Int ?? 0
        self.size = dataDictionary["size"] as? Int ?? 0
        self.eof = dataDictionary["eof"] as? Bool ?? false
    }
}
