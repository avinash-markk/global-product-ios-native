//
//  LocationModel.swift
//  Markk
//
//  Created by Pushkar Deshmukh on 18/07/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import CoreLocation

struct Location {
    let location: CLLocation
    let accuracy: CLLocationAccuracy?
    var distanceFromPlace: Double?
    
    init(location: CLLocation, accuracy: CLLocationAccuracy?) {
        self.location = location
        self.accuracy = accuracy
    }
    
    func toString() -> String {
        return "\(self.location.coordinate.latitude):\(self.location.coordinate.longitude),accuracy=\(self.accuracy ?? 0),horizontalAccuracy=\(location.horizontalAccuracy)"
    }
}
