//
//  StickersData.swift
//  Sminq
//
//  Created by Bjorn Mascarenhas on 25/09/18.
//  Copyright © 2018 Suraj Sagare. All rights reserved.
//

import Gloss

public struct Stickers: Glossy {
    
    public var category: String
    public var subCategory: String?
    public var name: String
    public var searchTags: String
    public var key: String
    public var weight: Int
    public var image: StickerImage?
    public var id: String
    public var weightGroup: [Int]?

    public init?(json: JSON) {
        if let category: String = "category" <~~ json {
            self.category = category
        } else {
            self.category = ""
        }
        
        if let subCategory: String = "subCategory" <~~ json {
            self.subCategory = subCategory
        } else {
            self.subCategory = ""
        }
        
        if let name: String = "name" <~~ json {
            self.name = name
        } else {
            self.name = ""
        }
        
        if let searchTags: String = "searchTags" <~~ json {
            self.searchTags = searchTags
        } else {
            self.searchTags = ""
        }
        
        if let key: String = "key" <~~ json {
            self.key = key
        } else {
            self.key = ""
        }
        
        self.weight = "weight" <~~ json ?? 2
        
        if let image: StickerImage = "image" <~~ json {
            self.image = image
        } else {
            self.image = nil
        }
        
        if let _id: String = "_id" <~~ json {
            self.id = _id
        } else {
            self.id = ""
        }
        
        self.weightGroup = "weightGroup" <~~ json ?? []
    }
    
    public func toJSON() -> JSON? {
        return jsonify([
            "category" ~~> self.category,
            "subCategory" ~~> self.subCategory,
            "name" ~~> self.name,
            "searchTags" ~~> self.searchTags,
            "key" ~~> self.key,
            "weight" ~~> self.weight,
            "image" ~~> self.image,
            "_id" ~~> self.id,
            "weightGroup" ~~> self.weightGroup
            ])
    }
    
}

public struct StickerImage: Glossy {
    
    public var path: String
    public var size: Double
    public var bucket: String
    public var url: String
    
    public init?(json: JSON) {
        if let path: String = "path" <~~ json {
            self.path = path
        } else {
            self.path = ""
        }
        
        if let size: Double = "size" <~~ json {
            self.size = size
        } else {
            self.size = 0
        }
        
        if let bucket: String = "bucket" <~~ json {
            self.bucket = bucket
        } else {
            self.bucket = ""
        }
        
        if let url: String = "url" <~~ json {
            self.url = url
        } else {
            self.url = ""
        }
    }
    
    public func toJSON() -> JSON? {
        return jsonify([
            "path" ~~> self.path,
            "size" ~~> self.size,
            "bucket" ~~> self.bucket,
            "url" ~~> self.url
            ])
    }
    
}
