//
//  PlaceFeedData.swift
//  Sminq
//
//  Created by Suraj on 3/19/18.
//  Copyright © 2018 Suraj Sagare. All rights reserved.
//

import Foundation
import Gloss

// UserDataPlaceFeed
public struct UserDataPlaceFeed: Codable {
    var subPlace: String!
    var user: UserDataDict!
    var placeDetails: LivePlaces!
    var locationVerified: LocationVerificationState? // Internal/custom types can not be made public
    var verified: Bool?
    var archived: Bool?
    var expired: Bool = false
    var createdAt: Int?
    var moodScore: Int
    var upVotesCount: Int?
    var downVotesCount: Int?
    var sharesCount: Int?
    var commentsCount: Int?
    var viewCount: Int?
    var hasUserUpVoted: Bool
    var hasUserDownVoted: Bool
    var moderationRequired: Bool
    var displayName: String!
    var comments: Comments?
    var contributions: [Contributions]!
    var notificationStatus: Bool!
    var userDistance: Int?
    var _id: String!
    var eventType: String!
    var isPostDeletedLocalFlag: Bool = false
    var isPostReportedLocalFlag: Bool = false
}

extension UserDataPlaceFeed: Glossy {
    
    public init?(json: JSON) {
        if let subPlace: String = "subPlace" <~~ json {
            self.subPlace = subPlace
        } else {
            self.subPlace = ""
        }
        
        self.user = "user" <~~ json
        self.placeDetails = "placeDetails" <~~ json
        
        if let locationVerified: LocationVerificationState = "locationVerified" <~~ json {
            self.locationVerified = locationVerified
        }
        
        if let verified: Bool = "verified" <~~ json {
            self.verified = verified
        } else {
            self.verified = false
        }
        
        if let archived: Int = "archived" <~~ json {
            self.archived = archived == 1 ? true : false
        } else {
            self.archived = false
        }
        
        self.expired = "expired" <~~ json ?? false
        
        if let createdAt: Int = "createdAt" <~~ json {
            self.createdAt = createdAt
        } else {
            self.createdAt = 0
        }
        
        self.moodScore = "moodScore" <~~ json ?? AppConstants.defaultMoodScore
        
        if let upVotesCount: Int = "upVotesCount" <~~ json {
            self.upVotesCount = upVotesCount
        } else {
            self.upVotesCount = 0
        }
        
        if let downVotesCount: Int = "downVotesCount" <~~ json {
            self.downVotesCount = downVotesCount
        } else {
            self.downVotesCount = 0
        }
        
        if let sharesCount: Int = "sharesCount" <~~ json {
            self.sharesCount = sharesCount
        } else {
            self.sharesCount = 0
        }
        
        if let commentsCount: Int = "commentsCount" <~~ json {
            self.commentsCount = commentsCount
        } else {
            self.commentsCount = 0
        }
        
        if let viewCount: Int = "viewCount" <~~ json {
            self.viewCount = viewCount
        } else {
            self.viewCount = 0
        }
        
        self.user = "user" <~~ json
        
//        self.isPostDeletedLocalFlag = "isPostDeletedLocalFlag" <~~ json ?? false
//        self.isPostReportedLocalFlag = "isPostReportedLocalFlag" <~~ json ?? false
        
        if let hasUserUpVoted: Int = "hasUserUpVoted" <~~ json {
            self.hasUserUpVoted = hasUserUpVoted == 1 ? true : false
        } else {
            self.hasUserUpVoted = false
        }
        
        if let hasUserDownVoted: Int = "hasUserDownVoted" <~~ json {
            self.hasUserDownVoted = hasUserDownVoted == 1 ? true : false
        } else {
            self.hasUserDownVoted = false
        }
        
        if let moderationRequired: Int = "moderationRequired" <~~ json {
            self.moderationRequired = moderationRequired == 1 ? true : false
        } else {
            self.moderationRequired = false
        }
        
        if let displayName: String = "displayName" <~~ json {
            self.displayName = displayName
        } else {
            self.displayName = ""
        }
    
        self.comments = "comments" <~~ json
        
        self.contributions = "contributions" <~~ json
        
        if let notificationStatus: Bool = "notificationStatus" <~~ json {
            self.notificationStatus = notificationStatus
        } else {
            self.notificationStatus = false
        }
        
        if let userDistance: Int = "userDistance" <~~ json {
            self.userDistance = userDistance
        } else {
            self.userDistance = 0
        }
        
        if let _id: String = "_id" <~~ json {
            self._id = _id
        } else {
            self._id = ""
        }
        
        if let eventType: String = "eventType" <~~ json {
            self.eventType = eventType
        } else {
            self.eventType = ""
        }
    }
    
    public func toJSON() -> JSON? {
        return jsonify([
            "subPlace" ~~> self.subPlace,
            "user" ~~> self.user,
            "placeDetails" ~~> self.placeDetails,
            "locationVerified" ~~> self.locationVerified,
            "verified" ~~> self.verified,
            "archived" ~~> self.archived,
            "expired" ~~> self.expired,
            "createdAt" ~~> self.createdAt,
            "moodScore" ~~> self.moodScore,
            "upVotesCount" ~~> self.upVotesCount,
            "downVotesCount" ~~> self.downVotesCount,
            "sharesCount" ~~> self.sharesCount,
            "commentsCount" ~~> self.commentsCount,
            "viewCount" ~~> self.viewCount,
            "hasUserUpVoted" ~~> self.hasUserUpVoted,
            "hasUserDownVoted" ~~> self.hasUserDownVoted,
            "moderationRequired" ~~> self.moderationRequired,
            "displayName" ~~> self.displayName,
            "comments" ~~> self.comments,
            "contributions" ~~> self.contributions,
            "notificationStatus" ~~> self.notificationStatus,
            "userDistance" ~~> self.userDistance,
            "_id" ~~> self._id,
            "eventType" ~~> self.eventType
            ])
    }
    
    func hasCounts() -> Bool {
        var countsFlag = false
        if let count = self.upVotesCount, count > 0 {
            countsFlag = true
        }
        
        if let count = self.commentsCount, count > 0 {
            countsFlag = true
        }
        
        if let count = self.sharesCount, count > 0 {
            countsFlag = true
        }
        
        return countsFlag
    }
    
    func getOtherContext() -> Contributions? {
        var otherContext: Contributions?
        
        for contribution in self.contributions {
            if contribution.subCategoryQuestion.context == "other" {
                otherContext = contribution
            }
        }
        
        return otherContext
    }
    
    func getImageURL() -> URL? {
        guard let otherContext = self.getOtherContext() else { return nil }
        
        if let url =  MediaCacheManagerService.shared.getFeedMediaDocumentURL(feed: self) {
            print("Image is already cached!")
            return url
        } else if let imageName = otherContext.image, let url = CloudinaryService.shared.generateCloudinaryImageURL(folder: FIRRemoteConfigService.shared.getCloudinaryOriginalImageFolder(), fileName: imageName) {
            print("Image is not cached!")
            return url
        }
        
        return nil
    }
    
    func getVideoURL() -> URL? {
        guard let otherContext = self.getOtherContext() else { return nil }
        
        if let url =  MediaCacheManagerService.shared.getFeedMediaDocumentURL(feed: self) {
            print("Video is already cached!")
            return url
        } else if let videoName = otherContext.video, let url = CloudinaryService.shared.generateCloudinaryVideoURL(folder: FIRRemoteConfigService.shared.getCloudinaryVideoFolder(), fileName: videoName) {
            print("Video is not cached!")
            return url
        }
        
        return nil
    }
    
    func getFeedMediaURL() -> URL? {
        guard let otherContext = self.getOtherContext() else { return nil }
        
        var absoluteURL: URL? = nil
        
        if otherContext.video != "" {
            absoluteURL = getVideoURL()
        } else {
            absoluteURL = getImageURL()
        }
        
        guard let url = absoluteURL else { return nil }
        return url
    }
    
    func getUpvoteCountLabel() -> String {
        if let count = self.upVotesCount {
            if count == 1 {
                return "• \(count) Like "
            } else {
                return "• \(count) Likes "
            }
        } else {
            return ""
        }
    }
    
    func getDownvoteCountLabel() -> String {
        if let count = self.downVotesCount {
            if count == 1 {
                return "• \(count) Downvote "
            } else {
                return "• \(count) Downvotes "
            }
        } else {
            return ""
        }
    }
    
    func getCommentsCountLabel() -> String {
        guard let comments = self.comments else { return "" }
        
        if comments.users.count == 1 {
            return "• \(comments.users.count) Comment "
        } else {
            return "• \(comments.users.count) Comments "
        }
    }
    
    func getViewsCountLabel() -> String {
        if let viewCount = viewCount {
            if viewCount == 1 {
                return "\(viewCount) view"
            } else {
                return "\(viewCount) views"
            }
        } else {
            return ""
        }
    }
    
    func getFeedMediaType() -> MediaType? {
        guard let otherContext = self.getOtherContext() else { return nil }
        
        return otherContext.video != "" ? .video : .image
        
    }
        
    func getLocationVerificationImageIcon() -> UIImage? {
        var image = UIImage(named: "iconMarkerUnverified24WhitePure")
        
        if let locationVerificationState = locationVerified {
            switch locationVerificationState {
            case .success:
                image = UIImage(named: "iconMarkerVerified24WhitePure")
            case .pending:
                image = UIImage(named: "iconMarkerPending24WhitePure")
            case .failed:
                image = UIImage(named: "iconMarkerUnverified24WhitePure")
            }
        } else {
            if let verified = verified, verified {
                image = UIImage(named: "iconMarkerVerified24WhitePure")
            } else {
                image = UIImage(named: "iconMarkerUnverified24WhitePure")
            }
        }
        
        return image
    }
    
    func getLatestReplies(count: Int) -> [Replies] {
        var tempReplies: [Replies] = []
        
        if let comments = comments, comments.users.count > count {
            let tempReversedReplies = comments.users.reversed()
            
            for (index, reply) in tempReversedReplies.enumerated() {
                if index < count {
                    tempReplies.append(reply)
                }
            }
            tempReplies = tempReplies.reversed()
        }
        
        return tempReplies
    }
}

struct Replies: Codable {
     var userId: String!
     var displayName: String
     var userImage: String!
     var createdAt: Int?
     var input: String!
     var _id: String!
     var mediaMetadata: MediaMetadata?
}

extension Replies: Glossy {
    
    public init?(json: JSON) {
        if let userId: String = "userId" <~~ json {
            self.userId = userId
        } else {
            self.userId = ""
        }
        
        if let displayName: String = "displayName" <~~ json {
            self.displayName = displayName
        } else {
            self.displayName = ""
        }
        
        if let userImage: String = "userImage" <~~ json {
            self.userImage = userImage
        } else {
            self.userImage = ""
        }
        
        if let createdAt: Int = "createdAt" <~~ json {
            self.createdAt = createdAt
        } else {
            self.createdAt = 0
        }
        
        if let input: String = "input" <~~ json {
            self.input = input
        } else {
            self.input = ""
        }
        
        if let _id: String = "_id" <~~ json {
            self._id = _id
        } else {
            self._id = ""
        }
        
        self.mediaMetadata = "mediaMetadata" <~~ json
    }
    
    public func toJSON() -> JSON? {
        return jsonify([
            "userId" ~~> self.userId,
            "displayName" ~~> self.displayName,
            "userImage" ~~> self.userImage,
            "createdAt" ~~> self.createdAt,
            "input" ~~> self.input,
            "_id" ~~> self._id,
            "mediaMetadata" ~~> self.mediaMetadata
            ])
    }
    
}

struct Comments: Codable {
    var users: [Replies]
    var nextPageToken: String
}

extension Comments: Glossy {
    
    public init?(json: JSON) {
        self.users = "users" <~~ json ?? []
        
        self.nextPageToken = "nextPageToken" <~~ json ?? ""
    }
    
    public func toJSON() -> JSON? {
        return jsonify([
            "users" ~~> self.users,
            "nextPageToken" ~~> self.nextPageToken
            ])
    }
    
}

struct MediaMetadata: Codable {
    var textList: [TextList]?
    var stickerList: [StickerList]?
}

extension MediaMetadata: Glossy {

    public init?(json: JSON) {
        self.textList = "textList" <~~ json
        self.stickerList = "stickerList" <~~ json
    }
    
    public func toJSON() -> JSON? {
        return jsonify([
            "textList" ~~> self.textList,
            "stickerList" ~~> self.stickerList
            ])
    }
    
}

struct TextList: Codable {
    public var text: String?
    public var tags: [Tags]?
    public var fontSize: Int?
    public var position: TextPosition?
    public var uniqueId: Int? // Unique identifier
}

extension TextList: Glossy {

    public init?(json: JSON) {
        if let text: String = "text" <~~ json {
            self.text = text
        } else {
            self.text = ""
        }
        
        self.tags = "tags" <~~ json
        self.position = "position" <~~ json
        
        if let fontSize: Int = "fontSize" <~~ json {
            self.fontSize = fontSize
        } else {
            self.fontSize = 0
        }
        
        if let uniqueId: Int = "uniqueId" <~~ json {
            self.uniqueId = uniqueId
        } else {
            self.uniqueId = 0
        }
    }
    
    public func toJSON() -> JSON? {
        return jsonify([
            "text" ~~> self.text,
            "tags" ~~> self.tags,
            "fontSize" ~~> self.fontSize,
            "position" ~~> self.position,
            "uniqueId" ~~> self.uniqueId
            ])
    }
    
}

struct StickerList: Codable {
    public var id: String?
    public var sticker: String?
    public var weight: Double
}

extension StickerList: Glossy {
    public init?(json: JSON) {
         self.id = "id" <~~ json ?? ""
         self.sticker = "sticker" <~~ json ?? ""
         self.weight = "weight" <~~ json ?? 0.0
    }
        
        public func toJSON() -> JSON? {
            return jsonify([
                "id" ~~> self.id,
                "sticker" ~~> self.sticker,
                "weight" ~~> self.weight
                ])
        }
}

struct TextPosition: Codable {
     var topLeft: [CGFloat]?
     var height: CGFloat?
     var width: CGFloat?
     var rotation: CGFloat?
}

extension TextPosition: Glossy {
    
    public init?(json: JSON) {
        if let topLeft: [CGFloat] = "topLeft" <~~ json {
            self.topLeft = topLeft
        } else {
            self.topLeft = []
        }
        
        if let height: CGFloat = "height" <~~ json {
            self.height = height
        } else {
            self.height = 0.0
        }
        
        if let width: CGFloat = "width" <~~ json {
            self.width = width
        } else {
            self.width = 0.0
        }
        
        if let rotation: CGFloat = "rotation" <~~ json {
            self.rotation = rotation
        } else {
            self.rotation = 0.0
        }
    }
    
    public func toJSON() -> JSON? {
        return jsonify([
            "topLeft" ~~> self.topLeft,
            "height" ~~> self.height,
            "width" ~~> self.width,
            "rotation" ~~> self.rotation
            ])
    }
    
}

struct Tags: Codable {
     var value: String?
     var name: String?
     var network: String?
     var baseUrl: String?
     var imageUrl: String?
     var userId: String?
     var displayName: String?
}

extension Tags: Glossy {
    
    public init?(json: JSON) {
        if let value: String = "value" <~~ json {
            self.value = value
        } else {
            self.value = ""
        }
        
        if let name: String = "name" <~~ json {
            self.name = name
        } else {
            self.name = ""
        }
        
        if let network: String = "network" <~~ json {
            self.network = network
        } else {
            self.network = ""
        }
        
        if let baseUrl: String = "baseUrl" <~~ json {
            self.baseUrl = baseUrl
        } else {
            self.baseUrl = ""
        }
        
        if let imageUrl: String = "imageUrl" <~~ json {
            self.imageUrl = imageUrl
        } else {
            self.imageUrl = ""
        }
        
        if let userId: String = "userId" <~~ json {
            self.userId = userId
        } else {
            self.userId = nil // API doesn't take "" <Empty space> in payload for reply. Which is why nil is used
        }
        
        if let displayName: String = "displayName" <~~ json {
            self.displayName = displayName
        } else {
            self.displayName = nil // API doesn't take "" <Empty space> in payload for reply. Which is why nil is used
        }
    }
    
    public func toJSON() -> JSON? {
        return jsonify([
            "value" ~~> self.value,
            "name" ~~> self.name,
            "network" ~~> self.network,
            "baseUrl" ~~> self.baseUrl,
            "imageUrl" ~~> self.imageUrl,
            "userId" ~~> self.userId,
            "displayName" ~~> self.displayName
            ])
    }
    
}

struct Roles: Codable {
     var level: Int?
     var status: String!
     var type: String!
     var _id: String!
}

extension Roles: Glossy {
   
    public init?(json: JSON) {
        if let level: Int = "level" <~~ json {
            self.level = level
        } else {
            self.level = 0
        }
        
        if let status: String = "status" <~~ json {
            self.status = status
        } else {
            self.status = ""
        }
        
        if let type: String = "type" <~~ json {
            self.type = type
        } else {
            self.type = ""
        }
        
        if let _id: String = "_id" <~~ json {
            self._id = _id
        } else {
            self._id = ""
        }

    }
    
    public func toJSON() -> JSON? {
        return jsonify([
            "level" ~~> self.level,
            "status" ~~> self.status,
            "type" ~~> self.type,
            "_id" ~~> self._id
            ])
    }
    
}

