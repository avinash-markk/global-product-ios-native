//
//  PlaceStoriesData.swift
//  Sminq
//
//  Created by Pushkar Deshmukh on 01/08/18.
//  Copyright © 2018 Suraj Sagare. All rights reserved.
//

import Foundation
import Gloss
import UIKit


public struct ArchiveRatingsData: Codable {
    public var archiveDate: String
    public var archivePlacesRatings: [ArchivePlacesRatings]?
}

extension ArchiveRatingsData: Glossy {
    
    public init?(json: JSON) {
        self.archiveDate = "date" <~~ json ?? ""
        self.archivePlacesRatings = "placeStories" <~~ json ?? []
    }
    
    public func toJSON() -> JSON? {
        return jsonify([
            "date" ~~> self.archiveDate,
            "placeStories" ~~> self.archivePlacesRatings
            ])
    }
    
}

public struct ArchivePlacesRatings: Codable {
    public var archivePlace: LivePlaces!
     var archiveRatings: [UserStory]?
}
        
extension ArchivePlacesRatings: Glossy {
    
    public init?(json: JSON) {
        self.archivePlace = "placeDetails" <~~ json
        self.archiveRatings = "stories" <~~ json ?? []
    }
    
    public func toJSON() -> JSON? {
        return jsonify([
            "placeDetails" ~~> self.archivePlace,
            "stories" ~~> self.archiveRatings
            ])
    }
}

public struct PlacesStoriesData: Codable {
    public var placeDetails: LivePlaces!
    public var stories: [UserStory]?
    public var archiveStories: [UserStory]?
}

extension PlacesStoriesData: Glossy {
    public init?(json: JSON) {
        self.placeDetails = "placeDetails" <~~ json
        self.stories = "stories" <~~ json ?? []
        self.archiveStories = "archiveStories" <~~ json
    }
    
    public func toJSON() -> JSON? {
        return jsonify([
            "placeDetails" ~~> self.placeDetails,
            "stories" ~~> self.stories,
            "archiveStories" ~~> self.archiveStories
            ])
    }
    
    func copyPlaceDetailsToPostsAndReturnStories(stories: [UserStory]) -> [UserStory] {
        var storiesList = stories
        var tempStories = [UserStory]()
        
        if storiesList.count > 0, let place = self.placeDetails {
            for storyIndex in 0..<storiesList.count {
                var tempPostArray = [UserDataPlaceFeed]()
                for postIndex in 0..<storiesList[storyIndex].posts.count {
                    storiesList[storyIndex].posts[postIndex].placeDetails = place
                    tempPostArray.append(storiesList[storyIndex].posts[postIndex])
                }
                storiesList[storyIndex].posts = tempPostArray
    
                tempStories.append(storiesList[storyIndex])
            }
        }

        return tempStories
    }
    
    func convertToStoryDataFromPlaceStoriesData(filterByPostId: String?) -> StoryData? {
        guard let stories = self.stories else { return nil }
        var allPostsInsideStory: [UserDataPlaceFeed] = []
        
        stories.forEach { (userStory) in
            userStory.posts.forEach({ (post) in
                if let postId = filterByPostId {
                    if postId == post._id {
                        allPostsInsideStory.append(post)
                    }
                } else {
                    allPostsInsideStory.append(post)
                }
                
            })
        }
        
        let feedViewModel = FeedViewModel.init(screen: "")
        feedViewModel?.setFeedList(feedList: allPostsInsideStory)
        
        return StoryData.init(userId: nil, placeId: self.placeDetails._id, placeDetails: self.placeDetails, archived: false, posts: allPostsInsideStory, feedViewModel: feedViewModel)
    }
}

public struct UserStory: Codable {
    public var totalPosts: Int?
    public var placeMetrics: PlaceMetrics?
    public var posts: [UserDataPlaceFeed]
}

extension UserStory: Glossy {
    public init?(json: JSON) {
        if let totalPosts: Int = "totalPosts" <~~ json {
            self.totalPosts = totalPosts
        } else {
            self.totalPosts = 0
        }
        
        self.placeMetrics = "placeMetrics" <~~ json
        self.posts = "posts" <~~ json ?? []
    }
    
    public func toJSON() -> JSON? {
        return jsonify([
            "totalPosts" ~~> self.totalPosts,
            "placeMetrics" ~~> self.placeMetrics,
            "posts" ~~> self.posts
            ])
    }
    
}

struct UserProfileData: Codable {
    var user: UserDataDict!
    var userMetrics: UserMetrics?
    var stories: [StoryData]
    var levelsList: [LevelsList]
    var streaks: Streaks?
    var highlight: AchievementHighlight?
    var claims: Claims?
}

extension UserProfileData: Glossy {

    public init?(json: JSON) {
        self.user = "user" <~~ json
        self.userMetrics = "userMetrics" <~~ json
        self.stories = "stories" <~~ json ?? []
        self.levelsList = "levels" <~~ json ?? []
        self.streaks = "streaks" <~~ json
        self.highlight = "highlight" <~~ json
        self.claims = "claims" <~~ json
    }
    
    public func toJSON() -> JSON? {
        return jsonify([
            "user" ~~> self.user,
            "userMetrics" ~~> self.userMetrics,
            "stories" ~~> self.stories,
            "levels" ~~> self.levelsList,
            "streaks" ~~> self.streaks,
            "highlight" ~~> self.highlight,
            "claims" ~~> self.claims
            ])
    }
    
}

struct UserMetrics: Codable {
    var lifeTimeMetrics: LifeTimeMetrics?
}

extension UserMetrics: Glossy {
    
    public init?(json: JSON) {
        self.lifeTimeMetrics = "lifeTime" <~~ json
    }
    
    public func toJSON() -> JSON? {
        return jsonify([
            "lifeTime" ~~> self.lifeTimeMetrics,
            ])
    }
    
}

struct LifeTimeMetrics: Codable {
    var totalPosts: Int?
    var totalPlaces: Int?
    var totalDiscovered: Int?
}

extension LifeTimeMetrics: Glossy {
    
    public init?(json: JSON) {
         self.totalPosts = "totalPosts" <~~ json ?? 0
         self.totalPlaces = "totalPlaces" <~~ json ?? 0
         self.totalDiscovered = "totalDiscovered" <~~ json ?? 0
    }
    
    public func toJSON() -> JSON? {
        return jsonify([
            "totalPosts" ~~> self.totalPosts,
            "totalPlaces" ~~> self.totalPlaces,
            "totalDiscovered" ~~> self.totalDiscovered,
            ])
    }
    
}
