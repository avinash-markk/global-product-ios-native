//
//  UsersData.swift
//  Sminq
//
//  Created by Bjorn Mascarenhas on 27/02/18.
//  Copyright © 2018 Suraj Sagare. All rights reserved.
//

import Gloss

public struct SignUpData: Glossy {
    public var success: Bool
    public var httpCode: Int?
    var userDataDict: UserDataDict!
    
    public init?(json: JSON) {
        if let success: Int = "success" <~~ json {
            self.success = success == 1 ? true : false
        } else {
            self.success = false
        }
        
        self.httpCode = "httpCode" <~~ json
        self.userDataDict = "status" <~~ json
        
    }
    
    public func toJSON() -> JSON? {
        return jsonify([
            "success" ~~> self.success,
            "httpCode" ~~> self.httpCode,
            "status" ~~> self.userDataDict
            ])
    }
    
}

// SocialProfile
struct SocialProfile: Codable {
    var network: String!
    var identity: String!
    var accessToken: String!
}

extension SocialProfile: Glossy {
    
    public init?(json: JSON) {
        if let network: String = "network" <~~ json {
            self.network = network
        } else {
            self.network = ""
        }
        
        if let identity: String = "identity" <~~ json {
            self.identity = identity
        } else {
            self.identity = ""
        }
        
        if let accessToken: String = "accessToken" <~~ json {
            self.accessToken = accessToken
        } else {
            self.accessToken = ""
        }
        
    }
    
    public func toJSON() -> JSON? {
        return jsonify([
            "network" ~~> self.network,
            "identity" ~~> self.identity,
            "accessToken" ~~> self.accessToken
            ])
    }
    
}
