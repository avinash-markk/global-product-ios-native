//
//  GooglePlacesData.swift
//  Markk
//
//  Created by Pushkar Deshmukh on 27/06/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import Foundation
import Gloss


struct GoogleNearbyPlace: Codable {
    var id: String
    var name: String
    var place_id: String
    var vicinity: String
    var types: [String]
}

extension GoogleNearbyPlace: Glossy {
    public init?(json: JSON) {
        self.id = "id" <~~ json ?? ""
        self.name = "name" <~~ json ?? ""
        self.place_id = "place_id" <~~ json ?? ""
        self.vicinity = "vicinity" <~~ json ?? ""
        self.types = "types" <~~ json ?? []
    }
    
    public func toJSON() -> JSON? {
        return jsonify([
            "_id" ~~> self.id,
            "name" ~~> self.name,
            "place_id" ~~> self.place_id,
            "vicinity" ~~> self.vicinity,
            "types" ~~> self.types
            ])
    }
}

struct GoogleNearbyPlacesResponse: Codable {
    var results: [GoogleNearbyPlace]
    var next_page_token: String
}

extension GoogleNearbyPlacesResponse: Glossy {
    public init?(json: JSON) {
        self.next_page_token = "next_page_token" <~~ json ?? ""
        self.results = "results" <~~ json ?? []
    }
    
    public func toJSON() -> JSON? {
        return jsonify([
            "next_page_token" ~~> self.next_page_token,
            "results" ~~> self.results
            ])
    }
}

struct GoogleSearchPlace: Codable {
    var id: String
    var description: String
    var place_id: String
    var structured_formatting: StructuredFormatting?
    var types: [String]
}

extension GoogleSearchPlace: Glossy {
    public init?(json: JSON) {
        self.id = "id" <~~ json ?? ""
        self.description = "description" <~~ json ?? ""
        self.place_id = "place_id" <~~ json ?? ""
        self.structured_formatting = "structured_formatting" <~~ json
        self.types = "types" <~~ json ?? []
        
    }
    
    public func toJSON() -> JSON? {
        return jsonify([
            "_id" ~~> self.id,
            "description" ~~> self.description,
            "place_id" ~~> self.place_id,
            "structured_formatting" ~~> self.structured_formatting,
            "types" ~~> self.types
            ])
    }
}

struct StructuredFormatting: Codable {
    var main_text: String
}

extension StructuredFormatting: Glossy {
    public init?(json: JSON) {
        self.main_text = "main_text" <~~ json ?? ""
    }
    
    public func toJSON() -> JSON? {
        return jsonify([
            "main_text" ~~> self.main_text,
            ])
    }
}


struct GoogleAutocompleteResponse: Codable {
    var predictions: [GoogleSearchPlace]
}

extension GoogleAutocompleteResponse: Glossy {
    public init?(json: JSON) {
        self.predictions = "predictions" <~~ json ?? []
    }
    
    public func toJSON() -> JSON? {
        return jsonify([
            "predictions" ~~> self.predictions
            ])
    }
}
