//
//  LevelBadgesData.swift
//  Markk
//
//  Created by Avinash Thakur on 19/06/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import Gloss

struct LevelsList: Codable {
    var id: String!
    var levels: [Levels]
    var showProgress: Bool
    var threshold: Int
}

extension LevelsList: Glossy {
    
    public init?(json: JSON) {
        if let id: String = "_id" <~~ json {
            self.id = id
        } else {
            self.id = ""
        }
        if let showProgress: Int = "showProgress" <~~ json {
            self.showProgress = showProgress == 1 ? true : false
        } else {
            self.showProgress = false
        }
        if let threshold: Int = "threshold" <~~ json {
            self.threshold = threshold
        } else {
            self.threshold = 0
        }
         self.levels = "levels" <~~ json ?? []
    }
    
    public func toJSON() -> JSON? {
        return jsonify([
            "_id" ~~> self.id,
            "levels" ~~> self.levels,
            "threshold" ~~> self.threshold,
            "showProgress" ~~> self.showProgress
            ])
    }
    
}

struct Levels: Codable {
    var level: Int
    var rule: Int
    var levelName: String
    var completed: Bool
    var isRedeemable: Bool
    var badge: Badge?
}

extension Levels: Glossy {
    
    public init?(json: JSON) {
        if let level: Int = "level" <~~ json {
            self.level = level
        } else {
            self.level = 0
        }
        
        if let rule: Int = "rule" <~~ json {
            self.rule = rule
        } else {
            self.rule = 0
        }
        
        if let completed: Int = "completed" <~~ json {
            self.completed = completed == 1 ? true : false
        } else {
            self.completed = false
        }
        
        if let isRedeemable: Int = "isRedeemable" <~~ json {
            self.isRedeemable = isRedeemable == 1 ? true : false
        } else {
            self.isRedeemable = false
        }
        
        self.badge = "badge" <~~ json ?? nil
        
        if let levelName: String = "levelName" <~~ json {
            self.levelName = levelName
        } else {
            self.levelName = ""
        }
        
    }
    
    public func toJSON() -> JSON? {
        return jsonify([
            "level" ~~> self.level,
            "rule" ~~> self.rule,
            "completed" ~~> self.completed,
            "badge" ~~> self.badge,
            "levelName" ~~> self.levelName,
            "isRedeemable" ~~> self.isRedeemable
            ])
    }
    
}

struct Badge: Codable {
    var id: String!
    var description: String
    var activeImage: String?
    var inActiveImage: String?
    var homeIconActive: String?
    var homeIconInactive: String?
    var name: String
}

extension Badge: Glossy {
    
    public init?(json: JSON) {
        
        if let id: String = "_id" <~~ json {
            self.id = id
        } else {
            self.id = ""
        }
        
        if let description: String = "description" <~~ json {
            self.description = description
        } else {
            self.description = ""
        }
        
        if let name: String = "name" <~~ json {
            self.name = name
        } else {
            self.name = ""
        }
        
        if let activeImage: String = "activeImage" <~~ json {
            self.activeImage = activeImage
        } else {
            self.activeImage = nil
        }
        
        if let inActiveImage: String = "inActiveImage" <~~ json {
            self.inActiveImage = inActiveImage
        } else {
            self.inActiveImage = nil
        }
        
        if let homeIconActive: String = "homeIconActive" <~~ json {
            self.homeIconActive = homeIconActive
        } else {
            self.homeIconActive = nil
        }
        
        if let homeIconInactive: String = "homeIconInactive" <~~ json {
            self.homeIconInactive = homeIconInactive
        } else {
            self.homeIconInactive = nil
        }
        
    }
    
    public func toJSON() -> JSON? {
        return jsonify([
            "_id" ~~> self.id,
            "description" ~~> self.description,
            "activeImage" ~~> self.activeImage,
            "inActiveImage" ~~> self.inActiveImage,
            "homeIconInactive" ~~> self.homeIconInactive,
            "homeIconActive" ~~> self.homeIconActive,
            "name" ~~> self.name
            ])
    }
    
}

struct Streaks: Codable {
    var currentProgress: Int
    var currentStreakCount: Int
    var longestStreakCount: Int
    var longestStreakWeek: Int
}

extension Streaks: Glossy {
    
    public init?(json: JSON) {
        
        if let currentProgress: Int = "currentProgress" <~~ json {
            self.currentProgress = currentProgress
        } else {
            self.currentProgress = 0
        }
        
        if let currentStreakCount: Int = "currentStreakCount" <~~ json {
            self.currentStreakCount = currentStreakCount
        } else {
            self.currentStreakCount = 0
        }
        
        if let longestStreakCount: Int = "longestStreakCount" <~~ json {
            self.longestStreakCount = longestStreakCount
        } else {
            self.longestStreakCount = 0
        }
        
        if let longestStreakWeek: Int = "longestStreakWeek" <~~ json {
            self.longestStreakWeek = longestStreakWeek
        } else {
            self.longestStreakWeek = 0
        }
        
}
    
    public func toJSON() -> JSON? {
        return jsonify([
            "currentProgress" ~~> self.currentProgress,
            "currentStreakCount" ~~> self.currentStreakCount,
            "longestStreakCount" ~~> self.longestStreakCount,
            "longestStreakWeek" ~~> self.longestStreakWeek
            ])
    }
    
}

struct AchievementHighlight: Codable {
    var id: String!
    var achievementlevels: [AchievementLevel]
}

extension AchievementHighlight: Glossy {
    
     public init?(json: JSON) {
        self.id = "_id" <~~ json
        self.achievementlevels = "levels" <~~ json ?? []
    }
    
    public func toJSON() -> JSON? {
        return jsonify([
            "_id" ~~> self.id,
            "levels" ~~> self.achievementlevels
            ])
    }
    
}

struct AchievementLevel: Codable {
    var badge: Badge?
    var messageHeader: String!
    var message: String!
    var currentProgress: Int!
    var level: Int!
}

extension AchievementLevel: Glossy {
    
    public init?(json: JSON) {
        
        self.messageHeader = "messageHeader" <~~ json
        self.message = "message" <~~ json
        
        if let level: Int = "level" <~~ json {
            self.level = level
        } else {
            self.level = 0
        }
        
        if let currentProgress: Int = "currentProgress" <~~ json {
            self.currentProgress = currentProgress
        } else {
            self.currentProgress = 0
        }
        
        self.badge = "badge" <~~ json ?? nil
}
    
    public func toJSON() -> JSON? {
        return jsonify([
            "messageHeader" ~~> self.messageHeader,
            "message" ~~> self.message,
            "currentProgress" ~~> self.currentProgress,
            "level" ~~> self.level,
            "badge" ~~> self.badge
            ])
    }
    
}

struct Claims: Codable {
    var claimable: Bool
    var countOfClaimableBadges: Int
    var claimMessage: String
}

extension Claims: Glossy {
    
     public init?(json: JSON) {
        if let claimable: Int = "claimable" <~~ json {
            self.claimable = claimable == 1 ? true : false
        } else {
            self.claimable = false
        }
        
        if let countOfClaimableBadges: Int = "countOfClaimableBadges" <~~ json {
            self.countOfClaimableBadges = countOfClaimableBadges
        } else {
            self.countOfClaimableBadges = 0
        }
        
        if let claimMessage: String = "claimMessage" <~~ json {
            self.claimMessage = claimMessage
        } else {
            self.claimMessage = ""
        }
    }
    
    public func toJSON() -> JSON? {
        return jsonify([
            "claimable" ~~> self.claimable,
            "countOfClaimableBadges" ~~> self.countOfClaimableBadges,
            "claimMessage" ~~> self.claimMessage
        ])
    }
        
}

