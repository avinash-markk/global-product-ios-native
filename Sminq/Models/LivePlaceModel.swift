//
//  LivePlaceModel.swift
//  Markk
//
//  Created by Pushkar Deshmukh on 02/07/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import Foundation
import Gloss

public struct LivePlaces: Codable {
    var name: String!
    var geoPoint: GeoPoint!
    var address: String!
    var internationalPhoneNumber: String?
    var openingHours: OpeningHours?
    var isUserFollowing: Bool!
    var notificationStatus: Bool!
    var category: Category?
    var addressComponents: AddressComponents?
    var subCategory: SubCategory!
    var distance: Double!
    var defaultSubPlace: String!
    var latestFeed: [UserDataPlaceFeed]?
    var lastUpdatedTimeStamp: Double!
    var latestContributor: LatestContributor!
    var shortUrl: String!
    var shareUrl: String!
    var dynamicLink: String!
    var totalFeedItems: Int?
    var _id: String!
    var eventType: String!
    var placeMetrics: PlaceMetrics?
    var placeMoodScore: Int?
    var totalPlaceMoodScore: Int?
    var socialProfile: [SocialProfile]?
}

extension LivePlaces: Glossy {
    public init?(json: JSON) {
        if let name: String = "name" <~~ json {
            self.name = name
        } else {
            self.name = ""
        }
        
        self.geoPoint = "geoPoint" <~~ json
        
        if let address: String = "address" <~~ json {
            self.address = address
        } else {
            self.address = ""
        }
        
        if let internationalPhoneNumber: String = "internationalPhoneNumber" <~~ json {
            self.internationalPhoneNumber = internationalPhoneNumber
        } else {
            self.internationalPhoneNumber = ""
        }
        
        self.openingHours = "openingHours" <~~ json
        
        if let isUserFollowing: Bool = "isUserFollowing" <~~ json {
            self.isUserFollowing = isUserFollowing
        } else {
            self.isUserFollowing = false
        }
        
        if let notificationStatus: Bool = "notificationStatus" <~~ json {
            self.notificationStatus = notificationStatus
        } else {
            self.notificationStatus = false
        }
        
        self.category = "category" <~~ json
        self.subCategory = "subCategory" <~~ json
        self.addressComponents = "addressComponents" <~~ json
        
        if let distance: Double = "distance" <~~ json {
            self.distance = distance
        } else {
            self.distance = 0.0
        }
        
        if let defaultSubPlace: String = "defaultSubPlace" <~~ json {
            self.defaultSubPlace = defaultSubPlace
        } else {
            self.defaultSubPlace = ""
        }
        
        self.latestFeed = "latestFeed" <~~ json
        
        if let lastUpdatedTimeStamp: Double = "lastUpdatedTimeStamp" <~~ json {
            self.lastUpdatedTimeStamp = lastUpdatedTimeStamp
        } else {
            self.lastUpdatedTimeStamp = -1
        }
        
        self.latestContributor = "latestContributor" <~~ json
        
        if let shortUrl: String = "shortUrl" <~~ json {
            self.shortUrl = shortUrl
        } else {
            self.shortUrl = ""
        }
        
        if let shareUrl: String = "shareUrl" <~~ json {
            self.shareUrl = shareUrl
        } else {
            self.shareUrl = ""
        }
        
        if let dynamicLink: String = "dynamicLink" <~~ json {
            self.dynamicLink = dynamicLink
        } else {
            self.dynamicLink = ""
        }
        
        if let totalFeedItems: Int = "totalFeedItems" <~~ json {
            self.totalFeedItems = totalFeedItems
        } else {
            self.totalFeedItems = 0
        }
        
        if let _id: String = "_id" <~~ json {
            self._id = _id
        } else {
            self._id = ""
        }
        
        if let eventType: String = "eventType" <~~ json {
            self.eventType = eventType
        } else {
            self.eventType = ""
        }
        
        self.placeMetrics = "placeMetrics" <~~ json
        
        if let placeMoodScore: Int = "placeMoodScore" <~~ json {
            self.placeMoodScore = placeMoodScore
        } else {
            self.totalFeedItems = 0
        }
        
        if let totalPlaceMoodScore: Int = "totalPlaceMoodScore" <~~ json {
            self.totalPlaceMoodScore = totalPlaceMoodScore
        } else {
            self.totalPlaceMoodScore = 0
        }
        
        self.socialProfile = "socialProfile" <~~ json
    }
    
    public func toJSON() -> JSON? {
        return jsonify([
            "name" ~~> self.name,
            "geoPoint" ~~> self.geoPoint,
            "address" ~~> self.address,
            "internationalPhoneNumber" ~~> self.internationalPhoneNumber,
            "openingHours" ~~> self.openingHours,
            "isUserFollowing" ~~> self.isUserFollowing,
            "notificationStatus" ~~> self.notificationStatus,
            "category" ~~> self.category,
            "subCategory" ~~> self.subCategory,
            "distance" ~~> self.distance,
            "defaultSubPlace" ~~> self.defaultSubPlace,
            "latestFeed" ~~> self.latestFeed,
            "lastUpdatedTimeStamp" ~~> self.lastUpdatedTimeStamp,
            "latestContributor" ~~> self.latestContributor,
            "shortUrl" ~~> self.shortUrl,
            "shareUrl" ~~> self.shareUrl,
            "dynamicLink" ~~> self.dynamicLink,
            "totalFeedItems" ~~> self.totalFeedItems,
            "_id" ~~> self._id,
            "eventType" ~~> self.eventType,
            "placeMetrics" ~~> self.placeMetrics,
            "placeMoodScore" ~~> self.placeMoodScore,
            "totalPlaceMoodScore" ~~> self.totalPlaceMoodScore,
            "socialProfile" ~~> self.socialProfile,
            "addressComponents" ~~> self.addressComponents
            ])
    }
    
    public func getSubCategoryName() -> String {
        var tempSubCategoryName = ""
        
        if let subCategory = self.subCategory, let subCategoryName = subCategory.name {
            tempSubCategoryName = subCategoryName
        }
        
        return tempSubCategoryName
    }
    
    public func getSubCategoryWithDistance() -> String {
        var tempString = ""
        
        let subCat = getSubCategoryName()
        
        if subCat != "" && distance > 0 {
            tempString = "\(subCat) • \(SminqAPI.distanceFormated(rawDistanceDouble: round((distance))))"
        } else if subCat == "" && distance > 0 {
            tempString = "\(SminqAPI.distanceFormated(rawDistanceDouble: round((distance))))"
        } else if subCat != "" && distance == 0 {
            tempString = "\(subCat)"
        }
        
        return tempString
    }
    
}
