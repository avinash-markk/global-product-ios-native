//
//  AddPostQuestionsData.swift
//  Sminq
//
//  Created by Suraj on 3/11/18.
//  Copyright © 2018 Suraj Sagare. All rights reserved.
//

import Gloss

public struct PostData: Glossy {
    
    public var questions: [Question]!
    public var verified: Bool
    public var locationData: [Double]!
    public var error: ErrorObjectData?
    var place: LivePlaces?
    public var mood: String?
    public var liveShot: String?
    public var selectedStickers: [Stickers]?
    public var locationAccuracy: Double
    public var createdAt: String
    
    public init?(json: JSON) {
        
        self.questions = "questions" <~~ json
        
        if let verified: Int = "verified" <~~ json {
            self.verified = verified == 1 ? true : false
        } else {
            self.verified = false
        }
        
        if let coordinates: [Double] = "coordinates" <~~ json {
            self.locationData = coordinates
        } else {
            self.locationData = []
        }
        
        if let mood: String = "mood" <~~ json {
            self.mood = mood
        } else {
            self.mood = ""
        }
        
        if let tempPlace: LivePlaces = "place" <~~ json {
            self.place = tempPlace
        }
        
        if let live: String = "liveShot" <~~ json {
            self.liveShot = live
        } else {
            self.liveShot = ""
        }
        
        self.locationAccuracy = "locationAccuracy" <~~ json ?? 0
        
        self.error = "error" <~~ json
        
        self.selectedStickers = "selectedStickers" <~~ json
        
        self.createdAt = "createdAt" <~~ json ?? ""
    }
    
    public func toJSON() -> JSON? {
        return jsonify([
            "questions" ~~> self.questions,
            "verified" ~~> self.verified,
            "coordinates" ~~> self.locationData,
            "error" ~~> self.error,
            "place" ~~> self.place,
            "mood" ~~> self.mood,
            "liveShot" ~~> self.liveShot,
            "locationAccuracy" ~~> self.locationAccuracy,
            "selectedStickers" ~~> self.selectedStickers,
            "createdAt" ~~> self.createdAt
            ])
    }
    
}

public struct QuestionsData: Glossy {
    var questions: [SubCategoryQuestion]!
    public var stickers: StickersData?
    
    public init?(json: JSON) {
        self.questions = "questions" <~~ json
        self.stickers = "stickers" <~~ json
    }
    
    public func toJSON() -> JSON? {
        return jsonify([
            "questions" ~~> self.questions,
            "stickers" ~~> self.stickers
            ])
    }

}

public struct StickersData: Glossy {
    public var defaultStickers: [Stickers]?
    public var otherStickers: [Stickers]?
    
    public init?(json: JSON) {
        if let defaultStickers: [Stickers] = "default" <~~ json {
            self.defaultStickers = defaultStickers
        } else {
            self.defaultStickers = []
        }
        
        self.defaultStickers = "default" <~~ json
        
        if let otherStickers: [Stickers] = "others" <~~ json {
            self.otherStickers = otherStickers
        } else {
            self.otherStickers = []
        }
      //  self.otherStickers = "others" <~~ json
    }
    
    public func toJSON() -> JSON? {
        return jsonify([
            "default" ~~> self.defaultStickers,
            "others" ~~> self.otherStickers
            ])
    }
    
}

struct PostStickerData: Glossy {
    public var sticker: String
    public var weight: Double?
    public var id: String
    
    init?(json: JSON) {
        
        if let id: String = "id" <~~ json {
            self.id = id
        } else {
            self.id = ""
        }
        
        if let sticker: String = "sticker" <~~ json {
            self.sticker = sticker
        } else {
            self.sticker = ""
        }
        
        if let weight: Double = "weight" <~~ json {
            self.weight = weight
        } else {
            self.weight = 0.0
        }
    }
    
    func toJSON() -> JSON? {
        return jsonify([
            "id" ~~> self.id,
            "sticker" ~~> self.sticker,
            "weight" ~~> self.weight
            ])
    }
    
}
