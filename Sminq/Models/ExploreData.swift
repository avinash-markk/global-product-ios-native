//
//  ExploreData.swift
//  Sminq
//
//  Created by Bjorn Mascarenhas on 27/02/18.
//  Copyright © 2018 Suraj Sagare. All rights reserved.
//

import Gloss

struct OpeningHours: Codable {
    var weekdayText: [String]?
}

extension OpeningHours: Glossy {
    
    public init?(json: JSON) {
        if let weekdayText: [String] = "weekdayText" <~~ json {
            self.weekdayText = weekdayText
        } else {
            self.weekdayText = []
        }
    }
    
    public func toJSON() -> JSON? {
        return jsonify([
            "weekdayText" ~~> self.weekdayText
            ])
    }
    
}

struct AddressComponents: Codable {
     var administrative_area_level_1: String?
     var administrative_area_level_2: String?
     var country: String?
     var locality: String?
     var sublocality_level_1: String?
     var subLocality: String?
}

extension AddressComponents: Glossy {
    
    public init?(json: JSON) {
        if let administrative_area_level_1: String = "administrative_area_level_1" <~~ json {
            self.administrative_area_level_1 = administrative_area_level_1
        } else {
            self.administrative_area_level_1 = ""
        }
        
        if let administrative_area_level_2: String = "administrative_area_level_2" <~~ json {
            self.administrative_area_level_2 = administrative_area_level_2
        } else {
            self.administrative_area_level_2 = ""
        }
        
        if let country: String = "country" <~~ json {
            self.country = country
        } else {
            self.country = ""
        }
        
        if let locality: String = "locality" <~~ json {
            self.locality = locality
        } else {
            self.locality = ""
        }
        
        if let subLocality: String = "subLocality" <~~ json {
            self.subLocality = subLocality
        } else {
            self.subLocality = ""
        }
        
        if let sublocality_level_1: String = "sublocality_level_1" <~~ json {
            self.sublocality_level_1 = sublocality_level_1
        } else {
            self.sublocality_level_1 = ""
        }
    }
    
    public func toJSON() -> JSON? {
        return jsonify([
            "administrative_area_level_1" ~~> self.administrative_area_level_1,
            "administrative_area_level_2" ~~> self.administrative_area_level_2,
            "country" ~~> self.country,
            "locality" ~~> self.locality,
            "subLocality" ~~> self.subLocality,
            "sublocality_level_1" ~~> self.sublocality_level_1
            ])
    }
    
}

// Image Data
struct ImageData: Codable {
     var mimeType: String!
     var path: String!
     var size: Int?
     var fileName: String!
     var bucket: String!
     var url: String!
     var etag: String!
}

extension ImageData: Glossy {
    
    public init?(json: JSON) {
        if let mimeType: String = "mimeType" <~~ json {
            self.mimeType = mimeType
        } else {
            self.mimeType = ""
        }
        
        if let path: String = "path" <~~ json {
            self.path = path
        } else {
            self.path = ""
        }
        
        if let size: Int = "size" <~~ json {
            self.size = size
        } else {
            self.size = 0
        }
        
        if let fileName: String = "fileName" <~~ json {
            self.fileName = fileName
        } else {
            self.fileName = ""
        }
        
        if let bucket: String = "bucket" <~~ json {
            self.bucket = bucket
        } else {
            self.bucket = ""
        }
        
        if let url: String = "url" <~~ json {
            self.url = url
        } else {
            self.url = ""
        }
        
        if let etag: String = "etag" <~~ json {
            self.etag = etag
        } else {
            self.etag = ""
        }
        
    }
    
    public func toJSON() -> JSON? {
        return jsonify([
            "mimeType" ~~> self.mimeType,
            "path" ~~> self.path,
            "size" ~~> self.size,
            "fileName" ~~> self.fileName,
            "bucket" ~~> self.bucket,
            "url" ~~> self.url,
            "etag" ~~> self.etag
            ])
    }
    
}

// LatestContributor
struct LatestContributor: Codable {
     var name: String!
     var displayName: [String]!
     var imageUrl: String!
     var _id: String!
}

extension LatestContributor: Glossy {
    
    public init?(json: JSON) {
        if let _id: String = "_id" <~~ json {
            self._id = _id
        } else {
            self._id = ""
        }
        
        if let name: String = "name" <~~ json {
            self.name = name
        } else {
            self.name = ""
        }
        
        if let displayName: [String] = "displayName" <~~ json {
            self.displayName = displayName
        } else {
            self.displayName = [""]
        }
        
        if let imageUrl: String = "imageUrl" <~~ json {
            self.imageUrl = imageUrl
        } else {
            self.imageUrl = ""
        }
    }
    
    public func toJSON() -> JSON? {
        return jsonify([
            "_id" ~~> self._id,
            "name" ~~> self.name,
            "displayName" ~~> self.displayName,
            "imageUrl" ~~> self.imageUrl
            ])
    }
    
}
