//
//  ErrorData.swift
//  Sminq
//
//  Created by Pushkar Deshmukh on 05/12/18.
//  Copyright © 2018 Markk. All rights reserved.
//

import Foundation
import Gloss

public struct APIErrorResponse: Glossy {
    public var success: Bool!
    public var error: ErrorObjectData!
    public var errorMessage: String?

    public init?(json: JSON) {
        self.success = "success" <~~ json
        self.error = "error" <~~ json
        self.errorMessage = "errorMessage" <~~ json ?? ""
    }
    
    public func toJSON() -> JSON? {
        return jsonify([
            "success" ~~> self.success,
            "error" ~~> self.error,
            "errorMessage" ~~> self.errorMessage
            ])
    }
    
}

public struct ErrorObjectData: Glossy {
    public var code: Int?
    public var message: String?
    
    public init?(json: JSON) {
        self.code = "code" <~~ json
        
        if let message: String = "message" <~~ json {
            self.message = message
        } else {
            self.message = ""
        }
    }

    public func toJSON() -> JSON? {
        return jsonify([
            "code" ~~> self.code,
            "message" ~~> self.message
            ])
    }
    
}
