//
//  UsersData.swift
//  Sminq
//
//  Created by Bjorn Mascarenhas on 27/02/18.
//  Copyright © 2018 Suraj Sagare. All rights reserved.
//

import Gloss

// UserDataDict

struct UserDataDict: Codable {
    var name: String!
    var email: String!
    var mobile: String!
    var type: String?
    var bio: String?
    var countryCode: String!
    var imageUrl: String!
    var createdAt: Int?
    var mobileVerified: Bool?
    var emailVerified: Bool?
    var userFollowing: Bool?
    var displayName: [String]!
    var roles: [Roles]!
    var socialIdentity: [SocialIdentity]!
    var userCity: City!
    var userCountry: Country!
    var existingUser: Bool
    var _id: String!
    var birthDate: String!
    var gender: String!
    var accuracyScore: String!
    var followingPlaces: [String]!
    var authToken: String
    var errorMessage: String?
    var homeCountryLocalFlag: String
}

extension UserDataDict: Glossy {
    
     public init?(json: JSON) {
        if let name: String = "name" <~~ json {
            self.name = name
        } else {
            self.name = ""
        }
        
        if let mobile: String = "mobile" <~~ json {
            self.mobile = mobile
        } else {
            self.mobile = ""
        }
        
        if let type: String = "type" <~~ json {
            self.type = type
        } else {
            self.type = ""
        }
        
        if let bio: String = "bio" <~~ json {
            self.bio = bio
        } else {
            self.bio = ""
        }
        
        if let countryCode: String = "countryCode" <~~ json {
            self.countryCode = countryCode
        } else {
            self.countryCode = ""
        }
        
        if let email: String = "email" <~~ json {
            self.email = email
        } else {
            self.email = ""
        }
        
        if let imageUrl: String = "imageUrl" <~~ json {
            self.imageUrl = imageUrl
        } else {
            self.imageUrl = ""
        }
        
        if let displayName: [String] = "displayName" <~~ json {
            self.displayName = displayName
        } else {
            self.displayName = []
        }
        
        if let createdAt: Int = "createdAt" <~~ json {
            self.createdAt = createdAt
        } else {
            self.createdAt = 0
        }
        
        if let mobileVerified: Bool = "mobileVerified" <~~ json {
            self.mobileVerified = mobileVerified
        } else {
            self.mobileVerified = false
        }
        
        if let emailVerified: Bool = "emailVerified" <~~ json {
            self.emailVerified = emailVerified
        } else {
            self.emailVerified = false
        }
        
        if let userFollowing: Bool = "userFollowing" <~~ json {
            self.userFollowing = userFollowing
        } else {
            self.userFollowing = false
        }

        self.socialIdentity = "socialIdentity" <~~ json
        self.userCity = "userCity" <~~ json
        self.userCountry = "userCountry" <~~ json
        self.roles = "roles" <~~ json
        
        if let existingUser: Int = "existingUser" <~~ json {
            self.existingUser = existingUser == 1 ? true : false
        } else {
            self.existingUser = false
        }
        
        if let _id: String = "_id" <~~ json {
            self._id = _id
        } else {
            self._id = ""
        }
        
        if let birthDate: String = "birthDate" <~~ json {
            self.birthDate = birthDate
        } else {
            self.birthDate = ""
        }
        
        if let gender: String = "gender" <~~ json {
            self.gender = gender
        } else {
            self.gender = ""
        }
        
        if let accuracyScore: String = "accuracyScore" <~~ json {
            self.accuracyScore = accuracyScore
        } else {
            self.accuracyScore = ""
        }
        
        if let followingPlaces: [String] = "followingPlaces" <~~ json {
            self.followingPlaces = followingPlaces
        } else {
            self.followingPlaces = []
        }
        
        self.authToken = "authToken" <~~ json ?? ""
        
        if let errorMessage: String = "errorMessage" <~~ json {
            self.errorMessage = errorMessage
        } else {
            self.errorMessage = ""
        }
        
        self.homeCountryLocalFlag = "homeCountryLocalFlag" <~~ json ?? ""
    }
    
    func getKeyValueDictionaryForCleverTap() -> [String: Any] {
        var profile: [String: Any] = ["Name": self.name, "Identity": self._id, "Phone": self.mobile, "Email": self.email, "displayName": self.displayName[0], "Photo": self.imageUrl, "email_verified": self.emailVerified, "mobile_verified": self.mobileVerified as Any]
        
        if self.birthDate != "" && self.birthDate != nil {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            let userBirthDate = dateFormatter.date(from: self.birthDate)
            
            profile["DOB"] = userBirthDate
        }
        
        if self.bio != "" && self.bio != nil {
            profile["bio"] = AppConstants.yes
        } else {
            profile["bio"] = AppConstants.no
        }
        
        if self.gender != "" && self.gender != nil {
            profile["Gender"] = self.gender
        }
        
        if let userCity = self.userCity, let cityName = userCity.name {
            profile["city"] = cityName
        }
        
        if let userCountry = self.userCountry, let countryName = userCountry.name {
            profile["country"] = countryName
        }
        
        if self.homeCountryLocalFlag != "" {
            profile["homeCountry"] = self.homeCountryLocalFlag
        }
        
        return profile
    }
    
    func isProfileComplete() -> Bool {
        if self.email == "" || self.mobile == "" || (self.userCity != nil && self.userCity.name == "" ) || (self.userCountry != nil && self.userCountry.name == "" ) || self.birthDate == "" || self.gender == "" || self.bio == "" {
            return false
        }
        
        return true
    }
    
    func getUserDisplayName() -> String {
        let displayName = self.displayName[0]
        return displayName
    }
    
    public func toJSON() -> JSON? {
        return jsonify([
            "name" ~~> self.name,
            "email" ~~> self.email,
            "mobile" ~~> self.mobile,
            "type" ~~> self.type,
            "bio" ~~> self.bio,
            "countryCode" ~~> self.countryCode,
            "displayName" ~~> self.displayName,
            "imageUrl" ~~> self.imageUrl,
            "createdAt" ~~> self.createdAt,
            "emailVerified" ~~> self.emailVerified,
            "mobileVerified" ~~> self.mobileVerified,
            "userFollowing" ~~> self.userFollowing,
            "socialIdentity" ~~> self.socialIdentity,
            "userCity" ~~> self.userCity,
            "userCountry" ~~> self.userCountry,
            "roles" ~~> self.roles,
            "existingUser" ~~> self.existingUser,
            "_id" ~~> self._id,
            "birthDate" ~~> self.birthDate,
            "gender" ~~> self.gender,
            "accuracyScore" ~~> self.accuracyScore,
            "followingPlaces" ~~> self.followingPlaces,
            "homeCountryLocalFlag" ~~> self.homeCountryLocalFlag
            ])
    }
    
}
// Social Identity
struct SocialIdentity: Codable {
     var network: String!
     var identity: String!
     var accessToken: String!
}
extension SocialIdentity: Glossy {
    

    public init?(json: JSON) {
        if let network: String = "network" <~~ json {
            self.network = network
        } else {
            self.network = ""
        }
        
        if let identity: String = "identity" <~~ json {
            self.identity = identity
        } else {
            self.identity = ""
        }
        
        if let accessToken: String = "accessToken" <~~ json {
            self.accessToken = accessToken
        } else {
            self.accessToken = ""
        }
    }
    
    public func toJSON() -> JSON? {
        return jsonify([
            "network" ~~> self.network,
            "identity" ~~> self.identity,
            "accessToken" ~~> self.accessToken
            ])
    }
    
}


struct City: Codable {
    public var _id: String!
    public var name: String!
}

extension City: Glossy {
   
    public init?(json: JSON) {
        if let _id: String = "_id" <~~ json {
            self._id = _id
        } else {
            self._id = ""
        }
        
        if let name: String = "name" <~~ json {
            self.name = name
        } else {
            self.name = ""
        }
        
    }
    
    public func toJSON() -> JSON? {
        return jsonify([
            "_id" ~~> self._id,
            "name" ~~> self.name
            ])
    }
    
}

struct Country: Codable {
    public var _id: String!
    public var name: String!
}

extension Country: Glossy {

    public init?(json: JSON) {
        if let _id: String = "_id" <~~ json {
            self._id = _id
        } else {
            self._id = ""
        }
        
        if let name: String = "name" <~~ json {
            self.name = name
        } else {
            self.name = ""
        }
        
    }
    
    public func toJSON() -> JSON? {
        return jsonify([
            "_id" ~~> self._id,
            "name" ~~> self.name
            ])
    }
    
}
