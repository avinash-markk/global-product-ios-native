//
//  UserFeedItemsData.swift
//  Sminq
//
//  Created by Pushkar Deshmukh on 27/06/18.
//  Copyright © 2018 Suraj Sagare. All rights reserved.
//

import Foundation
import Gloss

public struct PostFeedResponse: Glossy {
    public var success: Bool
    public var httpCode: Int?
    public var status: PostFeedResponseStatus?
    
    public init?(json: JSON) {
        if let success: Int = "success" <~~ json {
            self.success = success == 1 ? true : false
        } else {
            self.success = false
        }
        
        self.httpCode = "httpCode" <~~ json
        self.status = "status" <~~ json
        
    }
    
    public func toJSON() -> JSON? {
        return jsonify([
            "success" ~~> self.success,
            "httpCode" ~~> self.httpCode,
            "status" ~~> self.status
            ])
    }
    
}

public struct PostFeedResponseStatus: Glossy {
    var feedItemsList: [UserDataPlaceFeed]?
    
    public init?(json: JSON) {
        self.feedItemsList = "feedItemsList" <~~ json
    }
    
    public func toJSON() -> JSON? {
        return jsonify([
            "feedItemsList" ~~> self.feedItemsList
            ])
    }
    
}
