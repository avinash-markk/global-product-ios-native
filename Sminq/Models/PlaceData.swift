//
//  PlaceData.swift
//  Sminq
//
//  Created by Pushkar Deshmukh on 06/12/18.
//  Copyright © 2018 Suraj Sagare. All rights reserved.
//

import Foundation
import Gloss

public struct PlaceDetail: Glossy {
    var place: LivePlaces?
    public var questions: [Question]?
    public var stickers: [Stickers]?
    
    public init?(json: JSON) {
        if let livePLace: LivePlaces = "place" <~~ json {
            self.place = livePLace
        }
        self.questions = "questions" <~~ json
        self.stickers = "stickers" <~~ json
    }
    
    public func toJSON() -> JSON? {
        return jsonify([
            "place" ~~> self.place,
            "questions" ~~> self.questions,
            "stickers" ~~> self.stickers
            ])
    }
    
}

// GeoPoint
struct GeoPoint: Codable {
    var type: String!
    var coordinates: [Double]!
}

extension GeoPoint: Glossy {
    public init?(json: JSON) {
        if let type: String = "type" <~~ json {
            self.type = type
        } else {
            self.type = ""
        }
        
        if let coordinates: [Double] = "coordinates" <~~ json {
            self.coordinates = coordinates
        } else {
            self.coordinates = [0.0]
        }
        
    }
    
    public func toJSON() -> JSON? {
        return jsonify([
            "type" ~~> self.type,
            "coordinates" ~~> self.coordinates
            ])
    }
}

// Category
struct Category: Codable {
    var name: String!
    var status: Bool
    var imageData: ImageData!
    var key: String!
    var color: String!
    var _id: String!
}

extension Category: Glossy {
    
    public init?(json: JSON) {
        if let name: String = "name" <~~ json {
            self.name = name
        } else {
            self.name = ""
        }
        
        if let status: Int = "status" <~~ json {
            self.status = status == 1 ? true : false
        } else {
            self.status = false
        }
        
        self.imageData = "image" <~~ json
        
        if let key: String = "key" <~~ json {
            self.key = key
        } else {
            self.key = ""
        }
        
        if let color: String = "color" <~~ json {
            self.color = color
        } else {
            self.color = ""
        }
        
        if let _id: String = "_id" <~~ json {
            self._id = _id
        } else {
            self._id = ""
        }
        
    }
    
    public func toJSON() -> JSON? {
        return jsonify([
            "name" ~~> self.name,
            "status" ~~> self.status,
            "image" ~~> self.imageData,
            "key" ~~> self.key,
            "color" ~~> self.color,
            "_id" ~~> self._id
            ])
    }
    
}

public struct PlaceMetrics: Codable {
    var totalComments: Int?
    var totalPosts: Int?
    var totalDownVotes: Int?
    var totalUpVotes: Int?
    var totalViews: Int
    var totalShares: Int
}

extension PlaceMetrics: Glossy {
    
    public init?(json: JSON) {
        if let totalComments: Int = "totalComments" <~~ json {
            self.totalComments = totalComments
        } else {
            self.totalComments = 0
        }
        
        if let totalPosts: Int = "totalPosts" <~~ json {
            self.totalPosts = totalPosts
        } else {
            self.totalPosts = 0
        }
        
        if let totalDownVotes: Int = "totalDownVotes" <~~ json {
            self.totalDownVotes = totalDownVotes
        } else {
            self.totalDownVotes = 0
        }
        
        if let totalUpVotes: Int = "totalUpVotes" <~~ json {
            self.totalUpVotes = totalUpVotes
        } else {
            self.totalUpVotes = 0
        }
        
        self.totalViews = "totalViews" <~~ json ?? 0
        self.totalShares = "totalShares" <~~ json ?? 0
    }
    
    public func toJSON() -> JSON? {
        return jsonify([
            "totalComments" ~~> self.totalComments,
            "totalPosts" ~~> self.totalPosts,
            "totalDownVotes" ~~> self.totalDownVotes,
            "totalDownVotes" ~~> self.totalDownVotes,
            "totalUpVotes" ~~> self.totalUpVotes,
            "totalViews" ~~> self.totalViews,
            "totalShares" ~~> self.totalShares
            ])
    }
    
}

// SubCategory
struct SubCategory: Codable {
    var parentCategory: String!
    var name: String!
    var status: Bool
    var _id: String!
}

extension SubCategory: Glossy {
    
    public init?(json: JSON) {
        if let parentCategory: String = "parentCategory" <~~ json {
            self.parentCategory = parentCategory
        } else {
            self.parentCategory = ""
        }
        
        if let name: String = "name" <~~ json {
            self.name = name
        } else {
            self.name = ""
        }
        
        if let status: Int = "status" <~~ json {
            self.status = status == 1 ? true : false
        } else {
            self.status = false
        }
        
        if let _id: String = "_id" <~~ json {
            self._id = _id
        } else {
            self._id = ""
        }
    }
    
    public func toJSON() -> JSON? {
        return jsonify([
            "parentCategory" ~~> self.parentCategory,
            "name" ~~> self.name,
            "status" ~~> self.status,
            "_id" ~~> self._id
            ])
    }
    
}
