//
//  SearchModel.swift
//  Markk
//
//  Created by Pushkar Deshmukh on 05/07/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import Foundation
import Gloss

// Hybrid search
public struct HybridSearch: Glossy {
    public var success: Bool
    public var httpCode: Int?
    public var hybridPlaces: HybridPlaces!
    public var error: ErrorObjectData?
    
    public init?(json: JSON) {
        if let success: Int = "success" <~~ json {
            self.success = success == 1 ? true : false
        } else {
            self.success = false
        }
        
        self.httpCode = "httpCode" <~~ json
        self.hybridPlaces = "status" <~~ json
        self.error = "error" <~~ json
        
    }
    
    public func toJSON() -> JSON? {
        return jsonify([
            "success" ~~> self.success,
            "httpCode" ~~> self.httpCode,
            "status" ~~> self.hybridPlaces,
            "error" ~~> self.error
            ])
    }
    
}

public struct HybridPlaces: Glossy {
    var sminqResults: [LivePlaces]?
    var googleResults: [LivePlaces]?
    var peopleResults: [UserDataDict]?
    
    public init?(json: JSON) {
        self.sminqResults = "sminqResults" <~~ json
        self.googleResults = "googleResults" <~~ json
        self.peopleResults = "peopleResults" <~~ json
    }
    
    public func toJSON() -> JSON? {
        return jsonify([
            "sminqResults" ~~> self.sminqResults,
            "googleResults" ~~> self.googleResults,
            "peopleResults" ~~> self.peopleResults
            ])
    }
    
}
