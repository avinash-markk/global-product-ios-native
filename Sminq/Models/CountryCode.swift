//
//  CountryCode.swift
//  Sminq
//
//  Created by Pushkar Deshmukh on 25/06/18.
//  Copyright © 2018 Suraj Sagare. All rights reserved.
//

import Foundation
import Gloss

public struct CountryCode: Glossy {
    public var code: String!
    public var dial_code: String!
    public var flag: String!
    public var name: String!
    
    public init?(json: JSON) {
        if let code: String = "code" <~~ json {
            self.code = code
        } else {
            self.code = ""
        }
        
        if let dial_code: String = "dial_code" <~~ json {
            self.dial_code = dial_code
        } else {
            self.dial_code = ""
        }
        
        if let flag: String = "flag" <~~ json {
            self.flag = flag
        } else {
            self.flag = ""
        }
        
        if let name: String = "name" <~~ json {
            self.name = name
        } else {
            self.name = ""
        }
        
      
    }
    
    public func toJSON() -> JSON? {
        return jsonify([
            "code" ~~> self.code,
            "dial_code" ~~> self.dial_code,
            "flag" ~~> self.flag,
            "name" ~~> self.name
            ])
    }
    
}
