//
//  MediaExtensions.swift
//  Sminq
//
//  Created by Pushkar Deshmukh on 25/01/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import Foundation

enum MediaExtensions: String {
    case mp4 = "mp4"
    case jpg = "jpg"
}
