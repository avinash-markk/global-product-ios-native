//
//  QuestionModel.swift
//  Markk
//
//  Created by Pushkar Deshmukh on 05/07/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import Foundation
import Gloss

public struct Question: Glossy {
    public var subCategory: String!
    public var text: String!
    var options: [Options]!
    public var dataType: String!
    public var context: String!
    public var status: Bool
    public var controlType: String!
    public var _id: String!
    
    public init?(json: JSON) {
        if let subCategory: String = "subCategory" <~~ json {
            self.subCategory = subCategory
        } else {
            self.subCategory = ""
        }
        
        if let text: String = "text" <~~ json {
            self.text = text
        } else {
            self.text = ""
        }
        
        self.options = "options" <~~ json
        
        if let dataType: String = "dataType" <~~ json {
            self.dataType = dataType
        } else {
            self.dataType = ""
        }
        
        if let context: String = "context" <~~ json {
            self.context = context
        } else {
            self.context = ""
        }
        
        if let status: Int = "status" <~~ json {
            self.status = status == 1 ? true : false
        } else {
            self.status = false
        }
        
        if let controlType: String = "controlType" <~~ json {
            self.controlType = controlType
        } else {
            self.controlType = ""
        }
        
        if let _id: String = "_id" <~~ json {
            self._id = _id
        } else {
            self._id = ""
        }
        
    }
    
    public func toJSON() -> JSON? {
        return jsonify([
            "subCategory" ~~> self.subCategory,
            "text" ~~> self.text,
            "options" ~~> self.options,
            "dataType" ~~> self.dataType,
            "context" ~~> self.context,
            "status" ~~> self.status,
            "controlType" ~~> self.controlType,
            "_id" ~~> self._id
            ])
    }
    
}


// SubCategoryQuestions
struct SubCategoryQuestion: Codable {
    var subCategory: String!
    var text: String!
    var options: [Options]!
    var dataType: String!
    var context: String!
    var status: Bool
    var controlType: String!
    var _id: String!
}

extension SubCategoryQuestion: Glossy {
    
    public init?(json: JSON) {
        if let subCategory: String = "subCategory" <~~ json {
            self.subCategory = subCategory
        } else {
            self.subCategory = ""
        }
        
        if let text: String = "text" <~~ json {
            self.text = text
        } else {
            self.text = ""
        }
        
        self.options = "options" <~~ json
        
        if let dataType: String = "dataType" <~~ json {
            self.dataType = dataType
        } else {
            self.dataType = ""
        }
        
        if let context: String = "context" <~~ json {
            self.context = context
        } else {
            self.context = ""
        }
        
        if let status: Int = "status" <~~ json {
            self.status = status == 1 ? true : false
        } else {
            self.status = false
        }
        
        if let controlType: String = "controlType" <~~ json {
            self.controlType = controlType
        } else {
            self.controlType = ""
        }
        
        if let _id: String = "_id" <~~ json {
            self._id = _id
        } else {
            self._id = ""
        }
        
    }
    
    public func toJSON() -> JSON? {
        return jsonify([
            "subCategory" ~~> self.subCategory,
            "text" ~~> self.text,
            "options" ~~> self.options,
            "dataType" ~~> self.dataType,
            "context" ~~> self.context,
            "status" ~~> self.status,
            "controlType" ~~> self.controlType,
            "_id" ~~> self._id
            ])
    }
}

// Options
struct Options: Codable {
    var icon: String!
    var option: String!
}

extension Options: Glossy {
    
    public init?(json: JSON) {
        if let icon: String = "icon" <~~ json {
            self.icon = icon
        } else {
            self.icon = ""
        }
        
        if let option: String = "option" <~~ json {
            self.option = option
        } else {
            self.option = ""
        }
    }
    
    public func toJSON() -> JSON? {
        return jsonify([
            "icon" ~~> self.icon,
            "option" ~~> self.option,
            ])
    }
    
}

// Contributor
struct Contributions: Codable {
    var input: String!
    var subCategoryQuestion: SubCategoryQuestion!
    var image: String!
    var video: String!
    var videoDuration: Int?
    var mediaMetadata: MediaMetadata?
}

extension Contributions: Glossy {
    
    public init?(json: JSON) {
        if let input: String = "input" <~~ json {
            self.input = input
        } else {
            self.input = ""
        }
        
        if let image: String = "image" <~~ json {
            self.image = image
        } else {
            self.image = ""
        }
        
        if let video: String = "video" <~~ json {
            self.video = video
        } else {
            self.video = ""
        }
        self.subCategoryQuestion = "subCategoryQuestion" <~~ json
        
        if let videoDuration: Int = "videoDuration" <~~ json {
            self.videoDuration = videoDuration
        } else {
            self.videoDuration = 0
        }
        
        self.mediaMetadata = "mediaMetadata" <~~ json
        
    }
    
    public func toJSON() -> JSON? {
        return jsonify([
            "input" ~~> self.input,
            "subCategoryQuestion" ~~> self.subCategoryQuestion,
            "image" ~~> self.image,
            "video" ~~> self.video,
            "videoDuration" ~~> self.videoDuration,
            "mediaMetadata" ~~> self.mediaMetadata
            ])
    }
    
}
