//
//  StoriesData.swift
//  Sminq
//
//  Created by Pushkar Deshmukh on 18/07/18.
//  Copyright © 2018 Suraj Sagare. All rights reserved.
//

import Foundation
import Gloss

struct StoryData: Codable {
    var userId: String!
    var placeId: String!
    var placeDetails: LivePlaces?
    var archived: Bool?
    var posts: [UserDataPlaceFeed]?
    var feedViewModel = FeedViewModel.init(screen: "")
}

extension StoryData: Glossy {
    
    public init?(json: JSON) {
        if let userId: String = "userId" <~~ json {
            self.userId = userId
        } else {
            self.userId = ""
        }
        
        if let placeId: String = "placeId" <~~ json {
            self.placeId = placeId
        } else {
            self.placeId = ""
        }
        
        if let archived: Bool = "archived" <~~ json {
            self.archived = archived
        } else {
            self.archived = false
        }
        
        if let posts: [UserDataPlaceFeed] = "posts" <~~ json {
            self.posts = posts
            self.feedViewModel?.setFeedList(feedList: posts)
        } else {
            self.posts = []
            self.feedViewModel?.setFeedList(feedList: [])
        }
        
        self.placeDetails = "placeDetails" <~~ json
    }
    
    public func toJSON() -> JSON? {
        return jsonify([
            "userId" ~~> self.userId,
            "placeId" ~~> self.placeId,
            "archived" ~~> self.archived,
            "placeDetails" ~~> self.placeDetails,
            "posts" ~~> self.posts
            ])
    }
    
    func copyPlaceDetailsToPostsAndReturnList() -> [UserDataPlaceFeed] {
        var localFeedList = [UserDataPlaceFeed]()
        
        if var feedTempList = self.posts, feedTempList.count > 0, let place = self.placeDetails {
            for feedIndex in 0..<feedTempList.count {
                feedTempList[feedIndex].placeDetails = place
                localFeedList.append(feedTempList[feedIndex])
            }
        }
        
        return localFeedList
    }
    
    func getPostCountsLabel() -> String? {
        var tempString = ""
        if let posts = self.posts {
            if posts.count == 1 {
                tempString = "rating"
            } else {
                tempString = "ratings"
            }
            
            return "\(posts.count) \(tempString)"
        }
        
        return nil
    }
    
//    mutating func copyPlaceDetailsToPosts() {
//        var feedList = [UserDataPlaceFeed]()
//        
//        if var feedTempList = self.posts, feedTempList.count > 0, let place = self.placeDetails  {
//            for feedIndex in 0..<feedTempList.count {
//                feedTempList[feedIndex].placeDetails = place
//                feedList.append(feedTempList[feedIndex])
//            }
//            
//            self.posts = feedList
//        }
//    }
    
}

public struct StoryViewData: Codable {
    public var id: String!
    public var user: String!
    public var place: String?
    public var views: Int?
    public var owner: String
}

extension StoryViewData: Glossy {
    public init?(json: JSON) {
        if let id: String = "id" <~~ json {
            self.id = id
        } else {
            self.id = ""
        }
        
        if let user: String = "user" <~~ json {
            self.user = user
        } else {
            self.user = ""
        }
        
        if let place: String = "place" <~~ json {
            self.place = place
        } else {
            self.place = ""
        }
        
        if let views: Int = "views" <~~ json {
            self.views = views
        } else {
            self.views = 0
        }
        
        self.owner = "owner" <~~ json ?? ""
    }
    
    public func toJSON() -> JSON? {
        return jsonify([
            "id" ~~> self.id,
            "user" ~~> self.user,
            "place" ~~> self.place,
            "views" ~~> self.views,
            "owner" ~~> self.owner
            ])
    }
}
