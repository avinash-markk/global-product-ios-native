//
//  CustomPin.swift
//  Sminq
//
//  Created by Pushkar Deshmukh on 08/08/18.
//  Copyright © 2018 Suraj Sagare. All rights reserved.
//

import Foundation
import MapKit

class CustomPin: NSObject, MKAnnotation {
    var title: String?
    var subtitle: String?
    var coordinate: CLLocationCoordinate2D
    var pinImage: String?
    
    init(pinTitle: String, pinSubTitle: String, pinCoordinate: CLLocationCoordinate2D, pinImage: String) {
        self.title = pinTitle
        self.subtitle = pinSubTitle
        self.coordinate = pinCoordinate
        self.pinImage = pinImage
    }
}
