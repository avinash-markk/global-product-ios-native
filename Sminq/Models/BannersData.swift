//
//  BannersData.swift
//  Sminq
//
//  Created by Pushkar Deshmukh on 14/03/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import Foundation
import Gloss

enum BannerSectionType: String, Codable {
    case highlights = "highlights"
    case trending = "trending"
}

public struct BannerData: Codable {
    var type: BannerSectionType?
    var channel: String
    var label: String
    var description: String
    var maxBanners: Int
    var minBanners: Int
    var banners = [Banner]()
}

extension BannerData: Glossy {
    public init?(json: JSON) {
        self.type = "type" <~~ json
        self.channel = "channel" <~~ json ?? ""
        self.label = "label" <~~ json ?? ""
        self.description = "description" <~~ json ?? ""
        self.maxBanners = "maxBanners" <~~ json ?? 0
        self.minBanners = "minBanners" <~~ json ?? 0
        self.banners = "banners" <~~ json ?? []
    }
    
    public func toJSON() -> JSON? {
        return jsonify([
            "type" ~~> self.type,
            "channel" ~~> self.channel,
            "label" ~~> self.label,
            "description" ~~> self.label,
            "maxBanners" ~~> self.label,
            "minBanners" ~~> self.label,
            "banners" ~~> self.banners
            ])
    }
}

enum BannerType: String, Codable {
    case maxLikes = "max_likes"
    case maxViews = "max_views"
    case maxRatings = "max_ratings"
    case maxUserRatings = "max_user_ratings"
    case maxUserPlaces = "max_user_places"
    case totalDope = "total_dope"
    case totalStickers = "total_sticker"
    case newBadge = "new_badge"
    case claimedReward = "claimed_reward"
    case showcase = "showcase"
}

enum BannerCTA: String, Codable {
    case user = "user"
    case story = "story"
}

public struct Banner: Codable {
    var cta: BannerCTA?
    var type: BannerType?
    var label: String
    var icon: String
    var threshold: Int
    var timestamp: Double
    var users = [UserDataDict]()
    var places = [PlacesStoriesData]()
    var color: String?
}

extension Banner: Glossy {
    public init?(json: JSON) {
        self.cta = "cta" <~~ json
        self.type = "type" <~~ json
        self.label = "label" <~~ json ?? ""
        self.icon = "icon" <~~ json ?? ""
        self.threshold = "threshold" <~~ json ?? 0
        self.timestamp = "timestamp" <~~ json ?? 0.0
        self.users = "users" <~~ json ?? []
        self.places = "places" <~~ json ?? []
        self.color = "color" <~~ json ?? ""
    }
    
    public func toJSON() -> JSON? {
        return jsonify([
            "cta" ~~> self.cta,
            "type" ~~> self.type,
            "label" ~~> self.label,
            "icon" ~~> self.icon,
            "threshold" ~~> self.threshold,
            "timestamp" ~~> self.timestamp,
            "users" ~~> self.users,
            "places" ~~> self.places,
            "color" ~~> self.color,
            ])
    }
}

