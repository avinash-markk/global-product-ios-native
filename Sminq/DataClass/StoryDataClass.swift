//
//  StoryDataClass.swift
//  Sminq
//
//  Created by Pushkar Deshmukh on 22/02/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import Foundation
import Gloss
import Firebase

class StoryDataClass {
    private(set) var screen = ""
    
    init?(screen: String) {
        self.screen = screen
    }
    
    func getLiveStories(userId: String, city: String, sort: String, limit: Int?, latitude: Double, longitude: Double, postType: String, distance: String, placeGroup: String, completion: @escaping(_ stories: [StoryData]?, _ apiResponseState: APIResponseState, _ networkError: Error?, _ error: ErrorObject?) -> Void) {
        if InternetConnection.isConnectedToInternet() {
            guard let metric = HTTPMetric(url: URL(string: PlaceService().getStoriesAPIEndPoint())!, httpMethod: .get) else { return }
            metric.start()
            
            let getStoriesAPI = PlaceService().getStories(userId: Helper().getUserId(), city: city, sort: sort, limit: nil, latitude: latitude, longitude: longitude, postType: postType, distance: distance, placeGroup: placeGroup)
            
            let getLiveStoriesListAPITrace = Performance.startTrace(name: "\(AppConstants.apiNames.getLiveStoriesList) API loading")
            getLiveStoriesListAPITrace?.start()
            
            getStoriesAPI.response.validate(statusCode: 200..<300).responseJSON { response in
//                debugPrint(response)
                
                if let httpCode = response.response?.statusCode {
                    metric.responseCode = httpCode
                }
                
                switch response.result {
                case .success:
                    guard let json = response.result.value as? [JSON] else {
                        LogUtil.error(response)
                        AnalyticsHelper.APIFailureEvent(screen: self.screen, apiName: AppConstants.apiNames.getLiveStoriesList, errorMessage: AppConstants.ErrorLog.jsonParse, userId: Helper().getUserId(), errorCode: nil, requestPayload: getStoriesAPI.requestPayload, requestMethod: getStoriesAPI.requestMethod.rawValue, requestPathParams: getStoriesAPI.requestPathParams)
                        
                        let errorObject = ErrorObject.init(apiName: AppConstants.apiNames.getLiveStoriesList, errorType: .jsonParsingError, httpCode: nil, errorTitle: AppConstants.ErrorMessages.opsSomethingWentWrong, errorDescription: AppConstants.ErrorMessages.serverBusyPleaseTryAgain)
                        completion(nil, .jsonParseError, nil, errorObject)
                        return
                    }
                    
                    if let stories: [StoryData] = [StoryData].from(jsonArray: json) {
                        completion(stories, .success, nil, nil)
                    } else {
                        LogUtil.error(response)
                        
                        AnalyticsHelper.APIFailureEvent(screen: self.screen, apiName: AppConstants.apiNames.getLiveStoriesList, errorMessage: AppConstants.ErrorMessages.emptyResponse, userId: Helper().getUserId(), errorCode: nil, requestPayload: getStoriesAPI.requestPayload, requestMethod: getStoriesAPI.requestMethod.rawValue, requestPathParams: getStoriesAPI.requestPathParams)
                        
                        let errorObject = ErrorObject.init(apiName: AppConstants.apiNames.getLiveStoriesList, errorType: .jsonParsingError, httpCode: nil, errorTitle: AppConstants.ErrorMessages.opsSomethingWentWrong, errorDescription: AppConstants.ErrorMessages.serverBusyPleaseTryAgain)
                        completion(nil, .jsonParseError, nil, errorObject)
                    }
                    
                case .failure(let error):
                    LogUtil.error(error)
                    
                    let error = error as NSError
                    
                    var errorObject: ErrorObject? = ErrorObject(apiName: AppConstants.apiNames.getLiveStoriesList, errorType: .unknown, httpCode: error.code, errorTitle: "", errorDescription: AppConstants.ErrorMessages.serverError)
                    
                    if let httpCode = response.response?.statusCode {
                        errorObject = APIErrorCodes.getErrorObject(apiName: AppConstants.apiNames.getLiveStoriesList, httpCode: httpCode)
                    }
                    
                    if let errorDesc = error.userInfo.valueForKeyPath(keyPath: "NSLocalizedDescription") as? String {
                        AnalyticsHelper.APIFailureEvent(screen: self.screen, apiName: AppConstants.apiNames.getLiveStoriesList, errorMessage: errorDesc, userId: Helper().getUserId(), errorCode: nil, requestPayload: getStoriesAPI.requestPayload, requestMethod: getStoriesAPI.requestMethod.rawValue, requestPathParams: getStoriesAPI.requestPathParams)
                    }
                    
                    completion(nil, .networkError, error, errorObject)
                }
                
                metric.stop()
                getLiveStoriesListAPITrace?.stop()
            }
        } else {
            let errorObject = ErrorObject.init(apiName: AppConstants.apiNames.getLiveStoriesList, errorType: .noNetwork, httpCode: nil, errorTitle: AppConstants.ErrorMessages.noInternet, errorDescription: AppConstants.ErrorMessages.noInterneSubHeading)
            
            completion(nil, .noInternet, nil, errorObject)
        }
    }
}
