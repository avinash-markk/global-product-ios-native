//
//  BannerDataClass.swift
//  Sminq
//
//  Created by Pushkar Deshmukh on 14/03/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import Foundation
import Firebase
import Gloss

class BannerDataClass {
    private(set) var screen = ""
    
    init?(screen: String) {
        self.screen = screen
    }
    
    func getBanners(completion: @escaping(_ bannersData: BannerData?, _ apiResponseState: APIResponseState, _ networkError: Error?) -> Void) {
        
        if InternetConnection.isConnectedToInternet() {
            guard let metric = HTTPMetric(url: URL(string: BannerService().getBannersAPIEndPoint())!, httpMethod: .get) else { return }
            metric.start()
            
            let getBannersAPI = BannerService().getBanners()
            
            let trace = Performance.startTrace(name: "\(AppConstants.apiNames.getTrends) API loading")
            trace?.start()
            
            getBannersAPI.response.validate(statusCode: 200..<300).responseJSON { response in
//               debugPrint(response)
                
                trace?.stop()
                
                if let httpCode = response.response?.statusCode {
                    metric.responseCode = httpCode
                }
                
                switch response.result {
                case .success:
                    guard let json = response.result.value as? JSON else {
                        LogUtil.error(response)
                        AnalyticsHelper.APIFailureEvent(screen: self.screen, apiName: AppConstants.apiNames.getTrends, errorMessage: AppConstants.ErrorLog.jsonParse, userId: Helper().getUserId(), errorCode: nil, requestPayload: getBannersAPI.requestPayload, requestMethod: getBannersAPI.requestMethod.rawValue, requestPathParams: getBannersAPI.requestPathParams)
                        completion(nil, .jsonParseError, nil)
                        return
                    }
                    
                    if let bannersData: BannerData = BannerData(json: json) {
                        completion(bannersData, .success, nil)
                    } else {
                        LogUtil.error(response)
                        
                        AnalyticsHelper.APIFailureEvent(screen: self.screen, apiName: AppConstants.apiNames.getTrends, errorMessage: AppConstants.ErrorMessages.emptyResponse, userId: Helper().getUserId(), errorCode: nil, requestPayload: getBannersAPI.requestPayload, requestMethod: getBannersAPI.requestMethod.rawValue, requestPathParams: getBannersAPI.requestPathParams)
                        
                        completion(nil, .jsonParseError, nil)
                    }
                case .failure(let error):
                    LogUtil.error(error)
                    
                    let error = error as NSError
                    let userInfo = error.userInfo
                    if let errorDesc = userInfo.valueForKeyPath(keyPath: "NSLocalizedDescription") as? String {
                        AnalyticsHelper.APIFailureEvent(screen: self.screen, apiName: AppConstants.apiNames.getTrends, errorMessage: errorDesc, userId: Helper().getUserId(), errorCode: nil, requestPayload: getBannersAPI.requestPayload, requestMethod: getBannersAPI.requestMethod.rawValue, requestPathParams: getBannersAPI.requestPathParams)
                    }
                    
                    completion(nil, .networkError, error)
                }
                
                metric.stop()
            }
        } else {
            completion(nil, .noInternet, nil)
        }
        
    }
}
