//
//  FeedDataClass.swift
//  Sminq
//
//  Created by Pushkar Deshmukh on 19/02/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import Foundation
import Gloss
import Firebase

class FeedDataClass {    
    private(set) var screen = ""
    
    init?(screen: String) {
        self.screen = screen
    }
    
    func upvoteFeed(feed: UserDataPlaceFeed, userId: String, status: Int, completion: @escaping (_ feedResponse: UserDataPlaceFeed?, _ apiResponseState: APIResponseState, _ networkError: Error?, _ error: ErrorObject?) -> Void) {
        
        if InternetConnection.isConnectedToInternet() {
            
            guard let upvoteMetric = HTTPMetric(url: URL(string: FeedService().upvoteAPIEndPoint())!, httpMethod: .post) else { return }
            upvoteMetric.start()
            
            let upvoteAPI = FeedService().upvote(feedItemsId: feed._id, userId: userId, status: status)
            
            upvoteAPI.response.validate(statusCode: 200..<300).responseJSON { response in
//                debugPrint(response)
                
                if let httpCode = response.response?.statusCode {
                    upvoteMetric.responseCode = httpCode
                }
                
                switch response.result {
                case .success:
                    guard let upvoteJson = response.result.value as? JSON else {
                        AnalyticsHelper.APIFailureEvent(screen: self.screen, apiName: AppConstants.apiNames.upvotePost, errorMessage: AppConstants.ErrorLog.jsonParse, userId: userId, errorCode: nil, requestPayload: upvoteAPI.requestPayload, requestMethod: upvoteAPI.requestMethod.rawValue, requestPathParams: upvoteAPI.requestPathParams)
                        
                        let errorObject = ErrorObject.init(apiName: AppConstants.apiNames.upvotePost, errorType: .jsonParsingError, httpCode: nil, errorTitle: AppConstants.ErrorMessages.opsSomethingWentWrong, errorDescription: AppConstants.ErrorMessages.serverBusyPleaseTryAgain)
                        completion(nil, .jsonParseError, nil, errorObject)
                        
                        return
                    }
                    
                    if let feedResponse: UserDataPlaceFeed = UserDataPlaceFeed(json: upvoteJson) {
                        completion(feedResponse, .success, nil, nil)
                    } else {
                        LogUtil.error(response)
                        
                        AnalyticsHelper.APIFailureEvent(screen: self.screen, apiName: AppConstants.apiNames.upvotePost, errorMessage: AppConstants.ErrorMessages.emptyResponse, userId: userId, errorCode: nil, requestPayload: upvoteAPI.requestPayload, requestMethod: upvoteAPI.requestMethod.rawValue, requestPathParams: upvoteAPI.requestPathParams)
                        
                        let errorObject = ErrorObject.init(apiName: AppConstants.apiNames.upvotePost, errorType: .jsonParsingError, httpCode: nil, errorTitle: AppConstants.ErrorMessages.opsSomethingWentWrong, errorDescription: AppConstants.ErrorMessages.serverBusyPleaseTryAgain)
                        completion(nil, .jsonParseError, nil, errorObject)
                    }
                    
                case .failure(let error):
                    LogUtil.error(error)
                    
                    let error = error as NSError
                    
                    var errorObject: ErrorObject? = ErrorObject(apiName: AppConstants.apiNames.upvotePost, errorType: .unknown, httpCode: error.code, errorTitle: "", errorDescription: AppConstants.ErrorMessages.serverError)
                    
                    if let httpCode = response.response?.statusCode {
                        errorObject = APIErrorCodes.getErrorObject(apiName: AppConstants.apiNames.upvotePost, httpCode: httpCode)
                    }
                    
                    if let errorDesc = error.userInfo.valueForKeyPath(keyPath: "NSLocalizedDescription") as? String {
                        AnalyticsHelper.APIFailureEvent(screen: self.screen, apiName: AppConstants.apiNames.upvotePost, errorMessage: errorDesc, userId: userId, errorCode: nil, requestPayload: upvoteAPI.requestPayload, requestMethod: upvoteAPI.requestMethod.rawValue, requestPathParams: upvoteAPI.requestPathParams)
                    }
                    
                    completion(nil, .networkError, error, errorObject)
                }
                
                upvoteMetric.stop()
            }
            
            
        } else {
            let errorObject = ErrorObject.init(apiName: AppConstants.apiNames.upvotePost, errorType: .noNetwork, httpCode: nil, errorTitle: AppConstants.ErrorMessages.noInternet, errorDescription: AppConstants.ErrorMessages.noInterneSubHeading)
            
            completion(nil, .noInternet, nil, errorObject)
        }
    }
    
    func downvoteFeed(feed: UserDataPlaceFeed, userId: String, status: Int, reason: String, completion: @escaping (_ feedResponse: UserDataPlaceFeed?, _ apiResponseState: APIResponseState, _ networkError: Error?, _ error: ErrorObject?) -> Void) {
        
        if InternetConnection.isConnectedToInternet() {
            guard let downvoteMetric = HTTPMetric(url: URL(string: FeedService().downvoteAPIEndPoint())!, httpMethod: .post) else { return }
            downvoteMetric.start()
            
            let downvoteAPI = FeedService().downvote(feedItemsId: feed._id, userId: userId, status: 1, reason: reason)
            
            downvoteAPI.response.validate(statusCode: 200..<300).responseJSON { response in
//                debugPrint(response)
                
                if let httpCode = response.response?.statusCode {
                    downvoteMetric.responseCode = httpCode
                }
                
                switch response.result {
                case .success:
                    guard let downvoteJson = response.result.value as? JSON else {
                        AnalyticsHelper.APIFailureEvent(screen: self.screen, apiName: AppConstants.apiNames.downvotePost, errorMessage: AppConstants.ErrorLog.jsonParse, userId: userId, errorCode: nil, requestPayload: downvoteAPI.requestPayload, requestMethod: downvoteAPI.requestMethod.rawValue, requestPathParams: downvoteAPI.requestPathParams)
                        
                        let errorObject = ErrorObject.init(apiName: AppConstants.apiNames.downvotePost, errorType: .jsonParsingError, httpCode: nil, errorTitle: AppConstants.ErrorMessages.opsSomethingWentWrong, errorDescription: AppConstants.ErrorMessages.serverBusyPleaseTryAgain)
                        completion(nil, .jsonParseError, nil, errorObject)
                        
                        return
                    }
                    
                    if let feedResponse: UserDataPlaceFeed = UserDataPlaceFeed(json: downvoteJson) {
                        completion(feedResponse, .success, nil, nil)
                    } else {
                        LogUtil.error(response)
                        
                        AnalyticsHelper.APIFailureEvent(screen: self.screen, apiName: AppConstants.apiNames.downvotePost, errorMessage: AppConstants.ErrorMessages.emptyResponse, userId: userId, errorCode: nil, requestPayload: downvoteAPI.requestPayload, requestMethod: downvoteAPI.requestMethod.rawValue, requestPathParams: downvoteAPI.requestPathParams)
                        
                        let errorObject = ErrorObject.init(apiName: AppConstants.apiNames.downvotePost, errorType: .jsonParsingError, httpCode: nil, errorTitle: AppConstants.ErrorMessages.opsSomethingWentWrong, errorDescription: AppConstants.ErrorMessages.serverBusyPleaseTryAgain)
                        completion(nil, .jsonParseError, nil, errorObject)
                    }
    
                case .failure(let error):
                    LogUtil.error(error)
                    
                    let error = error as NSError
                    
                    var errorObject: ErrorObject? = ErrorObject(apiName: AppConstants.apiNames.downvotePost, errorType: .unknown, httpCode: error.code, errorTitle: "", errorDescription: AppConstants.ErrorMessages.serverError)
                    
                    if let httpCode = response.response?.statusCode {
                        errorObject = APIErrorCodes.getErrorObject(apiName: AppConstants.apiNames.downvotePost, httpCode: httpCode)
                    }
                    
                    if let errorDesc = error.userInfo.valueForKeyPath(keyPath: "NSLocalizedDescription") as? String {
                        AnalyticsHelper.APIFailureEvent(screen: self.screen, apiName: AppConstants.apiNames.downvotePost, errorMessage: errorDesc, userId: userId, errorCode: nil, requestPayload: downvoteAPI.requestPayload, requestMethod: downvoteAPI.requestMethod.rawValue, requestPathParams: downvoteAPI.requestPathParams)
                    }
                    
                    completion(nil, .networkError, error, errorObject)
                }
                
                downvoteMetric.stop()
            }
        } else {
            let errorObject = ErrorObject.init(apiName: AppConstants.apiNames.downvotePost, errorType: .noNetwork, httpCode: nil, errorTitle: AppConstants.ErrorMessages.noInternet, errorDescription: AppConstants.ErrorMessages.noInterneSubHeading)
            
            completion(nil, .noInternet, nil, errorObject)
        }
    }
    
    func getStoryDetails(userId: String, placeId: String, viewerId: String, latitude: Double?, longitude: Double?, archive: Bool, completion: @escaping(_ storyData: StoryData?, _ apiResponseState: APIResponseState, _ networkError: Error?, _ error: ErrorObject?) -> Void) {
        if InternetConnection.isConnectedToInternet() {
            guard let metric = HTTPMetric(url: URL(string: PlaceService().getStoryAPIEndPoint(userId: userId))!, httpMethod: .get) else { return }
            metric.start()
            
            let getStoryAPI = PlaceService().getStory(userId: userId, placeId: placeId, viewerId: userId, latitude: latitude, longitude: longitude, archive: archive)
            
            let getStoryDetailsAPITrace = Performance.startTrace(name: "\(AppConstants.apiNames.getStoryDetails) API loading")
            getStoryDetailsAPITrace?.start()
            
            getStoryAPI.response.validate(statusCode: 200..<300).responseJSON { response in
//                debugPrint(response)
                
                if let httpCode = response.response?.statusCode {
                    metric.responseCode = httpCode
                }
                
                switch response.result {
                case .success:
                    guard let json = response.result.value as? JSON else {
                        LogUtil.error(response)
                        AnalyticsHelper.APIFailureEvent(screen: self.screen, apiName: AppConstants.apiNames.getStoryDetails, errorMessage: AppConstants.ErrorLog.jsonParse, userId: viewerId, errorCode: nil, requestPayload: getStoryAPI.requestPayload, requestMethod: getStoryAPI.requestMethod.rawValue, requestPathParams: getStoryAPI.requestPathParams)
                        
                        let errorObject = ErrorObject.init(apiName: AppConstants.apiNames.getStoryDetails, errorType: .jsonParsingError, httpCode: nil, errorTitle: AppConstants.ErrorMessages.opsSomethingWentWrong, errorDescription: AppConstants.ErrorMessages.serverBusyPleaseTryAgain)
                        completion(nil, .jsonParseError, nil, errorObject)
                        return
                    }
                    
                    if var storyData: StoryData = StoryData(json: json) {
                        storyData.posts = storyData.copyPlaceDetailsToPostsAndReturnList()
                        completion(storyData, .success, nil, nil)
                    } else {
                        LogUtil.error(response)
                        
                        AnalyticsHelper.APIFailureEvent(screen: self.screen, apiName: AppConstants.apiNames.getStoryDetails, errorMessage: AppConstants.ErrorMessages.emptyResponse, userId: viewerId, errorCode: nil, requestPayload: getStoryAPI.requestPayload, requestMethod: getStoryAPI.requestMethod.rawValue, requestPathParams: getStoryAPI.requestPathParams)
                        
                        let errorObject = ErrorObject.init(apiName: AppConstants.apiNames.getStoryDetails, errorType: .jsonParsingError, httpCode: nil, errorTitle: AppConstants.ErrorMessages.opsSomethingWentWrong, errorDescription: AppConstants.ErrorMessages.serverBusyPleaseTryAgain)
                        completion(nil, .jsonParseError, nil, errorObject)
                    }
                    
                case .failure(let error):
                    LogUtil.error(error)
                    
                    let error = error as NSError
                    
                    var errorObject: ErrorObject? = ErrorObject(apiName: AppConstants.apiNames.getStoryDetails, errorType: .unknown, httpCode: error.code, errorTitle: "", errorDescription: AppConstants.ErrorMessages.serverError)
                    
                    if let httpCode = response.response?.statusCode {
                        errorObject = APIErrorCodes.getErrorObject(apiName: AppConstants.apiNames.getStoryDetails, httpCode: httpCode)
                    }
                    
                    if let errorDesc = error.userInfo.valueForKeyPath(keyPath: "NSLocalizedDescription") as? String {
                        AnalyticsHelper.APIFailureEvent(screen: self.screen, apiName: AppConstants.apiNames.getStoryDetails, errorMessage: errorDesc, userId: viewerId, errorCode: nil, requestPayload: getStoryAPI.requestPayload, requestMethod: getStoryAPI.requestMethod.rawValue, requestPathParams: getStoryAPI.requestPathParams)
                    }
                    
                    completion(nil, .networkError, error, errorObject)
                }
                
                metric.stop()
                getStoryDetailsAPITrace?.stop()
            }
        } else {
            let errorObject = ErrorObject.init(apiName: AppConstants.apiNames.getStoryDetails, errorType: .noNetwork, httpCode: nil, errorTitle: AppConstants.ErrorMessages.noInternet, errorDescription: AppConstants.ErrorMessages.noInterneSubHeading)
            
            completion(nil, .noInternet, nil, errorObject)
        }
        
    }
    
    func deleteFeed(feed: UserDataPlaceFeed, completion: @escaping(_ feedResponse: UserDataPlaceFeed?, _ apiResponseState: APIResponseState, _ networkError: Error?, _ error: ErrorObject?) -> Void) {
        if InternetConnection.isConnectedToInternet() {
            guard let metric = HTTPMetric(url: URL(string: FeedService().deleteFeedAPIEndPoint())!, httpMethod: .delete) else { return }
            metric.start()
            
            let deleteFeedAPI = FeedService().deleteFeed(feedItemsId: feed._id)
            
            deleteFeedAPI.response.validate(statusCode: 200..<300).responseJSON { response in
//                debugPrint(response)
                
                if let httpCode = response.response?.statusCode {
                    metric.responseCode = httpCode
                }
                
                switch response.result {
                case .success:
                    guard let json = response.result.value as? JSON else {
                        LogUtil.error(response)
                        
                        AnalyticsHelper.APIFailureEvent(screen: self.screen, apiName: AppConstants.apiNames.deletePost, errorMessage: AppConstants.ErrorLog.jsonParse, userId: Helper().getUserId(), errorCode: nil, requestPayload: deleteFeedAPI.requestPayload, requestMethod: deleteFeedAPI.requestMethod.rawValue, requestPathParams: deleteFeedAPI.requestPathParams)
                        
                        let errorObject = ErrorObject.init(apiName: AppConstants.apiNames.deletePost, errorType: .jsonParsingError, httpCode: nil, errorTitle: AppConstants.ErrorMessages.opsSomethingWentWrong, errorDescription: AppConstants.ErrorMessages.serverBusyPleaseTryAgain)
                        completion(nil, .jsonParseError, nil, errorObject)
                        return
                    }
                    
                    if let deleteFeedData: UserDataPlaceFeed = UserDataPlaceFeed(json: json) {
                        completion(deleteFeedData, .success, nil, nil)
                    } else {
                        LogUtil.error(response)
                        
                        AnalyticsHelper.APIFailureEvent(screen: self.screen, apiName: AppConstants.apiNames.deletePost, errorMessage: AppConstants.ErrorMessages.emptyResponse, userId: Helper().getUserId(), errorCode: nil, requestPayload: deleteFeedAPI.requestPayload, requestMethod: deleteFeedAPI.requestMethod.rawValue, requestPathParams: deleteFeedAPI.requestPathParams)
                        
                        let errorObject = ErrorObject.init(apiName: AppConstants.apiNames.deletePost, errorType: .jsonParsingError, httpCode: nil, errorTitle: AppConstants.ErrorMessages.opsSomethingWentWrong, errorDescription: AppConstants.ErrorMessages.serverBusyPleaseTryAgain)
                        completion(nil, .jsonParseError, nil, errorObject)
                    }
                    
                case .failure(let error):
                    LogUtil.error(error)
                    
                    let error = error as NSError
                    
                    var errorObject: ErrorObject? = ErrorObject(apiName: AppConstants.apiNames.deletePost, errorType: .unknown, httpCode: error.code, errorTitle: "", errorDescription: AppConstants.ErrorMessages.serverError)
                    
                    if let httpCode = response.response?.statusCode {
                        errorObject = APIErrorCodes.getErrorObject(apiName: AppConstants.apiNames.downvotePost, httpCode: httpCode)
                    }
                    
                    if let errorDesc = error.userInfo.valueForKeyPath(keyPath: "NSLocalizedDescription") as? String {
                        AnalyticsHelper.APIFailureEvent(screen: self.screen, apiName: AppConstants.apiNames.deletePost, errorMessage: errorDesc, userId: Helper().getUserId(), errorCode: nil, requestPayload: deleteFeedAPI.requestPayload, requestMethod: deleteFeedAPI.requestMethod.rawValue, requestPathParams: deleteFeedAPI.requestPathParams)
                    }
                    
                    completion(nil, .networkError, error, errorObject)
                }
                
                metric.stop()
            }
        } else {
            let errorObject = ErrorObject.init(apiName: AppConstants.apiNames.deletePost, errorType: .noNetwork, httpCode: nil, errorTitle: AppConstants.ErrorMessages.noInternet, errorDescription: AppConstants.ErrorMessages.noInterneSubHeading)
            
            completion(nil, .noInternet, nil, errorObject)
        }
    }
    
    func shareFeed(feed: UserDataPlaceFeed, completion: @escaping (_ shareFeedResponse: UserDataPlaceFeed?, _ apiResponseState: APIResponseState, _ networkError: Error?, _ error: ErrorObject?) -> Void) {
        if InternetConnection.isConnectedToInternet() {
            guard let metric = HTTPMetric(url: URL(string: FeedService().shareFeedItemAPIEndPoint())!, httpMethod: .post) else { return }
            metric.start()
            
            let shareFeedItemAPI = FeedService().shareFeedItem(userId: Helper().getUserId(), medium: "", feedItemsId: feed._id)
            
            shareFeedItemAPI.response.validate(statusCode: 200..<300).responseJSON { response in
                debugPrint(response)
                
                if let httpCode = response.response?.statusCode {
                    metric.responseCode = httpCode
                }
                
                switch response.result {
                case .success:
                    guard let json = response.result.value as? JSON else {
                        LogUtil.error(response)
                        
                        AnalyticsHelper.APIFailureEvent(screen: self.screen, apiName: AppConstants.apiNames.sharePost, errorMessage: AppConstants.ErrorLog.jsonParse, userId: Helper().getUserId(), errorCode: nil, requestPayload: shareFeedItemAPI.requestPayload, requestMethod: shareFeedItemAPI.requestMethod.rawValue, requestPathParams: shareFeedItemAPI.requestPathParams)
                        
                        let errorObject = ErrorObject.init(apiName: AppConstants.apiNames.sharePost, errorType: .jsonParsingError, httpCode: nil, errorTitle: AppConstants.ErrorMessages.opsSomethingWentWrong, errorDescription: AppConstants.ErrorMessages.serverBusyPleaseTryAgain)
                        completion(nil, .jsonParseError, nil, errorObject)
                        return
                    }
                    
                    if let shareFeedResponse: UserDataPlaceFeed = UserDataPlaceFeed(json: json) {
                        completion(shareFeedResponse, .success, nil, nil)
                    } else {
                        LogUtil.error(response)
                        AnalyticsHelper.APIFailureEvent(screen: self.screen, apiName: AppConstants.apiNames.sharePost, errorMessage: AppConstants.ErrorMessages.emptyResponse, userId: Helper().getUserId(), errorCode: nil, requestPayload: shareFeedItemAPI.requestPayload, requestMethod: shareFeedItemAPI.requestMethod.rawValue, requestPathParams: shareFeedItemAPI.requestPathParams)
                        
                        let errorObject = ErrorObject.init(apiName: AppConstants.apiNames.sharePost, errorType: .jsonParsingError, httpCode: nil, errorTitle: AppConstants.ErrorMessages.opsSomethingWentWrong, errorDescription: AppConstants.ErrorMessages.serverBusyPleaseTryAgain)
                        completion(nil, .jsonParseError, nil, errorObject)
                    }
                    
                case .failure(let error):
                    LogUtil.error(error)
                    
                    let error = error as NSError
                    
                    var errorObject: ErrorObject? = ErrorObject(apiName: AppConstants.apiNames.sharePost, errorType: .unknown, httpCode: error.code, errorTitle: "", errorDescription: AppConstants.ErrorMessages.serverError)
                    
                    if let httpCode = response.response?.statusCode {
                        errorObject = APIErrorCodes.getErrorObject(apiName: AppConstants.apiNames.sharePost, httpCode: httpCode)
                    }
                    
                    if let errorDesc = error.userInfo.valueForKeyPath(keyPath: "NSLocalizedDescription") as? String {
                        AnalyticsHelper.APIFailureEvent(screen: self.screen, apiName: AppConstants.apiNames.sharePost, errorMessage: errorDesc, userId: Helper().getUserId(), errorCode: nil, requestPayload: shareFeedItemAPI.requestPayload, requestMethod: shareFeedItemAPI.requestMethod.rawValue, requestPathParams: shareFeedItemAPI.requestPathParams)
                    }
                    
                    completion(nil, .networkError, error, errorObject)
                }
                
                metric.stop()
            }
        } else {
            let errorObject = ErrorObject.init(apiName: AppConstants.apiNames.sharePost, errorType: .noNetwork, httpCode: nil, errorTitle: AppConstants.ErrorMessages.noInternet, errorDescription: AppConstants.ErrorMessages.noInterneSubHeading)
            
            completion(nil, .noInternet, nil, errorObject)
        }
    }
    
    func addComment(feed: UserDataPlaceFeed, input: String, completion: @escaping (_ reply: Replies?, _ apiResponseState: APIResponseState, _ networkError: Error?, _ error: ErrorObject?) -> Void) {
        if InternetConnection.isConnectedToInternet() {

            guard let metric = HTTPMetric(url: URL(string: FeedService().replyOnFeedAPIEndPoint())!, httpMethod: .post) else { return }
            metric.start()
            
            let replyOnFeedAPI = FeedService().replyOnFeed(feedItemsId: feed._id, userId: Helper().getUserId(), defaultSubPlace: "", input: input, tags: [])
            
            replyOnFeedAPI.response.validate(statusCode: 200..<300).responseJSON { response in
                if let httpCode = response.response?.statusCode {
                    metric.responseCode = httpCode
                }
                
                switch response.result {
                case .success:
                    guard let json = response.result.value as? JSON else {
                        LogUtil.error(response)
                        
                        AnalyticsHelper.APIFailureEvent(screen: self.screen, apiName: AppConstants.apiNames.addComment, errorMessage: AppConstants.ErrorLog.jsonParse, userId: Helper().getUserId(), errorCode: nil, requestPayload: replyOnFeedAPI.requestPayload, requestMethod: replyOnFeedAPI.requestMethod.rawValue, requestPathParams: replyOnFeedAPI.requestPathParams)
                        
                        let errorObject = ErrorObject.init(apiName: AppConstants.apiNames.addComment, errorType: .jsonParsingError, httpCode: nil, errorTitle: AppConstants.ErrorMessages.opsSomethingWentWrong, errorDescription: AppConstants.ErrorMessages.serverBusyPleaseTryAgain)
                        completion(nil, .jsonParseError, nil, errorObject)
                        return
                    }
                    
                    if let reply: Replies = Replies(json: json) {
                        completion(reply, .success, nil, nil)
                    } else {
                        LogUtil.error(response)
                        AnalyticsHelper.APIFailureEvent(screen: self.screen, apiName: AppConstants.apiNames.addComment, errorMessage: AppConstants.ErrorMessages.emptyResponse, userId: Helper().getUserId(), errorCode: nil, requestPayload: replyOnFeedAPI.requestPayload, requestMethod: replyOnFeedAPI.requestMethod.rawValue, requestPathParams: replyOnFeedAPI.requestPathParams)
                        
                        let errorObject = ErrorObject.init(apiName: AppConstants.apiNames.addComment, errorType: .jsonParsingError, httpCode: nil, errorTitle: AppConstants.ErrorMessages.opsSomethingWentWrong, errorDescription: AppConstants.ErrorMessages.serverBusyPleaseTryAgain)
                        completion(nil, .jsonParseError, nil, errorObject)
                    }
                    
                case .failure(let error):
                    LogUtil.error(error)
                    
                    let error = error as NSError
                    
                    var errorObject: ErrorObject? = ErrorObject(apiName: AppConstants.apiNames.addComment, errorType: .unknown, httpCode: error.code, errorTitle: "", errorDescription: AppConstants.ErrorMessages.serverError)
                    
                    if let httpCode = response.response?.statusCode {
                        errorObject = APIErrorCodes.getErrorObject(apiName: AppConstants.apiNames.addComment, httpCode: httpCode)
                    }
                    
                    if let errorDesc = error.userInfo.valueForKeyPath(keyPath: "NSLocalizedDescription") as? String {
                        AnalyticsHelper.APIFailureEvent(screen: self.screen, apiName: AppConstants.apiNames.addComment, errorMessage: errorDesc, userId: Helper().getUserId(), errorCode: nil, requestPayload: replyOnFeedAPI.requestPayload, requestMethod: replyOnFeedAPI.requestMethod.rawValue, requestPathParams: replyOnFeedAPI.requestPathParams)
                    }
                    
                    completion(nil, .networkError, error, errorObject)
                }
                
                metric.stop()
            }
        } else {
            let errorObject = ErrorObject.init(apiName: AppConstants.apiNames.addComment, errorType: .noNetwork, httpCode: nil, errorTitle: AppConstants.ErrorMessages.noInternet, errorDescription: AppConstants.ErrorMessages.noInterneSubHeading)
            
            completion(nil, .noInternet, nil, errorObject)
        }
    }
    
    func deleteComment(feed: UserDataPlaceFeed, commentToDelete: Replies, completion: @escaping (_ feed: UserDataPlaceFeed?, _ apiResponseState: APIResponseState, _ networkError: Error?, _ error: ErrorObject?) -> Void) {
        if InternetConnection.isConnectedToInternet() {
            guard let metric = HTTPMetric(url: URL(string: FeedService().deleteCommentAPIEndPoint())!, httpMethod: .delete) else { return }
            metric.start()
            
            let deleteCommentAPI = FeedService().deleteComment(feedItemsId: feed._id, replyId: commentToDelete._id, userId: Helper().getUserId())
            
            deleteCommentAPI.response.validate(statusCode: 200..<300).responseJSON { response in
//                debugPrint(response)
                
                if let httpCode = response.response?.statusCode {
                    metric.responseCode = httpCode
                }
                
                switch response.result {
                case .success:
                    guard let json = response.result.value as? JSON else {
                        LogUtil.error(response)
                        
                        AnalyticsHelper.APIFailureEvent(screen: self.screen, apiName: AppConstants.apiNames.deleteComment, errorMessage: AppConstants.ErrorLog.jsonParse, userId: Helper().getUserId(), errorCode: nil, requestPayload: deleteCommentAPI.requestPayload, requestMethod: deleteCommentAPI.requestMethod.rawValue, requestPathParams: deleteCommentAPI.requestPathParams)
                        
                        let errorObject = ErrorObject.init(apiName: AppConstants.apiNames.deleteComment, errorType: .jsonParsingError, httpCode: nil, errorTitle: AppConstants.ErrorMessages.opsSomethingWentWrong, errorDescription: AppConstants.ErrorMessages.serverBusyPleaseTryAgain)
                        completion(nil, .jsonParseError, nil, errorObject)
                        return
                    }
                    
                    if let feed: UserDataPlaceFeed = UserDataPlaceFeed(json: json) {
                        completion(feed, .success, nil, nil)
                    } else {
                        LogUtil.error(response)
                        AnalyticsHelper.APIFailureEvent(screen: self.screen, apiName: AppConstants.apiNames.deleteComment, errorMessage: AppConstants.ErrorMessages.emptyResponse, userId: Helper().getUserId(), errorCode: nil, requestPayload: deleteCommentAPI.requestPayload, requestMethod: deleteCommentAPI.requestMethod.rawValue, requestPathParams: deleteCommentAPI.requestPathParams)
                        
                        let errorObject = ErrorObject.init(apiName: AppConstants.apiNames.deleteComment, errorType: .jsonParsingError, httpCode: nil, errorTitle: AppConstants.ErrorMessages.opsSomethingWentWrong, errorDescription: AppConstants.ErrorMessages.serverBusyPleaseTryAgain)
                        completion(nil, .jsonParseError, nil, errorObject)
                    }
                    
                case .failure(let error):
                    LogUtil.error(error)
                    
                    let error = error as NSError
                    
                    var errorObject: ErrorObject? = ErrorObject(apiName: AppConstants.apiNames.deleteComment, errorType: .unknown, httpCode: error.code, errorTitle: "", errorDescription: AppConstants.ErrorMessages.serverError)
                    
                    if let httpCode = response.response?.statusCode {
                        errorObject = APIErrorCodes.getErrorObject(apiName: AppConstants.apiNames.deleteComment, httpCode: httpCode)
                    }
                    
                    if let errorDesc = error.userInfo.valueForKeyPath(keyPath: "NSLocalizedDescription") as? String {
                        AnalyticsHelper.APIFailureEvent(screen: self.screen, apiName: AppConstants.apiNames.deleteComment, errorMessage: errorDesc, userId: Helper().getUserId(), errorCode: nil, requestPayload: deleteCommentAPI.requestPayload, requestMethod: deleteCommentAPI.requestMethod.rawValue, requestPathParams: deleteCommentAPI.requestPathParams)
                    }
                    
                    completion(nil, .networkError, error, errorObject)
                }
                
                metric.stop()
            }
            
        } else {
            let errorObject = ErrorObject.init(apiName: AppConstants.apiNames.deleteComment, errorType: .noNetwork, httpCode: nil, errorTitle: AppConstants.ErrorMessages.noInternet, errorDescription: AppConstants.ErrorMessages.noInterneSubHeading)
            
            completion(nil, .noInternet, nil, errorObject)
        }
    }
}

