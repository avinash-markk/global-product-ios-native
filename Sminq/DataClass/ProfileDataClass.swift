//
//  ProfileDataClass.swift
//  Sminq
//
//  Created by Avinash Thakur on 06/03/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import UIKit
import Firebase
import Gloss
import Alamofire

class ProfileDataClass {
    let profileCacheKey = "ProfileCache"
    let cache = DataCacheService()
    var isMyProfile: Bool = false
    
    func getUserProfileDetails(userDisplayName: String, enableCache: Bool, completion: @escaping (_ result: UserProfileData?, _ apiResponseState: APIResponseState, _ networkError: Error?, _ error: ErrorObject?) -> Void) {
        
        var latitude = 0.0
        var longitude = 0.0
        
        if self.isMyProfile && enableCache {
            if let data = cache.loadDataFromCache(cacheKey: profileCacheKey) {
                if let userProfile = try? PropertyListDecoder().decode(UserProfileData.self, from: data) {
                    completion(userProfile, .success, nil, nil)
                }
            }
        }
        
        if let userLocation = UserDefaults.standard.value(forKey: "UserLocation") {
            latitude = (userLocation as AnyObject).value(forKey: "latitude") as? Double ?? 0.0
            longitude = (userLocation as AnyObject).value(forKey: "longitude") as? Double ?? 0.0
        }
        
        if InternetConnection.isConnectedToInternet() {
            var getUserDetailsAPI = UserService().getUserDetails(userDisplayName: userDisplayName, latitude: latitude, longitude: longitude)
            var apiName = ""
            
            var metric: HTTPMetric? = nil
            
            if self.isMyProfile {
                metric = HTTPMetric(url: URL(string: UserService().getOwnUserDetailsAPIEndPoint())!, httpMethod: .get)
                metric?.start()
                
                getUserDetailsAPI = UserService().getOwnUserDetails()
                apiName = AppConstants.apiNames.getOwnUserDetails
            } else {
                metric = HTTPMetric(url: URL(string: UserService().getUserDetailsAPIEndPoint(user: userDisplayName))!, httpMethod: .get)
                metric?.start()
                
                apiName = AppConstants.apiNames.getUserDetails
            }
            
            getUserDetailsAPI.response.validate(statusCode: 200..<300).responseJSON { response in
//                debugPrint(response)
                
                switch response.result {
                case .success:
                    
                    guard let json = response.result.value as? JSON else { return }
                    
                    guard var userProfileData: UserProfileData = UserProfileData(json: json) else {
                        LogUtil.error(response)
                        AnalyticsHelper.APIFailureEvent(screen: AppConstants.screenNames.profile, apiName: AppConstants.apiNames.getUserDetails, errorMessage: AppConstants.ErrorMessages.emptyResponse, userId: Helper().getUserId(), errorCode: nil, requestPayload: getUserDetailsAPI.requestPayload, requestMethod: getUserDetailsAPI.requestMethod.rawValue, requestPathParams: getUserDetailsAPI.requestPathParams)
                        
                        let errorObject = ErrorObject.init(apiName: apiName, errorType: .jsonParsingError, httpCode: nil, errorTitle: AppConstants.ErrorMessages.opsSomethingWentWrong, errorDescription: AppConstants.ErrorMessages.serverBusyPleaseTryAgain)
                        completion(nil, .jsonParseError, nil, errorObject)
                        return
                    }
                    
                    for (index, story) in userProfileData.stories.enumerated() {
                        userProfileData.stories[index].posts = story.copyPlaceDetailsToPostsAndReturnList()
                    }
                    
                    if self.isMyProfile && enableCache {
                        self.saveProfileData(userProfileData: userProfileData)
                    }
                    
                    completion(userProfileData, .success, nil, nil)
                    
                case .failure(let error):
                    LogUtil.error(error)
                    
                    let error = error as NSError
                    
                    var errorObject: ErrorObject? = ErrorObject(apiName: apiName, errorType: .unknown, httpCode: error.code, errorTitle: "", errorDescription: AppConstants.ErrorMessages.serverError)
                    
                    if let httpCode = response.response?.statusCode {
                        errorObject = APIErrorCodes.getErrorObject(apiName: apiName, httpCode: httpCode)
                    }
                    
                    let userInfo = error.userInfo
                    if let errorDesc = userInfo.valueForKeyPath(keyPath: "NSLocalizedDescription") as? String {
                        AnalyticsHelper.APIFailureEvent(screen: AppConstants.screenNames.profile, apiName: apiName, errorMessage: errorDesc, userId: Helper().getUserId(), errorCode: nil, requestPayload: getUserDetailsAPI.requestPayload, requestMethod: getUserDetailsAPI.requestMethod.rawValue, requestPathParams: getUserDetailsAPI.requestPathParams)
                    }
                    
                    completion(nil, .apiError, error, errorObject)
                    break
                }
                metric?.stop()
            }
        } else {
            let errorObject = ErrorObject.init(apiName: AppConstants.apiNames.getUserDetails, errorType: .noNetwork, httpCode: nil, errorTitle: AppConstants.ErrorMessages.noInternet, errorDescription: AppConstants.ErrorMessages.noInterneSubHeading)
            
            completion(nil, .noInternet, nil, errorObject)
        }
        
    }
    
    func getProfileCachedData() -> UserProfileData? {
        if let data = cache.loadDataFromCache(cacheKey: profileCacheKey) {
            if let userProfile = try? PropertyListDecoder().decode(UserProfileData.self, from: data) {
                return userProfile
            }
        }
        
        return nil
    }
    
    func saveProfileData(userProfileData: UserProfileData) {
        if let data = try? PropertyListEncoder().encode(userProfileData) {
            self.cache.saveDataIntoCache(cacheKey: self.profileCacheKey, cacheData: data)
        }
    }
    
    func getCompletedLevel() -> Levels? {
        var completedLevel: Levels?
        
        if let userProfileData = getProfileCachedData() {
            
            userProfileData.levelsList.forEach { (mainLevel) in
                mainLevel.levels.forEach({ (level) in
                    if level.completed {
                        completedLevel = level
                    }
                })
            }
        }
        
        return completedLevel
    }
    
    func claimUserReward(userID: String, emailID: String, completion: @escaping (_ result: UserProfileData?, _ apiResponseState: APIResponseState, _ networkError: Error?, _ error: ErrorObject?) -> Void) {
        if InternetConnection.isConnectedToInternet() {
            let claimRewardsApi = UserService().claimRewards(userID: userID, emailID: emailID)
            claimRewardsApi.response.validate(statusCode: 200..<300).responseJSON { response in
                switch response.result {
                case .success: completion(nil, .success, nil, nil)
                case .failure(let error):
                    LogUtil.error(error)
                    let error = error as NSError
                    
                    var errorObject: ErrorObject? = ErrorObject(apiName: AppConstants.apiNames.claimRewards, errorType: .unknown, httpCode: error.code, errorTitle: "", errorDescription: AppConstants.ErrorMessages.serverError)
                    
                    if let httpCode = response.response?.statusCode {
                        errorObject = APIErrorCodes.getErrorObject(apiName: AppConstants.apiNames.claimRewards, httpCode: httpCode)
                    }
                    
                    let userInfo = error.userInfo
                    if let errorDesc = userInfo.valueForKeyPath(keyPath: "NSLocalizedDescription") as? String {
                        AnalyticsHelper.APIFailureEvent(screen: AppConstants.screenNames.profile, apiName: AppConstants.apiNames.claimRewards, errorMessage: errorDesc, userId: Helper().getUserId(), errorCode: nil, requestPayload: claimRewardsApi.requestPayload, requestMethod: claimRewardsApi.requestMethod.rawValue, requestPathParams: claimRewardsApi.requestPathParams)
                    }
                    
                    completion(nil, .apiError, error, errorObject)
                }
            }
        } else {
            let errorObject = ErrorObject.init(apiName: AppConstants.apiNames.claimRewards, errorType: .noNetwork, httpCode: nil, errorTitle: AppConstants.ErrorMessages.noInternet, errorDescription: AppConstants.ErrorMessages.noInterneSubHeading)
            
            completion(nil, .noInternet, nil, errorObject)
        }
    }
    
    func getUserUnlockedLevel(completion: @escaping (_ result: [LevelsList]?, _ apiResponseState: APIResponseState, _ networkError: Error?, _ error: ErrorObject?) -> Void) {
        
        if InternetConnection.isConnectedToInternet() {
            let getUserUnlockedLevelsAPI = UserService().getUserUnlockedLevels()
            getUserUnlockedLevelsAPI.response.responseJSON { response in
                debugPrint(response)
                guard let json = response.result.value as? [JSON] else { return }
                let levelList: [LevelsList] = [LevelsList].from(jsonArray: json) ?? []
                completion(levelList, .success, nil, nil)
            }
        } else {
            let errorObject = ErrorObject.init(apiName: AppConstants.apiNames.getUnlockedLevel, errorType: .noNetwork, httpCode: nil, errorTitle: AppConstants.ErrorMessages.noInternet, errorDescription: AppConstants.ErrorMessages.noInterneSubHeading)
            completion(nil, .noInternet, nil, errorObject)
        }
    }
    
    
}
