//
//  StoryViewTrackDataClass.swift
//  Sminq
//
//  Created by Pushkar Deshmukh on 22/02/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import Foundation
import Firebase
import Gloss

class StoryViewTrackDataClass {
    private(set) var screen = ""
    
    init?(screen: String) {
        self.screen = screen
    }
    
    func postStoryViewMetrics(storyViewList: [StoryViewData], completion: @escaping(_ apiResponseState: APIResponseState, _ networkError: Error?) -> Void) {
        if InternetConnection.isConnectedToInternet() {
            guard let metric = HTTPMetric(url: URL(string: FeedService().postMetricsAPIEndPoint())!, httpMethod: .post) else { return }
            metric.start()
            
            let postMetricsAPI = FeedService().postMetrics(storyViewList: storyViewList)
            
            postMetricsAPI.response.validate(statusCode: 200..<300).responseJSON { response in
//                debugPrint(response)
                
                if let httpCode = response.response?.statusCode {
                    metric.responseCode = httpCode
                }
                
                switch response.result {
                case .success:
                    guard let _ = response.result.value as? JSON else {
                         AnalyticsHelper.APIFailureEvent(screen: self.screen, apiName: AppConstants.apiNames.viewPostCount, errorMessage: AppConstants.ErrorLog.jsonParse, userId: Helper().getUserId(), errorCode: nil, requestPayload: postMetricsAPI.requestPayload, requestMethod: postMetricsAPI.requestMethod.rawValue, requestPathParams: postMetricsAPI.requestPathParams)
                        
                        completion(.jsonParseError, nil)
                        return
                        
                    }
                    
                    completion(.success, nil)
                    
                case .failure(let error):
                    LogUtil.error(error)
                    
                    let error = error as NSError
                    let userInfo = error.userInfo
                    if let errorDesc = userInfo.valueForKeyPath(keyPath: "NSLocalizedDescription") as? String {
                        AnalyticsHelper.APIFailureEvent(screen: self.screen, apiName: AppConstants.apiNames.viewPostCount, errorMessage: errorDesc, userId: Helper().getUserId(), errorCode: nil, requestPayload: postMetricsAPI.requestPayload, requestMethod: postMetricsAPI.requestMethod.rawValue, requestPathParams: postMetricsAPI.requestPathParams)
                    }
                    
                    completion(.networkError, error)
                }
                
                metric.stop()
            }
        } else {
            completion(.noInternet, nil)
        }
    }
}

