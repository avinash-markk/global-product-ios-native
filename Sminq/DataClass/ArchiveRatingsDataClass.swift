//
//  ArchiveRatingsDataClass.swift
//  Markk
//
//  Created by Avinash Thakur on 20/06/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import Gloss
import Firebase

class ArchiveRatingsDataClass {
    
    let archiveCacheKey = "ArchiveCache"
    let cache = DataCacheService()
    var displayName = ""
    
    func getArchivedRatings(displayName: String, completion: @escaping (_ result: [ArchiveRatingsData]?, _ apiResponseState: APIResponseState, _ networkError: Error?, _ error: ErrorObject?) -> Void) {
        
        self.displayName = displayName
        if let archivesData = self.getArchivesCacheData() {
            completion(archivesData, .success, nil, nil)
        }
        
        let apiName = AppConstants.apiNames.getUserArchivePosts
        if InternetConnection.isConnectedToInternet() {
            let getArchiveRatingsAPI = UserService().getArchivedStories(displayName: displayName)
        
            let metric = HTTPMetric(url: URL(string: UserService().getArchivedStoriesAPIEndPoint())!, httpMethod: .get)
            metric?.start()
            
            getArchiveRatingsAPI.response.validate(statusCode: 200..<300).responseJSON { response in
//                debugPrint(response)
                switch response.result {
                case .success:
                    guard let json = response.result.value as? [JSON] else {
                        return
                    }
                    
                    if let archives: [ArchiveRatingsData] = [ArchiveRatingsData].from(jsonArray: json) {
                        self.saveArchivesData(archivesData: archives)
                        completion(archives, .success, nil, nil)
                    } else {
                        let errorObject = ErrorObject.init(apiName: apiName, errorType: .jsonParsingError, httpCode: nil, errorTitle: AppConstants.ErrorMessages.opsSomethingWentWrong, errorDescription: AppConstants.ErrorMessages.serverBusyPleaseTryAgain)
                        completion(nil, .jsonParseError, nil, errorObject)
                        return
                    }
                case .failure(let error):
                    LogUtil.error(error)
                    
                    let error = error as NSError
                    
                    var errorObject: ErrorObject? = ErrorObject(apiName: apiName, errorType: .unknown, httpCode: error.code, errorTitle: "", errorDescription: AppConstants.ErrorMessages.serverError)
                    
                    if let httpCode = response.response?.statusCode {
                        errorObject = APIErrorCodes.getErrorObject(apiName: apiName, httpCode: httpCode)
                    }
                    
                    let userInfo = error.userInfo
                    if let errorDesc = userInfo.valueForKeyPath(keyPath: "NSLocalizedDescription") as? String {
                        AnalyticsHelper.APIFailureEvent(screen: AppConstants.screenNames.profile, apiName: apiName, errorMessage: errorDesc, userId: Helper().getUserId(), errorCode: nil, requestPayload: getArchiveRatingsAPI.requestPayload, requestMethod: getArchiveRatingsAPI.requestMethod.rawValue, requestPathParams: getArchiveRatingsAPI.requestPathParams)
                    }
                    
                    completion(nil, .apiError, error, errorObject)
                    break
                }
                metric?.stop()
            }
        } else {
            let errorObject = ErrorObject.init(apiName: apiName, errorType: .noNetwork, httpCode: nil, errorTitle: AppConstants.ErrorMessages.noInternet, errorDescription: AppConstants.ErrorMessages.noInterneSubHeading)
            completion(nil, .noInternet, nil, errorObject)
        }
    }
    
    func getArchivesCacheData() -> [ArchiveRatingsData]? {
        if self.displayName == Helper().getUserDisplayName() {
            if let data = cache.loadDataFromCache(cacheKey: archiveCacheKey) {
                if let archives = try? PropertyListDecoder().decode([ArchiveRatingsData].self, from: data) {
                    return archives
                }
            }
        }
        return nil
    }
    
    func saveArchivesData(archivesData: [ArchiveRatingsData]) {
        if let data = try? PropertyListEncoder().encode(archivesData) {
            self.cache.saveDataIntoCache(cacheKey: self.archiveCacheKey, cacheData: data)
        }
    }
    
}
