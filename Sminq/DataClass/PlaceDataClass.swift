//
//  PlaceDataClass.swift
//  Sminq
//
//  Created by Pushkar Deshmukh on 11/03/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import Foundation
import Firebase
import Gloss

class PlaceDataClass {
    private(set) var screen = ""
    let nearByStoriesCache = "NearByStoriesCache"
    let cache = DataCacheService()
    
    init?(screen: String) {
        self.screen = screen
    }
    
    func getTimeline(latitude: Double, longitude: Double, distance: String, enableCache: Bool, completion: @escaping(_ placesData: [PlacesStoriesData]?, _ apiResponseState: APIResponseState, _ networkError: Error?, _ error: ErrorObject?) -> Void) {
        
        if enableCache {
            if let data = cache.loadDataFromCache(cacheKey: nearByStoriesCache) {
                if let timelineList = try? PropertyListDecoder().decode([PlacesStoriesData].self, from: data) {
                    completion(timelineList, .success, nil, nil)
                }
            }
        }
        
        if InternetConnection.isConnectedToInternet() {
            guard let metric = HTTPMetric(url: URL(string: UserService().getUserTimelineAPIEndPoint())!, httpMethod: .get) else { return }
            metric.start()
            
            let getUserTimelineAPI = UserService().getUserTimeline(userId: Helper().getUserId(), latitude: latitude, longitude: longitude, distance: distance)
            
            let getTimeLineDataAPITrace = Performance.startTrace(name: "\(AppConstants.apiNames.getUserTimeline) API loading")
            getTimeLineDataAPITrace?.start()
            
            getUserTimelineAPI.response.validate(statusCode: 200..<300).responseJSON { response in
//                debugPrint(response)
                
                getTimeLineDataAPITrace?.stop()
                
                if let httpCode = response.response?.statusCode {
                    metric.responseCode = httpCode
                }
                
                switch response.result {
                case .success:
                    guard let json = response.result.value as? [JSON] else {
                        AnalyticsHelper.APIFailureEvent(screen: self.screen, apiName: AppConstants.apiNames.getUserTimeline, errorMessage: AppConstants.ErrorLog.jsonParse, userId: Helper().getUserId(), errorCode: nil, requestPayload: getUserTimelineAPI.requestPayload, requestMethod: getUserTimelineAPI.requestMethod.rawValue, requestPathParams: getUserTimelineAPI.requestPathParams)
                        
                        let errorObject = ErrorObject.init(apiName: AppConstants.apiNames.getUserTimeline, errorType: .jsonParsingError, httpCode: nil, errorTitle: AppConstants.ErrorMessages.opsSomethingWentWrong, errorDescription: AppConstants.ErrorMessages.serverBusyPleaseTryAgain)
                        completion(nil, .jsonParseError, nil, errorObject)
                        return
                    }
                    
                    if let timeLineResponse: [PlacesStoriesData] = [PlacesStoriesData].from(jsonArray: json) {
                        
                        if enableCache {
                            if let data = try? PropertyListEncoder().encode(timeLineResponse) {
                                self.cache.saveDataIntoCache(cacheKey: self.nearByStoriesCache, cacheData: data)
                            }
                        }
                        
                        completion(timeLineResponse, .success, nil, nil)
                    }
                     else {
                        LogUtil.error(response)
                        
                        AnalyticsHelper.APIFailureEvent(screen: self.screen, apiName: AppConstants.apiNames.getUserTimeline, errorMessage: AppConstants.ErrorMessages.emptyResponse, userId: Helper().getUserId(), errorCode: nil, requestPayload: getUserTimelineAPI.requestPayload, requestMethod: getUserTimelineAPI.requestMethod.rawValue, requestPathParams: getUserTimelineAPI.requestPathParams)
                        
                        let errorObject = ErrorObject.init(apiName: AppConstants.apiNames.getUserTimeline, errorType: .jsonParsingError, httpCode: nil, errorTitle: AppConstants.ErrorMessages.opsSomethingWentWrong, errorDescription: AppConstants.ErrorMessages.serverBusyPleaseTryAgain)
                        
                        completion(nil, .jsonParseError, nil, errorObject)
                    }
                    
                case .failure(let error):
                    LogUtil.error(error)
                    
                    let error = error as NSError
                    
                    var errorObject: ErrorObject? = ErrorObject(apiName: AppConstants.apiNames.getUserTimeline, errorType: .unknown, httpCode: error.code, errorTitle: "", errorDescription: AppConstants.ErrorMessages.serverError)
                    
                    if let httpCode = response.response?.statusCode {
                        errorObject = APIErrorCodes.getErrorObject(apiName: AppConstants.apiNames.getUserTimeline, httpCode: httpCode)
                    }
                    
                    if let errorDesc = error.userInfo.valueForKeyPath(keyPath: "NSLocalizedDescription") as? String {
                        AnalyticsHelper.APIFailureEvent(screen: self.screen, apiName: AppConstants.apiNames.getUserTimeline, errorMessage: errorDesc, userId: Helper().getUserId(), errorCode: nil, requestPayload: getUserTimelineAPI.requestPayload, requestMethod: getUserTimelineAPI.requestMethod.rawValue, requestPathParams: getUserTimelineAPI.requestPathParams)
                    }
                    
                    completion(nil, .networkError, error, errorObject)
                }
                
                metric.stop()
                getTimeLineDataAPITrace?.stop()
            }
        } else {
            let errorObject = ErrorObject.init(apiName: AppConstants.apiNames.getUserTimeline, errorType: .noNetwork, httpCode: nil, errorTitle: AppConstants.ErrorMessages.noInternet, errorDescription: AppConstants.ErrorMessages.noInterneSubHeading)
            
            completion(nil, .noInternet, nil, errorObject)
        }
    }
    
    func getPlaceStories(placeId: String, latitude: Double, longitude: Double, userId: String, completion: @escaping(_ placeData: PlacesStoriesData?, _ apiResponseState: APIResponseState, _ networkError: Error?, _ error: ErrorObject?) -> Void) {
        if InternetConnection.isConnectedToInternet() {
            guard let metric = HTTPMetric(url: URL(string: PlaceService().getPlaceStoriesAPIEndPoint(placeId: placeId))!, httpMethod: .get) else { return }
            metric.start()
            
            let getPlaceStoriesAPI = PlaceService().getPlaceStories(placeId: placeId, latitude: latitude, longitude: longitude, userId: Helper().getUserId())
            getPlaceStoriesAPI.response.validate(statusCode: 200..<300).responseJSON { response in
//                debugPrint(response)
                
                if let httpCode = response.response?.statusCode {
                    metric.responseCode = httpCode
                }
                
                switch response.result {
                case .success:
                    guard let json = response.result.value as? JSON else {
                        AnalyticsHelper.APIFailureEvent(screen: self.screen, apiName: AppConstants.apiNames.getPlaceStories, errorMessage: AppConstants.ErrorLog.jsonParse, userId: Helper().getUserId(), errorCode: nil, requestPayload: getPlaceStoriesAPI.requestPayload, requestMethod: getPlaceStoriesAPI.requestMethod.rawValue, requestPathParams: getPlaceStoriesAPI.requestPathParams)
                        
                        let errorObject = ErrorObject.init(apiName: AppConstants.apiNames.getPlaceStories, errorType: .jsonParsingError, httpCode: nil, errorTitle: AppConstants.ErrorMessages.opsSomethingWentWrong, errorDescription: AppConstants.ErrorMessages.serverBusyPleaseTryAgain)
                        completion(nil, .jsonParseError, nil, errorObject)
                        
                        return
                    }
                    
                    if let placesStoriesData: PlacesStoriesData = PlacesStoriesData(json: json) {
                        completion(placesStoriesData, .success, nil, nil)
                    } else {
                        LogUtil.error(response)
                        
                        AnalyticsHelper.APIFailureEvent(screen: self.screen, apiName: AppConstants.apiNames.getPlaceStories, errorMessage: AppConstants.ErrorMessages.emptyResponse, userId: Helper().getUserId(), errorCode: nil, requestPayload: getPlaceStoriesAPI.requestPayload, requestMethod: getPlaceStoriesAPI.requestMethod.rawValue, requestPathParams: getPlaceStoriesAPI.requestPathParams)
                        
                        let errorObject = ErrorObject.init(apiName: AppConstants.apiNames.getPlaceStories, errorType: .jsonParsingError, httpCode: nil, errorTitle: AppConstants.ErrorMessages.opsSomethingWentWrong, errorDescription: AppConstants.ErrorMessages.serverBusyPleaseTryAgain)
                        completion(nil, .jsonParseError, nil, errorObject)
                    }
                    
                case .failure(let error):
                    LogUtil.error(error)
                    
                    let error = error as NSError
                    
                    var errorObject: ErrorObject? = ErrorObject(apiName: AppConstants.apiNames.getPlaceStories, errorType: .unknown, httpCode: error.code, errorTitle: "", errorDescription: AppConstants.ErrorMessages.serverError)
                    
                    if let httpCode = response.response?.statusCode {
                        errorObject = APIErrorCodes.getErrorObject(apiName: AppConstants.apiNames.getPlaceStories, httpCode: httpCode)
                    }
                    
                    if let errorDesc = error.userInfo.valueForKeyPath(keyPath: "NSLocalizedDescription") as? String {
                        AnalyticsHelper.APIFailureEvent(screen: self.screen, apiName: AppConstants.apiNames.getPlaceStories, errorMessage: errorDesc, userId: Helper().getUserId(), errorCode: nil, requestPayload: getPlaceStoriesAPI.requestPayload, requestMethod: getPlaceStoriesAPI.requestMethod.rawValue, requestPathParams: getPlaceStoriesAPI.requestPathParams)
                    }
                    
                    completion(nil, .networkError, error, errorObject)
                }
                
                metric.stop()
            }
        } else {
            let errorObject = ErrorObject.init(apiName: AppConstants.apiNames.getPlaceStories, errorType: .noNetwork, httpCode: nil, errorTitle: AppConstants.ErrorMessages.noInternet, errorDescription: AppConstants.ErrorMessages.noInterneSubHeading)
            
            completion(nil, .noInternet, nil, errorObject)
        }
    }
    
    func getPlaceStories(slug: String, completion: @escaping(_ placeData: PlacesStoriesData?, _ apiResponseState: APIResponseState, _ networkError: Error?, _ error: ErrorObject?) -> Void) {
        if InternetConnection.isConnectedToInternet() {
            guard let metric = HTTPMetric(url: URL(string: PlaceService().getPlaceStoriesBySlugAPIEndPoint())!, httpMethod: .get) else { return }
            metric.start()
            
            let getPlaceStoriesAPI = PlaceService().getPlaceStories(slug: slug)
            getPlaceStoriesAPI.response.validate(statusCode: 200..<300).responseJSON { response in
//            debugPrint(response)
                
                if let httpCode = response.response?.statusCode {
                    metric.responseCode = httpCode
                }
                
                switch response.result {
                case .success:
                    guard let json = response.result.value as? JSON else {
                        AnalyticsHelper.APIFailureEvent(screen: self.screen, apiName: AppConstants.apiNames.getPlaceStoriesBySlug, errorMessage: AppConstants.ErrorLog.jsonParse, userId: Helper().getUserId(), errorCode: nil, requestPayload: getPlaceStoriesAPI.requestPayload, requestMethod: getPlaceStoriesAPI.requestMethod.rawValue, requestPathParams: getPlaceStoriesAPI.requestPathParams)
                        
                        let errorObject = ErrorObject.init(apiName: AppConstants.apiNames.getPlaceStoriesBySlug, errorType: .jsonParsingError, httpCode: nil, errorTitle: AppConstants.ErrorMessages.opsSomethingWentWrong, errorDescription: AppConstants.ErrorMessages.serverBusyPleaseTryAgain)
                        completion(nil, .jsonParseError, nil, errorObject)
                        
                        return
                    }
                    
                    if let placesStoriesData: PlacesStoriesData = PlacesStoriesData(json: json) {
                        completion(placesStoriesData, .success, nil, nil)
                    } else {
                        LogUtil.error(response)
                        
                        AnalyticsHelper.APIFailureEvent(screen: self.screen, apiName: AppConstants.apiNames.getPlaceStoriesBySlug, errorMessage: AppConstants.ErrorMessages.emptyResponse, userId: Helper().getUserId(), errorCode: nil, requestPayload: getPlaceStoriesAPI.requestPayload, requestMethod: getPlaceStoriesAPI.requestMethod.rawValue, requestPathParams: getPlaceStoriesAPI.requestPathParams)
                        
                        let errorObject = ErrorObject.init(apiName: AppConstants.apiNames.getPlaceStoriesBySlug, errorType: .jsonParsingError, httpCode: nil, errorTitle: AppConstants.ErrorMessages.opsSomethingWentWrong, errorDescription: AppConstants.ErrorMessages.serverBusyPleaseTryAgain)
                        completion(nil, .jsonParseError, nil, errorObject)
                    }
                    
                case .failure(let error):
                    LogUtil.error(error)
                    
                    let error = error as NSError
                    
                    var errorObject: ErrorObject? = ErrorObject(apiName: AppConstants.apiNames.getPlaceStoriesBySlug, errorType: .unknown, httpCode: error.code, errorTitle: "", errorDescription: AppConstants.ErrorMessages.serverError)
                    
                    if let httpCode = response.response?.statusCode {
                        errorObject = APIErrorCodes.getErrorObject(apiName: AppConstants.apiNames.getPlaceStoriesBySlug, httpCode: httpCode)
                    }
                    
                    if let errorDesc = error.userInfo.valueForKeyPath(keyPath: "NSLocalizedDescription") as? String {
                        AnalyticsHelper.APIFailureEvent(screen: self.screen, apiName: AppConstants.apiNames.getPlaceStoriesBySlug, errorMessage: errorDesc, userId: Helper().getUserId(), errorCode: nil, requestPayload: getPlaceStoriesAPI.requestPayload, requestMethod: getPlaceStoriesAPI.requestMethod.rawValue, requestPathParams: getPlaceStoriesAPI.requestPathParams)
                    }
                    
                    completion(nil, .networkError, error, errorObject)
                }
                
                metric.stop()
            }
        } else {
            let errorObject = ErrorObject.init(apiName: AppConstants.apiNames.getPlaceStoriesBySlug, errorType: .noNetwork, httpCode: nil, errorTitle: AppConstants.ErrorMessages.noInternet, errorDescription: AppConstants.ErrorMessages.noInterneSubHeading)
            
            completion(nil, .noInternet, nil, errorObject)
        }
    }
    
    func getPlaceStories(postId: String, displayName: String, completion: @escaping(_ placeData: PlacesStoriesData?, _ apiResponseState: APIResponseState, _ networkError: Error?, _ error: ErrorObject?) -> Void) {
        if InternetConnection.isConnectedToInternet() {
            guard let metric = HTTPMetric(url: URL(string: PlaceService().getPlaceStoriesByPostIdAPIEndPoint())!, httpMethod: .get) else { return }
            metric.start()
            
            let getPlaceStoriesAPI = PlaceService().getPlaceStories(postId: postId, displayName: displayName)
            getPlaceStoriesAPI.response.validate(statusCode: 200..<300).responseJSON { response in
//                debugPrint(response)
                
                if let httpCode = response.response?.statusCode {
                    metric.responseCode = httpCode
                }
                
                switch response.result {
                case .success:
                    guard let json = response.result.value as? JSON else {
                        AnalyticsHelper.APIFailureEvent(screen: self.screen, apiName: AppConstants.apiNames.getPlaceStoriesByPostId, errorMessage: AppConstants.ErrorLog.jsonParse, userId: Helper().getUserId(), errorCode: nil, requestPayload: getPlaceStoriesAPI.requestPayload, requestMethod: getPlaceStoriesAPI.requestMethod.rawValue, requestPathParams: getPlaceStoriesAPI.requestPathParams)
                        
                        let errorObject = ErrorObject.init(apiName: AppConstants.apiNames.getPlaceStoriesByPostId, errorType: .jsonParsingError, httpCode: nil, errorTitle: AppConstants.ErrorMessages.opsSomethingWentWrong, errorDescription: AppConstants.ErrorMessages.serverBusyPleaseTryAgain)
                        completion(nil, .jsonParseError, nil, errorObject)
                        
                        return
                    }
                    
                    if let placesStoriesData: PlacesStoriesData = PlacesStoriesData(json: json) {
                        completion(placesStoriesData, .success, nil, nil)
                    } else {
                        LogUtil.error(response)
                        
                        AnalyticsHelper.APIFailureEvent(screen: self.screen, apiName: AppConstants.apiNames.getPlaceStoriesByPostId, errorMessage: AppConstants.ErrorMessages.emptyResponse, userId: Helper().getUserId(), errorCode: nil, requestPayload: getPlaceStoriesAPI.requestPayload, requestMethod: getPlaceStoriesAPI.requestMethod.rawValue, requestPathParams: getPlaceStoriesAPI.requestPathParams)
                        
                        let errorObject = ErrorObject.init(apiName: AppConstants.apiNames.getPlaceStoriesByPostId, errorType: .jsonParsingError, httpCode: nil, errorTitle: AppConstants.ErrorMessages.opsSomethingWentWrong, errorDescription: AppConstants.ErrorMessages.serverBusyPleaseTryAgain)
                        completion(nil, .jsonParseError, nil, errorObject)
                    }
                    
                case .failure(let error):
                    LogUtil.error(error)
                    
                    let error = error as NSError
                    
                    var errorObject: ErrorObject? = ErrorObject(apiName: AppConstants.apiNames.getPlaceStoriesByPostId, errorType: .unknown, httpCode: error.code, errorTitle: "", errorDescription: AppConstants.ErrorMessages.serverError)
                    
                    if let httpCode = response.response?.statusCode {
                        errorObject = APIErrorCodes.getErrorObject(apiName: AppConstants.apiNames.getPlaceStoriesByPostId, httpCode: httpCode)
                    }
                    
                    if let errorDesc = error.userInfo.valueForKeyPath(keyPath: "NSLocalizedDescription") as? String {
                        AnalyticsHelper.APIFailureEvent(screen: self.screen, apiName: AppConstants.apiNames.getPlaceStoriesByPostId, errorMessage: errorDesc, userId: Helper().getUserId(), errorCode: nil, requestPayload: getPlaceStoriesAPI.requestPayload, requestMethod: getPlaceStoriesAPI.requestMethod.rawValue, requestPathParams: getPlaceStoriesAPI.requestPathParams)
                    }
                    
                    completion(nil, .networkError, error, errorObject)
                }
                
                metric.stop()
            }
        } else {
            let errorObject = ErrorObject.init(apiName: AppConstants.apiNames.getPlaceStoriesByPostId, errorType: .noNetwork, httpCode: nil, errorTitle: AppConstants.ErrorMessages.noInternet, errorDescription: AppConstants.ErrorMessages.noInterneSubHeading)
            
            completion(nil, .noInternet, nil, errorObject)
        }
    }
    
    func getGoogleNearbyPlaces(latitude: Double, longitude: Double, completion: @escaping(_ googleNearbyPlacesResponse: GoogleNearbyPlacesResponse?, _ apiResponseState: APIResponseState, _ networkError: Error?, _ error: ErrorObject?) -> Void) {
        
       
        if InternetConnection.isConnectedToInternet() {
            guard let metric = HTTPMetric(url: URL(string: PlaceService().getGoogleNearbyPlacesAPIEndPoint())!, httpMethod: .get) else { return }
            metric.start()
            
            let getGoogleNearbyPlacesAPI = PlaceService().getGoogleNearbyPlaces(latitude: latitude, longitude: longitude)
            
            let trace = Performance.startTrace(name: "\(AppConstants.apiNames.googleSearchNearbyPlaces) API loading")
            trace?.start()
            
            getGoogleNearbyPlacesAPI.response.validate(statusCode: 200..<300).responseJSON { response in
//                debugPrint(response)
                
                trace?.stop()
                
                if let httpCode = response.response?.statusCode {
                    metric.responseCode = httpCode
                }
                
                switch response.result {
                case .success:
                    guard let json = response.result.value as? JSON else {
                        AnalyticsHelper.APIFailureEvent(screen: self.screen, apiName: AppConstants.apiNames.googleSearchNearbyPlaces, errorMessage: AppConstants.ErrorLog.jsonParse, userId: Helper().getUserId(), errorCode: nil, requestPayload: getGoogleNearbyPlacesAPI.requestPayload, requestMethod: getGoogleNearbyPlacesAPI.requestMethod.rawValue, requestPathParams: getGoogleNearbyPlacesAPI.requestPathParams)
                        
                        let errorObject = ErrorObject.init(apiName: AppConstants.apiNames.googleSearchNearbyPlaces, errorType: .jsonParsingError, httpCode: nil, errorTitle: AppConstants.ErrorMessages.opsSomethingWentWrong, errorDescription: AppConstants.ErrorMessages.serverBusyPleaseTryAgain)
                        completion(nil, .jsonParseError, nil, errorObject)
                        return
                    }
                    
                    if let nearbyPlacesResponse: GoogleNearbyPlacesResponse = GoogleNearbyPlacesResponse(json: json) {
                        completion(nearbyPlacesResponse, .success, nil, nil)
                    }
                    else {
                        LogUtil.error(response)
                        
                        AnalyticsHelper.APIFailureEvent(screen: self.screen, apiName: AppConstants.apiNames.googleSearchNearbyPlaces, errorMessage: AppConstants.ErrorMessages.emptyResponse, userId: Helper().getUserId(), errorCode: nil, requestPayload: getGoogleNearbyPlacesAPI.requestPayload, requestMethod: getGoogleNearbyPlacesAPI.requestMethod.rawValue, requestPathParams: getGoogleNearbyPlacesAPI.requestPathParams)
                        
                        let errorObject = ErrorObject.init(apiName: AppConstants.apiNames.googleSearchNearbyPlaces, errorType: .jsonParsingError, httpCode: nil, errorTitle: AppConstants.ErrorMessages.opsSomethingWentWrong, errorDescription: AppConstants.ErrorMessages.serverBusyPleaseTryAgain)
                        
                        completion(nil, .jsonParseError, nil, errorObject)
                    }
                    
                case .failure(let error):
                    LogUtil.error(error)
                    
                    let error = error as NSError
                    
                    var errorObject: ErrorObject? = ErrorObject(apiName: AppConstants.apiNames.googleSearchNearbyPlaces, errorType: .unknown, httpCode: error.code, errorTitle: "", errorDescription: AppConstants.ErrorMessages.serverError)
                    
                    if let httpCode = response.response?.statusCode {
                        errorObject = APIErrorCodes.getErrorObject(apiName: AppConstants.apiNames.googleSearchNearbyPlaces, httpCode: httpCode)
                    }
                    
                    if let errorDesc = error.userInfo.valueForKeyPath(keyPath: "NSLocalizedDescription") as? String {
                        AnalyticsHelper.APIFailureEvent(screen: self.screen, apiName: AppConstants.apiNames.googleSearchNearbyPlaces, errorMessage: errorDesc, userId: Helper().getUserId(), errorCode: nil, requestPayload: getGoogleNearbyPlacesAPI.requestPayload, requestMethod: getGoogleNearbyPlacesAPI.requestMethod.rawValue, requestPathParams: getGoogleNearbyPlacesAPI.requestPathParams)
                    }
                    
                    completion(nil, .networkError, error, errorObject)
                }
                
                metric.stop()
                trace?.stop()
            }
        } else {
            let errorObject = ErrorObject.init(apiName: AppConstants.apiNames.googleSearchNearbyPlaces, errorType: .noNetwork, httpCode: nil, errorTitle: AppConstants.ErrorMessages.noInternet, errorDescription: AppConstants.ErrorMessages.noInterneSubHeading)
            
            completion(nil, .noInternet, nil, errorObject)
        }
    }
    
    func getGooglePlacesBySearch(latitude: Double, longitude: Double, input: String, completion: @escaping(_ googleAutococompleteResponse: GoogleAutocompleteResponse?, _ apiResponseState: APIResponseState, _ networkError: Error?, _ error: ErrorObject?) -> Void) {
        
        
        if InternetConnection.isConnectedToInternet() {
            guard let metric = HTTPMetric(url: URL(string: PlaceService().getGooglePlacesBySearchAPIEndPoint())!, httpMethod: .get) else { return }
            metric.start()
            
            let getGoogleNearbyPlacesAPI = PlaceService().getGooglePlacesBySearch(latitude: latitude, longitude: longitude, input: input)
            
            let trace = Performance.startTrace(name: "\(AppConstants.apiNames.googleSearchAutocomplete) API loading")
            trace?.start()
            
            getGoogleNearbyPlacesAPI.response.validate(statusCode: 200..<300).responseJSON { response in
//                debugPrint(response)
                
                trace?.stop()
                
                if let httpCode = response.response?.statusCode {
                    metric.responseCode = httpCode
                }
                
                switch response.result {
                case .success:
                    guard let json = response.result.value as? JSON else {
                        AnalyticsHelper.APIFailureEvent(screen: self.screen, apiName: AppConstants.apiNames.googleSearchAutocomplete, errorMessage: AppConstants.ErrorLog.jsonParse, userId: Helper().getUserId(), errorCode: nil, requestPayload: getGoogleNearbyPlacesAPI.requestPayload, requestMethod: getGoogleNearbyPlacesAPI.requestMethod.rawValue, requestPathParams: getGoogleNearbyPlacesAPI.requestPathParams)
                        
                        let errorObject = ErrorObject.init(apiName: AppConstants.apiNames.googleSearchAutocomplete, errorType: .jsonParsingError, httpCode: nil, errorTitle: AppConstants.ErrorMessages.opsSomethingWentWrong, errorDescription: AppConstants.ErrorMessages.serverBusyPleaseTryAgain)
                        completion(nil, .jsonParseError, nil, errorObject)
                        return
                    }
                    
                    if let googleAutocompleteResponse: GoogleAutocompleteResponse = GoogleAutocompleteResponse(json: json) {
                        completion(googleAutocompleteResponse, .success, nil, nil)
                    }
                    else {
                        LogUtil.error(response)
                        
                        AnalyticsHelper.APIFailureEvent(screen: self.screen, apiName: AppConstants.apiNames.googleSearchAutocomplete, errorMessage: AppConstants.ErrorMessages.emptyResponse, userId: Helper().getUserId(), errorCode: nil, requestPayload: getGoogleNearbyPlacesAPI.requestPayload, requestMethod: getGoogleNearbyPlacesAPI.requestMethod.rawValue, requestPathParams: getGoogleNearbyPlacesAPI.requestPathParams)
                        
                        let errorObject = ErrorObject.init(apiName: AppConstants.apiNames.googleSearchAutocomplete, errorType: .jsonParsingError, httpCode: nil, errorTitle: AppConstants.ErrorMessages.opsSomethingWentWrong, errorDescription: AppConstants.ErrorMessages.serverBusyPleaseTryAgain)
                        
                        completion(nil, .jsonParseError, nil, errorObject)
                    }
                    
                case .failure(let error):
                    LogUtil.error(error)
                    
                    let error = error as NSError
                    
                    var errorObject: ErrorObject? = ErrorObject(apiName: AppConstants.apiNames.googleSearchAutocomplete, errorType: .unknown, httpCode: error.code, errorTitle: "", errorDescription: AppConstants.ErrorMessages.serverError)
                    
                    if let httpCode = response.response?.statusCode {
                        errorObject = APIErrorCodes.getErrorObject(apiName: AppConstants.apiNames.googleSearchAutocomplete, httpCode: httpCode)
                    }
                    
                    if let errorDesc = error.userInfo.valueForKeyPath(keyPath: "NSLocalizedDescription") as? String {
                        AnalyticsHelper.APIFailureEvent(screen: self.screen, apiName: AppConstants.apiNames.googleSearchAutocomplete, errorMessage: errorDesc, userId: Helper().getUserId(), errorCode: nil, requestPayload: getGoogleNearbyPlacesAPI.requestPayload, requestMethod: getGoogleNearbyPlacesAPI.requestMethod.rawValue, requestPathParams: getGoogleNearbyPlacesAPI.requestPathParams)
                    }
                    
                    completion(nil, .networkError, error, errorObject)
                }
                
                metric.stop()
                trace?.stop()
            }
        } else {
            let errorObject = ErrorObject.init(apiName: AppConstants.apiNames.googleSearchAutocomplete, errorType: .noNetwork, httpCode: nil, errorTitle: AppConstants.ErrorMessages.noInternet, errorDescription: AppConstants.ErrorMessages.noInterneSubHeading)
            
            completion(nil, .noInternet, nil, errorObject)
        }
    }
    
    func getPlaceDetails(identity: String, completion: @escaping(_ googleAutococompleteResponse: PlaceDetail?, _ apiResponseState: APIResponseState, _ networkError: Error?, _ error: ErrorObject?) -> Void){
        
        if InternetConnection.isConnectedToInternet() {
            guard let metric = HTTPMetric(url: URL(string: PlaceService().getPlaceDetailAPIEndPoint())!, httpMethod: .post) else { return }
            metric.start()
            
            let trace = Performance.startTrace(name: "\(AppConstants.apiNames.getPlaceDetail) API loading")
            trace?.start()
            
            let getPlaceDetailAPI = PlaceService().getPlaceDetail(placeId: identity, userId: Helper().getUserId())
            
            getPlaceDetailAPI.response.validate(statusCode: 200..<300).responseJSON { response in
//                debugPrint(response)
                
                trace?.stop()
                
                if let httpCode = response.response?.statusCode {
                    metric.responseCode = httpCode
                }
                
                switch response.result {
                case .success:
                    guard let json = response.result.value as? JSON else {
                        LogUtil.error(response)
                        return
                    }
                    
                    if let placeDetail: PlaceDetail = PlaceDetail(json: json) {
                        completion(placeDetail, .success, nil, nil)
                    } else {
                        LogUtil.error(response)
                        AnalyticsHelper.APIFailureEvent(screen: self.screen, apiName: AppConstants.apiNames.getPlaceDetail, errorMessage: AppConstants.ErrorMessages.emptyResponse, userId: Helper().getUserId(), errorCode: nil, requestPayload: getPlaceDetailAPI.requestPayload, requestMethod: getPlaceDetailAPI.requestMethod.rawValue, requestPathParams: getPlaceDetailAPI.requestPathParams)
                        
                        let errorObject = ErrorObject.init(apiName: AppConstants.apiNames.getPlaceDetail, errorType: .jsonParsingError, httpCode: nil, errorTitle: AppConstants.ErrorMessages.opsSomethingWentWrong, errorDescription: AppConstants.ErrorMessages.serverBusyPleaseTryAgain)
                        
                        completion(nil, .jsonParseError, nil, errorObject)
                    }
                    
                    break
                    
                case .failure(let error):
                    LogUtil.error(error)
                    
                    let error = error as NSError
                    
                    var errorObject: ErrorObject? = ErrorObject(apiName: AppConstants.apiNames.getPlaceDetail, errorType: .unknown, httpCode: error.code, errorTitle: "", errorDescription: AppConstants.ErrorMessages.serverError)
                    
                    if let httpCode = response.response?.statusCode {
                        errorObject = APIErrorCodes.getErrorObject(apiName: AppConstants.apiNames.getPlaceDetail, httpCode: httpCode)
                    }
                    
                    if let errorDesc = error.userInfo.valueForKeyPath(keyPath: "NSLocalizedDescription") as? String {
                        AnalyticsHelper.APIFailureEvent(screen: self.screen, apiName: AppConstants.apiNames.getPlaceDetail, errorMessage: errorDesc, userId: Helper().getUserId(), errorCode: nil, requestPayload: getPlaceDetailAPI.requestPayload, requestMethod: getPlaceDetailAPI.requestMethod.rawValue, requestPathParams: getPlaceDetailAPI.requestPathParams)
                    }
                    
                    completion(nil, .networkError, error, errorObject)
                    

                }
                metric.stop()
            }
        } else {
            let errorObject = ErrorObject.init(apiName: AppConstants.apiNames.googleSearchAutocomplete, errorType: .noNetwork, httpCode: nil, errorTitle: AppConstants.ErrorMessages.noInternet, errorDescription: AppConstants.ErrorMessages.noInterneSubHeading)
            
            completion(nil, .noInternet, nil, errorObject)
        }
    }
}
