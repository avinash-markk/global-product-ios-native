//
//  UserStoriesSkeletonTableViewCell.swift
//  Sminq
//
//  Created by Pushkar Deshmukh on 14/03/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import UIKit

class UserStoriesSkeletonTableViewCell: UITableViewCell {

    @IBOutlet weak var topView: UIView!
    
    @IBOutlet weak var bottomView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.initUI()
    }
    
    // MARK: Private functions
    private func initUI() {
        self.contentView.backgroundColor = AppConstants.SminqColors.blackDark100
    }

    func showSkeleton() {
        self.topView.showAnimatedGradientAnimation()
        self.bottomView.showAnimatedGradientAnimation()
    }
    
}
