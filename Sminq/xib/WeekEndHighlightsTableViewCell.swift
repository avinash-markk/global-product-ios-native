//
//  WeekEndHighlightsTableViewCell.swift
//  Markk
//
//  Created by Pushkar Deshmukh on 22/07/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import UIKit

protocol WeekEndHighlightsTableViewCellDelegate: NSObjectProtocol {
    func didSelectItemAt(indexPath: IndexPath, banner: Banner)
}

class WeekEndHighlightsTableViewCell: UITableViewCell {
    public weak var delegate: WeekEndHighlightsTableViewCellDelegate?
    
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var headingLabel: UILabel!
    @IBOutlet weak var subHeadingLabel: UILabel!
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var collectionViewFlowLayout: UICollectionViewFlowLayout!
    
    let cardHeight: CGFloat = 298
    var cardWidth: CGFloat = 167.5
    let spacing: CGFloat = 16
    lazy var labelWidth: CGFloat = 176
    lazy var collectionViewLeftPadding: CGFloat = self.labelWidth + 16 + 16
    
    var bannerViewModel: BannerViewModel? {
        didSet {
            updateData()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        initDelegates()
        setupUI()
    }
    
    // MARK: Fileprivate functions
    
    fileprivate func setupUI() {
        contentView.backgroundColor = AppConstants.SminqColors.blackDark100
        
        collectionView.backgroundColor = .clear
        
        collectionView.register(UINib(nibName: "PlaceStoryVariant1CollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "PlaceStoryVariant1CollectionViewCell")
        collectionView.register(UINib(nibName: "HighlightMoodCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "HighlightMoodCollectionViewCell")
        collectionView.register(UINib(nibName: "EmptyCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "EmptyCollectionViewCell")
        
        collectionViewFlowLayout.scrollDirection = .horizontal
        collectionViewFlowLayout.sectionInset = UIEdgeInsets(top: spacing, left: collectionViewLeftPadding + 16, bottom: spacing, right: spacing / 2)
        collectionView.showsVerticalScrollIndicator = false
        collectionView.showsHorizontalScrollIndicator = false
        
        cardView.backgroundColor = AppConstants.SminqColors.red100
        cardView.layer.cornerRadius = 8
        
        headingLabel.font = UIFont.init(name: AppConstants.Font.montSerratBlack, size: 20)
        headingLabel.textColor = AppConstants.SminqColors.white100
        
        subHeadingLabel.font = UIFont.init(name: AppConstants.Font.primaryRegular, size: 14)
        subHeadingLabel.textColor = AppConstants.SminqColors.white100

    }
    
    fileprivate func initDelegates() {
        collectionView.delegate = self
        collectionView.dataSource = self
    }
    
    fileprivate func setLabelsAlpha(alpha: CGFloat) {
        headingLabel.alpha = alpha
        subHeadingLabel.alpha = alpha
    }
    
    fileprivate func handleScrollViewDidScroll(_ scrollView: UIScrollView) {
        let scrollXPos = scrollView.contentOffset.x
        
        if scrollXPos <= 0 {
            setLabelsAlpha(alpha: 1)
        } else {
            let percentScrolled = ((scrollXPos * 100) / collectionViewLeftPadding)
            let alpha = 1 - (percentScrolled / 100)
            setLabelsAlpha(alpha: alpha)
        }
    }
    
    fileprivate func updateData() {
        guard let bannerViewModel = bannerViewModel, let bannerData = bannerViewModel.bannerData else { return }
        
        headingLabel.text = bannerData.label
        subHeadingLabel.text = bannerData.description
        
        collectionView.reloadData()
        collectionView.collectionViewLayout.invalidateLayout()
    }
    
    // MARK: Public functions
    
    public func setBannerViewModel(bannerViewModel: BannerViewModel) {
        self.bannerViewModel = bannerViewModel
    }
    
}

extension WeekEndHighlightsTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let bannerViewModel = bannerViewModel, let bannerData = bannerViewModel.bannerData {
            return bannerData.banners.count
        }
        
        return 0
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let bannerViewModel = bannerViewModel, let bannerData = bannerViewModel.bannerData {
            let banner = bannerData.banners[indexPath.row]
            
            if let bannerType = banner.type {
                switch bannerType {
                case .showcase:
                    guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PlaceStoryVariant1CollectionViewCell", for: indexPath) as? PlaceStoryVariant1CollectionViewCell else { return UICollectionViewCell() }
                    
                    if let placeStoriesData = banner.places.first {
                        cell.setPlace(place: placeStoriesData)
                    }
                    
                    return cell
                    
                case .totalDope, .totalStickers, .maxRatings, .maxLikes, .maxViews:
                    guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HighlightMoodCollectionViewCell", for: indexPath) as? HighlightMoodCollectionViewCell else { return UICollectionViewCell() }
                    
                    cell.setBanner(banner: banner)
                    return cell
                case .maxUserRatings:
                    break
                case .maxUserPlaces:
                    break
                case .newBadge:
                    break
                case .claimedReward:
                    break
                }
                
            }
            
        }
        
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "EmptyCollectionViewCell", for: indexPath) as? EmptyCollectionViewCell else { return UICollectionViewCell() }
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: cardWidth, height: cardHeight)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let bannerViewModel = bannerViewModel, let bannerData = bannerViewModel.bannerData {
            let banner = bannerData.banners[indexPath.row]
            delegate?.didSelectItemAt(indexPath: indexPath, banner: banner)
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        handleScrollViewDidScroll(scrollView)
    }
}
