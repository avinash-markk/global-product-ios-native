//
//  UserEditProfileBioCell.swift
//  Sminq
//
//  Created by Pushkar Deshmukh on 04/04/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import UIKit

protocol UserEditProfileBioCellDelegate: NSObjectProtocol {
    func didCellHeightChange(height: CGFloat, cell: UserEditProfileBioCell, bio: String)
}

class UserEditProfileBioCell: UITableViewCell {
    public weak var delegate: UserEditProfileBioCellDelegate?
    
    @IBOutlet weak var bioHeadingLabel: UILabel!
    
    @IBOutlet weak var bioTextView: UITextView!
    @IBOutlet weak var bioTextViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var bioTextPlaceHolderLabel: UILabel!
    
    var textViewMaxContentSize: CGFloat = 60
    var maxBioLength = 200
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.initUI()
        self.initDelegates()
    }

    // MARK: Private function
    
    private func initUI() {
        self.contentView.backgroundColor = AppConstants.SminqColors.blackDark100
        
        self.bioHeadingLabel.textColor = AppConstants.SminqColors.white100.withAlphaComponent(0.75)
        self.bioHeadingLabel.text = "BIO"
        self.bioHeadingLabel.font = UIFont(name: AppConstants.Font.primaryRegular, size: 12)

        self.bioTextView.textColor = AppConstants.SminqColors.white100
        self.bioTextView.font = UIFont(name: AppConstants.Font.primaryRegular, size: 16)
        self.bioTextView.textContainer.lineFragmentPadding = 0
        self.bioTextView.textContainerInset = UIEdgeInsets.zero
        
        self.bioTextPlaceHolderLabel.font = UIFont(name: AppConstants.Font.primaryRegular, size: 16)
        self.bioTextPlaceHolderLabel.textColor = AppConstants.SminqColors.white100.withAlphaComponent(0.5)
        self.bioTextPlaceHolderLabel.text = "Your bio..."
        self.bioTextPlaceHolderLabel.isHidden = true
        self.bioTextPlaceHolderLabel.isUserInteractionEnabled = false
    }
    
    private func initDelegates() {
        self.bioTextView.delegate = self
    }
    
    private func getDynamicHeight() -> CGFloat {
        var textViewHeight = self.bioTextView.contentSize.height
        
        if textViewHeight >= textViewMaxContentSize {
            textViewHeight = textViewMaxContentSize
        }
        
        return textViewHeight + 20
    }
    
    private func bioPlaceholderVisibility() {
        if self.bioTextView.text.count == 0 {
            self.bioTextPlaceHolderLabel.isHidden = false
        } else {
            self.bioTextPlaceHolderLabel.isHidden = true
        }
    }
    
    // MARK: Public functions
    func setBioLabel(bio: String) {
        self.bioTextView.text = bio
        
        self.bioPlaceholderVisibility()
    }
}

extension UserEditProfileBioCell: UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
//        print(textView.contentSize.height)
        
        self.bioPlaceholderVisibility()
        
        self.bioTextViewHeightConstraint.constant = self.getDynamicHeight()
        self.delegate?.didCellHeightChange(height: self.bioTextViewHeightConstraint.constant, cell: self, bio: textView.text)
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        
        return newText.count < maxBioLength
    }
}
