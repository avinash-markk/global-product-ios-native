//
//  MoodsSkeletonViewTableViewCell.swift
//  Sminq
//
//  Created by Pushkar Deshmukh on 16/03/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import UIKit

class MoodsSkeletonViewTableViewCell: UITableViewCell {
    @IBOutlet weak var topView: UIView!
    
    @IBOutlet weak var stackView1: UIView!
    @IBOutlet weak var stackView2: UIView!
    
    @IBOutlet weak var stackView3: UIView!
    @IBOutlet weak var stackView4: UIView!
    
    @IBOutlet weak var stackView5: UIView!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.initUI()
    }

    // MARK: Private functions
    
    private func initUI() {
        self.contentView.backgroundColor = AppConstants.SminqColors.blackDark100
        
        self.stackView1.clipsToBounds = true
        self.stackView1.layer.cornerRadius = self.stackView1.height / 2.0
        
        self.stackView2.clipsToBounds = true
        self.stackView2.layer.cornerRadius = self.stackView2.height / 2.0
        
        self.stackView3.clipsToBounds = true
        self.stackView3.layer.cornerRadius = self.stackView3.height / 2.0
        
        self.stackView4.clipsToBounds = true
        self.stackView4.layer.cornerRadius = self.stackView4.height / 2.0
        
        self.stackView5.clipsToBounds = true
        self.stackView5.layer.cornerRadius = self.stackView5.height / 2.0
        
        self.topView.clipsToBounds = true
        self.topView.layer.cornerRadius = self.topView.height / 2.0
    }
    
    // MARK: Public functions
    
    func showSkeleton() {
        self.topView.showAnimatedGradientAnimation()
        self.stackView1.showAnimatedGradientAnimation()
        self.stackView2.showAnimatedGradientAnimation()
        self.stackView3.showAnimatedGradientAnimation()
        self.stackView4.showAnimatedGradientAnimation()
        self.stackView5.showAnimatedGradientAnimation()
        
    }
    
}
