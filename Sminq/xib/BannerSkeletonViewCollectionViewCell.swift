//
//  BannerSkeletonViewCollectionViewCell.swift
//  Sminq
//
//  Created by Pushkar Deshmukh on 08/03/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import UIKit

class BannerSkeletonViewCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var skeletonCardView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.initUI()
    }

    // MARK: Private functions
    
    func initUI() {
        skeletonCardView.backgroundColor = UIColor(white: 0, alpha: 0.1)
        
        let shinyView = UIView()
        shinyView.frame = skeletonCardView.frame
        shinyView.backgroundColor = UIColor(white: 0, alpha: 0.2)
        
        self.contentView.addSubview(shinyView)
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [
            UIColor.clear.cgColor, UIColor.clear.cgColor,
            UIColor.black.cgColor, UIColor.black.cgColor,
            UIColor.clear.cgColor, UIColor.clear.cgColor
        ]
        
        gradientLayer.locations = [0, 0.2, 0.4, 0.6, 0.8, 1]
        gradientLayer.frame = shinyView.frame
        
        let angle = 90 * CGFloat.pi / 180
        gradientLayer.transform = CATransform3DMakeRotation(angle, 0, 0, 1)
        
        shinyView.layer.mask = gradientLayer
        
        gradientLayer.transform = CATransform3DConcat(gradientLayer.transform, CATransform3DMakeScale(2, 2, 0))
        
        let animation = CABasicAnimation(keyPath: "transform.translation.x")
        animation.fromValue = -1.5 * skeletonCardView.frame.width
        animation.toValue = 1.5 * skeletonCardView.frame.width
        animation.repeatCount = Float.infinity
        animation.duration = 2
        
        gradientLayer.add(animation, forKey: "animationn")
    }
}
