//
//  AppDelegate+Extension.swift
//  Markk
//
//  Created by Pushkar Deshmukh on 09/07/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import UIKit
import Alamofire
import AVKit

extension AppDelegate {
    public func handleSelectedNotification(launchOptions: [UIApplicationLaunchOptionsKey: Any]?) {
        if launchOptions != nil {
            if let userInfo = launchOptions?[UIApplicationLaunchOptionsKey.remoteNotification] as? [AnyHashable: Any] {
                UserNoficationService.shared.handleNotification(userInfo: userInfo)
                NotificationCacheService.shared.cacheNotificationByUserInfo(userInfo: userInfo)
            }
        }
    }
    
    public func cacheAllDeliveredNotifications() {
        UserNoficationService.shared.getAllDeliveredNotifications { (notifications) in
            for notification in notifications.reversed() {
                NotificationCacheService.shared.cacheNotificationByUNNotification(unNotification: notification)
            }
            
            UIApplication.shared.applicationIconBadgeNumber = 0
            UserNoficationService.shared.removeAllNotificationsFromTray(completion: {
            })
        }
    }
    
    public func setupAlamofireManager() {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = AppConstants.apiRequestTimeout // seconds
        configuration.timeoutIntervalForResource = AppConstants.apiRequestTimeout // seconds
        AFManager = Alamofire.SessionManager(configuration: configuration)
    }
    
    public func audioConfig() {
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryAmbient)
            try AVAudioSession.sharedInstance().setActive(true)
        } catch {
            LogUtil.error(error)
        }
    }
}
