//
//  BannerViewModel.swift
//  Sminq
//
//  Created by Pushkar Deshmukh on 14/03/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import Foundation


class BannerViewModel {
    private(set) var screen = ""
    var bannerData: BannerData?
    
    init(screen: String) {
        self.screen = screen
    }
    
    func setBannerAt(index: Int, banner: Banner) {
        guard var bannerData = bannerData else { return }
        if index < bannerData.banners.count  && getBannerCount() > 0 {
            bannerData.banners[index] = banner
        }
        
        self.bannerData = bannerData
    }
    
    func getBannerCount() -> Int {
        if let bannerData = bannerData {
            return bannerData.banners.count
        }
        return 0
    }
    
    func getBanners(completion: @escaping(_ bannerData: BannerData?, _ apiResponseState: APIResponseState, _ networkError: Error?) -> Void) {
        let bannersDataClass = BannerDataClass.init(screen: self.screen)
        
        bannersDataClass?.getBanners(completion: { (bannerData, apiResponseState, networkError) in
            self.bannerData = bannerData
            
            DispatchQueue.main.async {
                completion(bannerData, apiResponseState, networkError)
            }
        })
    }
    
    func handlePusherUpdate(newBanner: Banner) {
        guard let bannerData = bannerData else { return }
        
        var foundBanner = false
        
        for (index, banner) in bannerData.banners.enumerated() {
            if let newBannerType = newBanner.type, let oldBannerType = banner.type {
                if newBannerType == oldBannerType {
                    foundBanner = true
                    setBannerAt(index: index, banner: newBanner)
                }
            }
        }
        
        if !foundBanner {
            handlePusherPrepend(newBanner: newBanner)
            
        }
    }
    
    func handlePusherPrepend(newBanner: Banner) {
        guard var bannerData = bannerData else { return }
        
        bannerData.banners.insert(newBanner, at: 0)
        
        if bannerData.banners.count > FIRRemoteConfigService.shared.getTrendingMaxBannersCount() {
            bannerData.banners.removeLast()
        }
        
        self.bannerData = bannerData
        
    }
    
    func updateBannerFor(post: UserDataPlaceFeed) {
        guard var bannerData = bannerData else { return }
        
        if bannerData.banners.count > 0 {
            for (bannerIndex, banner) in bannerData.banners.enumerated() {
                for (placeIndex, place) in banner.places.enumerated() {
                    if let stories = place.stories {
                        for (storyIndex, story) in stories.enumerated() {
                            for (postIndex, postObject) in story.posts.enumerated() {
                                if postObject._id == post._id {
                                    bannerData.banners[bannerIndex].places[placeIndex].stories?[storyIndex].posts[postIndex] = post
                                }
                                
                            }
                        }
                    }
                    
                }
            }
            
            self.bannerData = bannerData
        }
    }
    
}


