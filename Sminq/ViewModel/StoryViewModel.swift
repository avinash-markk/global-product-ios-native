//
//  StoryViewModel.swift
//  Sminq
//
//  Created by Pushkar Deshmukh on 22/02/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import Foundation
import Gloss

class StoryViewModel {
    private(set) var storyList = [StoryData]()
    
    private(set) var screen = ""
    
    private(set) var storySnappedIndex = 0
    
    init?(screen: String) {
        self.screen = screen
    }
    
    init?(screen: String, storyList: [StoryData]) {
        self.screen = screen
        self.storyList = storyList
    }
    
    public func setStorySnapIndex(index: Int) {
        self.storySnappedIndex = index
    }
    
    public  func getStorySnapIndex() -> Int {
        return storySnappedIndex
    }
    
    public func getCurrentSnappedStory() -> StoryData? {
        if storySnappedIndex < storyList.count {
            return storyList[storySnappedIndex]
        }
        return nil
    }
    
    func setStoryList(storyList: [StoryData]) {
        self.storyList = storyList
    }
        
    func emptyStoryList() {
        self.storyList.removeAll()
    }
    
    func getStoryCount() -> Int {
        return self.storyList.count
    }
    
    func getPlaceIndexInStory(placeId: String) -> Int? {
        for (index, story) in self.storyList.enumerated() {
            guard let placeDetails = story.placeDetails else { continue }
            
            if placeDetails._id == placeId {
                return index
            }
        }
        
        return nil
    }
    
    func getPlaceIndexInStory(place: LivePlaces) -> Int? {
        for (index, story) in self.storyList.enumerated() {
            guard let placeDetails = story.placeDetails else { continue }
            if placeDetails._id == place._id {
                return index
            }
        }
        
        return nil
    }
    
    func getLiveStories(userId: String, city: String, sort: String, limit: Int?, latitude: Double, longitude: Double, postType: String, distance: String, placeGroup: String, completion: @escaping(_ stories: [StoryData]?, _ apiResponseState: APIResponseState, _ networkError: Error?, _ error: ErrorObject?) -> Void) {
        
        let storyDataClass = StoryDataClass(screen: self.screen)
        
        storyDataClass?.getLiveStories(userId: userId, city: city, sort: sort, limit: limit, latitude: latitude, longitude: longitude, postType: postType, distance: distance, placeGroup: placeGroup, completion: { (stories, apiResponseState, networkError, error) in
            if let tempStoryList = stories, tempStoryList.count > 0 {
                self.setStoryList(storyList: tempStoryList)
            }
            
            completion(stories, apiResponseState, networkError, error)
        })
    }
    
    func getStoriesFromJSON(completion: @escaping(_ stories: [PlacesStoriesData]?) -> ()) {
        if let path = Bundle.main.path(forResource: "stories", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
                
                let placesStoriesList: [PlacesStoriesData] = [PlacesStoriesData].from(jsonArray: jsonResult as! [JSON])!
                
                DispatchQueue.main.async {
                    completion(placesStoriesList)
                }
                
            } catch {
                LogUtil.error(error)
            }
        }
    }
}
