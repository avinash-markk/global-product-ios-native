//
//  GooglePlaceViewModel.swift
//  Markk
//
//  Created by Pushkar Deshmukh on 27/06/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import Foundation

class GooglePlace {
    var place_id: String
    var name: String
    var address: String
    var types: [String]
    

    init(place_id: String, name: String, address: String, types: [String]) {
        self.place_id = place_id
        self.name = name
        self.address = address
        self.types = types
    }
}

class GooglePlaceViewModel {
    private(set) var screen = ""
    private var places = [GooglePlace]()
    private let typesToFilter = ["locality", "sublocality_level_1", "sublocality_level_2", "administrative_level_1", "administrative_level_2"]
    
    init?(screen: String, places: [GooglePlace]) {
        self.screen = screen
    }
    
    func setPlaceList(placeList: [GooglePlace]) {
        self.places = placeList
    }
    
    func getPlaceAt(index: Int) -> GooglePlace? {
        if index < places.count {
            return places[index]
        }
        
        return nil
    }
    
    func emptyPlaceList() {
        self.places.removeAll()
    }
    
    func getPlacesCount() -> Int {
        return self.places.count
    }
    
    func getGoogleNearbyPlaces(latitude: Double, longitude: Double, completion: @escaping(_ nearbyPlaces: [GooglePlace], _ apiResponseState: APIResponseState, _ networkError: Error?, _ error: ErrorObject?) -> Void) {
        let placeDataClass = PlaceDataClass(screen: self.screen)!
        placeDataClass.getGoogleNearbyPlaces(latitude: latitude, longitude: longitude) { (nearbyPlacesResponse, apiResponseState, networkError, error) in
            
            DispatchQueue.main.async {
                var googlePlaces = self.convertNearbyPlaceToGooglePlace(googleNearbyPlaces: nearbyPlacesResponse?.results ?? [])
                googlePlaces = self.filterPlacesByTypes(places: googlePlaces)
                self.setPlaceList(placeList: googlePlaces)
                completion(googlePlaces, apiResponseState, networkError, error)
            }
        }
    }
    
    func getGooglePlacesBySearch(latitude: Double, longitude: Double, input: String, completion: @escaping(_ searchPlaces: [GooglePlace], _ apiResponseState: APIResponseState, _ networkError: Error?, _ error: ErrorObject?) -> Void) {
        let placeDataClass = PlaceDataClass(screen: self.screen)!
        placeDataClass.getGooglePlacesBySearch(latitude: latitude, longitude: longitude, input: input) { (googleAutocompleteResponse, apiResponseState, networkError, error) in
            DispatchQueue.main.async {
                if let autoCompleteResponse = googleAutocompleteResponse {
                    var googlePlaces = self.convertSearchPlaceToGooglePlace(places: autoCompleteResponse.predictions)
                    googlePlaces = self.filterPlacesByTypes(places: googlePlaces)
                    self.setPlaceList(placeList: googlePlaces)
                    completion(googlePlaces, apiResponseState, networkError, error)
                } else {
                    completion([], apiResponseState, networkError, error)
                }
                
            }
        }
        
    }
    
    func convertNearbyPlaceToGooglePlace(googleNearbyPlaces: [GoogleNearbyPlace]) -> [GooglePlace] {
        let places = googleNearbyPlaces.map { (place) -> GooglePlace in
            return GooglePlace.init(place_id: place.place_id, name: place.name, address: place.vicinity, types: place.types)
        }
        
        return places
    }
    
    func convertSearchPlaceToGooglePlace(places: [GoogleSearchPlace]) -> [GooglePlace] {
        let places = places.compactMap { (place) -> GooglePlace? in
            guard let structuredFormatting = place.structured_formatting else { return nil }
            
            return GooglePlace.init(place_id: place.place_id, name: structuredFormatting.main_text, address: place.description, types: place.types)
        }
        
        return places
    }
    
    func filterPlacesByTypes(places: [GooglePlace]) -> [GooglePlace] {
        return places.filter({ (place) -> Bool in
            var isTypeFound = false
            
            place.types.forEach({ (type) in
                if typesToFilter.contains(type) {
                    isTypeFound = true
                }
            })
            
            return !isTypeFound
        })
    }
    
    
}


