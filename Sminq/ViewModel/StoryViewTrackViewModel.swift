//
//  StoryViewTrackViewModel.swift
//  Sminq
//
//  Created by Pushkar Deshmukh on 22/02/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import Foundation

class StoryViewTrackViewModel {
    private(set) var screen = ""
    private(set) var storyViewList = [StoryViewData]()
    let storyViewMetricsCacheKey = "storyViewMetricsCacheKey"
    let cache = DataCacheService()
    
    init?(screen: String) {
        self.screen = screen
    }
    
    init?(screen: String, storyViewList: [StoryViewData]) {
        self.screen = screen
        self.storyViewList = storyViewList
    }
    
    func getStoryViewListCount() -> Int{
        return self.storyViewList.count
    }
    
    func emptyStoryViewList() {
        self.storyViewList.removeAll()
    }
    
    func addStoryView(storyView: StoryViewData) {
        var isStoryFound = false
        
        if let _ = storyViewList.first(where: { $0.id == storyView.id }) {
            isStoryFound = true
        }
        
        if !isStoryFound {
            self.storyViewList.append(storyView)
            appendMetricsAndSave(storyViewData: storyView)
        }
        
    }
    
    func postStoryMetrics(completion: @escaping(_ apiResponseState: APIResponseState, _ networkError: Error?) -> Void) {
        let storyViewTrackDataClass = StoryViewTrackDataClass(screen: self.screen)
        
        let cachedStoryMetrics = getCachedStoryViewMetrics()
        guard cachedStoryMetrics.count > 0, InternetConnection.isConnectedToInternet() else { return }
        
        // NOTE: Clear local data before making network call. And pass the cached metrics to API
        self.emptyStoryViewList()
        self.cache.removeCacheByKey(key: self.storyViewMetricsCacheKey)
        
        storyViewTrackDataClass?.postStoryViewMetrics(storyViewList: cachedStoryMetrics, completion: { (apiResponseState, networkError) in
            print("in response")
//            guard let this = self else { return }
            
            
            completion(apiResponseState, networkError)
        })
    }
    
    func appendMetricsAndSave(storyViewData: StoryViewData) {
        var cachedStoryViewMetrics = getCachedStoryViewMetrics()
        cachedStoryViewMetrics.append(storyViewData)
        
        saveStoryViewMetrics(storyViewMetricsList: cachedStoryViewMetrics)
    }
    
    func getCachedStoryViewMetrics() -> [StoryViewData] {
        var tempStoryViewList = [StoryViewData]()
        
        if let data = cache.loadDataFromCache(cacheKey: storyViewMetricsCacheKey) {
            if let storyViewList = try? PropertyListDecoder().decode([StoryViewData].self, from: data) {
                tempStoryViewList = storyViewList
            }
        }
        
        return tempStoryViewList
    }
    
    func saveStoryViewMetrics(storyViewMetricsList: [StoryViewData]) {
        if let data = try? PropertyListEncoder().encode(storyViewMetricsList) {
            cache.saveDataIntoCache(cacheKey: storyViewMetricsCacheKey, cacheData: data)
        }
    }
    
}
