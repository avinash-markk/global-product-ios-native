//
//  ArchiveRatingsViewModel.swift
//  Markk
//
//  Created by Avinash Thakur on 20/06/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import UIKit

class ArchiveRatingsViewModel: NSObject {
    
    var displayName: String!
    var archiveRatingsList = [ArchiveRatingsData]()
    let archiveRatingsDataClass = ArchiveRatingsDataClass()
    
    init(displayName: String) {
        self.displayName = displayName
    }
    
    func getArchiveRatings(displayName: String, completion: @escaping(_ result: APIResponseState, _ networkError: Error?, _ error: ErrorObject?) -> Void) {
        
        archiveRatingsDataClass.getArchivedRatings(displayName: self.displayName, completion: { (result, apiResponseState, networkError, error) in
            
            switch apiResponseState {
                
            case .success:
                self.archiveRatingsList = result ?? []
                DispatchQueue.main.async {
                    completion(.success, nil, nil)
                }
                
            case .apiError, .networkError, .jsonParseError, .noInternet:
                DispatchQueue.main.async {
                    completion(apiResponseState, networkError, error)
                }
            }
            
        })
    }
    
    func getArchivesSectionHeight() -> CGFloat {
        let height = archiveRatingsList.count <= 0 ? 298 : self.calcaluteHeightforArchivesData()
        return CGFloat(height)
    }
    
    func calcaluteHeightforArchivesData() -> CGFloat {
        var totalHeight = 0
        let divider = UIScreen.main.bounds.width > 320 ? 4 : 3
        totalHeight =  totalHeight + (archiveRatingsList.count * 56)
        for index in 0...archiveRatingsList.count - 1 {
            if let placesRatings = archiveRatingsList[index].archivePlacesRatings {
                totalHeight =  totalHeight + (placesRatings.count * (40 + 24))
                for placeIndex in 0...placesRatings.count - 1 {
                    if let stories = placesRatings[placeIndex].archiveRatings {
                        let multiplier = self.getMultiplierForArchivedRatings(storyCount: stories.count, divider: divider)
                        totalHeight = totalHeight + (multiplier * 93)
                    }
                }
            }
        }
        return CGFloat(totalHeight)
    }
    
    func getArchiveCollectionViewHeight(atSection: Int, atIndex: Int) -> CGFloat {
        var totalHeight = 0
        totalHeight = totalHeight + 40 + 24
        let divider = UIScreen.main.bounds.width > 320 ? 4 : 3
        let placesRatings = archiveRatingsList[atSection].archivePlacesRatings
        let stories = placesRatings![atIndex].archiveRatings
        let multiplier = self.getMultiplierForArchivedRatings(storyCount: stories!.count, divider: divider)
        totalHeight = totalHeight + (multiplier * 93)
        return CGFloat(totalHeight)
    }
    
    func getMultiplierForArchivedRatings(storyCount: Int, divider: Int) -> Int {
        var multiplier = 0
        if storyCount <= divider {
             multiplier = 1
        } else {
            let remainder = storyCount % divider
            if remainder == 0 {
               multiplier = storyCount / divider
            } else {
                multiplier = ((storyCount - remainder) / divider) + 1
            }
        }
        return multiplier
    }
    
}
