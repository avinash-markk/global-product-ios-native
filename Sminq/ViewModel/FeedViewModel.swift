//
//  FeedViewModel.swift
//  Sminq
//
//  Created by Pushkar Deshmukh on 19/02/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import Foundation

class FeedViewModel: Codable {
    private(set) var feedList = [UserDataPlaceFeed]()
    
    private(set) var screen = ""
    
    init?(screen: String) {
        self.screen = screen
    }
    
    func setFeedList(feedList: [UserDataPlaceFeed]) {
        self.feedList = feedList
    }
    
    func emptyFeedList() {
        self.feedList.removeAll()
    }
    
    func getFeedAt(index: Int) -> UserDataPlaceFeed? {
        if index < self.feedList.count {
            return self.feedList[index]
        }
        return nil
    }
    
    func setFeedAt(index: Int, feed: UserDataPlaceFeed) {
        if index < self.feedList.count  && self.getFeedCount() > 0 {
            self.feedList[index] = feed
        }
    }
    
    func updateFeed(with feed: UserDataPlaceFeed) {
        for (index, tempFeed) in self.feedList.enumerated() {
            if tempFeed._id == feed._id {
                self.setFeedAt(index: index, feed: feed)
            }
        }
    }
    
    func setFeedRepliesAt(index: Int, replies: [Replies]) {
        if index < self.feedList.count  && self.getFeedCount() > 0 {
            self.feedList[index].comments?.users = replies
        }
    }
    
    func getFeedList() -> [UserDataPlaceFeed]{
        return feedList
    }
    
    func getFeedCount() -> Int {
        return self.feedList.count
    }
    
    func filterFeedById(feedId: String) {
        let tempFeedList = self.feedList.filter { feed in
            if feed._id == feedId {
                return true
            }
            
            return false
        }
        
        self.setFeedList(feedList: tempFeedList)
    }
    
    func upvoteFeed(feed: UserDataPlaceFeed, completion: @escaping (_ feedResponse: UserDataPlaceFeed?, _ apiResponseState: APIResponseState, _ networkError: Error?, _ updatedFeed: UserDataPlaceFeed, _ error: ErrorObject?) -> Void) {
        var feedObject = feed
        
        if !(feedObject.hasUserUpVoted)  && !(feedObject.hasUserDownVoted) {
            // NOTE: No upvote and no downvote - So Upvoting here
            
            feedObject.hasUserUpVoted = true
            feedObject.hasUserDownVoted = false
            
            if let count = feedObject.upVotesCount {
                feedObject.upVotesCount = count + 1
            }
            
            self.updateFeed(with: feedObject)
            completion(nil, .success, nil, feedObject, nil)
            
            let feedDataClass = FeedDataClass.init(screen: self.screen)
            
            feedDataClass?.upvoteFeed(feed: feedObject, userId: Helper().getUserId(), status: 1) { (feedResponse, apiResponseState, networkError, upvoteError) in
                DispatchQueue.main.async {
                    if upvoteError == nil {
                        feedObject.hasUserUpVoted = true
                        feedObject.hasUserDownVoted = false
                        feedObject.upVotesCount = feedResponse?.upVotesCount
                        feedObject.downVotesCount = feedResponse?.downVotesCount
                    } else {
                        feedObject.hasUserUpVoted = false
                        feedObject.hasUserDownVoted = false
                        
                        if let count = feedObject.upVotesCount {
                            feedObject.upVotesCount = count - 1
                        }
                    }
                    
                    self.updateFeed(with: feedObject)
                    completion(feedResponse, apiResponseState, networkError, feedObject, upvoteError)
                }
            }
            
        } else if (feedObject.hasUserUpVoted) && !(feedObject.hasUserDownVoted) {
            // NOTE: Upvoted already and no downvote - So removing upvote here
            
            feedObject.hasUserUpVoted = false
            feedObject.hasUserDownVoted = false
            
            if let count = feedObject.upVotesCount {
                feedObject.upVotesCount = count - 1
            }
            
            self.updateFeed(with: feedObject)
            completion(nil, .success, nil, feedObject, nil)
            
            let feedDataClass = FeedDataClass.init(screen: self.screen)
            
            feedDataClass?.upvoteFeed(feed: feedObject, userId: Helper().getUserId(), status: 0) { (feedResponse, apiResponseState, networkError, upvoteError) in
                DispatchQueue.main.async {
                    if upvoteError == nil {
                        feedObject.hasUserUpVoted = false
                        feedObject.hasUserDownVoted = false
                        feedObject.upVotesCount = feedResponse?.upVotesCount
                        feedObject.downVotesCount = feedResponse?.downVotesCount
                    } else {
                        feedObject.hasUserUpVoted = true
                        feedObject.hasUserDownVoted = false
                        
                        if let count = feedObject.upVotesCount {
                            feedObject.upVotesCount = count + 1
                        }
                    }
                   
                    self.updateFeed(with: feedObject)
                    completion(feedResponse, apiResponseState, networkError, feedObject, upvoteError)
                }
            }
        } else if !(feed.hasUserUpVoted)  && (feed.hasUserDownVoted) {
            // NOTE: Already downvoted , So Removing downvote first and then upvoting!
            
            feedObject.hasUserUpVoted = true
            feedObject.hasUserDownVoted = false
            
            if let count = feedObject.upVotesCount {
                feedObject.upVotesCount = count + 1
            }

            self.updateFeed(with: feedObject)
            completion(nil, .success, nil, feedObject, nil)
            
            let feedDataClass = FeedDataClass.init(screen: self.screen)
            
            feedDataClass?.downvoteFeed(feed: feedObject, userId: Helper().getUserId(), status: 0, reason: "") { (feedResponse, downvoteAPIResponseState, downvoteNetworkError, downvoteError) in
                
                if downvoteError == nil {
                    feedDataClass?.upvoteFeed(feed: feedObject, userId: Helper().getUserId(), status: 1) { (feedResponse, apiResponseState, networkError, upvoteError) in
                        DispatchQueue.main.async {
                            if upvoteError == nil {
                                feedObject.hasUserUpVoted = true
                                feedObject.hasUserDownVoted = false
                                feedObject.upVotesCount = feedResponse?.upVotesCount
                                feedObject.downVotesCount = feedResponse?.downVotesCount
                            } else {
                                feedObject.hasUserUpVoted = false
                                feedObject.hasUserDownVoted = false
                                
                                if let count = feedObject.upVotesCount {
                                    feedObject.upVotesCount = count - 1
                                }
                            }
                            
                            self.updateFeed(with: feedObject)
                            completion(feedResponse, apiResponseState, networkError, feedObject, upvoteError)
                        }
                    }
                } else {
                    DispatchQueue.main.async {
                        completion(feedResponse, downvoteAPIResponseState, downvoteNetworkError, feedObject, downvoteError)
                    }
                }
            }
        }
    }
    
    // Downvote Feed View Model
    
    func downvoteFeed(feed: UserDataPlaceFeed, reason: String, completion: @escaping (_ feedResponse: UserDataPlaceFeed?, _ apiResponseState: APIResponseState, _ networkError: Error?, _ updatedFeed: UserDataPlaceFeed, _ error: ErrorObject?) -> Void) {
        var feedObject = feed
        
        if !(feed.hasUserUpVoted)  && !(feed.hasUserDownVoted) {
            // NOTE: No upvote and no downvote - So Downvoting here
            
            let feedDataClass = FeedDataClass.init(screen: self.screen)
            
            feedDataClass?.downvoteFeed(feed: feedObject, userId: Helper().getUserId(), status: 1, reason: reason) { (feedResponse, apiResponseState, networkError, downvoteError) in
                DispatchQueue.main.async {
                    if downvoteError == nil {
                        feedObject.hasUserUpVoted = false
                        feedObject.hasUserDownVoted = true
                        feedObject.upVotesCount = feedResponse?.upVotesCount
                        feedObject.downVotesCount = feedResponse?.downVotesCount
                        feedObject.isPostReportedLocalFlag = true
                    }
                    
                    self.updateFeed(with: feedObject)
                    completion(feedResponse, apiResponseState, networkError, feedObject, downvoteError)
                }
            }
        }
        else if (feed.hasUserUpVoted) && !(feed.hasUserDownVoted) {
            // NOTE: Already upvoted , So Removing upvote first and then downvoting!
            
            let feedDataClass = FeedDataClass.init(screen: self.screen)
            
            feedDataClass?.upvoteFeed(feed: feed, userId: Helper().getUserId(), status: 0) { (feedResponse, upvoteAPIResponseState, upvoteNetworkError, upvoteError) in
                if upvoteError == nil {
                    feedDataClass?.downvoteFeed(feed: feed, userId: Helper().getUserId(), status: 1, reason: reason, completion: { (feedResponse, downvoteAPIResponseState, downvoteNetworkError, downvoteError) in
                        DispatchQueue.main.async {
                            if downvoteNetworkError == nil {
                                feedObject.hasUserUpVoted = false
                                feedObject.hasUserDownVoted = true
                                feedObject.upVotesCount = feedResponse?.upVotesCount
                                feedObject.downVotesCount = feedResponse?.downVotesCount
                                feedObject.isPostReportedLocalFlag = true
                            }
                            
                            self.updateFeed(with: feedObject)
                            completion(feedResponse, downvoteAPIResponseState, downvoteNetworkError, feedObject, downvoteError)
                        }
                    })
                } else {
                    DispatchQueue.main.async {
                        completion(feedResponse, upvoteAPIResponseState, upvoteNetworkError, feedObject, upvoteError)
                    }
                }
            }
        }
        else if !(feed.hasUserUpVoted)  && (feed.hasUserDownVoted) {
            // NOTE: Downvoted already and no upvote - So removing downvote here
            
            let feedDataClass = FeedDataClass.init(screen: self.screen)
            
            feedDataClass?.downvoteFeed(feed: feedObject, userId: Helper().getUserId(), status: 0, reason: reason) { (feedResponse, apiResponseState, networkError, downvoteError) in
                DispatchQueue.main.async {
                    if downvoteError == nil {
                        feedObject.hasUserUpVoted = false
                        feedObject.hasUserDownVoted = false
                        feedObject.upVotesCount = feedResponse?.upVotesCount
                        feedObject.downVotesCount = feedResponse?.downVotesCount
                        feedObject.isPostReportedLocalFlag = true
                    }
                    
                    self.updateFeed(with: feedObject)
                    completion(feedResponse, apiResponseState, networkError, feedObject, downvoteError)
                }
            }
        }
    }

    func getFeedListInStory(userId: String, placeId: String, viewerId: String, latitude: Double?, longitude: Double?, archive: Bool, completion: @escaping(_ storyData: StoryData?, _ apiResponseState: APIResponseState, _ networkError: Error?, _ error: ErrorObject?) -> Void) {
        
        let feedDataClass = FeedDataClass(screen: self.screen)
        
        feedDataClass?.getStoryDetails(userId: userId, placeId: placeId, viewerId: viewerId, latitude: latitude, longitude: longitude, archive: archive, completion: { (storyData, apiResponseState, networkError, error) in
            var feedList = [UserDataPlaceFeed]()
            
            if let tempStoryData = storyData, let posts = tempStoryData.posts, posts.count > 0 {
                feedList = posts
            }
            
            self.setFeedList(feedList: feedList)
            DispatchQueue.main.async {
                completion(storyData, apiResponseState, networkError, error)
            }
            
        })
    }
    
    func deleteFeed(feed: UserDataPlaceFeed, completion: @escaping(_ feedResponse: UserDataPlaceFeed?, _ apiResponseState: APIResponseState, _ updatedFeed: UserDataPlaceFeed?, _ error: ErrorObject?) -> Void) {
        var feedObject = feed
        
        let feedDataClass = FeedDataClass(screen: self.screen)
        
        feedDataClass?.deleteFeed(feed: feed, completion: { (feedResponse, apiResponseState, networkError, error) in
            switch apiResponseState {
            case .success:
                feedObject.isPostDeletedLocalFlag = true
                self.updateFeed(with: feedObject)
                
            case .apiError, .networkError, .jsonParseError, .noInternet: break
            }
            
            completion(nil, apiResponseState, feedObject, error)
        })
    }
    
    func shareFeed(feed: UserDataPlaceFeed, completion: @escaping (_ shareFeedResponse: UserDataPlaceFeed?, _ apiResponseState: APIResponseState, _ networkError: Error?, _ updatedFeed: UserDataPlaceFeed, _ error: ErrorObject?) -> Void) {
        var feedObject = feed
        
        let feedDataClass = FeedDataClass(screen: self.screen)
        
        feedDataClass?.shareFeed(feed: feed, completion: { (shareFeedResponse, apiResponseState, networkError, error) in
            switch apiResponseState {
            case .success:
                if var shareCount = feedObject.sharesCount {
                    shareCount += 1
                    
                    feedObject.sharesCount = shareCount
                    self.updateFeed(with: feedObject)
                }
                
            case .apiError, .networkError, .jsonParseError, .noInternet: break
            }
            
            completion(shareFeedResponse, apiResponseState, networkError, feedObject, error)
        })
    }
    
    func addComment(feed: UserDataPlaceFeed, input: String, completion: @escaping (_ addFeedResponse: UserDataPlaceFeed, _ aPIResponseState: APIResponseState, _ networkError: Error?, _ updatedFeed: UserDataPlaceFeed, _ error: ErrorObject?, _ addedComment: Replies?) -> Void) {
        var feedObject = feed
        
        let feedDataClass = FeedDataClass(screen: self.screen)
        
        feedDataClass?.addComment(feed: feed, input: input, completion: { [weak self] (reply, apiResponseState, networkError, errorObject) in
            guard let this = self else { return }
            
            var replies = [Replies]()
            
            if let comments = feedObject.comments {
                comments.users.forEach { (reply) in
                    replies.append(reply)
                }
            }
            
            if let reply = reply {
                replies.append(reply)
            }
            
            if feedObject.comments == nil {
                // If comments is empty then create a comment object
                let comments = Comments.init(users: replies, nextPageToken: "")
                feedObject.comments = comments
            } else {
                feedObject.comments?.users = replies
            }
            
            this.updateFeed(with: feedObject)
            DispatchQueue.main.async {
                completion(feedObject, .success, networkError, feedObject, errorObject, reply)
            }
            
        })
    }
    
    func deleteComment(feed: UserDataPlaceFeed, commentToDelete: Replies, completion: @escaping (_ aPIResponseState: APIResponseState, _ networkError: Error?, _ updatedFeed: UserDataPlaceFeed?, _ error: ErrorObject?, _ deletedComment: Replies?) -> Void) {
        var feedObject = feed
        
        let feedDataClass = FeedDataClass(screen: self.screen)
        
        feedDataClass?.deleteComment(feed: feed, commentToDelete: commentToDelete, completion: { [weak self] (_, apiResponseState, networkError, errorObject) in
            // NOTE: Not to use 'updatedFeed' i.e. first param because API returns `replies` array and not `comments` model
            guard let this = self else { return }
            
            var replies = [Replies]()
            
            if let comments = feedObject.comments {
                comments.users.forEach { (reply) in
                    if reply._id != commentToDelete._id {
                        replies.append(reply)
                    }
                }
            }
            
            feedObject.comments?.users = replies
            
            this.updateFeed(with: feedObject)
            
            DispatchQueue.main.async {
                completion(.success, networkError, feedObject, errorObject, commentToDelete)
            }
        })
    }
    
    func getDurationArray() -> [TimeInterval] {
        var timeIntervalArray = [TimeInterval]()
        
        if self.getFeedCount() > 0 {
            timeIntervalArray = getFeedList().compactMap({ (post) -> TimeInterval? in
                guard let otherContext = post.getOtherContext() else { return nil }
                if let duration = otherContext.videoDuration, otherContext.video != "" {
                    // feed with video
                    return TimeInterval(duration)
                } else {
                    // feed with image
                    return 7
                }
            })
        }
        
        return timeIntervalArray
    }
}
