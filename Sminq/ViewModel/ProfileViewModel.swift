//
//  ProfileViewModel.swift
//  Sminq
//
//  Created by Avinash Thakur on 06/03/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import UIKit
import Alamofire
import Gloss
import Foundation
import Firebase

class ProfileViewModel: NSObject {

    var displayName: String!
    var isMyProfile: Bool!
    var userProfileData: UserDataDict?
    var userMetricsData: UserMetrics?
    var userRatingsData = [StoryData]()
    var userLevelsList = [LevelsList]()
    var userStreaks: Streaks?
    var userHighlight: AchievementHighlight?
    var userClaims: Claims?
    
    init(displayName: String, isMyProfile: Bool) {
        self.displayName = displayName
        self.isMyProfile = isMyProfile
    }
    
    func getUserProfileDetails(enableCache: Bool, completion: @escaping(_ result: APIResponseState, _ networkError: Error?, _ error: ErrorObject?) -> Void) {
        let profileDataClass = ProfileDataClass()
        profileDataClass.isMyProfile = isMyProfile
        profileDataClass.getUserProfileDetails(userDisplayName: self.displayName, enableCache: enableCache, completion: { (result, apiResponseState, networkError, error) in
            switch apiResponseState {
            case .success:
                self.setProfileDetails(data: result)
                DispatchQueue.main.async {
                    completion(.success, nil, nil)
                }
                
            case .apiError, .networkError, .jsonParseError, .noInternet:
                DispatchQueue.main.async {
                    completion(apiResponseState, networkError, error)
                }
            }
        })
    }
    
    func setProfileDetails(data: UserProfileData?) {
        self.userProfileData = data?.user
        self.userMetricsData = data?.userMetrics
        self.userRatingsData = data?.stories ?? []
        self.userStreaks = data?.streaks
        self.userLevelsList = data?.levelsList ?? []
        self.userHighlight = data?.highlight
        self.userClaims = data?.claims
        self.userRatingsData = self.userRatingsData.compactMap { (story) -> StoryData? in
            guard let place = story.placeDetails, let posts = story.posts else { return nil }
            let feedViewModel = FeedViewModel.init(screen: "")
            feedViewModel?.setFeedList(feedList: posts)
            
            return StoryData.init(userId: "", placeId: "", placeDetails: place, archived: false, posts: posts, feedViewModel: feedViewModel)
        }
    }
    
    func updateUser(userId: String, name: String?, displayName: String?, email: String?, mobile: String?, latitude: Double?, longitude: Double?, countryName: String?, cityName: String?, countryCode: String?, gender: String?, birthDate: String?, isSignUp: Int, isExistingUser: Bool, bio: String?, image: String?, completion: @escaping(_ userDataDict: UserDataDict?, _ apiResponseState: APIResponseState, _ networkError: Error?, _ error: ErrorObject?) -> Void) {
        
        // SVProgressHUD.show()
        
        guard let metric = HTTPMetric(url: URL(string: UserService().editUserAPIEndPoint(userId: userId))!, httpMethod: .put) else { return }
        metric.start()
        
        let updateUserAPI = UserService().editUser(userId: userId, name: name, displayName: displayName, email: email, mobile: mobile, latitude: latitude, longitude: longitude, countryName: countryName, cityName: cityName, countryCode: countryCode, gender: gender, birthDate: birthDate, isSignUp: isSignUp, isExistingUser: isExistingUser, bio: bio, image: image)
        
        let trace = Performance.startTrace(name: "\(AppConstants.apiNames.updateUserProfile) API loading")
        trace?.start()
        
        updateUserAPI.response.validate(statusCode: 200..<300).responseJSON { response in
//            debugPrint(response)
            
            if let httpCode = response.response?.statusCode {
                metric.responseCode = httpCode
            }
            
            switch response.result {
            case .success:
                guard let json = response.result.value as? JSON else {
                    let errorObject = ErrorObject.init(apiName: AppConstants.apiNames.updateUserProfile, errorType: .jsonParsingError, httpCode: nil, errorTitle: AppConstants.ErrorMessages.opsSomethingWentWrong, errorDescription: AppConstants.ErrorMessages.serverBusyPleaseTryAgain)
                    completion(nil, .jsonParseError, nil, errorObject)
                    return
                    
                }
                if let editResponse: UserDataDict = UserDataDict(json: json) {
                    
                    let profileDataClass = ProfileDataClass()
                    
                    if let userCachedData = profileDataClass.getProfileCachedData() {
                        // Only user is updated. Hence update user object from cache. And preserve old userMetrics and stories cache
                        profileDataClass.saveProfileData(userProfileData: UserProfileData.init(user: editResponse, userMetrics: userCachedData.userMetrics, stories: userCachedData.stories, levelsList: userCachedData.levelsList, streaks: userCachedData.streaks, highlight: userCachedData.highlight, claims: userCachedData.claims))
                        completion(editResponse, .success, nil, nil)
                    }
                    
                    
                    
                    AnalyticsHelper.pushUserProfileToCleverTap(user: editResponse)
                    
                    
                } else {
                    LogUtil.error(response)
                    
                    AnalyticsHelper.APIFailureEvent(screen: AppConstants.screenNames.editProfile, apiName: AppConstants.apiNames.updateUserProfile, errorMessage: AppConstants.ErrorMessages.emptyResponse, userId: Helper().getUserId(), errorCode: nil, requestPayload: updateUserAPI.requestPayload, requestMethod: updateUserAPI.requestMethod.rawValue, requestPathParams: updateUserAPI.requestPathParams)
                    
                    let errorObject = ErrorObject.init(apiName: AppConstants.apiNames.updateUserProfile, errorType: .jsonParsingError, httpCode: nil, errorTitle: AppConstants.ErrorMessages.opsSomethingWentWrong, errorDescription: AppConstants.ErrorMessages.serverBusyPleaseTryAgain)
                    
                    completion(nil, .jsonParseError, nil, errorObject)
                    
                }

                break
                
            case .failure(let error):
                LogUtil.error(error)
                
                let error = error as NSError
                
                var errorObject: ErrorObject? = ErrorObject(apiName: AppConstants.apiNames.getUserTimeline, errorType: .unknown, httpCode: error.code, errorTitle: "", errorDescription: AppConstants.ErrorMessages.serverError)
                
                if let httpCode = response.response?.statusCode {
                    errorObject = APIErrorCodes.getErrorObject(apiName: AppConstants.apiNames.getUserTimeline, httpCode: httpCode)
                }
                
                if let errorDesc = error.userInfo.valueForKeyPath(keyPath: "NSLocalizedDescription") as? String {
                     AnalyticsHelper.APIFailureEvent(screen: AppConstants.screenNames.editProfile, apiName: AppConstants.apiNames.updateUserProfile, errorMessage: errorDesc, userId: Helper().getUserId(), errorCode: nil, requestPayload: updateUserAPI.requestPayload, requestMethod: updateUserAPI.requestMethod.rawValue, requestPathParams: updateUserAPI.requestPathParams)
                }
                
                completion(nil, .networkError, error, errorObject)
             
                
                break
            }
            
            metric.stop()
            trace?.stop()
        }
    }
    
    func checkIfUserHasNoLiveRatings() -> Bool {
        let noRatings = self.userRatingsData.count <= 0 ? true : false
        return noRatings
    }
    
    func getRatingsSectionHeight() -> CGFloat {
        var totalCount = 0
        let ratingsCount = self.userRatingsData.count
        if ratingsCount > 0 {
            totalCount = ratingsCount % 2 == 1 ? (ratingsCount / 2) + 1 : (ratingsCount / 2)
            return CGFloat((totalCount * 306) + 20)
        }
        return CGFloat(298)
    }
    
    func getBadgesSectionHeight() -> CGFloat {
        return CGFloat((344 * 3) + 56 + 112)
    }
    
    func checkIfUserHasRewardsToClaim() -> (Bool, String) {
        if let claim = self.userClaims {
            if claim.claimable {
                return (true, claim.claimMessage)
            }
        }
        return (false, "")
    }
    
    func claimRewards(emailID: String, completion: @escaping(_ result: APIResponseState, _ networkError: Error?, _ error: ErrorObject?) -> Void) {
        guard let user = Helper().getUser() else { return }
        let profileDataClass = ProfileDataClass()
        profileDataClass.isMyProfile = isMyProfile
        profileDataClass.claimUserReward(userID: user._id, emailID: emailID, completion: { (result, apiResponseState, networkError, error) in
            switch apiResponseState {
            case .success:
                DispatchQueue.main.async {
                    completion(.success, nil, nil)
                }
            case .apiError, .networkError, .jsonParseError, .noInternet:
                DispatchQueue.main.async {
                    completion(apiResponseState, networkError, error)
                }
            }
        })
    }
    
    func getUserUnlockedLevel(completion: @escaping(_ result: [LevelsList]?, _ networkError: Error?, _ error: ErrorObject?) -> Void) {
        let profileDataClass = ProfileDataClass()
        profileDataClass.isMyProfile = isMyProfile
        profileDataClass.getUserUnlockedLevel(completion: { (result, apiResponseState, networkError, error) in
            switch apiResponseState {
            case .success:
                DispatchQueue.main.async {
                    completion(result, nil, nil)
                }
            case .apiError, .networkError, .jsonParseError, .noInternet:
                DispatchQueue.main.async {
                    completion([], networkError, error)
                }
            }
    })
    }
    
    func updatePost(post: UserDataPlaceFeed) {
        if userRatingsData.count > 0 {
            for (storyIndex, story) in userRatingsData.enumerated() {
                if let posts = story.posts {
                    for (postIndex, postObject) in posts.enumerated() {
                        if postObject._id == post._id {
                            self.userRatingsData[storyIndex].posts?[postIndex] = post
                        }
                    }
                }
            }
        }
    }
}
