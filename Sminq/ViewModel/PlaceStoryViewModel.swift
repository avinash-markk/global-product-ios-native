//
//  PlaceStoryViewModel.swift
//  Sminq
//
//  Created by Pushkar Deshmukh on 11/03/19.
//  Copyright © 2019 Sminq. All rights reserved.
//

import Foundation

class PlaceStoryViewModel {
    private(set) var screen = ""
    private(set) var places = [PlacesStoriesData]()
    private(set) var placesCopy = [PlacesStoriesData]() // These are used for filters
    
    public var isArchived = false
    
    init?(screen: String) {
        self.screen = screen
    }
    
    func setPlaceStoryList(places: [PlacesStoriesData]) {
        self.places = places
        self.placesCopy = self.places
    }
    
    func emptyPlaces() {
        self.places.removeAll()
    }
    
    func getPlacesCount() -> Int {
        return self.places.count
    }
    
    func getPlaceAt(index: Int) -> PlacesStoriesData? {
        if index < self.places.count {
            return self.places[index]
        }
        
        return nil
    }
    
    func getTimeline(latitude: Double, longitude: Double, distance: String, enableCache: Bool, completion: @escaping(_ placesData: [PlacesStoriesData]?, _ apiResponseState: APIResponseState, _ networkError: Error?, _ error: ErrorObject?) -> Void) {
        let placeDataClass = PlaceDataClass(screen: self.screen)
        
        placeDataClass?.getTimeline(latitude: latitude, longitude: longitude, distance: distance, enableCache: enableCache, completion: { (placesData, apiResponseState, networkError, error) in
            
            if var tempPlacesData = placesData {
                for (index, _) in tempPlacesData.enumerated() {
                    if let stories = tempPlacesData[index].stories {
                        tempPlacesData[index].stories = tempPlacesData[index].copyPlaceDetailsToPostsAndReturnStories(stories: stories)
                    }
                }
                
                self.setPlaceStoryList(places: tempPlacesData)
                
                DispatchQueue.main.async {
                    completion([], apiResponseState, networkError, error)
                }
                
            } else {
                DispatchQueue.main.async {
                    completion(nil, apiResponseState, networkError, error)
                }
            }
        })
    }
    
    func getPlaceStories(placeId: String, latitude: Double, longitude: Double, userId: String, completion: @escaping(_ placeData: [PlacesStoriesData]?, _ apiResponseState: APIResponseState, _ networkError: Error?, _ error: ErrorObject?) -> Void) {
        
        let placeDataClass = PlaceDataClass(screen: self.screen)
        
        placeDataClass?.getPlaceStories(placeId: placeId, latitude: latitude, longitude: longitude, userId: userId, completion: { (placeStoriesData, apiResponseState, networkError, error) in
            if var tempPlacesData = placeStoriesData {
                if let stories = tempPlacesData.stories, stories.count > 0 {
                    tempPlacesData.stories = tempPlacesData.copyPlaceDetailsToPostsAndReturnStories(stories: stories)
                    self.isArchived = false
                } else if let archivedStories = tempPlacesData.archiveStories, archivedStories.count > 0 {
                    tempPlacesData.stories = tempPlacesData.copyPlaceDetailsToPostsAndReturnStories(stories: archivedStories)
                    self.isArchived = true
                }
                
                self.emptyPlaces()
                self.setPlaceStoryList(places: [tempPlacesData])
                
                DispatchQueue.main.async {
                    completion([tempPlacesData], apiResponseState, networkError, error)
                }
                
            } else {
                DispatchQueue.main.async {
                    completion(nil, apiResponseState, networkError, error)
                }
            }
        })
    }
    
    func getPlaceStories(slug: String, completion: @escaping(_ placeData: [PlacesStoriesData]?, _ apiResponseState: APIResponseState, _ networkError: Error?, _ error: ErrorObject?) -> Void) {
        
        let placeDataClass = PlaceDataClass(screen: self.screen)
        
        placeDataClass?.getPlaceStories(slug: slug, completion: { (placeStoriesData, apiResponseState, networkError, error) in
            if let tempPlacesData = placeStoriesData {
                self.emptyPlaces()
                self.setPlaceStoryList(places: [tempPlacesData])
                
                DispatchQueue.main.async {
                    completion([tempPlacesData], apiResponseState, networkError, error)
                }
                
            } else {
                DispatchQueue.main.async {
                    completion(nil, apiResponseState, networkError, error)
                }
            }
        })
    }
    
    func getPlaceStories(postId: String, displayName: String, completion: @escaping(_ placeData: [PlacesStoriesData]?, _ apiResponseState: APIResponseState, _ networkError: Error?, _ error: ErrorObject?) -> Void) {
        
        let placeDataClass = PlaceDataClass(screen: self.screen)
        
        placeDataClass?.getPlaceStories(postId: postId, displayName: displayName, completion: { (placeStoriesData, apiResponseState, networkError, error) in
            if let tempPlacesData = placeStoriesData {
                self.emptyPlaces()
                self.setPlaceStoryList(places: [tempPlacesData])
                
                DispatchQueue.main.async {
                    completion([tempPlacesData], apiResponseState, networkError, error)
                }
                
            } else {
                DispatchQueue.main.async {
                    completion(nil, apiResponseState, networkError, error)
                }
            }
        })
    }
    
    func filterPlacesByMood(placeMood: [Int]) {
        self.places = self.placesCopy
        
        if placeMood.count > 0 {
            var filteredPlaces = [PlacesStoriesData]()
            
            filteredPlaces = self.places.filter { place in
                
                var placeFound = false
                
                for mood in placeMood {
                    if let placeStoryMood = place.placeDetails.placeMoodScore, placeStoryMood == mood {
                        placeFound = true
                    }
                }
                
                if placeFound {
                    return true
                } else {
                    return false
                }
            }
            
            self.places = filteredPlaces
        }
    }
    
    func convertToStoryDataList(filterByPostId: String?) -> [StoryData] {
        var storyDataList = [StoryData]()
        
        places.forEach { (placeStoryData) in
            if let storyData = placeStoryData.convertToStoryDataFromPlaceStoriesData(filterByPostId: filterByPostId) {
                storyDataList.append(storyData)
            }
        }
        
        return storyDataList
    }
    
    func updatePost(post: UserDataPlaceFeed) {
        if places.count > 0 {
            for (placeIndex, place) in places.enumerated() {
                if let stories = place.stories {
                    for (storyIndex, story) in stories.enumerated() {
                        for (postIndex, postObject) in story.posts.enumerated() {
                            if postObject._id == post._id {
                                self.places[placeIndex].stories?[storyIndex].posts[postIndex] = post
                            }
                        }
                    }
                }
                
            }
        }
    }
}
