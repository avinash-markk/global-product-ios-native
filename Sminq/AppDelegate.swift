//
//  AppDelegate.swift
//  Sminq
//
//  Created by Bjorn Mascarenhas on 21/02/18.
//  Copyright © 2018 Suraj Sagare. All rights reserved.
//

import CleverTapSDK
import UIKit
import Firebase
import GoogleSignIn
import UserNotifications
import FirebaseRemoteConfig
import MapKit
import GooglePlaces
import GoogleMaps
import SVProgressHUD
import Fabric
import Crashlytics
import Alamofire
import FirebaseDynamicLinks
import Gloss
import AVFoundation
import AppsFlyerLib
import FirebaseMessaging
import FacebookCore
import FBSDKCoreKit

var AFManager = SessionManager()

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    var lastAppLaunchedTime: TimeInterval?
    var minSessionMinutes: NSInteger = 20
    
    var tabBarViewController: TabbarViewController?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        audioConfig()
        setupCleverTap()
        Fabric.with([Crashlytics.self])
        initAppsFlyer() // NOTE: Apps Flyer should be initialised after clevertap. Since profile attributes are passed
        BugfenderService.shared.configure()
        initFirebase()
        GMSPlacesClient.provideAPIKey(Config.getGoogleAPIKey())
        GMSServices.provideAPIKey(Config.getGoogleAPIKey())
        setupRemoteConfig()
        
//        UserNoficationService.shared.authorize()
        
        // Google Sign In
        GIDSignIn.sharedInstance().clientID = FirebaseApp.app()?.options.clientID
        
        setupWindow()
        Messaging.messaging().delegate = self
        // Set FCM token in userdafaults
        
        UserNoficationService.shared.subscribeToPushNotifications()
        
        // FCM Refresh Token Observer
        NotificationCenter.default.addObserver(self, selector: #selector(self.tokenRefreshNotification(notification:)), name: NSNotification.Name.InstanceIDTokenRefresh, object: nil)
        
        
        setupAlamofireManager()
        handleSelectedNotification(launchOptions: launchOptions)
        cacheAllDeliveredNotifications()
        
        return true
    }
    
    // MARK: Fileprivate functions
    
    fileprivate func setupCleverTap() {
        CleverTap.setDebugLevel(CleverTapLogLevel.debug.rawValue)
        CleverTap.setCredentialsWithAccountID(SminqAPI.cleverTapAccountId(), andToken: SminqAPI.cleverTapToken())
        
    }
    
    fileprivate func initAppsFlyer() {
        if ApiConfig.appEnvironment == ENVIRONMENT.production {
            AppsFlyerTracker.shared().delegate = self
        }
        
        AppsFlyerTracker.shared().appsFlyerDevKey = Config.getAppsFlyerDevKey()
        AppsFlyerTracker.shared().appleAppID = Config.getAppsFlyerAppleId()
        AppsFlyerTracker.shared()?.customerUserID = CleverTap.sharedInstance()?.profileGetAttributionIdentifier()
        //        AppsFlyerTracker.shared().isDebug = true
    }
    
    fileprivate func initFirebase() {
        var googleServicesPList: String = ""
        
        // Pickup staging and production plists
        switch ApiConfig.appEnvironment {
        case .staging, .development:
            googleServicesPList = "GoogleService-Info-Staging"
        case .production:
            googleServicesPList = "GoogleService-Info-Production"
        }
        
        // FCM Integration
        let filePath = Bundle.main.path(forResource: googleServicesPList, ofType: "plist")!
        let options = FirebaseOptions(contentsOfFile: filePath)
        FirebaseApp.configure(options: options!)
    }
    
    fileprivate func setupRemoteConfig() {
        RemoteConfig.remoteConfig().setDefaults(ApiConfig.remoteconfigDefaults)
        
        let fetchDuration: TimeInterval = 0
        RemoteConfig.remoteConfig().fetch(withExpirationDuration: fetchDuration) {
            [weak self] (status, error) in
            // Error
            guard error == nil else {
                print ("Uh-oh. Got an error fetching remote values \(String(describing: error))")
                return
            }
            // Success
            RemoteConfig.remoteConfig().activateFetched()
            self?.showForceUpgradeCheck()
        }
    }
    

    fileprivate func setupWindow() {
        // Initialize window
        self.window = UIWindow.init(frame: UIScreen.main.bounds)
        
        let isOnBoardingDone = UserDefaultsUtility.shared.getOnBoardingDoneFlag()
        
        if isOnBoardingDone {
            let signedIn = UserDefaults.standard.bool(forKey: "SignIn")
            if !signedIn {
                setLoginVCAsRootController()
            } else {
                setTimelineVCAsRootController()
            }
        } else {
            setOnboardingVCAsRootController()
        }
        
        self.window!.backgroundColor = UIColor.white
        self.window?.makeKeyAndVisible()
    }
    
    fileprivate func setTimelineVCAsRootController() {
        self.tabBarViewController = AppConstants.storyboard.instantiateViewController(withIdentifier: "TabbarViewController") as? TabbarViewController
        self.tabBarViewController?.selectedIndex = 0
        self.tabBarViewController?.previousController = TimelineViewController()
        
        self.window!.rootViewController = self.tabBarViewController
        if InternetConnection.isConnectedToInternet() {
            NotificationCenter.default.post(name: NSNotification.Name("Check_For_Failed_Uploads"), object: nil, userInfo: nil)
        }
    }
    
    fileprivate func setOnboardingVCAsRootController() {
        let onBoardingViewController = UINavigationController(rootViewController: AppConstants.storyboard.instantiateViewController(withIdentifier: "OnBoardingViewController") as! OnBoardingViewController)
        self.window!.rootViewController = onBoardingViewController
    }
    
    fileprivate func setLoginVCAsRootController() {
        let loginViewController = UINavigationController(rootViewController: AppConstants.storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController)
        self.window!.rootViewController = loginViewController
    }
    
    
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        AppsFlyerTracker.shared().handleOpen(url, sourceApplication: sourceApplication, withAnnotation: annotation)
        
        if let dynamicLink = DynamicLinks.dynamicLinks().dynamicLink(fromCustomSchemeURL: url) {
            handleDynamicLink(dynamicLink, isFirstInstalled: false)
            return true
        }
    
        if GIDSignIn.sharedInstance().handle(url,
                                             sourceApplication: sourceApplication,
                                             annotation: annotation) {
            return true
        }
        
        return false
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey: Any] = [:]) -> Bool {
        AppsFlyerTracker.shared().handleOpen(url, options: options)
        // NOTE: This is called when app has URL schemes and installed
        
        if let dynamicLink = DynamicLinks.dynamicLinks().dynamicLink(fromCustomSchemeURL: url) {
            self.handleDynamicLink(dynamicLink, isFirstInstalled: true)
            return true
        } else {
            return application(app, open: url,
                               sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String,
                               annotation: "")
        }
        
    }
    
    func application(_ application: UIApplication, continue userActivity: NSUserActivity,
                     restorationHandler: @escaping ([Any]?) -> Void) -> Bool {
        AppsFlyerTracker.shared().continue(userActivity, restorationHandler: restorationHandler)
        
        let handled = DynamicLinks.dynamicLinks().handleUniversalLink(userActivity.webpageURL!) { (dynamicLink, error) in
            print("Handled")
            if let dynamicLink = dynamicLink {
                self.handleDynamicLink(dynamicLink, isFirstInstalled: false)
            }
            
        }
        return handled
    }
    
    func application(_ application: UIApplication, continue userActivity: NSUserActivity,
                     restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
        let handled = DynamicLinks.dynamicLinks().handleUniversalLink(userActivity.webpageURL!) { (dynamicLink, error) in
            print("Handled")
            if let dynamicLink = dynamicLink {
                self.handleDynamicLink(dynamicLink, isFirstInstalled: false)
            }
            
        }
        
        return handled
    }
    
    
    
    func handleDynamicLink(_ dynamicLink: DynamicLink, isFirstInstalled: Bool) {
        print("Your Dynamic Link parameter: \(dynamicLink)")
        // TODO: Update screen name
        
        
        if let url = dynamicLink.url, let decodedUrl = url.getDecodedURL() {
            print("\n", url)
            print("\n", decodedUrl)
            let firstPathParam = decodedUrl.path.getFirstPathParam()
            
            if firstPathParam == "places" {
                AnalyticsHelper.dynamicLinkOpenedEvent(dynamicUrl: decodedUrl.absoluteString, type: .place, isFirstInstalled: isFirstInstalled)
                handlePlaceNavigation(slug: decodedUrl.path.getLastPathParam())
            } else if firstPathParam == "story" {
                AnalyticsHelper.dynamicLinkOpenedEvent(dynamicUrl: decodedUrl.absoluteString, type: .ratings,isFirstInstalled: isFirstInstalled)
                if let postId = decodedUrl.getQueryString(parameter: AppConstants.QueryParamKeys.storyId) {
                    handleStoryNavigation(postId: postId, displayName: decodedUrl.path.getSecondPathParam())
                }
            } else if firstPathParam == "profile" {
                // TODO: User handling
            }
            
        }
    }
    
    func handlePlaceNavigation(slug: String) {
        guard let _ = Helper().getUser() else { return }
        
        let placeStoryViewModel = PlaceStoryViewModel.init(screen: AppConstants.screenNames.placeDetails)!
        
        SVProgressHUD.show()
        placeStoryViewModel.getPlaceStories(slug: slug) { (placesStoriesData, apiResponseState, networkError, error) in
            SVProgressHUD.dismiss()
            
            if let _ = error {
                return
            }
            
            if let placeFeedViewController = AppConstants.storyboard.instantiateViewController(withIdentifier: "PlaceFeedViewController") as? PlaceFeedViewController {
                placeFeedViewController.place = placesStoriesData?.first?.placeDetails
                
                let navController = UINavigationController(rootViewController: placeFeedViewController)
                AppConstants.appDelegate.window?.rootViewController?.present(navController, animated: true, completion: nil)
            }
        }
    }
    
    func handleStoryNavigation(postId: String, displayName: String) {
        guard let _ = Helper().getUser() else { return }
        
        let placeStoryViewModel = PlaceStoryViewModel.init(screen: AppConstants.screenNames.placeDetails)!
        
        SVProgressHUD.show()
        placeStoryViewModel.getPlaceStories(postId: postId, displayName: displayName) { (placesStoriesData, apiResponseState, networkError, error) in
            SVProgressHUD.dismiss()
            if let _ = error {
                return
            }
            
            let storyViewManager = StoryViewManager.init(screen: AppConstants.screenNames.home, feedItemId: postId)
            let storyViewController = StoryViewController.init(storyViewManager: storyViewManager)
            
            let storyViewModel = StoryViewModel.init(screen: AppConstants.screenNames.storyView, storyList: placeStoryViewModel.convertToStoryDataList(filterByPostId: postId))!
            storyViewController.storyViewModel = storyViewModel
            
            let navController = UINavigationController(rootViewController: storyViewController)
            navController.modalPresentationStyle = .overFullScreen
            navController.modalPresentationCapturesStatusBarAppearance = true
            
            AppConstants.appDelegate.window?.rootViewController?.present(navController, animated: true, completion: nil)
            
        }
        
        
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
//        print("applicationDidEnterBackground")
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        
        // track session time on clevertap
        let currentTS = NSDate().timeIntervalSince1970 // current time stamp
        guard let lastAppLaunchTS = lastAppLaunchedTime else {
            return
        }
        var sessionTime = Int(currentTS - lastAppLaunchTS) // The difference between current time and last time app launched returns the time spent by user
        
        if sessionTime > self.minSessionMinutes * 60 {
            sessionTime = 0
        } else {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MMM d, yyyy" // Specify your format that you want
            
            let date = Date(timeIntervalSince1970: currentTS)
            let strDate = dateFormatter.string(from: date)
            AnalyticsHelper.sessionStoppedEvent(time: Int(sessionTime/60), date: strDate)
            //            CleverTap.sharedInstance().recordEvent("Session Stopped", withProps: ["Session Time":Int(sessionTime/60)])
            sessionTime = 0
        }
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
//        AppEventsLogger.activate(application)
        AppEvents.activateApp()
        
//        print("applicationDidBecomeActive")
        
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        
        // track session time on clevertap
        
        // NOTE: Run this on bakcground this. W/o running on background thread freezes and blocks ui whenever app comes in active state
        DispatchQueue.global(qos: .background).async {
            
            
            self.lastAppLaunchedTime = NSDate().timeIntervalSince1970
            
            if ApiConfig.appEnvironment == ENVIRONMENT.production {
                AppsFlyerTracker.shared().trackAppLaunch()
            }
        }
        
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
    }

    // Force upgrade logic
    func showForceUpgradeCheck() {
        guard let shortVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String,
            let remoteConfigBuildVersion = RemoteConfig.remoteConfig().configValue(forKey: "force_update_ios_versioncode").stringValue
            else { return }
        
        if remoteConfigBuildVersion.compare(shortVersion, options: .numeric) == .orderedDescending {
            AnalyticsHelper.forceUpdateEvent(currentVersion: shortVersion, remoteConfigVersion: remoteConfigBuildVersion)
            self.showForceUpgradeModal()
        }
    }
    
    // Show Force upgrade Viewcontroller
    func showForceUpgradeModal() {
        let forceUpgradeModalViewController = AppConstants.storyboard.instantiateViewController(withIdentifier: "ForceUpgradeModalViewController") as! ForceUpgradeModalViewController
        forceUpgradeModalViewController.modalPresentationStyle = .custom
        forceUpgradeModalViewController.modalTransitionStyle = .crossDissolve
        window!.rootViewController?.present(forceUpgradeModalViewController, animated: true, completion: nil)
    }
    
    // mark :- FCM Push Notification Methods
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        completionHandler(UIBackgroundFetchResult.newData)
    }
    
    // The FCM registration token.
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        CleverTap.sharedInstance()?.setPushToken(deviceToken)
        AppsFlyerTracker.shared().registerUninstall(deviceToken)
        
        InstanceID.instanceID().instanceID { (result, error) in
            if let _ = error {
                print("error fetching remote instance id")
            } else if let result = result, let _ = Helper().getUser() {
                UserDefaults.standard.set(result.token, forKey: "FCMToken")
                PushNotificationAPI.registerForSminqPushNotifications(userId: Helper().getUserId(), fcmToken: UserDefaults.standard.string(forKey: "FCMToken")!, register: AppConstants.enablePush)
            }
        }
    }
    
    // FCM Token Refresh
    @objc func tokenRefreshNotification(notification: NSNotification) {
        UserNoficationService.shared.subscribeToPushNotifications()
    }
}

extension AppDelegate: AppsFlyerTrackerDelegate {
    
}

extension AppDelegate: MessagingDelegate {
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("MessagingDelegate")
        print(fcmToken)
        
        UserNoficationService.shared.subscribeToPushNotifications()
    }
}
